﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Erp.Core.Services.Store;
using Erp.Infrastructure;
using Microsoft.AspNetCore.Mvc;

namespace Erp.App.Components
{
    public class ItemList : ViewComponent
    {
        //private StoreService _storeService;
        public ItemList(InfrastructureDbContext db)
        {
            //_storeService = new StoreService(db); 
        }

        public IViewComponentResult Invoke(VmStoreIn vmStoreIn)
        {
            // var data = new VmStoreIn(); 
            //var data = _storeService.GetStoreInByPoId(vmStoreIn);
            return View(); 
        }
    }
}
