﻿using Erp.Core.Services.Payroll;
using Erp.Infrastructure;
using Microsoft.AspNetCore.Mvc;
using System;

namespace Erp.App.Components
{
    public class EmployeeSalaryIncreamentViewComponent : ViewComponent
    {
        private readonly PayrollService _service;
        public EmployeeSalaryIncreamentViewComponent(InfrastructureDbContext db)
        {
            _service = new PayrollService(db);
        }
        public IViewComponentResult Invoke(int bunit, int unit, int dept, int section,DateTime fromdate, DateTime todate)
        {
            var model = new VMSalaryIncreament();
            model = _service.FilterIncreamentEmployeeSalary(bunit, unit, dept, section,fromdate,todate);
            model.FromDate = fromdate;
            model.ToDate = todate;
            return View("IncreamentSalaryList", model);
        }
    }
}
