﻿using Erp.Core.Services;
using Erp.Core.Services.Production;
using Erp.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Erp.App.Components
{
    public class ProductionUIViewComponent : ViewComponent
    {
        private readonly ProductionService _service;
        private readonly HttpContext httpContext;
        public ProductionUIViewComponent(InfrastructureDbContext db, IHttpContextAccessor httpContextAccessor)
        {
            httpContext = httpContextAccessor.HttpContext;
            _service = new ProductionService(db, httpContextAccessor.HttpContext);
        }

        public IViewComponentResult Invoke(int RID, int ModeID, int SectionID)
        {
            if (SectionID == 1)
            {
                ViewBag.StyleList = new SelectList(_service.DDLStyleForCutting(), "Value", "Text");
                ViewBag.LineList = new SelectList(_service.DDLCuttingTable(), "Value", "Text");
                var model = new VMProductionReference();
                model.vmDailyProduction = new VMDailyProduction();
                
                if (ModeID != 0)
                {
                    model.ActionId = (int)ActionEnum.Edit;
                    model.vmDailyProduction = _service.CuttingPlanGetByID(ModeID);
                    return View("_CuttingPlanUpdateUI", model);
                }
                else
                {
                    model.vmDailyProduction.Prod_ReferenceFK = RID;
                    return View("_CuttingPlanUI", model);
                }
            }
            else if (SectionID == 2)
            {
                ViewBag.StyleList = new SelectList(_service.DDLStyleForSewing(), "Value", "Text");
                ViewBag.LineList = new SelectList(_service.DDLSewingLine(), "Value", "Text");
                var model = new VMProductionReference();
                
                model.vmDailyProduction = new VMDailyProduction();
                model.LinePlanList = new List<VMLinePlanHourlyChart>();
                model.LinePlanList = _service.GetDailyLineHourlyOccupied(RID);
                
                if (ModeID != 0)
                {
                    model.ActionId = (int)ActionEnum.Edit;
                    model.vmDailyProduction = _service.SewingPlanGetByID(ModeID);
                    return View("_SewingPlanUpdateUI", model);
                }
                else
                {
                    model.vmDailyProduction.Prod_ReferenceFK = RID;
                    return View("_SewingPlanUI", model);
                }
            }
            else if (SectionID == 3)
            {
                ViewBag.StyleList = new SelectList(_service.DDLStyleForPlanIroning(), "Value", "Text");
                var model = new VMProductionReference();
                model.vmDailyProduction = new VMDailyProduction();
                model.vmDailyProduction.Prod_ReferenceFK = RID;
                if (ModeID == 0)
                {
                    return View("_IroningPlanUI", model);
                }
                else
                {
                    return View("_IroningPlanUI", model);
                }
            }
            else if (SectionID == 4)
            {
                ViewBag.StyleList = new SelectList(_service.DDLStyleForPacking(), "Value", "Text");
                var model = new VMProductionReference();
                model.vmDailyProduction = new VMDailyProduction();
                model.vmDailyProduction.Prod_ReferenceFK = RID;
                if (ModeID == 0)
                {
                    return View("_PackingPlanUI", model);
                }
                else
                {
                    return View("_PackingPlanUI", model);
                }
            }
            else
            {
                return View();
            }
        }
    }
}
