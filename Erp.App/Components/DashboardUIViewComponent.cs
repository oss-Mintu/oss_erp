﻿using Erp.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Erp.Core.Services;
using Erp.Core.Services.Dashboard;
using Erp.Core.Services.Home;

namespace Erp.App.Components
{
    public class DashboardUIViewComponent: ViewComponent
    {
        private readonly DashboardService _service;
        private readonly VMLogin _vMLogin;
        private readonly int _UserID;
        public DashboardUIViewComponent(InfrastructureDbContext db, IHttpContextAccessor httpContextAccessor)
        {
            _vMLogin = httpContextAccessor.HttpContext.Session.GetObjectFromJson<VMLogin>("LoginUser");
            _service = new DashboardService(db, httpContextAccessor.HttpContext);
        }
        public IViewComponentResult Invoke()
        {
            var model = _service.GetAllDashboardData();
            
            return View("MerchandisingView",model);
        }
    }
}
