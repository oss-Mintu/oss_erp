﻿using Erp.Core.Services.Home;
using Erp.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Erp.App.Components
{
    public class SettingUIViewComponent : ViewComponent
    {
        private readonly HomeService _service;
        public SettingUIViewComponent(InfrastructureDbContext db, IHttpContextAccessor httpContextAccessor)
        {
            _service = new HomeService(db, httpContextAccessor.HttpContext);
        }

        public IViewComponentResult Invoke(int ID)
        {
            ViewBag.CountryList = new SelectList(_service.CountryDropDownList(), "Value", "Text");
            
            var model = new VMBuyer();
            if (ID != 0)
            {
                model = _service.GetBuyerByID(ID);
            }
            return View(model);
        }
    }
}
