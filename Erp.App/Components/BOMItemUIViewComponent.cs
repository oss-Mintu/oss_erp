﻿using Erp.Core.Services.Merchandising;
using Erp.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Erp.App.Components
{
    public class BOMItemUIViewComponent : ViewComponent
    {
        private readonly MerchandisingService _service;
        public BOMItemUIViewComponent(InfrastructureDbContext db, IHttpContextAccessor httpContextAccessor)
        {
            _service = new MerchandisingService(db, httpContextAccessor.HttpContext);
        }

        public IViewComponentResult Invoke(int ID,int RawCategoryId, int StyleId,int TotalPeice,int TypeID)
        {
            ViewBag.FabricList = new SelectList(_service.DDLBOMFabricByStyleID(StyleId), "Value", "Text");
            ViewBag.SubCategoryList1 = new SelectList(_service.DDLRawCategoryItemByCatID(RawCategoryId), "Value", "Text");
            ViewBag.CurrencyList = new SelectList(_service.CurrencyDropDownList(), "Value", "Text");
            ViewBag.ColorList = new SelectList(_service.ColorDropDownList(), "Value", "Text");
            ViewBag.SizeList = new SelectList(_service.DDLSizeByStyleID(StyleId), "Value", "Text");
            var model = new VMStyleView();
            model.vmBOM = new VMBOM();
            switch (TypeID)
            {
                case 1:
                    if (ID != 0)
                    {
                        model.ActionId = 2;
                        model.vmBOM = _service.BOMItemGetByID(ID);
                    }
                    else
                    {
                        model.vmBOM.Common_RawCategoryFK = RawCategoryId;
                        model.vmBOM.Merchandising_StyleFK = StyleId;
                        model.vmBOM.RawCategory = _service.GetBOMCategoryName(RawCategoryId);
                        model.ActionId = 1;
                    }
                    model.vmBOM.TypeID = TypeID;
                    model.vmBOM.TotalPiece = TotalPeice;
                    return View(model);

                case 2:
                    if (ID != 0)
                    {
                        
                        model.vmBOM = _service.BOMItemGetByID(ID);
                        model.ActionId = 2;
                    }
                    else
                    {
                        model.vmBOM.Common_RawCategoryFK = RawCategoryId;
                        model.vmBOM.Merchandising_StyleFK = StyleId;
                        model.ActionId = 1;
                    }
                    model.vmBOM.TypeID = TypeID;
                    model.vmBOM.TotalPiece = TotalPeice;
                    return View("FabricItemUI", model);
                case 3:
                    if (ID != 0)
                    {
                        model.ActionId = 2;
                        model.vmBOM = _service.BOMItemGetByID(ID);
                    }
                    else
                    {
                        model.vmBOM.Common_RawCategoryFK = RawCategoryId;
                        model.vmBOM.Merchandising_StyleFK = StyleId;
                        model.ActionId = 1;
                    }
                    model.vmBOM.TypeID = TypeID;
                    model.vmBOM.TotalPiece = TotalPeice;
                    model.vmBOM.RequiredQty = 1;
                    return View("ChargeItemUI", model);
                case 4:
                    if (ID != 0)
                    {
                        model.ActionId = 2;
                        model.vmBOM = _service.BOMItemGetByID(ID);
                    }
                    else
                    {
                        model.vmBOM.Common_RawCategoryFK = RawCategoryId;
                        model.vmBOM.Merchandising_StyleFK = StyleId;
                        model.ActionId = 1;
                    }
                    model.vmBOM.TypeID = TypeID;
                    model.vmBOM.TotalPiece = TotalPeice;
                    model.vmBOM.RequiredQty = 1;
                    return View("AccessoriesItemUI", model);
                default:
                    return View(model);
            }
        }
    }
}
