﻿using Erp.Core.Services.Production;
using Erp.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Erp.App.Components
{
    public class TimeActionViewComponent : ViewComponent
    {
        private readonly ProductionService _service;
        private readonly HttpContext httpContext;
        public TimeActionViewComponent(InfrastructureDbContext db, IHttpContextAccessor httpContextAccessor)
        {
            httpContext = httpContextAccessor.HttpContext;
            _service = new ProductionService(db, httpContextAccessor.HttpContext);
        }

        public IViewComponentResult Invoke(int ID, int FID, string ViewName)
        {
            switch (ViewName)
            {
                case "OrderAction":
                    VMOrderAction model = new VMOrderAction();
                    model.BtnLavel = "ADD";
                    if (ID != 0)
                    {
                        model = _service.OrderActionGetByID(ID);
                        model.BtnLavel = "UPDATE";
                    }
                    return View("OrderActionUI", model);

                case "ActionStep":
                    VMOrderActionStep smodel = new VMOrderActionStep();
                    smodel.Plan_OrderActionFK = FID;
                    smodel.BtnLavel = "ADD";
                    if (ID != 0)
                    {
                        smodel = _service.OrderActionStepGetByID(ID);
                        smodel.BtnLavel = "UPDATE";
                    }
                    return View("OrderActionStepUI", smodel);
                default:
                    return View();
            }
        }
    }
}
