﻿using Erp.Core.Services.Merchandising;
using Erp.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Erp.App.Components
{
    public class BOMCategoryUIViewComponent : ViewComponent
    {
        private readonly MerchandisingService _service;
        public BOMCategoryUIViewComponent(InfrastructureDbContext db, IHttpContextAccessor httpContextAccessor)
        {
            _service = new MerchandisingService(db, httpContextAccessor.HttpContext);
        }

        public IViewComponentResult Invoke(int RawCategoryId, int StyleId, int TotalPeice,int TypeID)
        {
            ViewBag.SubCategoryList1 = new SelectList(_service.DDLRawCategoryItemByCatID(RawCategoryId), "Value", "Text");
            ViewBag.CurrencyList = new SelectList(_service.CurrencyDropDownList(), "Value", "Text");
            var model = new VMStyleView();
            model.vmBOM = new VMBOM();
            model.vmBOM = _service.BOMGetByStyleID(StyleId, RawCategoryId);
            model.vmBOM.Common_RawCategoryFK = RawCategoryId;
            model.vmBOM.Merchandising_StyleFK = StyleId;
            model.vmBOM.TotalPiece = TotalPeice;
            model.vmBOM.RawCategory = _service.GetCategory(RawCategoryId);
            model.TypeID = TypeID;
            switch (TypeID)
            {
                case 1: return View(model);
                case 2: return View("BOMFabricUI",model);
                case 3: return View("BOMChargeUI", model);
                case 4: return View("BOMAccessoriesUI", model);
                default: return View();
            }
        }
    }
}
