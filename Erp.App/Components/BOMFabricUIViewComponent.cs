﻿using Erp.Core.Services.Merchandising;
using Erp.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Erp.App.Components
{
    public class BOMFabricUIViewComponent : ViewComponent
    {
        private readonly MerchandisingService _service;
        public BOMFabricUIViewComponent(InfrastructureDbContext db, IHttpContextAccessor httpContextAccessor)
        {
            _service = new MerchandisingService(db,httpContextAccessor.HttpContext);
        }

        public IViewComponentResult Invoke(int ID, int RawCategoryId, int StyleId, int TotalPeice)
        {
            ViewBag.SubCategoryList1 = new SelectList(_service.DDLRawCategoryItemByCatID(RawCategoryId), "Value", "Text");
            var model = new VMStyleView();
            model.vmBOM = new VMBOM();
            model.vmBOM = _service.BOMGetByStyleID(StyleId, RawCategoryId);
            model.vmBOM.Common_RawCategoryFK = RawCategoryId;
            model.vmBOM.Merchandising_StyleFK = StyleId;
            model.vmBOM.TotalPiece = TotalPeice;
            model.vmBOM.RawCategory = _service.GetCategory(RawCategoryId);
            return View(model);
        }
    }
}
