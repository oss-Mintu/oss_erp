﻿using Erp.Core.Services.Production;
using Erp.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Erp.App.Components
{
    public class RemarksViewComponent:ViewComponent
    {
        private readonly ProductionService _service;
        private readonly HttpContext httpContext;
        public RemarksViewComponent(InfrastructureDbContext db, IHttpContextAccessor httpContextAccessor)
        {
            httpContext = httpContextAccessor.HttpContext;
            _service = new ProductionService(db, httpContextAccessor.HttpContext);
        }
        public IViewComponentResult Invoke(int id, string view)
        {
            var model = new VMRemarks();
            if (view.Equals("List"))
            {
                model.DataList = _service.GetRemarks();
                return View("RemarksListView", model);
            }
            else
            {
               
                if (id != 0)
                {
                    model = _service.GetRemarks(id);
                }
                return View("RemarksActionView", model);
            }
        }
    }
}
