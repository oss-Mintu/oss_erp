﻿using Erp.Core.Services.Merchandising;
using Erp.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Erp.App.Components
{
    public class CBSItemUIViewComponent : ViewComponent
    {
        private readonly MerchandisingService _service;
        public CBSItemUIViewComponent(InfrastructureDbContext db, IHttpContextAccessor httpContextAccessor)
        {
            _service = new MerchandisingService(db, httpContextAccessor.HttpContext);
        }

        public IViewComponentResult Invoke(int ID, int RawCategoryId, int CBSId,int TotalPeice,int TypeID)
        {
            ViewBag.SubCategoryList = new SelectList(_service.DDLRawCategoryItemByCatID(RawCategoryId), "Value", "Text");
            ViewBag.CurrencyList = new SelectList(_service.CurrencyDropDownList(), "Value", "Text");
            var model = new VMStyleView();
            model.vmCBSSlave = new VMCBSSlave();
            switch (TypeID)
            {
                case 1:
                    if (ID != 0)
                    {
                        model.ActionId = 2;
                        model.vmCBSSlave = _service.CBSItemGetByID(ID);
                    }
                    else
                    {
                        model.vmCBSSlave.Common_RawCategoryFK = RawCategoryId;
                        model.vmCBSSlave.Merchandising_CBSFK = CBSId;
                        model.ActionId = 1;
                    }
                    model.vmCBSSlave.TotalPieceQty = TotalPeice;
                    model.vmCBSSlave.TypeID = TypeID;
                    return View(model);
                case 2:
                    ViewBag.ColorList = new SelectList(_service.ColorDropDownList(), "Value", "Text");
                    if (ID != 0)
                    {
                        model.ActionId = 2;
                        model.vmCBSSlave = _service.CBSItemGetByID(ID);
                    }
                    else
                    {
                        model.vmCBSSlave.Common_RawCategoryFK = RawCategoryId;
                        model.vmCBSSlave.Merchandising_CBSFK = CBSId;
                        model.ActionId = 1;
                    }
                    model.vmCBSSlave.TotalPieceQty = TotalPeice;
                    model.vmCBSSlave.TypeID = TypeID;
                    return View("CBSFabricItemUI", model);
                case 3:
                    if (ID != 0)
                    {
                        model.ActionId = 2;
                        model.vmCBSSlave = _service.CBSItemGetByID(ID);
                    }
                    else
                    {
                        model.vmCBSSlave.Common_RawCategoryFK = RawCategoryId;
                        model.vmCBSSlave.Merchandising_CBSFK = CBSId;
                        model.ActionId = 1;
                    }
                    model.vmCBSSlave.TotalPieceQty = TotalPeice;
                    model.vmCBSSlave.RequiredQty = 1;
                    model.vmCBSSlave.TypeID = TypeID;
                    return View("CBSChargeItemUI", model);
                default:return View();
            }
        }
    }
}
