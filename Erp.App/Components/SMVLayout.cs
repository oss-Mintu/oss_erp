﻿using Erp.Core.Services;
using Erp.Core.Services.Merchandising;
using Erp.Core.Services.Production;
using Erp.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Erp.App.Components
{
    public class SMVLayout : ViewComponent
    {
        private readonly ProductionService _service;
        private readonly HttpContext httpContext;
        public SMVLayout(InfrastructureDbContext db, IHttpContextAccessor httpContextAccessor)
        {
            httpContext = httpContextAccessor.HttpContext;
            _service = new ProductionService(db, httpContextAccessor.HttpContext);
        }

        public IViewComponentResult Invoke(int ID, int StyleId, int TypeID)
        {
            var model = new VMPlanView();
            model.vmSMVLayout = new VMSMVLayout();
            model.vmSMVLayout.ID = ID;
            switch (TypeID)
            {
                case 1:
                    if (model.vmSMVLayout.ID > 0)
                    {
                        model.vmSMVLayout= _service.GetPreSMVByID(model.vmSMVLayout.ID);
                        ViewBag.SubSetList = new SelectList(_service.DDLSubSetListByStyleID(model.vmSMVLayout.Merchandising_StyleFk), "Value", "Text");
                        
                    }
                    else
                    {
                        ViewBag.SubSetList = new SelectList(_service.DDLSubSetListByStyleID(StyleId), "Value", "Text");
                        model.vmSMVLayout.Merchandising_StyleFk = StyleId;
                        model.vmSMVLayout.ActionId = (int)ActionEnum.Add;
                    }
                    return View(model);
                case 2:
                    ViewBag.StyleList = new SelectList(_service.DDLStyle(), "Value", "Text");
                    if (model.vmSMVLayout.ID > 0)
                    {
                        model.vmSMVLayout = _service.GetMasterSMVByID(model.vmSMVLayout.ID);
                        ViewBag.SubSetList = new SelectList(_service.DDLSubSetListByStyleID(model.vmSMVLayout.Merchandising_StyleFk), "Value", "Text");
                    }
                    return View("MasterSMV", model);
                default: return View();
            }
        }
    }
}
