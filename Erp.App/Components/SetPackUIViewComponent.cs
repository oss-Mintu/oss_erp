﻿using Erp.Core.Services.Merchandising;
using Erp.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Erp.App.Components
{
    public class SetPackUIViewComponent : ViewComponent
    {
        private readonly MerchandisingService _service;
        public SetPackUIViewComponent(InfrastructureDbContext db, IHttpContextAccessor httpContextAccessor)
        {
            _service = new MerchandisingService(db, httpContextAccessor.HttpContext);
        }

        public IViewComponentResult Invoke(int ID,int StyleId)
        {
            ViewBag.ColorList = new SelectList(_service.ColorDropDownList(), "Value", "Text");
            var model = new VMStyleView();
            model.vmStyleSetPack = new VMStyleSetPack();
            if (ID != 0)
            {
                model.vmStyleSetPack = _service.StyleSetPackByID(ID);
                model.ActionId = 2;
            }
            else
            {
                model.vmStyleSetPack.Merchandising_StyleFK = StyleId;
                if (_service.IsAllow(StyleId))
                {
                    model.vmStyleSetPack.Quantity = 1;
                    model.vmStyleSetPack.Merchandising_StyleFK = StyleId;
                }
                else { model.ActionId = 8; }
            }
            return View(model);
        }
    }
}
