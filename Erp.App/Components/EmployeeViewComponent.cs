﻿using Erp.Core.Services.HRMS;
using Erp.Core.Services.Merchandising;
using Erp.Infrastructure;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Erp.App.Components
{
    public class EmployeeViewComponent : ViewComponent
    {
        private readonly HRMSService _service;
        public EmployeeViewComponent(InfrastructureDbContext db)
        {
            _service = new HRMSService(db);
        }

        public async Task<IViewComponentResult> InvokeAsync(int ID, int EmployeeID, string Name)
        {


            switch (Name)
            {
                case "Spouse":
                    var Spousemodel = new VMEmployeeSpouse();

                    if (ID != 0)
                    {
                        Spousemodel = await _service.EmployeeSpouseSpecificGet(new VMEmployeeSpouse() { ID = ID, VMEmployeeId = EmployeeID });
                        Spousemodel.ActionId = 2;
                    }
                    else
                    {
                        Spousemodel.ActionId = 1;
                        Spousemodel.VMEmployeeId = EmployeeID;
                    }

                    return View("Spouse", Spousemodel);
                case "Children":
                    var Childrenmodel = new VMEmployeeChildren();

                    if (ID != 0)
                    {
                        Childrenmodel = await _service.EmployeeChildrenSpecificGet(new VMEmployeeChildren() { ID = ID, VMEmployeeId = EmployeeID });
                        Childrenmodel.ActionId = 2;
                    }
                    else
                    {
                        Childrenmodel.ActionId = 1;
                        Childrenmodel.VMEmployeeId = EmployeeID;
                    }

                    return View("Children", Childrenmodel);
                case "Education":
                    var Educationmodel = new VMEmployeeEducation();
                    Educationmodel.VMDegreeList = await _service.DegreeDropDownListAsync();
                    if (ID != 0)
                    {
                        Educationmodel = await _service.EmployeeEducationSpecificGet(new VMEmployeeEducation() { ID = ID, VMEmployeeId = EmployeeID });
                        Educationmodel.ActionId = 2;
                        Educationmodel.VMDegreeList = await _service.DegreeDropDownListAsync();
                    }
                    else
                    {
                        Educationmodel.ActionId = 1;
                        Educationmodel.VMEmployeeId = EmployeeID;
                        Educationmodel.VMDegreeList = await _service.DegreeDropDownListAsync();
                    }

                    return View("Education", Educationmodel);
                case "Training":
                    var Trainingmodel = new VMEmployeeTraining();

                    Trainingmodel = await _service.EmployeeTrainingSpecificGet(new VMEmployeeTraining() { ID = ID, VMEmployeeId = EmployeeID });


                    if (ID > 0)
                    {
                        Trainingmodel.ActionId = 2;
                    }
                    else
                    {
                        Trainingmodel.VMEmployeeId = EmployeeID;
                    }

                    return View("Training", Trainingmodel);
                case "Experience":
                    var Experiencemodel = new VMEmployeeExperience();

                    if (ID != 0)
                    {
                        Experiencemodel = await _service.EmployeeExperienceSpecificGet(new VMEmployeeExperience() { ID = ID, VMEmployeeId = EmployeeID });
                        Experiencemodel.ActionId = 2;
                    }
                    else
                    {
                        Experiencemodel.ActionId = 1;
                        Experiencemodel.VMEmployeeId = EmployeeID;
                    }

                    return View("Experience", Experiencemodel);
                case "Skill":
                    var Skillmodel = new VMEmployeeSkill();

                    if (ID != 0)
                    {
                        Skillmodel = await _service.EmployeeSkillSpecificGet(new VMEmployeeSkill() { ID = ID, VMEmployeeId = EmployeeID });
                        Skillmodel.ActionId = 2;
                    }
                    else
                    {
                        Skillmodel.ActionId = 1;
                        Skillmodel.VMEmployeeId = EmployeeID;
                    }

                    return View("Skill", Skillmodel);
                case "Reference":
                    var Referencemodel = new VMEmployeeReference();

                    if (ID != 0)
                    {
                        Referencemodel = await _service.EmployeeReferenceSpecificGet(new VMEmployeeReference() { ID = ID, VMEmployeeId = EmployeeID });
                        Referencemodel.ActionId = 2;
                    }
                    else
                    {
                        Referencemodel.ActionId = 1;
                        Referencemodel.VMEmployeeId = EmployeeID;
                    }

                    return View("Reference", Referencemodel);
                case "Nominee":
                    var Nomineemodel = new VMEmployeeNominee();

                    if (ID != 0)
                    {
                        Nomineemodel = await _service.EmployeeNomineeSpecificGet(new VMEmployeeNominee() { ID = ID, VMEmployeeId = EmployeeID });
                        Nomineemodel.ActionId = 2;
                    }
                    else
                    {
                        Nomineemodel.ActionId = 1;
                        Nomineemodel.VMEmployeeId = EmployeeID;
                    }

                    return View("Nominee", Nomineemodel);
                default:
                    return View();
            }

        }
    }
}
