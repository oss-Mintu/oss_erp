﻿using Erp.Core.Services;
using Erp.Core.Services.Merchandising;
using Erp.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;

namespace Erp.App.Components
{
    public class CommonUIViewComponent : ViewComponent
    {
        private readonly MerchandisingService _service;
        public CommonUIViewComponent(InfrastructureDbContext db, IHttpContextAccessor httpContextAccessor)
        {
            _service = new MerchandisingService(db, httpContextAccessor.HttpContext);
        }

        public IViewComponentResult Invoke(int id,int oid, string name)
        {
            if (name.Equals("Order"))
            {
                ViewBag.AgentList = new SelectList(_service.DDLAgentList(), "Value", "Text");
                ViewBag.AgentInspectionList = new SelectList(_service.DDLAgentInspection(), "Value", "Text");
                ViewBag.BuyerList = new SelectList(_service.BuyerDropDownList(), "Value", "Text");
                var model = new VMOrder();

                if (id != 0)
                {
                    model = _service.OrderById(id);
                }
                else
                {
                    model.ActionId = (int)ActionEnum.Add;
                    model.OrderDate = DateTime.Today;
                }

                return View("OrderFrom",model);
            }
            else if (name.Equals("Style"))
            {
                ViewBag.CurrencyList = new SelectList(_service.CurrencyDropDownList(), "Value", "Text");
                ViewBag.UnitList = new SelectList(_service.UnitDropDownList(), "Value", "Text");
                
                var model = new VMOrderView();
                model.vmStyle = new VMStyle();
                if (id != 0)
                {
                    model.vmStyle = _service.StyleById(id);
                    model.ActionId = (int)ActionEnum.Edit;
                    ViewBag.OrderSubCategoryList = new SelectList(_service.OrderSubCategoryByItemIDDropDownList(model.vmStyle.Mkt_ItemFK), "Value", "Text");
                    ViewBag.OrderItemList = new SelectList(_service.OrderItemByItemIDDropDownList(model.vmStyle.Mkt_ItemFK), "Value", "Text");
                }
                else
                {
                    ViewBag.OrderCategoryList = new SelectList(_service.OrderCategoryDropDownList(), "Value", "Text");
                    ViewBag.OrderSubCategoryList = new SelectList(_service.OrderSubCategoryDropDownList(), "Value", "Text");
                    ViewBag.OrderItemList = new SelectList(_service.OrderItemDropDownList(), "Value", "Text");

                    MultiSelectList lstColor = new MultiSelectList(_service.ColorDropDownList(), "Value", "Text");
                    MultiSelectList lstSize = new MultiSelectList(_service.SizeDropDownList(), "Value", "Text");
                    
                    model.ActionId = (int)ActionEnum.Add;
                    model.vmStyle.Merchandising_BuyerOrderFK = oid;
                    model.vmStyle.SizeList = lstSize;
                    model.vmStyle.ColorList = lstColor;
                }
                return View("StyleFrom",model);
            }
            else
            {
                return View();
            }
        }
    }
}
