﻿using Erp.Core.Services.Payroll;
using Erp.Infrastructure;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Erp.App.Components
{
    public class EmployeeSalaryViewComponent : ViewComponent
    {
        private readonly PayrollService _service;
        public EmployeeSalaryViewComponent(InfrastructureDbContext db)
        {
            _service = new PayrollService(db);
        } 
        public IViewComponentResult Invoke(int bunit, int unit, int dept, int section)
        {
            var model = new VMEmployeeSalary();
            model = _service.FilterEmployeeSalary(bunit, unit, dept, section);
            return View("EmployeeSalary",model);
        }
    }
}
