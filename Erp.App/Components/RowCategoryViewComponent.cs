﻿using Erp.Core.Services.Merchandising;
using Erp.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Erp.App.Components
{
    public class RowCategoryViewComponent : ViewComponent
    {
        private readonly MerchandisingService _service;
        public RowCategoryViewComponent(InfrastructureDbContext db, IHttpContextAccessor httpContextAccessor)
        {
            _service = new MerchandisingService(db, httpContextAccessor.HttpContext);
        }

        public IViewComponentResult Invoke(int RawCategoryId,int CBSId,int TotalPeice,int TypeID)
        {
            //ViewBag.SubCategoryList = new SelectList(_service.RawSubCatByCategoryIDDropDownList(RawCategoryId), "Value", "Text");
            ViewBag.SubCategoryList= new SelectList(_service.DDLRawCategoryItemByCatID(RawCategoryId), "Value", "Text");
            
            ViewBag.CurrencyList = new SelectList(_service.CurrencyDropDownList(), "Value", "Text");
            var model = new VMStyleView();
            model.vmCBSSlave = _service.CBSSlaveGetByCBSID(CBSId,RawCategoryId);
            model.vmCBSSlave.Common_RawCategoryFK = RawCategoryId;
            model.vmCBSSlave.Merchandising_CBSFK = CBSId;
            model.vmCBSSlave.RawCategory = _service.GetCategory(RawCategoryId);
            model.vmCBSSlave.TotalPieceQty = TotalPeice;
            
            switch (TypeID)
            {
                case 1: return View(model);
                case 2: return View("CBSFabricUI",model);
                case 3: return View("CBSChargeUI", model);
                default: return View();
            }
        }
    }
}
