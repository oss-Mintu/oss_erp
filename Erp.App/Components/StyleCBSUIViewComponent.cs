﻿using Erp.Core.Services.Merchandising;
using Erp.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Erp.App.Components
{
    public class StyleCBSUIViewComponent : ViewComponent
    {
        private readonly MerchandisingService _service;
        public StyleCBSUIViewComponent(InfrastructureDbContext db, IHttpContextAccessor httpContextAccessor)
        {
            _service = new MerchandisingService(db, httpContextAccessor.HttpContext);
        }

        public IViewComponentResult Invoke(int ID)
        {
            ViewBag.OrderCategoryList = new SelectList(_service.OrderCategoryDropDownList(), "Value", "Text");
            ViewBag.OrderSubCategoryList = new SelectList(_service.OrderSubCategoryDropDownList(), "Value", "Text");
            ViewBag.OrderItemList = new SelectList(_service.OrderItemDropDownList(), "Value", "Text");
            var model = new VMStyle();
            if (ID != 0)
            {
                model = _service.CBSStyleGetById(ID);
                //ViewBag.OrderSubCategoryList = new SelectList(_service.OrderSubCategoryByItemIDDropDownList(model.Mkt_ItemFK), "Value", "Text");
                //ViewBag.OrderItemList = new SelectList(_service.OrderItemByItemIDDropDownList(model.Mkt_ItemFK), "Value", "Text");
            }
            return View(model);
        }
    }
}
