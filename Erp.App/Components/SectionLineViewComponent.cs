﻿using Erp.Core.Services.Production;
using Erp.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Erp.App.Components
{
    public class SectionLine:ViewComponent
    {
        private readonly ProductionService _service;
        private readonly HttpContext httpContext;
        public SectionLine(InfrastructureDbContext db, IHttpContextAccessor httpContextAccessor)
        {
            httpContext = httpContextAccessor.HttpContext;
            _service = new ProductionService(db, httpContextAccessor.HttpContext);
        }
        
        public IViewComponentResult Invoke(int ID,string view)
        {
            var model = new VMSectionLine();
            if (view.Equals("List"))
            {
                 model.Datalist = _service.SectionLineGet();
                return View("SectionLineList", model);
            }
            else 
            {
                ViewBag.Floor = new SelectList(_service.DDLProductionFloor(), "Value", "Text");
          
                if (ID != 0)
                {
                    model = _service.SectionLineGetByID(ID);
                }
                return View("SectionLineView",model);
            }
        }
    }
}