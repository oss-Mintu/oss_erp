﻿using Erp.Core.Services.Merchandising;
using Erp.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Erp.App.Components
{
    public class BOMFabricItemUIViewComponent : ViewComponent
    {
        private readonly MerchandisingService _service;
        public BOMFabricItemUIViewComponent(InfrastructureDbContext db, IHttpContextAccessor httpContextAccessor)
        {
            _service = new MerchandisingService(db, httpContextAccessor.HttpContext);
        }

        public IViewComponentResult Invoke(int ID, int RawCategoryId, int StyleId, int TotalPeice)
        {
            ViewBag.SubCategoryList1 = new SelectList(_service.DDLRawCategoryItemByCatID(RawCategoryId), "Value", "Text");
            ViewBag.ColorList = new SelectList(_service.ColorDropDownList(), "Value", "Text");
            ViewBag.CurrencyList = new SelectList(_service.CurrencyDropDownList(), "Value", "Text");
            var model = new VMStyleView();
            model.vmBOM = new VMBOM();
            if (ID != 0)
            {
                model.ActionId = 2;
                model.vmBOM = _service.BOMItemGetByID(ID);
                model.vmBOM.TotalPiece = TotalPeice;
            }
            else
            {
                model.vmBOM.Common_RawCategoryFK = RawCategoryId;
                model.vmBOM.Merchandising_StyleFK = StyleId;
                model.vmBOM.TotalPiece = TotalPeice;
                model.ActionId = 1;
            }
            return View(model);
        }
    }
}
