﻿using Erp.Core.Services.Merchandising;
using Erp.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Erp.App.Components
{
    public class CBSUIViewComponent : ViewComponent
    {
        private readonly MerchandisingService _service;
        public CBSUIViewComponent(InfrastructureDbContext db, IHttpContextAccessor httpContextAccessor)
        {
            _service = new MerchandisingService(db, httpContextAccessor.HttpContext);
        }

        public IViewComponentResult Invoke(int ID)
        {
            ViewBag.StyleList = new SelectList(_service.CBSStyleDownList(), "Value", "Text");
            ViewBag.BuyerList = new SelectList(_service.BuyerDropDownList(), "Value", "Text");
            var model = new VMCBS();
            if (ID != 0)
            {
                model = _service.CBSGetByID(ID);
            }
            return View(model);
        }
    }
}
