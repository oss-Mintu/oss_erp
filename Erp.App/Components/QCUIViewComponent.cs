﻿using Erp.Core.Services.Production;
using Erp.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Erp.App.Components
{
    public class QCUIViewComponent : ViewComponent
    {
        private readonly ProductionService _service;
        public QCUIViewComponent(InfrastructureDbContext db, IHttpContextAccessor httpContextAccessor)
        {
            _service = new ProductionService(db, httpContextAccessor.HttpContext);
        }

        public IViewComponentResult Invoke(int ID,int UINo,int StyleID=0)
        {
            switch (UINo)
            {
                case 1:
                    ViewBag.LineList = new SelectList(_service.DDLDeviceLine(), "Value", "Text");
                    var model = new VMLineDevice();
                    if (ID != 0)
                    {
                        model = _service.LineDeviceGetByID(ID);
                    }
                    return View(model);
                case 2:
                    var qcModel = new VMQCType();
                    if (ID != 0)
                    {
                        qcModel = _service.QCTypeGetByID(ID);
                    }
                    return View("QCType",qcModel);
                case 3:
                    ViewBag.QCTypeList = new SelectList(_service.DDLQCType(), "Value", "Text");
                    var oQCModel = new VMOrderQC();
                    oQCModel.Merchandising_StyleFK = StyleID;
                    if (ID != 0)
                    {
                        oQCModel = _service.OrderQCGetByID(ID);
                    }
                    return View("QCOrderUI", oQCModel);
                default: return View();
            }
            
        }
    }
}
