$(function () {
    $('.js-basic-example').DataTable({
        responsive: true,
        "order": [[0, "desc"]]
    });

    //Exportable table
    $('.js-exportable').DataTable({
        dom: 'Bfrtip',
        "order": [[0, "desc"]],
        responsive: true,
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
});