﻿// Write your Javascript code.
var url = window.location;
$('ul.sidebar-menu a').filter(function () {
    return this.href == url;
}).parent().siblings()
    .removeClass('active').end()
    .addClass('active');
$('ul.treeview-menu a').filter(function () {
    return this.href == url;
}).parentsUntil(".sidebar-menu > .treeview-menu")
    .siblings().removeClass('active menu-open')
    .end().addClass('active menu-open');