﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Erp.App.Models;
using Erp.Infrastructure;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Erp.Core.Services.Store;
using Microsoft.AspNetCore.Mvc.Rendering;
using Erp.Core.Services;
using Erp.Core.Services.User;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Http;
using Erp.Core.Services.Merchandising;
using Erp.Core.Services.Procurement;

namespace Erp.App.Controllers
{
    [SoftwareAdmin]
    public class StoreController : Controller
    {
        private readonly ILogger _logger;
        private readonly StoreService _service;

        public StoreController(InfrastructureDbContext db, IHttpContextAccessor httpContextAccessor, ILogger<StoreController> logger, IConfiguration iconfiguration)
        {
            _logger = logger;
            _service = new StoreService(db, httpContextAccessor.HttpContext, iconfiguration);
        }
        public async Task<IActionResult> StoreBin()
        {
            var adStoreBin = new VmStoreBin();
            var data = HttpContext.Session.GetObjectFromJson<List<VmStoreBin>>("Bin");
            adStoreBin.DataList = data;
            if (data != null && data.Count > 0)
            {
                return View(adStoreBin);
            }
            ViewBag.StoreList = new SelectList(await _service.StoreDropDownListAsync(), "Value", "Text");
            return View(await _service.GetStoreBin());
        }
        public async Task<IActionResult> BinAddorEdit()
        {
            ViewBag.UnitList = new SelectList(await _service.UnitDropDownListAsync(), "Value", "Text");
            ViewBag.StoreList = new SelectList(await _service.StoreDropDownListAsync(), "Value", "Text");
            ViewBag.SectionList = new SelectList(_service.SectionDropDownList(), "Value", "Text");
            return View();
        }

        public async Task<IActionResult> BinAddorEditById(int id)
        {
            var data = HttpContext.Session.GetObjectFromJson<List<VmStoreBin>>("Bin");
            var adStoreBin = new VmStoreBin { DataList = new List<VmStoreBin>() };
            if (data == null) return RedirectToAction(nameof(BinAddorEdit), adStoreBin);// View(adStoreBin);
            var selected = data.Where(x => x.ID == id).Select(x => x).AsEnumerable();
            adStoreBin.DataList = selected;
            ViewBag.UnitList = new SelectList(await _service.UnitDropDownListAsync(), "Value", "Text");
            ViewBag.StoreList = new SelectList(await _service.StoreDropDownListAsync(), "Value", "Text");
            return RedirectToAction(nameof(BinAddorEdit), adStoreBin);
        }
        [HttpPost]
        public async Task<IActionResult> BinAddorEdit(VmStoreBin storeBin)
        {
            if (_service.HasThisItem(storeBin)) return RedirectToAction(nameof(BinAddorEdit));
            var data = GetTotalBin(storeBin);
            return RedirectToAction(nameof(StoreBin));
        }

        [HttpPost]
        public async Task<IActionResult> AddBin()
        {
            var data = HttpContext.Session.GetObjectFromJson<List<VmStoreBin>>("Bin");
            var result = await _service.SeveAll(data);
            var vmStore = new List<VmStoreBin>();
            HttpContext.Session.SetObjectAsJson("Bin", vmStore);
            return RedirectToAction(nameof(StoreBin));
        }

        [HttpPost]
        public IActionResult CloseBin()
        {
            HttpContext.Session.SetObjectAsJson("Bin", new VmStoreBin());
            return RedirectToAction(nameof(StoreBin));
        }
        private object GetTotalBin(VmStoreBin storeBin)
        {
            var storeBins = new List<VmStoreBin>();
            var k = 1;
            if (storeBin.TotalRow > 0)
            {
                for (var i = 0; i < storeBin.TotalRow; i++)
                {
                    if (storeBin.TotalShelf > 0)
                    {
                        for (var j = 1; j <= storeBin.TotalShelf; j++)
                        {
                            var bin = Bin(storeBin, i, j);
                            var store = new VmStoreBin
                            {
                                ID = k++,
                                CID = bin,
                                Section = storeBin.Section,
                                Rack = storeBin.Rack,
                                Row = storeBin.Row,
                                Shelf = storeBin.Shelf,
                                TotalRow = storeBin.TotalRow,
                                TotalShelf = storeBin.TotalShelf,
                                Dimension = storeBin.Dimension,
                                Capacity = storeBin.Capacity,
                                Remarks = storeBin.Remarks,
                                IsActive = true,//storeBin.IsActive,
                                StoreGeneralFK = storeBin.StoreGeneralFK,
                                StoreGeneralName = storeBin.StoreGeneralName,
                                CommonUnitFK = storeBin.CommonUnitFK,
                                CommonUnitName = storeBin.CommonUnitName
                            };
                            storeBins.Add(store);
                        }
                    }
                }
            }
            HttpContext.Session.SetObjectAsJson("Bin", storeBins);
            return storeBins;
        }

        private static string Bin(VmStoreBin storeBin, int i, int j)
        {
            var letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".ToCharArray();
            if (i <= 25)
            {
                return "Bin/" + letters[i] + "-" + j + "/Rack" + storeBin.Rack + "/Line" + storeBin.Row + "/" + storeBin.Section + "/" + storeBin.StoreGeneralName.Trim();
            }
            return "Bin/" + letters[i - 26] + letters[i - 26] + "-" + j + "/Rack" + storeBin.Rack + "/Line" + storeBin.Row + "/" + storeBin.Section + "/" + storeBin.StoreGeneralName.Trim();
        }

        public async Task<IActionResult> StoreIn(int id = 0)
        {
            //var model = await _service.GetStoreIn(id);
            VmStoreIn model;
            if (id == 0)
            {
                model = await _service.GetStoreIn(id);
            }
            else
            {
                model = await _service.GetStoreInCutting(id);
            }
            return View(model);
        }
        public async Task<IActionResult> StoreInAdd(VmStoreIn vmStoreIn, int id)
        {
            ViewBag.BinList = new SelectList(_service.BinDropDownList(), "Value", "Text");
            var model = new VmStoreIn();
            if (id == 0)
            {
                model = await _service.GetItemByPoId(vmStoreIn, id);
            }
            else
            {
                model = await _service.GetItemByPoId(vmStoreIn, id);
            }
            return View(model);
        }
        public async Task<IActionResult> StoreInInternal(VmStoreIn vmStoreIn, int id = 0)
        {
            ViewBag.StoreList = new SelectList(await _service.StoreDropDownListAsync(), "Value", "Text");
            ViewBag.BinList = new SelectList(_service.BinDropDownList(), "Value", "Text");
            ViewBag.Datetoday = DateTime.Now.ToString("MM/dd/yyyy");
            var model = new VmStoreIn();
            if (id == 0)
            {
                model = await _service.GetItemByInternalChallanId(vmStoreIn, id);
            }
            else
            {
                model = await _service.GetItemByInternalChallanId(vmStoreIn, id);
            }
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> SaveAllItem(VmStoreIn vmStoreIn, int id)
        {
            vmStoreIn.CommonStoreFK = id == 0 ? 2 : id;
            var data = await _service.SaveAllItem(vmStoreIn);
            return RedirectToAction(nameof(StoreIn), new { id = id });
        }
        [HttpGet]
        public async Task<IActionResult> AddBinByItem(int id = 0, int binId = 0, string remarks = "", double qty = 0)
        {
            var data = await _service.AddBinByItem(id, binId, remarks, qty);
            return Ok(StatusCode(200));
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            return View(nameof(BinAddorEdit));
        }
        [HttpPost, ActionName("Delete")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            return RedirectToAction(nameof(BinAddorEdit));
        }

        public IActionResult StoreAddorUpdate()
        {
            var model = Task.Run(() => _service.GetStoreAll()).Result;
            //ViewBag.DepartmentList = new SelectList(_service.DepartmentDropDownList(), "Value", "Text");
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> StoreAddorUpdate(VmStoreGeneral model)
        {
            #region session values
            string sUser = HttpContext.Session.GetString("User");
            string nUserId = HttpContext.Session.GetString("UserID");
            model.User = sUser;
            #endregion

            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.StoreAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.StoreEdit(model);
            }
            else if (model.ActionId == 3)
            {
                await _service.StoreDelete(model);
            }
            return RedirectToAction(nameof(StoreAddorUpdate));
        }
        public async Task<IActionResult> StockIn()
        {
            ViewBag.POList = new SelectList(await _service.SupplierPoDropDownListAsync(), "Value", "Text");
            ViewBag.BinList = new SelectList(_service.BinDropDownList(), "Value", "Text");
            //model.SupplierPoList = await _service.SupplierPoDropDownListAsync();
            //return  await Task.Run(() => View());
            return View();
        }
        public async Task<IActionResult> StockTransfer()
        {
            ViewBag.RequisitionList = new SelectList(_service.RequisitionDropDownList(), "Value", "Text");
            //ViewBag.StoreTransferList = new SelectList(_service.StoreTransferDropDownList(), "Value", "Text");
            ViewBag.BinList = new SelectList(_service.BinDropDownList(), "Value", "Text");
            return await Task.Run(() => View());
        }

        public int GetSupplierIdByChallanId(int id)
        {
            var data = _service.GetItemByChallanId(id);
            return data;
        }
        public int GetSupplierIdBySupplierChallanID(int id)
        {
            var data = _service.GetSupplierIdBySupplierChallanID(id);
            return data;
        }

        public int GetBrandModelByItemId(int id)
        {
            var data = _service.GetBrandModelByItemId(id);
            return data;
        }

        public string GetRegisterItemId(int id)
        {
            //var data = _service.GetRegisterItemId(id);
            //var adata = JsonConvert.SerializeObject(data);
            //string jsonData = "1_1_1_1"; 
            return _service.GetRegisterItemId(id);
            // return Json(_service.GetRegisterItemId(id));           
        }

        #region StoreOut
        public async Task<IActionResult> StoreOut(int id = 0, int id2 = 0)
        {
            var model = new VmStoreOut();
            int nId2 = id2 == 0 ? 2 : id2;
            model = await _service.GetStoreOutByStoreId(nId2);
            model.CommonStoreFK = id2;
            return View(model);
        }
        public async Task<IActionResult> StoreOutAdd(VmStoreOut vmModel, int id2 = 0)
        {
            #region session values
            string sUser = HttpContext.Session.GetString("User");
            string nUserId = HttpContext.Session.GetString("UserID");
            string ndeptId = HttpContext.Session.GetString("UserDepartmentId");
            #endregion

            vmModel.User = sUser;
            vmModel.DepartmentId = ndeptId;
            vmModel.CommonStoreFK = id2;
            var model = await _service.GetItemBySrId(vmModel);
            model.CommonStoreFK = id2;
            return View(model);
        }
        public async Task<IActionResult> StoreOutSaleReturn(VmStoreOut vmStoreIn, int id)
        {

            ViewBag.StoreList = new SelectList(await _service.StoreDropDownListAsync(), "Value", "Text");
            ViewBag.ChallanList = new SelectList(_service.ChallanDropDownList(), "Value", "Text");
            var model = new VmStoreOut();
            if (id == 0)
            {
                model = await _service.StoreOutSaleReturn(vmStoreIn, id);
            }
            else
            {
                model = await _service.StoreOutSaleReturnByStoreId(vmStoreIn, id);
            }

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> SaveAllItemStoreOut(VmStoreOut vmModel, int id2)
        {
            #region session values
            string sUser = HttpContext.Session.GetString("User");
            string nUserId = HttpContext.Session.GetString("UserID");
            string ndeptId = HttpContext.Session.GetString("UserDepartmentId");
            #endregion

            vmModel.User = sUser;
            vmModel.DepartmentId = ndeptId;
            vmModel.Store_GeneralOutFK = id2 == 0 ? 2 : id2; 

            var data = await _service.SaveAllItemStoreOut(vmModel);
            return RedirectToAction(nameof(StoreOut), new { id = 0, id2 = id2 });
        }
        public string GetStoreIdsByChallanId(int id)
        {
            var data = _service.GetStoreIdsByChallanId(id);
            return data;
        }
        #endregion

        #region StoreSection 

        public async Task<IActionResult> SectionAddorUpdate()
        {
            ViewBag.StoreList = new SelectList(await _service.StoreDropDownListAsync(), "Value", "Text");
            return View(await _service.GetStoreSection());
        }

        [HttpPost]
        public async Task<IActionResult> SectionAddorUpdate(VmStoreSection model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.SectionAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.SectionEdit(model);
            }
            else if (model.ActionId == 3)
            {
                await _service.SectionDelete(model);
            }
            return RedirectToAction(nameof(SectionAddorUpdate));

        }
        #endregion
        #region fahim
        #region Common
        public JsonResult DDLSubSetListByStyleID(int id)
        {
            return Json(new SelectList(_service.DDLSubSetListByStyleID(id), "Value", "Text"));
        }

        public JsonResult OrderRawSubCategoryGet(int id)
        {
            return Json(new SelectList(_service.OrderRawSubCategoryGet(id), "Value", "Text"));
        }
        public JsonResult OrderRawItemGet(int id)
        {
            return Json(new SelectList(_service.OrderRawItemGet(id), "Value", "Text"));
        }
        public async Task<IActionResult> RawItemInfoGet(int id)
        {
            if (id < 0) { return RedirectToAction("Error", "Home"); }
            var model = await Task.Run(() => _service.RawItemInfoGet(id));
            return Json(model);
        }
        #endregion
        #region Store Requisition

        public async Task<IActionResult> CreateSR(int id = 0)
        {
            VMStoreRequisitionSlave model = new VMStoreRequisitionSlave();
            if (id == 0)
            {
                model = new VMStoreRequisitionSlave()
                {
                    Status = SRStatusEnum.Draft,
                    RawCategoryList = new SelectList(_service.RawCategoryDropDownList(), "Value", "Text"),

                    EmployeeList = new SelectList(_service.EmployeeDropDownList(), "Value", "Text"),
                    StoreFromList = await _service.StoreDropDownListAsync(),
                    StoreToList = await _service.StoreDropDownListAsync(),
                    DataListSlave = new List<VMStoreRequisitionSlave>()
                };
            }
            else if (id > 0)
            {
                model = await _service.SRWithSlaveListGet(new VMStoreRequisitionSlave() { Store_RequisitionFK = id });

            }
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> CreateSR(VMStoreRequisitionSlave model)
        {
            #region session values
            string sUser = HttpContext.Session.GetString("User");
            string nUserId = HttpContext.Session.GetString("UserID");
            string ndeptId = HttpContext.Session.GetString("UserDepartmentId");
            #endregion

            model.User = sUser;
            model.Status = SRStatusEnum.Draft;
            model.DepartmentId = ndeptId;
            if (model.ActionEum == ActionEnum.Add)
            {
                if (model.Store_RequisitionFK <= 0)
                {
                    model.Store_RequisitionFK = await _service.StoreRequisitionAdd(
                        new VMStoreRequisition()
                        {
                            User = sUser,
                            Status = SRStatusEnum.Draft,
                            DepartmentId = ndeptId
                        });
                }

                //Add 
                await _service.SRSlaveAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.SRSlaveEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.SRSlaveDelete(model.ID);
            }
            else if ((int)model.ActionEum > 3 && model.Store_RequisitionFK > 0)
            {
                //Delete
                await _service.StoreRequisitionStatusUpdate(new VMStoreRequisition()
                {
                    User = sUser,
                    ID = model.Store_RequisitionFK,
                    ActionId = (int)model.ActionEum,
                    SRActionId = model.SRActionId,
                    Description = model.Description,
                    VMStoreFromFK = model.VMStoreFromFK,
                    VMStoreToFK = model.VMStoreToFK
                });
            }
            else
            {
                return RedirectToAction("Error", "Home");
            }
            return RedirectToAction("CreateSR", new { id = model.Store_RequisitionFK });
        }

        public async Task<IActionResult> StoreRequisition(int id = 0, int id2 = 0)
        {
            if (id < 0) { return RedirectToAction("Error", "Home"); }
            var model = await Task.Run(() => _service.StoreRequisitionGet(new VMStoreRequisition()
            {
                Status = (SRStatusEnum)id,
                CommonStoreFK = id2
            }));
            model.CommonStoreFK = id2;
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> StoreRequisition(VMStoreRequisition model)
        {
            #region session values
            string sUser = HttpContext.Session.GetString("User");
            string nUserId = HttpContext.Session.GetString("UserID");
            string ndeptId = HttpContext.Session.GetString("UserDepartmentId");

            #endregion

            model.User = sUser;
            model.Status = SRStatusEnum.Draft;
            model.DepartmentId = ndeptId;
            if (model.ActionEum == ActionEnum.Add)
            {
                await _service.StoreRequisitionAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                await _service.StoreRequisitionEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                await _service.StoreRequisitionDelete(model.ID);
            }
            else if (model.ActionId > 3)
            {
                await _service.StoreRequisitionStatusUpdate(model);
            }
            else
            {
                return RedirectToAction("Error", "Home");
            }
            return RedirectToAction("StoreRequisition");
        }
        public async Task<IActionResult> StoreRequisitionDetails(int id = 0)
        {
            if (id <= 0) { RedirectToAction("Error", "Home"); }
            //get SR & detail info from Database
            var model = await Task.Run(() => _service.SRWithSlaveListGet(new VMStoreRequisitionSlave() { Store_RequisitionFK = id }));



            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> StoreRequisitionDetails(VMStoreRequisitionSlave model)
        {
            #region session values
            string sUser = HttpContext.Session.GetString("User");
            string nUserId = HttpContext.Session.GetString("UserID");
            #endregion

            model.User = sUser;
            model.Status = SRStatusEnum.Draft;
            if (model.ActionEum == ActionEnum.Add)
            {

                //Add 
                await _service.SRSlaveAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.SRSlaveEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.SRSlaveDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error", "Home");
            }
            return RedirectToAction("StoreRequisitionDetails", new { id = model.Store_RequisitionFK });
        }

        #endregion
        #endregion

        public async Task<IActionResult> StoreAssetRegister(int id)
        {
            #region session values
            string sUser = HttpContext.Session.GetString("User");
            string nUserId = HttpContext.Session.GetString("UserID");
            #endregion
            var model = new VmStoreAssetRegister
            {
                StoreList = await _service.StoreDropDownListAsync(id),
                RawItemList = await _service.RawItemDropDownList(id),
                BrandList = await _service.BrandDropDownList(),
                ModelList = await _service.ModelDropDownList()
            };
            var data = await _service.GetAssetRegisterAll(id);
            model.DataLists = data.DataLists;
            model.CommonStoreFK = id;
            model.User = sUser;
            return View(model);
        }
        public async Task<IActionResult> StoreAssetRegisterAdd(VmStoreAssetRegister obj, int id = 0)
        {
            #region session values
            string sUser = HttpContext.Session.GetString("User");
            string nUserId = HttpContext.Session.GetString("UserID");
            #endregion
            obj.User = sUser;
            var model = new VmStoreAssetRegister
            {
                StoreList = await _service.StoreDropDownListAsync(),
                RawItemList = await _service.RawItemDropDownList(),
                BrandList = await _service.BrandDropDownList(),
                ModelList = await _service.ModelDropDownList(),
                DataLists = new List<VmStoreAssetRegister>()
            };
            var data = await _service.GetAssetRegisterByItem(obj);
            model.DataLists = data.DataLists;
            model.CommonStoreFK = id;
            model.User = sUser;
            ViewBag.Name = data.DataLists.Count == 0 ? "" : data.DataLists[0].ItemName.ToUpper().Substring(0, 3);
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> SaveAllRegisterItem(VmStoreAssetRegister vmmodel, int id = 0)
        {
            #region session values
            string sUser = HttpContext.Session.GetString("User");
            string nUserId = HttpContext.Session.GetString("UserID");
            vmmodel.UserID = Convert.ToInt32(nUserId);
            #endregion

            var data = await _service.SaveAllRegisterItem(vmmodel, "Register", "Store", id);
            return RedirectToAction(nameof(StoreAssetRegister), new { id });
        }

        public async Task<IActionResult> StoreOutPersonal(int id = 0)
        {
            #region session values
            string sUser = HttpContext.Session.GetString("User");
            string nUserId = HttpContext.Session.GetString("UserID");
            #endregion
            var model = new VmStoreOutPersonal
            {
                StoreList = await _service.StoreDropDownListAsync(id),
                RawItemList = await _service.RawItemDropDownList(id),
                BrandList = await _service.BrandDropDownList(),
                ModelList = await _service.ModelDropDownList(),
                AssetList = await _service.AssetDropDownList(),
                DepartmentList = await _service.DeptDropDownList(),
                EmployeeList = await _service.EmpDropDownList(),
                UnitList = await _service.CommonUnitDropDownList(),
                DataLists = new List<VmStoreOutPersonal>()
            };
            ViewBag.Datetoday = DateTime.Now.ToString("MM/dd/yyyy");
            var data = await _service.GetStoreOutPersonal();
            model.DataLists = data.DataLists;
            model.User = sUser;
            //ViewBag.Name = data.DataLists.Count == 0 ? "" : data.DataLists[0].ItemName.ToUpper().Substring(0, 3);
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> StoreOutPersonalSave(VmStoreOutPersonal obj)
        {
            var data = await _service.StoreOutPersonalSave(obj, "Personal", "Store");
            return RedirectToAction(nameof(StoreOutPersonal));
        }

        public async Task<IActionResult> StoreInPersonal(VmStoreOutPersonal obj)
        {
            var model = new VmStoreOutPersonal
            {
                StoreList = await _service.StoreReturnDropDownListAsync(),
                RawItemList = await _service.RawItemReturnDropDownList(),
                BrandList = await _service.BrandReturnDropDownList(),
                ModelList = await _service.ModelReturnDropDownList(),
                AssetList = await _service.AssetReturnDropDownList(),
                DepartmentList = await _service.DeptReturnDropDownList(),
                EmployeeList = await _service.EmpReturnDropDownList(),
                UnitList = await _service.CommonUnitReturnDropDownList(),
                DataLists = new List<VmStoreOutPersonal>()
            };
            ViewBag.Datetoday = DateTime.Now.ToString("MM/dd/yyyy");
            var data = await _service.GetStoreInPersonal();
            model.DataLists = data.DataLists;
            //ViewBag.Name = data.DataLists.Count == 0 ? "" : data.DataLists[0].ItemName.ToUpper().Substring(0, 3);
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> StoreInPersonalSave(VmStoreOutPersonal obj)
        {
            var data = await _service.StoreInPersonalSave(obj, "Personal", "Store");
            return RedirectToAction(nameof(StoreInPersonal));
        }

        public List<object> GetStoreNameByID(int id)
        {
            return _service.StoreToDropDownList(id);
        }
        public List<object> GetApprovedPONameByStyleIdID(int styleId, int storeId)
        {
            if (storeId > 0)
            {
                return _service.ApprovedPOListAsync(styleId);
            }
            else if (storeId == 3)
            {

                return _service.ApprovedPOListAsync(styleId);
            }

            return null;
        }

        public async Task<IActionResult> CreateSRProduction(int id = 0, int id2 = 0)
        {           
            string ndeptId = HttpContext.Session.GetString("UserDepartmentId");           
            var storeId = _service.GetStoreIdsByDeptId(ndeptId);
            var model = new VMStoreRequisitionSlave();

            if (id == 0)
            {
                model = new VMStoreRequisitionSlave()
                {
                    Status = SRStatusEnum.Draft,
                    RawCategoryList = new SelectList(_service.ItemTypeListAsync(id2), "Value", "Text"),
                    //RawCategoryList = new SelectList(_service.RawCategoryDropDownList(), "Value", "Text"),
                    //RawCategoryList = new SelectList(_service.OrderCategoryDropDownList(), "Value", "Text"),
                    EmployeeList = new SelectList(_service.EmployeeDropDownList(), "Value", "Text"),
                    StyleList = new SelectList(_service.StyleDropDownList(), "Value", "Text"),
                    ApprovedStyleList = new SelectList(_service.ApprovedStyleList(), "Value", "Text"),
                    ApprovedPOList = new SelectList(_service.ApprovedPOListAsync(id), "Value", "Text"),
                    Merchandising_StyleSetPackList = new SelectList(_service.DDLSubSetListByStyleID(id), "Value", "Text"),

                    StoreFromList = await _service.StoreDropDownListAsync(),
                    StoreToList = await _service.StoreDropDownListAsync(),
                    DataListSlave = new List<VMStoreRequisitionSlave>()
                };
                model.CommonStoreFK = id2; //storeId;
            }
            else if (id > 0)
            {
                model = await _service.SRWithSlaveListGet(new VMStoreRequisitionSlave() { Store_RequisitionFK = id });
                model.CommonStoreFK = id2; //storeId;
            }
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> CreateSRProduction(VMStoreRequisitionSlave model)
        {
            #region session values
            string sUser = HttpContext.Session.GetString("User");
            string nUserId = HttpContext.Session.GetString("UserID");
            string ndeptId = HttpContext.Session.GetString("UserDepartmentId");
            #endregion

            model.User = sUser;
            model.Status = SRStatusEnum.Draft;

            if (model.ActionEum == ActionEnum.Add)
            {
                if (model.Store_RequisitionFK <= 0)
                {
                    model.Store_RequisitionFK = await _service.ProductionStoreRequisitionAdd(
                        new VMStoreRequisition()
                        {
                            User = sUser,
                            Status = SRStatusEnum.Draft,
                            StyleId = model.StyleId
                        });
                }

                //Add 
                await _service.SRProductionSlaveAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.SRProductionSlaveEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.SRProductionSlaveDelete(model.ID);
            }
            else if ((int)model.ActionEum > 3 && model.Store_RequisitionFK > 0)
            {
                //Delete
                await _service.StoreRequisitionStatusUpdate(new VMStoreRequisition()
                {
                    User = sUser,
                    ID = model.Store_RequisitionFK,
                    ActionId = (int)model.ActionEum,
                    SRActionId = model.SRActionId,
                    Description = model.Description,
                    VMStoreFromFK = model.StoreFromFK,
                    VMStoreToFK = model.StoreToFK
                });
            }
            return RedirectToAction(nameof(CreateSRProduction), new { id = model.Store_RequisitionFK, id2 = model.StoreToFK });
        }

        public async Task<IActionResult> RawSubCategoryGet(int id)
        {
            if (id < 0) { return RedirectToAction("Error", "Home"); }

            var model = await Task.Run(() => _service.RawSubCategoryGet(id));
            var list = model.Select(x => new { Value = x.ID, Text = x.Name }).ToList();
            return Json(list);
        }
        public async Task<IActionResult> RawItemGet(int id)
        {
            if (id < 0) { return RedirectToAction("Error", "Home"); }
            var model = await Task.Run(() => _service.RawItemGet(id));
            var list = model.Select(x => new { Value = x.ID, Text = x.Name }).ToList();
            return Json(list);
        }

        //GetChalllanNoByChallanID
        public JsonResult GetChalllanNoByChallanID(string id)
        {
            var mm = _service.SRStyleDropDownList();
            var model = _service.GetChalllanNoByChallanID(id);
            return Json(model);
        }

        [HttpGet]
        public IActionResult _SRSlaveList(int id = 0, int PRID = 0, int id2 = 0, int itemTypeId = 0, int subSetId = 0)
        {
            var model = new VMStoreRequisitionSlave();
            if (id2 == 8 ) // Knitting
            {
                if (subSetId > 0)
                {
                    model = _service.GetSRSlaveFromFinishingStyleSlave(id, PRID, id2, subSetId);
                }
                else
                {
                    model = _service.GetSRSlaveFromTrimsStyleSlave(id, PRID, id2, itemTypeId);
                    //model = _service.GetSRSlaveFromFinishingStyleSlave(id, PRID, id2); rawCategoryId
                }
            }
            if (id2 == 6 && itemTypeId == 6)
            {
                if (subSetId > 0)
                {
                    model = _service.GetSRSlaveFromFinishingStyleSlave(id, PRID, id2, subSetId);
                }
                else
                {
                    model = _service.GetSRSlaveFromFinishingStyleSlave(id, PRID, id2);
                }
            }
            if (id2 == 5 && itemTypeId == 5) // packing
            {
                if (subSetId > 0)
                {
                    model = _service.GetSRSlaveFromPackingStyleSlave(id, PRID, id2, subSetId);
                }
                else
                {
                    model = _service.GetSRSlaveFromPackingStyleSlave(id, PRID, id2);
                }
            }
            if (id2 == 4 && itemTypeId == 4)  // packing
            {
                if (subSetId > 0)
                {
                    model = _service.GetSRSlaveFromIroningStyleSlave(id, PRID, id2, subSetId);
                }
                else
                {
                    model = _service.GetSRSlaveFromIroningStyleSlave(id, PRID, id2);
                }
            }
            if (id2 == 4 && itemTypeId == 1)  // Ironing
            {
                if (subSetId > 0)
                {
                    model = _service.GetSRSlaveFromSewingStyleSlave(id, PRID, id2, subSetId);
                }
                else
                {
                    model = _service.GetSRSlaveFromSewingStyleSlave(id, PRID, id2);
                }
            }
            if (id2 == 3 && itemTypeId == 2) // sewing
            {
                if (subSetId > 0)
                {
                    model = _service.GetSRSlaveFromCuttingStyleSlave(id, PRID, id2, subSetId);
                }
                else
                {
                    model = _service.GetSRSlaveFromCuttingStyleSlave(id, PRID, id2);
                }
            }
            else if (id2 == 3 && itemTypeId > 0)  // sewing
            {
                model = _service.GetSRSlaveFromTrimsStyleSlave(id, PRID, id2, itemTypeId);
            }
            else if (id2 == 5 && itemTypeId == 6) // packing
            {
                model = _service.GetSRSlaveFromTrimsStyleSlave(id, PRID, id2, itemTypeId);
            }
            else if (id2 == 1 && itemTypeId > 0) //cutting
            {
                if (subSetId > 0)
                {
                    model = _service.GetSRSlaveFromFabricStyleSlave(id, PRID, id2, itemTypeId, subSetId);
                }
                else
                {
                    model = _service.GetSRSlaveFromFabricStyleSlave(id, PRID, id2, itemTypeId);
                }
            }

            model.CommonStoreFK = id2;
            return PartialView("_SRSlaveList", model);
        }

        [HttpPost]
        public async Task<IActionResult> _SRSlaveList(VMStoreRequisitionSlave model, int Id2 = 0)
        {
            #region session values

            string sUser = HttpContext.Session.GetString("User");
            string nUserId = HttpContext.Session.GetString("UserID");
            string ndeptId = HttpContext.Session.GetString("UserDepartmentId");
            #endregion

            var poid = 1;
            model.User = sUser;
            model.UserID = Convert.ToInt32(nUserId);
            if (model.DataListSlave.Count > 0)
            {
                if (model.ActionEum == ActionEnum.Add)
                {
                    model.DataListSlave = model.DataListSlave.Where(x => x.RequisitionQuantity > 0 && x.IsForSave).ToList();
                    if (model.DataListSlave == null || model.DataListSlave.Count <= 0)
                    {
                        return RedirectToAction("CreateSRProduction", new { id = 0 });
                    }

                    model.DataListSlave.ForEach(x =>
                    {
                        x.User = sUser;
                        x.RequiredDate = model.RequiredDate;
                        x.Merchandising_StyleSetPackFK = model.Merchandising_StyleSetPackFK;
                    });

                    poid = await _service.PRSlaveListAdd(model.DataListSlave, model.StyleId, ndeptId);
                    //return RedirectToAction("CreateSRProduction", new { id = poid });
                }
                //return RedirectToAction("CreateSRProduction", new { id = 0 });
            }
            return RedirectToAction("CreateSRProduction", new { id = poid, Id2 = Id2 });
        }

        public IActionResult Report_GetAllawItemListByStyleId(int styleId = 0)
        {
            var model = new ReportRawItem();
            model.DataList = _service.GetAllawItemListByStyleId(styleId);
            return View(model);
        }

        [HttpGet]
        public IActionResult PRSlaveList(int id = 0, int PRID = 0, int typeID = 0, int orgID = 0)
        {
            var model = _service.PRSlaveFromStyleSlaveGet(id, PRID, typeID, orgID);
            return PartialView("_SRSlaveList", model);
        }

        [HttpPost]
        public async Task<IActionResult> StoreSRequisition(VMStoreRequisition model)
        {
            #region session values
            string sUser = HttpContext.Session.GetString("User");
            string nUserId = HttpContext.Session.GetString("UserID");
            #endregion

            model.User = sUser;
            model.Status = SRStatusEnum.Draft;
            if (model.ActionEum == ActionEnum.Add)
            {

                //Add 
                await _service.StoreRequisitionAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.StoreRequisitionEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.StoreRequisitionDelete(model.ID);
            }
            else if (model.ActionId > 3)
            {
                //Delete
                await _service.StoreRequisitionStatusUpdate(model);
            }
            else
            {
                return RedirectToAction("Error", "Home");
            }
            return RedirectToAction("StoreRequisition");
        }
        public async Task<IActionResult> StoreSRequisitionDetails(int id = 0)
        {
            if (id <= 0) { RedirectToAction("Error", "Home"); }
            //get SR & detail info from Database
            var model = await Task.Run(() => _service.SRWithSlaveListGet(new VMStoreRequisitionSlave() { Store_RequisitionFK = id }));



            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> StoreSRequisitionDetails(VMStoreRequisitionSlave model)
        {
            #region session values
            string sUser = HttpContext.Session.GetString("User");
            string nUserId = HttpContext.Session.GetString("UserID");
            #endregion

            model.User = sUser;
            model.Status = SRStatusEnum.Draft;
            if (model.ActionEum == ActionEnum.Add)
            {

                //Add 
                await _service.SRSlaveAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.SRSlaveEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.SRSlaveDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error", "Home");
            }
            return RedirectToAction("StoreRequisitionDetails", new { id = model.Store_RequisitionFK });
        }

        public async Task<IActionResult> UnitListGetByBuIdAsync(int id = 0)
        {
            var list = await _service.UnitListGetByBUIdAsync(id);
            var realist = list.Select(x => new { Value = x.ID, Text = x.Name }).ToList();
            return Json(realist);
        }

        public async Task<IActionResult> DepartmentListGetByUIdAsync(int id = 0)
        {
            var list = await _service.DepartmentListGetByUIdAsync(id);
            var reenlist = list.Select(x => new { Value = x.ID, Text = x.Name }).ToList();
            return Json(reenlist);
        }

        ////////////// StoreIn Master and Slave //////////////

        public async Task<IActionResult> StoreInMaster(int id = 0)
        {
            //var model = await _service.GetStoreIn(id);
            VmStoreInMaster vmmodel = new VmStoreInMaster();
            vmmodel = await _service.GetStoreInMaster(id);
            //if (id == 0)
            //{
            //    vmmodel = await _service.GetStoreInMaster(id);
            //}
            //else
            //{
            //    vmmodel = await _service.GetStoreInMasterCutting(id);
            //}
            return View(vmmodel);
        }
        [HttpPost]
        public async Task<IActionResult> SaveMasterItem(VmStoreInMaster vmmodel, int id)
        {
            #region session values
            string sUser = HttpContext.Session.GetString("User");
            string nUserId = HttpContext.Session.GetString("UserID");
            string ndeptId = HttpContext.Session.GetString("UserDepartmentId");
            vmmodel.User = sUser;

            #endregion

            vmmodel.CommonStoreFK = id == 0 ? 2 : id;
            vmmodel.Store_GeneralFK = id == 0 ? 2 : id;
            vmmodel.DepartmentId = ndeptId;
            var data = await _service.SaveMasterItem(vmmodel);
            return RedirectToAction(nameof(StoreInMaster), new { id = id });
        }

        public async Task<IActionResult> StoreInSlave(VmStoreInMaster vmModel, int id, int id2)
        {
            ViewBag.BinList = new SelectList(_service.BinDropDownList(), "Value", "Text");
            var model = await _service.GetStoreInSlave(vmModel, id, id2);
            model.ID = id;
            model.CommonStoreFK = id2 == 0 ? 0 : id2;
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> SaveAllItemSlave(VmStoreIn vmmodel, int id = 0, int id2 = 0)
        {
            #region session values
            string sUser = HttpContext.Session.GetString("User");
            string nUserId = HttpContext.Session.GetString("UserID");
            string ndeptId = HttpContext.Session.GetString("UserDepartmentId");
            #endregion

            vmmodel.User = sUser;
            vmmodel.DepartmentId = ndeptId;
            var data = await _service.SaveAllItemSlave(vmmodel, id);
            return RedirectToAction(nameof(StoreInMaster), new { id = id2 });
        }

        //[HttpPost]
        //public async Task<IActionResult> SaveAllItemMaster(VmStoreIn vmmodel, int id)
        //{
        //    #region session values
        //    string sUser = HttpContext.Session.GetString("User");
        //    string nUserId = HttpContext.Session.GetString("UserID");
        //    vmmodel.User = sUser;
        //    #endregion
        //    vmmodel.CommonStoreFK = id == 0 ? 2 : id;
        //    var data = await _service.SaveAllItem(vmmodel, "Store", "StockIn");
        //    return RedirectToAction(nameof(StoreInMaster), new { id = id });
        //}

        public List<object> GetChallanByStoreID(int id = 0)
        {
            return _service.GetChallanByStoreID(id);
        }

        ////////////// StoreIn Master and Slave //////////////

        ////////// Report for Store ////////////

        [HttpGet]
        public IActionResult ReportStore(int id)
        {
            VmStoreReport model = new VmStoreReport();
            return View(model);
        }

        [HttpPost]
        public IActionResult ReportStoreInItemWise(VmStoreReport vmmodel)
        {
            var model = _service.StoreInItemWiseReport(vmmodel);
            return View(model);
        }


        ////////// Report for Store ////////////

        public async Task<IActionResult> AssetDepreciation(int id)
        {
            #region session values
            string sUser = HttpContext.Session.GetString("User");
            string nUserId = HttpContext.Session.GetString("UserID");
            #endregion
            var model = new VmAssetDepreciation
            {
                StoreList = await _service.StoreDropDownListAsync(id),
                RawItemList = await _service.RawItemDropDownList(id),
                BrandList = await _service.BrandDropDownList(),
                ModelList = await _service.ModelDropDownList()
            };
            var data = await _service.GetAssetDepreciationAll(id);
            model.DataLists = data.DataLists;
            model.CommonStoreFK = id;
            model.User = sUser;
            return View(model);
        }
        public async Task<IActionResult> AssetDepreciationAdd(VmAssetDepreciation obj, int id = 0)
        {
            #region session values
            string sUser = HttpContext.Session.GetString("User");
            string nUserId = HttpContext.Session.GetString("UserID");
            #endregion

            var model = new VmAssetDepreciation
            {
                StoreList = await _service.StoreDropDownListAsync(),
                RawItemList = await _service.RawItemDropDownList(),
                BrandList = await _service.BrandDropDownList(),
                ModelList = await _service.ModelDropDownList(),
                DataLists = new List<VmAssetDepreciation>()
            };
            var data = await _service.GetAssetDepreciation(obj, "Register", "Store");
            model.DataLists = data.DataLists;
            model.CommonStoreFK = id;
            model.User = sUser;
            ViewBag.Name = data.DataLists.Count == 0 ? "" : data.DataLists[0].ItemName.ToUpper().Substring(0, 3);
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> SaveAllAssetDepreciation(VmAssetDepreciation vmmodel, int id = 0)
        {
            #region session values
            string sUser = HttpContext.Session.GetString("User");
            string nUserId = HttpContext.Session.GetString("UserID");
            vmmodel.UserID = Convert.ToInt32(nUserId);
            #endregion

            var data = await _service.SaveAllAssetDepreciation(vmmodel, "Register", "Store", id);
            return RedirectToAction(nameof(StoreAssetRegister), new { id });
        }

        [HttpGet]
        public async Task<IActionResult> _DepriciationDetails(int year = 0, int dep = 0, int price = 0)
        {
            var datalist = new VmAssetDepreciationDetails();
            var model = new VmAssetDepreciationDetails();
            var modelList = new List<VmAssetDepreciationDetails>();
            for (int i = 1; i <= year; i++)
            {
                var vmodel = new VmAssetDepreciationDetails();
                vmodel.ID = i;
                vmodel.Depreciation = 20;
                vmodel.DepreciationValue = (price * dep) / 100;
                modelList.Add(vmodel);
            }
            datalist.DataLists = modelList;
            return PartialView("_DepriciationDetails", datalist);
        }


    }
}
