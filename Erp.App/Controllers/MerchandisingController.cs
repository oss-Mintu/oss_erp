﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Erp.App.Models;
using Erp.Core.Entity.Merchandising;
using Erp.Core.Services;
using Erp.Core.Services.Home;
using Erp.Core.Services.Merchandising;
using Erp.Core.Services.User;
using Erp.Infrastructure;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;

namespace Erp.App.Controllers
{
    [SoftwareAdmin]
    public class MerchandisingController : Controller
    {
        private readonly VMLogin _vMLogin;
        private readonly ILogger _logger;
        private readonly MerchandisingService _service;
        private readonly HttpContext _httpContext;

        public MerchandisingController(InfrastructureDbContext db, IHttpContextAccessor httpContextAccessor, ILogger<MerchandisingController> logger)
        {
            _service = new MerchandisingService(db, httpContextAccessor.HttpContext);
            _logger = logger;
            SessionHandler sessionHandler = new SessionHandler(db, httpContextAccessor.HttpContext);
            sessionHandler.Adjust();
            _vMLogin = httpContextAccessor.HttpContext.Session.GetObjectFromJson<VMLogin>("LoginUser");
            _httpContext = httpContextAccessor.HttpContext;
        }

        #region ManageOrder
        public IActionResult Order()
        {
            if (_vMLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            var model = Task.Run(() => _service.OrderGet()).Result;
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Order(VMOrder model)
        {
            if (_vMLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            model.UserID = _vMLogin.ID;
            model.Time = DateTime.Now;
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.OrderAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.OrderEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.OrderDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("Order");
        }

        public IActionResult AllOrder()
        {
            if (_vMLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            var model = Task.Run(() => _service.GetClosedOrder()).Result;
            return View(model);
        }

        #endregion

        #region ManageStyle
        public IActionResult OrderView(int Id)
        {
            if (_vMLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            var model = Task.Run(() => _service.OrderViewId(Id)).Result;
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> OrderView(VMOrderView model)
        {
            if (_vMLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            model.vmStyle.UserID = _vMLogin.ID;
            model.vmStyle.Time = DateTime.Now;
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.StyleAdd(model.vmStyle);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.StyleEdit(model.vmStyle);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.StyleDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("OrderView", new { Id = model.vmStyle.Merchandising_BuyerOrderFK });
        }

        public IActionResult UpdateFrom(int Id, string UI)
        {
            ViewBag.CurrencyList = new SelectList(_service.CurrencyDropDownList(), "Value", "Text");
            ViewBag.UnitList = new SelectList(_service.UnitDropDownList(), "Value", "Text");
            ViewBag.OrderCategoryList = new SelectList(_service.OrderCategoryDropDownList(), "Value", "Text");
            var model = new VMOrderView();
            if (Id != 0)
            {
                model.vmStyle = new VMStyle();
                model.vmStyle = _service.StyleById(Id);
                model.ActionId = 2;
                ViewBag.OrderSubCategoryList = new SelectList(_service.OrderSubCategoryByItemIDDropDownList(model.vmStyle.Mkt_ItemFK), "Value", "Text");
                ViewBag.OrderItemList = new SelectList(_service.OrderItemByItemIDDropDownList(model.vmStyle.Mkt_ItemFK), "Value", "Text");
            }
            else
            {
                MultiSelectList lstColor = new MultiSelectList(_service.ColorDropDownList(), "Value", "Text");
                MultiSelectList lstSize = new MultiSelectList(_service.SizeDropDownList(), "Value", "Text");
                model.vmStyle = new VMStyle();
                model.ActionId = 1;
                model.vmStyle.SizeList = lstSize;
                model.vmStyle.ColorList = lstColor;
            }

            return PartialView("_AddStyle", model);
        }

        #endregion

        #region ManageBOM_BOF
        public IActionResult StyleView(int Id)
        {
            if (_vMLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            ViewBag.BOMType = new SelectList(_service.BOMImportDropDownList(), "Value", "Text");
            var model = Task.Run(() => _service.StyleGetByIDView(Id)).Result;

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> StyleView(VMStyleView model)
        {
            if (_vMLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            model.vmBOM.UserID = _vMLogin.ID;
            model.vmBOM.Time = DateTime.Now;
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                switch (model.vmBOM.TypeID)
                {
                    case 1:
                        await _service.BOMAdd(model.vmBOM);
                        return RedirectToAction("StyleView", new { Id = model.vmBOM.Merchandising_StyleFK });
                    case 2:
                        await _service.BOMFabricAdd(model.vmBOM);
                        return RedirectToAction("StyleView", new { Id = model.vmBOM.Merchandising_StyleFK });
                    case 3:
                        await _service.GarmentsTestAdd(model.vmBOM);
                        return RedirectToAction("StyleView", new { Id = model.vmBOM.Merchandising_StyleFK });
                    case 4:
                        await _service.BOMAccessoriesAdd(model.vmBOM);
                        return RedirectToAction("StyleView", new { Id = model.vmBOM.Merchandising_StyleFK });
                    default: return RedirectToAction("Error");
                }
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                switch (model.vmBOM.TypeID)
                {
                    case 1:
                        await _service.BOMEdit(model.vmBOM);
                        return RedirectToAction("StyleView", new { Id = model.vmBOM.Merchandising_StyleFK });
                    case 2:
                        await _service.BOMFabricEdit(model.vmBOM);
                        return RedirectToAction("StyleView", new { Id = model.vmBOM.Merchandising_StyleFK });
                    case 3:
                        await _service.GarmentsTestEdit(model.vmBOM);
                        return RedirectToAction("StyleView", new { Id = model.vmBOM.Merchandising_StyleFK });
                    case 4:
                        await _service.BOMAccessoriesEdit(model.vmBOM);
                        return RedirectToAction("StyleView", new { Id = model.vmBOM.Merchandising_StyleFK });
                    default: return RedirectToAction("Error");
                }
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                if (model.vmBOM.ID > 0)
                {
                    await _service.BOMItemDelete(model.vmBOM.ID);
                }
                else
                {
                    await _service.BOMDelete(model.vmBOM.Merchandising_StyleFK);
                }
                return RedirectToAction("StyleView", new { Id = model.vmBOM.Merchandising_StyleFK });
            }
            else
            {
                return RedirectToAction("Error");
            }
        }

        [HttpPost]
        public async Task<IActionResult> UpdateStyleCMOverhead(VMStyleView model)
        {
            if (_vMLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.StyleCMOverheadUpdate(model.vmStyle);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("StyleView", new { Id = model.vmStyle.ID });
        }

        [HttpPost]
        public async Task<IActionResult> ApprovalBOMItem(VMStyleView model)
        {
            if (_vMLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            if (model.ActionEum == ActionEnum.Approve)
            {
                //Approve
                if (model.vmBOM.ID > 0)
                {
                    await _service.BOMApproval(model.vmBOM.ID, true);
                }
                else
                {
                    await _service.BOMAllItemApproval(model.vmBOM.Merchandising_StyleFK, true);
                }
            }
            else if (model.ActionEum == ActionEnum.UnApprove)
            {
                //Unapprove
                await _service.BOMApproval(model.vmBOM.ID, false);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("StyleView", new { Id = model.vmBOM.Merchandising_StyleFK });
        }

        [HttpPost]
        public async Task<IActionResult> BOM(VMStyleView model)
        {
            if (_vMLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.BOMGroupAdd(model);
            }
            else
            {
                return RedirectToAction("Error");
            }
            //else if (model.ActionEum == ActionEnum.Delete)
            //{
            //    //Delete
            //    await _service.BOMDelete(model.ID);
            //}

            return RedirectToAction("StyleView", new { Id = model.vmStyle.ID });
        }

        public IActionResult BOF(int id)
        {
            if (_vMLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            int userID = _vMLogin.ID;
            ViewBag.CommonUnit = new SelectList(_service.UnitDropDownList(), "Value", "Text");
            ViewBag.RawItems = new SelectList(_service.RawItemDropDownList(), "Value", "Text");
            //var model = Task.Run(() => _service.BOFGetByIDView(id, userID));
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> BOF(VMYarnCalculation model)
        {
            if (_vMLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            model.UserID = _vMLogin.ID;
            if (model.vmBOFView.ActionEum == ActionEnum.Add)
            {
                //Add
                await _service.MerchandisingBOFAdd(model.vmBOFView);
            }
            else if (model.vmBOFView.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.MerchandisingBOFEdit(model.vmBOFView);
            }
            else if (model.vmBOFView.ActionEum == ActionEnum.Delete)
            {
                //Delete
                model.vmBOFView.Merchandising_StyleFK = await _service.MerchandisingBOFDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction(nameof(YarnCalculation), new { Id = model.vmBOFView.Merchandising_StyleFK });
        }

        [HttpPost]
        public async Task<IActionResult> BillOfFabrics(VMStyleView model)
        {
            if (_vMLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.BOMGroupAdd(model);
            }
            else
            {
                return RedirectToAction("Error");
            }

            return RedirectToAction("StyleView", new { Id = model.vmStyle.ID });
        }

        public IActionResult AllStyle()
        {
            if (_vMLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            var model = Task.Run(() => _service.GetClosedStyle()).Result;
            return View(model);
        }
        #endregion

        #region CBSStyle
        public IActionResult Style()
        {
            if (_vMLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            VMStyle model = Task.Run(() => _service.StyleCBSGet()).Result;
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> Style(VMStyle model)
        {
            if (_vMLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            model.UserID = _vMLogin.ID;
            model.Time = DateTime.Now;
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.CBSStyleAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.CBSStyleEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.CBSStyleDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("Style");
        }

        public IActionResult UpdateStyle(int Id)
        {
            return ViewComponent("StyleCBSUI", new { ID = Id });
        }
        #endregion

        #region SetPack

        public IActionResult SetPackView(int Id)
        {
            if (_vMLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            var model = Task.Run(() => _service.SetPackGetByStyleIDView(Id)).Result;
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> SetPackView(VMStyleView model)
        {
            if (_vMLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            model.UserID = _vMLogin.ID;
            model.Time = DateTime.Now;
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.SetPackAdd(model.vmStyleSetPack);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.SetPackEdit(model.vmStyleSetPack);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                model.vmStyleSetPack.Merchandising_StyleFK = await _service.SetPackDelete(model.ID);
            }
            else if(model.ActionEum == ActionEnum.UnApprove)
            {
                return RedirectToAction("StyleShipment", new { Id = model.vmStyleSetPack.Merchandising_StyleFK });
            }
            return RedirectToAction("StyleShipment", new { Id = model.vmStyleSetPack.Merchandising_StyleFK });
        }

        #endregion

        #region ManageBOM

        public IActionResult GetBOMCBSForm(int ID, int RawCatId, int StyleId, string VId)
        {
            if (_vMLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            ViewBag.SubCategoryList = new SelectList(_service.RawSubCatByCategoryIDDropDownList(RawCatId), "Value", "Text");
            ViewBag.ItemList = new SelectList(_service.RawItemByCategoryIDDropDownList(RawCatId), "Value", "Text");
            ViewBag.CurrencyList = new SelectList(_service.CurrencyDropDownList(), "Value", "Text");
            var model = new VMStyleView();
            model.vmBOM = new VMBOM();
            if (ID != 0)
            {
                model.ActionId = 2;
                model.vmBOM = _service.BOMItemGetByID(ID);
            }
            else
            {
                model.vmBOM.Common_RawCategoryFK = RawCatId;
                model.vmBOM.Merchandising_StyleFK = StyleId;
                model.ActionId = (int)ActionEnum.Add;
                //model.vmBOM.Merchandising_CBSSlaveFK = StyleId;
            }
            return PartialView("_AddRawItem", model);
        }

        public IActionResult AllBOM()
        {
            if (_vMLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            var model = Task.Run(() => _service.GetClosedBOM()).Result;
            return View(model);
        }

        #endregion

        #region ShipmentSchedule

        public IActionResult StyleShipment(int Id)
        {
            if (_vMLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            ViewBag.DestinationList = new SelectList(_service.DestinationDropDownList(), "Value", "Text");
            ViewBag.PortList = new SelectList(_service.DDLPortList(), "Value", "Text");
            ViewBag.ColorList = new MultiSelectList(_service.ColorByStyleIDDropDownList(Id), "Value", "Text");
            ViewBag.SubSetList = new SelectList(_service.DDLSubSetListByStyleID(Id), "Value", "Text");
            var model = Task.Run(() => _service.StyleShipmentViewId(Id)).Result;
            model.vmShipmentSchedule.Date = DateTime.Now;
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> ShipmentSchedule(VMStyleView model)
        {
            if (_vMLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            model.UserID = _vMLogin.ID;
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add
                await _service.ShipmentAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.ShipmentEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.ShipmentDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("StyleShipment", new { Id = model.vmStyle.ID });
        }

        [HttpPost]
        public async Task<IActionResult> ColorSizeRatio(VMStyleView model)
        {
            if (_vMLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.ShipmentAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.ShipmentRatioDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("StyleShipment", new { Id = model.vmStyle.ID });
        }

        public IActionResult AddPortColorSize(List<string> ColorIds, int StyleId, int? SubSetId)
        {
            if (_vMLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            var model = new VMStyleView();
            model.ListColorSize = _service.GetColorSizeRatioByStyleID(ColorIds, StyleId, SubSetId);
            return PartialView("_PortSize", model);
        }
        #endregion

        #region ManageCBS
        public IActionResult CBS()
        {
            if (_vMLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            var model = Task.Run(() => _service.CBSGet()).Result;
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> CBS(VMCBS model)
        {
            if (_vMLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.CBSAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.CBSEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.CBSDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("CBS");
        }

        public IActionResult UpdateCBS(int Id)
        {
            return ViewComponent("CBSUI", new { ID = Id });
        }

        public IActionResult CBSView(int Id)
        {
            if (_vMLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            ViewBag.ReferenceType = new SelectList(_service.BOMImportDropDownList(), "Value", "Text");
            var model = Task.Run(() => _service.CBSGetByIDView(Id)).Result;
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> CBSView(VMStyleView model)
        {
            if (_vMLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.CBSReferenceAdd(model);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("CBSView", new { Id = model.vmCBS.ID });
        }

        [HttpPost]
        public async Task<IActionResult> CBSItem(VMStyleView model)
        {
            if (_vMLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                switch (model.vmCBSSlave.TypeID)
                {
                    case 1:
                        await _service.CBSSlaveAdd(model.vmCBSSlave);
                        return RedirectToAction("CBSView", new { Id = model.vmCBSSlave.Merchandising_CBSFK });
                    case 2:
                        await _service.CBSItemFabricAdd(model.vmCBSSlave);
                        return RedirectToAction("CBSView", new { Id = model.vmCBSSlave.Merchandising_CBSFK });
                    case 3:
                        await _service.CBSItemChargeAdd(model.vmCBSSlave);
                        return RedirectToAction("CBSView", new { Id = model.vmCBSSlave.Merchandising_CBSFK });
                    default: return RedirectToAction("Error");
                }
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                switch (model.vmCBSSlave.TypeID)
                {
                    case 1:
                        await _service.CBSSlaveEdit(model.vmCBSSlave);
                        return RedirectToAction("CBSView", new { Id = model.vmCBSSlave.Merchandising_CBSFK });
                    case 2:
                        await _service.CBSItemFabricEdit(model.vmCBSSlave);
                        return RedirectToAction("CBSView", new { Id = model.vmCBSSlave.Merchandising_CBSFK });
                    case 3:
                        await _service.CBSItemChargeEdit(model.vmCBSSlave);
                        return RedirectToAction("CBSView", new { Id = model.vmCBSSlave.Merchandising_CBSFK });
                    default: return RedirectToAction("Error");
                }

            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.CBSSlaveDelete(model.vmCBSSlave.ID);
                return RedirectToAction("CBSView", new { Id = model.vmCBSSlave.Merchandising_CBSFK });
            }
            else
            {
                return RedirectToAction("Error");
            }
        }

        [HttpPost]
        public async Task<IActionResult> UpdateCBSQty(VMStyleView model)
        {
            if (_vMLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.CBSUpdate(model.vmCBS);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("CBSView", new { Id = model.vmCBS.ID });
        }
        #endregion

        #region MyRegion

        public IActionResult YarnCalculation(int Id)
        {
            if (_vMLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            int userID = _vMLogin.ID;

            ViewBag.MachineDia = new SelectList(_service.MachineDiaDownList(), "Value", "Text");
            ViewBag.StyleList = new SelectList(_service.StyleDropDownList(), "Value", "Text");
            ViewBag.YarnType = new SelectList(_service.YarnTypeDropDownList(), "Value", "Text");
            ViewBag.Currency = new SelectList(_service.CurrencyDropDownList(), "Value", "Text");
            ViewBag.Unit = new SelectList(_service.UnitDropDownList(), "Value", "Text");
            ViewBag.RawSubCategory = new SelectList(_service.RawSubCategoryDropDownList(), "Value", "Text");
            ViewBag.RawItem = new SelectList(_service.RawItemDropDownList(), "Value", "Text");
            ViewBag.StyleSlaveFabricItemsList = new SelectList(_service.GetStyleSlaveFabricItems(Id), "Value", "Text");


            var model = Task.Run(() => _service.GetYarnCalculation(Id)).Result;
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> YarnCalculation(VMYarnCalculation model)
        {
            if (_vMLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add
                await _service.YarnCalculationAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.YarnCalculationEdit(model);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction(nameof(YarnCalculation), new { Id = model.VMStyle.ID });
        }
        public IActionResult BOFItemPartial(int styleId)
        {
            if (_vMLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            var model = new VMYarnCalculation();

            model.VMBOFViewList = _service.GetItemForBOF(styleId, _vMLogin.ID);
            return PartialView("_BOFItem", model);
        }

        [HttpPost]
        public async Task<IActionResult> ChangeYarnCalculationStatus(VMYarnCalculation model)
        {
            if (_vMLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            if (model.ActionEum == ActionEnum.Approve)
            {
                int userID = _vMLogin.ID;
                ViewBag.CommonUnit = new SelectList(_service.UnitDropDownList(), "Value", "Text");

                ViewBag.RawItems = new SelectList(_service.RawItemDropDownList(), "Value", "Text");
                //Approve
                await _service.YarnCalculationApprove(model.ID);
                // _service.BOFGetByIDView(model.ID,model.Merchandising_StyleFk, userID);
                //_service.EditExistingBOF(model);

            }
            else if (model.ActionEum == ActionEnum.UnApprove)
            {
                //Unapprove
                await _service.YarnCalculationUnApprove(model.ID);
                _service.DeleteExistingBOF(model);

            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.YarnCalculationDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction(nameof(YarnCalculation), new { Id = model.Merchandising_StyleFk });
        }
        [HttpPost]
        public async Task<IActionResult> LoadExistingFabricsSpec(VMYarnCalculation model)
        {
            if (_vMLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            if (model.ActionEum == ActionEnum.Add)
            {
                int userID = _vMLogin.ID;
                await _service.LoadExistingFabricsSpec(model);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction(nameof(YarnCalculation), new { Id = model.VMStyle.ID });
        }
        [HttpPost]
        public async Task<IActionResult> AddMerchandisingBOF(VMYarnCalculation model)
        {
            if (_vMLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            if (model.ActionEum == ActionEnum.Add)
            {
                int userID = _vMLogin.ID;


                await _service.MerchandisingBOFMultiAdd(model.VMBOFViewList);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction(nameof(YarnCalculation), new { Id = model.Merchandising_StyleFk });
        }
        public JsonResult GetRawItemByRawSubCatID(int ID)
        {
            if (ID != 0)
            {
                var list = _service.RawItemBySubCategoryIDDropDownList(ID);
                return Json(list);
            }
            return null;
        }
        public JsonResult SingleYarnCalculation(int ID)
        {
            if (ID != 0)
            {
                var list = _service.GetSingleYarnCalculation(ID);
                return Json(list);
            }
            return null;
        }
        public JsonResult SingleBOF(int ID)
        {
            if (ID != 0)
            {
                var list = _service.GetSingleBOF(ID);
                return Json(list);
            }
            return null;
        }
        #endregion

        #region Packing
        public IActionResult Packing()
        {
            return View();
        }
        #endregion

        #region ComponentView
        public IActionResult CreateForm(int Id, string VId)
        {
            return ViewComponent("CommonUI", new { id = Id, oid = 0, name = VId });
        }

        public IActionResult BOMForm(int Id, int RawCatId, int StyleId, int TotalPeice, int typeID)
        {
            return ViewComponent("BOMItemUI", new { ID = Id, RawCategoryId = RawCatId, StyleId = StyleId, TotalPeice = TotalPeice, TypeID = typeID });
        }

        public IActionResult CBSForm(int Id, int RawCatId, int CBSId, int TotalPeice, int typeID)
        {
            return ViewComponent("CBSItemUI", new { ID = Id, RawCategoryId = RawCatId, CBSId = CBSId, TotalPeice = TotalPeice, TypeID = typeID });
        }

        public IActionResult SetPackForm(int Id, int StyleID)
        {
            return ViewComponent("SetPackUI", new { ID = Id, StyleId = StyleID });
        }

        #endregion

        #region JsonResult

        public JsonResult GetCBSStyleInfo(int ID)
        {
            if (ID != 0)
            {
                var list = _service.CBSStyleGetById(ID);
                return Json(list);
            }
            return null;
        }

        #endregion

        #region Report

        public IActionResult ReportBOM(int ID)
        {
            if (_vMLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            var model = _service.StyleBOMReport(ID);
            return View(model);
        }

        public IActionResult ShipmentReport()
        {
            if (_vMLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            ViewBag.ReportType = new SelectList(_service.DDLShipmentReportTypeList(), "Value", "Text");
            ViewBag.BuyerList = new SelectList(_service.BuyerDropDownList(), "Value", "Text");
            VMStyle model = new VMStyle();
            model.FromDate = DateTime.Today.AddMonths(-1);
            model.ToDate = DateTime.Today;
            return View(model);
        }

        [HttpPost]
        public IActionResult ShipmentReport(VMStyle model)
        {
            if (_vMLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            ViewBag.ReportType = new SelectList(_service.DDLShipmentReportTypeList(), "Value", "Text");
            ViewBag.BuyerList = new SelectList(_service.BuyerDropDownList(), "Value", "Text");
            model = _service.GetShipmentDetailsReport(model);
            return View(model);
        }

        public IActionResult StyleReport()
        {
            if (_vMLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            VMStyleView model = new VMStyleView();
            model.vmStyle = new VMStyle();
            model.lstCat1 = new VMBOM();
            model.lstCat2 = new VMBOM();
            model.lstCat3 = new VMBOM();
            model.lstCat4 = new VMBOM();
            model.lstCat5 = new VMBOM();
            model.lstCat6 = new VMBOM();
            model.VMCBSSummary = new VMCBSSummary();
            ViewBag.Style = new SelectList(_service.StyleDropDownList(), "Value", "Text");

            return View(model);
        }

        [HttpPost]
        public IActionResult StyleReport(VMStyleView model)
        {
            if (_vMLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            model = _service.StyleBOMReport(model.vmStyle.ID);
            return View(model);
        }

        public IActionResult ReportBOF(int ID)
        {
            if (_vMLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            var model = Task.Run(() => _service.StyleBOFReportAsync(ID)).Result;
            //var model = _service.StyleBOFReportAsync(ID);
            return View(model);
        }

        public IActionResult StyleFolloup(int ID)
        {
            if (_vMLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            return View();
        }
        [HttpPost]
        public IActionResult StyleFolloup(VMStyleView model)
        {
            if (_vMLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            return View();
        }

        public IActionResult StylePR()
        {
            if (_vMLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            ViewBag.StyleList = new SelectList(_service.StyleDropDownList(), "Value", "Text");
            return View();
        }
        [HttpPost]
        public IActionResult StylePR(VMStyleView model)
        {
            if (_vMLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            return View();
        }
        #endregion

    }
}