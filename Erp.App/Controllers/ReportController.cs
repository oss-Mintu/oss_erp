﻿using Erp.Core.Services;
using Erp.Core.Services.Procurement;
using Erp.Core.Services.Report;
using Erp.Core.Services.Store;
using Erp.Core.Services.User;
using Erp.Infrastructure;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Erp.App.Controllers
{

    [SoftwareAdmin]
    public class ReportController : Controller
    {
        private readonly ILogger _logger;
        private readonly ReportService _service;
        IDataProtector _protector;
        public ReportController(InfrastructureDbContext db, IHttpContextAccessor httpContextAccessor, ILogger<ProcurementController> logger, IDataProtectionProvider provider)
        {
            SessionHandler sessionHandler = new SessionHandler(db, httpContextAccessor.HttpContext);
            sessionHandler.Adjust();
            //_protector = provider.CreateProtector(GetType().FullName);
            _protector = provider.CreateProtector("OSS.ERP");
            _service = new ReportService(db, httpContextAccessor.HttpContext);
            _logger = logger;
        }
        #region ddl
        [HttpGet]
        public async Task<IActionResult> BuyerPOGet(string id = null)
        {
            if (id == null) { return Json(new List<object>()); }
            int nID = 0;

            if (!int.TryParse(_protector.Unprotect(id), out nID))
            {
                return Json(new List<object>());
            }
            var list = await _service.BuyerPOByBuyerDDLAsync(nID);
            var retlist = list.Select(x => new { Value = _protector.Protect(((int)x.GetType().GetProperty("Value").GetValue(x, null)).ToString()), Text = (string)x.GetType().GetProperty("Text").GetValue(x, null) }).ToList();
            return Json(retlist);
        }
        [HttpGet]
        public async Task<IActionResult> StyleGet(string id = null)
        {
            if (id == null) { return Json(new List<object>()); }
            int nID = 0;

            if (!int.TryParse(_protector.Unprotect(id), out nID))
            {
                return Json(new List<object>());
            }

            var list = await _service.StyleByBPODDLAsync(nID);
            var retlist = list.Select(x => new { Value = _protector.Protect(((int)x.GetType().GetProperty("Value").GetValue(x, null)).ToString()), Text = (string)x.GetType().GetProperty("Text").GetValue(x, null) }).ToList();
            return Json(retlist);
        }
        [HttpGet]
        public async Task<IActionResult> SupplierPOGet(string id = null)
        {
            if (id == null) { return Json(new List<object>()); }
            int nID = 0;

            if (!int.TryParse(_protector.Unprotect(id), out nID))
            {
                return Json(new List<object>());
            }
            var list = await _service.SupplierPOBySupplierDDLAsync(nID);
            var retlist = list.Select(x => new { Value = _protector.Protect(((int)x.GetType().GetProperty("Value").GetValue(x, null)).ToString()), Text = (string)x.GetType().GetProperty("Text").GetValue(x, null) }).ToList();
            return Json(retlist);
        }
        #endregion
        #region Buyer PO
        public async Task<IActionResult> BuyerPOInventorySummary(string id = null)
        {
            if (id == null) { return RedirectToAction("Error", "Home"); }
            //get PR & detail info from Database

            int nID = 0;

            if (!int.TryParse(_protector.Unprotect(id), out nID))
            {
                return RedirectToAction("Error", "Home");
            }

            VMBPOReportSlave model = new VMBPOReportSlave();
            if (nID > 0)
            {
                model = await Task.Run(() => _service.BPOInventorySummaryGet(new VMBPOReportSlave() { VMMerchandising_StyleID = nID }));
            }
            return PartialView(model);
        }

        public async Task<IActionResult> BPOWiseInventory()
        {
            VMReportOrder model = new VMReportOrder();
            var list = await _service.BuyerDDLAsync();
            var retlist = list.Select(x => new { Value = _protector.Protect(((int)x.GetType().GetProperty("Value").GetValue(x, null)).ToString()), Text = (string)x.GetType().GetProperty("Text").GetValue(x, null) }).ToList();
            model.BuyerList = new SelectList(retlist, "Value", "Text");
            return View(model);
        }
        #endregion
        #region Supplier Purchase Order

        public async Task<IActionResult> SupplierPO(string id = null)
        {
            if (id == null) { return RedirectToAction("Error", "Home"); }
            //get PR & detail info from Database

            int nID = 0;

            if (!int.TryParse(_protector.Unprotect(id), out nID))
            {
                return RedirectToAction("Error", "Home");
            }

            VMPOReportSlave model = new VMPOReportSlave();
            if (nID > 0)
            {
                model = await Task.Run(() => _service.POWithSlaveListGet(new VMPOReportSlave() { Procurement_PurchaseOrderFK = nID }));
            }
            return PartialView(model);
        }
        public async Task<IActionResult> SupplierPOInventorySummary(string id = null)
        {
            if (id == null) { return RedirectToAction("Error", "Home"); }
            //get PR & detail info from Database

            int nID = 0;

            if (!int.TryParse(_protector.Unprotect(id), out nID))
            {
                return RedirectToAction("Error", "Home");
            }

            VMPOReportSlave model = new VMPOReportSlave();
            if (nID > 0)
            {
                model = await Task.Run(() => _service.POInventorySummaryGet(new VMPOReportSlave() { Procurement_PurchaseOrderFK = nID }));

            }


            return PartialView(model);
        }


        public async Task<IActionResult> POWiseInventory()
        {

            VMReportOrder model = new VMReportOrder();
            var list = await _service.SupplierDDLAsync();
            var retlist = list.Select(x => new { Value = _protector.Protect(((int)x.GetType().GetProperty("Value").GetValue(x, null)).ToString()), Text = (string)x.GetType().GetProperty("Text").GetValue(x, null) }).ToList();
            model.SupplierList = new SelectList(retlist, "Value", "Text");
            return View(model);
        }

        #endregion
        #region StylePO_Emrul

        public async Task<IActionResult> StylePO()
        {
            VMBPOReportSlave model = new VMBPOReportSlave();
            model.DataListSlave = new List<VMBPOReportSlave>();

            var list = await _service.StyleByBPODDLAsync(0);
            var retlist = list.Select(x => new { Value = x.GetType().GetProperty("Value").GetValue(x, null).ToString(), Text = x.GetType().GetProperty("Text").GetValue(x, null)}).ToList();
            ViewBag.StyleList = new SelectList(retlist, "Value", "Text");
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> StylePO(VMBPOReportSlave model)
        {
            var list = await _service.StyleByBPODDLAsync(0);
            var retlist = list.Select(x => new { Value = (string)x.GetType().GetProperty("Value").GetValue(x, null).ToString(), Text = (string)x.GetType().GetProperty("Text").GetValue(x, null) }).ToList();
            ViewBag.StyleList = new SelectList(retlist, "Value", "Text");
            model = await Task.Run(() => _service.POReportGet(model));
            return View(model);
        }


        public async Task<IActionResult> POReport()
        {
            VMBPOReportSlave model = new VMBPOReportSlave();
            model.DataListSlave = new List<VMBPOReportSlave>();

            var list = await _service.StyleByBPODDLAsync(0);
            var retlist = list.Select(x => new { Value = (string)x.GetType().GetProperty("Value").GetValue(x, null).ToString(), Text = (string)x.GetType().GetProperty("Text").GetValue(x, null) }).ToList();
            ViewBag.StyleList = new SelectList(retlist, "Value", "Text");
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> POReport(VMBPOReportSlave model)
        {
            return View();
        }


        public IActionResult ItemPOStoreInventory()
        {
            VMPOReportSlave model = new VMPOReportSlave();
            model.DataListSlave = new List<VMPOReportSlave>();
            ViewBag.SupplierList = new SelectList(_service.DDLSupplier(), "Value", "Text");
            ViewBag.ReportList = new SelectList(_service.DDLReportType(), "Value", "Text");
            ViewBag.OrderList = new SelectList(_service.DDLOrder(), "Value", "Text");
            ViewBag.StyleList = new SelectList(_service.DDLStyle(), "Value", "Text");
            return View(model);
        }
        [HttpPost]
        public IActionResult ItemPOStoreInventory(VMPOReportSlave model)
        {
            ViewBag.SupplierList = new SelectList(_service.DDLSupplier(), "Value", "Text");
            ViewBag.ReportList = new SelectList(_service.DDLReportType(), "Value", "Text");
            ViewBag.OrderList = new SelectList(_service.DDLOrder(), "Value", "Text");
            ViewBag.StyleList = new SelectList(_service.DDLStyle(), "Value", "Text");

            model = _service.PORawInventory(model);
            return View(model);
        }
        #endregion
        #region  Store Inventory
        [HttpGet]
        public IActionResult ReportStore(int id)
        {
            VmStoreReport model = new VmStoreReport();
            return View(model);
        }

        [HttpPost]
        public IActionResult ReportStoreInItemWise(VmStoreReport vmmodel)
        {
            var model = _service.StoreInItemWiseReport(vmmodel);
            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> FormReportInventory(int id = 0)
        {
            VmStoreReport model = new VmStoreReport
            {
                RawCategoryList = new SelectList(_service.RawCategoryDropDownList(), "Value", "Text")                
                //RawItemList = await _service.RawItemDropDownList(id),
                //ApprovedStyleList = new SelectList(_service.ApprovedStyleList(0), "Value", "Text"),
                //RawCategoryList = new SelectList(_service.RawCategoryDropDownList(), "Value", "Text"),
                //UserList = new SelectList(_service.UserDropDownList(), "Value", "Text")
            };
            return View(model);
        }

        [HttpPost]
        public IActionResult ViewReportInventory(VmStoreReport vmmodel)
        {
            var model = _service.ReportStoreInventoryDetails(vmmodel);
            return View(model);
        }

        [HttpGet]
        public IActionResult ReportInventoryType(int id)
        {
            VmStoreReport model = new VmStoreReport
            {
                ApprovedStyleList = new SelectList(_service.ApprovedStyleList(), "Value", "Text"),
            };
            return View(model);
        }

        [HttpPost]
        public IActionResult ReportStoreInventoryType(VmStoreReport vmmodel)
        {
            var model = _service.ReportStoreInventoryType(vmmodel);
            return View(model);
        }


        [HttpGet]
        public async Task<IActionResult> FormReportInventoryConsumption(int id = 0)  
        {
            VmStoreReport model = new VmStoreReport
            {
                ApprovedStyleList = new SelectList(_service.ApprovedStyleList(), "Value", "Text")
                //RawCategoryList = new SelectList(_service.RawCategoryDropDownList(), "Value", "Text")
                //RawItemList = await _service.RawItemDropDownList(id),
                //ApprovedStyleList = new SelectList(_service.ApprovedStyleList(0), "Value", "Text"),
                //RawCategoryList = new SelectList(_service.RawCategoryDropDownList(), "Value", "Text"),
                //UserList = new SelectList(_service.UserDropDownList(), "Value", "Text")
            };
            return View(model);
        }

        [HttpPost]
        public IActionResult ViewReportInventoryConsumption(VmStoreReport vmmodel)
        {
            var model = _service.ReportStoreInventoryDetailsConsumption(vmmodel); 
            return View(model);
        }

        public object GetInventoryRepoets(int styleId)
        {
            var model = _service.GetInventoryRepoets(styleId);            
            return "Data Save Successfully";
        }

        [HttpGet]
        public async Task<IActionResult> FormReportInventoryConsumptionDetails(int id = 0)
        {
            VmStoreReport model = new VmStoreReport
            {
                ApprovedStyleList = new SelectList(_service.ApprovedStyleList(), "Value", "Text"),
                //RawCategoryList = new SelectList(_service.RawCategoryDropDownList(), "Value", "Text")
                //RawItemList = await _service.RawItemDropDownList(id),
                //ApprovedStyleList = new SelectList(_service.ApprovedStyleList(0), "Value", "Text"),
                //RawCategoryList = new SelectList(_service.RawCategoryDropDownList(), "Value", "Text"),
                //UserList = new SelectList(_service.UserDropDownList(), "Value", "Text")
            };
            return View(model);
        }

        [HttpPost]
        public IActionResult ViewReportInventoryConsumptionDetails(VmStoreReport vmmodel)
        {
            //var model = _service.ReportStoreInventoryDetailsConsumptionDetails(vmmodel);
            //return View(model);

            if (vmmodel.Merchandising_StyleFK == 0)
            {
                //var model = _service.ReportStoreInventoryDetailsConsumptionAllStyle(vmmodel);
                return RedirectToAction(nameof(ViewReportInventoryConsumptionAllStyle), vmmodel);
            }
            else
            {
                var model = _service.ReportStoreInventoryDetailsConsumptionDetails(vmmodel);
                return View(model);
            }
        }
        [HttpPost]
        public IActionResult ViewReportInventoryConsumptionAllStyle(VmStoreReport vmmodel)
        {
            if (vmmodel.Merchandising_StyleFK == 0)
            {
                var model = _service.ReportStoreInventoryDetailsConsumptionAllStyle();
                return View(model);
            }
            else if(vmmodel.Merchandising_StyleFK == -1)
            {
                var model = _service.ReportStoreInventoryDetailsConsumptionItemWise();
                return View(model);
            }
            else
            {
                var model = _service.ReportStoreInventoryDetailsConsumptionStyleWise(vmmodel);
                return View(model);
            }
        }

        public async Task<IActionResult> ProductionSR(int id = 0) 
        {
            if (id == 0) { return RedirectToAction("Error", "Home"); }

            //get PR & detail info from Database
            //int nID = 0;
            //if (!int.TryParse(_protector.Unprotect(id), out nID))
            //{
            //    return RedirectToAction("Error", "Home");
            //}

            VMSRReportSlave model = new VMSRReportSlave(); 
            if (id > 0)
            {
                model = await Task.Run(() => _service.ProductionSRGet(
                    new VMSRReportSlave()
                    {
                        Store_RequisitionFK = id
                    }));
            }            
            return PartialView(model);
        }
        #endregion
    }
}