﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Erp.Core.Services;
using Erp.Core.Services.Merchandising;
using Erp.Core.Services.Production;
using Erp.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Erp.App.Controllers
{
    public class HelperController : Controller
    {
        private readonly MerchandisingService _service;
        private ProductionService _prodService;
        private IErpDbContext _db { get; set; }
        private readonly HttpContext httpContext;
        public HelperController(InfrastructureDbContext db, IHttpContextAccessor httpContextAccessor)
        {
            httpContext = httpContextAccessor.HttpContext;
            _db = db;
            _service = new MerchandisingService(db, httpContextAccessor.HttpContext);
        }

        public IActionResult Index()
        {
            return View();
        }

        #region Merchandising

        public JsonResult GetFinishSubCategory(int CatID)
        {
            if (CatID != 0)
            {
                var list = _service.FinishSubCatByCategoryIDDropDownList(CatID);
                return Json(list);
            }
            return null;
        }

        public JsonResult GetFinishItem(int SubCatID)
        {
            if (SubCatID != 0)
            {
                var list = _service.FinishItemBySubCategoryIDDropDownList(SubCatID);
                return Json(list);
            }
            return null;
        }

        public JsonResult GetBOMImport(int ID)
        {
            if (ID == 1)
            {
                var list = _service.CBSDropDownList();
                return Json(list);
            }
            else if (ID == 2)
            {
                var list = _service.StyleDropDownList();
                return Json(list);
            }
            return null;
        }

        public JsonResult GetRawItemByRawSubCatWithStyleID(int Id, int StyleID)
        {
            if (Id != 0)
            {
                var list = _service.BOMRawItemBySubCategoryIDDropDownList(Id, StyleID);
                return Json(list);
            }
            return null;
        }

        public JsonResult GetRawItemByRawSubCatWithCBSID(int Id, int CBSID)
        {
            if (Id != 0)
            {
                var list = _service.CBSRawItemBySubCategoryIDDropDownList(Id, CBSID);
                return Json(list);
            }
            return null;
        }
        #endregion
        #region Production
        public JsonResult GetSMVPaperByID(int ID)
        {
            _prodService = new ProductionService(_db, httpContext);
            if (ID != 0)
            {
                var data = _prodService.GetSMVPaperByID(ID);
                return Json(data);
            }
            return null;

        }

        public JsonResult GetLineWiseChiefSupervisor(int LineId)
        {
            _prodService = new ProductionService(_db, httpContext);
            var list = _prodService.GetLineChiefAndSuperVisorByID(LineId);
            return Json(list);
        }

        public JsonResult GetLineSewingByStyleID(int RefID, int SID)
        {
            _prodService = new ProductionService(_db, httpContext);
            if (RefID != 0 && SID != 0)
            {
                var data = _prodService.DDLSewingLineByStyleID(RefID, SID);
                return Json(data);
            }
            return null;
        }

        public JsonResult GetStyleColor(int ID)
        {
            _prodService = new ProductionService(_db, httpContext);
            if (ID != 0)
            {
                var data = _prodService.GetAllColorByStyleID(ID);
                return Json(data);
            }
            return null;
        }

        public JsonResult GetStyleSubset(int ID)
        {
            _prodService = new ProductionService(_db, httpContext);
            if (ID != 0)
            {
                var data = _prodService.DDLSubSetListByStyleID(ID);
                return Json(data);
            }
            return null;
        }

        public JsonResult GetPrePlanSMV(int Id)
        {
            _prodService = new ProductionService(_db, httpContext);
            if (Id != 0)
            {
                var data = _prodService.GetPrePlanSMVByID(Id);
                return Json(data);
            }
            return null;
        }

        public JsonResult GetCuttingStyleColor(int StyleID, int RefID)
        {
            _prodService = new ProductionService(_db, httpContext);
            if (StyleID != 0 && RefID != 0)
            {
                var data = _prodService.DDLCuttingStyleColor(StyleID, RefID);
                return Json(data);
            }
            return null;
        }
        #endregion
    }
}