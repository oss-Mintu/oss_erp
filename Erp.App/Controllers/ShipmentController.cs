﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Erp.Core.Entity.Shipment;
using Erp.Core.Services;
using Erp.Core.Services.Common;
using Erp.Core.Services.Integration;
using Erp.Core.Services.Merchandising;
using Erp.Core.Services.Procurement;
using Erp.Core.Services.Shipment;
using Erp.Core.Services.User;
using Erp.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;

namespace Erp.App.Controllers
{
    [SoftwareAdmin]
    public class ShipmentController : Controller
    {
        private readonly SessionHandler _sessionHandler;

        private readonly ILogger _logger;
        private readonly ShipmentService _service;
        private int UserID;
        public ShipmentController(InfrastructureDbContext db, IHttpContextAccessor httpContextAccessor, ILogger<ShipmentController> logger)
        {
            _service = new ShipmentService(db, httpContextAccessor.HttpContext);
            _sessionHandler = new SessionHandler(db, httpContextAccessor.HttpContext);
            _sessionHandler.Adjust();
            UserID = Convert.ToInt32(httpContextAccessor.HttpContext.Session.GetString("UserID"));
            _logger = logger;
        }
        [HttpGet]
        public async Task<IActionResult> CompletedShipmentStyle()
        {
            var model = await Task.Run(() => _service.GetCompletedShipmentStyle());            
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> CompletedShipmentStyle(VMCompletedShipmentStyle vmCompletedShipmentStyle)
        {
            var model = await Task.Run(() => _service.ChangeCompleteShipmentStyle(vmCompletedShipmentStyle.ID));
            return RedirectToAction(nameof(CompletedShipmentStyle));
        }
        #region Partial
        public IActionResult StyleShipmentSchedulPartial(string styleDate)
        {
            var model = new VMStyleSchedule();
            model.Date = Convert.ToDateTime(styleDate);
            model.StyleScheduleList = _service.GetStyleShipmentScheduleByDate(model.Date);
            return PartialView("_StyleShipmentSchedule", model);
        }
        public IActionResult StyleShipmentInstructionPartial(int StyleId)
        {
            var model = new VMStyleInstruction();
            model.StyleInstructionList = _service.GetShipmentInstructionByStyleID(StyleId);
            return PartialView("_StyleShipmentInstruction", model);
        }
        public IActionResult StyleByShipmentInvoiceForChallanPartial(int StyleId)
        {
            var model = new VMStyleInvoiced();
            model.UserID = UserID;
            model.vmStyleInvoicedList = _service.GetStyleByShipmentInvoiceForChallan(StyleId);
            return PartialView("_StyleShipmentInvoiceForChallan", model);
        }
        #endregion

        #region Instruction
        public async Task<IActionResult> ShipmentInstructionPrint(int id)
        {

            var model = await _service.ShipmentInstructionGet(id);

            return View(model);
        }
        public IActionResult ShipmentInstructiones()
        {
            VMShipmentInstruction vmShipmentInstruction = new VMShipmentInstruction();

            vmShipmentInstruction.DataList = _service.ShipmentInstructionesGet();

            return View(vmShipmentInstruction);
        }
        [HttpPost]
        public async Task<IActionResult> ShipmentInstructiones(VMShipmentInstruction model)
        {
            model.UserID = UserID;
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.ShipmentInstructionAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.ShipmentInstructionEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.ShipmentInstructionDelete(model);
            }
            //else if ((int)model.ActionEum > 3 && model.Shipment_ShipmentInstructionFk > 0)
            //{
            //    //Delete
            //    await _service.PurchaseRequisitionStatusUpdate(new VMPurchaseRequisition() { ID = model.Procurement_PurchaseRequisitionFK, ActionId = (int)model.ActionEum, PRActionId = model.PRActionId, Description = model.Description });
            //}
            else
            {
                return RedirectToAction("Error", "Home");
            }
            return RedirectToAction("ShipmentInstructiones");
        }
        public async Task<IActionResult> ShipmentInstruction(int id = 0)
        {
            VMShipmentInstructionSlave model = new VMShipmentInstructionSlave();

            if (id == 0)
            {
                model.MerchandisingStyleList = new SelectList(_service.StyleForShipmentInstruction(), "Value", "Text");

            }
            else if (id > 0)
            {
                model = await _service.ShipmentInstructionGet(id);

            }
            model.VMShipmentSchedule = new VMShipmentSchedule();
            model.VMShipmentSchedule = await _service.GetShipmentSchedule();
            model.ShipmentDate = DateTime.Today;
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> ShipmentInstruction(VMShipmentInstructionSlave model, VMStyleSchedule vmStyleSchedule)
        {
            model.UserID = UserID;
            if (model.ActionEum == ActionEnum.Add)
            {
                if (model.Shipment_ShipmentInstructionFk == 0)
                {

                    model.Shipment_ShipmentInstructionFk = await _service.ShipmentInstructionAdd(model);
                }
                //Add 
                await _service.ShipmentInstructionSlaveAdd(model, vmStyleSchedule);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.ShipmentInstructionSlaveEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.ShipmentInstructionSlaveDelete(model);
            }
            //else if ((int)model.ActionEum > 3 && model.Shipment_ShipmentInstructionFk > 0)
            //{
            //    //Delete
            //    await _service.PurchaseRequisitionStatusUpdate(new VMPurchaseRequisition() { ID = model.Procurement_PurchaseRequisitionFK, ActionId = (int)model.ActionEum, PRActionId = model.PRActionId, Description = model.Description });
            //}
            else
            {
                return RedirectToAction("Error", "Home");
            }
            return RedirectToAction("ShipmentInstruction", new { id = model.Shipment_ShipmentInstructionFk });
        }
        public async Task<JsonResult> SingleShipmentInstruction(int id)
        {
            VMShipmentInstruction model = new VMShipmentInstruction();
            model = await _service.GetSingleShipmentInstruction(id);

            return Json(model);
        }
        public async Task<JsonResult> GetShipmentInstruction(int id)
        {
            VMShipmentInstructionSlave model = new VMShipmentInstructionSlave();
            model = await _service.GetShipmentInstruction(id);

            return Json(model);
        }
        #endregion
        #region Invoie
        public async Task<IActionResult> ShipmentInvoiePrint(int id)
        {

            var v = await _service.ShipmentInvoicePrintGet(id);

            return View(v);
        }
        public IActionResult ShipmentInvoies()
        {
            VMShipmentInvoice vmShipmentInvoice = new VMShipmentInvoice();
            vmShipmentInvoice = new VMShipmentInvoiceSlave()
            {
                ShippedByList = new SelectList(_service.GetShippedByDropdown(), "Value", "Text"),
                TermsOfShipmentList = new SelectList(_service.GetTermsOfShipmentDropDown(), "Value", "Text"),               
                CommonBuyerList = new SelectList(_service.CommonBuyerDropDownList(), "Value", "Text"),
                PortOfLoadingList = new SelectList(_service.PortOfLoadingDropDownList(), "Value", "Text"),               
                ShipmentInstructionList = new SelectList(_service.ShipmentInstructionDropDownList(), "Value", "Text"),
               
                DataList = _service.ShipmentInvoicesGet()
            };

            return View(vmShipmentInvoice);
        }
        [HttpPost]
        public async Task<IActionResult> ShipmentInvoies(VMShipmentInvoice model)
        {
            model.UserID = UserID;
            if (model.ActionEum == ActionEnum.Add)
            {

                //Add 
                await _service.ShipmentInvoiceAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.ShipmentInvoiceEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.ShipmentInvoiceDelete(model.ID);
            }

            else
            {
                return RedirectToAction("Error", "Home");
            }
            return RedirectToAction("ShipmentInvoies");
        }
        
        public async Task<IActionResult> ShipmentInvoie(int id = 0)
        {
            VMShipmentInvoiceSlave model = new VMShipmentInvoiceSlave();
            if (id == 0)
            {
                model = new VMShipmentInvoiceSlave()
                {
                    ShippedByList = new SelectList(_service.GetShippedByDropdown(), "Value", "Text"),
                    TermsOfShipmentList = new SelectList(_service.GetTermsOfShipmentDropDown(), "Value", "Text"),
                    //ShipmentInstructionList = new SelectList(_service.ShipmentInstructionDropDownList(), "Value", "Text"),
                    CommonBuyerList = new SelectList(_service.CommonBuyerDropDownList(), "Value", "Text"),
                    PortOfLoadingList = new SelectList(_service.PortOfLoadingDropDownList(), "Value", "Text"),
                    BLDate = DateTime.Today,
                    ExpDate = DateTime.Today,
                    ErcNo = "RA-0106831"
                };
            }
            else if (id > 0)
            {
                model = await _service.ShipmentInvoiceGet(id);
            }
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> ShipmentInvoie(VMShipmentInvoiceSlave model, VMStyleInstruction vmStyleInstruction)
        {
            model.UserID = UserID;
            if (model.ActionEum == ActionEnum.Add)
            {
                if (model.Shipment_InvoiceFk == 0)
                {
                    model.Shipment_InvoiceFk = await _service.ShipmentInvoiceAdd(model);
                    await _service.CommonBuyerNotifyPartyAdd(model);

                }
                if (model.Shipment_InvoiceFk > 0)
                {
                    await _service.ShipmentInvoiceSlaveAdd(model, vmStyleInstruction);

                }
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.ShipmentInvoiceSlaveEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.ShipmentInvoiceSlaveDelete(model.ID);
            }
            //else if ((int)model.ActionEum > 3 && model.Shipment_ShipmentInstructionFk > 0)
            //{
            //    //Delete
            //    await _service.PurchaseRequisitionStatusUpdate(new VMPurchaseRequisition() { ID = model.Procurement_PurchaseRequisitionFK, ActionId = (int)model.ActionEum, PRActionId = model.PRActionId, Description = model.Description });
            //}
            else
            {
                return RedirectToAction("Error", "Home");
            }
            return RedirectToAction("ShipmentInvoie", new { id = model.Shipment_InvoiceFk });
        }

        [HttpPost]
        public async Task<IActionResult> ShipmentInvoieSlaveDelete(VMShipmentInvoiceSlave model)
        {
            model.UserID = UserID;
            int invoiceId;
            if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                invoiceId = await _service.ShipmentInvoiceSlaveDelete(model.ID);
            }

            else
            {
                return RedirectToAction("Error", "Home");
            }
            return RedirectToAction("ShipmentInvoie", new { id = invoiceId });
        }
        public async Task<JsonResult> SingleShipmentInvoice(int id)
        {
            VMShipmentInvoice model = new VMShipmentInvoice();
            model = await _service.GetSingleShipmentInvoice(id);

            return Json(model);
        }
        #endregion

        #region Delivery Challan
        public async Task<IActionResult> DeliveryChallanePrint(int id)
        {

            var model = await _service.ShipmentDeliveryChallanesPrintGet(id);

            return View(model);
        }
        public IActionResult DeliveryChallanes()
        {
            VMShipmentDeliveryChallan vmShipmentDeliveryChallan = new VMShipmentDeliveryChallan()
            {

                ShipmentInvoiceList = new SelectList(_service.ShipmentInvoiceDropDownList(), "Value", "Text"),
                TransportSupplierList = new SelectList(_service.CommonTransportSupplierDropDownList(), "Value", "Text"),
                CandFSupplierList = new SelectList(_service.CommonCandFSupplierDropDownList(), "Value", "Text"),
                ShipmentUnloadingPortList = new SelectList(_service.CommonCountryPortDropDownList(), "Value", "Text"),
                DataList = _service.ShipmentDeliveryChallanesGet(),
                Date = DateTime.Today,
                GatePassDate = DateTime.Today
            };
            return View(vmShipmentDeliveryChallan);
        }
        [HttpPost]
        public async Task<IActionResult> DeliveryChallanes(VMShipmentDeliveryChallan model)
        {
            model.UserID = UserID;
            if (model.ActionEum == ActionEnum.Add)
            {
                await _service.ShipmentDeliveryChallanAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.ShipmentDeliveryChallanEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.ShipmentDeliveryChallanDelete(model.ID);
            }

            else
            {
                return RedirectToAction("Error", "Home");
            }
            return RedirectToAction("DeliveryChallanes");
        }
        public async Task<IActionResult> DeliveryChallan(int id = 0)
        {
            VMShipmentDeliveryChallanSlave model = new VMShipmentDeliveryChallanSlave();
            if (id == 0)
            {
                model = new VMShipmentDeliveryChallanSlave()
                {
                    // Status = PRStatusEnum.Draft,
                    InvoicedStyleList = new SelectList(_service.StyleForDeliveryChallan(), "Value", "Text"),

                    //ShipmentStyleList = new SelectList(_service.StyleForDeliveryChallan(), "Value", "Text"),
                    TransportSupplierList = new SelectList(_service.CommonTransportSupplierDropDownList(), "Value", "Text"),
                    CandFSupplierList = new SelectList(_service.CommonCandFSupplierDropDownList(), "Value", "Text"),

                    ShipmentUnloadingPortList = new SelectList(_service.CommonCountryPortDropDownList(), "Value", "Text"),
                    Date = DateTime.Today,
                    GatePassDate = DateTime.Today
                };
            }
            else if (id > 0)
            {
                model = await _service.ShipmentDeliveryChallanGet(id);

            }
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> DeliveryChallan(VMShipmentDeliveryChallanSlave model, VMStyleInvoiced vmStyleInvoiced)
        {
            model.UserID = UserID;
            if (model.ActionEum == ActionEnum.Add)
            {
                if (model.Shipment_DeliveryChallanFk == 0)
                {
                    model.Shipment_DeliveryChallanFk = await _service.ShipmentDeliveryChallanAdd(model);
                }
                await _service.ShipmentDeliveryChallanSlaveAdd(model, vmStyleInvoiced);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.ShipmentDeliveryChallanSlaveEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.ShipmentDeliveryChallanSlaveDelete(model.ID);
            }

            else
            {
                return RedirectToAction("Error", "Home");
            }
            return RedirectToAction("DeliveryChallan", new { id = model.Shipment_DeliveryChallanFk });
        }
        public async Task<JsonResult> SingleShipmentChallan(int id)
        {
            VMShipmentDeliveryChallan model = new VMShipmentDeliveryChallan();
            model = await _service.GetSingleShipmentDeliveryChallan(id);

            return Json(model);
        }

        #endregion

        #region Terms of Shipment
        public async Task<IActionResult> TermsOfShipment()
        {
            VMTermsOfShipment vmTermsOfShipment;
            vmTermsOfShipment = await Task.Run(() => _service.TermsOfShipmentGet());
            return View(vmTermsOfShipment);
        }
        [HttpPost]
        public async Task<IActionResult> TermsOfShipment(VMTermsOfShipment model)
        {
            model.UserID = UserID;
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.TermsOfShipmentAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.TermsOfShipmentEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.TermsOfShipmentDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("TermsOfShipment");
        }

        #endregion

        #region Bill of Exchange
        public async Task<IActionResult> ShipmentBillOfExchangePrint(int id)
        {

            var v = await _service.ShipmentBillOfExchangePrintGet(id);
            CommonCurrency commonCurrency = new CommonCurrency();
            v.Value = v.DataListSlave != null ? v.DataListSlave.Select(x => decimal.Round(x.Value, 2)).DefaultIfEmpty(0).Sum() : 0;
            v.ValueInWord = commonCurrency.NumberToWords(v.Value, CurrencyType.USD);
            return View(v);
        }
        public IActionResult ShipmentBillofExchanges()
        {
            VMShipmentBillOfExchange vmShipmentBillOfExchange = new VMShipmentBillOfExchange()
            {
                ShipmentInvoiceList = new SelectList(_service.ShipmentInvoiceDropDownList(), "Value", "Text"),
                ExportCommercialInformationList = new SelectList(_service.ExportCommercialInformationDropDownList(), "Value", "Text"),
                ReceivedTypeList = new SelectList(_service.ReceivedTypeDropDownList(), "Value", "Text"),
                DataList = _service.ShipmentBillOfExchangesGet()
            };

            return View(vmShipmentBillOfExchange);
        }
        [HttpPost]
        public async Task<IActionResult> ShipmentBillofExchanges(VMShipmentBillOfExchange model)
        {
            model.UserID = UserID;
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.ShipmentBillOfExchangeAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.ShipmentBillOfExchangeEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.ShipmentBillOfExchangeDelete(model.ID);
            }
            else if (model.ActionEum == ActionEnum.Finalize)
            {
                //Finalize
                await _service.ShipmentBillOfExchangeFinalize(model.ID, model.AccBillValue);
            }
            else
            {
                return RedirectToAction("Error", "Home");
            }
            return RedirectToAction(nameof(ShipmentBillofExchanges));
        }
        public async Task<IActionResult> ShipmentBillofExchange(int id = 0)
        {
            VMShipmentBillOfExchangeSlave model = new VMShipmentBillOfExchangeSlave();
            if (id == 0)
            {
                model = new VMShipmentBillOfExchangeSlave()
                {
                    ShipmentInvoiceList = new SelectList(_service.ShipmentInvoiceForBillOfExchangeDropDownList(), "Value", "Text"),
                    ExportCommercialInformationList = new SelectList(_service.ExportCommercialInformationDropDownList(), "Value", "Text"),
                    ReceivedTypeList = new SelectList(_service.ReceivedTypeDropDownList(), "Value", "Text"),

                };
            }
            else if (id > 0)
            {
                model = await _service.ShipmentBillOfExchangeGet(id);

            }
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> ShipmentBillofExchange(VMShipmentBillOfExchangeSlave model) // , VMStyleInvoice vmStyleInvoice
        {
            model.UserID = UserID;
            if (model.ActionEum == ActionEnum.Add)
            {
                if (model.Shipment_BillOfExchangeFk == 0)
                {
                    model.Shipment_BillOfExchangeFk = await _service.ShipmentBillOfExchangeAdd(model);
                }

                //Add 
                await _service.ShipmentBillOfExchangeSlaveAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.ShipmentBillOfExchangeSlaveEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                model.Shipment_BillOfExchangeFk = await _service.ShipmentBillOfExchangeSlaveDelete(model.ID);
            }

            else
            {
                return RedirectToAction("Error", "Home");
            }
            return RedirectToAction("ShipmentBillofExchange", new { id = model.Shipment_BillOfExchangeFk });
        }
        public async Task<JsonResult> SingleBillOfExchange(int id)
        {
            VMShipmentBillOfExchange model = new VMShipmentBillOfExchange();
            model = await _service.GetSingleShipmentBillOfExchange(id);

            return Json(model);
        }
        #endregion


       
        public IActionResult StyleGetByBuyer(int id)
        {
            if (id < 0) { return RedirectToAction("Error", "Home"); }

            var model = _service.StyleGetByBuyer(id);
            var list = model.Select(x => new { Value = x.ID, Text = x.CID }).ToList();
            return Json(list);
        }
        public IActionResult InstructedStyleGetByBuyer(int id)
        {
            if (id < 0) { return RedirectToAction("Error", "Home"); }

            var model = _service.nstructedStyleGetByBuyer(id);
            var list = model.Select(x => new { Value = x.ID, Text = x.CID }).ToList();
            return Json(list);
        }
        public IActionResult BuyerNotifyPartyById(int id)
        {
            if (id < 0) { return RedirectToAction("Error", "Home"); }

            var model = _service.BuyerNotifyPartyByIdGet(id);
            var list = model.Select(x => new { Value = x.ID, Text = x.NotifyPartyName }).ToList();
            return Json(list);
        }
    }
}