﻿using Erp.Core.Services.Home;
using Erp.Core.Services.Integration;
using Erp.Core.Services.MIS;
using Erp.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace Erp.App.Controllers
{
    public class MobileController : Controller
    {
        private readonly ILogger _logger;
        private readonly MisService _service;
        private readonly IntegrationService _integrationService;

        public MobileController(InfrastructureDbContext db, IHttpContextAccessor httpContextAccessor, ILogger<MobileController> logger)
        {
            _integrationService = new IntegrationService(db, httpContextAccessor.HttpContext);

            _service = new MisService(db);
            _logger = logger;
        }
        //Login
        public IActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Index(VMLogin model)
        {
            if (model.UserName == "MIS" && model.Password == "MIS@2019")
            {
                //Updated by Mintu                 
                //HttpContext.Session.SetString(ControllerName, (string)RouteData.Values["controller"]);
                //HttpContext.Session.SetString(ActionName, (string)RouteData.Values["action"]);
                //HttpContext.Session.SetString(SessionName, model.UserName);
                //HttpContext.Session.SetInt32(SessionId, 1);
                //var name = HttpContext.Session.GetString(SessionName);
                //Updated by Mintu
                return RedirectToAction("Dashboard", "Mobile");
            }
            return RedirectToAction("Index", "Home");
        }

        public async Task<IActionResult> Dashboard()
        {
            var model = await Task.Run(() => _service.MisGet());
            return View(model);
        }
        public async Task<IActionResult> BalanceSheet()
        {
          var model =  await _integrationService.FinancialPositionGetByDateForMis();
            ViewBag.FinancialPosition = model.DataList;
            var frmdt = "2000-01-01";
            var fromDate = Convert.ToDateTime(frmdt);
            model.FromDate = fromDate;
            model.ToDate = DateTime.Today.Date;
            //var model = await Task.Run(() => _service.BalanceSheetGet());
            return View(model);
        }
        public async Task<IActionResult> IncomeStatement()
        {
            var model = await _integrationService.ComprehensiveIncomeSearchForMis();
            ViewBag.CostofGoodsSoldData = model.DataList;
            var frmdt = "2000-01-01";
            var fromDate = Convert.ToDateTime(frmdt);
            model.FromDate = fromDate;
            model.ToDate = DateTime.Today.Date;
            //var model = await Task.Run(() => _service.IncomeStatementGet());
            return View(model);
        }
        public async Task<IActionResult> Payable()
        {
            var model = await Task.Run(() => _service.AccountsPayableGet());
            return View(model);
        }
        public async Task<IActionResult> Receivable()
        {
            var model = await Task.Run(() => _service.AccountsReceivableGet());
            return View(model);
        }
        public async Task<IActionResult> ExportOrderInHand()
        {
            var model = await Task.Run(() => _service.MisOrderConfirmOrderAll());
            return View(model);
            
        }       
        public async Task<IActionResult> BtbLimit()
        {
            var model = await Task.Run(() => _service.BTBLCStatusGet());
            return View(model);
        }      
        public async Task<IActionResult> OrderList()
        {
            var model = await Task.Run(() => _service.MisShipmentStatusGet());
            return View(model);
        }
        public async Task<IActionResult> ShipmentStatusBuyerWise(int id)
        {
            var model = await Task.Run(() => _service.ShipmentStatusByBuyerGet(id));
            return View(model);
        }


        public async Task<IActionResult> RawInventory()
        {
            var model = await Task.Run(() => _service.RawMaterialsGet());
            return View(model);
        }        
        public async Task<IActionResult> ProductionReport()
        {
            var model = await Task.Run(() => _service.DailyProductionGet());
            return View(model);
        }
        public async Task<IActionResult> CMEarned()
        {
            var model = await Task.Run(() => _service.CMEarnedGet());
            return View(model);
        }
       
        public async Task<IActionResult> CashInHand()
        {
            var model = await Task.Run(() => _service.CashInHandGet());
            return View(model);
        }
        public async Task<IActionResult> CashAtBank()
        {
            var model = await Task.Run(() => _service.CashAtBankGet());
            return View(model);
        }       
        public async Task<IActionResult> AttendanceReport()
        {
            var model = await Task.Run(() => _service.MisDailyAttendanceGet());
            return View(model);
        }

        public async Task<IActionResult> DepartmentWiseAttendanceReport(DateTime id)
        {
            string date = id.ToString("yyyy/MM/d"); 

              var model = await Task.Run(() => _service.DepartmentWiseAttendanceGet(date));
            return View(model);
        }
        
        public async Task<ActionResult> ExportOrderInHandDetails(int id)
        {
            var model = await Task.Run(() => _service.OrderConfirmByBuyerGet(id));
            return View(model);
        }
        public IActionResult BtbLiablitites()
        {
            return View();
        }
        public IActionResult AcceptedBillAmount()
        {
            return View();

        }
        public IActionResult WipInventory()
        {
            return View();
        }
        public IActionResult FinishedInventory()
        {
            return View();
        }
        public IActionResult TotalProfitLoss()
        {
            return View();
        }
       
        public IActionResult Efficience()
        {
            return View();
        }
        public IActionResult FinancialStatement()
        {
            return View();
        }
        public IActionResult TimeAndAction()
        {
            return View();
        }
        public IActionResult TimeAndActionAlert()
        {
            return View();
        }
    }
}