﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Erp.Core.Services;
using Erp.Core.Services.App;
using Erp.Core.Services.Home;
using Erp.Core.Services.Merchandising;
using Erp.Core.Services.Store;
using Erp.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Erp.App.Controllers
{
    public class AppsController : Controller
    {
        private readonly ILogger _logger;
        private readonly StoreService _storeService;
        private readonly AppService _appService;
        private readonly HttpContext _httpContext;

        public AppsController(InfrastructureDbContext db, IHttpContextAccessor httpContextAccessor, ILogger<AppsController> logger)
        {
            _httpContext = httpContextAccessor.HttpContext;
            _storeService = new StoreService(db, httpContextAccessor.HttpContext);
            _appService = new AppService(db, httpContextAccessor.HttpContext);
            _logger = logger;
        }

        public IActionResult GetUser(string userName, string password)
        {
            //var userSession = _httpContext.Session.GetObjectFromJson<VMLogin>("LoginUser");
            //var user = _homeservice.GetUserName(userName, password);

            var user = _appService.GetUserName(userName, password);
            return Ok(user);
        }

        public IActionResult GetAllSize()
        {
            //var data = _homeservice.GetAllSizes();
            var data = _appService.GetAllQCStyleSize();
            return Ok(data);
        }

        public IActionResult GetAllColor()
        {
            //var data = _homeservice.GetAllColor();
            var data = _appService.GetAllQCStyleColor();
            return Ok(data);
        }

        public IActionResult GetAllRejectType()
        {
            //var data = _homeservice.GetAllRejectType();
            var data = _appService.GetAllStyleQCType();
            return Ok(data);
        }

        public IActionResult GetAllStyle()
        {
            //var data = _merchandisingService.GetAllApiStyle();
            var data = _appService.GetAllQCStyle();
            return Ok(data);
        }

        public IActionResult GetAllSetPack()
        {
            //var data = _merchandisingService.GetAllApiSetPack();
            var data = _appService.GetAllQCStyleSetPack();
            return Ok(data);
        }

        [HttpPost]
        public async Task<IActionResult> QCInformation([FromBody] VMStore_QCInformation model)
        {
            if (ModelState.IsValid)
            {
                int isAdded = await _storeService.Store_QCInformationAdd(model);
                if (isAdded == -1)
                {
                    model.IsSuccess = true;
                    model.Message = "Data Saved Successfully";
                }
                else
                {
                    model.Message = "Data Not Saved";
                }
            }
            return Ok(model);
        }
    }
}