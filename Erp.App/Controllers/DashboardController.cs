﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Erp.App.Models;
using Erp.Core.Services.Dashboard;
using Erp.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Erp.App.Controllers
{
    public class MerchandisingNotification
    {
        public int TotalStyle { get; set; }
        public int NewStyle { get; set; }
        public int CompleteStyle { get; set; }
        public int CompleteBOM { get; set; }

    }
    public class DashboardController : Controller
    {
        private readonly ILogger _logger;

        private readonly HttpContext httpContext;
    
        private readonly DashboardService _dashboardService;
       
        public DashboardController(InfrastructureDbContext db, IHttpContextAccessor httpContextAccessor, IOptions<AppSettings> appSettings, ILogger<HomeController> logger)
        {
            httpContext = httpContextAccessor.HttpContext;
            httpContext.Session.SetString("Environment", appSettings.Value.Environment);

            _dashboardService = new DashboardService(db, httpContextAccessor.HttpContext);
            _logger = logger;

        }
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult MerchandisingNotificationPartialView()
        {
            MerchandisingNotification merchandisingNotification = new MerchandisingNotification
            {
                CompleteBOM =1,
                CompleteStyle =1,
                NewStyle =2,
                TotalStyle =3
            };
            return View(merchandisingNotification);
        }
    }
}