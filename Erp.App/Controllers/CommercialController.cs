﻿
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Erp.Core.Services;
using Erp.Core.Services.Commercial;
using Erp.Core.Services.Integration;
using Erp.Core.Services.User;
using Erp.Infrastructure;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Logging;

namespace Erp.App.Controllers
{
    [SoftwareAdmin]
    public class CommercialController : Controller
    {
        private readonly SessionHandler _sessionHandler;
        private readonly ILogger _logger;
        private readonly CommercialService _service;
        private readonly IntegrationService _integrationService;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly int UserID;


        public CommercialController(InfrastructureDbContext db, ILogger<CommercialController> logger, IHostingEnvironment hostingEnvironment, IHttpContextAccessor httpContextAccessor)
        {
            _sessionHandler = new SessionHandler(db, httpContextAccessor.HttpContext);
            _sessionHandler.Adjust();
            _hostingEnvironment = hostingEnvironment;
            _service = new CommercialService(db, httpContextAccessor.HttpContext);
            _integrationService = new IntegrationService(db, httpContextAccessor.HttpContext);
            UserID = Convert.ToInt32(httpContextAccessor.HttpContext.Session.GetString("UserID"));
            _logger = logger;
        }
        public ActionResult CommercialSupplierList()
        {
            var model = Task.Run(() => _integrationService.GetSupplierList()).Result;
            return View(model);
        }
        public ActionResult CommercialBBLCPaymentInformation(int id)
        {
            var model = Task.Run(() => _service.BBLCPaymentInformationGet(id)).Result;
            model.B2bLCPayment = model.VmBBLC.Remaining;
            return View(model);
        }
        [HttpPost]
        public async Task<ActionResult> CommercialBBLCPaymentInformation(VMCommercial_BBLCPaymentInformation model)
        {
            model.UserID = UserID;
            if (model.ActionId == (int)ActionEnum.Add)
            {
                await _service.CommercialBBLCPaymentInformationAdd(model);
            }
            return RedirectToAction(nameof(CommercialBBLCPaymentInformation), new { id = model.VmBBLC.ID });
        }


        public ActionResult CommercialBBLCSupplierWise(int id)
        {
            var model = Task.Run(() => _service.GetBBLCSupplierWise(id)).Result;
            return View(model);
        }

        public ActionResult CommercialUDDetailsPrint(int id)
        {
            var model = Task.Run(() => _service.CommercialUDDetailsGet(id)).Result;

            return View(model);
        }


        public ActionResult CommercialUDDetails(int id)
        {
            var model = Task.Run(() => _service.CommercialUDDetailsGet(id)).Result;

            //ViewBag.LcTypeList = new SelectList(_service.LcTypeDropDownList(), "Value", "Text");
            //ViewBag.ApplicantList = new SelectList(_service.SupplierDropDownList(), "Value", "Text");
            //ViewBag.BuyerBankList = new SelectList(_service.BuyerBankDropDownList(), "Value", "Text");
            //ViewBag.LienBankList = new SelectList(_service.LienBankDropDownList(), "Value", "Text");
            //ViewBag.Currency = new SelectList(_service.CurrencyDropDownList(), "Value", "Text");
            //ViewBag.Unit = new SelectList(_service.UnitDropDownList(), "Value", "Text");
            //ViewBag.Item = new SelectList(_service.RawItemDropDownList(), "Value", "Text");
            //ViewBag.UD = new SelectList(_service.UDDropDownList(), "Value", "Text");
            //ViewBag.Supplier = new SelectList(_service.SupplierDropDownList(), "Value", "Text");
            //ViewBag.ECIList = new SelectList(_service.CommercialECIDropDownList(), "Value", "Text");
            //ViewBag.OrginList = new SelectList(_service.OriginDropDownList(), "Value", "Text");
            //ViewBag.BuyerPOList = new SelectList(_service.BuyerPODropDownList(), "Value", "Text");
            //ViewBag.POList = new SelectList(_service.PODropDownList(), "Value", "Text");
            return View(model);
        }

        //public ActionResult UDDetailBuyerOrder(int id)
        //{
        //    var model = Task.Run(() => service.GetUDDetailBuyerOrder(id)).Result;
        //    ViewBag.LcTypeList = new SelectList(service.LcTypeDropDownList(), "Value", "Text");
        //    ViewBag.ApplicantList = new SelectList(service.SupplierDropDownList(), "Value", "Text");
        //    ViewBag.BuyerBankList = new SelectList(service.BuyerBankDropDownList(), "Value", "Text");
        //    ViewBag.LienBankList = new SelectList(service.LienBankDropDownList(), "Value", "Text");
        //    ViewBag.CompanyBankList = new SelectList(service.CompanyBankDropDownList(), "Value", "Text");
        //    ViewBag.Currency = new SelectList(service.CurrencyDropDownList(), "Value", "Text");
        //    ViewBag.Unit = new SelectList(service.UnitDropDownList(), "Value", "Text");
        //    ViewBag.Item = new SelectList(service.RawItemDropDownList(), "Value", "Text");
        //    ViewBag.UD = new SelectList(service.UDDropDownList(), "Value", "Text");
        //    ViewBag.Supplier = new SelectList(service.SupplierDropDownList(), "Value", "Text");
        //    ViewBag.ECIList = new SelectList(service.ECIDropDownList(id), "Value", "Text");
        //    //ViewBag.OrginList = new SelectList(service.OrginDropDownList(), "Value", "Text");
        //    ViewBag.BuyerPOList = new SelectList(service.BuyerPODropDownList(), "Value", "Text");
        //    ViewBag.POList = new SelectList(service.PODropDownList(), "Value", "Text");
        //    return View(model);
        //}

        [HttpGet]
        public async Task<IActionResult> CommercialUD()
        {
            var model = await Task.Run(() => _service.CommercialUDGet());
            model.Date = DateTime.Today;

            ViewBag.Buyer = new SelectList(_service.BuyerDropDownList(), "Value", "Text");
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> CommercialUD(VMCommercialUD model, [FromForm] IFormFile ScanedFile)
        {
            //if (service.IsUdRefNoExits(model.Ref))
            //{
            //    return RedirectToAction(nameof(UDBuyerOrder));
            //}
            string fullPath = "";
            if (ScanedFile != null)
            {
                #region Upload file
                //string fName = Guid.NewGuid() + file.Substring(exten.IndexOf("."), exten.Length - exten.IndexOf("."));
                string folderName = "UDDocument";
                string webRootPath = _hostingEnvironment.WebRootPath;
                string newPath = Path.Combine(webRootPath, folderName);

                if (!Directory.Exists(newPath))
                {
                    Directory.CreateDirectory(newPath);
                }
                if (ScanedFile.Length > 0)
                {
                    string exten = Path.GetFileName(ScanedFile.FileName);
                    string fName = Guid.NewGuid() + exten.Substring(exten.IndexOf("."), exten.Length - exten.IndexOf("."));
                    string sFileExtension = Path.GetExtension(ScanedFile.FileName).ToLower();
                    model.ScanedFile = "/UDDocument/" + fName;
                    fullPath = Path.Combine(newPath, fName);
                    //using (var stream = new FileStream(fullPath, FileMode.OpenOrCreate))

                    FileStream stream;
                    using (stream = new FileStream(fullPath, FileMode.OpenOrCreate))
                    {
                        ScanedFile.CopyTo(stream);
                        stream.Position = 0;
                    }
                }
                #endregion
            }
            if (model.ActionEum == ActionEnum.Add)
            {
                await _service.CommercialUDAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.CommercialUDEdit(model);
            }
            //else if (model.ActionEum == ActionEnum.Delete)
            //{
            //    //Delete
            //    await service.UDBuyerOrderDelete(model.ID);
            //}
            return RedirectToAction(nameof(CommercialUD));
        }
        //public IActionResult UDView(int id)
        //{
        //    var model = Task.Run(() => service.GetUDBuyerOrder(id)).Result;
        //    ViewBag.Buyer = new SelectList(service.BuyerDropDownList(), "Value", "Text");
        //    return View(model);
        //}
        //public async Task<IActionResult> BuyerOrderWithoutLC()
        //{
        //    var model = Task.Run(() => service.GetBuyerOrder()).Result;
        //    var modelA = Task.Run(() => service.GetBuyerOrder());
        //    return View(model);
        //}
        //public async Task<IActionResult> PurchaseOrderWithoutLC()
        //{
        //    var model = Task.Run(() => service.GetPurchaseOrder()).Result;
        //    return View(model);
        //}

        //// Master LC

       
        [HttpGet]
        public IActionResult CommercialECI(int id = 0)
        {
            var model = Task.Run(() => _service.GetECI(id)).Result;
            model.ECIDate = DateTime.Today;
            model.ExpiryDate = DateTime.Today;
            model.ShipmentDate = DateTime.Today;

            ViewBag.LcTypeList = new SelectList(_service.LcTypeForECIDropDownList(), "Value", "Text");
            ViewBag.BuyerApplicantList = new SelectList(_service.BuyerDropDownList(), "Value", "Text");
            ViewBag.BuyerBankList = new SelectList(_service.BuyerBankDropDownList(), "Value", "Text");
            ViewBag.LienBankList = new SelectList(_service.LienBankDropDownList(), "Value", "Text");
            ViewBag.CompanyBankList = new SelectList(_service.CompanyBankDropDownList(), "Value", "Text");
            ViewBag.Currency = new SelectList(_service.CurrencyDropDownList(), "Value", "Text");
            ViewBag.Unit = new SelectList(_service.UnitDropDownList(), "Value", "Text");
            //ViewBag.BuyerPOList = new SelectList(_service.BuyerPODropDownList(), "Value", "Text");
            ViewBag.BuyerNotifyParty = new SelectList(_service.BuyerNotifyPartyDropDownList(), "Value", "Text");
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> CommercialECI(VmECI model, [FromForm] IFormFile ScanedFile)
        {
            string fullPath = "";
            model.UserID = UserID;
            if (ScanedFile != null)
            {
                #region Upload file
                //string fName = Guid.NewGuid() + file.Substring(exten.IndexOf("."), exten.Length - exten.IndexOf("."));
                string folderName = "ECIDocument";
                string webRootPath = _hostingEnvironment.WebRootPath;
                string newPath = Path.Combine(webRootPath, folderName);

                if (!Directory.Exists(newPath))
                {
                    Directory.CreateDirectory(newPath);
                }
                if (ScanedFile.Length > 0)
                {
                    string exten = Path.GetFileName(ScanedFile.FileName);
                    string fName = Guid.NewGuid() + exten.Substring(exten.IndexOf("."), exten.Length - exten.IndexOf("."));
                    string sFileExtension = Path.GetExtension(ScanedFile.FileName).ToLower();
                    model.ScanedFile = "/ECIDocument/" + fName;
                    fullPath = Path.Combine(newPath, fName);
                    //using (var stream = new FileStream(fullPath, FileMode.OpenOrCreate))

                    FileStream stream;
                    using (stream = new FileStream(fullPath, FileMode.OpenOrCreate))
                    {
                        ScanedFile.CopyTo(stream);
                        stream.Position = 0;
                    }
                }
                #endregion
            }

            if (model.ActionEum == ActionEnum.Add)
            {

                await _service.ECIAdd(model);

            }

            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.ECIEdit(model);
            }
            //else if (model.ActionEum == ActionEnum.Delete)
            //{
            //    //Delete
            //    await service.ECIDelete(model.ID);
            //}
            return RedirectToAction(nameof(CommercialECI));
        }

        //public IActionResult _Eciddl(int id = 0)
        //{
        //    var model = Task.Run(() => service.GetUDDetailBuyerOrder(id)).Result;
        //    ViewBag.ECIList = new SelectList(service.ECIDropDownList(id), "Value", "Text");
        //    return PartialView("_Eciddl", model);
        //}
        //public IActionResult _bbSuplierddl(int id = 0)
        //{
        //    var model = Task.Run(() => service.GetUDDetailBuyerOrder(id)).Result;
        //    ViewBag.POList = new SelectList(service.PODropDownList(id), "Value", "Text");
        //    return PartialView("_bbSuplierddl", model);
        //}
        //public ActionResult ECIDetail()
        //{

        //    var model = Task.Run(() => service.GetECI()).Result;
        //    ViewBag.LcTypeList = new SelectList(service.LcTypeDropDownList(), "Value", "Text");
        //    ViewBag.ApplicantList = new SelectList(service.SupplierDropDownList(), "Value", "Text");
        //    ViewBag.BuyerBankList = new SelectList(service.BuyerBankDropDownList(), "Value", "Text");
        //    ViewBag.LienBankList = new SelectList(service.LienBankDropDownList(), "Value", "Text");
        //    ViewBag.Currency = new SelectList(service.CurrencyDropDownList(), "Value", "Text");
        //    ViewBag.Unit = new SelectList(service.UnitDropDownList(), "Value", "Text");
        //    return View(model);

        //}
        //[HttpPost]
        //public async Task<IActionResult> ECIDetail(VmUDDetailBuyerOrder model)
        //{
        //    if (model.ECIObj.ActionEum == ActionEnum.Add)
        //    {
        //        await service.ECIAdd(model.ECIObj);
        //    }
        //    else if (model.ECIObj.ActionEum == ActionEnum.Edit)
        //    {
        //        //Edit
        //        await service.ECIEdit(model.ECIObj);
        //    }
        //    else if (model.ECIObj.ActionEum == ActionEnum.Delete)
        //    {
        //        //Delete
        //        await service.ECIDelete(model.ECIObj.ID);
        //    }
        //    else if (model.ECIObj.ActionEum == ActionEnum.Detech)
        //    {
        //        //Detech
        //        await service.ECIDetach(model.ECIObj.ID);
        //    }
        //    else if (model.ECIObj.ActionEum == ActionEnum.Attech)
        //    {
        //        //Attech
        //        await service.ECIAttech(model.ECIObj);
        //    }

        //    return RedirectToAction("UDDetailBuyerOrder", new { id = model.ECIObj.Common_UDFK });
        //}

        public ActionResult CommercialBackToBack(int id = 0)
        {
            var model = Task.Run(() => _service.GetBBLC(id)).Result;
            model.LCDate = DateTime.Today;
            //ViewBag.Item = new SelectList(service.SupplierDropDownList(), "Value", "Text");
            //ViewBag.UD = new SelectList(service.SupplierDropDownList(), "Value", "Text");
            ViewBag.LcTypeList = new SelectList(_service.LcTypeForBBDropDownList(), "Value", "Text");
            ViewBag.Common_SupplierList = new SelectList(_service.SupplierDropDownList(), "Value", "Text");
            ViewBag.Common_CurrencyList = new SelectList(_service.CurrencyDropDownList(), "Value", "Text");
            ViewBag.CommercialLCOreginList = new SelectList(_service.OriginDropDownList(), "Value", "Text");
            ViewBag.CommercialUDList = new SelectList(_service.UDDropDownList(), "Value", "Text");

            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> CommercialBackToBack(VmBBLC model, [FromForm] IFormFile ScanedFile)
        {
            string fullPath = "";
            if (ScanedFile != null)
            {
                #region Upload file
                //string fName = Guid.NewGuid() + file.Substring(exten.IndexOf("."), exten.Length - exten.IndexOf("."));
                string folderName = "BBDocument";
                string webRootPath = _hostingEnvironment.WebRootPath;
                string newPath = Path.Combine(webRootPath, folderName);

                if (!Directory.Exists(newPath))
                {
                    Directory.CreateDirectory(newPath);
                }
                if (ScanedFile.Length > 0)
                {
                    string exten = Path.GetFileName(ScanedFile.FileName);
                    string fName = Guid.NewGuid() + exten.Substring(exten.IndexOf("."), exten.Length - exten.IndexOf("."));
                    string sFileExtension = Path.GetExtension(ScanedFile.FileName).ToLower();
                    model.ScanedFile = "/BBDocument/" + fName;
                    fullPath = Path.Combine(newPath, fName);
                    //using (var stream = new FileStream(fullPath, FileMode.OpenOrCreate))

                    FileStream stream;
                    using (stream = new FileStream(fullPath, FileMode.OpenOrCreate))
                    {
                        ScanedFile.CopyTo(stream);
                        stream.Position = 0;
                    }
                }
                #endregion
            }
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.BBLCAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.BBLCEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.BBLCDelete(model.ID);
            }

            return RedirectToAction(nameof(CommercialBackToBack));
        }
        //[HttpPost]
        //public async Task<IActionResult> BBLCDetail(VmUDDetailBuyerOrder model)
        //{
        //    if (model.BBObj.ActionEum == ActionEnum.Add)
        //    {
        //        //Add 
        //        await service.BBLCAdd(model.BBObj);
        //    }
        //    else if (model.BBObj.ActionEum == ActionEnum.Edit)
        //    {
        //        //Edit               
        //        await service.BBLCEdit(model.BBObj);
        //    }
        //    else if (model.BBObj.ActionEum == ActionEnum.Delete)
        //    {
        //        //Delete
        //        await service.BBLCDelete(model.BBObj.ID);
        //    }
        //    else if (model.BBObj.ActionEum == ActionEnum.Detech)
        //    {
        //        //Detach
        //        await service.BBLCDetach(model.BBObj.ID);
        //    }
        //    return RedirectToAction(nameof(UDDetailBuyerOrder), new { id = model.BBObj.Common_UDFK });
        //}

        //public ActionResult AddBuyerPO()
        //{
        //    var model = Task.Run(() => service.GetECI()).Result;
        //    ViewBag.BuyerPOList = new SelectList(service.BuyerPODropDownList(), "Value", "Text");
        //    return View(model);
        //}
        //[HttpPost]
        //public async Task<IActionResult> AddUDDetailBuyerPO(VmUDDetailBuyerOrder model)
        //{
        //    if (model.ECIObj.ActionEum == ActionEnum.Add)
        //    {
        //        await service.ECIPOMapping(model.ECIObj);
        //    }
        //    return RedirectToAction("UDDetailBuyerOrder", new { id = model.ID });

        //}
        [HttpPost]
        public async Task<IActionResult> MappingUDInECI(VmECI model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                await _service.MappingUDInECIPost(model);
            }
            return RedirectToAction(nameof(CommercialECI));

        }
        [HttpPost]
        public async Task<IActionResult> MappingECIInUD(VMCommercialUDSlave model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                await _service.MappingECIInUDPost(model.VmECI);
            }
            return RedirectToAction(nameof(CommercialUDDetails), new { id = model.VmECI.Commercial_UDFk });

        }
        [HttpPost]
        public async Task<IActionResult> MappingBBInUD(VMCommercialUDSlave model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                await _service.MappingBBInUDPost(model.VmBBLC);
            }
            return RedirectToAction(nameof(CommercialUDDetails), new { id = model.VmBBLC.Commercial_UDFk });

        }


        [HttpPost]
        public async Task<IActionResult> DetachECIFromUD(VMCommercialUDSlave model)
        {

            if (model.ActionEum == ActionEnum.Add)
            {
                await _service.DetachECIFromUDPost(model.VmECI);
            }
            return RedirectToAction(nameof(CommercialUDDetails), new { id = model.VmECI.Commercial_UDFk });

        }
        [HttpPost]
        public async Task<IActionResult> DetachBBFromUD(VMCommercialUDSlave model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                await _service.DetachBBFromUDPost(model.VmBBLC);
            }
            return RedirectToAction(nameof(CommercialUDDetails), new { id = model.VmBBLC.Commercial_UDFk });

        }
        [HttpPost]
        public async Task<IActionResult> MappingUDInBB(VmBBLC vmBBLC)
        {
            if (vmBBLC.ActionEum == ActionEnum.Add)
            {
                await _service.MappingUDInBBPost(vmBBLC);
            }
            return RedirectToAction(nameof(CommercialBackToBack));

        }
        [HttpPost]
        public async Task<IActionResult> DeleteECI(VmECI model)
        {
            if (model.ActionEum == ActionEnum.Delete)
            {
                await _service.DeleteECIPost(model);
            }
            return RedirectToAction(nameof(CommercialECI));

        }
        [HttpPost]
        public async Task<IActionResult> CommercialUDDelete(VMCommercialUD model)
        {
            if (model.ActionEum == ActionEnum.Delete)
            {
                await _service.CommercialUDDelete(model.ID);
            }
            return RedirectToAction(nameof(CommercialUD));

        }
        [HttpPost]
        public async Task<IActionResult> CommercialUDClose(VMCommercialUD model)
        {
            if (model.ActionEum == ActionEnum.Close)
            {
                await _service.CommercialUDClose(model.ID);
            }
            return RedirectToAction(nameof(CommercialUD));
        }
        [HttpPost]
        public async Task<IActionResult> CommercialUDReOpen(VMCommercialUD model)
        {
            if (model.ActionEum == ActionEnum.ReOpen)
            {
                await _service.CommercialUDReOpen(model.ID);
            }
            return RedirectToAction(nameof(CommercialUD));
        }
        [HttpPost]
        public async Task<IActionResult> DeleteBacktoBack(VmBBLC vmBBLC)
        {
            if (vmBBLC.ActionEum == ActionEnum.Delete)
            {
                await _service.DeleteBacktoBackPost(vmBBLC);
            }
            return RedirectToAction(nameof(CommercialBackToBack));

        }
        [HttpPost]
        public async Task<IActionResult> DetachBuyerPoFromECI(VmECI model)
        {
            if (model.ActionEum == ActionEnum.Detech)
            {
                await _service.DetachBuyerPoFromECIPost(model);
            }
            return RedirectToAction(nameof(CommercialECI));

        }
        [HttpPost]
        public async Task<IActionResult> DetachSupplierPoFromBB(VmBBLC vmBBLC)
        {
            if (vmBBLC.ActionEum == ActionEnum.Detech)
            {
                await _service.DetachSupplierPoFromBBPost(vmBBLC);
            }
            return RedirectToAction(nameof(CommercialBackToBack));

        }
        [HttpPost]
        public async Task<IActionResult> AddBuyerPO(VmECI model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                await _service.ECIBuyerOrderMapping(model);
            }
            return RedirectToAction(nameof(CommercialECI));

        }
        [HttpPost]
        public async Task<IActionResult> AddSupplierPO(VmBBLC model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                await _service.BBSupplierOrderMapping(model);
            }
            return RedirectToAction(nameof(CommercialBackToBack));

        }
        public async Task<IActionResult> MerchandisingBuyerOrderByBuyerId(int id)
        {
            if (id < 0) { return RedirectToAction("Error", "Home"); }

            var model = await Task.Run(() => _service.MerchandisingBuyerOrderByBuyerIdGet(id));
            var list = model.Select(x => new { Value = x.ID, Text = x.CID }).ToList();
            return Json(list);
        }
        public async Task<IActionResult> BuyerBankByBuyerId(int id)
        {
            if (id < 0) { return RedirectToAction("Error", "Home"); }

            var model = await Task.Run(() => _service.BuyerBankByBuyerIdDropDownList(id));            
            return Json(model);
        }
        public async Task<IActionResult> ProcurementPurchaseOrderBySypplierId(int id)
        {
            if (id < 0) { return RedirectToAction("Error", "Home"); }

            var model = await Task.Run(() => _service.ProcurementPurchaseOrderBySypplierIdGet(id));
            var list = model.Select(x => new { Value = x.ID, Text = x.CID }).ToList();
            return Json(list);
        }
        public async Task<IActionResult> MappingUDByBuyerIdWithECI(int id)
        {
            if (id < 0) { return RedirectToAction("Error", "Home"); }

            var model = await Task.Run(() => _service.MappingUDByBuyerIdWithECIGet(id));
            var list = model.Select(x => new { Value = x.ID, Text = x.Ref }).ToList();
            return Json(list);
        }
        public async Task<IActionResult> MappingUDWithBB()
        {

            var model = await Task.Run(() => _service.MappingUDByIdWithBBGet());
            var list = model.Select(x => new { Value = x.ID, Text = x.Ref }).ToList();
            return Json(list);
        }
        public async Task<IActionResult> MappingBBWithUD()
        {

            var model = await Task.Run(() => _service.MappingBBWithUDGet());
            var list = model.Select(x => new { Value = x.ID, Text = x.Ref }).ToList();
            return Json(list);
        }
        public async Task<IActionResult> MappingECIWithUD(int id)
        {

            var model = await Task.Run(() => _service.MappingECIByIdWithUDGet(id));
            var list = model.Select(x => new { Value = x.ID, Text = x.Ref }).ToList();
            return Json(list);
        }
        public async Task<IActionResult> SingleECIJson(int id)
        {
            if (id < 0) { return RedirectToAction("Error", "Home"); }

            var model = await Task.Run(() => _service.SingleECIJsonGet(id));
            return Json(model);
        }
        public async Task<IActionResult> SingleUdJson(int id)
        {
            if (id < 0) { return RedirectToAction("Error", "Home"); }

            var model = await Task.Run(() => _service.SingleUDJsonGet(id));
            return Json(model);
        }
        public async Task<IActionResult> SingleBBLCJson(int id)
        {
            if (id < 0) { return RedirectToAction("Error", "Home"); }

            var model = await Task.Run(() => _service.SingleBBLCJsonGet(id));
            return Json(model);
        }
        //public ActionResult AddPO()
        //{
        //    var model = Task.Run(() => _service.GetBBLC()).Result;
        //    ViewBag.POList = new SelectList(_service.PODropDownList(), "Value", "Text");
        //    return View(model);
        //}
        //[HttpPost]
        //public async Task<IActionResult> AddPO(VmUDDetailBuyerOrder model)
        //{
        //    if (model.BBObj.ActionEum == ActionEnum.Add)
        //    {
        //        await _service.BBLCPOMapping(model.BBObj);
        //    }
        //    return RedirectToAction("UDDetailBuyerOrder", new { id = model.ID });
        //}

        //public JsonResult GetSupplierTypeById(int id)
        //{
        //    var model = _service.GetSupplierTypeById(id);
        //    return Json(model);
        //}

        //public JsonResult GetUDNoByUD(string id)
        //{
        //    var model = _service.IsUdRefNoExits(id);
        //    return Json(model);
        //}

        //public JsonResult GetEciNoByUd(string id)
        //{
        //    var model = _service.IsEciNoExits(id);
        //    return Json(model);
        //}

        //public JsonResult GetBblcNoByUd(string id)
        //{
        //    var model = _service.IsBblcNoExits(id); 
        //    return Json(model);
        //}

        //public JsonResult GetBblcValueByUd(int id, decimal value)
        //{
        //    var model = service.GetBblcValueByUd(id, value);
        //    return Json(model);
        //}
    }
}