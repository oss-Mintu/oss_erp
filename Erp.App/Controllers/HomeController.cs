﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Erp.App.Models;
using Erp.Infrastructure;
using Erp.Core.Services.Accounting;
using Erp.Core.Services.Home;
using Microsoft.Extensions.Logging;
using Erp.Core.Services;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Http;
using Erp.Core.Services.User;
using Erp.Core.Services.Merchandising;
using Erp.Core.Services.Integration;
using Microsoft.AspNetCore.Cors;
using Microsoft.Extensions.Options;

namespace Erp.App.Controllers
{
    [SoftwareAdmin]
    public class HomeController : Controller
    {

        private readonly ILogger _logger;
        public int MyProperty { get; set; }
        private readonly HttpContext httpContext;
        private readonly HomeService _service;
        private readonly IntegrationService _integrationService;
        //private readonly DashboardService _dashboardService;
        public HomeController(InfrastructureDbContext db, IHttpContextAccessor httpContextAccessor, IOptions<AppSettings> appSettings, ILogger<HomeController> logger)
        {
            httpContext = httpContextAccessor.HttpContext;

            httpContext.Session.SetString("Environment", appSettings.Value.Environment);

            _service = new HomeService(db, httpContextAccessor.HttpContext);
            _integrationService = new IntegrationService(db, httpContextAccessor.HttpContext);
            //_dashboardService = new DashboardService(db, httpContextAccessor.HttpContext);
            SessionHandler sessionHandler = new SessionHandler(db, httpContextAccessor.HttpContext);
            sessionHandler.Adjust();
            _logger = logger;
        }
        public IActionResult Index()
        {
            //string v = httpContext.Request.Cookies["Password"];

            var user = httpContext.Session.GetObjectFromJson<VMLogin>("LoginUser");
           

            //InfrastructureDbContext infrastructureDbContext = new InfrastructureDbContext();
            //AccountingService accountingService = new AccountingService(infrastructureDbContext);

            //var model = accountingService.GetAccountTypes().Result;
            return View();
        }

        public IActionResult Demo()
        {

            //var user = httpContext.Session.GetObjectFromJson<VMLogin>("LoginUser");
            
            //var data = _dashboardService.NewOrderCount(user);
            return View();
        }
        public IActionResult GraphData()
        {
            DateTime date = DateTime.Today;
            DateTime fromdate = date.AddDays(-15);
            DateTime todate = date.AddDays(15);
            //var data = _dashboardService.PeriodicProductionGraph(fromdate, todate);
            var data ="";
            return Json(data);
        }
        public IActionResult Profile()
        {
            return View();
        }
        public IActionResult Version()
        {
            return View();
        }

        public IActionResult Buyer()
        {
            var model = Task.Run(() => _service.GetBuyer()).Result;
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> Buyer(VMBuyer model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.BuyerAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.BuyerEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.BuyerDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("Buyer");
        }

        public IActionResult BuyerNotifyParty(int ID)
        {
            ViewBag.CountryList = new SelectList(_service.CountryDropDownList(), "Value", "Text");
            var model = _service.GetBuyerNotify(ID);
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> BuyerNotifyParty(VMBuyerNotify model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                model.ID = await _service.BuyerNotifyAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.BuyerNotifyEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.BuyerNotifyDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("BuyerNotifyParty");
        }

        public IActionResult SettingUpdateForm(int Id)
        {
            return ViewComponent("SettingUI", new { ID = Id });
        }

        public IActionResult BuyerNotify()
        {
            var model = Task.Run(() => _service.GetBuyer()).Result;
            return View(model);
        }

        public IActionResult YarnType()
        {
            var model = Task.Run(() => _service.GetYarnType()).Result;

            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> YarnType(VMYarnType model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.YarnTypeAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.YarnTypeEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.YarnTypeDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("YarnType");
        }
        public IActionResult Unit()
        {
            var model = Task.Run(() => _service.GetUnit()).Result;

            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> Unit(VMUnit model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.UnitAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.UnitEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.UnitDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("Unit");
        }

        public IActionResult Currency()
        {
            var model = Task.Run(() => _service.GetCurrency()).Result;

            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> Currency(VMCurrency model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.CurrencyAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.CurrencyEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.CurrencyDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("Currency");
        }

        public async Task<IActionResult> Supplier()
        {
            var model = await Task.Run(() => _service.GetSupplier());
            model.VMCommon_CountryList = await _service.CountryDropDownListAsync();
            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " GetSupplier / CountryDropDownListAsync, Supplier Page).");
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> Supplier(VMSupplier model)
        {
            #region session values
            string sUser = HttpContext.Session.GetString("User");
            string nUserId = HttpContext.Session.GetString("UserID");
            #endregion
            if (string.IsNullOrEmpty(sUser) || string.IsNullOrEmpty(nUserId)) { return RedirectToAction("Login", "Auth"); }
            model.User = sUser;
            model.UserID = int.Parse(nUserId);
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.SupplierAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.SupplierEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.SupplierDelete(model.ID);
            }
            else
            {
                _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " SupplierAdd / SupplierEdit / SupplierDelete, Supplier Operation failed).");
                return RedirectToAction("Error", "Home");
            }
            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " SupplierAdd / SupplierEdit / SupplierDelete, Supplier Operation).");
            return RedirectToAction("Supplier", "Home");
        }

        public IActionResult RawCategory()
        {
            var model =  _service.GetRawCategory();
            ViewBag.Type = new SelectList(_service.DDLCategoryType(), "Value", "Text");
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> RawCategory(VMRawCategory model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.RawCategoryAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.RawCategoryEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.RawCategoryDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("RawCategory");
        }

        public IActionResult RawSubCategory()
        {
            var model = Task.Run(() => _service.GetRawSubCategory()).Result;
            ViewBag.Raw_Category = new SelectList(_service.Raw_CategoryDropDownList(), "Value", "Text");

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> RawSubCategory(VMRawSubCategory model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.RawSubCategoryAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.RawSubCategoryEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.RawSubCategoryDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("RawSubCategory");
        }

        public IActionResult RawItem()
        {
            ViewBag.Raw_Category = new SelectList(_service.Raw_CategoryDropDownList(), "Value", "Text");
            ViewBag.Raw_SubCategory = new SelectList(_service.Raw_SubCategoryDropDownList(), "Value", "Text");
            ViewBag.Unit = new SelectList(_service.UnitDropDownList(), "Value", "Text");

            var model = Task.Run(() => _service.GetRawItem()).Result;
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> RawItem(VMRawItem model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.RawItemAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.RawItemEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.RawItemDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("RawItem");
        }
        
    

        public IActionResult AssetItem()
        {
            var model = Task.Run(() => _service.GetAssetItem()).Result;
            ViewBag.Raw_Category = new SelectList(_service.SpecificRaw_CategoryDropDownList(), "Value", "Text");
            ViewBag.Raw_SubCategory = new SelectList(_service.SpecificRaw_SubCategoryDropDownList(), "Value", "Text");
            ViewBag.Head = new SelectList(_service.AccountHeadDropDown(), "Value", "Text");
       
            ViewBag.Unit = new SelectList(_service.UnitDropDownList(), "Value", "Text");
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> AssetItem(VMRawItem model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.AssetItemAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.AssetItemEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.AssetItemDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("AssetItem");
        }

        public IActionResult Brand()
        {
            //ViewBag.RawItemList = _service.RawItemDropDownList();
            ViewBag.RawItemList = new SelectList(_service.GetRawItemList(), "Value", "Text");
            var model = Task.Run(() => _service.GetBrand()).Result;
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> Brand(VmBrand model)
        {
            switch (model.ActionEum)
            {
                case ActionEnum.Add:
                    //Add 
                    await _service.BrandAdd(model);
                    break;
                case ActionEnum.Edit:
                    //Edit
                    await _service.BrandEdit(model);
                    break;
                case ActionEnum.Delete:
                    //Delete
                    await _service.BrandDelete(model.ID);
                    break;
            }
            return RedirectToAction(nameof(Brand));
        }
        public IActionResult Model()
        {
            ViewBag.BrandList = new SelectList(_service.GetBrandList(), "Value", "Text");
            var model = Task.Run(() => _service.GetModel()).Result;
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Model(VmModel model) 
        {
            switch (model.ActionEum)
            {
                case ActionEnum.Add:
                    //Add 
                    await _service.ModelAdd(model);
                    break;
                case ActionEnum.Edit:
                    //Edit
                    await _service.ModelEdit(model);
                    break;
                case ActionEnum.Delete:
                    //Delete
                    await _service.ModelDelete(model.ID);
                    break;
            }
            return RedirectToAction(nameof(Model));
        }

        public IActionResult FinishCategory()
        {
            var model = Task.Run(() => _service.GetFinishCategory()).Result;

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> FinishCategory(VMFinishCategory model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.FinishCategoryAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.FinishCategoryEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.FinishCategoryDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("FinishCategory");
        }

        public IActionResult FinishSubCategory()
        {
            var model = Task.Run(() => _service.GetFinishSubCategory()).Result;
            ViewBag.FinishCategory = new SelectList(_service.FinishCategoryDropDownList(), "Value", "Text");

            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> FinishSubCategory(VMFinishSubCategory model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.FinishSubCategoryAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.FinishSubCategoryEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.FinishSubCategoryDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("FinishSubCategory");
        }

        public IActionResult FinishItem()
        {
            var model = Task.Run(() => _service.GetFinishItem()).Result;
            ViewBag.FinishCategory = new SelectList(_service.FinishCategoryDropDownList(), "Value", "Text");
            ViewBag.FinishSubCategory = new SelectList(_service.FinishSubCategoryDropDownList(), "Value", "Text");
            ViewBag.Unit = new SelectList(_service.UnitDropDownList(), "Value", "Text");



            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> FinishItem(VMFinishItem model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                ViewBag.ModalTitle = "Add Finish Item";
                //Add 
                await _service.FinishItemAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {

                //Edit
                await _service.FinishItemEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.FinishItemDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("FinishItem");
        }

        public IActionResult Color()
        {
            var model = Task.Run(() => _service.ColorGet()).Result;

            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> Color(VMColor model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.ColorAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.ColorEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.ColorDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("Color");
        }

        public IActionResult Size()
        {
            var model = Task.Run(() => _service.SizeGet()).Result;

            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> Size(VMSize model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.SizeAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.SizeEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.SizeDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("Size");
        }
        

        public IActionResult PortForm(int ID)
        {
            ViewBag.Country = new SelectList(_service.CountryDropDownList(), "Value", "Text");
            VMPort vMPort = new VMPort();
            if (ID != 0)
            {
                vMPort = Task.Run(() => _service.GetPortByCountryID(ID)).Result;
                vMPort.ActionId = 2;
            }
            //else
            //{
            //    ViewBag.PortList= new SelectList(string.Empty, "Value", "Text");
            //    //MultiSelectList lstPort= new MultiSelectList(string.Empty, "Value", "Text");
            //    //vMPort.PortIds = lstPort;

            //    vMPort.ActionId = 1;
            //}
            return PartialView("_PortForm", vMPort);
        }
        public IActionResult CountryPort()
        {
            var model = Task.Run(() => _service.GetPort()).Result;
            ViewBag.Country = new SelectList(_service.CountryWithPortDropDownList(), "Value", "Text");
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> CountryPort(VMPort model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.PortAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.PortEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.PortDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("CountryPort");
        }


        public IActionResult Country()
        {
            var model = Task.Run(() => _service.CountryGet()).Result;

            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> Country(VMCountry model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.CountryAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.CountryEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.CountryDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("Country");
        }
        public IActionResult Agent()
        {
            var model = Task.Run(() => _service.AgentGet()).Result;
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> Agent(VMAgent model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.AgentAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.AgentEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.AgentDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("Agent");
        }

        public IActionResult InspectionAgent()
        {
            var model = Task.Run(() => _service.InspectionAgentGet()).Result;
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> InspectionAgent(VMInspectionAgent model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.InspectionAgentAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.InspectionAgentEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.InspectionAgentDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("InspectionAgent");
        }




        #region Json Cascading Drop Down
        public JsonResult RawSubCategoryDropDownSpecific(int id)
        {
            if (id != 0)
            {
                var list = _service.RawSubCategoryDropDowncascading(id);
                return Json(list);
            }
            return null;
        }
        public JsonResult SpecificRawSubCategoryDropDown(int id)
        {
            if (id != 0)
            {
                var list = _service.SpecificRawSubCategoryDropDowncascading(id);
                return Json(list);
            }
            return null;
        }


        public JsonResult FinishSubCategoryDropDownSpecific(int id)
        {
            if (id != 0)
            {
                var list = _service.FinishCategoryDropDowncascading(id);
                return Json(list);
            }
            return null;
        }
        #endregion
        
        public IActionResult LCType()
        {
           ViewBag.ECIOrBBD = new SelectList(_service.ECIOrBBDropDownList(), "Value", "Text");
            var model = Task.Run(() => _service.GetLCType()).Result;
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> LCType(VmLCType model)
        {
            switch (model.ActionEum)
            {
                case ActionEnum.Add:
                    //Add 
                    await _service.LCTypeAdd(model);
                    break;
                case ActionEnum.Edit:
                    //Edit
                    await _service.LCTypeEdit(model); 
                    break;
                case ActionEnum.Delete:
                    //Delete
                    await _service.LCTypeDelete(model.ID); 
                    break;
            }
            return RedirectToAction(nameof(LCType));
        }

        public IActionResult ApplicantBank()
        {
            var model = Task.Run(() => _service.GetApplicantBank()).Result;
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> ApplicantBank(VmApplicantBank model)
        {
            switch (model.ActionEum)
            {
                case ActionEnum.Add:
                    //Add 
                    await _service.ApplicantBankAdd(model);
                    break;
                case ActionEnum.Edit:
                    //Edit
                    await _service.ApplicantBankEdit(model);
                    break;
                case ActionEnum.Delete:
                    //Delete
                    await _service.ApplicantBankDelete(model.ID);
                    break;
            }
            return RedirectToAction(nameof(ApplicantBank));
        }
        public IActionResult Bank()
        {
            ViewBag.CountryList = new SelectList(_service.CountryDropDownList(), "Value", "Text");
            var model = Task.Run(() => _service.BankGet()).Result;

            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> Bank(VMBank model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.BankAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.BankEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.BankDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("Bank");
        }


        public IActionResult BuyerBank(int id)
        {
            ViewBag.BankList = new SelectList(_service.BankForeignDropDownList(), "Value", "Text");
            var model =  _service.GetBuyerBankInfo(id);

            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> BuyerBank(VmBuyerBank model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.BuyerBankAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.BuyerBankEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.BuyerBankDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction(nameof(BuyerBank));
        }


        public IActionResult LienBank()
        {
            ViewBag.BankList = new SelectList(_service.BankWithCountryDDL(), "Value", "Text");
            var model = Task.Run(() => _service.GetLienBank()).Result;

            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> LienBank(VmLienBank model)
        {
            switch (model.ActionEum)
            {
                case ActionEnum.Add:
                    //Add 
                    await _service.LienBankAdd(model);
                    break;
                case ActionEnum.Edit:
                    //Edit
                    await _service.LienBankEdit(model);
                    break;
                case ActionEnum.Delete:
                    //Delete
                    await _service.LienBankDelete(model.ID);
                    break;
            }
            return RedirectToAction(nameof(LienBank));
        }

        public IActionResult ReceivingBank(int Id)
        {
            ViewBag.BankList = new SelectList(_service.BankLocalDropDownList(), "Value", "Text");
            var model = _service.ReceivingBankDetail(Id);

            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> ReceivingBank(VmReceivingBank model)
        {
            switch (model.ActionEum)
            {
                case ActionEnum.Add:
                    //Add 
                    await _service.ReceivingBankAdd(model);
                    break;
                case ActionEnum.Edit:
                    //Edit
                    await _service.ReceivingBankEdit(model);
                    break;
                case ActionEnum.Delete:
                    //Delete
                    await _service.ReceivingBankDelete(model.ID);
                    break;
            }
            return RedirectToAction(nameof(ReceivingBank));
        }

        public IActionResult Error() 
        {
            //string v = httpContext.Request.Cookies["Password"];

            var user = httpContext.Session.GetObjectFromJson<VMLogin>("LoginUser");

            //InfrastructureDbContext infrastructureDbContext = new InfrastructureDbContext();
            //AccountingService accountingService = new AccountingService(infrastructureDbContext);

            //var model = accountingService.GetAccountTypes().Result;
            return View();
        }


    




    }
}

