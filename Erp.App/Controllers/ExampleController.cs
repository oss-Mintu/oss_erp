﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Erp.App.Models;
//using Erp.Infrastructure;
using Erp.Core.Services.Accounting;
using Erp.Core.Services;
using Erp.Infrastructure;

using Microsoft.Extensions.Logging;
using Erp.Core.Services.User;

namespace Erp.App.Controllers
{
    [SoftwareAdmin]
    public class ExampleController : BaseController
    {
        private readonly InfrastructureDbContext _db;
        private readonly ILogger _logger;
        public ExampleController(InfrastructureDbContext db, ILogger<ExampleController> logger)
        {
            _db = db;
            _logger = logger;

        }
        public async Task<IActionResult> Index()
        {
            
            var x = new AccountingService(_db);

            var y = await x.Do();
            //var result = await x.GetAccountTypes();

            var ttct = await x.GetAccountTypes();
            //ErpDbContext infrastructureDbContext = new ErpDbContext();
            // AccountingService accountingService = new AccountingService(infrastructureDbContext);

            //var model = accountingService.GetAccountTypes().Result;

            _logger.LogInformation("---------------------------------------------------aadasdsad");
            return View();
        }


        public IActionResult Error()
        {
            return View("Error");
        }
    }
}