﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

//using Erp.Infrastructure;
using Erp.Core.Services.Procurement;
using Microsoft.Extensions.Logging;
using Erp.Infrastructure;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Routing;
using Erp.Core.Services;
using Erp.Core.Services.User;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Erp.Core.Services.Integration;

namespace Erp.App.Controllers
{

    [SoftwareAdmin]
    public class UserController : Controller
    {
        private readonly ILogger _logger;
        private readonly UserService _service;
        private readonly IntegrationService _IntegrationService;
        private IHostingEnvironment _hostingEnvironment;
        private HttpContext httpContext;
        public UserController(InfrastructureDbContext db, ILogger<UserController> logger, IHostingEnvironment hostingEnvironment, IHttpContextAccessor httpContextAccessor)
        {
            SessionHandler sessionHandler = new SessionHandler(db, httpContextAccessor.HttpContext);
            sessionHandler.Adjust();
            httpContext = httpContextAccessor.HttpContext;
            _hostingEnvironment = hostingEnvironment;
            _service = new UserService(db);
            _IntegrationService = new IntegrationService(db, httpContextAccessor.HttpContext);
            _logger = logger;
        }






        #region User
        public async Task<IActionResult> GetEmployeeInfo(int id)
        {
            if (id < 0) { return RedirectToAction("Error", "Home"); }

            var model = await Task.Run(() => _service.GetEmployeeInfoJson(id));
            return Json(model);
        }
        public async Task<IActionResult> UserList()
        {
            string v = httpContext.Request.Cookies["UserNameCookies"];
            VMUser vMUser;
            vMUser = await Task.Run(() => _service.UserGet());
            return View(vMUser);
        }
        public async Task<IActionResult> UserUpdate(int id,int actionId, int type)
        {
            var vMUser = await Task.Run(() => _service.UserGetById(id, actionId, type));
           
            return View(vMUser);
        }
        [HttpPost]
        public async Task<IActionResult> UserList(VMUser model)
        {
          
            model.User = HttpContext.Session.GetString("User");
            if (model.File != null)
            {
                model.Photo = UploadFile(model.File);
            }
            if (model.Password != null)
            {
                model.Password = _service.GetHashedPassword(model.Password);

            }
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.UserAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.UserEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.UserDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("UserList");
        }

        private string UploadFile(IFormFile file)
        {
            string fName = "";
            #region Upload file
            if (file != null)
            {
                string folderName = "User";
                string webRootPath = _hostingEnvironment.WebRootPath;
                string newPath = Path.Combine(webRootPath, folderName);

                if (!Directory.Exists(newPath))
                {
                    Directory.CreateDirectory(newPath);
                }
                if (file.Length > 0)
                {
                    string exten = Path.GetFileName(file.FileName);
                    fName = Guid.NewGuid() + exten.Substring(exten.IndexOf("."), exten.Length - exten.IndexOf("."));
                    string sFileExtension = Path.GetExtension(file.FileName).ToLower();
                    string fullPath = Path.Combine(newPath, fName);
                    FileStream stream;
                    using (stream = new FileStream(fullPath, FileMode.OpenOrCreate))
                    {
                        file.CopyTo(stream);
                        stream.Position = 0;
                    }


                }
            }
            #endregion
            return fName;
        }


        #endregion

        #region User Department
        public async Task<IActionResult> UserDepartment()
        {
            VMUserDepartment vmUserDepartment;
            vmUserDepartment = await _service.UserDepartmentGet();
            //vmUserDepartment.VMBusinessUnitList = await _IntegrationService.BusinessUnitDropDownListAsync();
            //vmUserDepartment.VMUnitList = await _IntegrationService.UnitDropDownListAsync();
            return View(vmUserDepartment);
        }
        [HttpPost]
        public async Task<IActionResult> UserDepartment(VMUserDepartment model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.UserDepartmentAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.UserDepartmentEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.UserDepartmentDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("UserDepartment");
        }
        [HttpGet]
        public async Task<IActionResult> HRMSUnitGet(int id = 0)
        {
            var list = await _IntegrationService.UnitListGetByBUIdAsync(id);
            var retlist = list.Select(x => new { Value = x.ID, Text = x.Name }).ToList();
            return Json(retlist);
        }

        #endregion

        #region User Menu
        public async Task<IActionResult> UserMenu()
        {
            VMUserMenu vmUserMenu;
            vmUserMenu = await Task.Run(() => _service.UserMenuGet());
            return View(vmUserMenu);
        }
        [HttpPost]
        public async Task<IActionResult> UserMenu(VMUserMenu model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.UserMenuAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.UserMenuEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.UserMenuDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("UserMenu");
        }


        #endregion

        #region User Accesslevel
        public async Task<IActionResult> UserAccessLevel()
        {
            VMUserAccessLevel vmUserAccessLevel;
            vmUserAccessLevel = await Task.Run(() => _service.UserAccessLevelGet());
            return View(vmUserAccessLevel);
        }
        [HttpPost]
        public async Task<IActionResult> UserAccessLevel(VMUserAccessLevel model)
        {
            model.User = "One";
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.UserAccessLevelAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.UserAccessLevelEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.UserAccessLevelDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("UserAccessLevel");
        }


        #endregion

        #region User Role
        public async Task<IActionResult> UserRole()
        {
            VMUserRole vmUserRole;
            vmUserRole = await Task.Run(() => _service.UserRoleGet());
            return View(vmUserRole);
        }
        [HttpPost]
        public async Task<IActionResult> UserRole(VMUserRole model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.UserRoleAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.UserRoleEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.UserRoleDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("UserRole");
        }


        #endregion

        #region User Submenu
        public async Task<IActionResult> UserSubMenu()
        {
            VMUserSubMenu vmUserSubMenu;
            vmUserSubMenu = await Task.Run(() => _service.UserSubMenuGet());
            return View(vmUserSubMenu);
        }
        [HttpPost]
        public async Task<IActionResult> UserSubMenu(VMUserSubMenu model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                await _service.UserSubMenuAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                await _service.UserSubMenuEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                await _service.UserSubMenuDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("UserSubMenu");
        }
        #endregion

        #region User Menuitem
        public async Task<IActionResult> UserMenuItem()
        {
            VMUserMenuItem vmUserMenuItem;
            vmUserMenuItem = await Task.Run(() => _service.UserMenuItemGet());
            return View(vmUserMenuItem);
        }
        [HttpPost]
        public async Task<IActionResult> UserMenuItem(VMUserMenuItem model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.UserMenuItemAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.UserMenuItemEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.UserMenuItemDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("UserMenuItem");
        }
        #endregion

        #region User Role Menuitem
        public async Task<IActionResult> UserRoleMenuItem(int id)
        {
            VMUserRoleMenuItem vmUserRoleMenuItem;
            vmUserRoleMenuItem = await Task.Run(() => _service.UserRoleMenuItemGet(id));
            return View(vmUserRoleMenuItem);
        }
        [HttpPost]
        public async Task<IActionResult> UserRoleMenuItem(VMUserRoleMenuItem model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.UserRoleMenuItemAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.UserRoleMenuItemEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.UserRoleMenuItemDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("UserRoleMenuItem");
        }

        public JsonResult SetUserMenuRole(int id, bool isAllowed)
        {
            VMUserRoleMenuItem model = new VMUserRoleMenuItem();
            model = _service.UserRoleMenuItemById(id);
            model.IsAllowed = isAllowed;
            model.User = "one";
            var v = _service.UserRoleMenuItemEdit(model);
            return Json(new { menuid = v.Result.userRoleMenuItemID, updatedstatus = v.Result.isAllowed });
        }

        public JsonResult GetUserSubMenu(int id)
        {
           
           var model = _service.GetUserSubMenu(id);
           
            return Json(model);
        }
        #endregion


        //public JsonResult GetUserById(int id)
        //{
        //    var model = _service.UserGetById(id);
        //    return Json(model);
        //}
    }
}