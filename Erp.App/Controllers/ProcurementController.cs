﻿using Erp.Core.Services;
using Erp.Core.Services.Home;
using Erp.Core.Services.Procurement;
using Erp.Core.Services.User;
using Erp.Infrastructure;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Erp.App.Controllers
{

    [SoftwareAdmin]
    public class ProcurementController : Controller
    {
        private readonly ILogger _logger;
        private readonly ProcurementService _service;
        private readonly HttpContext _httpContext;
        private readonly int _UserID;
        private readonly VMLogin _vMLogin;
        IDataProtector _protector;
        public ProcurementController(InfrastructureDbContext db, IHttpContextAccessor httpContextAccessor, ILogger<ProcurementController> logger, IDataProtectionProvider provider)
        {
            SessionHandler sessionHandler = new SessionHandler(db, httpContextAccessor.HttpContext);
            sessionHandler.Adjust();
            //_protector = provider.CreateProtector(GetType().FullName);
            _protector = provider.CreateProtector("OSS.ERP");
            _vMLogin = httpContextAccessor.HttpContext.Session.GetObjectFromJson<VMLogin>("LoginUser");
            _httpContext = httpContextAccessor.HttpContext;
            _UserID = _vMLogin.ID;
            _service = new ProcurementService(db, httpContextAccessor.HttpContext);
            _logger = logger;
        }

        #region Common
        public async Task<IActionResult> RawSubCategoryGet(int id)
        {
            if (id < 0) { return RedirectToAction("Error", "Home"); }

            var model = await Task.Run(() => _service.RawSubCategoryGet(id));



            var list = model.Select(x => new { Value = x.ID, Text = x.Name }).ToList();

            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + "RawSubCategoryGet(id), RawSubCategory Get by RawCategoryid).");
            return Json(list);
        }
        public async Task<IActionResult> RawItemGet(int id)
        {
            if (id < 0) { return RedirectToAction("Error", "Home"); }

            var model = await Task.Run(() => _service.RawItemGet(id));
            var list = model.Select(x => new { Value = x.ID, Text = x.Name }).ToList();

            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + "RawItemGet(id), RawItemGet Get by RawSubCategoryid).");
            return Json(list);
        }
        public async Task<IActionResult> RawItemInfoGet(int id)
        {
            if (id < 0) { return RedirectToAction("Error", "Home"); }

            var model = await Task.Run(() => _service.RawItemInfoGet(id));

            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + "RawItemInfoGet(id), RawItemInfo Get by RawItemid).");
            return Json(model);
        }
        #endregion

        #region Purchase_Requisition

        public async Task<IActionResult> CreatePR(int id = 0)
        {
            VMPurchaseRequisitionSlave model = new VMPurchaseRequisitionSlave();
            if (id == 0)
            {
                model = new VMPurchaseRequisitionSlave()
                {
                    Status = PRStatusEnum.Draft,

                    DataListSlave = new List<VMPurchaseRequisitionSlave>(),

                };
            }
            else if (id > 0)
            {
                model = await _service.PRWithSlaveListGet(new VMPurchaseRequisitionSlave() { Procurement_PurchaseRequisitionFK = id });

            }
            model.RawCategoryList = new SelectList(_service.RawCategoryDropDownList(), "Value", "Text");
            model.UserList = new SelectList(_service.UserDropDownList(), "Value", "Text");

            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + "PRWithSlaveListGet / RawCategoryDropDownList / EmployeeDropDownList, open Regular PR Create Page).");
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> CreatePR(VMPurchaseRequisitionSlave model)
        {
            #region session values
            string sUser = HttpContext.Session.GetString("User");
            string nUserId = HttpContext.Session.GetString("UserID");
            #endregion
            if (string.IsNullOrEmpty(sUser) || string.IsNullOrEmpty(nUserId)) { return RedirectToAction("Login", "Auth"); }
            model.User = sUser;
            model.UserID = int.Parse(nUserId);

            if (model.ActionEum == ActionEnum.Add)
            {
                if (model.Procurement_PurchaseRequisitionFK <= 0)
                {

                    model.Status = PRStatusEnum.Draft;

                    model.Procurement_PurchaseRequisitionFK = await _service.PurchaseRequisitionAdd(new VMPurchaseRequisition() { User = sUser, UserID = int.Parse(nUserId), Status = PRStatusEnum.Draft, PRType = PRTypeEnum.External, OriginType = ProcurementOriginTypeEnum.Regular });
                }

                //Add 
                await _service.PRSlaveAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.PRSlaveEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.PRSlaveDelete(model.ID);
            }
            else if ((int)model.ActionEum > 3 && model.Procurement_PurchaseRequisitionFK > 0)
            {
                //Delete
                await _service.PurchaseRequisitionStatusUpdate(new VMPurchaseRequisition() { User = sUser, ID = model.Procurement_PurchaseRequisitionFK, ActionId = (int)model.ActionEum, PRActionId = model.PRActionId, Description = model.Description });
            }
            else
            {
                _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " (PurchaseRequisitionAdd,PRSlaveAdd) / PRSlaveEdit / PRSlaveDelete / PurchaseRequisitionStatusUpdate, Regular PR Functionalities failed).");
                return RedirectToAction("Error", "Home");
            }
            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " (PurchaseRequisitionAdd,PRSlaveAdd) / PRSlaveEdit / PRSlaveDelete / PurchaseRequisitionStatusUpdate, Regular PR Functionalities).");
            return RedirectToAction("CreatePR", new { id = model.Procurement_PurchaseRequisitionFK });
        }

        public async Task<IActionResult> MerchandisingPR(int id = 0, int id2 = 0)
        {
            VMPurchaseRequisitionSlave model = new VMPurchaseRequisitionSlave();
            if (id == 0)
            {
                model = new VMPurchaseRequisitionSlave()
                {
                    Status = PRStatusEnum.Draft,
                    DataListSlave = new List<VMPurchaseRequisitionSlave>()
                };
            }
            else if (id > 0)
            {
                model = await _service.PRWithSlaveListGet(new VMPurchaseRequisitionSlave() { Procurement_PurchaseRequisitionFK = id });

            }
            model.OriginType = (ProcurementOriginTypeEnum)id2;
            model.ApprovedBOMStyleList = new SelectList(_service.ApprovedStyleList(model.Procurement_PurchaseRequisitionFK), "Value", "Text");
            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + "PRWithSlaveListGet / ApprovedStyleList, open Merchandising PR Create Page).");
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> MerchandisingPR(VMPurchaseRequisitionSlave model)
        {
            #region session values
            string sUser = HttpContext.Session.GetString("User");
            string nUserId = HttpContext.Session.GetString("UserID");
            #endregion
            if (string.IsNullOrEmpty(sUser) || string.IsNullOrEmpty(nUserId)) { return RedirectToAction("Login", "Auth"); }
            model.User = sUser;
            model.UserID = int.Parse(nUserId);

            if (model.ActionEum == ActionEnum.Add)
            {
                if (model.Procurement_PurchaseRequisitionFK <= 0)
                {

                    model.Status = PRStatusEnum.Draft;

                    model.Procurement_PurchaseRequisitionFK = await _service.PurchaseRequisitionAdd(new VMPurchaseRequisition() { User = sUser, UserID = int.Parse(nUserId), Status = PRStatusEnum.Draft, PRType = PRTypeEnum.External, OriginType = ProcurementOriginTypeEnum.Merchandising });
                }

                //Add 
                await _service.PRSlaveAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.PRSlaveEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.PRSlaveDelete(model.ID);
            }
            else if ((int)model.ActionEum > 3 && model.Procurement_PurchaseRequisitionFK > 0)
            {
                //Delete
                await _service.PurchaseRequisitionStatusUpdate(new VMPurchaseRequisition() { User = sUser, ID = model.Procurement_PurchaseRequisitionFK, ActionId = (int)model.ActionEum, PRActionId = model.PRActionId, Description = model.Description });
            }
            else
            {
                _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " (PurchaseRequisitionAdd,PRSlaveAdd) / PRSlaveEdit / PRSlaveDelete / PurchaseRequisitionStatusUpdate, Merchandising PR Functionalities failed).");
                return RedirectToAction("Error", "Home");
            }
            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " (PurchaseRequisitionAdd,PRSlaveAdd) / PRSlaveEdit / PRSlaveDelete / PurchaseRequisitionStatusUpdate, Merchandising PR Functionalities).");
            return RedirectToAction("MerchandisingPR", new { id = model.Procurement_PurchaseRequisitionFK, id2 = (int)model.OriginType });
        }
        

        [HttpGet]
        public IActionResult PRSlaveList(int id = 0, int PRID = 0, int typeID = 0, int orgID = 0)
        {
            var model = _service.PRSlaveFromStyleSlaveGet(id, PRID, typeID, orgID);
            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " PRSlaveFromStyleSlaveGet, Merchandising PR Item list partial).");
            return PartialView("_PRSlaveList", model);
        }

        [HttpPost]
        public async Task<IActionResult> PRSlaveList(VMPurchaseRequisitionSlave model)
        {
            #region session values
            string sUser = HttpContext.Session.GetString("User");
            string nUserId = HttpContext.Session.GetString("UserID");
            #endregion
            if (string.IsNullOrEmpty(sUser) || string.IsNullOrEmpty(nUserId)) { return RedirectToAction("Login", "Auth"); }
            model.User = sUser;
            model.UserID = int.Parse(nUserId);
            if (model.ActionEum == ActionEnum.Add)
            {
                model.DataListSlave = model.DataListSlave.Where(x => x.RequisitionQuantity > 0 && x.IsForSave).ToList();
                if (model.DataListSlave == null || model.DataListSlave.Count <= 0) { return RedirectToAction("MerchandisingPR", new { id = model.Procurement_PurchaseRequisitionFK, id2 = (int)model.OriginType }); }

                model.DataListSlave.ForEach(x => { x.User = sUser; x.UserID = int.Parse(nUserId); });

                var POID = await _service.PRSlaveListAdd(model.DataListSlave);
                POID = POID <= 0 ? model.Procurement_PurchaseRequisitionFK : POID;

                _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " PRSlaveListAdd, Merchandising PR Item list Save).");
                return RedirectToAction("MerchandisingPR", new { id = POID, id2 = (int)model.OriginType });
            }
            else
            {
                _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " PRSlaveListAdd, Merchandising PR Item list Save Failed).");
                return RedirectToAction("Error", "Home");
            }

        }

        public async Task<IActionResult> PurchaseRequisition(int id = 0)
        {
            if (id < 0) { return RedirectToAction("Error", "Home"); }


            var model = await Task.Run(() => _service.PurchaseRequisitionGet(new VMPurchaseRequisition() { Status = (PRStatusEnum)id }));



            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " PurchaseRequisitionGet, PR list Page (Status Wise)).");
            return View(model);
        }
        

        [HttpPost]
        public async Task<IActionResult> PurchaseRequisition(VMPurchaseRequisition model)
        {
            #region session values
            string sUser = HttpContext.Session.GetString("User");
            string nUserId = HttpContext.Session.GetString("UserID");
            #endregion
            if (string.IsNullOrEmpty(sUser) || string.IsNullOrEmpty(nUserId)) { return RedirectToAction("Login", "Auth"); }
            model.User = sUser;
            model.UserID = int.Parse(nUserId);

            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.PurchaseRequisitionAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.PurchaseRequisitionEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.PurchaseRequisitionDelete(model.ID);
            }
            else if (model.ActionId > 3)
            {
                //Delete
                await _service.PurchaseRequisitionStatusUpdate(model);
            }
            else
            {
                _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " PurchaseRequisitionAdd / PurchaseRequisitionEdit / PurchaseRequisitionDelete / PurchaseRequisitionStatusUpdate, PR functionalities failed).");
                return RedirectToAction("Error", "Home");
            }
            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " PurchaseRequisitionAdd / PurchaseRequisitionEdit / PurchaseRequisitionDelete / PurchaseRequisitionStatusUpdate, PR functionalities).");
            return RedirectToAction("PurchaseRequisition");
        }


        public async Task<IActionResult> PurchaseRequisitionDetails(int id = 0)
        {
            if (id <= 0) { RedirectToAction("Error", "Home"); }
            //get PR & detail info from Database
            var model = await Task.Run(() => _service.PRWithSlaveListGet(new VMPurchaseRequisitionSlave() { Procurement_PurchaseRequisitionFK = id }));


            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " PRWithSlaveListGet, PR Details Page).");
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> PurchaseRequisitionDetails(VMPurchaseRequisitionSlave model)
        {
            #region session values
            string sUser = HttpContext.Session.GetString("User");
            string nUserId = HttpContext.Session.GetString("UserID");
            #endregion
            if (string.IsNullOrEmpty(sUser) || string.IsNullOrEmpty(nUserId)) { return RedirectToAction("Login", "Auth"); }
            model.User = sUser;
            model.UserID = int.Parse(nUserId);

            

            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.PRSlaveAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.PRSlaveEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.PRSlaveDelete(model.ID);
            }
            else
            {
                _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " PRSlaveAdd / PRSlaveEdit / PRSlaveDelete, PR Slave functionalities failed).");
                return RedirectToAction("Error");
            }
            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " PRSlaveAdd / PRSlaveEdit / PRSlaveDelete, PR Slave functionalities).");
            return RedirectToAction("PurchaseRequisitionDetails", new { id = model.Procurement_PurchaseRequisitionFK });
        }


        #endregion

        #region Purchase_Order

        public async Task<IActionResult> PurchaseOrder(int id = 0)
        {
            if (id < 0) { return RedirectToAction("Error", "Home"); }
            var model = await Task.Run(() => _service.PurchaseOrderGet(new VMPurchaseOrder() { Status = (POStatusEnum)id }));
            if (model != null && model.DataList != null && model.DataList.Count > 0)
            {
                model.DataList.ForEach(x => { x.ProtectedPOID = _protector.Protect(x.ID.ToString()); });
            }
            model.CurrencyList = new SelectList(_service.CurrencyDropDownList(), "Value", "Text");
            model.SupplierList = new SelectList(_service.SupplierDropDownList(), "Value", "Text");
            model.UserList = new SelectList(_service.UserDropDownList(), "Value", "Text");
            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " PurchaseOrderGet / CurrencyDropDownList / EmployeeDropDownList / SupplierDropDownList, PO Page).");
            return View(model);
        }

        public async Task<IActionResult> PurchaseOrderSpecific(int id = 0)
        {
            if (id < 0) { RedirectToAction("Error", "Home"); }
            //get PR & detail info from Database
            VMPurchaseOrder model = new VMPurchaseOrder();
            if (id > 0)
            {
                model = await Task.Run(() => _service.PurchaseOrderGetSpecific(new VMPurchaseOrder() { ID = id }));
                model.ActionId = 2;
            }
            model.CurrencyList = new SelectList(_service.CurrencyDropDownList(), "Value", "Text");
            model.SupplierList = new SelectList(_service.SupplierDropDownList(), "Value", "Text");
            model.UserList = new SelectList(_service.UserDropDownList(), "Value", "Text");
            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " PurchaseOrderGetSpecific / CurrencyDropDownList / EmployeeDropDownList / SupplierDropDownList, Specific PO Get for partial).");
            return PartialView("_PurchaseOrderSpecific", model);
        }

        public async Task<IActionResult> OngoingPO(int id = 0)
        {
            if (id < 0) { return RedirectToAction("Error", "Home"); }


            var model = await Task.Run(() => _service.PurchaseOrderGet(new VMPurchaseOrder() { Status = (POStatusEnum)id }));
            if (model != null && model.DataList != null && model.DataList.Count > 0)
            {
                model.DataList.ForEach(x => { x.ProtectedPOID = _protector.Protect(x.ID.ToString()); });
            }
            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " PurchaseOrderGet, PO Get (status Wise)).");
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> POStatusUpdate(VMPurchaseOrder model)
        {
            #region session values
            string sUser = HttpContext.Session.GetString("User");
            string nUserId = HttpContext.Session.GetString("UserID");
            #endregion
            if (string.IsNullOrEmpty(sUser) || string.IsNullOrEmpty(nUserId)) { return RedirectToAction("Login", "Auth"); }
            model.User = sUser;
            model.UserID = int.Parse(nUserId);

            if (model.ActionId > 3)
            {
                //Delete
                await _service.PurchaseOrderStatusUpdate(model);
            }
            else
            {
                _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " PurchaseOrderStatusUpdate, PO status update failed).");
                return RedirectToAction("Error", "Home");
            }
            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " PurchaseOrderStatusUpdate, PO status update).");
            return RedirectToAction("OngoingPO", "Procurement", new { id = 0 });
        }

        [HttpPost]
        public async Task<IActionResult> PurchaseOrder(VMPurchaseOrder model)
        {
            #region session values
            string sUser = HttpContext.Session.GetString("User");
            string nUserId = HttpContext.Session.GetString("UserID");
            #endregion
            if (string.IsNullOrEmpty(sUser) || string.IsNullOrEmpty(nUserId)) { return RedirectToAction("Login", "Auth"); }
            model.User = sUser;
            model.UserID = int.Parse(nUserId);
            if (model.ActionEum == ActionEnum.Add)
            {
                model.Status = POStatusEnum.Draft;
                //Add 
                await _service.PurchaseOrderAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.PurchaseOrderEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.PurchaseOrderDelete(model.ID);
            }
            else if (model.ActionId > 3)
            {
                //Delete
                await _service.PurchaseOrderStatusUpdate(model);
            }
            else
            {
                _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " PurchaseOrderAdd / PurchaseOrderEdit / PurchaseOrderDelete / PurchaseOrderStatusUpdate, PO functionalities failed).");
                return RedirectToAction("Error", "Home");
            }
            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " PurchaseOrderAdd / PurchaseOrderEdit / PurchaseOrderDelete / PurchaseOrderStatusUpdate, PO functionalities).");
            return RedirectToAction("PurchaseOrder", "Procurement", new { id = 1 });
        }


        public async Task<IActionResult> PurchaseOrderDetails(int id = 0)
        {
            if (id <= 0) { RedirectToAction("Error", "Home"); }
            //get PR & detail info from Database
            VMPurchaseOrderSlave model = new VMPurchaseOrderSlave();

            model = await Task.Run(() => _service.POWithSlaveListGet(new VMPurchaseOrderSlave() { Procurement_PurchaseOrderFK = id }));

            if (model != null && model.Procurement_PurchaseOrderFK > 0)
            {
                model.ProtectedPOID = _protector.Protect(model.Procurement_PurchaseOrderFK.ToString());
            }

            var pritemlist =await _service.ApprovedPRItemList(model.Procurement_PurchaseOrderFK, (int)model.OriginType);
            if (pritemlist != null && pritemlist.Count > 0)
            {
                model.ApprovedPRList = new SelectList(pritemlist.GroupBy(x => new { x.Procurement_PurchaseRequisitionFK, x.CID }).Select(x => new { Value = x.Key.Procurement_PurchaseRequisitionFK, Text = x.Key.CID }).ToList(), "Value", "Text");
                model.ApprovedPRItemList = new SelectList(pritemlist.GroupBy(x => new { x.VMRawItemFK, x.VMRawItemName }).Select(x => new { Value = x.Key.VMRawItemFK, Text = x.Key.VMRawItemName }).ToList(), "Value", "Text");
            }
            


            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " POWithSlaveListGet / ApprovedPRList / ApprovedPRItemList, PO Details).");
            return View(model);
        }

        public async Task<IActionResult> PurchaseOrderDetailSpecific(int id = 0)
        {
            if (id < 0) { RedirectToAction("Error", "Home"); }
            //get PR & detail info from Database
            VMPurchaseOrderSlave model = new VMPurchaseOrderSlave();
            if (id > 0)
            {
                model = await Task.Run(() => _service.POSlaveGetSpecific(new VMPurchaseOrderSlave() { ID = id }));
                model.ActionId = 2;
            }

            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " POSlaveGetSpecific, PO Detail partial page).");
            return PartialView("_PurchaseOrderDetailSpecific", model);
        }



        [HttpPost]
        public async Task<IActionResult> PurchaseOrderDetails(VMPurchaseOrderSlave model)
        {
            #region session values
            string sUser = HttpContext.Session.GetString("User");
            string nUserId = HttpContext.Session.GetString("UserID");
            #endregion
            if (string.IsNullOrEmpty(sUser) || string.IsNullOrEmpty(nUserId)) { return RedirectToAction("Login", "Auth"); }
            model.User = sUser;
            model.UserID = int.Parse(nUserId);
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.POSlaveAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.POSlaveEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.POSlaveDelete(model.ID);
            }
            else
            {
                _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " POSlaveAdd / POSlaveEdit / POSlaveDelete, PO Detail functionalities failed).");
                return RedirectToAction("Error", "Home");
            }
            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " POSlaveAdd / POSlaveEdit / POSlaveDelete, PO Detail functionalities).");
            return RedirectToAction("PurchaseOrderDetails", new { id = model.Procurement_PurchaseOrderFK });
        }

        [HttpGet]
        public async Task<IActionResult> POSlaveList(int id = 0, int POId = 0)
        {
            var model = await _service.POSlaveFromPRSlaveGet(id, POId);
            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " POSlaveFromPRSlaveGet, PO Details list partial page).");
            return PartialView("_POSlaveList", model);
        }

        [HttpGet]
        public async Task<IActionResult> POSlaveItemList(int id = 0, int POId = 0)
        {
            var model = await _service.POSlaveFromPRSlaveItemGet(id, POId);
            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " POSlaveFromPRSlaveItemGet, PO Details list partial page).");
            return PartialView("_POSlaveList", model);
        }


        [HttpPost]
        public async Task<IActionResult> POSlaveList(VMPurchaseOrderSlave model)
        {
            #region session values
            string sUser = HttpContext.Session.GetString("User");
            string nUserId = HttpContext.Session.GetString("UserID");
            #endregion
            if (string.IsNullOrEmpty(sUser) || string.IsNullOrEmpty(nUserId)) { return RedirectToAction("Login", "Auth"); }
            model.User = sUser;
            model.UserID = int.Parse(nUserId);
            if (model.ActionEum == ActionEnum.Add)
            {
                model.DataListSlave = model.DataListSlave.Where(x => x.PurchaseQuantity > 0 && x.IsForSave).ToList();
                if (model.DataListSlave == null || model.DataListSlave.Count <= 0) { return RedirectToAction("PurchaseOrderDetails", new { id = model.Procurement_PurchaseOrderFK }); }

                model.DataListSlave.ForEach(x => { x.User = sUser; x.UserID = int.Parse(nUserId); });

                var POID = await _service.POSlaveListAdd(model.DataListSlave);
                _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " POSlaveListAdd, PO Details save).");
                return RedirectToAction("PurchaseOrderDetails", new { id = model.Procurement_PurchaseOrderFK });
            }
            else
            {
                _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " POSlaveListAdd, PO Details save failed).");
                return RedirectToAction("Error", "Home");
            }

        }
        #endregion

        #region Purchase_Invoice

        public async Task<IActionResult> PurchaseInvoice(int id = 0)
        {
            if (id < 0) { return RedirectToAction("Error", "Home"); }
            var model = await Task.Run(() => _service.PurchaseInvoiceGet(new VMPurchaseInvoice() { Status = (PurchaseInvoiceStatusEnum)id }));
            model.VMSupplierList = new SelectList(_service.SupplierDropDownList(), "Value", "Text");
            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " PurchaseInvoiceGet / SupplierDropDownList, PI list status wise).");
            return View(model);
        }

        public async Task<IActionResult> PurchaseInvoiceSpecific(int id = 0)
        {
            if (id < 0) { RedirectToAction("Error", "Home"); }
            //get PR & detail info from Database
            VMPurchaseInvoice model = new VMPurchaseInvoice();
            if (id > 0)
            {
                model = await Task.Run(() => _service.PurchaseInvoiceGetSpecific(new VMPurchaseInvoice() { ID = id }));
                model.ActionId = 2;
            }
            model.VMSupplierList = new SelectList(_service.SupplierDropDownList(), "Value", "Text");
            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " PurchaseInvoiceGetSpecific / SupplierDropDownList, PI specific partial page).");
            return PartialView("_PurchaseInvoiceSpecific", model);
        }

        public async Task<IActionResult> OngoingPurchaseInvoice(int id = 0)
        {
            if (id < 0) { return RedirectToAction("Error", "Home"); }

            var model = await Task.Run(() => _service.PurchaseInvoiceGet(new VMPurchaseInvoice() { Status = (PurchaseInvoiceStatusEnum)id }));

            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " PurchaseInvoiceGet, PI list status wise).");
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> PurchaseInvoiceStatusUpdate(VMPurchaseInvoice model)
        {
            #region session values
            string sUser = HttpContext.Session.GetString("User");
            string nUserId = HttpContext.Session.GetString("UserID");
            #endregion
            if (string.IsNullOrEmpty(sUser) || string.IsNullOrEmpty(nUserId)) { return RedirectToAction("Login", "Auth"); }
            
            int closedById = 0;
            int.TryParse(nUserId, out closedById);
            model.User = sUser;
            model.UserID = closedById;
            if (model.ActionId > 3)
            {
                //Delete
                await _service.PurchaseInvoiceStatusUpdate(model);
            }
            else
            {
                _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " PurchaseInvoiceStatusUpdate, PI status update failed).");
                return RedirectToAction("Error");
            }
            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " PurchaseInvoiceStatusUpdate, PI status update).");
            return RedirectToAction("OngoingPurchaseInvoice", "Procurement", new { id = 0 });
        }

        [HttpPost]
        public async Task<IActionResult> PurchaseInvoice(VMPurchaseInvoice model)
        {
            #region session values
            string sUser = HttpContext.Session.GetString("User");
            string nUserId = HttpContext.Session.GetString("UserID");
            #endregion
            if (string.IsNullOrEmpty(sUser) || string.IsNullOrEmpty(nUserId)) { return RedirectToAction("Login", "Auth"); }
            model.User = sUser;
            model.UserID = int.Parse(nUserId);
            if (model.ActionEum == ActionEnum.Add)
            {
                model.Status = PurchaseInvoiceStatusEnum.Draft;
                //Add 
                await _service.PurchaseInvoiceAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.PurchaseInvoiceEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.PurchaseInvoiceDelete(model.ID);
            }
            else if (model.ActionId > 3)
            {
                //Delete
                await _service.PurchaseInvoiceStatusUpdate(model);
            }
            else
            {
                _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " PurchaseInvoiceAdd / PurchaseInvoiceEdit / PurchaseInvoiceDelete / PurchaseInvoiceStatusUpdate, PI functionalities failed).");
                return RedirectToAction("Error");
            }
            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " PurchaseInvoiceAdd / PurchaseInvoiceEdit / PurchaseInvoiceDelete / PurchaseInvoiceStatusUpdate, PI functionalities).");
            return RedirectToAction("PurchaseInvoice");
        }

        public async Task<IActionResult> PurchaseInvoiceDetails(int id = 0)
        {
            if (id <= 0) { RedirectToAction("Error", "Home"); }
            //get PR & detail info from Database
            VMPurchaseInvoiceSlave model = new VMPurchaseInvoiceSlave();

            model = await Task.Run(() => _service.PurchaseInvoiceWithSlaveListGet(new VMPurchaseInvoiceSlave() { Procurement_PurchaseInvoiceFK = id }));
            var piitemlist = await _service.ApprovedSInItemList(model.VMSupplierFK, model.Procurement_PurchaseInvoiceFK,(int)model.OriginType);

            if (piitemlist != null && piitemlist.Count > 0)
            {
               // ApprovedSInItemList
                model.ApprovedSInList = new SelectList(piitemlist.GroupBy(x => new { x.Procurement_PurchaseOrderID, x.POID }).Select(x => new { Value = x.Key.Procurement_PurchaseOrderID, Text = x.Key.POID }).ToList(), "Value", "Text");
                model.ApprovedSInItemList = new SelectList(piitemlist.GroupBy(x => new { x.VMRawItemFK, x.VMRawItemName }).Select(x => new { Value = x.Key.VMRawItemFK, Text = x.Key.VMRawItemName }).ToList(), "Value", "Text");
            }
            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " PurchaseInvoiceWithSlaveListGet / ApprovedSInItemList, PI Details Page).");
            return View(model);
        }

        public async Task<IActionResult> PurchaseInvoiceDetailSpecific(int id = 0)
        {
            if (id < 0) { RedirectToAction("Error", "Home"); }
            //get PR & detail info from Database
            VMPurchaseInvoiceSlave model = new VMPurchaseInvoiceSlave();
            if (id > 0)
            {
                model = await Task.Run(() => _service.PurchaseInvoiceSlaveGetSpecific(new VMPurchaseInvoiceSlave() { ID = id }));
                model.ActionId = 2;
            }

            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " PurchaseInvoiceSlaveGetSpecific, PI Specific partial Page).");
            return PartialView("_PurchaseInvoiceDetailSpecific", model);
        }

        [HttpPost]
        public async Task<IActionResult> PurchaseInvoiceDetails(VMPurchaseInvoiceSlave model)
        {
            #region session values
            string sUser = HttpContext.Session.GetString("User");
            string nUserId = HttpContext.Session.GetString("UserID");
            #endregion
            if (string.IsNullOrEmpty(sUser) || string.IsNullOrEmpty(nUserId)) { return RedirectToAction("Login", "Auth"); }
            model.User = sUser;
            model.UserID = int.Parse(nUserId);
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.PurchaseInvoiceSlaveAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.PurchaseInvoiceSlaveEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.PurchaseInvoiceSlaveDelete(model.ID);
            }
            else
            {
                _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " PurchaseInvoiceSlaveAdd / PurchaseInvoiceSlaveEdit / PurchaseInvoiceSlaveDelete, PI Deatil functionalities failed).");
                return RedirectToAction("Error", "Home");
            }
            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " PurchaseInvoiceSlaveAdd / PurchaseInvoiceSlaveEdit / PurchaseInvoiceSlaveDelete, PI Deatil functionalities).");
            return RedirectToAction("PurchaseInvoiceDetails", new { id = model.Procurement_PurchaseInvoiceFK });
        }

        [HttpGet]
        public async Task<IActionResult> PurchaseInvoiceSlaveList(int id = 0, int PurchaseInvoiceId = 0)
        {
            var model = await _service.PurchaseInvoiceSlaveFromSInGet(id, PurchaseInvoiceId);
            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " PurchaseInvoiceSlaveFromSInGet, PI Deatil List partial page).");
            return PartialView("_PurchaseInvoiceSlaveList", model);
        }

        

        

        [HttpGet]
        public async Task<IActionResult> PurchaseInvoiceSlaveItemList(int id = 0, int PurchaseInvoiceId = 0)
        {
            var model = await _service.PurchaseInvoiceSlaveFromSInItemGet(id, PurchaseInvoiceId);
            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " PurchaseInvoiceSlaveFromSInItemGet, PI Deatil List partial page).");
            return PartialView("_PurchaseInvoiceSlaveList", model);
        }


        [HttpPost]
        public async Task<IActionResult> PurchaseInvoiceSlaveList(VMPurchaseInvoiceSlave model)
        {
            #region session values
            string sUser = HttpContext.Session.GetString("User");
            string nUserId = HttpContext.Session.GetString("UserID");
            #endregion
            if (string.IsNullOrEmpty(sUser) || string.IsNullOrEmpty(nUserId)) { return RedirectToAction("Login", "Auth"); }
            model.User = sUser;
            model.UserID = int.Parse(nUserId);
            if (model.ActionEum == ActionEnum.Add)
            {
                model.DataListSlave = model.DataListSlave.Where(x => x.InvoiceQuantity > 0 && x.IsForSave).ToList();
                if (model.DataListSlave == null || model.DataListSlave.Count <= 0) { return RedirectToAction("PurchaseInvoiceDetails", new { id = model.Procurement_PurchaseInvoiceFK }); }

                model.DataListSlave.ForEach(x => { x.User = sUser; x.UserID = int.Parse(nUserId); });

                var PurchaseInvoiceID = await _service.PurchaseInvoiceSlaveListAdd(model.DataListSlave);
                _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " PurchaseInvoiceSlaveListAdd, PI Deatil List save).");
                return RedirectToAction("PurchaseInvoiceDetails", new { id = model.Procurement_PurchaseInvoiceFK });
            }
            else
            {
                _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " PurchaseInvoiceSlaveListAdd, PI Deatil List save failed).");
                return RedirectToAction("Error", "Home");
            }
        }
        #endregion

        #region Procurement Invoice by Anis
        public async Task<IActionResult> ProcurementPurchaseInvoice()
        {
            VMPurchaseInvoiceSlave model = new VMPurchaseInvoiceSlave();
            var piitemlist = await _service.ApprovedSInItemListNew();
            if (piitemlist != null && piitemlist.Count > 0)
            {
                  model.ApprovedSInItemList = new SelectList(piitemlist.GroupBy(x => new { x.Procurement_PurchaseOrderID, x.POID }).Select(x => new { Value = x.Key.Procurement_PurchaseOrderID, Text = x.Key.POID }).ToList(), "Value", "Text");
            }
            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " PurchaseInvoiceGet / SupplierDropDownList, PI list status wise).");
            return View(model);
        }
        [HttpGet]
        public async Task<IActionResult> PurchaseInvoiceSlaveListPoWise(int id = 0)
        {
            var model = await _service.PurchaseInvoiceSlaveFromSInGet(id);
            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " PurchaseInvoiceSlaveFromSInGet, PI Deatil List partial page).");
            return PartialView("_PurchaseInvoiceSlaveListWithMaster", model);
        }

        [HttpPost]
        public async Task<IActionResult> PurchaseInvoiceSlaveListWithMaster(VMPurchaseInvoiceSlave model)
        {
            #region session values
            string sUser = HttpContext.Session.GetString("User");
            string nUserId = HttpContext.Session.GetString("UserID");
            #endregion
            if (string.IsNullOrEmpty(sUser) || string.IsNullOrEmpty(nUserId)) { return RedirectToAction("Login", "Auth"); }
            model.User = sUser;
            model.UserID = int.Parse(nUserId);
            if (model.ActionEum == ActionEnum.Add)
            {
                model.Status = PurchaseInvoiceStatusEnum.Draft;
                model.DataListSlave = model.DataListSlave.Where(x => x.InvoiceQuantity > 0 && x.IsForSave).ToList();
                model.TotalInvoiceValue = model.DataListSlave.Where(x=>x.InvoiceQuantity>0 && x.IsForSave).Sum(x => x.InvoiceQuantity * x.InvoicePrice);
                if (model.DataListSlave == null || model.DataListSlave.Count <= 0) { return RedirectToAction("ProcurementPurchaseInvoice", new { id = model.Procurement_PurchaseInvoiceFK }); }
                model.DataListSlave.ForEach(x => { x.User = sUser; x.UserID = int.Parse(nUserId); });

                var PurchaseInvoiceID = await _service.ProcurementMasterInvoicePush(model);
                _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " PurchaseInvoiceSlaveListAdd, PI Deatil List save).");
                return RedirectToAction("ProcurementPurchaseInvoice");
            }
            else
            {
                _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " PurchaseInvoiceSlaveListAdd, PI Deatil List save failed).");
                return RedirectToAction("Error", "Home");
            }
        }

        #endregion

    }
}