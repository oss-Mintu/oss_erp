﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Erp.Core.Services;
using Erp.Core.Services.MIS;
using Erp.Infrastructure;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Erp.Core.Services.User;

namespace Erp.App.Controllers
{
    [SoftwareAdmin]
    public class MisController : Controller
    {
        private readonly ILogger _logger;
        private readonly MisService _service;
        public MisController(InfrastructureDbContext db, ILogger<MisController> logger)
        {
            _service = new MisService(db);
            _logger = logger;
        }

        #region CM Earned
        public IActionResult InlineEditAndMultiAddition()
        {
            return View();
        }

        public async Task<IActionResult> InputCMEarned()
        {
            var model = await Task.Run(() => _service.CMEarnedGet());
            ViewBag.Year = new SelectList(_service.GetYear(), "Value", "Text");
            ViewBag.Month = new SelectList(_service.GetMonth(), "Value", "Text");
            return View(model);
        } 
        [HttpPost]
        public async Task<IActionResult> InputCMEarned(VMMis_CMEarned model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.CMEarnedAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.CMEarnedEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.CMEarnedDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("InputCMEarned");
        }
       

        #endregion

        #region Cash In Hand
        public IActionResult InputCashInHand()
        {
            var model = Task.Run(() => _service.CashInHandGet()).Result;
            ViewBag.Accounting_Head = new SelectList(_service.AccountingHeadDropDownList(), "Value", "Text");
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> InputCashInHand(VMMis_CashInHand model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.CashInHandAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.CashInHandEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.CashInHandDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("InputCashInHand");
        }
        

        #endregion

        #region Cash at Bank
        public IActionResult InputCashAtBank()
        {
            var model = Task.Run(() => _service.CashAtBankGet()).Result;
            ViewBag.Accounting_Head = new SelectList(_service.AccountingHeadDropDownList(), "Value", "Text");
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> InputCashAtBank(VMMis_CashAtBank model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.CashAtBankAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.CashAtBankEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.CashAtBankDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("InputCashAtBank");
        }
       
        #endregion

        #region Accounts Receivable Ledger
        public IActionResult InputAccReceivableLedger()
        {
            var model = Task.Run(() => _service.AccountsReceivableInputGet()).Result;
            ViewBag.CommonBuyer = new SelectList(_service.CommonBuyerDropDownList(), "Value", "Text");
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> InputAccReceivableLedger(VMMis_AccountsReceivable model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.MisAccountsReceivableAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.AccountsReceivableEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.AccountsReceivableDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("InputAccReceivableLedger");
        }
       

        #endregion

        #region Accounts Payable Ledger
        public IActionResult InputAccPayableLedger()
        {
            var model = Task.Run(() => _service.AccountsPayableInputGet()).Result;
            ViewBag.CommonSupplier = new SelectList(_service.CommonSupplierDropDownList(), "Value", "Text");
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> InputAccPayableLedger(VMMis_AccountsPayable model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.MisAccountsPayableAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.AccountsPayableEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.AccountsPayableDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("InputAccPayableLedger");
        }

        #endregion

        #region Order Confirm
        public IActionResult InputOrderConfirm()
        {
            var model = Task.Run(() => _service.OrderConfirmGet()).Result;
            ViewBag.CommonBuyer = new SelectList(_service.CommonBuyerDropDownList(), "Value", "Text");
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> InputOrderConfirm(VMMis_OrderConfirm model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.MisOrderConfirmAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.OrderConfirmEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.OrderConfirmDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("InputOrderConfirm");
        }
        
        #endregion

        #region Raw Materials
        public IActionResult InputRawMaterial()
        {
            var model = Task.Run(() => _service.RawMaterialsGet()).Result;
            ViewBag.RawCategory = new SelectList(_service.CommonRawCategoryDropDownList(), "Value", "Text");
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> InputRawMaterial(VMMis_RawMaterials model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.MisRawMaterialsAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.RawMaterialsEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.RawMaterialsDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("InputRawMaterial");
        }

        #endregion

        #region Balance Sheet
        public IActionResult BalanceSheet()
        {
            var model = Task.Run(() => _service.BalanceSheetGet()).Result;
            ViewBag.Accounting_Head = new SelectList(_service.AccountingHeadDropDownList(), "Value", "Text");
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> BalanceSheet(VMMis_BalanceSheet model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.MisBalanceSheetAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.BalanceSheetEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.BalanceSheetDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("BalanceSheet");
        }




        #endregion

        #region Income Statement
        public IActionResult IncomeStatement()
        {
            var model = Task.Run(() => _service.IncomeStatementGet()).Result;
            ViewBag.Accounting_Head = new SelectList(_service.AccountingHeadDropDownList(), "Value", "Text");
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> IncomeStatement(VMMis_IncomeStatement model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.IncomeStatementAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.IncomeStatementEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.IncomeStatementDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("IncomeStatement");
        }




        #endregion
        
        #region Shipment Status
        public IActionResult InputShipmentStatus()
        {
            var model = Task.Run(() => _service.ShipmentStatusGet()).Result;
            ViewBag.CommonBuyer = new SelectList(_service.CommonBuyerDropDownList(), "Value", "Text");
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> InputShipmentStatus(VMMis_ShipmentStatus model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.MisShipmentStatusAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.ShipmentStatusEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.ShipmentStatusDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("InputShipmentStatus");
        }
       
        #endregion

        #region Daily Production
        public IActionResult InputDailyProduction()
        {
            var model = Task.Run(() => _service.DailyProductionGet()).Result;
            ViewBag.CommonOrder = new SelectList(_service.CommonOrderDropDownList(), "Value", "Text");
            ViewBag.CommonBuyer = new SelectList(_service.CommonBuyerDropDownList(), "Value", "Text");

            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> InputDailyProduction(VMMis_DailyProduction model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.MisDailyProductionAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.DailyProductionEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.DailyProductionDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("InputDailyProduction");
        }
       
        #endregion

        #region Attendance Report
        public IActionResult InputAttendanceRecorded()
        {
            var model = Task.Run(() => _service.DailyAttendanceGet()).Result;
            ViewBag.CommonDepartment = new SelectList(_service.GetCommonDepartment(), "Value", "Text");
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> InputAttendanceRecorded(VMMis_DailyAttendance model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.MisDailyAttendanceAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.DailyAttendanceEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.DailyAttendanceDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("InputAttendanceRecorded");
        }
       
        #endregion

        #region BTB Status
        public IActionResult InputBTBStatus()
        {
            var model = Task.Run(() => _service.BTBLCStatusGet()).Result;
            //ViewBag.AccountTypeList = new SelectList(_service.AccountTypeDropDownList(), "Value", "Text");
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> InputBTBStatus(VMMis_BTBLCStatus model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.MisBTBLCStatusAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.BTBLCStatusEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.BTBLCStatusDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("InputBTBStatus");
        }

        #endregion

        #region BTB Status
        public IActionResult FinancialStatus()
        {
            var model = Task.Run(() => _service.FinancialStatusGet()).Result;
            ViewBag.Month = new SelectList(_service.GetMonth(), "Value", "Text");
            ViewBag.Year = new SelectList(_service.GetYear(), "Value", "Text");
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> FinancialStatus(VMMis_FinancialStatus model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.FinancialStatusAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.FinancialStatusEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.FinancialStatusDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("FinancialStatus");
        }

        #endregion



    }






}
