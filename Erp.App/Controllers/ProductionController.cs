﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Erp.Core.Services;
using Erp.Core.Services.Home;
using Erp.Core.Services.Integration;
using Erp.Core.Services.Production;
using Erp.Core.Services.User;
using Erp.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;

namespace Erp.App.Controllers
{
    [SoftwareAdmin]
    public class ProductionController : Controller
    {
        private readonly ILogger _logger;
        private readonly VMLogin _vMLogin;
        private readonly ProductionService _service;
        private readonly IntegrationService _integrationService;
        private int _userid;
        public ProductionController(InfrastructureDbContext db, IHttpContextAccessor httpContextAccessor, ILogger<ProductionService> logger)
        {
            _service = new ProductionService(db, httpContextAccessor.HttpContext);
            _logger = logger;
            SessionHandler sessionHandler = new SessionHandler(db, httpContextAccessor.HttpContext);
            _userid = Convert.ToInt32(httpContextAccessor.HttpContext.Session.GetString("UserID"));
            _vMLogin = httpContextAccessor.HttpContext.Session.GetObjectFromJson<VMLogin>("LoginUser");
            _integrationService= new IntegrationService(db, httpContextAccessor.HttpContext);
        }

        public IActionResult Index()
        {
            return View();
        }

        #region ProductionReference

        public IActionResult ProductionReference()
        {
            if (_vMLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            var model = Task.Run(() => _service.ProdReferenceGet()).Result;
            model.ReferenceDate = DateTime.Today;
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> ProductionReference(VMProductionReference model)
        {
            if (_vMLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            model.UserID = _vMLogin.ID;
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                model.ID = await _service.ProdReferenceAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.ProdReferenceEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.ProdReferenceDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("ProductionReference");
        }

        public IActionResult ProductionStep(int ID)
        {
            if (_vMLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            var model = Task.Run(() => _service.ProdReferenceGetByID(ID)).Result;
            return View(model);
        }

        #endregion

        #region ProductionStep

        public IActionResult CuttingStep(int rid)
        {
            var model = Task.Run(() => _service.CuttingProductionGetByRefID(rid)).Result;
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> CuttingStep(VMProductionReference model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.CuttingStyleAdd(model.vmDailyProduction);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Add 
                await _service.CuttingStyleEdit(model.vmDailyProduction);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("CuttingStep", new { rid = model.vmDailyProduction.Prod_ReferenceFK });
        }

        public async Task<IActionResult> CuttingPlanDelete(VMProductionReference model)
        {
            await Task.Run(() => _service.GetDeletePlanOrderWithoutPlan(model.vmDailyProduction.Prod_ReferencePlanFK));
            return RedirectToAction("CuttingStep", "Production", new { rid = model.vmDailyProduction.Prod_ReferenceFK });
        }

        public IActionResult SewingStep(int rid)
        {
            var model = Task.Run(() => _service.SewingProductionGetByRefID(rid)).Result;
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> SewingStep(VMProductionReference model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.SewingStyleAdd(model.vmDailyProduction);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Add 
                await _service.SewingStyleEdit(model.vmDailyProduction);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("SewingStep", new { rid = model.vmDailyProduction.Prod_ReferenceFK });
        }

        public IActionResult IroningStep(int rid)
        {
            var model = Task.Run(() => _service.IroningProductionGetByRefID(rid)).Result;
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> IroningStep(VMProductionReference model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.IroningStyleAdd(model.vmDailyProduction);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("IroningStep", new { rid = model.vmDailyProduction.Prod_ReferenceFK });
        }

        public IActionResult PackingStep(int rid)
        {
            var model = Task.Run(() => _service.PackingProductionGetByRefID(rid)).Result;
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> PackingStep(VMProductionReference model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.PackingStyleAdd(model.vmDailyProduction);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("PackingStep", new { rid = model.vmDailyProduction.Prod_ReferenceFK });
        }

        public IActionResult ProductionForm(int rid, int modeid, int sectionid)
        {
            return ViewComponent("ProductionUI", new { RID = rid, ModeID = modeid, SectionID = sectionid });
        }

        #endregion

        [HttpGet]
        public async Task<IActionResult> AddSewingPreviousPlan(int rid)
        {
            await Task.Run(() => _service.AddPreviousSewingPlanOrder(rid));
            return RedirectToAction("SewingStep", new { rid = rid });
        }

        public async Task<IActionResult> SewingPlanDelete(VMProductionReference model)
        {
            await Task.Run(() => _service.GetDeletePlanOrderWithoutPlan(model.vmDailyProduction.Prod_ReferencePlanFK));
            return RedirectToAction("SewingStep", "Production", new { rid = model.vmDailyProduction.Prod_ReferenceFK });
        }
        
        #region ProductionFollowup

        public IActionResult CuttingFollowup(int rid)
        {
            ViewBag.StyleList = new SelectList(_service.DDLStyleForCuttingFollowup(rid), "Value", "Text");
            //ViewBag.StyleList = new SelectList(_service.DDLStyleFollowUp(rid,1), "Value", "Text");
            var model = Task.Run(() => _service.CuttingFollowupGetByRefID(rid)).Result;
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> CuttingFollowup(VMProductionReference model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.CuttingFollowUpStyleAdd(model.vmDailyProduction);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("CuttingFollowup", new { rid = model.ID });
        }

        public IActionResult SewingFollowup(int rid)
        {
            ViewBag.StyleList = new SelectList(_service.DDLSewingPlanOrder(rid), "Value", "Text");
            var model = Task.Run(() => _service.SewingFollowupGetByRefID(rid)).Result;
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> SewingFollowup(VMProductionReference model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.SewingFollowUpStyleAdd(model.vmDailyProduction);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("SewingFollowup", new { rid = model.ID });
        }

        public IActionResult DailyAchievement(int id)
        {
            ViewBag.ProductionUnit = new SelectList(_service.DDProductionUnit(), "Value", "Text");
            ViewBag.ProductionFloor = new SelectList(_service.DDLProductionFloor(), "Value", "Text");
            ViewBag.RemarkList = new SelectList(_service.DDLRemarks(), "Value", "Text");
            VMProductionReport model = new VMProductionReport();
            model.ProdReferenceId = id;
            model.DataList = new List<VMProductionReport>();
            if (id > 0)
            {
                model = _service.GetTargetAchivementReport(model);
            }
            return View(model);
        }

        [HttpPost]
        public IActionResult DailyAchievement(VMProductionReport model)
        {
            ViewBag.ProductionUnit = new SelectList(_service.DDProductionUnit(), "Value", "Text");
            ViewBag.ProductionFloor = new SelectList(_service.DDLProductionFloor(), "Value", "Text");
            ViewBag.RemarkList = new SelectList(_service.DDLRemarks(), "Value", "Text");
            model = _service.GetTargetAchivementReport(model);
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> AddAchievement(VMProductionReport model)
        {
            ViewBag.ProductionUnit = new SelectList(_service.DDProductionUnit(), "Value", "Text");
            ViewBag.ProductionFloor = new SelectList(_service.DDLProductionFloor(), "Value", "Text");
            ViewBag.RemarkList = new SelectList(_service.DDLRemarks(), "Value", "Text");
            await _service.AchivementAdd(model);
            return RedirectToAction("DailyAchievement", new { id = model.ProdReferenceId });
        }

        public IActionResult IroningFollowup(int rid)
        {
            ViewBag.StyleList = new SelectList(_service.DDLStyleForFollowupIroning(rid), "Value", "Text");
            var model = Task.Run(() => _service.IroningFollowupGetByRefID(rid)).Result;
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> IroningFollowup(VMProductionReference model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.IroningFollowUpStyleAdd(model.vmDailyProduction);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("IroningFollowup", new { rid = model.ID });
        }

        public IActionResult PackingFollowup(int rid)
        {
            ViewBag.StyleList = new SelectList(_service.DDLStyleForFollowupPacking(rid), "Value", "Text");
            var model = Task.Run(() => _service.PackingFollowupGetByRefID(rid)).Result;
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> PackingFollowup(VMProductionReference model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.PackingFollowUpStyleAdd(model.vmDailyProduction);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("PackingFollowup", new { rid = model.ID });
        }

        public async Task<JsonResult> FollowupUpdate(int Id, int Qty)
        {
            VMDailyProduction model = new VMDailyProduction();
            model.Prod_ReferencePlanFollowupFK = Id;
            model.DoneQty = Qty;
            int TQty = 0;
            TQty = await _service.FollowUpDetailsAdd(model);
            var jsonData = new
            {
                qty = TQty,
                result = 1
            };

            return Json(jsonData);
        }

        public async Task<IActionResult> DeleteZeroFollowup(int Id,int SId)
        {
            if (SId == 1)
            {
                await _service.DeleteZeroFollowUp(Id, SId);
                return RedirectToAction("CuttingFollowup", new { rid = Id });
            }
            else if (SId == 2)
            {
                await _service.DeleteZeroFollowUp(Id, SId);
                return RedirectToAction("SewingFollowup", new { rid = Id });
            }
            else if (SId == 3)
            {
                await _service.DeleteZeroFollowUp(Id, SId);
                return RedirectToAction("IroningFollowup", new { rid = Id });
            }
            else if (SId == 4)
            {
                await _service.DeleteZeroFollowUp(Id, SId);
                return RedirectToAction("PackingFollowup", new { rid = Id });
            }
            else
            {
                return RedirectToAction("Error");
            }
        }

        #endregion

        #region ProductionPrePlanning

        public IActionResult OrderPrePlan(int? ID)
        {
            ViewBag.StyleList = new SelectList(_service.DDLStyle(), "Value", "Text");
            var model = new VMPlanView();
            model.vmSMVLayout = new VMSMVLayout();
            model.vmOrderPlanning = new VMOrderPlanning();
            model.vmOrderPlanning.DataList = new List<VMOrderPlanning>();
            model.vmOrderPlanning.PlanList = new List<VMOrderPlanning>();
            if (ID != null)
            {
                model = Task.Run(() => _service.PlanGetByStyleID(model.ID)).Result;
                model.ID = ID.Value;
            }
            return View(model);
        }

        [HttpPost]
        public IActionResult OrderPrePlan(VMPlanView model)
        {
            ViewBag.StyleList = new SelectList(_service.DDLStyle(), "Value", "Text");
            model = Task.Run(() => _service.PlanGetByStyleID(model.ID)).Result;
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> PlanSMV(VMPlanView model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.PlanSMVAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                //await _service.PlanSMVEdit(model.vmSMVLayout);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                //await _service.PlanSMVDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("OrderPrePlan", model.ID);
        }

        [HttpPost]
        public async Task<IActionResult> StylePrePlan(VMPlanView model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.StylePrePlanAdd(model.ListOrderPlan);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                //await _service.StylePrePlanEdit(model.vmOrderPlanning);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                //await _service.StylePrePlanDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("OrderPrePlan", model.ID);
        }

        public IActionResult PreLinePlan(int Id,int SId)
        {
            ViewBag.SMVList = new SelectList(_service.DDLSMVByStyleID(SId,false), "Value", "Text");
            ViewBag.LineList = new MultiSelectList(_service.DDLSewingLine(), "Value", "Text");
            var model = Task.Run(() => _service.StylePrePlanByID(Id)).Result;
            return PartialView(model);
        }

        [HttpPost]
        public async Task<IActionResult> PreLinePlan(VMShipmentProductionPlan model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.PreLinePlanAdd(model.ListLinePlan);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.LinePrePlanEdit(model);
            }
            //else if (model.ActionEum == ActionEnum.Delete)
            //{
            //    //Delete
            //    await _service.PlanSMVDelete(model.ID);
            //}
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("PreLinePlan", new { Id = model.ID, SId = model.Merchandising_StyleFK });
        }

        public IActionResult GetAutoLinePrePlan(int ID, int SID, List<int> LID)
        {
            var model = new VMShipmentProductionPlan();
            model.ListLinePlan = _service.GetAutoLinePrePlan2(ID, SID, LID);

            return PartialView("_LinePlan", model);
        }
        #endregion

        #region SMVLayout

        #region StylePreSMVLayout

        public IActionResult StylePreSMV(int ID)
        {
            var model = _service.GetStylePreSMV(ID);
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> StylePreSMV(VMPlanView model)
        {
            if (model.vmSMVLayout.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.PlanSMVAdd(model);
            }
            else if (model.vmSMVLayout.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.PlanSMVEdit(model);
            }
            else if (model.vmSMVLayout.ActionEum == ActionEnum.Delete)
            {
                //Delete
                //await _service.PlanSMVDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("StylePreSMV", model.vmSMVLayout.Merchandising_StyleFk);
        }

        #endregion

        #region StyleMasterSMVLayout
        public IActionResult StyleMasterSMV()
        {
            var model = _service.IndexMasterSMV();
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> StyleMasterSMV(VMPlanView model)
        {
            if (model.vmSMVLayout.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.SMVMasterLayoutEdit(model.vmSMVLayout);
            }
            return RedirectToAction("StyleMasterSMV");
        }

        [HttpPost]
        public async Task<IActionResult> SMVMasterLayoutDelete(VMPlanView model)
        {
            if (model.vmSMVLayout.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.SMVMasterLayoutDelete(model.vmSMVLayout.ID);
            }
            return RedirectToAction("StyleMasterSMV");
        }

        #endregion

        public IActionResult UpdateSMV(int Id, int styleId, int typeID)
        {
            return ViewComponent("SMVLayout", new { ID = Id, StyleId = styleId, TypeID = typeID });
        }

        #endregion

        #region ShipmentPrePlan

        public IActionResult ShipmentPrePlan()
        {
            ViewBag.StyleList = new SelectList(_service.DDLStyle(), "Value", "Text");
            var model = new VMPlanView();
            model = _service.GetPrePlan();
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> ShipmentPrePlan(VMPlanView model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.StylePrePlanAdd(model.ListOrderPlan);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                //await _service.StylePrePlanEdit(model.vmOrderPlanning);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.StylePrePlanDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("ShipmentPrePlan");
        }
        #endregion

        #region ShipmentExecutionPlan

        public IActionResult OrderPlan()
        {
            var model = new VMPlanView();
            model = _service.GetMasterPlan();
            //var model = Task.Run(() => _service.GetMasterPlan());
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> OrderPlan(VMPlanView model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.ExecutionPlanAdd(model.ListOrderPlan);
            }
            //else if (model.ActionEum == ActionEnum.Edit)
            //{
            //    //Edit
            //    //await _service.OrderEdit(model);
            //}
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.ExecutionPlanDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("OrderPlan");
        }

        public IActionResult LinePlan(int Id, int SId)
        {
            ViewBag.SMVList = new SelectList(_service.DDLSMVByStyleID(SId,true), "Value", "Text");
            ViewBag.LineList = new MultiSelectList(_service.DDLSewingLine(), "Value", "Text");
            var model = Task.Run(() => _service.StylePlanByID(Id)).Result;
            return PartialView(model);
        }

        [HttpPost]
        public async Task<IActionResult> LinePlan(VMShipmentProductionPlan model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                //Edit
                await _service.LineMasterPlanEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.LineMasterPlanEdit(model);
            }
            //else if (model.ActionEum == ActionEnum.Delete)
            //{
            //    //Delete
            //    await _service.PlanSMVDelete(model.ID);
            //}
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("LinePlan", new { Id = model.ID, SId = model.Merchandising_StyleFK });
        }

        public JsonResult LineStatusChange(int Id, bool status)
        {
            string str = string.Empty;
            str = _service.ChangeLineStatus(Id, status).Result;
            return Json(str);
        }

        #endregion

        #region ManageProductionLine
        public IActionResult ProductionLine()
        {
            ViewBag.OrderProcess = new SelectList(_service.DDLProductionStep(), "Value", "Text");
            ViewBag.SectionLine = new SelectList(_service.DDLSectionLine(), "Value", "Text");
            var model = _service.ProductionLineGet();
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> ProductionLine(VMProductionLine model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.ProductionLineAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.ProductionLineEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.ProductionLineDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("ProductionLine");
        }
        #endregion

        #region PlanSetting

        public IActionResult PlanConfig()
        {
            var model = Task.Run(() => _service.GetPlanConfiguration()).Result;
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> PlanConfig(VMPlanConfig model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.PlanConfigUpdate(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.PlanConfigUpdate(model);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return View(model);
        }
        #endregion

        #region LineChiefLine
        public IActionResult LineChiefLine()
        {
            ViewBag.LineChiefORSV= new SelectList(_service.DDLLineMan(), "Value", "Text");
            ViewBag.ProductionLine = new SelectList(_service.DDLProductionLineDropDownList(), "Value", "Text");
            var model = "";
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> LineChiefLine(VMLineChiefLine model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.LineChiefLineAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.LineChiefLineEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.LineChiefLineDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("LineChiefLine");
        }
        #endregion

        #region ProductionLineDevice
        public IActionResult LineDevice()
        {
            
            var model = Task.Run(() => _service.LineDeviceGet()).Result;
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> LineDevice(VMLineDevice model)
        {
            ViewBag.LineList = new SelectList(_service.DDLDeviceLine(), "Value", "Text");
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.LineDeviceAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.LineDeviceEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.LineDeviceDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("LineDevice");
        }

        public IActionResult LineDeviceUpdateEdit(int id)
        {
            return ViewComponent("QCUI", new { ID = id, UINo = 1 });
        }

        #endregion

        #region QCType
        public IActionResult QCType()
        {
            if (_vMLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            var model = Task.Run(() => _service.QCTypeGet()).Result;
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> QCType(VMQCType model)
        {
            if (_vMLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            model.UserID = _vMLogin.ID;
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.QCTypeAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.QCTypeEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.QCTypeDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("QCType");
        }

        public IActionResult QCTypeEdit(int id)
        {
            return ViewComponent("QCUI", new { ID = id, UINo = 2 });
        }
        #endregion

        #region OrderQC

        public IActionResult QCView()
        {
            if (_vMLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            var model = Task.Run(() => _service.GetStyle()).Result;
            return View(model);
        }

        public IActionResult OrderQC(int id)
        {
            var model = Task.Run(() => _service.StyleQCGetByStyleID(id)).Result;
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> OrderQC(VMOrderQC model)
        {
            if (_vMLogin == null)
            {
                return RedirectToAction("Login", "Auth");
            }
            model.UserID = _vMLogin.ID;
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.OrderQCAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.OrderQCEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.OrderQCDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("OrderQC", new {id= model.Merchandising_StyleFK });
        }

        public IActionResult OrderQCEdit(int id,int sid)
        {
            return ViewComponent("QCUI", new { ID = id, UINo = 3,StyleID=sid });
        }
        #endregion

        #region ManageLineChief
        public IActionResult LineChief()
        {
            var model = Task.Run(() => _service.GetLineChief()).Result;
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> LineChief(VMLineChief model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.LineChiefAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.LineChiefEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.LineChiefDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("LineChief");
        }

        #endregion

        #region ManageSuperVisor

        public IActionResult LineSuperVisor()
        {
            var model = Task.Run(() => _service.GetLineSuperVisor()).Result;
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> LineSuperVisor(VMLineSuperVisor model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.LineSuperVisorAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.LineSuperVisorEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.LineSuperVisorDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("LineSuperVisor");
        }

        #endregion

        #region Production Floor Unit
        public IActionResult SectionLine()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> SectionLine(VMSectionLine model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.SectionLineAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.SectionLineEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.SectionLineDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("SectionLine");
        }

        #endregion

        #region ManageLineChief
        public IActionResult ProductionLineChief(int ID)
        {
            ViewBag.LineList = new SelectList(_service.DDLProductionLineDropDownList(), "Value", "Text");
            var model = _service.GetLineChiefLine(ID);
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> ProductionLineChief(VMLineChiefLine model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                model.ID = await _service.LineChiefLineAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.LineChiefLineEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.LineChiefLineDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("ProductionLineChief");
        }

        #endregion

        #region ManageSupervisorLine
        public IActionResult ProductionSupervisorLine(int ID)
        {
            ViewBag.LineList = new SelectList(_service.DDLProductionLineDropDownList(), "Value", "Text");
            var model = _service.GetLineSuperVisorForProLine(ID);
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> ProductionSupervisorLine(VMLineChiefLine model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                model.ID = await _service.SupervisorLineAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.SupervisorLineEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.SupervisorLineDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("ProductionSupervisorLine");
        }

        #endregion

        #region Manage Production Unit
        public IActionResult ProductionUnit()
        {
            ViewBag.BusinessUnitList = new SelectList(_service.DDLUnit(), "Value", "Text");
            var model = Task.Run(() => _service.GetProductionUnit()).Result;
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> ProductionUnit(VMProductionUnit model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                model.ID = await _service.ProductionUnitAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.ProductionUnitEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.ProductionUnitDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("ProductionUnit");
        }
        #endregion

        #region Manage Production Floor
        public IActionResult ProductionFloor()
        {
            ViewBag.ProductionUnitList = new SelectList(_service.DDProductionUnit(), "Value", "Text");
            var model = Task.Run(() => _service.GetProductionFloor()).Result;
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> ProductionFloor(VMProductionFloor model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                model.ID = await _service.ProductionFloorAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.ProductionFloorEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.ProductionFloorDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("ProductionFloor");
        }
        #endregion

        #region TimeAndAction
        public IActionResult OrderAction()
        {
            var model = _service.OrderActionGet().Result;
            return View(model);
        }

        [HttpPost] 
        public async Task<IActionResult> OrderAction(VMOrderAction model)
        {
            model.UserID = _userid;
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                model.ID = await _service.OrderActionAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.OrderActionEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.OrderActionDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("OrderAction");
        }

        public IActionResult OrderActionStep(int id)
        {
            var model = _service.OrderActionStepGetByActionID(id);
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> OrderActionStep(VMOrderActionStep model)
        {
            model.UserID = _userid;
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                model.ID = await _service.OrderActionStepAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.OrderActionStepEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.OrderActionStepDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("OrderActionStep",new { id=model.Plan_OrderActionFK});
        }

        public IActionResult OrderPlanProcess()
        {
            ViewBag.StyleList = new SelectList(_service.DDLStyle(), "Value", "Text");
            VMOrderStep model = new VMOrderStep();
            model.StepList = new List<VMOrderProcess>();
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> OrderPlanProcess(VMOrderStep model)
        {
            model.UserID = _userid;
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                model.ID = await _service.OrderProcessAdd(model);
            }
            //else if (model.ActionEum == ActionEnum.Edit)
            //{
            //    //Edit
            //    await _service.OrderActionEdit(model);
            //}
            //else if (model.ActionEum == ActionEnum.Delete)
            //{
            //    //Delete
            //    await _service.OrderActionDelete(model.ID);
            //}
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("OrderPlanProcess");
        }

        public IActionResult GetOrderStep(int sid)
        {
            //VMOrderStep model = new VMOrderStep();
            //model.StepList = new List<VMOrderProcess>();
            var model = _service.GetActionStep(sid);
            model.SaveDataList = new List<VMOrderProcess>();
            model.SaveDataList = _service.GetAllStepByStyleID(sid);
            return PartialView("_OrderStep", model);
        }

        public async Task<IActionResult> OrderStepUpdate(VMOrderStep model)
        {
            model.UserID = _userid;
            model.ID = await _service.OrderProcessEdit(model);
            return RedirectToAction("OrderPlanProcess");
        }

        public IActionResult OrderStepReport()
        {
            ViewBag.StyleList = new SelectList(_service.DDLStyle(), "Value", "Text");
            VMOrderStepReport model = new VMOrderStepReport();
            model.DataList = new List<VMOrderStepReport>();
            return View(model);
        }
        [HttpPost]
        public IActionResult OrderStepReport(VMOrderStepReport model)
        {
            //ViewBag.StyleList = new SelectList(_service.DDLStyle(), "Value", "Text");
            //model.DataList = _service.GetGraphOrderPlan(model.ID);
            //return View(model);
            ViewBag.StyleList = new SelectList(_service.DDLStyle(), "Value", "Text");
            model.DataList = _service.GetGraphOrderActionPlan(model.ID);
            return View(model);
        }
        #endregion

        #region ComponentView
        public IActionResult SectionEdit(int ID)
        {
            return ViewComponent("SectionLine", new { ID = ID, view = "" });
        }
        public IActionResult RemarksEdit(int id)
        {
            return ViewComponent("Remarks", new { ID = id, view = "" });
        }

        public IActionResult GetUpdateForm(int id,int fid, string vn)
        {
            return ViewComponent("TimeAction", new { ID = id, FID=fid, ViewName = vn });
        }
        #endregion

        #region JsonResult
        public JsonResult GetAllShipmentInfo(int BomId)
        {
            if (BomId != 0)
            {
                var list = _service.DDLShipmentByBPOStyleID(BomId);
                return Json(list);
            }
            return null;
        }

        public JsonResult GetAvailableLine(int ID)
        {
            if (ID != 0)
            {
                var list = _service.DDLSewingLine();
                return Json(list);
            }
            return null;
        }

        public JsonResult GetPlanSMV(int Id)
        {
            if (Id != 0)
            {
                var data = _service.GetPlanSMVByID(Id);
                return Json(data);
            }
            return null;
        }

        public JsonResult GetCuttingStyleColor(int StyleID, int RefID)
        {
            if (StyleID != 0 && RefID != 0)
            {
                var data = _service.DDLCuttingStyleColor(StyleID, RefID);
                return Json(data);
            }
            return null;
        }

        public JsonResult GetCuttingPlan(int RefID, int StyleID, int ColorID)
        {
            if (StyleID != 0 && RefID != 0 && ColorID != 0)
            {
                var data = _service.GetCuttingPlanInfo(RefID, StyleID, ColorID);
                return Json(data);
            }
            return null;
        }

        public JsonResult GetLineChiefOrSV(int ID)
        {
            if (ID == 1)
            {
                var list = _service.DDLLineChiefDropDownList();
                return Json(list);
            }
            else if (ID == 2)
            {
                var list = _service.DDLLineSuperVisorDropDownList();
                return Json(list);
            }
            return null;
        }

        #endregion

        #region PlanningReport

        public IActionResult PlanReport()
        {
            ViewBag.PlanType = new SelectList(_service.DDLPlanType(), "Value", "Text");
            VMOrderPlanning model = new VMOrderPlanning();
            model.DataList = new List<VMOrderPlanning>();
            model.SewingStartDate = DateTime.Today;
            model.ShipmentDate = DateTime.Today.AddMonths(1);
            return View(model);
        }
        [HttpPost]
        public IActionResult PlanReport(VMOrderPlanning model)
        {
            ViewBag.PlanType = new SelectList(_service.DDLPlanType(), "Value", "Text");
            model.DataList = _service.GetPlanReport(model);
            return View(model);
        }

        public IActionResult PlanScheduleChart()
        {
            ViewBag.PlanType = new SelectList(_service.DDLPlanType(), "Value", "Text");
            ViewBag.ProductionUnit = new SelectList(_service.DDLBuilding(), "Value", "Text");
            ViewBag.ProductionFloor = new SelectList(_service.DDLFloor(), "Value", "Text");
            VMShipmentProductionPlan model = new VMShipmentProductionPlan();
            model.LinePlanChart = new List<VMLineSchedule>();
            model.FromDate = DateTime.Today;
            model.ToDate = DateTime.Today.AddMonths(1);
            return View(model);
        }
        [HttpPost]
        public IActionResult PlanScheduleChart(VMShipmentProductionPlan model)
        {
            ViewBag.PlanType = new SelectList(_service.DDLPlanType(), "Value", "Text");
            ViewBag.ProductionUnit = new SelectList(_service.DDLBuilding(), "Value", "Text");
            ViewBag.ProductionFloor = new SelectList(_service.DDLFloor(), "Value", "Text");
            model.LinePlanChart = _service.GetGraphProductionPlan(model);
            return View(model);
        }
        #endregion

        #region ProductionReport

        public IActionResult AchievementReport()
        {
            ViewBag.ProductionUnit = new SelectList(_service.DDLBuilding(), "Value", "Text");
            ViewBag.ProductionFloor = new SelectList(_service.DDLFloor(), "Value", "Text");
            VMProductionReport model = new VMProductionReport();
            model.DataList = new List<VMProductionReport>();
            return View(model);
        }
        [HttpPost]
        public IActionResult AchievementReport(VMProductionReport model)
        {
            ViewBag.ProductionUnit = new SelectList(_service.DDLBuilding(), "Value", "Text");
            ViewBag.ProductionFloor = new SelectList(_service.DDLFloor(), "Value", "Text");
            model = _service.GetTargetAchivementReport(model);
            return View(model);
        }

        public IActionResult ReportView()
        {
            ViewBag.Report = new SelectList(_service.GetProductionReport(), "Value", "Text");
            VMProductionReport model = new VMProductionReport();
            model.FromDate = DateTime.Today.AddDays(-30);
            model.ToDate = DateTime.Today;
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> ReportView(VMProductionReport model)
        {
            ViewBag.Report = new SelectList(_service.GetProductionReport(), "Value", "Text");

            if (model.ID == 1)
            {
                ViewData["ProductionData"] = _service.GetPreodicProductionBuyerWise(model);
            }
            else if (model.ID == 2)
            {
                await Task.Run(() => model.DataList= _service.GetPreodicOrderProduction(model));
            }
            else if (model.ID == 3)
            {
                await Task.Run(() => model.DataList = _service.GetPreodicProductionLineWise(model));
            }
            else if (model.ID == 4)
            {
                await Task.Run(() => model.DataList = _service.GetMonthlyAcheivment(model));
            }
            return View(model);
        }

        public ActionResult DailyProductionReport()
        {
            ViewBag.ProductionStep = new SelectList(_service.DDLProductionStep(), "Value", "Text");
            VMProductionReport model = new VMProductionReport();
            model.FromDate = DateTime.Today;
            model.DataList = new List<VMProductionReport>();
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> DailyProductionReport(VMProductionReport model)
        {
            ViewBag.ProductionStep = new SelectList(_service.DDLProductionStep(), "Value", "Text");
            await Task.Run(() => model.DataList = _service.GetDailyProduction(model));

            if (model.SectionId == (int)EnumProcess.Cutting)
            {
                return View("_DailyCutting", model);
            }
            //return View(model);

            else if (model.SectionId == (int)EnumProcess.Sewing)
            {
                return View("_DailySewing", model);
            }
            //return View(model);

            else if (model.SectionId == (int)EnumProcess.Ironning)
            {
                return View("_DailyIroning", model);
            }
            //return View(model);

            else if (model.SectionId == (int)EnumProcess.Packing)
            {
                return View("_DailyPacking", model);
            }
            return View(model);
        }

        public IActionResult ProductionClose()
        {
            ViewBag.StyleList = new SelectList(_service.StyleDropDownList(), "Value", "Text");
            VMProductionReport model = new VMProductionReport();
            return View(model);
        }
        
        //[HttpPost]
        //public async Task<IActionResult> ProductionClose(VMProductionReport model)
        //{
        //    ViewBag.StyleList = new SelectList(_service.StyleDropDownList(), "Value", "Text");
        //    await Task.Run(() => model.GetOrderCloseReport(model.BOMID));
        //    return View(model);
        //}
        #endregion

        #region Remarks
        public IActionResult Remarks()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Remarks(VMRemarks model)
        {
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.RemarksAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.RemarksEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.RemarksDelete(model.ID);
            }
            else if (model.ActionEum == ActionEnum.Close)
            {
                //Delete
                await _service.Acknowledge(model);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("Remarks");
        }
        public IActionResult LockedRemarks(int id)
        {
            var model = Task.Run(() => _service.GetRemarks(id)).Result;
            return View(model);

        }

        #endregion

        #region Style Wise Production Reports

        public ActionResult StyleWiseProductionReport()
        {
            ViewBag.Style = new SelectList(_service.StyleDropDownList(), "Value", "Text");
            VMProductionReport model = new VMProductionReport();
            model.DataList = new List<VMProductionReport>();
        
            return View(model);
        }
        [HttpPost]
        public async Task<ActionResult> StyleWiseProductionReport(VMProductionReport model)
        {
            ViewBag.Style= new SelectList(_service.StyleDropDownList(), "Value", "Text");
            await Task.Run(() => model.DataList = _service.GetStyleWiseProduction(model));
            return View(model);
        }

        #endregion
    }
}