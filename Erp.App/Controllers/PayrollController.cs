﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Erp.Core.Services;
using Erp.Core.Services.Payroll;
using Erp.Core.Services.User;
using Erp.Infrastructure;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;

namespace Erp.App.Controllers
{
    [SoftwareAdmin]
    public class PayrollController : Controller
    {
        private readonly ILogger _logger;
        private readonly PayrollService _service;
        public PayrollController(InfrastructureDbContext db, ILogger<PayrollController> logger, IHttpContextAccessor httpContextAccessor)
        {
            SessionHandler sessionHandler = new SessionHandler(db, httpContextAccessor.HttpContext);
            sessionHandler.Adjust();
            _service = new PayrollService(db);
            _logger = logger;
        }
        /// <summary>
        /// This region hold all mechanism for  salary and it's structure
        /// </summary>
        #region Employee Salary

        ///<summary>
        /// Create & Edit salary structure for both worker & employee (white & blue)
        /// </summary>
        ///<see cref=" _service.SalaryStructureGet()"/> to see procedure of data it get
        ///<returns>
        ///VMSalaryStructure view model
        ///</returns>
        public async Task<IActionResult> SalaryStructure()
        {
            VMSalaryStructure model = new VMSalaryStructure();
            model = await _service.SalaryStructureGet();
            return await Task.Run(() => View(model));
        }
        [HttpPost]
        public async Task<IActionResult> SalaryStructure(VMSalaryStructure model)
        {
            #region session values
            string sUser = HttpContext.Session.GetString("User");
            string nUserId = HttpContext.Session.GetString("UserID");
            #endregion
            model.User = nUserId;
            if (model.ActionEum == ActionEnum.Add)
            {
                int result = await _service.SalaryStructureAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                int result = await _service.SalaryStructureEdit(model);
            }
            return RedirectToAction("SalaryStructure");
        }

        ///<summary>
        /// Create & Filter particular employee salary
        /// </summary>
        /// <remarks>
        /// Initially it's load all employee salary of business unit 1 (Hard coded)
        /// Filter employee salary by <paramref name="bunit"/> Business Unit, <paramref name="unit"/> Unit,  <paramref name="dept"/> Department  and  <paramref name="section"/> Section  and returns the filter result all together or individually.
        /// </remarks>
        ///<see cref=" _service.EmployeeSalaryGet()"/> to see procedure of data it get
        ///<seealso cref="ERP\erp\Erp.App\Components\EmployeeSalaryViewComponent.cs"/> to get procedure of filtering system in view component (same as partial view)
        ///<returns>
        ///VMEmployeeSalary View Model
        ///EmployeeSalary View Component
        ///</returns>
        public async Task<IActionResult> EmployeeSalary()
        {
            VMEmployeeSalary model = new VMEmployeeSalary
            {
                VMBusinessUnitId = 1
            };
            model = await _service.EmployeeSalaryGet(model);
            return await Task.Run(() => View(model));
        }
        public IActionResult FilterEmployeeSalary(int bunit, int unit, int dept, int section)
        {
            return ViewComponent("EmployeeSalary", new { bunit, unit, dept, section });
        }


        ///<summary>
        /// Create & Edit particular employee salary
        /// </summary>
        /// <remarks>
        /// Hard coded AllocationId & BillEligibilityId within if else due to massive change of UI design 
        /// </remarks>
        ///<see cref=" _service.EmployeeSalaryAdd()"/> to see procedure of data it Save
        ///<seealso cref="_service.EmployeeSalaryEdit()"/> to see procedure of data it Edit
        /// <param name="VMEmployeeSalary model">A view model contain gross salary along with other information.</param>
        /// <param name="model.AllocationId"> 1= OT & Attendance Bonus, 2= Only OT, 3= Only Attendance Bonus & 4= No Bonus Allocations</param>
        /// <param name="model.BillEligibility"> 1=Only Holiday Bill, 2= Only Night Bill, 3= Holiday Bill & Night Bill , 4= No Bill Allocation</param>
        ///<returns>
        ///VMEmployeeSalary View Model
        ///</returns>
        [HttpPost]
        public async Task<IActionResult> EmployeeSalary(VMEmployeeSalary model)
        {
            #region session values
            string sUser = HttpContext.Session.GetString("User");
            string nUserId = HttpContext.Session.GetString("UserID");
            #endregion
            VMEmployeeSalary vm = new VMEmployeeSalary();
            model.User = nUserId;
            model.AllocationId = model.AllocationIds != null ? model.AllocationIds.Length == 2 ? model.AllocationId = 1 : model.AllocationId = model.AllocationIds[0] : 4;
            model.BillEligibility = model.BillEligibilityIds != null ? model.BillEligibilityIds.Length == 2 ? model.BillEligibility = 3 : model.BillEligibility = model.BillEligibilityIds[0] : 4;
            if (model.ActionEum == ActionEnum.Add)
            {
                await _service.EmployeeSalaryAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                model.AllocationId = model.AllocationIdForEdit != null ? model.AllocationIdForEdit.Length == 2 ? model.AllocationId = 1 : model.AllocationId = model.AllocationIdForEdit[0] : 4;
                model.BillEligibility = model.BillEligibilityForEdit != null ? model.BillEligibilityForEdit.Length == 2 ? model.BillEligibility = 3 : model.BillEligibility = model.BillEligibilityForEdit[0] : 4;
                model.GrossSalary = model.GrossSalaryForEdit;
                model.Remarks = model.RemarksForEdit;
                model.Remarks = model.RemarksForEdit;
                await _service.EmployeeSalaryEdit(model);
            }
            vm = await _service.EmployeeSalaryGet(model);
            ModelState.Clear();
            return await Task.Run(() => View(vm));
        }
        #endregion

        /// <summary>
        /// This region hold all mechanism for  salary increament and it's approval system
        /// </summary>
        #region Salary Increament

        ///<summary>
        /// Create & Filter particular employee salary increament
        /// </summary>
        /// <remarks>
        /// Initially it's load all pending increament employee salary of current month of business unit 1 (Hard coded)
        /// Filter employee salary by 
        /// <paramref name="bunit"/> Business Unit, <paramref name="unit"/> Unit,  <paramref name="dept"/> Department,  <paramref name="section"/> Section,  <paramref name="fromdate"/> From Date  and <paramref name="todate"/> To Date
        /// Returns the filter result all together or individually within a time range.
        /// </remarks>
        ///<see cref=" _service.IncreamentSalaryGet()"/> to see procedure of data it get
        ///<seealso cref="ERP\erp\Erp.App\Components\EmployeeSalaryIncreament.cs"/> to get procedure of filtering system in view component (same as partial view)
        ///<returns>
        ///VMSalaryIncreament View Model
        ///EmployeeSalaryIncreament View Component
        ///</returns>
        public async Task<IActionResult> SalaryIncreament()
        {
            VMSalaryIncreament model = new VMSalaryIncreament
            {
                VMBusinessUnitId = 1
            };
            model = await _service.IncreamentSalaryGet(model);
            return await Task.Run(() => View(model));
        }
        public IActionResult FilterSalaryIncreamentList(int bunit, int unit, int dept, int section, DateTime fromdate, DateTime todate)
        {
            return ViewComponent("EmployeeSalaryIncreament", new { bunit, unit, dept, section, fromdate, todate });
        }
        ///<summary>
        /// Approve employee salary increament
        /// </summary>
        /// <remarks>
        /// <see cref="ApproveSalaryIncreamentAsync()"/> For HR Approval 
        /// <seealso cref="ManagementApprovalOfIncreamentSalary()"/> Approved salary list form HR For Management approval
        /// </remarks>
        ///<see cref=" _service.ApproveIncreamentedSalary()"/> to approve from HR 
        ///<seealso cref="ManagementApprovalOfIncreamentSalaryGet()"/> to get data for management approval
        ///<seealso cref="ManagementApprovalProcess()"/> to approve from management
        ///<param name="effecteddate"></param> New increamental salary effected from
        ///<param name="eodmasterid"></param>  To track & get previous salary structure (salary increament from) for particular employee
        ///<param name="grosssalary"></param>
        public async Task<JsonResult> ApproveSalaryIncreamentAsync(int eodmasterid, decimal grosssalary, DateTime effecteddate)
        {
            #region session values
            string sUser = HttpContext.Session.GetString("User");
            string nUserId = HttpContext.Session.GetString("UserID");
            #endregion
            int result = await _service.ApproveIncreamentedSalary(eodmasterid, grosssalary, nUserId, effecteddate);
            return Json(result);
        }
        public async Task<IActionResult> ManagementApprovalOfIncreamentSalary()
        {
            VMSalaryIncreament model = new VMSalaryIncreament();
            model = await _service.ManagementApprovalOfIncreamentSalaryGet();
            return await Task.Run(() => View(model));
        }
        /// <summary>
        /// Management Approval system
        /// </summary>
        /// <param name="id"></param>
        /// <param name="status"></param> 1= Pending (Default), 2= Approve and 3= Reject 
        /// <returns></returns>
        public async Task<IActionResult> ApproveIncreament(int id, int status)
        {
            int result = await _service.ManagementApprovalProcess(id, status);
            return RedirectToAction("ManagementApprovalOfIncreamentSalary");
        }
        #endregion
        /// <summary>
        /// This region hold all mechanism for generate payroll and it's approval system & status
        /// </summary>
        #region Generate Payroll
        ///<summary>
        /// Generate Business Unit wise employee payroll & view
        /// </summary>
        /// <remarks>
        /// Payroll generates upon creating a new payroll reference within a time period
        /// </remarks>
        ///<see cref=" _service.GeneratePayroll()"/> to approve from HR 
        ///<see cref="PayrollDetailsGet()"/> to get data for management approval
        public async Task<IActionResult> PayrollManage()
        {
            var model = new VMPayroll();
            model = await _service.PayrollGet();
            return await Task.Run(() => View(model));
        }
        [HttpPost]
        public async Task<IActionResult> PayrollManage(VMPayroll model)
        {
            #region session values
            string sUser = HttpContext.Session.GetString("User");
            string nUserId = HttpContext.Session.GetString("UserID");
            #endregion
            model.User = nUserId;
            var data = await _service.GeneratePayroll(model);
            return RedirectToAction("PayrollManage");
        }
        /// <summary>
        /// Change Payroll Status (Pending/Processed/Closed)
        /// </summary>
        /// <remarks>
        /// Initially Payroll status is "Pending".
        /// Upon verifying (Approve/Hold/Reprocess) each employee send "Approved" list to the Account/Management for payment which status is "Processed"
        /// After all payment and other procedure end particular payroll status to "Closed" 
        /// </remarks>
        /// <param name="payrollmasterid"></param> Payroll reference
        /// <param name="status"></param> 1= pending, 2= Processed & 3= Closed
        /// <see cref=" _service.ChangePayrollStatus()"/>
        /// <returns>
        /// Return to "PayrollManage" view if status is Processed.  
        /// i,e. if status change from Pending (1) to Processed (2) return main page "PayrollManage" which contain all (Pending, Processed & Closed payroll) list
        /// Else (change from Processed (2) to Closed (3)) return to "ProcessedPayroll" list which contain processed & closed list of payroll 
        /// </returns>
        public async Task<IActionResult> PayrollStatus(int payrollmasterid, int status)
        {
            int result = await _service.ChangePayrollStatus(payrollmasterid, status);
            return status == 2
               ? RedirectToAction("PayrollManage")
               : RedirectToAction("ProcessedPayroll");
        }
        /// <summary>
        /// View all payroll information which is generated
        /// </summary>
        /// <param name="id"></param> Payroll ID (master id)
        /// <returns></returns>
        public async Task<IActionResult> PayrollDetails(int id)
        {
            var model = new VMPayrollDetails();
            model = await _service.PayrollDetailsGet(id);
            return await Task.Run(() => View(model));
        }
        /// <summary>
        /// Change particular employee payroll status
        /// </summary>
        /// <remarks>
        /// User can Hold, Reprocess (required) of a particullar employee
        /// After Processed payroll, Unapproved (Hold & Reprocess (required)) list send to respective authority(s)
        /// This authority(s) can Reprocess & Approved that paricular unapproved employee payroll
        /// t1.ApprovalStatus == 1 ? "Approved" : t1.ApprovalStatus == 2 ? "On Hold" : t1.ApprovalStatus == 3 ? "Reprocess" : t1.ApprovalStatus == 4 ? "Hold & Approved" : t1.ApprovalStatus == 5 ? "Reprocessed" : t1.ApprovalStatus == 6 ? "Reprocessed & approved" : ""
        /// </remarks>
        /// <param name="payrollmasterid"></param> For redirect to same form where its coming from after submit
        /// <param name="payrolldetailsid"></param> Which payroll (particular employee) details status will change
        /// <param name="status"></param>   ** (1= "Approved" (Default), 2= "On Hold", 3= "Reprocess")-->  PayrollDetails view **
        ///                                 ** (4= "Hold & Approved", 5= "Reprocessed", 6= "Reprocessed & approved")-->  UnApprovedPayrollDetails view **
        /// <returns>
        /// Return to particular page from where the status is changed 
        /// </returns>
        public async Task<IActionResult> PayrollDetailsStatus(int payrollmasterid, int payrolldetailsid, int status)
        {
            int result = await _service.ChangePayrollDetailsStatus(payrolldetailsid, status);
            return status == 4 || status == 5 || status == 6
                ? RedirectToAction("UnApprovedPayrollDetails", new { id = payrollmasterid })
                : RedirectToAction("PayrollDetails", new { id = payrollmasterid });
        }
        /// <summary>
        /// Get the list of Processed Payroll which is ready for pay.
        /// </summary> 
        /// <see cref=" _service.PayrollGet()"/> For data get process
        /// <remarks>
        /// ** Get Processed & Closed Payroll List (2= Processed & 3= Closed ,i.e. Master Status) **
        /// ** Get amounts and members of "Approved" employee (1= Approved, 4= Hold then Approved & 6= Reprocessed then Approved, i,e. Child Status)  
        /// </remarks>
        /// <returns> List of Processed and Closed Payroll</returns>
        public async Task<IActionResult> ProcessedPayroll()
        {
            var model = new VMPayroll();
            model = await _service.PayrollGet(new int[] { 2, 3 }, new int[] { 1, 4, 6 });
            return await Task.Run(() => View(model));
        }
        /// <summary>
        /// Get the list of Processed Payroll Details (only approved) which is ready for pay.
        /// </summary> 
        /// <see cref=" _service.PayrollDetailsGet()"/> For data get process
        /// <remarks>
        /// ** Get amounts and members of "Approved" employee (1= Approved, 4= Hold then Approved & 6= Reprocessed then Approved, i,e. Child Status)  
        /// </remarks>
        /// <returns> List of Processed and Closed Payroll</returns>
        public async Task<IActionResult> ProcessedPayrollDetails(int id)
        {
            var model = new VMPayrollDetails();
            model = await _service.PayrollDetailsGet(id, new int[] { 1, 4, 6 });
            return await Task.Run(() => View(model));
        }
        /// <summary>
        /// Get the list of Processed Payroll which are in Hold or Reprocess Required.
        /// </summary> 
        /// <see cref=" _service.PayrollGet()"/> For data get process
        /// <remarks>
        /// ** Get Processed & Closed Payroll List (2= Processed & 3= Closed ,i.e. Master Status) **
        /// ** Get amounts and members of "Un-Approved" employee (2= Hold, 3= Re-processed (required), i,e. Child Status)  
        /// </remarks>
        /// <returns> List of Processed and Closed Payroll (Unapproved)</returns>
        public async Task<IActionResult> UnApprovedPayroll()
        {
            var model = new VMPayroll();
            model = await _service.PayrollGet(new int[] { 2, 3 }, new int[] { 2, 3 });
            return await Task.Run(() => View(model));
        }
        /// <summary>
        /// Get the list of Processed Payroll Details (only Unapproved) which is ready for pay.
        /// </summary> 
        /// <see cref=" _service.PayrollDetailsGet()"/> For data get process
        /// <remarks>
        /// ** Get amounts and members of "Approved" employee (2= Hold, 3= Reprocess & 5= Reprocessed, i,e. Child Status)  
        /// </remarks>
        /// <returns> List of Processed and Closed Unapproved Payroll</returns>
        public async Task<IActionResult> UnApprovedPayrollDetails(int id)
        {
            var model = new VMPayrollDetails();
            model = await _service.PayrollDetailsGet(id, new int[] { 2, 3, 5 });
            return await Task.Run(() => View(model));
        }
        /// <summary>
        /// Chnage Payment Status
        /// </summary>
        /// <param name="payrollmasterid"></param> for redirect to main page after form submit
        /// <param name="payrolldetailsid"></param> which status will change
        /// <param name="status"></param> 1= Paid & 2= Unpaid
        public async Task<IActionResult> PaymentStatus(int payrollmasterid, int payrolldetailsid, int status)
        {
            int result = await _service.ChangePaymentStatus(payrolldetailsid, status);
            return RedirectToAction("ProcessedPayrollDetails", new { id = payrollmasterid });
        }
        /// <summary>
        /// Regenarate Specific Employee salary
        /// </summary>
        /// <param name="payrollmasterid"></param> For redirect to page from which it called
        /// <param name="payrolldetailsid"></param> Which will be regenrate (DetailsId rather than employeeId itself)
        /// <see cref="_service.RegenerateSpecificEmployeeSalary"/>
        public async Task<IActionResult> RegenarateSpecificEmployeeSalary(int payrollmasterid, int payrolldetailsid)
        {
            #region session values
            string sUser = HttpContext.Session.GetString("User");
            string nUserId = HttpContext.Session.GetString("UserID");
            #endregion
            var result = await _service.RegenerateSpecificEmployeeSalary(payrolldetailsid, nUserId);
            return RedirectToAction("UnApprovedPayrollDetails", new { id = payrollmasterid });
        }
        #endregion

        public IActionResult NightAndHolidayBill()
        {
            ViewBag.DegignationList = new SelectList(_service.DesignationDropDownList(), "Value", "Text");
            _logger.LogInformation("");
            return View();
        }

        /// <summary>
        /// This region hold all mechanism for generate Bonus dynamitically
        /// </summary>
        #region Bonus
        public async Task<IActionResult> BonusSetting()
        {
            var model = new VMBonusSettings();
            model = await _service.BonusSettingsGet();
            return await Task.Run(() => View(model));
        }
        [HttpPost]
        public async Task<IActionResult> BonusSetting(VMBonusSettings model)
        {
            int result = -1;
            if (model.ActionEum == ActionEnum.Add)
            {
                result = await _service.BonusSettingsAddAsync(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                result = await _service.BonusSettingsEditAsync(model);
            }

            return RedirectToAction("BonusSetting");
        }
        public async Task<IActionResult> BonusManage()
        {
            var model = new VMBonusMaster();
            model = await _service.BonusMasterGet();
            return await Task.Run(() => View(model));
        }
        [HttpPost]
        public async Task<IActionResult> BonusManage(VMBonusMaster model)
        {
            #region session values
            string sUser = HttpContext.Session.GetString("User");
            string nUserId = HttpContext.Session.GetString("UserID");
            #endregion
            model.User = nUserId;
            var result = await _service.BonusMasterAdd(model);
            return RedirectToAction("BonusManage");
        }
        public async Task<IActionResult> BonusDetails(int id)
        {
            var model = new VMBonusDetails();
            model = await _service.BonusDetailsGet(id);
            return await Task.Run(() => View(model));
        }
        #endregion
        /// <summary>
        /// This region hold all mechanism for particular employee promotion with & without increament
        /// </summary>
        #region Promotion
        public async Task<IActionResult> Promotion()
        {
            var model = new VMPromotion();
            model = await _service.EmployeePromotionGet();
            return await Task.Run(() => View(model));
        }
        /// <summary>
        /// Get employee details
        /// </summary>
        /// <param name="id"></param>employee id
        public async Task<JsonResult> GetEmployeeParticularById(int id)
        {
            var result = await _service.EmployeeParticularGetById(id);
            return Json(result);
        }
        [HttpPost]
        public async Task<IActionResult> Promotion(VMPromotion model)
        {
            #region session values
            string sUser = HttpContext.Session.GetString("User");
            string nUserId = HttpContext.Session.GetString("UserID");
            #endregion
            model.User = nUserId;

            var result = await _service.EmployeePromotionAddAsync(model);
            return result < 0 ? View(model) : (IActionResult)RedirectToAction("PromotionList");
        }
        /// <summary>
        /// List of employee who had promotion
        /// </summary>
        public async Task<IActionResult> PromotionList()
        {
            var data = await _service.PromotionList();
            return await Task.Run(() => View(data));
        }
        #endregion

    }
}