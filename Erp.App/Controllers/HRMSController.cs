﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Erp.Infrastructure;
using Erp.Core.Services.HRMS;
using Erp.Core.Entity.HRMS;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc.Rendering;
using Erp.Core.Services;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;
using Erp.Core.Services.Integration;
using System.Web;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using Erp.Core.Services.User;

namespace Erp.App.Controllers
{
    [SoftwareAdmin]
    public class HRMSController : Controller
    {
        private readonly ILogger _logger;
        private readonly HRMSService _service;
        private readonly IntegrationService _IntegrationService;
        private readonly IHostingEnvironment _appEnvironment;


        public HRMSController(InfrastructureDbContext db, ILogger<HRMSController> logger, IHostingEnvironment appEnvironment, IHttpContextAccessor httpContextAccessor)
        {
            SessionHandler sessionHandler = new SessionHandler(db, httpContextAccessor.HttpContext);
            sessionHandler.Adjust();
            _service = new HRMSService(db);
            _IntegrationService = new IntegrationService(db, httpContextAccessor.HttpContext);
            _logger = logger;
            _appEnvironment = appEnvironment;
        }

        #region Employee Information
        public async Task<IActionResult> Employee(int id = 0)
        {
            
            VMEmployee model = new VMEmployee();

            model = await _service.EmployeeSpecificGet(new VMEmployee() { ID = id });

            if (id > 0)
            {
                model.ActionId = 2;
            }

            model.VMCountryList = await _service.CountryDropDownListAsync();
            model.VMBusinessUnitList = await _service.BusinessUnitDropDownListAsync();
            model.VMReportingEmployeeList = await _service.ReportingEmployeeList();
            //model.VMDegreeList = await _service.DegreeDropDownListAsync();
            model.VMDepartmentList = await _service.DepartmentDropDownListAsync();
            model.VMCountryId = 4;
            if (model.VMBusinessUnitId.HasValue && model.VMBusinessUnitId > 0) { model.VMUnitList = await _service.UnitDropDownListAsync(model.VMBusinessUnitId.Value); }
            if (model.VMUnitId.HasValue && model.VMUnitId > 0) { model.VMSectionList = await _service.SectionDropDownListAsync(model.VMUnitId.Value); }
            //if (model.VMUnitId.HasValue && model.VMUnitId > 0) { model.VMDepartmentList = await _IntegrationService.DepartmentDropDownListAsync(model.VMUnitId.Value); }
            if (model.VMDepartmentId.HasValue && model.VMDepartmentId > 0) { model.VMDesignationList = await _service.DesignationDropDownListAsync(model.VMDepartmentId.Value); }


            if (model.VMCountryId.HasValue && model.VMCountryId > 0) { model.VMDistrictList = await _service.DistrictDropDownListAsync(model.VMCountryId.Value); }
            if (model.VMDistrictId.HasValue && model.VMDistrictId > 0) { model.VMPoliceStationList = await _service.PoliceStationDropDownListAsync(model.VMDistrictId.Value); }
            if (model.VMPoliceStationId.HasValue && model.VMPoliceStationId > 0) { model.VMPostOfficeList = await _service.PostOfficeDropDownListAsync(model.VMPoliceStationId.Value); }

            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " EmployeeSpecificGet / CountryDropDownListAsync / BusinessUnitDropDownListAsync / UnitDropDownListAsync / DepartmentDropDownListAsync / SectionDropDownListAsync / DesignationDropDownListAsync, Employee Page).");
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> Employee(VMEmployee model)
        {
            #region session values
            string sUser = HttpContext.Session.GetString("User");
            string nUserId = HttpContext.Session.GetString("UserID");
            #endregion
            if (string.IsNullOrEmpty(sUser) || string.IsNullOrEmpty(nUserId)) { return RedirectToAction("Login", "Auth"); }
            model.User = sUser;
            model.UserID = int.Parse(nUserId);
            int savedId = 0;
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                savedId = await _service.EmployeeAdd(model);
                _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " EmployeeAdd, Employee Operation).");
                return RedirectToAction("Employee", "HRMS", new { id = savedId });
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                savedId = await _service.EmployeeEdit(model);
                _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " EmployeeEdit, Employee Operation).");
                return RedirectToAction("Employee", "HRMS", new { id = savedId });
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.EmployeeDelete(model.ID);
                _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " EmployeeDelete, Employee Operation).");
                return RedirectToAction("EmployeeList", "HRMS");
            }
            else if (model.ActionId > 3)
            {
                //Delete
                await _service.EmployeeStatusUpdate(model);
                _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " EmployeeStatusUpdate, Employee Operation).");
                return RedirectToAction("EmployeeList", "HRMS");
            }
            else
            {
                _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " Employee Operation failed).");
                return RedirectToAction("Error", "Home");
            }
            
        }

        [HttpPost]
        public async Task<IActionResult> EmployeeBasic(VMEmployee model)
        {
            #region session values
            string sUser = HttpContext.Session.GetString("User");
            string nUserId = HttpContext.Session.GetString("UserID");
            #endregion
            if (string.IsNullOrEmpty(sUser) || string.IsNullOrEmpty(nUserId)) { return RedirectToAction("Login", "Auth"); }
            model.User = sUser;
            model.UserID = int.Parse(nUserId);
            int savedId = 0;

            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 

                savedId = await _service.EmployeeBasicAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                savedId = await _service.EmployeeBasicEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.EmployeeDelete(model.ID);
            }
            else
            {
                _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " EmployeeBasicAdd / EmployeeBasicEdit / EmployeeDelete, Employee Basic Operation failed).");
                return RedirectToAction("Error", "Home");
            }
            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " EmployeeBasicAdd / EmployeeBasicEdit / EmployeeDelete, Employee Basic Operation).");
            return RedirectToAction("Employee", "HRMS", new { id = savedId });
        }
        [HttpPost]
        public async Task<IActionResult> EmployeeContact(VMEmployee model)
        {
            #region session values
            string sUser = HttpContext.Session.GetString("User");
            string nUserId = HttpContext.Session.GetString("UserID");
            #endregion
            if (string.IsNullOrEmpty(sUser) || string.IsNullOrEmpty(nUserId)) { return RedirectToAction("Login", "Auth"); }
            model.User = sUser;
            model.UserID = int.Parse(nUserId);
            int savedId = 0;
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                savedId = await _service.EmployeeContactAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                savedId = await _service.EmployeeContactEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.EmployeeDelete(model.ID);
            }
            else
            {
                _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " EmployeeContactAdd / EmployeeContactEdit / EmployeeDelete, Employee Cantact Operation failed).");
                return RedirectToAction("Error", "Home");
            }
            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " EmployeeContactAdd / EmployeeContactEdit / EmployeeDelete, Employee Cantact Operation).");
            return RedirectToAction("Employee", "HRMS", new { id = savedId });
        }

        [HttpPost]
        public async Task<IActionResult> EmployeeOffice(VMEmployee model)
        {
            #region session values
            string sUser = HttpContext.Session.GetString("User");
            string nUserId = HttpContext.Session.GetString("UserID");
            #endregion
            if (string.IsNullOrEmpty(sUser) || string.IsNullOrEmpty(nUserId)) { return RedirectToAction("Login", "Auth"); }
            model.User = sUser;
            model.UserID = int.Parse(nUserId);
            int savedId = 0;
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                savedId = await _service.EmployeeOfficeAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                savedId = await _service.EmployeeOfficeEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.EmployeeDelete(model.ID);
            }
            else
            {
                _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " EmployeeOfficeAdd / EmployeeOfficeEdit / EmployeeDelete, Employee Office Operation Failed).");
                return RedirectToAction("Error", "Home");
            }
            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " EmployeeOfficeAdd / EmployeeOfficeEdit / EmployeeDelete, Employee Office Operation).");
            return RedirectToAction("Employee", "HRMS", new { id = savedId });
        }
        [HttpGet]
        public IActionResult LoadEmployeePartial(int ID, int EmployeeID, string Name)
        {
            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " Various Employee partial pages).");
            return ViewComponent("Employee", new { ID = ID, EmployeeID = EmployeeID, Name = Name });
        }
        [HttpPost]
        public async Task<IActionResult> EmployeeSpouse(VMEmployeeSpouse model)
        {
            #region session values
            string sUser = HttpContext.Session.GetString("User");
            string nUserId = HttpContext.Session.GetString("UserID");
            #endregion
            if (string.IsNullOrEmpty(sUser) || string.IsNullOrEmpty(nUserId)) { return RedirectToAction("Login", "Auth"); }
            model.User = sUser;
            model.UserID = int.Parse(nUserId);
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.SpouseAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.SpouseEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.SpouseDelete(model.ID);
            }
            else
            {
                _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " SpouseAdd / SpouseEdit / SpouseDelete, Spouse Operation failed).");
                return RedirectToAction("Error", "Home");
            }
            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " SpouseAdd / SpouseEdit / SpouseDelete, Spouse Operation).");
            return RedirectToAction("Employee", "HRMS", new { id = model.VMEmployeeId });
        }

        [HttpPost]
        public async Task<IActionResult> EmployeeNominee(VMEmployeeNominee model)
        {
            #region session values
            string sUser = HttpContext.Session.GetString("User");
            string nUserId = HttpContext.Session.GetString("UserID");
            #endregion
            if (string.IsNullOrEmpty(sUser) || string.IsNullOrEmpty(nUserId)) { return RedirectToAction("Login", "Auth"); }
            model.User = sUser;
            model.UserID = int.Parse(nUserId);
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.NomineeAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.NomineeEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.NomineeDelete(model.ID);
            }
            else
            {
                _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " NomineeAdd / NomineeEdit / NomineeDelete, Nominee Operation failed).");
                return RedirectToAction("Error", "Home");
            }
            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " NomineeAdd / NomineeEdit / NomineeDelete, Nominee Operation).");
            return RedirectToAction("Employee", "HRMS", new { id = model.VMEmployeeId });
        }

        [HttpPost]
        public async Task<IActionResult> EmployeeChildren(VMEmployeeChildren model)
        {
            #region session values
            string sUser = HttpContext.Session.GetString("User");
            string nUserId = HttpContext.Session.GetString("UserID");
            #endregion
            if (string.IsNullOrEmpty(sUser) || string.IsNullOrEmpty(nUserId)) { return RedirectToAction("Login", "Auth"); }
            model.User = sUser;
            model.UserID = int.Parse(nUserId);
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.ChildrenAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.ChildrenEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.ChildrenDelete(model.ID);
            }
            else
            {
                _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " ChildrenAdd / ChildrenEdit / ChildrenDelete, Children Operation failed).");
                return RedirectToAction("Error", "Home");
            }
            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " ChildrenAdd / ChildrenEdit / ChildrenDelete, Children Operation).");
            return RedirectToAction("Employee", "HRMS", new { id = model.VMEmployeeId });
        }
        [HttpPost]
        public async Task<IActionResult> EmployeeEducation(VMEmployeeEducation model)
        {
            #region session values
            string sUser = HttpContext.Session.GetString("User");
            string nUserId = HttpContext.Session.GetString("UserID");
            #endregion
            if (string.IsNullOrEmpty(sUser) || string.IsNullOrEmpty(nUserId)) { return RedirectToAction("Login", "Auth"); }
            model.User = sUser;
            model.UserID = int.Parse(nUserId);
            model.VMDegreeList = await _service.DegreeDropDownListAsync();
            //model.VMDegreeList =  new SelectList(await _service.DegreeDropDownListAsync(), "Value", "Text");
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.EducationAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.EducationEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.EducationDelete(model.ID);
            }
            else
            {
                _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " EducationAdd / EducationEdit / EducationDelete, Education Operation failed).");
                return RedirectToAction("Error", "Home");
            }

            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " EducationAdd / EducationEdit / EducationDelete, Education Operation).");
            return RedirectToAction("Employee", "HRMS", new { id = model.VMEmployeeId });
        }

        [HttpPost]
        public async Task<IActionResult> EmployeeTraining(VMEmployeeTraining model)
        {
            #region session values
            string sUser = HttpContext.Session.GetString("User");
            string nUserId = HttpContext.Session.GetString("UserID");
            #endregion
            if (string.IsNullOrEmpty(sUser) || string.IsNullOrEmpty(nUserId)) { return RedirectToAction("Login", "Auth"); }
            model.User = sUser;
            model.UserID = int.Parse(nUserId);
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.TrainingAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.TrainingEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.TrainingDelete(model.ID);
            }
            else
            {
                _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " TrainingAdd / TrainingEdit / TrainingDelete, Training Operation failed).");
                return RedirectToAction("Error", "Home");
            }
            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " TrainingAdd / TrainingEdit / TrainingDelete, Training Operation).");
            return RedirectToAction("Employee", "HRMS", new { id = model.VMEmployeeId });
        }
        [HttpPost]
        public async Task<IActionResult> EmployeeExperience(VMEmployeeExperience model)
        {
            #region session values
            string sUser = HttpContext.Session.GetString("User");
            string nUserId = HttpContext.Session.GetString("UserID");
            #endregion
            if (string.IsNullOrEmpty(sUser) || string.IsNullOrEmpty(nUserId)) { return RedirectToAction("Login", "Auth"); }
            model.User = sUser;
            model.UserID = int.Parse(nUserId);
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.ExperienceAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.ExperienceEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.ExperienceDelete(model.ID);
            }
            else
            {
                _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " ExperienceAdd / ExperienceEdit / ExperienceDelete, Experience Operation failed).");
                return RedirectToAction("Error", "Home");
            }
            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " ExperienceAdd / ExperienceEdit / ExperienceDelete, Experience Operation).");
            return RedirectToAction("Employee", "HRMS", new { id = model.VMEmployeeId });
        }
        [HttpPost]
        public async Task<IActionResult> EmployeeSkill(VMEmployeeSkill model)
        {
            #region session values
            string sUser = HttpContext.Session.GetString("User");
            string nUserId = HttpContext.Session.GetString("UserID");
            #endregion

            if (string.IsNullOrEmpty(sUser) || string.IsNullOrEmpty(nUserId)) { return RedirectToAction("Login", "Auth"); }
            model.User = sUser;
            model.UserID = int.Parse(nUserId);
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.SkillAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.SkillEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.SkillDelete(model.ID);
            }
            else
            {
                _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " SkillAdd / SkillEdit / SkillDelete, Skill Operation failed).");
                return RedirectToAction("Error", "Home");
            }
            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " SkillAdd / SkillEdit / SkillDelete, Skill Operation).");
            return RedirectToAction("Employee", "HRMS", new { id = model.VMEmployeeId });
        }

        [HttpPost]
        public async Task<IActionResult> EmployeeReference(VMEmployeeReference model)
        {
            #region session values
            string sUser = HttpContext.Session.GetString("User");
            string nUserId = HttpContext.Session.GetString("UserID");
            #endregion

            if (string.IsNullOrEmpty(sUser) || string.IsNullOrEmpty(nUserId)) { return RedirectToAction("Login", "Auth"); }
            model.User = sUser;
            model.UserID = int.Parse(nUserId);
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.ReferenceAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.ReferenceEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.ReferenceDelete(model.ID);
            }
            else
            {
                _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " ReferenceAdd / ReferenceEdit / ReferenceDelete, Reference Operation failed).");
                return RedirectToAction("Error", "Home");
            }
            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " ReferenceAdd / ReferenceEdit / ReferenceDelete, Reference Operation).");
            return RedirectToAction("Employee", "HRMS", new { id = model.VMEmployeeId });
        }

        [HttpPost]
        public async Task<IActionResult> UploadFiles(VMEmployee VME)
        {
            var files = HttpContext.Request.Form.Files;
            foreach (var Image in files)
            {
                if (Image != null && Image.Length > 0)
                {
                    var file = Image;
                    //There is an error here
                    var uploads = Path.Combine(_appEnvironment.WebRootPath, "ProfilePhoto");
                    if (file.Length > 0)
                    {
                        var fileName = Guid.NewGuid().ToString().Replace("-", "") + Path.GetExtension(file.FileName);
                        using (var fileStream = new FileStream(Path.Combine(uploads, fileName), FileMode.Create))
                        {
                            await _service.ImageAdd(fileName, VME.ID);
                            await file.CopyToAsync(fileStream);
                        }

                    }
                }
            }
            return RedirectToAction("Employee");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> UploadImage(VMEmployee VME, IFormFile files)
        {
            using (var ms = new MemoryStream())
            {
                files.CopyTo(ms);
                var A = ms.ToArray();
                await _service.EmployeeImage(A, VME.ID);
            }
            return RedirectToAction("Employee");
        }

        public async Task<IActionResult> EmployeeList()
        {
            VMEmployee model = new VMEmployee();
            model.DataList = new List<VMEmployee>();
            model.DataList = await _service.EmployeeListGet(new VMEmployee());

            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> DistrictGet(int id)
        {
            var list = await _service.DistrictGetByBUIdAsync(id);
            var Districtlist = list.Select(x => new { Value = x.ID, Text = x.Name }).ToList();
            return Json(Districtlist);
        }

        [HttpGet]
        public async Task<IActionResult> PoliceStationGet(int id)
        {
            var list = await _service.PoliceStationGetByBUIdAsync(id);
            var PoliceStationlist = list.Select(x => new { Value = x.ID, Text = x.Name }).ToList();
            return Json(PoliceStationlist);
        }

        [HttpGet]
        public async Task<IActionResult> GetLeaveTypeByEmployeeTypeId(int id)
        {
            var list = await _service.GetLeaveTypeByEmpType(id);
            var LeaveTypeList = list.Select(x => new { Value = x.ID, Text = x.Name }).ToList();
            return Json(LeaveTypeList);
        }

        [HttpGet]
        public async Task<IActionResult> PostOfficeGet(int id)
        {
            var list = await _service.PostOfficeGetByBUIdAsync(id);
            var PostOfficelist = list.Select(x => new { Value = x.ID, Text = x.Name }).ToList();
            return Json(PostOfficelist);
        }

        public async Task<IActionResult> EmployeeIdCardGeneration()
        {
            HRMSEmployeesCardGenerationVM HECGVM = new HRMSEmployeesCardGenerationVM();
            ViewBag.Sections = await _service.SectionDropDownListAsync();
            ViewBag.Departments = await _service.DesignationDropDownListAsync();
            return View(HECGVM);
        }

        [HttpPost]
        public async Task<IActionResult> EmployeeIdCardGeneration(HRMSEmployeesCardGenerationVM HECGVM)
        {
            HECGVM.Emplists = await  _service.GetEmployeesForCardGeneration(HECGVM);

            ViewBag.Sections = await _service.SectionDropDownListAsync();
            ViewBag.Departments = await _service.DesignationDropDownListAsync();

            return View(HECGVM);
        }

        public IActionResult GenerateCard()
        {
            HRMSEmployeesCardGenerationVM HECGVM = new HRMSEmployeesCardGenerationVM();
            return View(HECGVM);
        }

        [HttpPost]
        public async Task<IActionResult> GenerateCard(HRMSEmployeesCardGenerationVM HECGVM)
        {
            if (HECGVM.IDCardIssueDate != null && HECGVM.IDCardExpireDate != null && HECGVM.Quantity != 0)
            {
                HECGVM.EmployeeIdCards = await _service.GenerateEmployeesIdCard(HECGVM);
            }
            return View(HECGVM);
        }
        #endregion

        #region Shift Information

        [HttpGet]
        public async Task<IActionResult> Shift(string error, int id = 0)
        {
            VMShift VMS = new VMShift();
            if (id != 0)
            {
                await _service.AddShift(id, null, VMS);
            }
            if (error != null)
            {
                VMS.error = error;
            }
            VMS.VMShifts = await _service.GetShift();
            return View(VMS);
        }

        [HttpPost]
        public async Task<IActionResult> Shift(VMShift VMS)
        {            
            await _service.AddShift(null, null, VMS);

            VMS.VMShifts = await _service.GetShift();
            return RedirectToAction("Shift", "HRMS", new { id = 0});
        }

        public async Task<IActionResult> DeleteShift(int id)
        {
            VMShift VMS = new VMShift();
            await _service.AddShift(id, "Delete", VMS);
            return RedirectToAction("Shift", "HRMS", new { VMS.error, id = 0 });
        }

        public async Task<IActionResult> ShiftAssign(VMShiftAssign VM)
        {
            VMShiftAssign model = new VMShiftAssign() { VMBusinessUnitID = VM.VMBusinessUnitID, VMDepartmentID = VM.VMDepartmentID, VMUnitID = VM.VMUnitID, VMSectionID = VM.VMSectionID };

            model.DataList = new List<VMShiftAssign>();

            model.DataList = await _service.ShiftAssignListGet(new VMShiftAssign() { VMBusinessUnitID = VM.VMBusinessUnitID, VMDepartmentID = VM.VMDepartmentID, VMUnitID = VM.VMUnitID, VMSectionID = VM.VMSectionID, IsForListView = false });

            model.DepartmentList = await _service.DepartmentDropDownListAsync(); 
            model.ShiftList = await _service.ShiftDropDownListAsync();
            model.BusinessUnitList = await _service.BusinessUnitDropDownListAsync();
            if (model.VMBusinessUnitID > 0) { model.UnitList = await _service.UnitDropDownListAsync(model.VMBusinessUnitID); }
            if (model.VMUnitID > 0) { model.SectionList = await _service.SectionDropDownListAsync(model.VMUnitID); }
            if (model.VMDepartmentID > 0) { model.DepartmentList = await _IntegrationService.DepartmentDropDownListAsync(model.VMDepartmentID); }
            
            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " ShiftAssignListGet / ShiftDropDownListAsync / BusinessUnitDropDownListAsync / UnitDropDownListAsync / DepartmentDropDownListAsync / SectionDropDownListAsync, ShiftAssign Page).");
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> ShiftAssignOperation(VMShiftAssign model)
        {
            #region session values
            string sUser = HttpContext.Session.GetString("User");
            string nUserId = HttpContext.Session.GetString("UserID");
            #endregion
            if (string.IsNullOrEmpty(sUser) || string.IsNullOrEmpty(nUserId)) { return RedirectToAction("Login", "Auth"); }
            model.User = sUser;
            model.UserID = int.Parse(nUserId);
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                List<VMShiftAssign> vMList = model.DataList
                    .Where(x => x.IsAssign)
                    .Select(x => new VMShiftAssign()
                    {
                        VMEmployeeID = x.VMEmployeeID,
                        VMShiftID = model.VMShiftID,
                        StartDate = model.StartDate,
                        EndDate = model.EndDate,
                        User = model.User,
                        UserID = model.UserID,
                    }).ToList();

                await _service.ShiftAssignListAdd(vMList);
                _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " ShiftAssignListAdd, ShiftAssign operation).");
                //return RedirectToAction("ShiftAssign", "HRMS", new { VMBusinessUnitID = model.VMBusinessUnitID, VMDepartmentID = model.VMDepartmentID, VMUnitID = model.VMUnitID, VMSectionID = model.VMSectionID });
                return RedirectToAction("ShiftAssignedList");

            }

            else
            {
                _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " ShiftAssignListAdd, ShiftAssign operation failed).");
                return RedirectToAction("Error", "Home");
            }
        }

        public IActionResult ShiftAssignEdit(int id = 0)
        {
            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " ShiftAssigned List Page).");
            return RedirectToAction("ShiftAssignedList", "HRMS", new { ID = id });
        }

        public async Task<IActionResult> ShiftAssignedList(VMShiftAssign VM)
        {
            VMShiftAssign model = new VMShiftAssign()
            {
                VMBusinessUnitID = VM.VMBusinessUnitID,
                VMDepartmentID = VM.VMDepartmentID,
                VMUnitID = VM.VMUnitID,
                VMSectionID = VM.VMSectionID,
                VMFilterByShiftID = VM.VMFilterByShiftID,
                FilterByStartDate = VM.FilterByStartDate,
                FilterByEndDate = VM.FilterByEndDate
            };


            if (VM.ID > 0)
            {
                VMShiftAssign retModel = new VMShiftAssign();
                retModel = await _service.ShiftAssignSpecificGet(new VMShiftAssign() { ID = VM.ID });
                retModel.DataList = await _service.ShiftAssignListGet(new VMShiftAssign()
                {
                    VMBusinessUnitID = retModel.VMBusinessUnitID,
                    VMDepartmentID = retModel.VMDepartmentID,
                    VMUnitID = retModel.VMUnitID,
                    VMSectionID = retModel.VMSectionID,
                    IsForListView = true,
                    VMFilterByShiftID = VM.VMFilterByShiftID,
                    FilterByStartDate = VM.FilterByStartDate,
                    FilterByEndDate = VM.FilterByEndDate
                });
                retModel.ActionId = 2;
                model = new VMShiftAssign();
                model = retModel;
            }
            else
            {
                model.DataList = new List<VMShiftAssign>();

                model.DataList = await _service.ShiftAssignListGet(new VMShiftAssign()
                {
                    VMBusinessUnitID = VM.VMBusinessUnitID,
                    VMDepartmentID = VM.VMDepartmentID,
                    VMUnitID = VM.VMUnitID,
                    VMSectionID = VM.VMSectionID,
                    IsForListView = true,
                    VMFilterByShiftID = VM.VMFilterByShiftID,
                    FilterByStartDate = VM.FilterByStartDate,
                    FilterByEndDate = VM.FilterByEndDate
                });
            }

            model.ShiftList = await _service.ShiftDropDownListAsync();
            model.BusinessUnitList = await _service.BusinessUnitDropDownListAsync();
            model.DepartmentList = await _service.DepartmentDropDownListAsync();
            if (model.VMBusinessUnitID > 0) { model.UnitList = await _service.UnitDropDownListAsync(model.VMBusinessUnitID); }
            if (model.VMUnitID > 0) { model.SectionList = await _service.SectionDropDownListAsync(model.VMUnitID); }
            if (model.VMDepartmentID > 0) { model.DepartmentList = await _IntegrationService.DepartmentDropDownListAsync(model.VMDepartmentID); }

            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " ShiftAssignSpecificGet / ShiftAssignListGet / ShiftDropDownListAsync / BusinessUnitDropDownListAsync / UnitDropDownListAsync / DepartmentDropDownListAsync / SectionDropDownListAsync, ShiftAssigned List Page).");
            return View(model);
        }

        public async Task<IActionResult> DeleteShiftAssigned(int id, int min)
        {
            VMShiftAssign VMH = new VMShiftAssign();
            await _service.DeleteShiftAssigned(id, min);
            return RedirectToAction("ShiftAssignedList", "HRMS", new { VMH });
        }
        #endregion

        #region Assign Weekend information

        public async Task<IActionResult> CompanyWeekend(string error, int id = 0)
        {
             
            VMHoliday VMH = new VMHoliday();
            if (id != 0)
            {
                await _service.AddEditOrDeleteHolidayDay(id, null, VMH, "CompanyWeekend");
            }
            if(error != null)
            {
                VMH.error = error;
            }
            ViewBag.HolidayList = new SelectList(_service.OffdayDropDownList(), "Value", "Text");
            ViewBag.CompanyWeekend = _service._db.HRMS_Holiday.Where(h => h.IsCompanyWeekendDay == true && h.Active == true).ToList();
            return View(VMH);
        }
        [HttpPost]
        public async Task<IActionResult> CompanyWeekend(VMHoliday VMH)
        {
            if (VMH.WeekenedDay != null)
            {
                await _service.AddEditOrDeleteHolidayDay(null, null, VMH, "CompanyWeekend");
            }
            ViewBag.HolidayList = new SelectList(_service.OffdayDropDownList(), "Value", "Text");
            ViewBag.Holidays = _service._db.HRMS_Holiday.Where(h => h.IsCompanyWeekendDay == true && h.Active == true).ToList();
            return RedirectToAction("CompanyWeekend", "HRMS", new {VMH.error, id = 0 });
        }

        public async Task<IActionResult> DeleteCompanyWeekend(int id)
        {
            VMHoliday VMH = new VMHoliday();
            await _service.AddEditOrDeleteHolidayDay(id, "Delete", VMH, "CompanyWeekend");
            return RedirectToAction("CompanyWeekend", "HRMS", new { VMH.error, id = 0 });
        }

        public async Task<IActionResult> WeekendDayAssign()
        {
            VMWeekendDay VMWD = new VMWeekendDay();
            VMWD.B_UnitList = await _service.BusinessUnitDropDownListAsync();
            VMWD.Department_List = await _service.DepartmentDropDownListAsync();
            ViewBag.HolidayList = new SelectList(_service.OffdayDropDownList(), "Value", "Text");
            return View(VMWD);
        }
        [HttpPost]
        public async Task<IActionResult> WeekendDayAssign(VMWeekendDay VMWD)
        {
            VMWD.B_UnitList = await _service.BusinessUnitDropDownListAsync();
            VMWD.Department_List = await _service.DepartmentDropDownListAsync();
            ViewBag.HolidayList = new SelectList(_service.OffdayDropDownList(), "Value", "Text");


            var EList = _service.GetEmployeesForWeekend(VMWD);

            if (EList != null)
            {
                var elist = EList.Select(i => new Emplist()
                {
                    ID = i.ID,
                    EmployeeIdentity = i.EmployeeIdentity,
                    Name = i.Name,
                    Designation = i.Designation,
                    Unit = i.Unit,
                    Section = i.Section,
                    BusinessUnit = i.BusinessUnit,
                    Department = i.Department,
                    IsWeekendAssigned = i.IsWeekendAssigned,
                    WeekendDays = i.WeekendDays
                }).ToList();

                VMWD.Emplists = elist;
            }

            return View(VMWD);
        }


        public async Task<IActionResult> AddAssignWeekend(VMWeekendDay vmwd)
        {
            if (vmwd.Emplists != null)
            {
                await _service.AddAssignWeekend(vmwd);
            }
            return RedirectToAction("WeekendAssignedList");
        }

        public async Task<IActionResult> WeekendAssignedList()
        {
            VMWeekendDay VMWD = new VMWeekendDay();
            VMWD.B_UnitList = await _service.BusinessUnitDropDownListAsync();
            VMWD.Department_List = await _service.DepartmentDropDownListAsync();
            return View(VMWD);
        }
        [HttpPost]
        public async Task<IActionResult> WeekendAssignedList(VMWeekendDay VMWD)
        {
            VMWD.B_UnitList = await _service.BusinessUnitDropDownListAsync();
            VMWD.Department_List = await _service.DepartmentDropDownListAsync();
            var EList = _service.GetEmployeesForWeekendFilter(VMWD);


            if (EList != null)
            {
                var elist = EList.Select(i => new Emplist()
                {
                    ID = i.ID,
                    EmployeeIdentity = i.EmployeeIdentity,
                    Name = i.Name,
                    Designation = i.Designation,
                    Unit = i.Unit,
                    Section = i.Section,
                    BusinessUnit = i.BusinessUnit,
                    Department = i.Department,
                    WeekendDays = i.WeekendDays
                }).ToList();

                VMWD.Emplists = elist;
            }
            return View(VMWD);

        }
        #endregion

        #region Leave Assign, Application & Earn Leave Calculation
        public async Task<IActionResult> LeaveAssign()
        {
            VMLeave VML = new VMLeave();
            VML.B_UnitList = await _service.BusinessUnitDropDownListAsync();
            ViewBag.LeaveTypeList = new SelectList(_service.LeaveTypeDropDownList(1), "Value", "Text");
            VML.Department_List = await _service.DepartmentDropDownListAsync();
            VML.EmpType = new SelectList(_service.GetEmployeeType(), "Value", "Text");

            return View(VML);
        }
        [HttpPost]
        public async Task<IActionResult> LeaveAssign(VMLeave VML)
        {
            VML.B_UnitList = await _service.BusinessUnitDropDownListAsync();
            VML.EmpType = new SelectList(_service.GetEmployeeType(), "Value", "Text");
            ViewBag.LeaveTypeList = new SelectList(_service.LeaveTypeDropDownList(1), "Value", "Text");

            var EList = await _service.GetEmployees(VML);
            if (EList != null)
            {
                var elist = EList.Select(i => new Emplist()
                {
                    ID = i.ID,
                    EmployeeIdentity = i.EmployeeIdentity,
                    Name = i.Name,
                    Designation = i.Designation,
                    Unit = i.Unit,
                    Section = i.Section,
                    BusinessUnit = i.BusinessUnit,
                    Department = i.Department,
                    EmpType = i.EmpType,
                    IsLeaveAssigned = i.IsLeaveAssigned,
                    AnnualLeaveDays = i.AnnualLeaveDays,
                    TakenAnnualLeaveDays = i.TakenAnnualLeaveDays,
                    SickLeaveDays = i.SickLeaveDays,
                    TakenSickLeaveDays = i.TakenSickLeaveDays,
                    CasualLeaveDays = i.CasualLeaveDays,
                    TakenCasualLeaveDays = i.TakenCasualLeaveDays
                }).ToList();

                VML.Emplists = elist;
            }

            return View(VML);
        }

        public async Task<IActionResult> AddLeaveAssign(VMLeave vml)
        {
            if (vml.year != "" && vml.LeaveTypeId != 0 && vml.Emplists != null)
            {
                await _service.AddAssignLeave(vml);
            }
            else
            {
                return RedirectToAction("LeaveAssign");
            }
            return RedirectToAction("LeaveAssignedList");
        }

        public async Task<IActionResult> LeaveAssignedList()
        {
            VMLeave VML = new VMLeave();

            VML.B_UnitList = await _service.BusinessUnitDropDownListAsync();
            VML.Department_List = await _service.DepartmentDropDownListAsync();
            ViewBag.LeaveTypeList = new SelectList(_service.LeaveTypeDropDownList(1), "Value", "Text");
            ViewBag.EmployeeList = new SelectList(_service.EmployeeDropDownList(0), "Value", "Text");

            var EList = await _service.getAssinedLeaveList(VML);
            {
                var elist = EList.Select(i => new Emplist()
                {
                    ID = i.ID,
                    EmployeeIdentity = i.EmployeeIdentity,
                    Name = i.Name,
                    Designation = i.Designation,
                    Unit = i.Unit,
                    Section = i.Section,
                    BusinessUnit = i.BusinessUnit,
                    Department = i.Department,
                    IsLeaveAssigned = i.IsLeaveAssigned,
                    AnnualLeaveDays = i.AnnualLeaveDays,
                    TakenAnnualLeaveDays = i.TakenAnnualLeaveDays,
                    SickLeaveDays = i.SickLeaveDays,
                    TakenSickLeaveDays = i.TakenSickLeaveDays,
                    CasualLeaveDays = i.CasualLeaveDays,
                    TakenCasualLeaveDays = i.TakenCasualLeaveDays
                }).ToList();
                VML.Emplists = elist;
            }

            return View(VML);
        }

        [HttpPost]
        public async Task<IActionResult> LeaveAssignedList(VMLeave VML)
        {

            VML.B_UnitList = await _service.BusinessUnitDropDownListAsync();
            VML.Department_List = await _service.DepartmentDropDownListAsync();
            ViewBag.LeaveTypeList = new SelectList(_service.LeaveTypeDropDownList(1), "Value", "Text");
            ViewBag.EmployeeList = new SelectList(_service.EmployeeDropDownList(0), "Value", "Text");

            if (VML.BU_Id != 0)
            {
                var EList = await _service.getAssinedLeaveList(VML);
                {
                    var elist = EList.Select(i => new Emplist()
                    {
                        ID = i.ID,
                        EmployeeIdentity = i.EmployeeIdentity,
                        Name = i.Name,
                        Designation = i.Designation,
                        Unit = i.Unit,
                        Section = i.Section,
                        BusinessUnit = i.BusinessUnit,
                        Department = i.Department,
                        IsLeaveAssigned = i.IsLeaveAssigned,
                        AnnualLeaveDays = i.AnnualLeaveDays,
                        TakenAnnualLeaveDays = i.TakenAnnualLeaveDays,
                        SickLeaveDays = i.SickLeaveDays,
                        TakenSickLeaveDays = i.TakenSickLeaveDays,
                        CasualLeaveDays = i.CasualLeaveDays,
                        TakenCasualLeaveDays = i.TakenCasualLeaveDays
                    }).ToList();

                    VML.Emplists = elist;
                }
            }

            return View(VML);
        }

        public IActionResult LeaveApplication()
        {
            VMLeaveApplication VMLA = new VMLeaveApplication();
            ViewBag.EmployeeCardList = new SelectList(_service.EmployeeDropDownList(0), "Value", "Text");
            ViewBag.LeaveTypeList = new SelectList(_service.LeaveTypeDropDownList(3), "Value", "Text");
            ViewBag.LeavePaidTypeList = new SelectList(_service.LeavePaidTypeDropDownList(), "Value", "Text");
            ViewBag.WorkerEmployeeList = new SelectList(_service.EmployeeDropDownList((int)EnumStaffType.Worker), "Value", "Text");
            ViewBag.EmployeeList = new SelectList(_service.EmployeeDropDownList(0), "Value", "Text");
            return View(VMLA);
        }
        [HttpPost]
        public async Task<IActionResult> LeaveApplication(VMLeaveApplication VMLA)
        {
            await _service.AddLeaveApplication(VMLA);
            return RedirectToAction("LeaveApplicationList");
        }

        public IActionResult PersonalLeaveApplication()
        {
            VMLeaveApplication VMLA = new VMLeaveApplication();           
            VMLA.EmployeeId = HttpContext.Session.GetInt32("EmployeeID");
            ViewBag.EmployeeCardList = new SelectList(_service.EmployeeDropDownListExceptThisId(VMLA.EmployeeId), "Value", "Text");
            ViewBag.LeaveTypeList = new SelectList(_service.LeaveTypeDropDownList(2), "Value", "Text");
            ViewBag.LeavePaidTypeList = new SelectList(_service.LeavePaidTypeDropDownList(), "Value", "Text");
            ViewBag.EmployeeList = new SelectList(_service.EmployeeDropDownList(0), "Value", "Text");
            return View(VMLA);
        }
        [HttpPost]
        public async Task<IActionResult> PersonalLeaveApplication(VMLeaveApplication VMLA)
        {
            await _service.AddLeaveApplication(VMLA);
            return RedirectToAction("LeaveApplicationList");
        }

        public async Task<IActionResult> LeaveApplicationList(int id = 0)
        {
            VMLeaveApplication VMLA = new VMLeaveApplication();
            VMLA.B_UnitList = new SelectList(_service.BusinessUnitDropDownList(), "Value", "Text");
            VMLA.Department_List = await _service.DepartmentDropDownListAsync();
            ViewBag.LeaveStatusList = new SelectList(_service.LeaveStatusDropDownList(), "Value", "Text");
            ViewBag.EmployeeList = new SelectList(_service.EmployeeDropDownList(0), "Value", "Text");
            var leaveList = await _service.getLeaveApplicationList(VMLA);
            if (leaveList != null)
            {
                var Llist = leaveList.Select(s => new LeaveDays()
                {
                    ID = s.ID,
                    eId = s.eId,
                    EmployeeIdentity = s.EmployeeIdentity,
                    Name = s.Name,
                    leaveName = s.leaveName,
                    From = s.From,
                    To = s.To,
                    DeptStatus = s.DeptStatus,
                    HRStatus = s.HRStatus,
                    TotalDays = s.TotalDays,
                    LeaveType = s.LeaveType
                }).ToList();
                VMLA.LeaveDays = Llist;
            }
            return View(VMLA);
        }
        [HttpPost]
        public async Task<IActionResult> LeaveApplicationList(VMLeaveApplication VMLA)
        {
            VMLA.B_UnitList = new SelectList(_service.BusinessUnitDropDownList(), "Value", "Text");
            VMLA.Department_List = await _service.DepartmentDropDownListAsync();
            ViewBag.LeaveStatusList = new SelectList(_service.LeaveStatusDropDownList(), "Value", "Text");
            ViewBag.EmployeeList = new SelectList(_service.EmployeeDropDownList(0), "Value", "Text");

            var leaveList = await _service.getLeaveApplicationList(VMLA);
            if (leaveList != null)
            {
                var Llist = leaveList.Select(s => new LeaveDays()
                {
                    ID = s.ID,
                    eId = s.eId,
                    EmployeeIdentity = s.EmployeeIdentity,
                    Name = s.Name,
                    leaveName = s.leaveName,
                    From = s.From,
                    To = s.To,
                    DeptStatus = s.DeptStatus,
                    HRStatus = s.HRStatus,
                    TotalDays = s.TotalDays,
                    LeaveType = s.LeaveType
                }).ToList();
                VMLA.LeaveDays = Llist;
            }
            return View(VMLA);
        }

        public async Task<IActionResult> LeaveStatusUpdate(int id, bool status, int eId, int TotalDays, int LeaveType)
        {
            var lsd = await _service.SetLeaveStatusUpdate(id, status, eId, TotalDays, LeaveType);
            return RedirectToAction("LeaveApplicationList", "HRMS", new { id = 0 });
        }

        public async Task<IActionResult> EarnLeaveCalculation()
        {
            VMEarnLeaveCalculation VMELC = new VMEarnLeaveCalculation();
            VMELC.B_UnitList = await _service.BusinessUnitDropDownListAsync();
            VMELC.EarnLeaveDate = DateTime.Now;
            ViewBag.Sections = new SelectList(_service.SectiontDropDownList(), "Value", "Text");
            return View(VMELC);
        }
        [HttpPost]
        public async Task<IActionResult> EarnLeaveCalculation(VMEarnLeaveCalculation VMELC)
        {
            VMELC.B_UnitList = await _service.BusinessUnitDropDownListAsync();

            var eList = await _service.GetEmployeesforEarnLeave(VMELC, 0);
            if (eList != null)
            {
                var emp = eList.Select(s => new VMEarnLeaveCalculationList()
                {
                    ID = s.ID,
                    Unit = s.Unit,
                    Section = s.Section,
                    Department = s.Department,
                    Designation = s.Designation,
                    EmployeeIdentity = s.EmployeeIdentity,
                    Name = s.Name,
                    JoiningDate = s.JoiningDate,
                    LastPaymentDate = s.LastPaymentDate,
                    TotalMonth = s.TotalMonth,
                    WorkingDays = s.WorkingDays,
                    Holidays = s.Holidays,
                    Offdays = s.Offdays,
                    TotalLeave = s.TotalLeave,
                    Absent = s.Absent,
                    TotalDeductionDays = s.TotalDeductionDays,
                    PayableWorkingDays = s.PayableWorkingDays,
                    TotalEarnLeave = s.TotalEarnLeave,
                    EnjoyableEarnLeaveDays = s.EnjoyableEarnLeaveDays,
                    PayableEarnLeave = s.PayableEarnLeave,
                    Stamp = 10,
                    PayableEarnLeaveTk = s.PayableEarnLeaveTk
                }).ToList();

                VMELC.VMEarnLeaveCalculationList = emp;

                return View(VMELC);
            }

            return View(VMELC);
        }
        [HttpPost]
        public async Task<IActionResult> AddEmployeeEarnLeave(VMEarnLeaveCalculation vmelc)
        {
            if (vmelc.VMEarnLeaveCalculationList != null)
            {
                await _service.AddEmployeesEarnLeaveCalculation(vmelc);
            }
            return RedirectToAction("EarnLeaveCalculationList");
        }
       
        public IActionResult EarnLeaveCalculationList()
        {
            VMEarnLeaveCalculation VMELC = new VMEarnLeaveCalculation();
            return View(VMELC);
        }
        [HttpPost]
        public async Task<IActionResult> EarnLeaveCalculationList(VMEarnLeaveCalculation VMELC)
        {
            var eList = await _service.EmployeesEarnLeaveList(VMELC);
            if (eList != null)
            {
                var emp = eList.Select(s => new VMEarnLeaveCalculationList()
                {
                    ID = s.ID,
                    Unit = s.Unit,
                    Section = s.Section,
                    Department = s.Department,
                    Designation = s.Designation,
                    EmployeeIdentity = s.EmployeeIdentity,
                    Name = s.Name,
                    JoiningDate = s.JoiningDate,
                    LastPaymentDate = s.LastPaymentDate,
                    TotalMonth = s.TotalMonth,
                    WorkingDays = s.WorkingDays,
                    Holidays = s.Holidays,
                    Offdays = s.Offdays,
                    TotalLeave = s.TotalLeave,
                    Absent = s.Absent,
                    TotalDeductionDays = s.TotalDeductionDays,
                    PayableWorkingDays = s.PayableWorkingDays,
                    TotalEarnLeave = s.TotalEarnLeave,
                    EnjoyableEarnLeaveDays = s.EnjoyableEarnLeaveDays,
                    PayableEarnLeave = s.PayableEarnLeave,
                    Stamp = 10,
                    PayableEarnLeaveTk = s.PayableEarnLeaveTk
                }).ToList();

                VMELC.VMEarnLeaveCalculationList = emp;

                return View(VMELC);
            }
            return View(VMELC);
        }

        public IActionResult ServiceBenefitCalculation()
        {
            VMServiceBenefitCalculation vmsbc = new VMServiceBenefitCalculation();
            ViewBag.EmployeeList = new SelectList(_service.EmployeeDropDownList(0), "Value", "Text");
            return View(vmsbc);
        }
        [HttpPost]
        public async Task<IActionResult> ServiceBenefitCalculation(VMServiceBenefitCalculation vmsbc)
        {
            if (vmsbc.EmployeeID != 0)
            {
                var eInfo = await _service.GetEmployeesforServiceBenefit(vmsbc);
                if (eInfo != null)
                {
                    var eserviceInfo = eInfo.Select(s => new VMServiceBenefitCalculationList()
                    {
                        ID = s.ID,
                        Unit = s.Unit,
                        Section = s.Section,
                        Department = s.Department,
                        Designation = s.Designation,
                        EmployeeIdentity = s.EmployeeIdentity,
                        Name = s.Name,
                        JoiningDate = s.JoiningDate,
                        ResignationDate = s.ResignationDate,
                        Year = s.Year,
                        Month = s.Month,
                        Day = s.Day,
                        PayableServiceBenifitDays = s.PayableServiceBenifitDays,
                        PresentSalary = s.PresentSalary,
                        PresentBasic = s.PresentBasic,
                        PresentBasicPerDay = s.PresentBasicPerDay,
                        TotalAmount = s.TotalAmount,
                        Stamp = s.Stamp,
                        PayableAmount = s.PayableAmount
                    }).ToList();

                    vmsbc.VMServiceBenefitCalculationLists = eserviceInfo;
                }
            }
            ViewBag.EmployeeList = new SelectList(_service.EmployeeDropDownList(0), "Value", "Text");
            return View(vmsbc);
        }

        public async Task<IActionResult> AddEmployeeServiceBenefit(VMServiceBenefitCalculation vmsbc)
        {
            await _service.AddEmployeesServiceBenifitCalculation(vmsbc);
            return RedirectToAction("ServiceBenefitCalculationList");
        }

        public IActionResult ServiceBenefitCalculationList()
        {
            VMServiceBenefitCalculation VMSBC = new VMServiceBenefitCalculation();
            ViewBag.BUnits = new SelectList(_service.BusinessUnitDropDownList(), "Value", "Text");
            ViewBag.EmployeeList = new SelectList(_service.EmployeeDropDownList(0), "Value", "Text");
            return View(VMSBC);
        }
        [HttpPost]
        public async Task<IActionResult> ServiceBenefitCalculationList(VMServiceBenefitCalculation VMSBC)
        {
            var eList = await _service.EmployeesServiceBenefitList(VMSBC);

            if (eList != null)
            {
                var emp = eList.Select(s => new VMServiceBenefitCalculationList()
                {
                    ID = s.ID,
                    Section = s.Name,
                    EmployeeIdentity = s.EmployeeIdentity,
                    Name = s.Name,
                    JoiningDate = s.JoiningDate,
                    ResignationDate = s.JoiningDate,
                    Year = s.Year,
                    Month = s.Month,
                    Day = s.Day,
                    PayableServiceBenifitDays = s.PayableServiceBenifitDays,
                    PresentSalary = s.PresentSalary,
                    PresentBasic = s.PresentBasic,
                    PresentBasicPerDay = s.PresentBasicPerDay,
                    TotalAmount = s.TotalAmount,
                    Stamp = s.Stamp,
                    PayableAmount = s.PayableAmount
                }).ToList();
                VMSBC.VMServiceBenefitCalculationLists = emp;
                return View(VMSBC);
            }

            ViewBag.EmployeeList = new SelectList(_service.EmployeeDropDownList(0), "Value", "Text");
            return View(VMSBC);
        }

        [HttpGet]
        public async Task<JsonResult> GetEmployeeInfo(int id, JsonSerializerSettings serializerSettings)
        {
            var emp = await _service.getEmployeeInfo(id);

            return Json(emp, serializerSettings);
        }

        [HttpGet]
        public async Task<JsonResult> GetEmployeeLeaveInfo(int id, JsonSerializerSettings serializerSettings)
        {
            var emp = await _service.GetEmployeeLeaveInfo(id);

            return Json(emp, serializerSettings);
        }

        [HttpGet]
        public JsonResult getLeaveTypeDaysInfo(int id, JsonSerializerSettings serializerSettings)
        {
            var days = _service._db.HRMS_Leave.Where(s => s.ID == id).Select(s => s.LeaveDays).FirstOrDefault();
            return Json(days, serializerSettings);
        }
        #endregion

        #region Attendance

        public IActionResult DailyAttendance()
        {
            ViewBag.ShiftTypeList = new SelectList(_service.ShiftDropDownList(), "Value", "Text");
            ViewBag.BUnits = new SelectList(_service.BusinessUnitDropDownList(), "Value", "Text");
            return View();
        }

        public async Task<IActionResult> AttendanceHistory(VMAttendance vM)
        {
            VMAttendance model = new VMAttendance()
            {
                BusinessUnitId = vM.BusinessUnitId,
                DepartmentId = vM.DepartmentId,
                UnitId = vM.UnitId,
                SectionId = vM.SectionId,
                ShiftId = vM.ShiftId,
                EmployeeId = vM.EmployeeId,
                FromDate = vM.FromDate,
                ToDate = vM.ToDate
            };
            model.DataList = await _service.AttendanceHistoryListGet(new VMAttendance()
            {
                BusinessUnitId = vM.BusinessUnitId,
                DepartmentId = vM.DepartmentId,
                UnitId = vM.UnitId,
                SectionId = vM.SectionId,
                ShiftId = vM.ShiftId,
                EmployeeId = vM.EmployeeId,
                FromDate = vM.FromDate,
                ToDate = vM.ToDate
            });

            model.BusinessUnitList = await _service.BusinessUnitDropDownListAsync();
            model.EmployeeList = await _service.EmployeeDropDownListAsync();
            model.RemarksList = await _service.AttendanceRemarksDropDownListAsync();
            
            model.ShiftList = await _service.ShiftDropDownListAsync();

            if (model.BusinessUnitId > 0) { model.UnitList = await _service.UnitDropDownListAsync(model.BusinessUnitId); }
            if (model.UnitId > 0) { model.DepartmentList = await _IntegrationService.DepartmentDropDownListAsync(model.UnitId); }
            if (model.DepartmentID > 0) { model.SectionList = await _service.SectionDropDownListAsync(model.DepartmentID); }

            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " AttendanceHistoryListGet / BusinessUnitDropDownListAsync / EmployeeDropDownListAsync / AttendanceRemarksDropDownListAsync / ShiftDropDownListAsync / UnitDropDownListAsync / DepartmentDropDownListAsync / SectionDropDownListAsync, Attendance Page).");
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> UpdateAttendance(VMAttendance vM)
        {
            #region session values
            string sUser = HttpContext.Session.GetString("User");
            string nUserId = HttpContext.Session.GetString("UserID");
            #endregion
            if (string.IsNullOrEmpty(sUser) || string.IsNullOrEmpty(nUserId)) { return RedirectToAction("Login", "Auth"); }
            vM.User = sUser;
            vM.UserID = int.Parse(nUserId);

            int result = 0;
            if (vM.ActionEum == ActionEnum.Edit)
            {
                vM.Remarks = string.IsNullOrEmpty(vM.Remarks) ? "" : vM.Remarks.Trim();
                vM.ExtendedRemarks = string.IsNullOrEmpty(vM.ExtendedRemarks) ? "" : vM.ExtendedRemarks.Trim();
                vM.Remarks = vM.Remarks + (string.IsNullOrEmpty(vM.ExtendedRemarks) ? "" : " (" + vM.ExtendedRemarks + ")");
                result = await _service.AttendanceHistoryEdit(vM);
                _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " AttendanceHistoryEdit, Attendance operation).");
                return RedirectToAction("AttendanceHistory", "HRMS", new { vM.BusinessUnitId, vM.DepartmentId, vM.UnitId, vM.SectionId, vM.ShiftId, vM.EmployeeId, vM.FromDate, vM.ToDate });
            }
            else { _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " AttendanceHistoryEdit, Attendance operation failed)."); return RedirectToAction("Home", "Error"); }
        }
        public async Task<IActionResult> EmployeeGet(int id)
        {
            if (id < 0) { return RedirectToAction("Error", "Home"); }

            var model = await Task.Run(() => _service.EmployeeListGetAsync(id));



            var list = model.Select(x => new { Value = x.ID, Text = x.CardNo + " : " + x.Name }).ToList();
            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " EmployeeListGetAsync, Employee DDL by Shift ID).");
            return Json(list);
        }
        public async Task<IActionResult> AttendanceProcess()
        {
            VMAttendance model = new VMAttendance();

         
            model.ShiftList = await _service.ShiftDropDownListAsync();
            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " ShiftDropDownListAsync, Attendance Process Page).");
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> AttendanceProcess(VMAttendance vM)
        {
            #region session values
            string sUser = HttpContext.Session.GetString("User");
            string nUserId = HttpContext.Session.GetString("UserID");
            #endregion

            vM.User = sUser;
            vM.UserID = Convert.ToInt32(nUserId);


            vM.FromDate = vM.FromDate.HasValue ? vM.FromDate : DateTime.MinValue;
            vM.ToDate = vM.FromDate.HasValue ? vM.FromDate : DateTime.MinValue;

            await _service.AttendanceProcess(vM);
            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " AttendanceProcess, Attendance Process operation).");
            return RedirectToAction("AttendanceProcess", "HRMS");
        }

        public async Task<ActionResult> AttendanceDownload()
        {
            using (var webClient = new System.Net.WebClient())
            {
                var lastSourceID = _service.CheckInOutLastSourceId();
                bool reTry = true;
                while (reTry)
                {
                    try
                    {
                        var uri = new Uri("http://49.0.35.196/TempHRM/HRM/GetData/" + lastSourceID);
                        var json = webClient.DownloadString(uri);

                        //JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
                        List<HRMS_CheckInOuts> checkInOut = JsonConvert.DeserializeObject<List<HRMS_CheckInOuts>>(json);

                        checkInOut.ForEach(x => { x.SourceId = x.ID; x.CheckTime = x.CheckTime.AddHours(6); x.ID = 0; });

                        //foreach (var v in checkInOut)
                        //{
                        //    v.SourceId = v.ID;
                        //    v.CheckTime = v.CheckTime.AddHours(6);
                            
                        //}
                        if(checkInOut!=null && checkInOut.Count > 0) { reTry = checkInOut[0].Retry; }
                        else { reTry = false; }
                        
                        await _service.CheckInOutDataDump(checkInOut);
                        if (reTry)
                        {
                            lastSourceID =  _service.CheckInOutLastSourceId();
                        }
                    }
                    catch (Exception ex)
                    {
                        ex.Message.ToString();
                    }
                }
            }
            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + " Attendance Download).");
            return RedirectToAction("AttendanceProcess", "HRMS");
        }
        #endregion

        #region General Settings
        public async Task<IActionResult> BusinessUnitSetup(string error, int id = 0)
        {
            HRMSBusinessUnitVM BUVM = new HRMSBusinessUnitVM();
            if (id != 0)
            {
                await _service.AddEditOrDeleteBusinessUnit(id, null, BUVM);
            }
            BUVM.BULists = await _service.GetBusinessUnit();

            if (error != null)
            {
                BUVM.error = error;
            }
            return View(BUVM);
        }
        [HttpPost]
        public async Task<IActionResult> BusinessUnitSetup(HRMSBusinessUnitVM BUVM)
        {
            if (ModelState.IsValid)
            {
                await _service.AddEditOrDeleteBusinessUnit(null, null, BUVM);
            }
            BUVM.BULists = await _service.GetBusinessUnit();
            return RedirectToAction("BusinessUnitSetup", "HRMS", new { id = 0 });
        }

        public async Task<IActionResult> DeleteBusinessUnit(int id)
        {
            HRMSBusinessUnitVM BUVM = new HRMSBusinessUnitVM();
            await _service.AddEditOrDeleteBusinessUnit(id, "Delete", BUVM);
            return RedirectToAction("BusinessUnitSetup", "HRMS", new { BUVM.error, id = 0 });
        }

        public async Task<IActionResult> DesignationSetup(string error, int id = 0)
        {
            HRMSDesignationVM HDVM = new HRMSDesignationVM();
            if (id != 0)
            {
                await _service.AddEditOrDeleteDesignation(id, null, HDVM);
            }
            ViewBag.HSL = _service._db.HRMS_Designation.Where(hd => hd.Active == true).ToList();
            ViewBag.HierarchyLabelDropDownList = new SelectList(_service.CompanyHierarchyLabelDropDownList(), "Value", "Text");
            ViewBag.DepartmentDropDownList = new SelectList(_service.DepartmentDropDownList(), "Value", "Text");
            HDVM.DataList = await _service.HRMSDesignationListGet();
            if(error != null)
            {
                HDVM.error = error;
            }
            return View(HDVM);
        }

        [HttpPost]
        public async Task<IActionResult> DesignationSetup(HRMSDesignationVM BUVM)
        {
            if (ModelState.IsValid)
            {
                await _service.AddEditOrDeleteDesignation(null, null, BUVM);
            }
            ViewBag.HSL = _service._db.HRMS_Designation.Where(hd => hd.Active == true && hd.IsAttendanceBonusGiven == true).ToList();
            ViewBag.HierarchyLabelDropDownList = new SelectList(_service.CompanyHierarchyLabelDropDownList(), "Value", "Text");
            ViewBag.DepartmentDropDownList = new SelectList(_service.DepartmentDropDownList(), "Value", "Text");
            return RedirectToAction("DesignationSetup", "HRMS", new { id = 0});
        }

        public async Task<IActionResult> DeleteDesignation(int id)
        {
            HRMSDesignationVM DVM = new HRMSDesignationVM();
            await _service.AddEditOrDeleteDesignation(id, "Delete", DVM);
            return RedirectToAction("DesignationSetup", "HRMS", new { DVM.error, id = 0 });
        }

        public async Task<IActionResult> DesignationWiseAttendanceBonus(int id = 0)
        {
            HRMSDesignationVM HDVM = new HRMSDesignationVM();
            if (id != 0)
            {
                await _service.AddEditOrDeleteDesignation(id, null, HDVM);
            }
            HDVM.DataList = await _service.HRMSDesignationGet();
            ViewBag.DesignationDropDownList = new SelectList(await _service.DesignationDropDownListAsync(), "Value", "Text");
            HDVM.VMLevelList = await _service.EmployeeHierarchyLevelList();
            return View(HDVM);
        }
        [HttpPost]
        public async Task<IActionResult> DesignationWiseAttendanceBonus(HRMSDesignationVM BUVM)
        {

            await _service.AddEditOrDeleteDesignation(null, null, BUVM);

            BUVM.DataList = await _service.HRMSDesignationGet();
            ViewBag.DesignationDropDownList = new SelectList(await _service.DesignationDropDownListAsync(), "Value", "Text");
            return RedirectToAction("DesignationWiseAttendanceBonus", "HRMS", new { id = 0 });
        }

        public async Task<IActionResult> UnitSetup(string error, int id = 0)
        {
            HRMSUnitVM HUVM = new HRMSUnitVM();
            if (id != 0)
            {
                await _service.AddEditOrDeleteUnit(id, null, HUVM);
            }
            HUVM.DataList = await _service.HRMSUnitListGet();
            HUVM.VMBusinessUnitList = await _service.BusinessUnitDropDownListAsync();
            if(error != null)
            {
                HUVM.error = error;
            }
            return View(HUVM);
        }
        [HttpPost]
        public async Task<IActionResult> UnitSetup(HRMSUnitVM HUVM)
        {
            if (ModelState.IsValid)
            {
                await _service.AddEditOrDeleteUnit(null, null, HUVM);
            }
            return RedirectToAction("UnitSetup", "HRMS", new { id = 0 });
        }
        public async Task<IActionResult> DeleteUnit(int id)
        {
            HRMSUnitVM HUVM = new HRMSUnitVM();
            await _service.AddEditOrDeleteUnit(id, "Delete", HUVM);
            return RedirectToAction("UnitSetup", "HRMS", new { HUVM.error, id = 0 });
        }

        public async Task<IActionResult> SectionSetup(string error, int id = 0)
        {

            HRMSSectionVM HSVM = new HRMSSectionVM();
            if (id != 0)
            {
                await _service.AddEditOrDeleteSection(id, null, HSVM);
                var unitList = await _service.UnitListGetByBUIdAsync(HSVM.VMBusinessUnitID.Value);
                HSVM.VMUnitList = new SelectList(unitList.Select(x => new { Value = x.ID, Text = x.Name }), "Value", "Text");
                var departmentList = await _IntegrationService.DepartmentListGetByUIdAsync(HSVM.VMUnitID.Value);
                HSVM.VMDepartmentList = new SelectList(departmentList.Select(x => new { Value = x.ID, Text = x.Name }), "Value", "Text");
            }
            if(error != null)
            {
                HSVM.error = error;
            }
            HSVM.DataList = await _service.HRMSSectionListGet();
            HSVM.VMBusinessUnitList = await _service.BusinessUnitDropDownListAsync();
            return View(HSVM);
        }
        [HttpPost]
        public async Task<IActionResult> SectionSetup(HRMSSectionVM HSVM)
        {
            if (ModelState.IsValid)
            {
                await _service.AddEditOrDeleteSection(null, null, HSVM);
            }
            
            return RedirectToAction("SectionSetup", "HRMS", new { id = 0 });
        }

        public async Task<IActionResult> DeleteSection(int id)
        {
            HRMSSectionVM HSVM = new HRMSSectionVM();
            await _service.AddEditOrDeleteSection(id, "Delete", HSVM);
            return RedirectToAction("SectionSetup", "HRMS", new { HSVM.error, id = 0});
        }

        [HttpGet]
        public async Task<IActionResult> UnitGet(int id = 0)
        {
            var list = await _service.UnitListGetByBUIdAsync(id);
            var retlist = list.Select(x => new { Value = x.ID, Text = x.Name }).ToList();
            return Json(retlist);
        }
        [HttpGet]
        public async Task<IActionResult> UserDepartmentGet(int id = 0)
        {
            var list = await _IntegrationService.DepartmentListGetByUIdAsync(id);
            var retlist = list.Select(x => new { Value = x.ID, Text = x.Name }).ToList();
            return Json(retlist);

        }
        [HttpGet]
        public async Task<IActionResult> SectionGet(int id = 0)
        {
            var list = await _service.SectionListGetByDIdAsync(id);
            var retlist = list.Select(x => new { Value = x.ID, Text = x.Name }).ToList();
            return Json(retlist);
        }
        [HttpGet]
        public async Task<IActionResult> HierarchyLevelWiseDesignationGet(int id = 0)
        {
            var list = await _service.DesignationListGetByLIdAsync(id);
            var retlist = list.Select(x => new { Value = x.ID, Text = x.Name }).ToList();
            return Json(retlist);
        }
        [HttpGet]
        public async Task<IActionResult> DesignationGet(int id = 0)
        {
            var list = await _service.DesignationListGetByDIdAsync(id);
            var retlist = list.Select(x => new { Value = x.ID, Text = x.Name }).ToList();
            return Json(retlist);
        }

        public async Task<IActionResult> CompanySetup(int id = 0)
        {
            CommonCompanyVM CCVM = new CommonCompanyVM();
            if (id != 0)
            {
                await _service.AddEditOrDeleteCompany( null, id, null, CCVM);
            }
            CCVM.DataList = await _service.CommonCompanyGet();
            return View(CCVM);
        }
        [HttpPost]
        public async Task<IActionResult> CompanySetup(CommonCompanyVM CCVM, IFormFile files)
        {
            if (ModelState.IsValid)
            {
                if (files != null)
                {
                    using (var ms = new MemoryStream())
                    {
                        files.CopyTo(ms);
                        var A = ms.ToArray();
                        await _service.AddEditOrDeleteCompany(A, null, null, CCVM);
                    }
                }
                else
                {
                    await _service.AddEditOrDeleteCompany(null, null, null, CCVM);
                }
                
            }
            return RedirectToAction("CompanySetup", "HRMS", new { id = 0 });
        }

        public async Task<IActionResult> DeleteCompany(int id)
        {
            await _service.AddEditOrDeleteCompany(null, id, "Delete", null);
            return RedirectToAction("CompanySetup", "HRMS", new { id = 0 });
        }

        public async Task<IActionResult> OffdayAssign(string error, int id = 0)
        {
            VMOffDay VMOD = new VMOffDay();
            if (id != 0)
            {
                await _service.AddEditOrDeleteOffDay(id, null, VMOD);
            }
            ViewBag.Offdays = _service._db.HRMS_OffDay.Where(o=>o.Active== true).OrderByDescending(O=>O.ID).ToList();
            if(error != null)
            {
                VMOD.error = error;
            }
            return View(VMOD);
        }

        [HttpPost]
        public async Task<IActionResult> OffdayAssign(VMOffDay VMOD)
        {
            if (ModelState.IsValid)
            {
                await _service.AddEditOrDeleteOffDay(null, null, VMOD);
            }
            ViewBag.Offdays = _service._db.HRMS_OffDay.ToList();
            return RedirectToAction("OffdayAssign", "HRMS", new { id = 0 });
        }

        public async Task<IActionResult> DeleteOffDay(int id)
        {
            VMOffDay VOD = new VMOffDay();
            await _service.AddEditOrDeleteOffDay(id, "Delete", VOD);
            return RedirectToAction("OffdayAssign", "HRMS", new { VOD.error, id = 0 });
        }

        public async Task<IActionResult> HolidayAssign(int id = 0)
        {
            VMHoliday VMH = new VMHoliday();
            if (id != 0)
            {
                await _service.AddEditOrDeleteHolidayDay(id, null, VMH, "HolidayAssign");
            }
            VMH.VMHolidays = await _service.HolidayGet();

            return View(VMH);
        }

        [HttpPost]
        public async Task<IActionResult> HolidayAssign(VMHoliday VMH)
        {
            if (ModelState.IsValid)
            {
                await _service.AddEditOrDeleteHolidayDay(null, null, VMH, "HolidayAssign");
            }
            VMH.VMHolidays = await _service.HolidayGet();
            return RedirectToAction("HolidayAssign", "HRMS", new { id = 0 });
        }

        public async Task<IActionResult> DeleteHolidayAssign(int id)
        {
            await _service.AddEditOrDeleteHolidayDay(id, "Delete", null, "HolidayAssign");
            return RedirectToAction("HolidayAssign", "HRMS", new { id = 0 });
        }

        public async Task<IActionResult> LeaveSetup(string error, int id = 0)
        {
            VMLeaveSetup VMLS = new VMLeaveSetup();
            if (id != 0)
            {
                await _service.AddEditOrDeleteLeaveDay(id, null, VMLS);
            }
            ViewBag.EmployeeType = new SelectList(_service.GetEmployeeType(), "Value", "Text");
            ViewBag.LeaveType = new SelectList(_service.FixedLeaveTypeDropDownList(), "Value", "Text");
            ViewBag.Leave = _service._db.HRMS_Leave.Where(l=>l.Active == true).ToList();
            if(error != null)
            {
                VMLS.error = error;
            }
            return View(VMLS);
        }
        [HttpPost]
        public async Task<IActionResult> LeaveSetup(VMLeaveSetup VMLS)
        {
            if (ModelState.IsValid)
            {
                await _service.AddEditOrDeleteLeaveDay(null, null, VMLS);
            }
            ViewBag.EmployeeType = new SelectList(_service.GetEmployeeType(), "Value", "Text");
            ViewBag.LeaveType = new SelectList(_service.FixedLeaveTypeDropDownList(), "Value", "Text");
            ViewBag.Leave = _service._db.HRMS_Leave.Where(l=>l.Active==true).ToList();
            return RedirectToAction("LeaveSetup", "HRMS", new { VMLS.error, id = 0 });
        }

        public async Task<IActionResult> DeleteLeave(int id)
        {
            VMLeaveSetup VMLS = new VMLeaveSetup();
            await _service.AddEditOrDeleteLeaveDay(id, "Delete", VMLS);
            return RedirectToAction("LeaveSetup", "HRMS", new { VMLS.error, id = 0 });
        }

        public async Task<IActionResult> EmployeehierarchyLevel(string error, int id = 0)
        {
            HRMSHierarchyVM BUVM = new HRMSHierarchyVM();
            if (id != 0)
            {
                await _service.AddEditOrDeleteCompanyHierarchyLevel(id, null, BUVM);
            }
            if(error != null)
            {
                BUVM.error = error;
            }
            ViewBag.HSL = _service._db.HRMS_CompanyHierarchyLabel.Where(a=>a.Active != false).ToList();
            return View(BUVM);
        }
        [HttpPost] 
        public async Task<IActionResult> EmployeehierarchyLevel(HRMSHierarchyVM BUVM)
        {
            if (ModelState.IsValid)
            {
                await _service.AddEditOrDeleteCompanyHierarchyLevel(null, null, BUVM);
            }
            ViewBag.HSL = _service._db.HRMS_CompanyHierarchyLabel.Where(a => a.Active != false).ToList();
            return RedirectToAction("EmployeehierarchyLevel", "HRMS", new { id = 0 });
        }
        public async Task<IActionResult> DeleteEmployeehierarchyLevel(int id)
        {
            HRMSHierarchyVM HHVM = new HRMSHierarchyVM();
            await _service.AddEditOrDeleteCompanyHierarchyLevel(id, "Delete", HHVM);
            return RedirectToAction("EmployeehierarchyLevel", "HRMS", new { HHVM.error, id = 0 });
        }

        public async Task<IActionResult> DistrictSetup(string error, int id = 0)
        {
            HRMSDistrictVM HDVM = new HRMSDistrictVM();
            HDVM.CountryList = await _service.CountryDropDownListAsync();
            if (id != 0)
            {
                await _service.AddEditOrDeleteDistrict(id, null, HDVM);
            }
            HDVM.DataList = await _service.CountryToPostOficeList(1);
            HDVM.CountryList = await _service.CountryDropDownListAsync();
            if(error != null)
            {
                HDVM.error = error;
            }
            return View(HDVM);
        }
        [HttpPost]
        public async Task<IActionResult> DistrictSetup(HRMSDistrictVM HDVM)
        {
            HDVM.CountryList = await _service.CountryDropDownListAsync();
            if (ModelState.IsValid)
            {
                await _service.AddEditOrDeleteDistrict(null, null, HDVM);
            }

            return RedirectToAction("DistrictSetup", "HRMS", new { id = 0 });
        }
        public async Task<IActionResult> DeleteDistrict(int id)
        {
            HRMSDistrictVM HDVM = new HRMSDistrictVM();
            await _service.AddEditOrDeleteDistrict(id, "Delete", HDVM);
            return RedirectToAction("DistrictSetup", "HRMS", new { HDVM.error, id = 0});
        }

        public async Task<IActionResult> PoliceStationSetup( string error, int id = 0)
        {
            HRMSPoliceStationVM HSVM = new HRMSPoliceStationVM();
            HSVM.CountryList = await _service.CountryDropDownListAsync();
            
            if (id != 0)
            {
                HSVM.DistrictList = await _service.DistrictDropDownListAsync();
                await _service.AddEditOrDeletePoliceStation(id, null, HSVM);
            }

            HSVM.DataList = await _service.CountryToPostOficeList(2);
            if(error != null)
            {
                HSVM.error = error;
            }
            return View(HSVM);
        }
        [HttpPost]
        public async Task<IActionResult> PoliceStationSetup(HRMSPoliceStationVM HSVM)
        {
            if (ModelState.IsValid)
            {
                await _service.AddEditOrDeletePoliceStation(null, null, HSVM);
            }

            return RedirectToAction("PoliceStationSetup", "HRMS", new { id = 0 });
        }
        public async Task<IActionResult> DeletePoliceStation(int id)
        {
            HRMSPoliceStationVM HPSVM = new HRMSPoliceStationVM();
            await _service.AddEditOrDeletePoliceStation(id, "Delete", HPSVM);
            return RedirectToAction("PoliceStationSetup", "HRMS", new { HPSVM.error, id = 0 });
        }

        public async Task<IActionResult> DegreeSetup(string error, int id = 0)
        {
            HRMSDegreeVM Deg = new HRMSDegreeVM();
            //Deg.Deglist = await _service.DegreeList();

            if (id != 0)
            {
                await _service.AddEditOrDeleteDegree(id, null, Deg);
            }

            Deg.DegList = await _service.DegreeList();
            if (error != null)
            {
                Deg.error = error;
            }
            return View(Deg);
        }
        [HttpPost]
        public async Task<IActionResult> DegreeSetup(HRMSDegreeVM HDVM)
        {
            if (ModelState.IsValid)
            {
                await _service.AddEditOrDeleteDegree(null, null, HDVM);
            }

            return RedirectToAction("DegreeSetup", "HRMS", new { id = 0 });
        }
        public async Task<IActionResult> DeleteDegree(int id)
        {
            HRMSDegreeVM HDVM = new HRMSDegreeVM();
            await _service.AddEditOrDeleteDegree(id, "Delete", HDVM);
            return RedirectToAction("DegreeSetup", "HRMS", new { HDVM.error, id = 0 });
        }

        public async Task<IActionResult> PostOfficeSetup(string error, int id = 0)
        {
            HRMSPostOfficeVM HPOVM = new HRMSPostOfficeVM();
            if (id != 0)
            {
                await _service.AddEditOrDeletePostOffice(id, null, HPOVM);
                HPOVM.DistrictList = await _service.DistrictDropDownListAsync();
                HPOVM.PoliceStationList = await _service.PoliceStationDropDownListAsync();
            }
            HPOVM.DataList = await _service.CountryToPostOficeList(3);
            HPOVM.CountryList = await _service.CountryDropDownListAsync();
            if(error != null)
            {
                HPOVM.error = error;
            }
            return View(HPOVM);
        }
        [HttpPost]
        public async Task<IActionResult> PostOfficeSetup(HRMSPostOfficeVM HPOVM)
        {
            if (ModelState.IsValid)
            {
                await _service.AddEditOrDeletePostOffice(null, null, HPOVM);
            }

            return RedirectToAction("PostOfficeSetup", "HRMS", new { id = 0 });
        }
        public async Task<IActionResult> DeletePostOffice(int id)
        {
            HRMSPostOfficeVM HPOVM = new HRMSPostOfficeVM();
            await _service.AddEditOrDeletePostOffice(id, "Delete", HPOVM);
            return RedirectToAction("PostOfficeSetup", "HRMS", new { HPOVM.error, id = 0});
        }

        public async Task<IActionResult> JobDescriptionSetup(int id = 0)
        {
            HRMSJobDescriptionVM HJDVM = new HRMSJobDescriptionVM();
            if (id != 0)
            {
                await _service.AddEditOrDeleteJobDescription(id, null, HJDVM);
            }
            HJDVM.DataList = await _service.HRMSJobDescriptionGet();
            HJDVM.VMDepartmentList = await _service.DepartmentDropDownListAsync();
            //HJDVM.VMDesignationList = await _service.DesignationDropDownListAsync();

            return View(HJDVM);
        }
        [HttpPost]
        public async Task<IActionResult> JobDescriptionSetup(HRMSJobDescriptionVM HJDVM)
        {
            if (ModelState.IsValid)
            {
                await _service.AddEditOrDeleteJobDescription(null, null, HJDVM);
            }
            return RedirectToAction("JobDescriptionSetup", "HRMS", new { id = 0 });
        }
        public async Task<IActionResult> DeleteJobDescription(int id)
        {
            await _service.AddEditOrDeleteJobDescription(id, "Delete", null);
            return RedirectToAction("JobDescriptionSetup", "HRMS", new { id = 0 });
        }

        public async Task<IActionResult> AttendanceRemarks(int id = 0)
        {
            VMAttendanceRemarks model = new VMAttendanceRemarks();
            if (id > 0)
            {
                model = await _service.AttendanceRemarksSpecificGet(id);
            }
            model.DataList = await _service.AttendanceRemarksListGet();

            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> AttendanceRemarks(VMAttendanceRemarks model)
        {
            #region session values
            string sUser = HttpContext.Session.GetString("User");
            string nUserId = HttpContext.Session.GetString("UserID");
            #endregion
            if (string.IsNullOrEmpty(sUser) || string.IsNullOrEmpty(nUserId)) { return RedirectToAction("Error", "Home"); }
            model.User = sUser;
            model.UserID = int.Parse(nUserId);
            if (model.ActionEum == ActionEnum.Add)
            {
                await _service.AttendanceRemarksAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                await _service.AttendanceRemarksEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                await _service.AttendanceRemarksDelete(model.ID);
            }
            else { return RedirectToAction("Error", "Home"); }
            return RedirectToAction("AttendanceRemarks", "HRMS");
        }
        #endregion

        #region Form
        public IActionResult EmployeeForm()
        {
            EmployeeFormVM EFVM = new EmployeeFormVM();
            ViewBag.EmployeeCardList = new SelectList(_service.EmployeeDropDownList(0), "Value", "Text");
            var unitList = new SelectList(_service.EmployeeDropDownList(0), "Value", "Text");
            EFVM.EmployeeList = new SelectList(unitList.Select(x => new { Value = x.Value, Text = x.Text }), "Value", "Text");
            return View(EFVM);
        }
        [HttpPost]
        public async Task<IActionResult> EmployeeForm(EmployeeFormVM EFVM)
        {
            EFVM.EmployeeInfo = await _service.getEmployeeInfo(EFVM.EmployeeId);
            return View(EFVM);
        }

        [HttpPost]
        public IActionResult MaternityLeaveForm(EmployeeFormVM EFVM)
        {
            return View(EFVM);
        }
        
        [HttpPost]
        public IActionResult ApplicationForm(EmployeeFormVM EFVM)
        {
            return View(EFVM);
        }

        [HttpPost]
        public IActionResult JobDescriptionForm(EmployeeFormVM EFVM)
        {
            return View(EFVM);
        }
        #endregion
    }
}