﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Erp.Infrastructure;
using Erp.Core.Services.Accounting;
using Erp.Core.Services.Home;
using Microsoft.Extensions.Logging;

using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Http;
using Erp.Core.Services.User;
using Erp.Core.Services.Merchandising;
using Erp.Core.Services;
using Erp.App.Models;

namespace Erp.App.Controllers
{
    
    public class AuthController : Controller
    {
       
        private readonly ILogger _logger;       
        private readonly HttpContext httpContext;
        private readonly HomeService _service;
        
        public AuthController(InfrastructureDbContext db, IHttpContextAccessor httpContextAccessor, ILogger<HomeController> logger)
        {
            httpContext = httpContextAccessor.HttpContext;

            _service = new HomeService(db, httpContextAccessor.HttpContext);
           // SessionHandler sessionHandler = new SessionHandler(db,httpContextAccessor.HttpContext);
           // sessionHandler.Adjust();
            _logger = logger;
        }
        public IActionResult Privacy()
        {
            return View();
        }
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        private void ClearAllSessionAndCookies()
        {
            httpContext.Session.Clear();
            foreach (var kookiesKeys in httpContext.Request.Cookies.Keys)
            {
                Response.Cookies.Delete(kookiesKeys);

            }

        }

        public IActionResult Login()
        {
            ClearAllSessionAndCookies();
            
            return View();
        }

       

        [HttpPost]
        public IActionResult Login(VMLogin model)
        {
            if (model.UserName == null || model.Password == null)
            {
                model.LblError = "Please enter Username and Password";
                return View(model);
            }
            var user = _service.UserLogin(model.UserName, model.Password);
            if (user.LblError != null)
            {
                return View(user);
            }
            if (user.LblError == null)
            {
                //Store 
                httpContext.Session.SetObjectAsJson("LoginUser", user);

                httpContext.Session.SetString("User", user.UserName);
                httpContext.Session.SetString("UserID", user.ID.ToString());
                httpContext.Session.SetString("UserAccessLevelId", user.UserAccessLevelId.ToString());
                httpContext.Session.SetString("UserDepartmentId", user.UserDepartmentId.ToString());

                httpContext.Session.SetInt32("EmployeeID", user.EmployeeId);
                // MvcApplication.userName  = VmUser_User.User_User.Name;
                //Store User in Cookie


                CookieOptions cookieOptions = new CookieOptions();
                Response.Cookies.Append("UserNameCookies", model.UserName);
                Response.Cookies.Append("PasswordCookies", model.Password);
               
                Response.Cookies.Append("userId", user.ID.ToString());
                cookieOptions.Expires = DateTime.Now.AddDays(1);
                if (user.UserName == "Mis")
                {
                    return RedirectToAction("Dashboard", "Mobile");

                }
                return RedirectToAction("Index", "Home");

            }
            else
            {
                model.LblError = "User/Password does not match!";
            }
            return RedirectToAction("Login", "Home");

            //to retrieve session value.
            //var myComplexObject = HttpContext.Session.GetObjectFromJson<VMLogin>("Users");


            //if (model.UserName == "Accountant" && model.Password == "RomoAccount")
            //{
            //    //Updated by Mintu                 
            //    HttpContext.Session.SetString(ControllerName, (string)RouteData.Values["controller"]);
            //    HttpContext.Session.SetString(ActionName, (string)RouteData.Values["action"]);
            //    HttpContext.Session.SetString(SessionName, model.UserName);
            //    HttpContext.Session.SetInt32(SessionId, 1);
            //    var name = HttpContext.Session.GetString(SessionName);
            //    //Updated by Mintu

            //    return RedirectToAction("Index", "Home");
            //}

            //return RedirectToAction("Index", "Home");
        }

       
    }
}
