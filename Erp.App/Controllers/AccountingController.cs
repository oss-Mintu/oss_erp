﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Erp.Core.Services.Accounting;
using Microsoft.Extensions.Logging;
using Erp.Infrastructure;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Routing;
using Erp.Core.Services;
using Microsoft.AspNetCore.Http;
using Erp.Core.Services.User;
using Erp.Core.Services.Procurement;
using Erp.Core.Services.Integration;
using Erp.Core.Services.Shipment;
using Erp.Core.Services.Home;
using Erp.Core.Services.Commercial;

namespace Erp.App.Controllers
{
    [SoftwareAdmin]
    public class AccountingController : Controller
    {
        private readonly SessionHandler _sessionHandler;
        private readonly ILogger _logger;
        private readonly AccountingService _service;
        private readonly AccountingPartialService _accountingPartialService;
        private readonly IntegrationService _integrationService;
        private int UserID;
        public AccountingController(InfrastructureDbContext db, ILogger<AccountingController> logger,IHttpContextAccessor httpContextAccessor)
        {
            _accountingPartialService = new AccountingPartialService(db, httpContextAccessor.HttpContext);
            _integrationService = new IntegrationService(db, httpContextAccessor.HttpContext);
            _service = new AccountingService(db);
            _sessionHandler = new SessionHandler(db, httpContextAccessor.HttpContext);
            _sessionHandler.Adjust();
            _logger = logger;
            UserID = Convert.ToInt32(httpContextAccessor.HttpContext.Session.GetString("UserID"));
        }
        public async Task<IActionResult> Index()
        {
            VMPurchaseOrder model = new VMPurchaseOrder();
            var fromdt = DateTime.Now.AddDays(-7);
            var todate = DateTime.Now;
            int suplierId = 0;
            ViewBag.POList = await Task.Run(() => _integrationService.POListGetForAccounts(fromdt, todate, suplierId));
            return View();
        }

        public IActionResult Ledger()
        {
            return View();
        }
        #region Journal Create
        public async Task<IActionResult> Journal()
        {
            ViewBag.AccountCostCenterList = new SelectList(_service.AccountCostCenterDropDownList(), "Value", "Text");
            //1 is journal type
            var model = await Task.Run(() => _service.JournalGet(1));
            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + "JournalGet(), Journal Voucher Get.");
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> Journal(VMJournal model)
        {
            // 1 means Journal voucher
            model.JournalType = 1;
           model.UserID =UserID;
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                model.Finalized = false;
                model.Approved = false;
                await _service.JournalAdd(model);
                _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + "JournalAdd(), Journal Voucher Add.");
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.JournalEdit(model);
                _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + "JournalEdit(), Journal Voucher Edit.");
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.JournalDelete(model.ID);
                _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + "JournalDelete(), Journal Voucher Delete.");
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("Journal");
        }
        public async Task<IActionResult> JournalDetails(int id)
        {
            var model = await Task.Run(() => _service.JournalDetailsGet(id));
            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + "JournalDetailsGet(), Single Journal Voucher Details Get by ID.");
            model.AmountType = 1;
            ViewBag.AccountTypeList = new SelectList(_service.AccountTypeDropDownList(), "Value", "Text");
            ViewBag.ChartOneList = new SelectList(_service.ChartOneDropDownList(), "Value", "Text");
            ViewBag.ChartTwoList = new SelectList(_service.ChartTwoDropDownList(), "Value", "Text");
            ViewBag.AccountHeadList = new SelectList(_service.AccountHeadDropDownList(), "Value", "Text");
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> JournalDetails(VMJournalSlave model)
        {

           model.UserID =UserID;
            if (model.AmountType == 0)
            {
                return RedirectToAction("JournalDetails", "Accounting", new RouteValueDictionary(new { id = model.Accounting_JournalFK }));
            }
            else
            {
                if (model.ActionEum == ActionEnum.Add)
                {
                    //Add 
                    if (model.Accounting_HeadFK != 0 && model.AmountType != 0)
                    {
                        model.Finalized = false;
                        await _service.JournalDetailsAdd(model);
                        _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + "JournalDetailsAdd(), Journal Details Add.");
                    }
                    return RedirectToAction("JournalDetails", "Accounting", new RouteValueDictionary(new { id = model.Accounting_JournalFK }));
                }
                else if (model.ActionEum == ActionEnum.Edit)
                {
                    //Edit
                    if (model.Accounting_HeadFK != 0 && model.AmountType != 0)
                    {
                        model.Finalized = false;
                        await _service.JournalDetailsEdit(model);
                        _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + "JournalDetailsEdit(), Journal Details Edit.");
                    }
                    return RedirectToAction("JournalDetails", "Accounting", new RouteValueDictionary(new { id = model.Accounting_JournalFK }));

                }
                else if (model.ActionEum == ActionEnum.Delete)
                {
                    //Delete
                    await _service.JournalDetailsDelete(model.ID);
                    _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + "JournalDetailsDelete(), Journal Details Delete.");
                }
                else
                {
                    return RedirectToAction("Error");
                }
            }
            return RedirectToAction("JournalDetails", "Accounting", new RouteValueDictionary(new { id = model.Accounting_JournalFK }));
        }

        public async Task<IActionResult> JournalFinalize(int id)
        {
            if (id != 0)
            {
                var output = await Task.Run(() => _service.JournalFinalized(id));
                _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + "JournalFinalized(), Single Journal Finalize in Journal Voucher.");
            }
            return RedirectToAction("Journal", "Accounting");
        }

        public async Task<IActionResult> JournalApprove(int id)
        {
            if (id != 0)
            {
                var output = await Task.Run(() => _service.JournalApproved(id));
                _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + "JournalApproved(), Single Journal Approved in Journal Voucher.");
            }
            return RedirectToAction("Journal", "Accounting");
        }

        #endregion

        #region   DEBIT Voucher
        public async Task<IActionResult> DebitVoucher()
        {
            ViewBag.AccountCostCenterList = new SelectList(_service.AccountCostCenterDropDownList(), "Value", "Text");
            //2 is Debit Voucher Type
            var model = await Task.Run(() => _service.JournalGet(2));
            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + "JournalGet(2), Debit Voucher Get.");
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> DebitVoucher(VMJournal model)
        {
            // 2 means Debit voucher
            model.JournalType = 2;
           model.UserID =UserID;
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                model.Finalized = false;
                model.Approved = false;
                await _service.JournalAdd(model);
                _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + "JournalAdd(), Debit Voucher Add.");
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.JournalEdit(model);
                _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + "JournalEdit(), Debit Voucher Edit.");
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.JournalDelete(model.ID);
                _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + "JournalDelete(), Debit Voucher Delete.");
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("DebitVoucher");
        }


        public async Task<IActionResult> DebitVoucherDetails(int id)
        {

            var model = await Task.Run(() => _service.JournalDetailsGet(id));
            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + "JournalDetailsGet(), Single Debit Voucher Details Get by ID.");
            model.AmountType = 1;
            ViewBag.AccountTypeList = new SelectList(_service.AccountTypeDropDownList(), "Value", "Text");
            ViewBag.ChartOneList = new SelectList(_service.ChartOneDropDownList(), "Value", "Text");
            ViewBag.ChartTwoList = new SelectList(_service.ChartTwoDropDownList(), "Value", "Text");
            ViewBag.AccountHeadList = new SelectList(_service.AccountHeadDropDownList(), "Value", "Text");
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> DebitVoucherDetails(VMJournalSlave model)
        {
           model.UserID =UserID;
            if (model.AmountType == 0)
            {
                return RedirectToAction("DebitVoucherDetails", "Accounting", new RouteValueDictionary(new { id = model.Accounting_JournalFK }));
            }
            else
            {
                if (model.ActionEum == ActionEnum.Add)
                {
                    //Add 
                    if (model.Accounting_HeadFK != 0 && model.AmountType != 0)
                    {
                        model.Finalized = false;
                        await _service.JournalDetailsAdd(model);
                        _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + "JournalDetailsAdd(), Journal Details Add in Debit Voucher.");
                    }
                    return RedirectToAction("DebitVoucherDetails", "Accounting", new RouteValueDictionary(new { id = model.Accounting_JournalFK }));
                }
                else if (model.ActionEum == ActionEnum.Edit)
                {
                    //Edit
                    if (model.Accounting_HeadFK != 0 && model.AmountType != 0)
                    {
                        model.Finalized = false;
                        await _service.JournalDetailsEdit(model);
                        _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + "JournalDetailsEdit(), Journal Details Edit in Debit Voucher.");
                    }
                    return RedirectToAction("DebitVoucherDetails", "Accounting", new RouteValueDictionary(new { id = model.Accounting_JournalFK }));

                }
                else if (model.ActionEum == ActionEnum.Delete)
                {
                    //Delete
                    await _service.JournalDetailsDelete(model.ID);
                    _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + "JournalDetailsDelete(), Journal Details Delete in Debit Voucher.");
                }
                else
                {
                    return RedirectToAction("Error");
                }
            }
            return RedirectToAction("DebitVoucherDetails", "Accounting", new RouteValueDictionary(new { id = model.Accounting_JournalFK }));
        }

        public async Task<IActionResult> DebitVoucherFinalize(int id)
        {
            if (id != 0)
            {
                var output = await Task.Run(() => _service.JournalFinalized(id));
                _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + "JournalFinalized(), Debit Voucher Finalize.");
            }
            return RedirectToAction("DebitVoucher", "Accounting");
        }
        public async Task<IActionResult> DebitVoucherApprove(int id)
        {
            if (id != 0)
            {
                var output = await Task.Run(() => _service.JournalApproved(id));
                _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + "JournalApproved(), Debit Voucher Approved.");
            }
            return RedirectToAction("DebitVoucher", "Accounting");
        }
        #endregion

        #region   CREDIT Voucher
        public async Task<IActionResult> CreditVoucher()
        {
            ViewBag.AccountCostCenterList = new SelectList(_service.AccountCostCenterDropDownList(), "Value", "Text");
            //1 is journal type
            var model = await Task.Run(() => _service.JournalGet(3));
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> CreditVoucher(VMJournal model)
        {
            // 3 means credit voucher
            model.JournalType = 3;
           model.UserID =UserID;
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                model.Finalized = false;
                model.Approved = false;
                await _service.JournalAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.JournalEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.JournalDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("CreditVoucher");
        }

        public async Task<IActionResult> CreditVoucherDetails(int id)
        {

            var model = await Task.Run(() => _service.JournalDetailsGet(id));
            model.AmountType = 1;
            ViewBag.AccountTypeList = new SelectList(_service.AccountTypeDropDownList(), "Value", "Text");
            ViewBag.ChartOneList = new SelectList(_service.ChartOneDropDownList(), "Value", "Text");
            ViewBag.ChartTwoList = new SelectList(_service.ChartTwoDropDownList(), "Value", "Text");
            ViewBag.AccountHeadList = new SelectList(_service.AccountHeadDropDownList(), "Value", "Text");
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> CreditVoucherDetails(VMJournalSlave model)
        {
           model.UserID =UserID;
            if (model.AmountType == 0)
            {
                return RedirectToAction("CreditVoucherDetails", "Accounting", new RouteValueDictionary(new { id = model.Accounting_JournalFK }));
            }
            else
            {
                if (model.ActionEum == ActionEnum.Add)
                {
                    //Add 
                    if (model.Accounting_HeadFK != 0 && model.AmountType != 0)
                    {
                        model.Finalized = false;
                        await _service.JournalDetailsAdd(model);
                    }
                    return RedirectToAction("CreditVoucherDetails", "Accounting", new RouteValueDictionary(new { id = model.Accounting_JournalFK }));
                }
                else if (model.ActionEum == ActionEnum.Edit)
                {
                    //Edit
                    if (model.Accounting_HeadFK != 0 && model.AmountType != 0)
                    {
                        model.Finalized = false;
                        await _service.JournalDetailsEdit(model);
                    }
                    return RedirectToAction("CreditVoucherDetails", "Accounting", new RouteValueDictionary(new { id = model.Accounting_JournalFK }));

                }
                else if (model.ActionEum == ActionEnum.Delete)
                {
                    //Delete
                    await _service.JournalDetailsDelete(model.ID);
                }
                else
                {
                    return RedirectToAction("Error");
                }
            }
            return RedirectToAction("CreditVoucherDetails", "Accounting", new RouteValueDictionary(new { id = model.Accounting_JournalFK }));
        }

        public async Task<IActionResult> CreditVoucherFinalize(int id)
        {
            if (id != 0)
            {
                var output = await Task.Run(() => _service.JournalFinalized(id));
            }
            return RedirectToAction("CreditVoucher", "Accounting");
        }
        public async Task<IActionResult> CreditVoucherApprove(int id)
        {
            if (id != 0)
            {
                var output = await Task.Run(() => _service.JournalApproved(id));
            }
            return RedirectToAction("CreditVoucher", "Accounting");
        }
        #endregion

        #region   Contra Voucher
        public async Task<IActionResult> ContraVoucher()
        {
            ViewBag.AccountCostCenterList = new SelectList(_service.AccountCostCenterDropDownList(), "Value", "Text");
            //1 is journal type
            var model = await Task.Run(() => _service.JournalGet(4));
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> ContraVoucher(VMJournal model)
        {
            // 4 means contra voucher
            model.JournalType = 4;
           model.UserID =UserID;
            if (model.ActionEum == ActionEnum.Add)
            {
                model.Finalized = false;
                model.Approved = false;
                //Add 
                await _service.JournalAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.JournalEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.JournalDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("ContraVoucher");
        }


        public async Task<IActionResult> ContraVoucherDetails(int id)
        {

            var model = await Task.Run(() => _service.JournalDetailsGet(id));
            model.AmountType = 1;
            ViewBag.AccountTypeList = new SelectList(_service.AccountTypeDropDownList(), "Value", "Text");
            ViewBag.ChartOneList = new SelectList(_service.ChartOneDropDownList(), "Value", "Text");
            ViewBag.ChartTwoList = new SelectList(_service.ChartTwoDropDownList(), "Value", "Text");
            ViewBag.AccountHeadList = new SelectList(_service.AccountHeadDropDownList(), "Value", "Text");
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> ContraVoucherDetails(VMJournalSlave model)
        {
           model.UserID =UserID;
            if (model.AmountType == 0)
            {
                return RedirectToAction("ContraVoucherDetails", "Accounting", new RouteValueDictionary(new { id = model.Accounting_JournalFK }));
            }
            else
            {

                if (model.ActionEum == ActionEnum.Add)
                {
                    //Add 
                    if (model.Accounting_HeadFK != 0 && model.AmountType != 0)
                    {
                        model.Finalized = false;
                        await _service.JournalDetailsAdd(model);
                    }
                    return RedirectToAction("ContraVoucherDetails", "Accounting", new RouteValueDictionary(new { id = model.Accounting_JournalFK }));
                }
                else if (model.ActionEum == ActionEnum.Edit)
                {
                    //Edit
                    if (model.Accounting_HeadFK != 0 && model.AmountType != 0)
                    {
                        model.Finalized = false;
                        await _service.JournalDetailsEdit(model);
                    }
                    return RedirectToAction("ContraVoucherDetails", "Accounting", new RouteValueDictionary(new { id = model.Accounting_JournalFK }));

                }
                else if (model.ActionEum == ActionEnum.Delete)
                {
                    //Delete
                    await _service.JournalDetailsDelete(model.ID);
                }
                else
                {
                    return RedirectToAction("Error");
                }
            }
            return RedirectToAction("ContraVoucherDetails", "Accounting", new RouteValueDictionary(new { id = model.Accounting_JournalFK }));
        }

        public async Task<IActionResult> ContraVoucherFinalize(int id)
        {
            if (id != 0)
            {
                var output = await Task.Run(() => _service.JournalFinalized(id));
            }
            return RedirectToAction("ContraVoucher", "Accounting");
        }
        public async Task<IActionResult> ContraVoucherApprove(int id)
        {
            if (id != 0)
            {
                var output = await Task.Run(() => _service.JournalApproved(id));
            }
            return RedirectToAction("ContraVoucher", "Accounting");
        }
        #endregion        

        #region   Purchase Voucher
        public async Task<IActionResult> PurchaseVoucher()
        {
            ViewBag.AccountCostCenterList = new SelectList(_service.AccountCostCenterDropDownList(), "Value", "Text");
            //1 is journal type
            var model = await Task.Run(() => _service.JournalGet(5));
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> PurchaseVoucher(VMJournal model)
        {
            // 5 means Purchase Voucher 
            model.JournalType = 5;
           model.UserID =UserID;
            if (model.ActionEum == ActionEnum.Add)
            {
                model.Finalized = false;
                model.Approved = false;
                //Add 
                await _service.JournalAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.JournalEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.JournalDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("PurchaseVoucher");
        }


        public async Task<IActionResult> PurchaseVoucherDetails(int id)
        {

            var model = await Task.Run(() => _service.JournalDetailsGet(id));
            model.AmountType = 1;
            ViewBag.AccountTypeList = new SelectList(_service.AccountTypeDropDownList(), "Value", "Text");
            ViewBag.ChartOneList = new SelectList(_service.ChartOneDropDownList(), "Value", "Text");
            ViewBag.ChartTwoList = new SelectList(_service.ChartTwoDropDownList(), "Value", "Text");
            ViewBag.AccountHeadList = new SelectList(_service.AccountHeadDropDownList(), "Value", "Text");
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> PurchaseVoucherDetails(VMJournalSlave model)
        {
           model.UserID =UserID;
            if (model.AmountType == 0)
            {
                return RedirectToAction("PurchaseVoucherDetails", "Accounting", new RouteValueDictionary(new { id = model.Accounting_JournalFK }));
            }
            else
            {
                if (model.ActionEum == ActionEnum.Add)
                {
                    //Add 
                    if (model.Accounting_HeadFK != 0 && model.AmountType != 0)
                    {
                        model.Finalized = false;
                        await _service.JournalDetailsAdd(model);
                    }
                    return RedirectToAction("PurchaseVoucherDetails", "Accounting", new RouteValueDictionary(new { id = model.Accounting_JournalFK }));
                }
                else if (model.ActionEum == ActionEnum.Edit)
                {
                    //Edit
                    if (model.Accounting_HeadFK != 0 && model.AmountType != 0)
                    {
                        model.Finalized = false;
                        await _service.JournalDetailsEdit(model);
                    }
                    return RedirectToAction("PurchaseVoucherDetails", "Accounting", new RouteValueDictionary(new { id = model.Accounting_JournalFK }));

                }
                else if (model.ActionEum == ActionEnum.Delete)
                {
                    //Delete
                    await _service.JournalDetailsDelete(model.ID);
                }
                else
                {
                    return RedirectToAction("Error");
                }
            }
            return RedirectToAction("PurchaseVoucherDetails", "Accounting", new RouteValueDictionary(new { id = model.Accounting_JournalFK }));
        }

        public async Task<IActionResult> PurchaseVoucherFinalize(int id)
        {
            if (id != 0)
            {
                var output = await Task.Run(() => _service.JournalFinalized(id));
            }
            return RedirectToAction("PurchaseVoucher", "Accounting");
        }
        public async Task<IActionResult> PurchaseVoucherApprove(int id)
        {
            if (id != 0)
            {
                var output = await Task.Run(() => _service.JournalApproved(id));
            }
            return RedirectToAction("PurchaseVoucher", "Accounting");
        }
        #endregion

        #region   Sells Voucher
        public async Task<IActionResult> SellsVoucher()
        {
            ViewBag.AccountCostCenterList = new SelectList(_service.AccountCostCenterDropDownList(), "Value", "Text");
            //6 is Sells type
            var model = await Task.Run(() => _service.JournalGet(6));
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> SellsVoucher(VMJournal model)
        {
            // 6 means Sells Voucher 
            model.JournalType = 6;
           model.UserID =UserID;
            if (model.ActionEum == ActionEnum.Add)
            {
                model.Finalized = false;
                model.Approved = false;
                //Add 
                await _service.JournalAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.JournalEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.JournalDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("SellsVoucher");
        }


        public async Task<IActionResult> SellsVoucherDetails(int id)
        {

            var model = await Task.Run(() => _service.JournalDetailsGet(id));
            model.AmountType = 1;
            ViewBag.AccountTypeList = new SelectList(_service.AccountTypeDropDownList(), "Value", "Text");
            ViewBag.ChartOneList = new SelectList(_service.ChartOneDropDownList(), "Value", "Text");
            ViewBag.ChartTwoList = new SelectList(_service.ChartTwoDropDownList(), "Value", "Text");
            ViewBag.AccountHeadList = new SelectList(_service.AccountHeadDropDownList(), "Value", "Text");
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> SellsVoucherDetails(VMJournalSlave model)
        {
           model.UserID =UserID;
            if (model.AmountType == 0)
            {
                return RedirectToAction("SellsVoucherDetails", "Accounting", new RouteValueDictionary(new { id = model.Accounting_JournalFK }));
            }
            else
            {
                if (model.ActionEum == ActionEnum.Add)
                {
                    //Add 
                    if (model.Accounting_HeadFK != 0 && model.AmountType != 0)
                    {
                        model.Finalized = false;
                        await _service.JournalDetailsAdd(model);
                    }
                    return RedirectToAction("SellsVoucherDetails", "Accounting", new RouteValueDictionary(new { id = model.Accounting_JournalFK }));
                }
                else if (model.ActionEum == ActionEnum.Edit)
                {
                    //Edit
                    if (model.Accounting_HeadFK != 0 && model.AmountType != 0)
                    {
                        model.Finalized = false;
                        await _service.JournalDetailsEdit(model);
                    }
                    return RedirectToAction("SellsVoucherDetails", "Accounting", new RouteValueDictionary(new { id = model.Accounting_JournalFK }));

                }
                else if (model.ActionEum == ActionEnum.Delete)
                {
                    //Delete
                    await _service.JournalDetailsDelete(model.ID);
                }
                else
                {
                    return RedirectToAction("Error");
                }
            }
            return RedirectToAction("SellsVoucherDetails", "Accounting", new RouteValueDictionary(new { id = model.Accounting_JournalFK }));
        }

        public async Task<IActionResult> SellsVoucherFinalize(int id)
        {
            if (id != 0)
            {
                var output = await Task.Run(() => _service.JournalFinalized(id));
            }
            return RedirectToAction("SellsVoucher", "Accounting");
        }
        public async Task<IActionResult> SellsVoucherApprove(int id)
        {
            if (id != 0)
            {
                var output = await Task.Run(() => _service.JournalApproved(id));
            }
            return RedirectToAction("SellsVoucher", "Accounting");
        }
        #endregion

        #region Bill of Exchange

        public IActionResult BillofExchanges()
        {
            VMShipmentBillOfExchange vmShipmentBillOfExchange = new VMShipmentBillOfExchange()
            {
                DataList = _integrationService.ShipmentBillOfExchangesGet()
            };

            return View(vmShipmentBillOfExchange);
        }
        #endregion

        #region   Realisation Voucher
        public async Task<IActionResult> RealisationVoucher(int id=0)
        {
            ViewBag.AccountCostCenterList = new SelectList(_service.AccountCostCenterDropDownList(), "Value", "Text");
            var model = new VMJournal();
            model.vMShipmentBillOfExchanges = await Task.Run(() => _service.ShipmentBillOfExchangesGetJoinVoucher());
            ViewBag.AccountHeadList = new SelectList(_service.AccountHeadDropDownList(), "Value", "Text");
            ViewBag.CurrencyList = new SelectList(_service.CurrencyDropDownList(), "Value", "Text");
            model.Accounting_CostCenterFK = 2;
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> RealisationVoucher(VMJournal model)
        {
            // 9 means Realisation Voucher 
            model.JournalType = 9;
           model.UserID =UserID;
            if (model.ActionEum == ActionEnum.Add)
            {
                model.Finalized = false;
                model.Approved = false;
                model.Description = "Integrated Journal: BOE-Export Realisation";
                //Add 
                await _service.JournalAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.JournalEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.JournalDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("RealisationVoucher");
        }

        public async Task<IActionResult> RealizationForBOMDetails(int id)
        {
            var model = new VMJournal();
            model.vMShipmentBillOfExchangeSlave = await Task.Run(() => _integrationService.ShipmentBillOfExchangeGet(id));
            return View(model);
        }

        public async Task<IActionResult> RealisationVoucherDetails(int id)
        {
            var model = await Task.Run(() => _service.JournalDetailsGet(id));
            model.AmountType = 1;
            ViewBag.AccountTypeList = new SelectList(_service.AccountTypeDropDownList(), "Value", "Text");
            ViewBag.ChartOneList = new SelectList(_service.ChartOneDropDownList(), "Value", "Text");
            ViewBag.ChartTwoList = new SelectList(_service.ChartTwoDropDownList(), "Value", "Text");
            ViewBag.AccountHeadList = new SelectList(_service.AccountHeadDropDownList(), "Value", "Text");
            return View(model);
        }

        [HttpPost]
        public async Task<JsonResult> JsonRealisationVoucherDetails(int? id)
        {
            VMJournalSlave model = await Task.Run(() => _service.RealisationJournalDetailsGet(id));
            return Json(new { id = model.vMJournal.ID, Success = false});
        }

        [HttpPost]
        public async Task<IActionResult> RealisationVoucherDetails(VMJournalSlave model)
        {
           model.UserID =UserID;
            if (model.AmountType == 0)
            {
                return RedirectToAction("RealisationVoucherDetails", "Accounting", new RouteValueDictionary(new { id = model.Accounting_JournalFK }));
            }
            else
            {
                if (model.ActionEum == ActionEnum.Add)
                {
                    //Add 
                    if (model.Accounting_HeadFK != 0 && model.AmountType != 0)
                    {
                        model.Finalized = false;
                        await _service.JournalDetailsAdd(model);
                    }
                    return RedirectToAction("RealisationVoucherDetails", "Accounting", new RouteValueDictionary(new { id = model.Accounting_JournalFK }));
                }
                else if (model.ActionEum == ActionEnum.Edit)
                {
                    //Edit
                    if (model.Accounting_HeadFK != 0 && model.AmountType != 0)
                    {
                        model.Finalized = false;
                        await _service.JournalDetailsEdit(model);
                    }
                    return RedirectToAction("RealisationVoucherDetails", "Accounting", new RouteValueDictionary(new { id = model.Accounting_JournalFK }));

                }
                else if (model.ActionEum == ActionEnum.Delete)
                {
                    //Delete
                    await _service.JournalDetailsDelete(model.ID);
                }
                else
                {
                    return RedirectToAction("Error");
                }
            }
            return RedirectToAction("RealisationVoucherDetails", "Accounting", new RouteValueDictionary(new { id = model.Accounting_JournalFK }));
        }

        
        public async Task<IActionResult> RealisationVoucherFinalize(int id)
        {
            if (id != 0)
            {
                var output = await Task.Run(() => _service.JournalFinalized(id));
            }
            return RedirectToAction("RealisationVoucher", "Accounting");
        }
        public async Task<IActionResult> RealisationVoucherApprove(int id)
        {
            if (id != 0)
            {
                var output = await Task.Run(() => _service.JournalApproved(id));
            }
            return RedirectToAction("RealisationVoucher", "Accounting");
        }
        #endregion
        
        #region   Integrated Journal
        public async Task<IActionResult> IntegratedJournal()
        {
            //2 is Debit Voucher Type
            var model = await Task.Run(() => _service.JournalGet(7));
            return View(model);
        }
        public async Task<IActionResult> IntegratedJournalDetails(int id)
        {

            var model = await Task.Run(() => _service.JournalDetailsGet(id));
            return View(model);
        }
        #endregion

        #region Quick Journal Create
        public async Task<IActionResult> QuickJournal()
        {
            ViewBag.AccountCostCenterList = new SelectList(_service.AccountCostCenterDropDownList(), "Value", "Text");
            //1 is journal type
            var model = await Task.Run(() => _service.JournalGet(8));
            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + "JournalGet(), Journal Voucher Get.");
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> QuickJournal(VMJournal model)
        {
            // 1 means Journal voucher
            model.JournalType = 8;
           model.UserID =UserID;
            if (model.ActionEum == ActionEnum.Add)
            {
                model.Finalized = false;
                model.Approved = false;
                //Add 
                await _service.JournalAdd(model);
                _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + "JournalAdd(), Journal Voucher Add.");
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.JournalEdit(model);
                _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + "JournalEdit(), Journal Voucher Edit.");
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.JournalDelete(model.ID);
                _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + "JournalDelete(), Journal Voucher Delete.");
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("QuickJournal");
        }
        public async Task<IActionResult> QuickJournalDetails(int id)
        {

            var model = await Task.Run(() => _service.JournalDetailsGet(id));
            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + "JournalDetailsGet(), Single Journal Voucher Details Get by ID.");
            model.AmountType = 1;
            ViewBag.AccountTypeList = new SelectList(_service.AccountTypeDropDownList(), "Value", "Text");
            ViewBag.ChartOneList = new SelectList(_service.ChartOneDropDownList(), "Value", "Text");
            ViewBag.ChartTwoList = new SelectList(_service.ChartTwoDropDownList(), "Value", "Text");
            ViewBag.AccountHeadList = new SelectList(_service.AccountHeadDropDownList(), "Value", "Text");
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> QuickJournalDetails(VMJournalSlave model)
        {

           model.UserID =UserID;
            if (model.AmountType == 0)
            {
                return RedirectToAction("QuickJournalDetails", "Accounting", new RouteValueDictionary(new { id = model.Accounting_JournalFK }));
            }
            else
            {
                if (model.ActionEum == ActionEnum.Add)
                {
                    //Add 
                    if (model.Accounting_HeadFK != 0 && model.Accounting_HeadFK1 != 0 && model.Amount == model.Amount1)
                    {
                        // credit add
                        model.AmountType = 1;
                        model.Finalized = false;
                        await _service.JournalDetailsAdd(model);

                        // debit add 
                        model.AmountType = 2;
                        model.Accounting_HeadFK = model.Accounting_HeadFK1;
                        model.Amount = model.Amount1;
                        model.Accounting_HeadFK = model.Accounting_HeadFK1;
                        await _service.JournalDetailsAdd(model);

                        _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + "JournalDetailsAdd(), Journal Details Add.");
                    }
                    return RedirectToAction("QuickJournalDetails", "Accounting", new RouteValueDictionary(new { id = model.Accounting_JournalFK }));
                }
                else if (model.ActionEum == ActionEnum.Edit)
                {
                    //Edit
                    if (model.Accounting_HeadFK != 0 && model.Accounting_HeadFK1 != 0 && model.Amount == model.Amount1)
                    {
                        //Edit Credit
                        model.AmountType = 1;
                        model.Finalized = false;
                        await _service.JournalDetailsEdit(model);

                        //Edit Debit
                        model.AmountType = 2;
                        model.ID = model.ID1;
                        model.Accounting_HeadFK = model.Accounting_HeadFK1;
                        model.Title = model.Title;
                        model.Description = model.Description;
                        model.Remark = model.Remark;
                        model.Amount = model.Amount1;
                        await _service.JournalDetailsEdit(model);
                        _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + "JournalDetailsEdit(), Journal Details Edit.");
                    }
                    return RedirectToAction("QuickJournalDetails", "Accounting", new RouteValueDictionary(new { id = model.Accounting_JournalFK }));

                }
                else if (model.ActionEum == ActionEnum.Delete)
                {
                    //Delete
                    await _service.JournalDetailsDelete(model.ID);

                    //Second Row Delete
                    model.ID = model.ID1;
                    await _service.JournalDetailsDelete(model.ID);
                    _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + "JournalDetailsDelete(), Journal Details Delete.");
                }
                else
                {
                    return RedirectToAction("Error");
                }
            }
            return RedirectToAction("QuickJournalDetails", "Accounting", new RouteValueDictionary(new { id = model.Accounting_JournalFK }));
        }

        public async Task<IActionResult> QuickJournalFinalize(int id)
        {
            if (id != 0)
            {
                var output = await Task.Run(() => _service.JournalFinalized(id));
                _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + "JournalFinalized(), Single Journal Finalize in Journal Voucher.");
            }
            return RedirectToAction("QuickJournal", "Accounting");
        }

        public async Task<IActionResult> QuickJournalApprove(int id)
        {
            if (id != 0)
            {
                var output = await Task.Run(() => _service.JournalApproved(id));
                _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + "JournalApproved(), Single Journal Approved in Journal Voucher.");
            }
            return RedirectToAction("QuickJournal", "Accounting");
        }

        #endregion

        #region Chart One Create
        public async Task<IActionResult> ChartOne()
        {
            var model = await Task.Run(() => _service.AccountChart1Get());
            ViewBag.AccountTypeList = new SelectList(_service.AccountTypeDropDownList(), "Value", "Text");
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> ChartOne(VMChart1 model)
        {
           model.UserID =UserID;
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.AccountChart1Add(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.AccountChart1Edit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.AccountChart1Delete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("ChartOne");
        }

        #endregion

        #region Account Chart Two 
        public async Task<IActionResult> ChartTwo()
        {
            var model = await Task.Run(() => _service.AccountChart2Get());
            ViewBag.AccountTypeList = new SelectList(_service.AccountTypeDropDownList(), "Value", "Text");
            ViewBag.ChartOneList = new SelectList(_service.ChartOneDropDownList(), "Value", "Text");
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> ChartTwo(VMChart2 model)
        {
           model.UserID =UserID;
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.AccountChart2Add(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.AccountChart2Edit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.AccountChart2Delete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("ChartTwo");
        }

        #endregion

        #region Account Head
        public async Task<IActionResult> AccountHead()
        {
            var model = await Task.Run(() => _service.AccountHeadGet());
            //await Task.Run(() => _service.AccountHeadGet()).Result;
            ViewBag.AccountTypeList = new SelectList(_service.AccountTypeDropDownList(), "Value", "Text");
            ViewBag.ChartOneList = new SelectList(_service.ChartOneDropDownList(), "Value", "Text");
            ViewBag.ChartTwoList = new SelectList(_service.ChartTwoDropDownList(), "Value", "Text");
            return View(model);
        }

        public async Task<IActionResult> AccountHeadWithDetails()
        {
            var model = await Task.Run(() => _service.AccountHeadGetWithDetails());
            //await Task.Run(() => _service.AccountHeadGet()).Result;
            ViewBag.AccountTypeList = new SelectList(_service.AccountTypeDropDownList(), "Value", "Text");
            ViewBag.ChartOneList = new SelectList(_service.ChartOneDropDownList(), "Value", "Text");
            ViewBag.ChartTwoList = new SelectList(_service.ChartTwoDropDownList(), "Value", "Text");
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> AccountHead(VMAccountHead model)
        {
           model.UserID =UserID;
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.AccountHeadAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.AccountHeadEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.AccountHeadDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("AccountHead");
        }
        #endregion

        #region Accounting Cost Center 
        public async Task<IActionResult> AccountingCostCenter()
        {
            var model = await Task.Run(() => _service.AccountingCostCenterGet());
            //await Task.Run(() => _service.AccountHeadGet()).Result;
            ViewBag.AccountTypeList = new SelectList(_service.AccountTypeDropDownList(), "Value", "Text");
            ViewBag.ChartOneList = new SelectList(_service.ChartOneDropDownList(), "Value", "Text");
            ViewBag.ChartTwoList = new SelectList(_service.ChartTwoDropDownList(), "Value", "Text");
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> AccountingCostCenter(VmAccountingCostCenter model)
        {
           model.UserID =UserID;
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                await _service.AccountingCostCenterAdd(model);
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _service.AccountingCostCenterEdit(model);
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _service.AccountingCostCenterDelete(model.ID);
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("AccountingCostCenter");
        }

        #endregion

        #region Json Result Region

        public JsonResult ChartOneDropDownSpecific(int id)
        {
            if (id != 0)
            {
                var list = _service.ChartOneDropDownSpecific(id);
                return Json(list);
            }
            return null;
        }

        public JsonResult ChartTwoDropDownSpecific(int id)
        {
            if (id != 0)
            {
                var list = _service.ChartTwoDropDownSpecific(id);
                return Json(list);
            }
            return null;
        }

        public JsonResult AccountHeadDropDownSpecific(int id)
        {
            if (id != 0)
            {
                var list = _service.AccountHeadDropDownSpecific(id);
                return Json(list);
            }
            return null;
        }
        #endregion

        #region Ledger Report
        public IActionResult LedgerReport()
        {
            ViewBag.AccountTypeList = new SelectList(_service.AccountTypeDropDownList(), "Value", "Text");
            ViewBag.ChartOneList = new SelectList(_service.ChartOneDropDownList(), "Value", "Text");
            ViewBag.ChartTwoList = new SelectList(_service.ChartTwoDropDownList(), "Value", "Text");
            ViewBag.AccountHeadList = new SelectList(_service.AccountHeadDropDownList(), "Value", "Text");
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> LedgerReport(VMLedger model)
        {
            ViewBag.AccountTypeList = new SelectList(_service.AccountTypeDropDownList(), "Value", "Text");
            ViewBag.ChartOneList = new SelectList(_service.ChartOneDropDownList(), "Value", "Text");
            ViewBag.ChartTwoList = new SelectList(_service.ChartTwoDropDownList(), "Value", "Text");
            ViewBag.AccountHeadList = new SelectList(_service.AccountHeadDropDownList(), "Value", "Text");
            model = await Task.Run(() => _service.LeadgerGetByDate(model));
            ViewBag.LedgerData = model.DataList;
            return View(model);
        }

        public async Task<IActionResult> LedgerReportView(int cid, string fromdate, string todate)
        {
            VMLedger model = new VMLedger();
            model.Accounting_HeadId = cid;
            model.FromDate = DateTime.ParseExact(fromdate, "MM/dd/yyyy", null);
            model.ToDate = DateTime.ParseExact(todate, "MM/dd/yyyy", null);
            model = await Task.Run(() => _service.LeadgerGetByDate(model));
            return View(model);
        }
        #endregion

        public IActionResult TrialBalanceView()
        {
            ViewBag.AccountTypeList = new SelectList(_service.TrialBalanceSearchCharttype(), "Value", "Text");
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> TrialBalanceView(VMTrialBalance model)
        {
            ViewBag.AccountTypeList = new SelectList(_service.TrialBalanceSearchCharttype(), "Value", "Text");
            model = await Task.Run(() => _service.TrialBalanceGet(model));
            // model = await Task.Run(() => _service.TrialBalanceGetByDate(model));
            ViewBag.TrialBalanceData = model.DataList;
            return View(model);
        }

        //[HttpPost]
        //public async Task<IActionResult> TrialBalance(VMTrialBalance model)
        //{
        //    ViewBag.AccountTypeList = new SelectList(_service.TrialBalanceSearchCharttype(), "Value", "Text");
        //    model = await Task.Run(() => _service.TrialBalanceGetByDate(model));
        //    ViewBag.TrialBalanceData = model.DataList;
        //    return View(model);
        //}

        public async Task<IActionResult> TrialBalance(string fromdate, string todate, int searchTypeId)
        {
            VMTrialBalance model = new VMTrialBalance();
            model.FromDate = DateTime.ParseExact(fromdate, "MM/dd/yyyy", null);
            model.ToDate = DateTime.ParseExact(todate, "MM/dd/yyyy", null);
            model.SearchTypeId = searchTypeId;
            model = await Task.Run(() => _service.TrialBalanceGet(model));
            // model = await Task.Run(() => _service.TrialBalanceGetByDate(model));
            return View(model);
        }

        #region CostofGoodsSold
        public IActionResult CostofGoodsSoldSearch()
        {
            VMCogs model = new VMCogs();
            var date = DateTime.Now;
            ViewBag.FromDate = date.ToString("yyyy/MM/dd");
            ViewBag.ToDate = date.ToString("yyyy/MM/dd");

            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> CostofGoodsSoldSearch(VMCogs model)
        {
            var fromDate = model.FromDate;
            var toDate = model.ToDate;
            model = await Task.Run(() => _service.CogsGetByDate(model.FromDate, model.ToDate));
            // ViewBag.CostofGoodsSoldData = model.DataList;
            model.FromDate = fromDate;
            model.ToDate = toDate;
            return View(model);
        }
        public async Task<IActionResult> CostofGoodsSold(string fromdate, string todate)
        {
            VMCogs model = new VMCogs();
            model.FromDate = DateTime.ParseExact(fromdate, "MM/dd/yyyy", null);
            model.ToDate = DateTime.ParseExact(todate, "MM/dd/yyyy", null);
            model = await Task.Run(() => _service.CogsGetByDate(model.FromDate, model.ToDate));
            return View(model);
        }

        // [HttpPost]
        public async Task<IActionResult> CostofGoodsSoldNew(string fromdate, string todate)
        {

            VMCogs model = new VMCogs();
            model.FromDate = DateTime.ParseExact(fromdate, "MM/dd/yyyy", null);
            model.ToDate = DateTime.ParseExact(todate, "MM/dd/yyyy", null);
            model = await Task.Run(() => _service.CogsGetByDate(model.FromDate, model.ToDate));
            ViewBag.CostofGoodsSoldData = model.DataList;
            model.FromDate = DateTime.ParseExact(fromdate, "MM/dd/yyyy", null);
            model.ToDate = DateTime.ParseExact(todate, "MM/dd/yyyy", null);
            return View(model);
        }
        #endregion

        #region Balance Sheet
        public IActionResult FinancialPositionSearch()
        {
            VMFinancialPosition model = new VMFinancialPosition();
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> FinancialPositionSearch(VMFinancialPosition model)
        {
            var frmdt = "2000-01-01";
            var fromDate = Convert.ToDateTime(frmdt);
            var toDate = model.ToDate;
            model = await Task.Run(() => _service.FinancialPositionGetByDate(fromDate, model.ToDate));
            ViewBag.FinancialPosition = model.DataList;
            model.FromDate = fromDate;
            model.ToDate = toDate;
            return View(model);
        }

        //[HttpPost]
        public async Task<IActionResult> FinancialPosition(string fromdate, string todate)
        {
            VMFinancialPosition model = new VMFinancialPosition();
            model.FromDate = DateTime.ParseExact(fromdate, "MM/dd/yyyy", null);
            model.ToDate = DateTime.ParseExact(todate, "MM/dd/yyyy", null);
            model = await Task.Run(() => _service.FinancialPositionGetByDate(model.FromDate, model.ToDate));
            ViewBag.FinancialPosition = model.DataList;
            model.FromDate = DateTime.ParseExact(fromdate, "MM/dd/yyyy", null);
            model.ToDate = DateTime.ParseExact(todate, "MM/dd/yyyy", null);
            return View(model);
        }
        #endregion

        #region Income Statement
        public IActionResult ComprehensiveIncomeSearch()
        {
            VMIncomeStatement model = new VMIncomeStatement();
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> ComprehensiveIncomeSearch(VMIncomeStatement model)
        {
            var fromDate = model.FromDate;
            var toDate = model.ToDate;
            model = await Task.Run(() => _service.ComprehensiveIncomeGetByDate(model.FromDate, model.ToDate));
            ViewBag.CostofGoodsSoldData = model.DataList;
            model.FromDate = fromDate;
            model.ToDate = toDate;
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> ComprehensiveIncomeSearchNew(VMIncomeStatement model)
        {
            var fromDate = model.FromDate;
            var toDate = model.ToDate;
            model = await Task.Run(() => _service.ComprehensiveIncomeGetByDate(model.FromDate, model.ToDate));
            ViewBag.CostofGoodsSoldData = model.DataList;
            model.FromDate = fromDate;
            model.ToDate = toDate;
            return View(model);
        }
        //[HttpPost]
        public async Task<IActionResult> ComprehensiveIncome(string fromdate, string todate)
        {
            VMIncomeStatement model = new VMIncomeStatement();
            model.FromDate = DateTime.ParseExact(fromdate, "MM/dd/yyyy", null);
            model.ToDate = DateTime.ParseExact(todate, "MM/dd/yyyy", null);
            model = await Task.Run(() => _service.ComprehensiveIncomeGetByDate(model.FromDate, model.ToDate));
            model.FromDate = DateTime.ParseExact(fromdate, "MM/dd/yyyy", null);
            model.ToDate = DateTime.ParseExact(todate, "MM/dd/yyyy", null);
            return View(model);
        }
        #endregion

        #region Trial Balance Index
        public IActionResult TrialBalanceSearch()
        {
            return View();
        }
        #endregion

        #region Equity
        public IActionResult EquitySearch()
        {
            return View();
        }
        public IActionResult Equity()
        {
            return View();
        }
        #endregion
        
        #region Cash Flow

        public IActionResult CashFlowSearch()
        {
            VMCashFlow model = new VMCashFlow();
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> CashFlowSearch(VMCashFlow model)
        {
            var fromDate = model.FromDate;
            var toDate = model.ToDate;
            model = await Task.Run(() => _service.CashFlowGetByDate(model.FromDate, model.ToDate));
            ViewBag.FinancialPosition = model.DataList;
            model.FromDate = fromDate;
            model.ToDate = toDate;
            return View(model);
        }
        //[HttpPost]
        public async Task<IActionResult> CashFlow(string fromdate, string todate)
        {
            VMCashFlow model = new VMCashFlow();
            model.FromDate = DateTime.ParseExact(fromdate, "MM/dd/yyyy", null);
            model.ToDate = DateTime.ParseExact(todate, "MM/dd/yyyy", null);
            model = await Task.Run(() => _service.CashFlowGetByDate(model.FromDate, model.ToDate));
            ViewBag.FinancialPosition = model.DataList;
            model.FromDate = DateTime.ParseExact(fromdate, "MM/dd/yyyy", null);
            model.ToDate = DateTime.ParseExact(todate, "MM/dd/yyyy", null);
            return View(model);
        }
        #endregion

        #region Financial Analysis
        public IActionResult FinancialAnalysisSearch()
        {
            return View();
        }
        public IActionResult FinancialAnalysis()
        {
            return View();
        }
        #endregion

        #region Ait Adjustment
        public IActionResult AitAdjustmentSearch()
        {
            return View();
        }
        public IActionResult AitAdjustment()
        {
            return View();
        }
        #endregion

        #region ProvisionforTaxAdjustment
        public IActionResult ProvisionTaxAdjustmentSearch()
        {
            return View();
        }
        public IActionResult ProvisionforTaxAdjustment()
        {
            return View();
        }
        #endregion

        #region Accounting Check Approval   

        public async Task<IActionResult> CheckApprovalCreate()
        {

            ViewBag.AccountCostCenterList = new SelectList(_service.AccountCostCenterDropDownList(), "Value", "Text");
            ViewBag.FromAccountignList = new SelectList(_accountingPartialService.GetCreditAccountDropDownList(), "Value", "Text");
            ViewBag.ToAccountignList = new SelectList(_accountingPartialService.GetDebitAccountDropDownList(), "Value", "Text");
            var model = await Task.Run(() => _accountingPartialService.AccountingTransactionGet());
            model.CID = _accountingPartialService.VaoucherNoGet();
            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + "CheckApprovalCreate(), Journal Voucher Get.");
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> CheckApprovalCreate(VMAccountingTransaction model)
        {
           model.UserID =UserID;
            if (model.ActionEum == ActionEnum.Add)
            {
                //Add 
                model.TransactionType = "Cash Withdraw";
                await _accountingPartialService.AccountingTransactionAdd(model);
                _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + "CheckApprovalCreate(), Journal Voucher Add.");
            }
            else if (model.ActionEum == ActionEnum.Edit)
            {
                //Edit
                await _accountingPartialService.AccountingTransactionEdit(model);
                _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + "AccountingTransactionEdit(), Journal Voucher Edit.");
            }
            else if (model.ActionEum == ActionEnum.Delete)
            {
                //Delete
                await _accountingPartialService.AccountTransactionDelete(model.ID);
                _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + "AccountTransactionDelete(), Journal Voucher Delete.");
            }
            else
            {
                return RedirectToAction("Error");
            }
            return RedirectToAction("CheckApprovalCreate");
        }

        public async Task<IActionResult> ChequeListWaitingforApproval()
        {

            var model = await Task.Run(() => _accountingPartialService.AccountingTransactionGetforChairmanview(DateTime.Today));
            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + "CheckApprovalCreate(), Journal Voucher Get.");
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> ChequeListWaitingforApproval(VMAccountingTransaction model)
        {
            var data = await Task.Run(() => _accountingPartialService.AccountingTransactionGetforChairmanview(model.Date));
            return View(data);
        }

        public async Task<IActionResult> Notifier()
        {

            var model = await Task.Run(() => _accountingPartialService.AccountingTransactionWithBalance());
            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + "CheckApprovalCreate(), Journal Voucher Get.");
            return View(model);
        }

        public async Task<IActionResult> BankWithDrawallUpdate(int id, int type)
        {
            if (id > 0)
            {
                await _accountingPartialService.AccountingTransactionApproval(id, type);
                return RedirectToAction("Notifier", "Accounting");
            }
            return RedirectToAction("Notifier", "Accounting");
        }

        public async Task<ActionResult> AccountingTransactionPrint(int id)
        {
            var data = await _accountingPartialService.AccountingTransactionGetSpecific(id);
            return View(data);
        }

        public async Task<ActionResult> AccountingVoucherPrint(int id)
        {
            var model = await Task.Run(() => _service.JournalDetailsGet(id));
            return View(model);
        }
        #endregion

        #region   Document Voucher
        public IActionResult BBLCwiseDocument()
        {
            var model = Task.Run(() => _integrationService.GetBBLCWiseSupplierList()).Result;
            return View(model);
        }
        public IActionResult SupplierBBLCwiseDocument(int id)
        {
            var model = Task.Run(() => _integrationService.GetBBLCSupplierWise(id)).Result;
            return View(model);
        }

        public IActionResult SupplierSpecificBBLCWisePaymentInformation(int id)
        {
            var model = Task.Run(() => _integrationService.BBLCPaymentInformationGet(id)).Result;
            model.B2bLCPayment = model.VmBBLC.Remaining;
            ViewBag.AccountHeadList = new SelectList(_service.AccountHeadDropDownList(), "Value", "Text");
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> SupplierSpecificBBLCWisePaymentInformation(VMCommercial_BBLCPaymentInformation model)
        {
            model.UserID = UserID;
            if (model.ActionId == (int)ActionEnum.Add)
            {
                await _integrationService.BBLCPaymentDocument(model);
            }
            return RedirectToAction(nameof(SupplierSpecificBBLCWisePaymentInformation), new { id = model.VmBBLC.ID });
        }

        public async Task<IActionResult> DocumentVoucherDetails(int id)
        {
            var model = await Task.Run(() => _service.JournalDetailsGet(id));
            model.AmountType = 1;
            ViewBag.AccountTypeList = new SelectList(_service.AccountTypeDropDownList(), "Value", "Text");
            ViewBag.ChartOneList = new SelectList(_service.ChartOneDropDownList(), "Value", "Text");
            ViewBag.ChartTwoList = new SelectList(_service.ChartTwoDropDownList(), "Value", "Text");
            ViewBag.AccountHeadList = new SelectList(_service.AccountHeadDropDownList(), "Value", "Text");
            return View(model);
        }

        [HttpPost]
        public async Task<JsonResult> JsonDocumentVoucherDetails(int? id)
        {
            VMJournalSlave model = await Task.Run(() => _service.RealisationJournalDetailsGet(id));
            return Json(new { id = model.vMJournal.ID, Success = false });
        }

        [HttpPost]
        public async Task<IActionResult> DocumentVoucherDetails(VMJournalSlave model)
        {
            model.UserID = UserID;
            if (model.AmountType == 0)
            {
                return RedirectToAction("DocumentVoucherDetails", "Accounting", new RouteValueDictionary(new { id = model.Accounting_JournalFK }));
            }
            else
            {
                if (model.ActionEum == ActionEnum.Add)
                {
                    //Add 
                    if (model.Accounting_HeadFK != 0 && model.AmountType != 0)
                    {
                        model.Finalized = false;
                        await _service.JournalDetailsAdd(model);
                    }
                    return RedirectToAction("DocumentVoucherDetails", "Accounting", new RouteValueDictionary(new { id = model.Accounting_JournalFK }));
                }
                else if (model.ActionEum == ActionEnum.Edit)
                {
                    //Edit
                    if (model.Accounting_HeadFK != 0 && model.AmountType != 0)
                    {
                        model.Finalized = false;
                        await _service.JournalDetailsEdit(model);
                    }
                    return RedirectToAction("DocumentVoucherDetails", "Accounting", new RouteValueDictionary(new { id = model.Accounting_JournalFK }));

                }
                else if (model.ActionEum == ActionEnum.Delete)
                {
                    //Delete
                    await _service.JournalDetailsDelete(model.ID);
                }
                else
                {
                    return RedirectToAction("Error");
                }
            }
            return RedirectToAction("DocumentVoucherDetails", "Accounting", new RouteValueDictionary(new { id = model.Accounting_JournalFK }));
        }


        public async Task<IActionResult> DocumentVoucherFinalize(int id)
        {
            if (id != 0)
            {
                var output = await Task.Run(() => _service.JournalFinalized(id));
            }
            return RedirectToAction("DocumentVoucher", "Accounting");
        }
        public async Task<IActionResult> DocumentVoucherApprove(int id)
        {
            if (id != 0)
            {
                var output = await Task.Run(() => _service.JournalApproved(id));
            }
            return RedirectToAction("DocumentVoucher", "Accounting");
        }
        #endregion

    }
}