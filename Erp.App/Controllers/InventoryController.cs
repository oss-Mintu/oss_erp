﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Erp.Core.Services.Inventory;
using Microsoft.Extensions.Logging;
using Erp.Infrastructure;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Routing;
using Erp.Core.Services;
using Microsoft.AspNetCore.Http;
using Erp.Core.Services.User;
using Erp.Core.Services.Procurement;
using Erp.Core.Services.Integration;

namespace Erp.App.Controllers
{
    [SoftwareAdmin]
    public class InventoryController : Controller
    {
        const string SessionController = "ControllerName";
        const string SessionAction = "ActionName";
        const string SessionName = "LoginName";
        const string SessionId = "LoginId";
        private readonly ILogger _logger;
        private readonly InventoryService _service;
        private readonly IntegrationService _integrationService;
        public InventoryController(InfrastructureDbContext db, ILogger<InventoryController> logger, IHttpContextAccessor httpContextAccessor)
        {
            _integrationService = new IntegrationService(db, httpContextAccessor.HttpContext);
            _service = new InventoryService(db);
            SessionHandler sessionHandler = new SessionHandler(db, httpContextAccessor.HttpContext);
            sessionHandler.Adjust();
            _logger = logger;
        }

        #region Inventory Report
        public async Task<IActionResult> InventoryIndex()
        {
            ViewBag.RawCategoryList = new SelectList(_integrationService.RawCategoryDropDownList(), "Value", "Text");
            ViewBag.RawSubCategoryList = new SelectList(_integrationService.RawSubCategoryDropDownList(), "Value", "Text");
            ViewBag.RawItemList = new SelectList(_integrationService.RawItemDropDownList(), "Value", "Text");
            var fromdt = DateTime.Now.AddDays(-7);
            var todt = DateTime.Now;
            int id = 0;
            int storeTypeFk = 1;
            var model = await Task.Run(() => _service.RawInventorySummeryListGet(fromdt,todt, id, storeTypeFk));
            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + "InventoryListGet(),  Get Last Sevendays Inventory.");
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> InventoryIndex(VmInventory model)
        {
            ViewBag.RawCategoryList = new SelectList(_integrationService.RawCategoryDropDownList(), "Value", "Text");
            ViewBag.RawSubCategoryList = new SelectList(_integrationService.RawSubCategoryDropDownList(), "Value", "Text");
            ViewBag.RawItemList = new SelectList(_integrationService.RawItemDropDownList(), "Value", "Text");
            int storeTypeFk = 1;
            var data = await Task.Run(() => _service.RawInventorySummeryListGet(model.FromDate, model.ToDate, model.Common_RawItemFK, storeTypeFk));
            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + "InventoryListGet(),  Get Inventory Summery List as per date range and id wise.");
            return View(data);
        }

        public async Task<IActionResult> ItemWiseInventoryDetatils(int id=0,int storeTypeFk=0)
        {
             var model = await Task.Run(() => _service.RawInventoryItemWiseListGet(id, storeTypeFk));
            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + "InventoryListGet(),  Get Last Sevendays Inventory.");
            return View(model);
        }

        public JsonResult RawSubCategoryDropDownSpecific(int id)
        {
            if (id != 0)
            {
                var list = _integrationService.RawSubCategoryDropDowncascading(id);
                return Json(list);
            }
            return null;
        }

        public JsonResult RawItemDropDownSpecific(int id)
        {
            if (id != 0)
            {
                var list = _integrationService.RawItemDropDownListSpecific(id);
                return Json(list);
            }
            return null;
        }


        public async Task<IActionResult> WipInventoryIndex()
        {
            ViewBag.RawCategoryList = new SelectList(_integrationService.RawCategoryDropDownList(), "Value", "Text");
            ViewBag.RawSubCategoryList = new SelectList(_integrationService.RawSubCategoryDropDownList(), "Value", "Text");
            ViewBag.RawItemList = new SelectList(_integrationService.RawItemDropDownList(), "Value", "Text");
            var fromdt = DateTime.Now.AddDays(-7);
            var todt = DateTime.Now;
            int id = 0;
            int storeTypeFk = 2;
            var model = await Task.Run(() => _service.RawInventorySummeryListGet(fromdt, todt, id, storeTypeFk));
            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + "InventoryListGet(),  Get Last Sevendays Inventory.");
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> WipInventoryIndex(VmInventory model)
        {
            ViewBag.RawCategoryList = new SelectList(_integrationService.RawCategoryDropDownList(), "Value", "Text");
            ViewBag.RawSubCategoryList = new SelectList(_integrationService.RawSubCategoryDropDownList(), "Value", "Text");
            ViewBag.RawItemList = new SelectList(_integrationService.RawItemDropDownList(), "Value", "Text");
            int storeTypeFk = 2;
            var data = await Task.Run(() => _service.RawInventorySummeryListGet(model.FromDate, model.ToDate, model.Common_RawItemFK, storeTypeFk));
            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + "InventoryListGet(),  Get Inventory Summery List as per date range and id wise.");
            return View(data);
        }


        public async Task<IActionResult> FinishInventoryIndex()
        {
            ViewBag.FinishCategoryList = new SelectList(_integrationService.FinishCategoryDropDownList(), "Value", "Text");
            ViewBag.FinishSubCategoryList = new SelectList(_integrationService.FinishSubCategoryDropDownList(), "Value", "Text");
            ViewBag.FinishItemList = new SelectList(_integrationService.FinishItemDropDownList(), "Value", "Text");
            var fromdt = DateTime.Now.AddDays(-7);
            var todt = DateTime.Now;
            int id = 0;
            int storeTypeFk = 3;
            var model = await Task.Run(() => _service.FinishInventorySummeryListGet(fromdt, todt, id, storeTypeFk));
            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + "InventoryListGet(),  Get Last Sevendays Inventory.");
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> FinishInventoryIndex(VmInventory model)
        {
            ViewBag.FinishCategoryList = new SelectList(_integrationService.FinishCategoryDropDownList(), "Value", "Text");
            ViewBag.FinishSubCategoryList = new SelectList(_integrationService.FinishSubCategoryDropDownList(), "Value", "Text");
            ViewBag.FinishItemList = new SelectList(_integrationService.FinishItemDropDownList(), "Value", "Text");
            int storeTypeFk = 3;
            var data = await Task.Run(() => _service.FinishInventorySummeryListGet(model.FromDate, model.ToDate, model.Common_RawItemFK, storeTypeFk));
            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + "InventoryListGet(),  Get Inventory Summery List as per date range and id wise.");
            return View(data);
        }
        public async Task<IActionResult> ItemWiseFinishInventoryDetatils(int id = 0, int storeTypeFk = 0)
        {
            var model = await Task.Run(() => _service.FinishInventoryItemWiseListGet(id, storeTypeFk));
            _logger.LogInformation("--From: " + ControllerContext.ActionDescriptor.ActionName + "InventoryListGet(),  Get Last Sevendays Inventory.");
            return View(model);
        }

        public JsonResult FinishSubCategoryDropDownSpecific(int id)
        {
            if (id != 0)
            {
                var list = _integrationService.FinishSubCategoryDropDownCascading(id);
                return Json(list);
            }
            return null;
        }

        public JsonResult FinishItemDropDownSpecific(int id)
        {
            if (id != 0)
            {
                var list = _integrationService.FinishItemDropDownListCascading(id);
                return Json(list);
            }
            return null;
        }

        //RawItemDropDownListSpecific
        #endregion
    }
}