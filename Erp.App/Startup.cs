﻿using Erp.App.Models;
using Erp.Core.Services.Integration;
using Erp.Infrastructure;
using Erp.Infrastructure.Database;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Erp.App
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public IConfiguration Configuration { get; }
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            // Added by Bono
            services.Configure<FormOptions>(options =>
            {
                options.ValueCountLimit = int.MaxValue;
            });

            // for SQL connection
            // for postgre sql connection


            // update by Mintu
            services.AddHttpContextAccessor();
            services.AddScoped<MessageService>();
            //services.AddCors(o => o.AddPolicy("AllowOrigin", builder =>
            //{
            //    builder.AllowAnyOrigin()
            //           .AllowAnyMethod();
                      
            //}));
           

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2).AddSessionStateTempDataProvider();
            services.AddSession(options =>
            {
                // Set a short timeout for easy testing.
                options.IdleTimeout = TimeSpan.FromMinutes(60);
                //options.Cookie.HttpOnly = true;
                // Make the session cookie essential
               // options.Cookie.IsEssential = true;
            });
            // update by Mintu
            services.AddDbContext<InfrastructureDbContext>(options => options.UseSqlServer(Configuration.GetConnectionString("DefaultMSConnection")));

            //services.AddDbContext<InfrastructureDbContext>(options => options.UseNpgsql(Configuration.GetConnectionString("DefaultPostGreConnection")));
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.Configure<AppSettings>(Configuration);
            //services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            //if (env.IsDevelopment())
            //{
            app.UseDeveloperExceptionPage();
            //}
            //else
            //{
            //    app.UseExceptionHandler("/Home/Error");
            //    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
            //    app.UseHsts();
            //}
            //app.UseHttpsRedirection();
            app.UseStaticFiles();
            //app.UseCookiePolicy();
            UpdateDatabase(app);
            app.UseSession();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Auth}/{action=Login}/{id?}/{id2?}");
            });
        }
        private static void UpdateDatabase(IApplicationBuilder app)
        {
            using (IServiceScope serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>()
            .CreateScope())
            {
                using (InfrastructureDbContext context = serviceScope.ServiceProvider.GetService<InfrastructureDbContext>())
                {
                    //context.Database.Migrate();
                    //Seed.Initialize(serviceScope.ServiceProvider);
                }
            }
        }
    }
}
