const puppeteer = require( "puppeteer" );
let browser, page;
jest.setTimeout(100000);

describe( "UI-testing-with-Jest-and-Puppeteer", () => {    
    beforeAll( async () => {
        browser = await puppeteer.launch({
        headless: false,
        devtools: true
        });
    page = await browser.newPage();  
    await page.goto( "http://103.108.144.78/Jest", { waitUntil: "networkidle2" } );
    });
  
    test( "Page Title Match", async () => {
        const title = await page.title();       
        expect( title ).toEqual( "Home Page -Demo" );       
    });
    test("Check the data field existence and inputting data", async () => { 
       // await page.waitForSelector('.row > .form-horizontal > .form-group > .col-md-6 > #txtFirstName')
        await page.click('.row > .form-horizontal > .form-group > .col-md-6 > #txtFirstName')
        
        await page.type('.row > .form-horizontal > .form-group > .col-md-6 > #txtFirstName', 'My First Name')
        await page.type('.row > .form-horizontal > .form-group > .col-md-6 > #txtLastName', 'My Last Name')
        
        await page.waitForSelector('.row > .form-horizontal > .form-group > .col-md-6 > #txtDOB')
        await page.click('.row > .form-horizontal > .form-group > .col-md-6 > #txtDOB')
        
        await page.waitForSelector('.datepicker-days > .table-condensed > thead > tr > .datepicker-switch')
        await page.click('.datepicker-days > .table-condensed > thead > tr > .datepicker-switch')
        
        await page.waitForSelector('.datepicker-months > .table-condensed > thead > tr > .datepicker-switch')
        await page.click('.datepicker-months > .table-condensed > thead > tr > .datepicker-switch')
        
        await page.waitForSelector('.datepicker-years > .table-condensed > thead > tr > .datepicker-switch')
        await page.click('.datepicker-years > .table-condensed > thead > tr > .datepicker-switch')
        
        await page.waitForSelector('.datepicker-decades > .table-condensed > thead > tr > .datepicker-switch')
        await page.click('.datepicker-decades > .table-condensed > thead > tr > .datepicker-switch')
        
        await page.waitForSelector('.table-condensed > tbody > tr > td > .century:nth-child(1)')
        await page.click('.table-condensed > tbody > tr > td > .century:nth-child(1)')
        
        await page.waitForSelector('.table-condensed > tbody > tr > td > .decade:nth-child(9)')
        await page.click('.table-condensed > tbody > tr > td > .decade:nth-child(9)')
        
        await page.waitForSelector('.table-condensed > tbody > tr > td > .year:nth-child(3)')
        await page.click('.table-condensed > tbody > tr > td > .year:nth-child(3)')
        
        await page.waitForSelector('.table-condensed > tbody > tr > td > .month:nth-child(12)')
        await page.click('.table-condensed > tbody > tr > td > .month:nth-child(12)')
        
        await page.waitForSelector('.datepicker-days > .table-condensed > tbody > tr:nth-child(3) > .day:nth-child(5)')
        await page.click('.datepicker-days > .table-condensed > tbody > tr:nth-child(3) > .day:nth-child(5)')
        
        // await page.type("input[id=txtFirstName]", "My First  Name");
        //await page.type("input[id=txtAge]", "100");
       
       
       
        
    });

    test("Check for calculation", async () => {         
        await page.waitForSelector('.row > .form-horizontal > .form-group > .col-md-6 > #txtAge');
        let age = await page.$eval('.row > .form-horizontal > .form-group > .col-md-6 > #txtAge', el => el.value ); 
        expect(age).toBe(
            "47"
          );
    });

    test("Check for submission of a form", async () => {      
        await page.waitForSelector('.row > .form-horizontal > div > p > .btn-success')
        await page.screenshot({path: 'buddy-screenshot.png'});
        await page.click('.row > .form-horizontal > div > p > .btn-success')
        await page.waitForNavigation({
            waitUntil: 'domcontentloaded'
          });
          const title = await page.title();
          expect(title).toBe(
            "Home Page -Demo"
          );
    });
    test("Check for the data after the submission", async () => {      
        const data = await page.evaluate(() => {
            const tds = Array.from(document.querySelectorAll('table tr td'))
            return tds.map(td => td.innerHTML)
          });
          expect(data[0]).toContain(
            "My First Name"
          );
          await page.screenshot({path: 'buddy-screenshot1.png'});
    });
},100000);
