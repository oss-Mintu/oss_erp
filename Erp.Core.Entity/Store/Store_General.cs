﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Erp.Core.Entity.Common;
using Erp.Core.Entity.HRMS;
using Erp.Core.Entity.User;
using Erp.Core.Entity.Procurement;
using Erp.Core.Entity.Merchandising;

namespace Erp.Core.Entity.Store
{
    public class Store_General : CoreModel
    {
        public string StoreName { get; set; }
        public string Location { get; set; }
        public string Description { get; set; }
        public int Capacity { get; set; }

        //[ForeignKey("Common_Unit")]
        //public int Common_UnitFK { get; set; }
        //public Common_Unit Common_Unit { get; set; }

        public int? Common_StoreTypeFK { get; set; }

        public int HRMS_BusinessUnitFK { get; set; }
        [ForeignKey("HRMS_BusinessUnitFK")]
        public HRMS_BusinessUnit HRMS_BusinessUnit { get; set; }

        public int HRMS_UnitFK { get; set; }
        [ForeignKey("HRMS_UnitFK")]
        public HRMS_Unit HRMS_Unit { get; set; }

        public int User_DepartmentFK { get; set; }
        [ForeignKey("User_DepartmentFK")]
        public User_Department User_Department { get; set; }

        public List<Store_Bin> Bin { get; set; }
    }
    public class Store_Section : CoreModel
    {
        public string SectionName { get; set; }
        public string Description { get; set; }
        public int Capacity { get; set; }

        [ForeignKey("Store_General")]
        public int Store_GeneralFK { get; set; }
        public Store_General Store_General { get; set; }
        //public List<Store_Section> SectionList { get; set; }
    }
    public class Store_Bin : CoreModel
    {
        public string CID { get; set; }
        public string Section { get; set; }
        public int Row { get; set; }
        public int Shelf { get; set; }
        public int Rack { get; set; }
        public int Capacity { get; set; }

        public int TotalRow { get; set; }
        public int TotalShelf { get; set; }
        public int Dimension { get; set; }
        public double ItemTotalQty { get; set; }

        public int Store_GeneralFK { get; set; }
        [ForeignKey("Store_GeneralFK")]
        public Store_General Store_General { get; set; }

        public int Common_RawItemFK { get; set; }
       // [ForeignKey("Common_RawItemFK")]
       // public Common_RawItem Common_RawItem { get; set; }

        //public int Common_UnitFK { get; set; }
        //[ForeignKey("Common_UnitFK")]
        //public Common_Unit Common_Unit { get; set; }
    }
    public class Store_Requisition : CoreModel
    {
        public string CID { get; set; }
        public string Description { get; set; }
        public int Status { get; set; }
        public DateTime CreationDate { get; set; } = DateTime.Now;

        public string CancellationRemaks { get; set; }
        public int RequisitionType { get; set; }

        public int? ClosedByUserFK { get; set; }
        [ForeignKey("User_UserFK")]
        public User_User User_User { get; set; }

        public int? Store_GeneralOutFK { get; set; }
        [ForeignKey("Store_GeneralOutFK")]
        public Store_General Store_GeneralOut { get; set; }

        public int? Store_GeneralInFK { get; set; }
        [ForeignKey("Store_GeneralInFK")]
        public Store_General Store_GeneralIn { get; set; }

        //public int Merchandising_StyleFK { get; set; } 
        //[ForeignKey("Merchandising_StyleFK")]
        //public Merchandising_Style Merchandising_Style { get; set; }

        public List<Store_RequisitionSlave> Store_RequisitionSlave { get; set; }
    }
    public class Store_RequisitionSlave : CoreModel
    {
        public string Description { get; set; }
        public double RequisitionQuantity { get; set; }
        public double RemainingQty { get; set; }
        public bool InspectionRequired { get; set; } = false;

        public int? Common_ColorFK { get; set; }
        [ForeignKey("Common_ColorFK")]
        public string Common_Color { get; set; } 

        public int? Common_SizeFK { get; set; }
        [ForeignKey("Common_SizeFK")]
        //public Common_Color Common_Size { get; set; } 

        public int Store_RequisitionFK { get; set; }
        [ForeignKey("Store_RequisitionFK")]
        public Store_Requisition Store_Requisition { get; set; }

        public int Common_RawItemFK { get; set; }
        [ForeignKey("Common_RawItemFK")]
        public Common_RawItem Common_RawItem { get; set; }

        public decimal Common_Size { get; set; }

        public int Merchandising_StyleFK { get; set; }
        //[ForeignKey("Merchandising_StyleFK")]
        //public Merchandising_Style Merchandising_Style { get; set; }

        public int Merchandising_StyleSlaveFK { get; set; }
        //[ForeignKey("Merchandising_StyleSlaveFK")]
        //public Merchandising_StyleSlave Merchandising_StyleSlave { get; set; }

        public int HRMS_EmployeeFK { get; set; }
        //public HRMS_Employee HRMS_Employee { get; set; }

        public DateTime RequiredDate { get; set; } = DateTime.Now;
        public int Merchandising_StyleSetPackFK { get; set; }
       
    }
    public class Store_StockIn : CoreModel
    {
        public string CID { get; set; }
        public string ChallanNo { get; set; }
        public DateTime ChallanDate { get; set; }
        public DateTime ReceivedDate { get; set; }
        public DateTime WarrentyDate { get; set; }


        public string Name { get; set; }
        public string Description { get; set; }
        public string StockType { get; set; }
        public string StoreInType { get; set; }

        public double PurchaseQuantity { get; set; }
        public double RemainingQty { get; set; }
        public double DamagedQty { get; set; }
        public double ReceivedQty { get; set; }


        public int CommonUnitFK { get; set; }
        public int ItemIDFK { get; set; }
        public int IPOFK { get; set; }
        public int CommonSupplierFK { get; set; }
        public int CommonStoreFK { get; set; }
        public int StoreBinFK { get; set; }
        public int CommonCategoryFK { get; set; }
        public int CommonSubCategoryFK { get; set; }

        //public int Store_StockInMasterFK { get; set; }
        //public int Common_RawItemFK { get; set; }
        //public int Common_ColorFK { get; set; }

        public decimal Common_Size { get; set; }
        public int Merchandising_StyleFK { get; set; }
        public int Merchandising_StyleSlaveFK { get; set; }
        public int Procurement_PurchaseOrderSlaveFK { get; set; }

        public bool IsLocked { get; set; }
        public double UnitPrice { get; set; }
        public double TotalPrice { get; set; }
        public bool FixAsset { get; set; }
        public bool BinDefine { get; set; }

        public int Common_RawItemFK { get; set; }
        [ForeignKey("Common_RawItemFK")]
        public Common_RawItem Common_RawItem { get; set; }

        public int? Store_StockInMasterFK { get; set; }
        [ForeignKey("Store_StockInMasterFK")]
        public Store_StockInMaster Store_StockInMaster { get; set; }


        public int Common_SupplierFK { get; set; }
        [ForeignKey("Common_SupplierFK")]
        public Common_Supplier Common_Supplier { get; set; }

        public int Store_GeneralFK { get; set; }
        [ForeignKey("Store_GeneralFK")]
        public Store_General Store_General { get; set; }

        public int? Common_ColorFK { get; set; }
        [ForeignKey("Common_ColorFK")]
        public Common_Color Common_Color { get; set; }
    }
    public class Store_StockOut : CoreModel
    {
        public string CID { get; set; }
        public string ChallanNo { get; set; }
        public DateTime ChallanDate { get; set; }
        public DateTime ReceivedDate { get; set; }
        public DateTime WarrentyDate { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public string StockType { get; set; }
        public string StoreInType { get; set; }


        public bool IsLocked { get; set; }
        public double UnitPrice { get; set; }
        public double TotalPrice { get; set; }
        public double RemainingQty { get; set; }
        public double DamagedQty { get; set; }
        public double ReceivedQty { get; set; }

        public bool FixAsset { get; set; }
        public bool BinDefine { get; set; }


        public int CommonUnitFK { get; set; }
        public int ItemIDFK { get; set; }
        public int IPOFK { get; set; }
        public int CommonSupplierFK { get; set; }
        public int CommonStoreFK { get; set; }
        public int StoreBinFK { get; set; }
        public int CommonCategoryFK { get; set; }
        public int CommonSubCategoryFK { get; set; }

        //public int Common_RawItemFK { get; set; }
        //public int Common_ColorFK { get; set; }
        public decimal Common_Size { get; set; }
        public int Merchandising_StyleFK { get; set; }
        public int Merchandising_StyleSlaveFK { get; set; }
        public int Procurement_PurchaseOrderSlaveFK { get; set; }
        public int Store_RequisitionSlaveFK { get; set; }


        public int Common_RawItemFK { get; set; }
        [ForeignKey("Common_RawItemFK")]
        public Common_RawItem Common_RawItem { get; set; }

        public int Store_StockOutMasterFK { get; set; }
        [ForeignKey("Store_StockOutMasterFK")]
        public Store_StockOutMaster Store_StockOutMaster { get; set; }


        public int Common_SupplierFK { get; set; }
        [ForeignKey("Common_SupplierFK")]
        public Common_Supplier Common_Supplier { get; set; }

        public int Store_GeneralFK { get; set; }
        [ForeignKey("Store_GeneralFK")]
        public Store_General Store_General { get; set; }

        public int? Common_ColorFK { get; set; }
        [ForeignKey("Common_ColorFK")]
        public Common_Color Common_Color { get; set; }

        public int? Store_GeneralOutFK { get; set; }
        //[ForeignKey("Store_GeneralOutFK")]
        //public Store_General Store_GeneralOut { get; set; }

        public int? Store_GeneralInFK { get; set; }
        //[ForeignKey("Store_GeneralInFK")]
        //public Store_General Store_GeneralIn { get; set; }

        [NotMapped]
        public List<Store_StockOut> StoreStokOutSlave { get; set; }
    }
    public class Store_StockRegister : CoreModel
    {
        public string CID { get; set; }
        public string ItemSLNO { get; set; }
        public double LotQty { get; set; }
        public double UnitPrice { get; set; }
        public double MinPrice { get; set; }
        //public string Remarks { get; set; }
        public int Status { get; set; }

        public int HREmployeeDepartmentFK { get; set; }
        public int HREmployeeFK { get; set; }
        public DateTime HREmployeeReceivedDate { get; set; }

        public int CommonStoreFK { get; set; }
        public int StockInFK { get; set; }
        public int CommonUnitFK { get; set; }
        public int CommonBrandFK { get; set; }
        public int CommonModelFK { get; set; }
        public int CommonDepreciationFK { get; set; }
        public int RawItemFK { get; set; }
        public int LifeTime { get; set; }
    }

    public class Store_StockOutPersonal : CoreModel
    {
        public string CID { get; set; }
        public string ChallanNo { get; set; }
        public DateTime ReceivedDate { get; set; } = DateTime.Now;
        public double ReceivedQty { get; set; }
        public int IssueReturnStatus { get; set; }

        public int ItemIDFK { get; set; }
        public int CommonUnitFK { get; set; }
        public int CommonStoreFK { get; set; }
        public int StoreBinFK { get; set; }
        public int StoreGeneralFK { get; set; }

        public int CommonDepartmentFK { get; set; }
        public int EmployeeFK { get; set; }
        public int CommonBrandFK { get; set; }
        public int CommonModelFK { get; set; }
        public int RawItemFK { get; set; }
        public int CommonAssetFK { get; set; }
        public string Name { get; set; }
    }

    public class Store_StockInMaster : CoreModel
    {
        public string CID { get; set; }
        public string ChallanNo { get; set; }
        public DateTime ChallanDate { get; set; }
        public DateTime ReceivedDate { get; set; }
        //public DateTime WarrentyDate { get; set; }

        //public string Name { get; set; }
        public string Description { get; set; }
        public string StockType { get; set; }
        public string StoreInType { get; set; }

        //public int CommonStoreFK { get; set; }

        public int? PurchaseOrderFK { get; set; }
        [ForeignKey("PurchaseOrderFK")]
        public Procurement_PurchaseOrder Procurement_PurchaseOrder { get; set; }

        public int? Common_SupplierFK { get; set; }
        [ForeignKey("Common_SupplierFK")]
        public Common_Supplier Common_Supplier { get; set; }

        public int? Store_GeneralFK { get; set; }
        [ForeignKey("Store_GeneralFK")]
        public Store_General Store_General { get; set; }

    }
    public class Store_StockInSlave : CoreModel
    {
        public double PurchaseQuantity { get; set; }
        public double RemainingQty { get; set; }
        public double DamagedQty { get; set; }
        public double ReceivedQty { get; set; }


        public double UnitPrice { get; set; }
        public double TotalPrice { get; set; }
        public bool IsLocked { get; set; }
        public bool FixAsset { get; set; }
        public bool BinDefine { get; set; }


        public int Common_RawItemFK { get; set; }
        [ForeignKey("Common_RawItemFK")]
        public Common_RawItem Common_RawItem { get; set; }

        public int Store_StockInMasterFK { get; set; }
        [ForeignKey("Store_StockInMasterFK")]
        public Store_StockInMaster Store_StockInMaster { get; set; }


        public decimal Common_Size { get; set; }
        public int Merchandising_StyleFK { get; set; }
        public int Merchandising_StyleSlaveFK { get; set; }
        public int PurchaseOrderSlaveFK { get; set; }
        public int? Store_BinFK { get; set; }


        //public int Common_SupplierFK { get; set; }
        //[ForeignKey("Common_SupplierFK")]
        //public Common_Supplier Common_Supplier { get; set; }

        //public int Store_GeneralFK { get; set; }
        //[ForeignKey("Store_GeneralFK")]
        //public Store_General Store_General { get; set; }

        //public int Common_ColorFK { get; set; }
        //[ForeignKey("Common_ColorFK")]
        //public Common_Color Common_Color { get; set; }

        //[ForeignKey("Store_BinFK")]
        //public Store_Bin Store_Bin { get; set; } 

        //public int Common_RawCategoryFK { get; set; }
        //[ForeignKey("Common_RawCategoryFK")]
        //public Common_RawCategory Common_RawCategory { get; set; }

        //public int CommonSubCategoryFK { get; set; }
        //[ForeignKey("Common_RawSubCategoryFK")]
        //public Common_RawSubCategory Common_RawSubCategory { get; set; }

        //public int Common_RawItemFK { get; set; }
        //[ForeignKey("Common_RawItemFK")]
        //public Common_RawItem Common_RawItem { get; set; }

        //public int Common_UnitFK { get; set; }
        //[ForeignKey("Common_UnitFK")]
        //public Common_Unit Common_Unit { get; set; }  

    }

    public class Store_StockOutMaster : CoreModel
    {
        public string CID { get; set; }
        public string ChallanNo { get; set; }
        public DateTime ChallanDate { get; set; }
        public DateTime ReceivedDate { get; set; }

        public string Description { get; set; }
        public string StockType { get; set; }
        public string StoreOutType { get; set; }
    }
    public class Store_StockOutSlave : CoreModel
    {
        public double PurchaseQuantity { get; set; }
        public double RemainingQty { get; set; }
        public double DamagedQty { get; set; }
        public double ReceivedQty { get; set; }

        public double UnitPrice { get; set; }
        public double TotalPrice { get; set; }
        public bool IsLocked { get; set; }
        public bool FixAsset { get; set; }
        public bool BinDefine { get; set; }

        public int Common_RawItemFK { get; set; }
        [ForeignKey("Common_RawItemFK")]
        public Common_RawItem Common_RawItem { get; set; }

        public int Store_StockOutMasterFK { get; set; }
        [ForeignKey("Store_StockOutMasterFK")]
        public Store_StockOutMaster Store_StockOutMaster { get; set; }

        public int Common_SupplierFK { get; set; }
        [ForeignKey("Common_SupplierFK")]
        public Common_Supplier Common_Supplier { get; set; }

        public int Store_GeneralFK { get; set; }
        [ForeignKey("Store_GeneralFK")]
        public Store_General Store_General { get; set; }

        public int? Common_ColorFK { get; set; }
        [ForeignKey("Common_ColorFK")]
        public Common_Color Common_Color { get; set; }

        public decimal Common_Size { get; set; }
        public int Merchandising_StyleFK { get; set; }
        public int Merchandising_StyleSlaveFK { get; set; }
        public int PurchaseOrderSlaveFK { get; set; }

        public int? Store_BinFK { get; set; }
    }

    public class Store_QCInformation : CoreModel
    {
        public string CID { get; set; }
        public string DeviceId { get; set; }

        public int? Merchandising_StyleIdFK { get; set; }
        [ForeignKey("Merchandising_StyleIdFK")]
        public Merchandising_Style Merchandising_Style { get; set; }

        public int? Common_ColorFK { get; set; }
        [ForeignKey("Common_ColorFK")]
        public Common_Color Common_Color { get; set; }

        public int? Common_SizeFK { get; set; }
        [ForeignKey("Common_SizeFK")]
        public Common_Size Common_Size { get; set; }

        public int? Common_ProductionLineFK { get; set; }
        [ForeignKey("Common_ProductionLineFK")]
        public Common_ProductionLine Common_ProductionLine { get; set; }

        public int? Merchandising_StyleSetPackFK { get; set; }
        [ForeignKey("Merchandising_StyleSetPackFK")]
        public Merchandising_StyleSetPack Merchandising_StyleSetPack { get; set; }

        public decimal QCQuantity { get; set; }
        public int RejectQuantityOne { get; set; }

        public int RejectQuantityTwo { get; set; }
        public int RejectQuantityThree { get; set; }
        public int RejectQuantityFour { get; set; }
        public int RejectQuantityFive { get; set; }
        public int RejectQuantitySix { get; set; }
        public int RejectQuantityTotal { get; set; }
        public DateTime EntryDate { get; set; }



    }
    public class Store_AssetDepreciation : CoreModel
    {
        public string CID { get; set; }
        public string ItemSLNO { get; set; }
        public double LotQty { get; set; }
        public double UnitPrice { get; set; }
        public double MinPrice { get; set; }
        //public string Remarks { get; set; }
        public int Status { get; set; }

        public int HREmployeeDepartmentFK { get; set; }
        public int HREmployeeFK { get; set; }
        public DateTime HREmployeeReceivedDate { get; set; }

        public int CommonStoreFK { get; set; }
        public int StockInFK { get; set; }
        public int CommonUnitFK { get; set; }
        public int CommonBrandFK { get; set; }
        public int CommonModelFK { get; set; }
        public int CommonDepreciationFK { get; set; }
        public int RawItemFK { get; set; }
        public int LifeTime { get; set; }
    }

    public class Store_ItemWiseConsumption : CoreModel
    {
        public string StyleName { get; set; }  
        public int Merchandising_StyleFK { get; set; }
        public int Common_RawItemFK { set; get; }
        public string Name { set; get; }
        public string UnitName { set; get; }
        public double ConsumptionDz { set; get; }
        public double OrderConsumQty { set; get; }
        public double FinishQtyDz { set; get; }
        public double TotalRecMain { set; get; }
        public double TotalRecCutting { set; get; }
        public double TotalRecSewing { set; get; }
        public double TotalRecIroning { set; get; }
        public double TotalRecPacking { set; get; }
        public double TotalRecFinish { set; get; }
        public double TotalOutMain { set; get; }
        public double TotalOutCutting { set; get; }
        public double TotalOutSewing { set; get; }
        public double TotalOutIroning { set; get; }
        public double TotalOutPacking { set; get; }
        public double TotalOutFinish { set; get; }
    }
    public class Store_ItemWiseConsumptionAllStyle : CoreModel
    { 
        public string StyleName { get; set; }
        public int Merchandising_StyleFK { get; set; }
        public int Common_RawItemFK { set; get; }
        public string Name { set; get; }
        public string UnitName { set; get; }
        public double ConsumptionDz { set; get; }
        public double OrderConsumQty { set; get; }
        public double FinishQtyDz { set; get; }
        public double TotalRecMain { set; get; }
        public double TotalRecCutting { set; get; }
        public double TotalRecSewing { set; get; }
        public double TotalRecIroning { set; get; }
        public double TotalRecPacking { set; get; }
        public double TotalRecFinish { set; get; }
        public double TotalOutMain { set; get; }
        public double TotalOutCutting { set; get; }
        public double TotalOutSewing { set; get; }
        public double TotalOutIroning { set; get; }
        public double TotalOutPacking { set; get; }
        public double TotalOutFinish { set; get; }
    }
    
}
