﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Erp.Core.Entity.Inventory
{
    public class Inventorydb : CoreModel
    {
        public int  StoreTypeFk { get; set; }
        public int  TransactionType { get; set; }
        public int? Procurement_PurchaseInvoiceFK { get; set; }
        public int? Procurement_PurchaseOrderSlaveID { get; set; }
        public int? Merchandising_StyleID { get; set; }
        public int? Merchandising_StyleSlaveID { get; set; }
        public int? Merchandising_BOFID { get; set; }
        public int? Store_StockInFK { get; set; }
             
        public int Common_RawItemFK { get; set; }
        public decimal Amount { get; set; } = 0;
        public decimal Price { get; set; } = 0;

        //new property
        public int? Store_StockOutFK { get; set; }
    }
}
