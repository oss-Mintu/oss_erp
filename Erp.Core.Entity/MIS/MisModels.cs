﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Erp.Core.Entity.MIS
{
    public class Mis_CMEarned : CoreModel
    {
        public DateTime? Date { get; set; } = DateTime.Today;
        public string Month { get; set; }
        public string Year { get; set; }
        public int Merchandising_StyleFK { get; set; } = 0;
        public string StyleName { get; set; }

        public int Quantity { get; set; }
        public decimal Value { get; set; } // per unit value.
    }
    public class Mis_CashInHand : CoreModel
    {
       
        public DateTime Date { get; set; }
        public string Accounting_Head { get; set; }
        public decimal Value { get; set; }
    }
    public class Mis_CashAtBank : CoreModel

    {
        public DateTime Date { get; set; }
        public string Accounting_Head { get; set; }
        public decimal Value { get; set; }
    }
    public class Mis_IncomeStatement : CoreModel
    {
        public int ChartTableId { get; set; }
        public int BaseHeadId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }

        public string Accounting_Head { get; set; }
        public decimal Value { get; set; }
    }

    public class Mis_BalanceSheet : CoreModel
    {
        public int ChartTableId { get; set; }
        public int BaseHeadId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string Accounting_Head { get; set; }
        public decimal Value { get; set; }
    }
    public class Mis_AccountsReceivable : CoreModel
    {
        public int Common_BuyerFk { get; set; }

       
        public DateTime Date { get; set; }
        public decimal Value { get; set; }
    }
    public class Mis_AccountsPayable : CoreModel
    {
        public int Common_SupplierFk { get; set; }
        public DateTime Date { get; set; }
        public decimal Value { get; set; }
    }
    public class Mis_OrderConfirm : CoreModel
    {
        public string OrderNo { get; set; }        
        public DateTime? FinalDeliveryDate { get; set; }
        public int Common_BuyerFk { get; set; }
        public decimal OrderValue { get; set; }
        public decimal CmValue { get; set; }
        public int OrderQty { get; set; }
    }

    public class Mis_RawMaterials : CoreModel
    {
        public int Common_RawCategoryFk { get; set; }
        public int RawValue { get; set; }
        public int WIPValue { get; set; }
    }
    public class Mis_ShipmentStatus : CoreModel
    {
     

        public int Common_BuyerFk { get; set; }
        public string OrderNo { get; set; }
        public DateTime? Date { get; set; }
        public int OrderQuantity { get; set; }
        public int Exfactory { get; set; }
        public int DeliveredQuantity { get; set; }
        public int DueQty { get; set; }

    }

    public class Mis_DailyProduction : CoreModel
    {
        public DateTime ProductionDate { get; set; }
        public int Common_BuyerFk { get; set; }
        public string OrderNo { get; set; }
        public int Knitting { get; set; }
        public int Dyeing { get; set; }
        public int Cutting { get; set; }
        public int Sewing { get; set; }
        public int Finishing { get; set; }
        public int KnittingDone { get; set; }
        public int DyeingDone { get; set; }
        public int CuttingDone { get; set; }
        public int SewingDone { get; set; }
        public int FinishingDone { get; set; }
        public int OrderQty { get; set; }
        public decimal SMV { get; set; }
        public int Ironing { get; set; }
        public int IroningDone { get; set; }

    }
    public class Mis_DailyAttendance : CoreModel
    {
        public int User_DepartmentFk { get; set; }
        public DateTime AttendanceDate { get; set; }
        public int WorkingHour { get; set; }
        public int TotalEmployee { get; set; }
        public int Present { get; set; }
        public int Absent { get; set; }
        public int OnLeave { get; set; }
        public int Off { get; set; }
    }

    public class Mis_BTBLCStatus : CoreModel
    {
        public string UDNo { get; set; }
        public decimal MasterLCTotalValue { get; set; }
        public int UdID { get; set; }
        public decimal BTBLimit { get; set; }
        public decimal BTBIssued { get; set; }
        public decimal BTBLiabilites { get; set; }
        public decimal BTBAcceptedValue { get; set; }
    }
    public class Mis_BTBLCStatusSlave : CoreModel
    {
        public string MasterLCNo { get; set; }
        public decimal MasterLCValue { get; set; }
        public decimal UDFK { get; set; }
       
    }

    public class Mis_FinancialStatus : CoreModel
    {
        public string Month { get; set; }
        public string Year { get; set; }
        public decimal Inflow { get; set; }
        public decimal Outflow { get; set; }
        public decimal RevenueProjection { get; set; }
        public decimal ExpenditureProjection { get; set; }
    }
    public class Mis_Log : CoreModel
    {       
        public string TableName { get; set; }
        public string Status { get; set; }
    }
}

