﻿using Erp.Core.Entity.Common;
using Erp.Core.Entity.Merchandising;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Erp.Core.Entity.Shipment
{
    public class Shipment_ShipmentInvoice : CoreModel
    {
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime InvoiceDate { get; set; }
        public string InvoiceCID { get; set; }
        [StringLength(15, ErrorMessage = "Upto 15 Chracter")]
        public string ErcNo { get; set; }
        [StringLength(50, ErrorMessage = "Upto 50 Chracter")]
        public string InvoiceIdNo { get; set; }
        public string ShippedBy { get; set; }
        public int Shipment_PortOfLoadingFk { get; set; }               
        [StringLength(50, ErrorMessage = "Upto 50 Chracter")]
        public string BLNo { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("BL Date")]
        public DateTime BLDate { get; set; }
        public string ExpNo { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime ExpDate { get; set; }
        [DataType(DataType.MultilineText)]
        public string ShippingMarks { get; set; }
        public int Shipment_TermsOfShipmentFK { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime EstimatedReceivedDate { get; set; }        
        public decimal AdjustedValue { get; set; }       
        public string AdjustedNote { get; set; }
    }
    public class Shipment_ShipmentInvoiceSlave : CoreModel
    {
        public int Shipment_InstructionSlaveFk { get; set; }
        public int Shipment_InvoiceFk { get; set; }
        public int Merchandising_StyleFk { get; set; }
        public int Merchandising_StyleShipmentScheduleFk { get; set; }
        public decimal InvoicedQuantity { get; set; }
        //public int CTN { get; set; }
    }

    public class Shipment_TermsOfShipment : CoreModel
    {
        public string Name { get; set; }
    }
    public class Shipment_InvoicedBuyerNotifyParty : CoreModel
    {
        public int Shipment_InvoiceFk { get; set; }
        public int Common_BuyerNotifyPartyFk { get; set; }

    }
}
