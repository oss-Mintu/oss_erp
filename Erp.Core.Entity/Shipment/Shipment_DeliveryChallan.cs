﻿using Erp.Core.Entity.Common;
using Erp.Core.Entity.Merchandising;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Erp.Core.Entity.Shipment
{
    public class Shipment_DeliveryChallan : CoreModel
    {
        public DateTime Date { get; set; }
        public string ChallanNo { get; set; }
        
        public int Common_CNFSupplierFk { get; set; }
        public string ContactPersonName { get; set; }
        public int Common_TransporterSupplierFk { get; set; }
        public string DriverName { get; set; }
        public string LockNo { get; set; }
        public string DLNo { get; set; }
        public string TruckNo { get; set; }
        public string DriverMobile { get; set; }        
        public string ContactPersonMobile { get; set; }
        public string InTime { get; set; }
        public string OutTime { get; set; }
        public string GatePassNo { get; set; }
        public DateTime GatePassDate { get; set; }    
        public int ShipmentUnloadingPortFk { get; set; }
        
    }

    public class Shipment_DeliveryChallanSlave : CoreModel
    {
        public int Shipment_DeliveryChallanFk { get; set; }        
        public int Shipment_InvoiceSlaveFk { get; set; }
        public int Shipment_InstructionSlaveFk { get; set; }
        public int Merchandising_StyleShipmentScheduleFk { get; set; }
        public int Merchandising_StyleFk { get; set; }
        public decimal DeliveryChallanQuantity { get; set; }
    }
}