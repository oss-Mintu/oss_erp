﻿using Erp.Core.Entity.Common;
using Erp.Core.Entity.Merchandising;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Erp.Core.Entity.Shipment
{
    public class Shipment_ExportRealisation : CoreModel
    {
        [DisplayName("Input Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }

        [DisplayName("Bill No")]
        [StringLength(100, ErrorMessage = "Upto 100 Chracter")]
        public string BillNo { get; set; }

        [DisplayName("FDBC")]
        [StringLength(100, ErrorMessage = "Upto 100 Chracter")]
        public string FDBC { get; set; }

        [DefaultValue(0.00)]
        [DisplayName("Bill Value")]
        public decimal BillValue { get; set; }

        [DefaultValue(0.00)]
        [DisplayName("Realised Amount")]
        public decimal RealisedAmount { get; set; }

        [DefaultValue(0.0000)]
        [DisplayName("Rate Tk")]
        public decimal DollarValue { get; set; }

        [DisplayName("Bill Of Exchange")]
        public int Shipment_BillOfExchangeFK { get; set; }

        [DisplayName("FDBC Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime FDBCDate { get; set; }

        [DisplayName("Value Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime ValueDate { get; set; }

        public bool IsFinal { get; set; }

        [DefaultValue(0.00)]
        [DisplayName("FCBPAR")]
        public decimal FcbParAmt { get; set; }

        [DefaultValue(0.00)]
        [DisplayName("Others")]
        public decimal Others { get; set; }

        [DefaultValue(0.00)]
        [DisplayName("Bill Purchase")]
        public decimal BillPurchase { get; set; }

        [DefaultValue(0.00)]
        [DisplayName("ERQ")]
        public decimal ERQAmt { get; set; }

        [DefaultValue(0.00)]
        [DisplayName("Fx Trading")]
        public decimal FxTrading { get; set; }

        [DefaultValue(0.00)]
        [DisplayName("PC Adjust")]
        public decimal PcAdjust { get; set; }

        [DefaultValue(0.00)]
        [DisplayName("Buying Commission")]
        public decimal BuyingCommission { get; set; }

        [DefaultValue(0.00)]
        [DisplayName("FDR")]
        public decimal FdrAmount { get; set; }

        [DefaultValue(0.00)]
        [DisplayName("Courier Expense")]
        public decimal CourierExpense { get; set; }

        [DefaultValue(0.00)]
        [DisplayName("RMG")]
        public decimal RmgAmount { get; set; }

        [DefaultValue(0.00)]
        [DisplayName("AIT")]
        public decimal AitAmount { get; set; }

        [DefaultValue(0.00)]
        [DisplayName("SOD A/C")]
        public decimal SodAcAmount { get; set; }

        [DefaultValue(0.00)]
        [DisplayName("CD Amount")]
        public decimal CdAmount { get; set; }

        [DefaultValue(0.00)]
        [DisplayName("Time Loan Amount")]
        public decimal TimeLoanAmount { get; set; }

        [DefaultValue(0.00)]
        [DisplayName("Doc Handling Charge")]
        public decimal DocHandlingCharge { get; set; }
    }
    public class Shipment_ExportRealisationExpense : CoreModel
    {

        [DisplayName("Export Realisation No")]
        public int Acc_ExportRealisationFK { get; set; }

        [DefaultValue(0.00)]
        [DisplayName("Percent")]
        public decimal Percent { get; set; }

        [DefaultValue(0.00)]
        [DisplayName("Amount(BDT)")]
        public decimal TotalAmount { get; set; }

        [DisplayName("Particulars")]
        public int Acc_AcNameFK { get; set; }

    }
}
