﻿using Erp.Core.Entity.Common;
using Erp.Core.Entity.Merchandising;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Erp.Core.Entity.Shipment
{
    public class Shipment_ShipmentInstruction : CoreModel
    {
        public DateTime SIDate { get; set; }
        public string SICID { get; set; }
    }
    public class Shipment_ShipmentInstructionSlave : CoreModel
    {
        public int? Shipment_ShipmentInstructionFk { get; set; }
        public int InstructionQuantity { get; set; }
        public int? Merchandising_StyleFk { get; set; }
        //[ForeignKey("Merchandising_StyleFk")]
        //public Merchandising_Style Merchandising_Style { get; set; }

        public int Merchandising_StyleShipmentScheduleFk { get; set; }
        //[ForeignKey("Merchandising_StyleShipmentScheduleFk")]
        //public Merchandising_StyleShipmentSchedule Merchandising_StyleShipmentSchedule { get; set; }

    }
}
