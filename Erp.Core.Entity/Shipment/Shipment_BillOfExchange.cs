﻿using Erp.Core.Entity.Common;
using Erp.Core.Entity.Merchandising;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Erp.Core.Entity.Shipment
{
    public class Shipment_BillOfExchange : CoreModel
    {
        public bool IsFinal { get; set; } = false;

        [DisplayName("Bill Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime BoEDate { get; set; }
        [Required(ErrorMessage = "Bill NO is Required")]
        [DisplayName("Bill NO")]
        public string BoECID { get; set; }
       
        [DisplayName("Courier NO")]
        public string CourierNO { get; set; }

        [DisplayName("Courier Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime CourierDate { get; set; }

        
        public int Commercial_ECIFK { get; set; }

        [StringLength(60, ErrorMessage = "Upto 60 Chracter")]
        [DisplayName("Received Type")]
        public string ReceivedType { get; set; }

        [DisplayName("FDBC NO")]
        public string FDBCNO { get; set; }

        [DisplayName("FDBC Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime FDBCDate { get; set; }

        [DisplayName("Description")]
        public string Description { get; set; }
    }
    public class Shipment_BillOfExchangeSlave : CoreModel
    {
      
       
        public int Shipment_ShipmentInvoiceFk { get; set; }    
        public int Shipment_BillOfExchangeFk { get; set; }
        [DisplayName("Remarks")]
        public string Description { get; set; }


    }
}
