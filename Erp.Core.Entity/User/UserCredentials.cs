﻿using Erp.Core.Entity.HRMS;
using Erp.Core.Entity.Store;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Erp.Core.Entity.User
{
    public class User_Department : CoreModel
    {
        public string Name { get; set; }
        public string Code { get; set; }
        //public int? HRMS_UnitFK { get; set; }
        //[ForeignKey("HRMS_UnitFK")]
        //public HRMS_Unit HRMS_Unit { get; set; }
        public List<User_User> User_User { get; set; }
    }
    public class User_User : CoreModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Photo { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public int User_AccessLevelFK { get; set; }
        public int User_RoleFK { get; set; }
        [DefaultValue(false)]
        public bool Locked { get; set; }
        public int HRMS_EmployeeFK { get; set; }
        public int User_DepartmentFK { get; set; }

        [ForeignKey("User_DepartmentFK")]
        public User_Department User_Department { get; set; }
        [ForeignKey("User_AccessLevelFK")]
        public User_AccessLevel User_AccessLevel { get; set; }
        [ForeignKey("User_RoleFK")]
        public User_Role User_Role { get; set; }

    }
    public class User_SubMenu : CoreModel
    {
        public string Name { get; set; }
        public int User_MenuFk { get; set; }

        public int Priority { get; set; }

        [ForeignKey("User_MenuFk")]
        public User_Menu User_Menu { get; set; }
        public List<User_MenuItem> User_MenuItem { get; set; }

    }
    public class User_RoleMenuItem : CoreModel
    {

        public int User_RoleFK { get; set; }
        public int User_MenuItemFk { get; set; }
        public bool IsAllowed { get; set; }



        [ForeignKey("User_MenuItemFk")]
        public User_MenuItem User_MenuItem { get; set; }
        [ForeignKey("User_RoleFK")]
        public User_Role User_Role { get; set; }
    }
    public class User_Role : CoreModel
    {

        public string Name { get; set; }
        public string Description { get; set; }


        public List<User_User> User_User { get; set; }

    }
    public class User_MenuItem : CoreModel
    {
        public string Name { get; set; }
        public int Priority { get; set; }      
        public int User_SubMenuFk { get; set; }
        public string Method { get; set; }
        public bool IsAlone { get; set; }

        public bool IsMultiLayerOption { get; set; } = false;

        [ForeignKey("User_SubMenuFk")]
        public User_SubMenu User_SubMenu { get; set; }
    }
    public class User_Menu : CoreModel
    {
        public int Priority { get; set; }

        public string Name { get; set; }

        public List<User_SubMenu> User_SubMenu { get; set; }
       
        
    }
    public class User_AccessLevel : CoreModel
    {
        public string Name { get; set; }
        public string Description { get; set; }

        public List<User_User> User_User { get; set; }
    }
}
