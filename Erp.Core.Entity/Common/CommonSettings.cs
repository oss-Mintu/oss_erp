﻿using Erp.Core.Entity.HRMS;
using Erp.Core.Entity.Merchandising;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Erp.Core.Entity.Common
{
   
    public class Common_RawCategory : CoreModel
    {
        public string Name { get; set; }
        public int? AutoView { get; set; }
        public Nullable<int> CategoryTypeFK { get; set; }
        public List<Common_RawSubCategory> Common_RawSubCategory { get; set; }
    }
    public class Common_RawSubCategory : CoreModel
    {
        public string Name { get; set; }
        public int Common_RawCategoryFK { get; set; }
        [ForeignKey("Common_RawCategoryFK")]
        public Common_RawCategory Common_RawCategory { get; set; }
        public List<Common_RawItem> Common_RawItem { get; set; }
    }
    public class Common_RawItem : CoreModel
    {
        public string Name { get; set; }
        public int Common_RawSubCategoryFK { get; set; }
        [ForeignKey("Common_RawSubCategoryFK")]
        public Common_RawSubCategory Common_RawSubCategory { get; set; }
        public int Common_UnitFK { get; set; }
        [ForeignKey("Common_UnitFK")]
        public Common_Unit Common_Unit { get; set; }
        public double Quantity { get; set; }
        public string GSM { get; set; }
        public Nullable<int> AccountHead { get; set; }

        public List<Merchandising_Style> Merchandising_Style { get; set; }
    }

    public class Common_Brand: CoreModel
    {
        public string Name { get; set; }
    }
    public class Common_Model : CoreModel
    {
        public string Name { get; set; }
        public int Common_BrandFK { get; set; }
    }
    public class Common_FinishCategory : CoreModel
    {
        public string Name { get; set; }
        public List<Common_FinishSubCategory> Common_FinishSubCategory { get; set; }
    }
    public class Common_FinishSubCategory : CoreModel
    {
        public string Name { get; set; }
        public int Common_FinishCategoryFK { get; set; }
        [ForeignKey("Common_FinishCategoryFK")]
        public Common_FinishCategory Common_FinishCategory { get; set; }
        public List<Common_FinishItem> Common_FinishItem { get; set; }

    }
    public class Common_FinishItem : CoreModel
    {
        public string Name { get; set; }
        public int Common_FinishSubCategoryFK { get; set; }
        [ForeignKey("Common_FinishSubCategoryFK")]
        public Common_FinishSubCategory Common_FinishSubCategory { get; set; }
        public int Common_UnitFK { get; set; }
        [ForeignKey("Common_UnitFK")]
        public Common_Unit Common_Unit { get; set; }

    }
    public class Common_Buyer : CoreModel
    {
        public string BuyerID { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string ContactPerson { get; set; }
        public string Address { get; set; }
        public int Common_CountryFK { get; set; }
        public int AccHeadID { get; set; }
    }
    public class Common_BuyerNotifyParty : CoreModel
    {
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string ContactPerson { get; set; }
        public string Address { get; set; }
        public int Common_CountryFK { get; set; }
        public int Common_BuyerFK { get; set; }
    }
    public class Common_Supplier : CoreModel
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public int SupplierType { get; set; }
        public bool IsForeignSupplier { get; set; }
        public string ContactPerson { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Address { get; set; }
        public int Common_CountryFK { get; set; }
        [ForeignKey("Common_CountryFK")]
        public Common_Country Common_Country { get; set; }
        public int AccHeadID { get; set; }


    }   
    public class Common_Unit : CoreModel
    {
        public string Name { get; set; }
        public List<Common_FinishItem> Common_FinishItem { get; set; }
        public List<Common_RawItem> Common_RawItem { get; set; }
    }
    public class Common_Currency : CoreModel
    {
        public string Name { get; set; }
        public decimal ConversionRateToBDT { get; set; }
    }
    public class Common_CurrencyChangesLog : CoreModel
    {
        public int Common_CurrencyFK { get; set; }
        [ForeignKey("Common_CurrencyFK")]
        public Common_Currency Common_Currency { get; set; }
        public decimal PreviousConversionRateToBDT { get; set; }
        public decimal ChangesConversionRateToBDT { get; set; }
    }
    public class Common_Country : CoreModel
    {
        public string Name { get; set; }
        public string Code { get; set; }
    }

    public class Common_CountryPort : CoreModel
    {
        public string Name { get; set; }
        public int? Common_CountryFK { get; set; }
        [ForeignKey("Common_CountryFK")]
        public Common_Country Common_Country { get; set; }
        public string Code{ get; set; }
    }
    public class Common_Size : CoreModel
    {
        public string Name { get; set; }
    } 

    public class Common_RejectType : CoreModel
    {
        //public int ID { get; set; }
        public string RejectOne { get; set; }
        public string RejectTwo { get; set; }
        public string RejectThree { get; set; }
        public string RejectFour { get; set; }
        public string RejectFive { get; set; }
        public string RejectSix { get; set; }
    }

    public class Common_CountrySize : CoreModel
    {
        public int? Common_SizeFK { get; set; }
        [ForeignKey("Common_SizeFK")]
        public Common_Size Common_Size { get; set; }

        public int? Common_CountryFK { get; set; }
        [ForeignKey("Common_CountryFK")]
        public Common_Country Common_Country { get; set; }
    }
    public class Common_Color : CoreModel
    {
        public string Name { get; set; }
    }
    public class Common_LcType : CoreModel
    {
        public string Name { get; set; }
        public int IsECIOrBB { get; set; } //enum ECIOrBB ECI = 1, BackToBack


    }
    
        
    public class Common_BuyerBank : CoreModel
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string ContactNo { get; set; }
        public int? Common_CountryFK { get; set; }
        public int? Common_BankFK { get; set; }
        public string ContactPerson { get; set; }
        public string Email { get; set; }
        public string SwiftCode { get; set; }
        public int Common_BuyerFK { get; set; }
    }
    public class Common_LienBank : CoreModel
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string ContactNo { get; set; }
        public string ContactPerson { get; set; }
        public int? Common_CountryFK { get; set; }
        public string SwiftCode { get; set; }
        public string Email { get; set; }
        public int? Common_BankFK { get; set; }
    }
    public class Common_CompanyBank : CoreModel 
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string ContactNo { get; set; }
        public int? Common_CountryFK { get; set; }
        public string SwiftCode { get; set; }
        public string ContactPerson { get; set; }
        public string Email { get; set; }
        public int Common_BankFK { get; set; }
        public int Common_CompanySetupFK { get; set; }
       
    }

    public class Common_Bank : CoreModel
    {
        public string Name { get; set; }
        public string SwiftCode { get; set; }
        public bool Flag { get; set; }
        public string Email { get; set; }
        public int Common_CountryFK { get; set; }
    }

    public class Common_TCTitle : CoreModel
    {
        public string Name { get; set; }
    }    
    public class Common_ProductionLine : CoreModel
    {
        public Nullable<int> ProductionStepFK { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public int Priority { get; set; }
        [ForeignKey("Common_SectionLineFK")]
        public int Common_SectionLineFK { get; set; }
        public Common_SectionLine Common_SectionLine { get; set; }
        public bool IsClosed { get; set; }
        public List<Common_LineChiefLine> Common_LineChiefLine { get; set; }
    }
    public class Common_SectionLine : CoreModel
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public int BuildingFk { get; set; }
        public int FloorFk { get; set; }

        public int? Common_ProductionFloorFK { get; set; }
        [ForeignKey("Common_ProductionFloorFK")]
        public Common_ProductionFloor Common_ProductionFloor{ get; set; }
      
        public bool IsClosed { get; set; }
    }

    public class Common_LineChief : CoreModel
    {
        public string Name { get; set; }
        public List<Common_LineChiefLine> Common_LineChiefLine { get; set; }
    }
    public class Common_LineSuperVisor : CoreModel
    {
        public string Name { get; set; }
        public List<Common_LineChiefLine> Common_LineChiefLine { get; set; }
    }

    public class Common_ProductionUnit : CoreModel
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public bool IsColsed { get; set; }
        public int? HRMS_UnitFK{get;set;}
    }

    public class Common_ProductionFloor : CoreModel
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public bool IsColsed { get; set; }
        public int Common_ProductionUnitFK { get; set; }
        [ForeignKey("Common_ProductionUnitFK")]
        public Common_ProductionUnit Common_ProductionUnit { get; set; }
    }

    public class Common_LineChiefLine:CoreModel
    {
        public int? Common_LineChiefFk { get; set; }
        [ForeignKey("Common_LineChiefFk")]
        public Common_LineChief Common_LineChief { get; set; }

        public int? Common_LineSuperVisorFk { get; set; }
        [ForeignKey("Common_LineSuperVisorFk")]
        public Common_LineSuperVisor Common_LineSuperVisor { get; set; }

        public int Common_ProductionLineFk { get; set; }
        [ForeignKey("Common_ProductionLineFk")]
        public Common_ProductionLine Common_ProductionLine { get; set; }

        public bool IsLineChief { get; set; }
    }

    //// Dummy Model for Bulk Insert
    public class Common_Product //: CoreModel
    {
        public int id { get; set; }
        public string barcode { get; set; }
        public string description { get; set; }
        public string size { get; set; }
        public string vatcode { get; set; } 

        public int quantity { get; set; }
        public double cost { get; set; }
        public double price { get; set; }
        //public override string ToString()
        //{
        //    return String.Format("string: {0}; string: {1}; string: {2}; string: {3};  int: {4}; decimal {5}; decimal {6}",
        //        barcode, description, size, vatcode, quantity, cost, price
        //    );
        //}
    }

    public class Common_Agent : CoreModel
    {
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string ContactPerson { get; set; }
        public string Address { get; set; }
    }

    public class Common_InspectionAgent  : CoreModel
    {
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string ContactPerson { get; set; }
        public string Address { get; set; }
    }

    public class Common_CompanySetup : CoreModel
    {
        public string Name { get; set; }
        public string ShortName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string ContactPerson { get; set; }
        public string TIN { get; set; }
        public string TradeLicense { get; set; }
        public string VatRegNo { get; set; }
        public string Address { get; set; }
        //public string AddressLine2 { get; set; }
        //public string AddressLine3 { get; set; }
        //public string AddressLine4 { get; set; }

        public byte[] Image { get; set; }
    }

    public class Common_Notification : CoreModel
    {
        public int StyleID { get; set; }
        public int TStyle { get; set; }
        public int TCompleteStyle { get; set; }
        public int NStyle { get; set; }
        public int IncompleteBOM { get; set; }
        public int CompleteBOM { get; set; }
        public int TPR{ get; set; }
        public int TCompletePR { get; set; }
        public int TPartialPR { get; set; }
        public int TCancelPR { get; set; }
        public int TCompletePO { get; set; }
        public int TPartialPO { get; set; }
        public int TCancelPO { get; set; }
        public int ShipmentCountDay { get; set; }
        public int ShipmentCountMonth { get; set; }
        public int TDraftPR { get; set; }
        public int TSubmittedPR { get; set; }
        public int PRApproved1 { get; set; }
        public int PRApproved2 { get; set; }
        public int PRApproved3 { get; set; }
        public int PRHold { get; set; }
        public int PartialPO { get; set; }

        public int FullPO { get; set; }
        public int TDratfPO { get; set; }

        public int TSubmittedPO { get; set; }
        public int POApproved1 { get; set; }
        public int POApproved2 { get; set; }
        public int POApproved3 { get; set; }
        public int POHold { get; set; }
        public int PartialReceived { get; set; }
        public int TGoodReceived { get; set; }
        public decimal TotalPRValue { get; set; }
        public decimal TotalPOValue { get; set; }
        public decimal TotalGoodReceiveValue { get; set; }



    }

    public class Common_LineDevice : CoreModel
    {
        public string Name { get; set; }
        public string DeviceID { get; set; }
        public bool IsClossed { get; set; }
        public int Common_ProductionLineFK { get; set; }
    }

    public class Common_Notify : CoreModel
    {
        public int StyleID { get; set; }
        public bool IsCloseShipment { get; set; }
        public int NoOfShipment { get; set; }
        public int TShipmentDone { get; set; }
        public bool IsBOMCreate { get; set; }
        public bool IsBOMComplete { get; set; }

        public int TDraftPR { get; set; }
        public int TSubmittedPR { get; set; }
        public int THoldPR { get; set; }
        public int TCancelPR { get; set; }
        public int TPartialPR { get; set; }
        public int TCompletePR { get; set; }

        public int PApprovedPR { get; set; }
        public int SApprovedPR { get; set; }
        public int FApprovedPR { get; set; }

        public int TDraftPO { get; set; }
        public int TSubmittedPO { get; set; }
        public int THoldPO { get; set; }
        public int TCancelPO { get; set; }
        public int TPartialPO { get; set; }
        public int TCompletePO { get; set; }

        public int PApprovedPO { get; set; }
        public int SApprovedPO { get; set; }
        public int FApprovedPO { get; set; }

        public int PartialReceived { get; set; }
        public int TGoodReceived { get; set; }
    }
}

