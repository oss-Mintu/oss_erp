﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Erp.Core.Entity.Accounting
{
    public abstract class Accounting_ChartBase : CoreModel
    {
        [StringLength(150)]
        public string Name { get; set; }
        [StringLength(20)]
        public string Code { get; set; }
    }
    public class Accounting_Type 
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID { get; set; }
        public string Name { get; set; }
        public bool DrIncrease { get; set; }
        public List<Accounting_Chart1> Accounting_Chart1 { get; set; }
    }   
    public class Accounting_Chart1 : Accounting_ChartBase
    {        
        public int Accounting_TypeFK { get; set; }
        [ForeignKey("Accounting_TypeFK")]
        public Accounting_Type Accounting_Type { get; set; }
        public bool Entryable { get; set; } = true;
        public bool Addable { get; set; } = true;
    }
    public class Accounting_Chart2 : Accounting_ChartBase
    {
        public int Accounting_Chart1FK { get; set; }
        [ForeignKey("Accounting_Chart1FK")]
        public Accounting_Chart1 Accounting_Chart1 { get; set; }
        public bool Entryable { get; set; } = true;
        public bool Addable { get; set; } = true;
    }
    public class Accounting_Head : Accounting_ChartBase
    {
        public int Accounting_Chart2FK { get; set; }
        [ForeignKey("Accounting_Chart2FK")]
        public Accounting_Chart2 Accounting_Chart2 { get; set; }
        public decimal Balance { get; set; }
        public bool Editable { get; set; } = true;
        public bool Journalable { get; set; } = true;
        public int? BaseHeadId { get; set; }
        public int? HeadType { get; set; }
        public List<Accounting_JournalSlave> Accounting_JournalSlave { get; set; }

    }
}
