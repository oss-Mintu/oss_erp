﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Erp.Core.Entity.Accounting
{
    public class Accounting_CostCenter : CoreModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string Remark { get; set; }
    }
    public class Accounting_Journal : CoreModel
    {
        
        public DateTime Date { get; set; } = DateTime.Now;
        public string CID { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Remark { get; set; }
        public bool Finalized { get; set; } = false;
        public bool Approved { get; set; } = false;
        public int JournalType { get; set; }

        public int Accounting_CostCenterFK { get; set; }
        [ForeignKey("Accounting_CostCenterFK")]
        public Accounting_CostCenter Accounting_CostCenter { get; set; }


        public List<Accounting_JournalSlave> Accounting_JournalSlave { get; set; }
       // public string CID { get; set; }
    }
    public class Accounting_JournalSlave : CoreModel
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string Remark { get; set; }
        public decimal Debit { get; set; } = 0;
        public decimal Credit { get; set; } = 0;

        public decimal ForeignCurrencyAmount { get; set; } = 0;
        public decimal ConversionRateToBDT { get; set; } = 0;

        public int Accounting_JournalFK { get; set; }
        [ForeignKey("Accounting_JournalFK")]
        public Accounting_Journal Accounting_Journal { get; set; }

        public int Accounting_HeadFK { get; set; }
        [ForeignKey("Accounting_HeadFK")]
        public Accounting_Head Accounting_Head { get; set; }
    }
    
}
