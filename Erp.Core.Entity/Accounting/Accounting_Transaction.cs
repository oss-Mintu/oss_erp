﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Erp.Core.Entity.Accounting
{
    public class Accounting_Transaction : CoreModel
    {
        [DisplayName("Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }

        [DisplayName("Type")]
        [StringLength(20, ErrorMessage = "Upto 20 Chracter")]
        public string TransactionType { get; set; }

        [DisplayName("Voucher No")]
        [StringLength(20, ErrorMessage = "Upto 20 Chracter")]
        public string CID { get; set; }

        [DisplayName("To Account")]
        public int To_Acc_NameFK { get; set; }

        [DisplayName("From Account")]
        public int From_Acc_NameFK { get; set; }

        public int Accounting_CostCenterFK { get; set; }
        [ForeignKey("Accounting_CostCenterFK")]
        public Accounting_CostCenter Accounting_CostCenter { get; set; }

        [DisplayName("Cheque No")]
        [StringLength(100, ErrorMessage = "Upto 100 Chracter")]
        public string ChequeNo { get; set; }

        [DisplayName("Cheque Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime? ChequeDate { get; set; }

        [DefaultValue(0.00)]
        [DisplayName("Amount")]
        public decimal Amount { get; set; }

        [DisplayName("Description")]
        [DataType(DataType.MultilineText)]
        [StringLength(500, ErrorMessage = "Upto 500 Chracter")]
        public string Description { get; set; }

        public int Status { get; set; }

        [DefaultValue(true)]
        public bool? Editable { get; set; }
    }
}
