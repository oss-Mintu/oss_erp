﻿using Erp.Core.Entity.Common;
using Erp.Core.Entity.HRMS;
using Erp.Core.Entity.Merchandising;
using Erp.Core.Entity.Store;
using Erp.Core.Entity.User;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;


namespace Erp.Core.Entity.Procurement
{
    public class Procurement_PurchaseInvoice : CoreModel
    {
        public string CID { get; set; }
        public int OriginType { get; set; }
        public int? Common_SupplierFK { get; set; }
        [ForeignKey("Common_SupplierFK")]
        public Common_Supplier Common_Supplier { get; set; }
        public string Description { get; set; }
        public int Status { get; set; }
        public DateTime CreationDate { get; set; } = DateTime.Now;
        public string CreatedBy { get; set; }
        public DateTime InvoiceDate { get; set; }
        public DateTime ApprovedDate { get; set; }
        public int PaymentMethod { get; set; }
        public string CancellationRemaks { get; set; }
        public int? ClosedByUserFK { get; set; }
        [ForeignKey("ClosedByUserFK")]
        public User_User User_UserClosedBy { get; set; }
        public double TotalInvoiceValue { get; set; }
        public DateTime InvoiceMatureDate { get; set; }
        public string SupplierInvoiceNo { get; set; }


        public List<Procurement_PurchaseInvoiceSlave> Procurement_PurchaseInvoiceSlave { get; set; }
    }

    public class Procurement_PurchaseInvoiceSlave : CoreModel
    {
        public double InvoiceQuantity { get; set; }

        public double InvoicePrice { get; set; }

        public string Description { get; set; }

        public int Procurement_PurchaseInvoiceFK { get; set; }
        [ForeignKey("Procurement_PurchaseInvoiceFK")]
        public Procurement_PurchaseInvoice Procurement_PurchaseInvoice { get; set; }

        public int? Procurement_PurchaseOrderSlaveID { get; set; }
        public int? Store_StockInFK { get; set; }
        [ForeignKey("Store_StockInFK")]

        public Store_StockIn Store_StockIn { get; set; }

        public int Common_RawItemFK { get; set; }
        [ForeignKey("Common_RawItemFK")]

        public Common_RawItem Common_RawItem { get; set; }
        public int? Merchandising_StyleID { get; set; }
        public int? Merchandising_StyleSlaveID { get; set; }


        public int? Merchandising_BOFID { get; set; }
        public string StrChallanNo { get; set; }



    }

}