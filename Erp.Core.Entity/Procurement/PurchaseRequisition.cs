﻿using Erp.Core.Entity.Common;
using Erp.Core.Entity.HRMS;
using Erp.Core.Entity.Merchandising;
using Erp.Core.Entity.User;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;


namespace Erp.Core.Entity.Procurement
{
    public class Procurement_PurchaseRequisition : CoreModel
    {
        public string CID { get; set; }
        public int PRType { get; set; }
        public int OriginType { get; set; }


        public string Description { get; set; }
        public int Status { get; set; }
        public DateTime CreationDate { get; set; } = DateTime.Now;


        public string CancellationRemaks { get; set; }
        public int? ClosedByUserFK { get; set; }
        [ForeignKey("ClosedByUserFK")]
        public User_User User_UserClosedBy { get; set; }



        public List<Procurement_PurchaseRequisitionSlave> Procurement_PurchaseRequisitionSlave { get; set; }
    }

    public class Procurement_PurchaseRequisitionSlave : CoreModel
    {
        public int Procurement_PurchaseRequisitionFK { get; set; }
        [ForeignKey("Procurement_PurchaseRequisitionFK")]
        public Procurement_PurchaseRequisition Procurement_PurchaseRequisition { get; set; }
        //public int? HRMS_EmployeeFK { get; set; }
        //[ForeignKey("HRMS_EmployeeFK")]
        //public HRMS_Employee HRMS_Employee { get; set; }
        [Required(ErrorMessage = "Requisition Date is Required")]
        public DateTime RequiredDate { get; set; } = DateTime.Now;
        [Required(ErrorMessage = "Item is Required")]
        public int Common_RawItemFK { get; set; }
        [ForeignKey("Common_RawItemFK")]
        public Common_RawItem Common_RawItem { get; set; }

        public int? Merchandising_StyleID { get; set; }
        public int? Merchandising_StyleSlaveFK { get; set; }
        [ForeignKey("Merchandising_StyleSlaveFK")]
        public Merchandising_StyleSlave Merchandising_StyleSlave { get; set; }

        public int? Merchandising_BOFFK { get; set; }
        [ForeignKey("Merchandising_BOFFK")]
        public Merchandising_BOF Merchandising_BOF { get; set; }

        public string Description { get; set; }

        public double RequisitionQuantity { get; set; }

        public bool InspectionRequired { get; set; } = false;
        public string SupplierNames { get; set; }
        
    }
}