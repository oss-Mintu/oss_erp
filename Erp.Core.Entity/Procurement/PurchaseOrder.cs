﻿using Erp.Core.Entity.Common;
using Erp.Core.Entity.HRMS;
using Erp.Core.Entity.Merchandising;
using Erp.Core.Entity.User;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;


namespace Erp.Core.Entity.Procurement
{
    public class Procurement_PurchaseOrder : CoreModel
    {
        public string CID { get; set; }
        public int OriginType { get; set; }
        public int Common_SupplierFK { get; set; }
        [ForeignKey("Common_SupplierFK")]
        public Common_Supplier Common_Supplier { get; set; }

        //public int? HRMS_EmployeeFK { get; set; }
        //[ForeignKey("HRMS_EmployeeFK")]
        //public HRMS_Employee HRMS_Employee { get; set; }

        public int Common_CurrencyFK { get; set; }
        [ForeignKey("Common_CurrencyFK")]
        public Common_Currency Common_Currency { get; set; }
        public string TermsAndCondition { get; set; }

        public string Description { get; set; }
        public int Status { get; set; }
        public DateTime CreationDate { get; set; } = DateTime.Now;
        public string DeliveryAddress { get; set; }

        public string CreatedBy { get; set; }


        public DateTime OrderDate { get; set; }

        public DateTime? DeliveryDate { get; set; }

        public DateTime? ApprovedDate { get; set; }

        public int RevisionNo { get; set; }
        public int POPaymentMethod { get; set; }
        public int POType { get; set; }

        public string CancellationRemaks { get; set; }
        public int? ClosedByUserFK { get; set; }
        [ForeignKey("ClosedByUserFK")]
        public User_User User_UserClosedBy { get; set; }

        public double TotalPOValue { get; set; }
        //This Column for L/C
        public int? Commercial_BBLCFk { get; set; } = 0;

        public List<Procurement_PurchaseOrderSlave> Procurement_PurchaseOrderSlave { get; set; }
    }

    public class Procurement_PurchaseOrderSlave : CoreModel
    {
        public double PurchaseQuantity { get; set; }

        public double PurchasingPrice { get; set; }

        public string Description { get; set; }
        public bool InspectionRequired { get; set; }

        public int Procurement_PurchaseOrderFK { get; set; }
        [ForeignKey("Procurement_PurchaseOrderFK")]
        public Procurement_PurchaseOrder Procurement_PurchaseOrder { get; set; }

        public int? Procurement_PurchaseRequisitionSlaveFK { get; set; }
        [ForeignKey("Procurement_PurchaseRequisitionSlaveFK")]

        public Procurement_PurchaseRequisitionSlave Procurement_PurchaseRequisitionSlave { get; set; }

        public int Common_RawItemFK { get; set; }
        [ForeignKey("Common_RawItemFK")]

        public Common_RawItem Common_RawItem { get; set; }

        public int? Merchandising_StyleID { get; set; }
        public int? Merchandising_StyleSlaveFK { get; set; }
        [ForeignKey("Merchandising_StyleSlaveFK")]
        public Merchandising_StyleSlave Merchandising_StyleSlave { get; set; }

        public int? Merchandising_BOFFK { get; set; }
        [ForeignKey("Merchandising_BOFFK")]
        public Merchandising_BOF Merchandising_BOF { get; set; }
    }

}