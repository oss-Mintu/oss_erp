﻿using Erp.Core.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Erp.Core.Services.Commercial
{ 
    public class Commercial_UD : CoreModel
    {
        public string CID { set; get; }
        public string Ref { set; get; }
        public DateTime Date { set; get; }
        //public string Declaration { set; get; }
        public decimal LimitDeclaration { set; get; }
        //Not need 
        public int Common_BuyerFk { get; set; }

        //Need column also.
        public int Accounting_HeadFk { get; set; }
        public bool IsClose { get; set; } = false;
        public string ScanedFile { get; set; }
    }
   
    public class Commercial_ECI : CoreModel 
    {
        public string CID { get; set; }
        public string ECINo { get; set; }  
        public DateTime ECIDate { get; set; }
        public int Common_ECITypeFK { get; set; } 
        public int Common_BuyerFK { get; set; }

        public int Common_BuyerBankFK { get; set; }
        public int Common_LienBankFK { get; set; }
        public int Common_CompanyBankFK { get; set; }

        public int Common_CurrencyFK { get; set; }        
        public decimal TotalValue { get; set; }
        public decimal Tolerance { get; set; }
        public DateTime ShipmentDate { get; set; }
        public DateTime ExpiryDate { get; set; }
        public string Destination { get; set; }
        public string ScanedFile { get; set; }       
        public int Commercial_UDFk { get; set; } = 0;
        public int BaseHeadId { get; set; }


        //Not Need
        // public int Common_BuyerPOFK { get; set; }
        //public decimal TotalValuePO { get; set; }

        //Need 
        public int Common_BuyerNotifyPartyFk { get; set; } // to notify buyer about L/C
        public bool IsClose { get; set; } = false;

    }
   public class Commercial_ECIValueAmendment : CoreModel
    {
        public decimal ValueAmendment { get; set; }
        public int Commercial_ECIFk { get; set; }

    }
    public class Commercial_BBLCValueAmendment : CoreModel
    {
        public decimal ValueAmendment { get; set; }
        public int Commercial_BBLCFk { get; set; }

    }

    public class Commercial_BBLC : CoreModel
    {
        public string CID { get; set; }
        public string BBNo { get; set; }
        public DateTime LCDate { get; set; }       
        public decimal BBValue { get; set; }
        public int Commercial_UDFk { get; set; } = 0;

        public int Common_SupplierFK { get; set; }
        public int Common_LcTypeFK { get; set; }
        //public int Common_UnitFK { get; set; }
        public string ScanedFile { get; set; }
        public int Common_CurrencyFK { get; set; }
        public decimal Tolerance { get; set; }
        
        // Need those column 
        //public decimal Amendment { get; set; }
        //public int? Commercial_ECIFk { get; set; }
        public int Commercial_LCOreginFK { get; set; }
        public string Status { get; set; } = "Running"; //Add due to requirement--FD 
        public bool IsApproved { get; set; } = false;//Add due to requirement--FD
        public bool IsClose { get; set; } = false;
        public int BaseHeadId { get; set; }

        /// <summary> Not Need Those Column
        //public string Orgin { get; set; }
        //public string Payable { get; set; }        
        //public string Description { get; set; }   
        //public int Common_POFK { get; set; }
        //public decimal TotalValuePO { get; set; } 
        /// </summary>

        // Why this Column
        //public int Common_RawItemFK { get; set; }
    }
    public class Commercial_BBLCPaymentInformation : CoreModel
    {
        [DisplayName("B2B L/C No")]
        public int Commercial_BBLCFk { get; set; }

        [DisplayName("Document Payment")]
        public decimal B2bLCPayment { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Payment Date")]
        public DateTime Date { get; set; }

        public bool IsApproved { get; set; }


    }

}
