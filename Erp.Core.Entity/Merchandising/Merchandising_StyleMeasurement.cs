﻿using Erp.Core.Entity.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Erp.Core.Entity.Merchandising
{
    public class Merchandising_StyleMeasurement : CoreModel
    {
        public int Merchandising_StyleFK { get; set; }
        [ForeignKey("Merchandising_StyleFK")]
        public Merchandising_Style Merchandising_Style { get; set; }

        public int Common_ColorFK { get; set; }
        [ForeignKey("Common_ColorFK")]
        public Common_Color Common_Color { get; set; }

        public int Common_SizeFK { get; set; }
        [ForeignKey("Common_SizeFK")]
        public Common_Size Common_Size { get; set; }
    }
}
