﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Erp.Core.Entity.Merchandising
{
    public class Merchandising_StyleShipmentRatio : CoreModel
    {
        public int Merchandising_StyleShipmentScheduleFK { get; set; }
        [ForeignKey("Merchandising_StyleShipmentScheduleFK")]
        public Merchandising_StyleShipmentSchedule Merchandising_StyleShipmentSchedule { get; set; }

        public int? Merchandising_StyleMeasurementFK { get; set; }
        [ForeignKey("Merchandising_StyleMeasurementFK")]
        public Merchandising_StyleMeasurement Merchandising_StyleMeasurement { get; set; }

        public int? Merchandising_StyleSetPackFK { get; set; }

        public int Quantity { get; set; }
        public int Ratio { get; set; }
    }
}
