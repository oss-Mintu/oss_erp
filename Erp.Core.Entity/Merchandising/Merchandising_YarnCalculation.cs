﻿using Erp.Core.Entity.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Erp.Core.Entity.Merchandising
{
    public class Merchandising_YarnCalculation : CoreModel
    {        
        public string ReferenceNo { get; set; }       
        public int Merchandising_StyleFk { get; set; }      
        public int Merchandising_YarnTypeFk { get; set; }
        [MaxLength(40, ErrorMessage = "Upto 40 Chracter")]
        public string Combo { get; set; }
        //[MaxLength(40, ErrorMessage = "Upto 40 Chracter")]
        ///public string Color { get; set; }      
        [MaxLength(100, ErrorMessage = "Upto 100 Chracter")]
        public string Fabrication { get; set; }
        
        public int Merchandising_StyleSlaveFK_Fabric { get; set; }
        //[ForeignKey("Common_ColorFK")]
        //public Common_Color Common_Color { get; set; }
        public int GSM { get; set; }        
        public int Raw_ItemFK { get; set; }
        public string FinishDIA { get; set; }
        public decimal OrderQuantityPCS { get; set; }
        [Column(TypeName = "decimal(18,5)")]
        public decimal Consumption { get; set; }
        [Column(TypeName = "decimal(18,5)")]
        public decimal ProcessLoss { get; set; }
        [Column(TypeName = "decimal(18,5)")]
        public decimal Lycra { get; set; }
        //[DisplayName("Supplier")]
        //public int? Common_SupplierFK { get; set; }        
        public int Common_CurrencyFK { get; set; }       
        public int Common_UnitFK { get; set; }       
        [Column(TypeName = "decimal(18,5)")]
        public decimal Price { get; set; }
        public bool IsApprove { get; set; } = false;
        public int Common_ColorFK { get; set; }

        //public bool IsNeedExtraWashing { get; set; } = false;

    }
}