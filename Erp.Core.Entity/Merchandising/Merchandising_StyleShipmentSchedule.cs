﻿using Erp.Core.Entity.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Erp.Core.Entity.Merchandising
{
    public class Merchandising_StyleShipmentSchedule : CoreModel
    {
        public int Merchandising_StyleFK { get; set; }
        [ForeignKey("Merchandising_StyleFK")]
        public Merchandising_Style Merchandising_Style { get; set; }

        public int Common_CountryFK { get; set; }
        [ForeignKey("Common_CountryFK")]
        public Common_Country Common_Country { get; set; }

        public int Common_CountryPortFK { get; set; }

        public string DestNo { get; set; }

        public DateTime ShipmentDate { get; set; }
        public int Quantity { get; set; }

        //public List<Shipment_ShipmentInstruction> Shipment_ShipmentInstruction { get; set; }
    }
}
