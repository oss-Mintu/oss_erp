﻿using Erp.Core.Entity.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Erp.Core.Entity.Merchandising
{
    public class Merchandising_CBSSlave : CoreModel
    {
        public int Merchandising_CBSFK { get; set; }
        [ForeignKey("Merchandising_CBSFK")]
        public Merchandising_CBS Merchandising_CBS { get; set; }
        public int? Common_RawItemFK { get; set; }
        [ForeignKey("Common_RawItemFK")]
        public Common_RawItem Common_RawItem { get; set; }
        public int? Common_CurrencyFK { get; set; }
        [ForeignKey("Common_CurrencyFK")]
        public Common_Currency Common_Currency { get; set; }
        public decimal ConRequiredQty { get; set; }
        [Range(0.0, Double.MaxValue)]
        [Column(TypeName = "decimal(18,5)")]
        public decimal Consumption { get; set; }
        [Range(0.0, Double.MaxValue)]
        public decimal RequiredQty { get; set; }
        [Range(0.0, Double.MaxValue)]
        [Column(TypeName = "decimal(18,5)")]
        public decimal UnitPrice { get; set; }
        [Range(0.0, Double.MaxValue)]
        [Column(TypeName = "decimal(18,5)")]
        public decimal TotalPrice { get; set; }
        public decimal GSM { get; set; }
        public int? Common_ColorFK { get; set; }
        public int? SelfReferenceFK { get; set; }
    }
}
