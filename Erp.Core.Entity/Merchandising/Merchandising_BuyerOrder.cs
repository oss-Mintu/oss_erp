﻿using Erp.Core.Entity.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Erp.Core.Entity.Merchandising
{
    public class Merchandising_BuyerOrder : CoreModel
    {
        [Required(ErrorMessage = "Preferred Buyer is Required")]
        public int Common_BuyerFK { get; set; }
        [ForeignKey("Common_BuyerFK")]
        public Common_Buyer Common_Buyer { get; set; }

        public int Common_AgentFK { get; set; }
        public int Common_InspectionAgentFK { get; set; }

        public string CID { get; set; }
        public string BuyerPO { get; set; }
        public DateTime OrderDate { get; set; }
        public string Season { get; set; }
        public string Agent { get; set; }
        public string InspectionAgent { get; set; }
        public decimal Commission { get; set; }
        [Range(0.0, Double.MaxValue)]
        [Column(TypeName = "decimal(18,5)")]
        public decimal BuyerOrderPOValue { get; set; }
        public bool IsComplete { get; set; }
        // This column is for L/C
        public int Commercial_ECIFk { set; get; } = 0;
    }
}
