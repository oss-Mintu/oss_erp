﻿using Erp.Core.Entity.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Erp.Core.Entity.Merchandising
{
    public class Merchandising_Style : CoreModel
    {
        public int Merchandising_BuyerOrderFK { get; set; }
        [ForeignKey("Merchandising_BuyerOrderFK")]
        public Merchandising_BuyerOrder Merchandising_BuyerOrder { get; set; }

        public int Common_FinishItemFK { get; set; }
        [ForeignKey("Common_FinishItemFK")]
        public Common_FinishItem Common_FinishItem { get; set; }

        public int Common_CurrencyFK { get; set; }

        public string CID { get; set; }
        public DateTime FirstMove { get; set; }
        public DateTime UpdateDate { get; set; }
        
        public string Class { get; set; }
        public string Fabrication { get; set; }
        public string StyleName { get; set; }

        public string ReferenceNo { get; set; }
        public string Kimball { get; set; }

        public int SetQuantity { get; set; }
        public int PackPieceQty { get; set; }
        public int PackQty { get; set; }
        public int PieceQty { get; set; }
        public int PackInCTN { get; set; }
        [Range(0.0, Double.MaxValue)]
        [Column(TypeName = "decimal(18,5)")]
        public decimal UnitPrice { get; set; }
        [Range(0.0, Double.MaxValue)]
        [Column(TypeName = "decimal(18,5)")]
        public decimal PackPrice { get; set; }
        [Range(0.0, Double.MaxValue)]
        [Column(TypeName = "decimal(18,5)")]
        public decimal MakingCost { get; set; }
        public decimal OverheadRate { get; set; }
        public string Description { get; set; }
        public string POChangeHistory { get; set; }
        [DisplayName("BOM Complete")]
        public bool IsComplete { get; set; }
        [DisplayName("Shipment Complete")]
        public bool IsShipmentComplete { get; set; } = false;

    }
}
