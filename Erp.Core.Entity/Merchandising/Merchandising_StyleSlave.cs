﻿using Erp.Core.Entity.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Erp.Core.Entity.Merchandising
{
    public class Merchandising_StyleSlave : CoreModel
    {
        public int Merchandising_StyleFK { get; set; }
        [ForeignKey("Merchandising_StyleFK")]
        public Merchandising_Style Merchandising_Style { get; set; }
        public int? Merchandising_CBSSlaveFK { get; set; }
        [ForeignKey("Merchandising_CBSSlaveFK")]
        public Merchandising_CBSSlave Merchandising_CBSSlave { get; set; }
        public int? Common_RawItemFK { get; set; }
        [ForeignKey("Common_RawItemFK")]
        public Common_RawItem Common_RawItem { get; set; }
        public int? Common_CurrencyFK { get; set; }
        [ForeignKey("Common_CurrencyFK")]
        public Common_Currency Common_Currency { get; set; }
        public decimal ConRequiredQty { get; set; }
        public decimal GSM { get; set; }
        public int? Common_ColorFK { get; set; }
        [Range(0.0, Double.MaxValue)]
        [Column(TypeName = "decimal(18,5)")]
        public decimal Consumption { get; set; }
        public decimal Tolerance { get; set; }
        public decimal RequiredQty { get; set; }
        [Range(0.0, Double.MaxValue)]
        [Column(TypeName = "decimal(18,5)")]
        public decimal UnitPrice { get; set; }
        [Range(0.0, Double.MaxValue)]
        [Column(TypeName = "decimal(18,5)")]
        public decimal TotalPrice { get; set; }
        public bool IsFabric { get; set; }
        public bool IsLocked { get; set; }
        public int? SelfReferenceFK { get; set; }

        public int? Common_SizeFk { get; set; }
    }
}
