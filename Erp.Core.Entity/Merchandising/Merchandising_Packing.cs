﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Erp.Core.Entity.Merchandising
{
    public class Merchandising_Packing : CoreModel
    {
        public int Merchandising_StyleShipmentScheduleFK { get; set; }
        public int Merchandising_StyleShipmentRatioFK { get; set; }
        public int Merchandising_StyleFK { get; set; }
        public decimal CartonRetio { get; set; }
        public decimal GrossWeight { get; set; } // product and Carton Weight.
        public decimal NetWeight { get; set; } // Only Product Weight.
        public decimal CartonMeasurement { get; set; } //CBM.


    }
}
