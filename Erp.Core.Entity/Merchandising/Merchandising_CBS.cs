﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Erp.Core.Entity.Merchandising
{
    public class Merchandising_CBS : CoreModel
    {
        public string CID { get; set; }
        public int Merchandising_CBSStyleFK { get; set; }
        [ForeignKey("Merchandising_CBSStyleFK")]
        public Merchandising_CBSStyle Merchandising_CBSStyle { get; set; }
        public int Common_BuyerFK { get; set; }
        public int OrderQty { get; set; }
        public decimal OverheadRate { get; set; }
        public decimal ProfitMargin { get; set; }
        public decimal BuyingCommission { get; set; }
        public decimal DozonQty { get; set; }
        public bool IsLocked { get; set; }
        public string Reference { get; set; }
    }
}
