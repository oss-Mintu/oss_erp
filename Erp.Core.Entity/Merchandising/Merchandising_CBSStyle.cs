﻿using Erp.Core.Entity.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Erp.Core.Entity.Merchandising
{
    public class Merchandising_CBSStyle : CoreModel
    {
        public int Common_FinishItemFK { get; set; }
        [ForeignKey("Common_FinishItemFK")]
        public Common_FinishItem Common_FinishItem { get; set; }

        public string CID { get; set; }
        public DateTime FirstMove { get; set; }
        public DateTime UpdateDate { get; set; }

        public string Class { get; set; }
        public string Fabrication { get; set; }
        public string StyleName { get; set; }

        public int SetQuantity { get; set; }
        public int PackPieceQty { get; set; }
        public int PackQty { get; set; }
        public int PieceQty { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal PackPrice { get; set; }
        public string Description { get; set; }
    }
}
