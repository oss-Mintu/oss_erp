﻿using Erp.Core.Entity.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Erp.Core.Entity.Merchandising
{
    public class Merchandising_BOF : CoreModel
    {
        public int Merchandising_StyleFK { get; set; }
        [ForeignKey("Merchandising_StyleFK")]
        public Merchandising_Style Merchandising_Style { get; set; }

        public int Merchandising_YarnCalculationFK { get; set; }
        //[ForeignKey("Merchandising_YarnCalculationFK")]
        //public Merchandising_YarnCalculation Merchandising_YarnCalculation { get; set; }
        public int Merchandising_StyleSlaveFK_Fabric { get; set; }

        public int? Common_RawItemFK { get; set; }
        [ForeignKey("Common_RawItemFK")]
        public Common_RawItem Common_RawItem { get; set; }

        public int? Common_CurrencyFK { get; set; }
        public int? Common_UnitFK { get; set; }

        //[ForeignKey("Common_CurrencyFK")]
        //public Common_Currency Common_Currency { get; set; }


        [StringLength(200, ErrorMessage = "Upto 500 Chracter")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [Column(TypeName = "decimal(18,5)")]
        public decimal Consumption { get; set; }

        [Column(TypeName = "decimal(18,5)")]
        public decimal? RequiredQuantity { get; set; }


        [Column(TypeName = "decimal(18,5)")]
        public decimal Price { get; set; }

        public bool IsFabric { get; set; } = false;

        
    }
}
