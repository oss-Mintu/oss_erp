﻿using Erp.Core.Entity.Common;
using Erp.Core.Entity.User;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Erp.Core.Entity.HRMS
{
    public class HRMS_AttendanceHistory : CoreModel
    {

        public int? HRMS_EmployeeFK { get; set; }
        [ForeignKey("HRMS_EmployeeFK")]
        public virtual HRMS_Employee HRMSEmployee { get; set; }
        public DateTime Date { get; set; }

        public DateTime InTime { get; set; }

        public DateTime OutTime { get; set; }


        public int TotalTime { get; set; }

        public int Late { get; set; }
        public int OverTime { get; set; }
        public int AttendanceStatus { get; set; }
        [StringLength(50)]
        public string CardNo { get; set; }
        public int HRMS_ShiftFK { get; set; }
        [ForeignKey("HRMS_ShiftFK")]
        public virtual HRMS_Shift HRMS_Shift { get; set; }
        public int? LeavePaidType { get; set; }
        public DateTime EntryDate { get; set; }

        public DateTime UpdateDate { get; set; }


        public int? EntryByUserFK { get; set; }

        public int? UpdateByUserFK { get; set; }
        [ForeignKey("UpdateByUserFK")]
        public virtual User_User UpdateByUser { get; set; }

        public int? PayableOverTime { get; set; }

        public bool? IsEdited { get; set; }
    }

    public class HRMS_AttendanceHistoryLog : CoreModel
    {

        public int? HRMS_AttendanceHistoryFK { get; set; }
        [ForeignKey("HRMS_AttendanceHistoryFK")]
        public HRMS_AttendanceHistory HRMS_AttendanceHistory { get; set; }
        public int? HRMS_EmployeeFK { get; set; }
        public DateTime Date { get; set; }

        public DateTime InTime { get; set; }

        public DateTime OutTime { get; set; }


        public int TotalTime { get; set; }

        public int Late { get; set; }
        public int OverTime { get; set; }
        public int AttendanceStatus { get; set; }
        [StringLength(50)]
        public string CardNo { get; set; }
        public int HRMS_ShiftFK { get; set; }
        public int? LeavePaidType { get; set; }
        public DateTime EntryDate { get; set; }

        public DateTime UpdateDate { get; set; }


        public int? EntryByUserFK { get; set; }

        public int? UpdateByUserFK { get; set; }

        public int? PayableOverTime { get; set; }

        public bool? IsEdited { get; set; }
    }
    public class HRMS_AttendanceRemarks : CoreModel
    {
        public string AttendanceRemark { get; set; }
    }
    public class HRMS_CheckInOuts
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }
        [StringLength(50)]
        public string UserId { get; set; }
        public DateTime CheckTime { get; set; }

        [StringLength(50)]
        public string Checktype { get; set; }
        [StringLength(50)]
        public string VerifyCode { get; set; }
        [StringLength(50)]
        public string CardNo { get; set; }
        public int SourceId { get; set; }
        [NotMapped]
        public bool Retry { get; set; }
    }


}