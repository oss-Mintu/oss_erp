﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Erp.Core.Entity.HRMS
{
    public class HRMS_EmployeeLeaveAssign: CoreModel
    {
        public int HRMS_EmployeeFK { get; set; }
        [ForeignKey("HRMS_EmployeeFK")]
        public virtual HRMS_Employee HRMSEmployee { get; set; }

        public int HRMS_LeaveFK { get; set; }
        [ForeignKey("HRMS_LeaveFK")]
        public virtual HRMS_Leave HRMS_Leave { get; set; }

        public int Year { get; set; }

        //(optional) If Individually day setup needed
        public int? AnnualLeaveDays { get; set; }
        public int? TakenAnnualLeaveDays { get; set; }
        public int? RemainingAnnualLeaveDays
        {
            get { return AnnualLeaveDays - TakenAnnualLeaveDays; }
        }

        public int? CasualLeaveDays { get; set; }
        public int? TakenCasualLeaveDays { get; set; }
        public int? RemainingCasualLeaveDays
        {
            get { return CasualLeaveDays - TakenCasualLeaveDays; }
        }

        public int? SickLeaveDays { get; set; }
        public int? TakenSickLeaveDays { get; set; }
        public int? RemainingSickLeaveDays
        {
            get { return SickLeaveDays - TakenSickLeaveDays; }
        }
        public int? MaternityLeaveDays { get; set; }
        public int? TakenMaternityLeaveDays { get; set; }
        public int? RemainingMaternityLeaveDays
        {
            get { return MaternityLeaveDays - TakenMaternityLeaveDays; }
        }

        public int? EarnLeaveDays { get; set; }
        public int? TakenEarnLeaveDays { get; set; }
        public int? RemainingEarnLeaveDays { get { return EarnLeaveDays - TakenEarnLeaveDays; } }
    }

    public class HRMS_EmployeeEarnLeaveCalculation : CoreModel
    {
        public int HRMS_EmployeeFK { get; set; }
        [ForeignKey("HRMS_EmployeeFK")]
        public virtual HRMS_Employee HRMSEmployee { get; set; }

        public DateTime PaymentDate { get; set; }

        public int TotalMonth { get; set; }
        public int WorkingDays { get; set; }
        public int Holidays { get; set; }
        public int Offdays { get; set; }
        public int TotalLeave { get; set; }
        public int Absent { get; set; }
        public int TotalDeductionDays { get; set; }
        public int PayableWorkingDays { get; set; }
        public int TotalEarnLeave { get; set; }
        public int EnjoyableEarnLeaveDays { get; set; }
        public int PayableEarnLeave { get; set; }
        public int Stamp { get; set; }
        public decimal PayableEarnLeaveTk { get; set; }
    }

    public class HRMS_EmployeeServiceBenifit : CoreModel
    {
        public int HRMS_EmployeeFK { get; set; }
        [ForeignKey("HRMS_EmployeeFK")]
        public virtual HRMS_Employee HRMSEmployee { get; set; }

        public DateTime ResignationDate { get; set; }

        public int Year { get; set; }
        public int Month { get; set; }
        public int Day { get; set; }
        public int PayableServiceBenifitDays { get; set; }

        public decimal PresentSalary { get; set; }
        public decimal PresentBasic { get; set; }
        public decimal PresentBasicPerDay { get; set; }
        public decimal TotalAmount { get; set; }
        public int Stamp { get; set; }
        public decimal PayableAmount { get; set; }
    }
}
