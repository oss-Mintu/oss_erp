﻿using Erp.Core.Entity.User;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Erp.Core.Entity.HRMS
{
    public class HRMS_Shift : CoreModel
    {
        public string ShiftName { get; set; }
        public TimeSpan? StartTime { get; set; }
        public TimeSpan? EndTime { get; set; }
        public int BreakTime { get; set; }
        public string ShiftRemarks { get; set; }
    }

    public class HRMS_ShiftAssign : CoreModel
    {
        
        public int HRMS_ShiftFK { get; set; }
        [ForeignKey("HRMS_ShiftFK")]
        public HRMS_Shift HRMS_Shift { get; set; }


        public int HRMS_EmployeeFK { get; set; }
        [ForeignKey("HRMS_EmployeeFK")]
        public HRMS_Employee HRMS_Employee { get; set; }


        public DateTime AssignedDate { get; set; }
        public Guid UID { get; set; }
        
    }
}