﻿using Erp.Core.Entity.Common;
using Erp.Core.Entity.User;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Erp.Core.Entity.HRMS
{
    public class HRMS_Employee : CoreModel
    {
        
        public string CardNo { get; set; }
        public string Name { get; set; }
        public string NameBN { get; set; }
        public string FatherName { get; set; }
        public string MotherName { get; set; }
        public DateTime DOB { get; set; }
        public string Gender { get; set; }


        public int? HRMS_DesignationFK { get; set; }
        [ForeignKey("HRMS_DesignationFK")]
        public HRMS_Designation HRMS_Designation { get; set; }

        public int? User_DepartmentFK { get; set; }
        [ForeignKey("User_DepartmentFK")]
        public User_Department User_Department { get; set; }

        public int? HRMS_SectionFK { get; set; }
        [ForeignKey("HRMS_SectionFK")]
        public HRMS_Section HRMS_Section { get; set; }


        public int? Common_CountryFK { get; set; }
        [ForeignKey("Common_CountryFK")]
        public Common_Country Common_Country { get; set; }

        public int? HRMS_PostOfficeFK { get; set; }
        [ForeignKey("HRMS_PostOfficeFK")]
        public HRMS_PostOffice HRMS_PostOffice { get; set; }

        public int? Common_ProductionLineFK { get; set; }
        [ForeignKey("Common_ProductionLineFK")]
        public Common_ProductionLine Common_ProductionLine { get; set; }

        public int BloodGroup { get; set; }
        public DateTime JoiningDate { get; set; }
        public string Religion { get; set; }


        public string AltPersonName { get; set; }
        public string MobileNo { get; set; }
        public string AltMobileNo { get; set; }
        public string AltPersonContactNo { get; set; }
        public string AltPersonAddress { get; set; }
        public string EmergencyContactNo { get; set; }
        public string OfficeContact { get; set; }
        public string LandPhone { get; set; }
        public string Email { get; set; }

        public int EmployementType { get; set; }

        public string NIDNo { get; set; }

        public string PassportNo { get; set; }
        public string PresentAddress { get; set; }
        public string PermanentAddress { get; set; }

        public string MaritalStatus { get; set; }

        //public string Photo { get; set; }
        

        public int PresentStatus { get; set; }

        public int StaffType { get; set; }

        public string OvertimeEligibility { get; set; }

        public string Grade { get; set; }

        public DateTime? QuitDate { get; set; }
        public string EmployeeIdentity { get; set; }

        public bool IsLeaveAssigned { get; set; }
        public bool IsWeekendAssigned { get; set; }

        public int? LineRoleId { get; set; }
        public int? ReportingDesignationId { get; set; }

        public string ImagePath { get; set; }
    }

    public class HRMS_EmployeeDependent : CoreModel
    {       
        public int HRMS_EmployeeFK { get; set; }
        [ForeignKey("HRMS_EmployeeFK")]
        public virtual HRMS_Employee HRMSEmployee { get; set; }
        public bool IsSpouse { get; set; }
        public string Name { get; set; }
        public string ContactNo { get; set; }
        public int BloodGroup { get; set; }
        public DateTime DOB { get; set; }
    }
    

    public class HRMS_EmployeeEducation : CoreModel
    {
        public int HRMS_EducationalDegreeFK { get; set; }
        [ForeignKey("HRMS_EducationalDegreeFK")]
        public virtual HRMS_EducationalDegree HRMS_EducationalDegree { get; set; }
        public string Institute { get; set; }
        public string GroupOrSubject { get; set; }
        public string PassingYear { get; set; }
        public string GradingType { get; set; }
        public string Result { get; set; }

        public int HRMS_EmployeeFK { get; set; }
        [ForeignKey("HRMS_EmployeeFK")]
        public virtual HRMS_Employee HRMSEmployee { get; set; }
    }

    public class HRMS_EmployeeTraining : CoreModel
    {
        public string Institute { get; set; }
        public int CountryID { get; set; }
        public string CountryName { get; set; }
        public DateTime CourseStartDate { get; set; }
        public DateTime CourseEndDate { get; set; }
        public string CourseDuration { get; set; }

        public int HRMS_EmployeeFK { get; set; }
        [ForeignKey("HRMS_EmployeeFK")]
        public virtual HRMS_Employee HRMSEmployee { get; set; }
        public string CourseName { get; set; }
    }

    public class HRMS_EmployeeExperience : CoreModel
    {
        public string Designation { get; set; }
        public string Department { get; set; }
        public string Responsibility { get; set; }
        public DateTime JobStartDate { get; set; }
        public DateTime JobEndDate { get; set; }
        public string Duration { get; set; }
        public string Location { get; set; }
        public string Comments { get; set; }

        public int HRMS_EmployeeFK { get; set; }
        [ForeignKey("HRMS_EmployeeFK")]
        public virtual HRMS_Employee HRMSEmployee { get; set; }
        public string CompanyName { get; set; }
    }

    public class HRMS_EmployeeSkill : CoreModel
    {
       public int HRMS_EmployeeFK { get; set; }
        [ForeignKey("HRMS_EmployeeFK")]
        public virtual HRMS_Employee HRMSEmployee { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public string Name { get; set; }
    }

    public class HRMS_EmployeeReference : CoreModel
    {
        public string EmailId { get; set; }
        public string Address { get; set; }
        public string Relation { get; set; }

        public int HRMS_EmployeeFK { get; set; }
        [ForeignKey("HRMS_EmployeeFK")]
        public virtual HRMS_Employee HRMSEmployee { get; set; }
        public string RefereeName { get; set; }
        public string ContactNo { get; set; }
        public bool IsVarified { get; set; }
        public string VarifyingComments { get; set; }
    }


    public class HRMS_EmployeeNominee : CoreModel
    {
        public string Name { get; set; }
        public string FatherName { get; set; }
        public string MotherName { get; set; }
        public string Relation { get; set; }
        public string NID { get; set; }
        public string Address { get; set; }
        public string Contact { get; set; }
        public string Email { get; set; }
        public int HRMS_EmployeeFK { get; set; }
        [ForeignKey("HRMS_EmployeeFK")]
        public virtual HRMS_Employee HRMSEmployee { get; set; }
    }


    public class HRMS_EducationalDegree : CoreModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }

    public class HRMS_EmploymentHistory: CoreModel
    {

    }
}
