﻿using Erp.Core.Entity.Common;
using Erp.Core.Entity.User;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Erp.Core.Entity.HRMS
{
    public class HRMS_BusinessUnit : CoreModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Code { get; set; }
    }

    public class HRMS_Unit : CoreModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Code { get; set; }
        public int? HRMS_BusinessUnitFK { get; set; }
        [ForeignKey("HRMS_BusinessUnitFK")]
        public HRMS_BusinessUnit HRMS_BusinessUnit { get; set; }
        


    }

    public class HRMS_Section : CoreModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Code { get; set; }
        public int? HRMS_UnitFK { get; set; }
        [ForeignKey("HRMS_UnitFK")]
        public HRMS_Unit HRMS_Unit { get; set; }
        //public int? HRMS_UnitFK { get; set; }
        //[ForeignKey("HRMS_UnitFK")]
        //public HRMS_Unit HRMS_Unit { get; set; }
    }

    public class HRMS_Designation : CoreModel
    {
        public int? HRMS_CompanyHierarchyLabelFK { get; set; }
        [ForeignKey("HRMS_CompanyHierarchyLabelFK")]
        public HRMS_CompanyHierarchyLabel HRMS_CompanyHierarchyLabel { get; set; }

        public int? User_DepartmentFK { get; set; }
        [ForeignKey("User_DepartmentFK")]
        public User_Department User_Department { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public decimal AttendanceBonus { get; set; }
        public string Code { get; set; }

        public bool IsAttendanceBonusGiven { get; set; }
        public decimal NightBill { get; set; }
    }
    //others

    public class HRMS_Leave : CoreModel
    {
        public int LeaveType { get; set; }
        public int EmployeeType { get; set; }
        public int LeaveDays { get; set; }

    }

    public class HRMS_OffDay : CoreModel
    {
        public string Title { get; set; }
        public DateTime? ApplicableDate { get; set; }
        public DateTime? ReplacementDate { get; set; }
        public string Description { get; set; }
    }
    public class HRMS_EmployeeWeekendAssign : CoreModel
    {
        public int? HRMS_EmployeeFK { get; set; }
        [ForeignKey("HRMS_EmployeeFK")]
        public HRMS_Employee HRMS_Employee { get; set; }

        public virtual ICollection<HRMS_EmployeeWeekend> HRMS_EmployeeWeekends { get; set; }
    }

    public class HRMS_EmployeeWeekend
    {
        public int ID { get; set; }

        public int? HRMS_EmployeeWeekendAssignFK { get; set; }
        [ForeignKey("HRMS_EmployeeWeekendAssignFK")]
        public HRMS_EmployeeWeekendAssign HRMS_EmployeeWeekendAssign { get; set; }

        public string WeekendDay { get; set; }
    }
    public class HRMS_Holiday : CoreModel
    {
        public string HoildayName { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public int TotalDays { get; set; }
        public string Description { get; set; }

        public bool IsCompanyWeekendDay { get; set; }
    }
    public class HRMS_LeaveApplication : CoreModel
    {
        public int? HRMS_EmployeeFK { get; set; }
        [ForeignKey("HRMS_EmployeeFK")]
        public HRMS_Employee HRMS_Employee { get; set; }

        public int? ReplacementEmployeeId { get; set; }
        public string Name { get; set; }

        public int? HRMS_LeaveFK { get; set; }
        [ForeignKey("HRMS_LeaveFK")]
        public HRMS_Leave HRMS_Leave { get; set; }

        public DateTime? From { get; set; }
        public DateTime? To { get; set; }
        public int TotalDays { get; set; }
        public string Purpose { get; set; }
        public string StayDuringLeave { get; set; }
        public string LeaveApplicationRemarks { get; set; }

        public int LeavePaidType { get; set; }
        public int LeaveStatus { get; set; }
        public int LeaveDeptStatus { get; set; }

        public int? HRMS_EmployeeApprovedByFK { get; set; }
        [ForeignKey("HRMS_EmployeeApprovedByFK")]
        public HRMS_Employee HRMS_EmployeeApprovedBy { get; set; }

        public int? HRMS_EmployeeManagerFK { get; set; }
        [ForeignKey("HRMS_EmployeeManagerFK")]
        public HRMS_Employee HRMS_EmployeeManager { get; set; }

        public int? HRMS_EmployeeSectionInchargeFK { get; set; }
        [ForeignKey("HRMS_EmployeeSectionInchargeFK")]
        public HRMS_Employee HRMS_EmployeeSectionIncharge { get; set; }
    }
    public class HRMS_EarnLeave : CoreModel
    {

        public int HRMS_BusinessUnitFK { get; set; }
        [ForeignKey("HRMS_BusinessUnitFK")]
        public HRMS_BusinessUnit HRMS_BusinessUnit { get; set; }
        public DateTime? JoiningDate { get; set; }
        public DateTime? ToDate { get; set; }
        public int HRMS_EmployeeFK { get; set; }
        [ForeignKey("HRMS_EmployeeFK")]
        public HRMS_Employee HRMS_Employee { get; set; }
    }

    public class HRMS_CompanyHierarchyLabel: CoreModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }

    public class HRMS_District: CoreModel
    {
        public string Name { get; set; }

        public int? Common_CountryFK { get; set; }
        [ForeignKey("Common_CountryFK")]
        public Common_Country Common_Country { get; set; }
    }

    public class HRMS_PoliceStation: CoreModel
    {
        public string Name { get; set; }

        public int? HRMS_DistrictFK { get; set; }
        [ForeignKey("HRMS_DistrictFK")]
        public HRMS_District HRMS_District { get; set; }

        public int? Common_CountryFK { get; set; }
        [ForeignKey("Common_CountryFK")]
        public Common_Country Common_Country { get; set; }
    }

    public class HRMS_PostOffice : CoreModel
    {
        public string Name { get; set; }

        public int? HRMS_DistrictFK { get; set; }
        [ForeignKey("HRMS_DistrictFK")]
        public HRMS_District HRMS_District { get; set; }

        public int? HRMS_PoliceStationFK { get; set; }
        [ForeignKey("HRMS_PoliceStationFK")]
        public HRMS_PoliceStation HRMS_PoliceStation { get; set; }

        public int? Common_CountryFK { get; set; }
        [ForeignKey("Common_CountryFK")]
        public Common_Country Common_Country { get; set; }

        public string PostCode { get; set; }
    }

    public class HRMS_EmployeeImage: CoreModel
    {
        public int HRMS_EmployeeFK { get; set; }
        [ForeignKey("HRMS_EmployeeFK")]
        public HRMS_Employee HRMS_Employee { get; set; }

        public byte[] Image { get; set; }
    }

    public class HRMS_JobDescription: CoreModel
    {
        public int? User_DepartmentFK { get; set; }
        [ForeignKey("User_DepartmentFK")]
        public User_Department User_Department { get; set; }

        public int? HRMS_DesignationFK { get; set; }
        [ForeignKey("HRMS_DesignationFK")]
        public HRMS_Designation HRMS_Designation { get; set; }

        public string JobDescription { get; set; }
    }

}
