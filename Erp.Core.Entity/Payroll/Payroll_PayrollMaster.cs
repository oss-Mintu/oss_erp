﻿using Erp.Core.Entity.HRMS;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Erp.Core.Entity.Payroll
{
    public class Payroll_PayrollMaster : CoreModel
    {
        public string PayrollTitle { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public DateTime PaymentDate { get; set; }
        public int HRMS_BusinessUnitFK { get; set; }
        [ForeignKey("HRMS_BusinessUnitFK")]
        public virtual HRMS_BusinessUnit HRMS_BusinessUnit { get; set; }
        public int Status { get; set; }
    }
    
    public class Payroll_PayrollDetails : CoreModel
    {
        public int Payroll_PayrollMasterFk { get; set; }
        public int EmployeeFk { get; set; }
        public decimal GrossSalary { get; set; }
        public decimal BasicSalary { get; set; }
        public decimal HouseRent { get; set; }
        public decimal OtherAllowance { get; set; }
        public int TotalPresents { get; set; }
        public int HolidayOffdays { get; set; }
        public int LeaveDays { get; set; }
        public int LateDays { get; set; }
        public int AbsentDays { get; set; }
        public decimal TotalDeduction { get; set; }
        public decimal DeductedSalary { get; set; }
        public decimal AttendanceBonus { get; set; }
        public int PayableOverTimeHours { get; set; }
        public int ExtraOverTimeHours { get; set; }
        public decimal OverTimeRate { get; set; }
        public decimal PayableOverTimeAmount { get; set; }
        public decimal ExtraOverTimeAmount { get; set; }
        public decimal StampCharge { get; set; }
        public decimal FinalSalary { get; set; }
        public int EligibleHoliday { get; set; }
        public decimal HolidayRate { get; set; }
        public decimal HolidayBill { get; set; }
        public int EligibleHalfNight { get; set; }
        public int EligibleFullNight { get; set; }
        public decimal NightBill { get; set; }
        public int ApprovalStatus { get; set; }
        public int PaymentStatus { get; set; }
    }
}
