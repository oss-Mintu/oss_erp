﻿using Erp.Core.Entity.Common;
using Erp.Core.Entity.HRMS;
using Erp.Core.Entity.User;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Erp.Core.Entity.Payroll
{
    public class Payroll_EmployeePromotionalHistory: CoreModel
    {
        public int HRMS_EmployeeFK { get; set; }
        [ForeignKey("HRMS_EmployeeFK")]
        public virtual HRMS_Employee HRMS_Employee { get; set; }

        public int? HRMS_SectionFK { get; set; }
        [ForeignKey("HRMS_SectionFK")]
        public virtual HRMS_Section HRMS_Section { get; set; }

        public int? HRMS_DesignationFK { get; set; }
        [ForeignKey("HRMS_DesignationFK")]
        public virtual HRMS_Designation HRMS_Designation { get; set; }

        public int? Common_ProductionLineFK { get; set; }
        [ForeignKey("Common_ProductionLineFK")]
        public Common_ProductionLine Common_ProductionLine { get; set; }

        public string Grade { get; set; }
        public int PromotionType { get; set; } //1-Only Promotion, 2-Promotion with increament
        public DateTime PromotionDate { get; set; }
        public DateTime PreviousPromotionDate { get; set; }

        public int? Payroll_EODRecordMasterFk { get; set; }
        [ForeignKey("Payroll_EODRecordMasterFk")]
        public virtual Payroll_EODRecordMaster Payroll_EODRecordMaster { get; set; }

        public int? PreviousPayroll_EODRecordMasterFk { get; set; }
        [ForeignKey("PreviousPayroll_EODRecordMasterFk")]
        public virtual Payroll_EODRecordMaster PreviousPayroll_EODRecordMaster { get; set; }
    }
}
