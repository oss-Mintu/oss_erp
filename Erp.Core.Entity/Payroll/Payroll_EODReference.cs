﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Erp.Core.Entity.Payroll
{
    public class Payroll_EODReference : CoreModel
    {
        public string ReferenceNameEn { get; set; }
        public string ReferenceNameBn { get; set; }
        public bool IsMandatory { get; set; }
    }
    public class Payroll_EODRecordMaster : CoreModel
    {
        public int EmployeeFk { get; set; }
        public decimal GrossSalary { get; set; }
        public int SalaryAllowance { get; set; }
        public int BillEligibily { get; set; }
        public DateTime NextIncreamentDate { get; set; }
        public DateTime AffectedDate { get; set; }
        public bool IsIncreamentedSalary { get; set; }
        public DateTime? UpdateDate { get; set; }
        public int Status { get; set; } = 0; // 0=Default,1=Pending, 2=Approve, 3=Reject
        public int PreviousSalaryRecord { get; set; }
        public int SalaryStructureFk { get; set; }
        [ForeignKey("SalaryStructureFk")]
        public Payroll_SalaryStructure Payroll_SalaryStructure { get; set; }
    }
    public class Payroll_EODRecord : CoreModel
    {
        public int Payroll_EODRecordMasterFk { get; set; }
        public int EODReferenceFk { get; set; }
        public decimal Amount { get; set; }
        [ForeignKey("Payroll_EODRecordMasterFk")]
        public Payroll_EODRecordMaster Payroll_EODRecordMaster { get; set; }
    }
}
