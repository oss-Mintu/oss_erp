﻿using Erp.Core.Entity.HRMS;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Erp.Core.Entity.Payroll
{
    public class Payroll_BonusSettings : CoreModel
    {
        public int FromMonth { get; set; }
        public int ToMonth { get; set; }
        public decimal BonusRate { get; set; }
        public int IsBasicOrGross { get; set; }
    }

    public class Payroll_BonusMaster : CoreModel
    {
        public string BonusTitle { get; set; }
        public DateTime PaymentDate { get; set; }
    }

    public class Payroll_BonusDetails : CoreModel
    {
        public int Payroll_BonusMasterFk { get; set; }
        [ForeignKey("Payroll_BonusMasterFk")]
        public virtual Payroll_BonusMaster Payroll_BonusMaster { get; set; }

        public int HRMS_EmployeeFK { get; set; }
        [ForeignKey("HRMS_EmployeeFK")]
        public virtual HRMS_Employee HRMS_Employee { get; set; }

        public decimal GrossSalary { get; set; }
        public decimal BasicSalary { get; set; }
        public decimal StampCharge { get; set; }
        public DateTime JoiningDate { get; set; }
        public string Tenure { get; set; }
        public decimal BonusRate { get; set; }
        public decimal BonusAmount { get; set; }
    }
}
