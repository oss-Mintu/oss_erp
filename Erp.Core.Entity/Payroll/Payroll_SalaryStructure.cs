﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Erp.Core.Entity.Payroll
{
    public class Payroll_SalaryStructure:CoreModel
    {
        public int StructerType { get; set; }//1=White(Employee),2=Blue (Worker)
        public decimal MedicalAllowance { get; set; }
        public decimal FoodlAllowance { get; set; }
        public decimal TransportAllowance { get; set; }
        public decimal StampCharge { get; set; }
        public decimal BasicInPercent { get; set; }
        public int MonthlyWorkingDays { get; set; }
        public int DailyWorkingHour { get; set; }
        public decimal OvertimeRate { get; set; }
        public decimal HouseRentInPercent { get; set; }
    }
}
