﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Erp.Core.Entity.Production
{
    public class Prod_QCOrder : CoreModel
    {
        public int Prod_QCTypeFK { get; set; }

        public int Merchandising_StyleFK { get; set; }

        public int Priority { get; set; }
    }
}
