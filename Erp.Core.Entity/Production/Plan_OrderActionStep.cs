﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Erp.Core.Entity.Production
{
    public class Plan_OrderActionStep : CoreModel
    {
        public int Plan_OrderActionFK { get; set; }
        [ForeignKey("Plan_OrderActionFK")]
        public Plan_OrderAction Plan_OrderAction { get; set; }

        public string Name { get; set; }

        public int Priority { get; set; }

        public int DefaultRange { get; set; }

        public string SetColor { get; set; }

        public int ExtendedRange { get; set; }

        public string ExtendColor { get; set; }
    }
}
