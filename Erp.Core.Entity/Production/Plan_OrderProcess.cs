﻿using Erp.Core.Entity.Merchandising;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Erp.Core.Entity.Production
{
    public class Plan_OrderProcess : CoreModel
    {
        public int Plan_OrderActionStepFK { get; set; }
        [ForeignKey("Plan_OrderActionStepFK")]
        public Plan_OrderActionStep Plan_OrderActionStep { get; set; }

        public int Merchandising_StyleFK { get; set; }
        [ForeignKey("Merchandising_StyleFK")]
        public Merchandising_Style Merchandising_Style { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Plan Start Date")]
        public DateTime PlanStartDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Plan Finish Date")]
        public DateTime PlanFinishDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Actual Start Date")]
        public DateTime ActualStartDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Actual Finish Date")]
        public DateTime ActualFinishDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Extend Date")]
        public DateTime ExtendDate { get; set; }

        [DisplayName("Assign To")]
        public string AssignedTo { get; set; }
    }
}
