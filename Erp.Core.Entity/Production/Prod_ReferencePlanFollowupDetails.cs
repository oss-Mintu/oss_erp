﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Erp.Core.Entity.Production
{
    public class Prod_ReferencePlanFollowupDetails : CoreModel
    {
        public int Prod_ReferencePlanFollowupFK { get; set; }
        [ForeignKey("Prod_ReferencePlanFollowupFK")]
        public Prod_ReferencePlanFollowup Prod_ReferencePlanFollowup { get; set; }

        public decimal Quantity { get; set; }
    }
}
