﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Erp.Core.Entity.Production
{
    public class Prod_Reference : CoreModel
    {
        public string ReferenceNo { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime ReferenceDate { get; set; }
    }
}
