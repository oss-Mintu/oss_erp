﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Erp.Core.Entity.Production
{
    public class Prod_Remarks : CoreModel
    {
        public string Name { get; set; }

        public bool IsClose { get; set; }
    }
}
