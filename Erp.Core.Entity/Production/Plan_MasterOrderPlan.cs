﻿using Erp.Core.Entity.Merchandising;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Erp.Core.Entity.Production
{
    public class Plan_MasterOrderPlan : CoreModel
    {
        public Nullable<int> Plan_DraftOrderPlanFK { get; set; }
        [ForeignKey("Plan_DraftOrderPlanFK")]
        public Plan_DraftOrderPlan Plan_DraftOrderPlan { get; set; }

        public int Merchandising_StyleFK { get; set; }
        [ForeignKey("Merchandising_StyleFK")]
        public Merchandising_Style Merchandising_Style { get; set; }

        public int? Merchandising_StyleSetPackFK { get; set; }

        [DisplayName("Order Plan Qty"), Required(ErrorMessage = "Order plan quantity is required.")]
        public decimal PlanQty { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Shipment Date")]
        public DateTime ShipmentDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("PP Sample Approved Date")]
        public DateTime SampleApprovedDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Fabric Inhouse Date")]
        public DateTime FabricInDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Accessories Inhouse Date")]
        public DateTime AccessoriesInDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Cutting Start Date")]
        public DateTime CuttingDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Sewing Start Date")]
        public DateTime SewingDate { get; set; }
        [Range(0.0, Double.MaxValue)]
        [Column(TypeName = "decimal(18,2)")]
        public decimal ExtraRate { get; set; }
        public bool IsClosed { get; set; }
        public bool IsPaused { get; set; }
    }
}
