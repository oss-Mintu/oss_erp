﻿using Erp.Core.Entity.Common;
using Erp.Core.Entity.Merchandising;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Erp.Core.Entity.Production
{
    public class Prod_PlanAchievment : CoreModel
    {
        public DateTime AchiveDate { get; set; }

        public int Merchandising_StyleFK { get; set; }
        [ForeignKey("Merchandising_StyleFK")]
        public Merchandising_Style Merchandising_Style { get; set; }

        public int? ProductionLineFK { get; set; }
        [ForeignKey("ProductionLineFK")]
        public Common_ProductionLine Common_ProductionLine { get; set; }

        public int? MasterOrderPlanFk { get; set; }
        [ForeignKey("MasterOrderPlanFk")]
        public Plan_MasterOrderPlan MasterOrderPlan { get; set; }

        public decimal HourCapacity { get; set; }

        public decimal WorkingHour { get; set; }

        public decimal PlanAchievQty { get; set; }

        public bool IsPlanedLine { get; set; }
    }
}
