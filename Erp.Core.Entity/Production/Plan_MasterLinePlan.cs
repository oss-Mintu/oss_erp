﻿using Erp.Core.Entity.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Erp.Core.Entity.Production
{
    public class Plan_MasterLinePlan : CoreModel
    {
        public int PLan_MasterOrderPlanFK { get; set; }
        [ForeignKey("PLan_MasterOrderPlanFK")]
        public Plan_MasterOrderPlan Plan_MasterOrderPlan { get; set; }

        public int? Plan_SMVMasterLayoutFK { get; set; }
        [ForeignKey("Plan_SMVMasterLayoutFK")]
        public Plan_SMVMasterLayout Plan_SMVMasterLayout { get; set; }

        public int Common_ProductionLineFK { get; set; }
        [ForeignKey("Common_ProductionLineFK")]
        public Common_ProductionLine Common_ProductionLine { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Start Date")]
        public DateTime StartDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Finish Date")]
        public DateTime FinishDate { get; set; }

        [DisplayName("Per Hour Capacity")]
        public decimal HourTargetPlan { get; set; }

        [DisplayName("Total Operator")]
        public int NoOfOperator { get; set; }

        [DisplayName("Working Hour")]
        public decimal WorkingHour { get; set; }

        [DisplayName("Target Days")]
        public decimal TargetDays { get; set; }

        [DisplayName("Daily Target")]
        public decimal DayOutputQty { get; set; }

        [DisplayName("Total Qty")]
        public decimal TargetOutputQty { get; set; }

        [DisplayName("Line Hour")]
        public decimal LineHour { get; set; }

        [DisplayName("Remain Qty")]
        public decimal RemainQty { get; set; }

        public bool IsClosed { get; set; }

        public bool IsPaused { get; set; }
    }
}
