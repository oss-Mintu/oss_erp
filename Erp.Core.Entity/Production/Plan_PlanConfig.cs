﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Erp.Core.Entity.Production
{
    public class Plan_PlanConfig : CoreModel
    {
        public int CuttingBFShip { get; set; }

        public int FabricINBFCutting { get; set; }

        public int AccessoriesINBFCutting { get; set; }

        public int PPSampleBFCutting { get; set; }

        public int SewingAFCutting { get; set; }

        public int ReservedDate { get; set; }

        public decimal ExtraRate { get; set; }

        public bool IsRunning { get; set; }
    }
}
