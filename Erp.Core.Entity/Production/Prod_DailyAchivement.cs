﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Erp.Core.Entity.Production
{
    public class Prod_DailyAchivement : CoreModel
    {
        public int Prod_ReferenceFK { get; set; }

        public int ProductionLineFK { get; set; }

        public int Merchandising_StyleFK { get; set; }

        public int Prod_RemarksFK { get; set; }

        [Range(0.0, Double.MaxValue)]
        [Column(TypeName = "decimal(18,2)")]
        public decimal TotalAchievQty { get; set; }

        [Range(0.0, Double.MaxValue)]
        [Column(TypeName = "decimal(18,2)")]
        public decimal TargetEfficiency { get; set; }

        [Range(0.0, Double.MaxValue)]
        [Column(TypeName = "decimal(18,2)")]
        public decimal AchiveEfficiency { get; set; }

        public int TargetBalance { get; set; }
        public int? Prod_ReferencePlanFk { get; set; }

    }
}
