﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Erp.Core.Entity.Production
{
    public class Plan_SMVDraftLayout : CoreModel
    {
        public int? Merchandising_StyleSetPackFK { get; set; }

        [DisplayName("Style")]
        public int Merchandising_StyleFK { get; set; }

        [DisplayName("Total Operator")]
        public int NoOfOperator { get; set; }

        [DisplayName("Total Helper")]
        public int NoOfHelper { get; set; }

        [DisplayName("Operator SMV")]
        public decimal OperatorSMV { get; set; }

        [DisplayName("Helper SMV")]
        public decimal HelperSMV { get; set; }

        [DisplayName("Total Worker")]
        public int TotalWorker { get; set; }

        [DisplayName("SMV")]
        public decimal TotalSMV { get; set; }

        [DisplayName("Working Hour")]
        public int WorkingHour { get; set; }

        [DisplayName("Hour Target")]
        public int PerHourTarget { get; set; }

        [DisplayName("Efficiency")]
        public decimal Efficiency { get; set; }

        [Range(0.0, Double.MaxValue)]
        [Column(TypeName = "decimal(18,5)")]
        public decimal QtyRate { get; set; }
    }
}
