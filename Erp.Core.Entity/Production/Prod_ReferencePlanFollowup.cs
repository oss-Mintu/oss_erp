﻿using Erp.Core.Entity.Merchandising;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Erp.Core.Entity.Production
{
    public class Prod_ReferencePlanFollowup : CoreModel
    {
        public int? Prod_ReferencePlanFK { get; set; }
        [ForeignKey("Prod_ReferencePlanFK")]
        public Prod_ReferencePlan Prod_ReferencePlan { get; set; }

        public int? Merchandising_StyleShipmentScheduleFK { get; set; }
       // [ForeignKey("Merchandising_StyleShipmentScheduleFK")]
       // public Merchandising_StyleShipmentSchedule Merchandising_StyleShipmentSchedule { get; set; }

        public int? Merchandising_StyleShipmentRatioFK { get; set; }
        //[ForeignKey("Merchandising_StyleShipmentRatioFK")]
        //public Merchandising_StyleShipmentRatio Merchandising_StyleShipmentRatio { get; set; }

        public decimal Quantity { get; set; }
    }
}
