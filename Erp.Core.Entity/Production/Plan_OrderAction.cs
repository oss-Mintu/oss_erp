﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Erp.Core.Entity.Production
{
    public class Plan_OrderAction : CoreModel
    {
        public string Name { get; set; }

        public int ActionRange { get; set; }

        public int Priority { get; set; }

        public bool IsStop { get; set; }
    }
}
