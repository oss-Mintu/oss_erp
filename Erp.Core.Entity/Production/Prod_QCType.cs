﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Erp.Core.Entity.Production
{
    public class Prod_QCType : CoreModel
    {
        public string Name { get; set; }

        public bool IsDeprecate { get; set; }
    }
}
