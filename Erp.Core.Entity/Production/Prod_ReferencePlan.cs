﻿using Erp.Core.Entity.Common;
using Erp.Core.Entity.Merchandising;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Erp.Core.Entity.Production
{
    public class Prod_ReferencePlan : CoreModel
    {
        public int Prod_ReferenceFK { get; set; }
        [ForeignKey("Prod_ReferenceFK")]
        public Prod_Reference Prod_Reference { get; set; }

        public string CID { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime ProductionDate { get; set; }

        public int Merchandising_StyleFK { get; set; }
        [ForeignKey("Merchandising_StyleFK")]
        public Merchandising_Style Merchandising_Style { get; set; }

        public int? Merchandising_StyleSetPackFK { get; set; }

        public int? Common_ColorFK { get; set; }
        [ForeignKey("Common_ColorFK")]
        public Common_Color Common_Color { get; set; }

        public int? ProductionLineFK { get; set; }
        [ForeignKey("ProductionLineFK")]
        public Common_ProductionLine Common_ProductionLine { get; set; }

        public int? Common_LineChiefFK { get; set; }
        [ForeignKey("Common_LineChiefFK")]
        public Common_LineChief Common_LineChief { get; set; }

        public int? Common_SuperVisorFK { get; set; }
        [ForeignKey("Common_SuperVisorFK")]
        public Common_LineSuperVisor Common_LineSuperVisor { get; set; }

        public int? Plan_SMVMasterLayoutFK { get; set; }
        [ForeignKey("Plan_SMVMasterLayoutFK")]
        public Plan_SMVMasterLayout Plan_SMVMasterLayout { get; set; }

        public int LineInputQuantity { get; set; }

        public int PlanQuantity { get; set; }

        public int ExtraCutting { get; set; }

        public int FabricLayer { get; set; }

        public decimal LayerWeight { get; set; }

        public int MarkerPiece { get; set; }

        public decimal MarkerLength { get; set; }

        public decimal MarkerWidth { get; set; }

        public decimal GSMORSMV { get; set; }

        public int MachineRunning { get; set; }

        public int WorkingHour { get; set; }

        public int PresentOperator { get; set; }

        public int PresentHelper { get; set; }

        public int SectionId { get; set; }

        public int InputMan { get; set; }

        public int SizeMan { get; set; }

        public int LineMan { get; set; }

        public string CuttingNo { get; set; }

        public int DayQty { get; set; }
    }
}
