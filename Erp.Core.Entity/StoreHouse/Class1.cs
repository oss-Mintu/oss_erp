﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Erp.Core.Entity.StoreHouse
{
    public class Store_PurchaseOrderSlaveReceiving : CoreModel
    {
       
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }

        
        public decimal ReceivedQuantity { get; set; } = 0;

        [StringLength(150, ErrorMessage = "Upto 150 Chracter")]
        [Required(ErrorMessage = "Challan Number is Required")]
        public string Challan { get; set; }

        public bool IsReturn { get; set; } 

        public int? Procurement_PurchaseOrderSlaveFk { get; set; }

        public int? Mkt_POExtTransferFK { get; set; }

        [MaxLength(500, ErrorMessage = "Upto 500 Chracter")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        public int User_DepartmentFk { get; set; }


        public decimal StockLossQuantity { get; set; } = 0;

        public bool IsFinishFabric { get; set; }

        public string FabricColor { get; set; }
    }

}
