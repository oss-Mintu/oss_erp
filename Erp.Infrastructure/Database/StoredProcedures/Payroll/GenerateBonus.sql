USE [Erp]
GO
/****** Object:  StoredProcedure [dbo].[GenerateBonus]    Script Date: 2019-07-10 12:18:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[GenerateBonus] 
@BonusTitle NVARCHAR(MAX),
@PaymentDate NVARCHAR(8),
@Remarks NVARCHAR(MAX)=NULL,
@UserID INT=0

AS 
BEGIN

DECLARE @BonusMasterFk int 

	INSERT INTO  Payroll_BonusMaster (BonusTitle,PaymentDate,Remarks,[Active],[UserID],[Time]) 
			VALUES	(@BonusTitle,@PaymentDate,@Remarks,1,@UserID,GETDATE())
	SET @BonusMasterFk=(select SCOPE_IDENTITY());

	INSERT INTO Payroll_BonusDetails(
				HRMS_EmployeeFK,
				Payroll_BonusMasterFk,
				GrossSalary,
				BasicSalary,
				StampCharge,
				JoiningDate,
				Tenure,
				BonusRate,
				BonusAmount,
				Active,
				UserID,
				[Time])
		SELECT	T.ID,
				@BonusMasterFk,
				MAX(T.GrossSalary),
				SUM(T.BasicAmount),
				SUM(T.StampCharge),
				T.JoiningDate,
				(SELECT [RESULT] from [dbo].FindDateDiff(T.JoiningDate,dateadd(day,1,@PaymentDate))) AS Tenure,
				T.BonusRate,
				CASE	WHEN T.IsBasicOrGross=1 THEN (MAX(T.GrossSalary)*T.BonusRate)/100-SUM(T.StampCharge)
				WHEN T.IsBasicOrGross=2 THEN (SUM(T.BasicAmount)*T.BonusRate)/100-SUM(T.StampCharge)
				END AS BonusAmount,
				1,
				@UserID,
				GETDATE()
		
		FROM
(SELECT  
	HE.ID,
	PM.GrossSalary, 
	CASE WHEN PD.EODReferenceFk=1 THEN PD.Amount ELSE 0 END AS BasicAmount,
	CASE WHEN PD.EODReferenceFk=8 THEN PD.Amount ELSE 0 END AS StampCharge,
	HE.JoiningDate,
	(Select ISNULL(S.BonusRate,0) FROM Payroll_BonusSettings S WHERE (SELECT (([Years]*12*30)+(Months*30)+[Days]) FROM [dbo].FindDateDiff(HE.JoiningDate,@PaymentDate)) BETWEEN   ((S.FromMonth*30)) AND ((S.ToMonth*30)-1)) AS BonusRate,
	(Select ISNULL(S.IsBasicOrGross,0) FROM Payroll_BonusSettings S WHERE (SELECT (([Years]*12*30)+(Months*30)+[Days]) FROM [dbo].FindDateDiff(HE.JoiningDate,@PaymentDate)) BETWEEN   ((S.FromMonth*30)) AND ((S.ToMonth*30)-1)) AS IsBasicOrGross

FROM HRMS_Employee HE 
JOIN Payroll_EODRecordMaster PM ON HE.ID=PM.EmployeeFk 
JOIN Payroll_EODRecord PD ON PM.ID=PD.Payroll_EODRecordMasterFk
WHERE HE.Active=1 AND PM.Active=1 AND PD.Active=1 AND HE.PresentStatus=1 AND HE.JoiningDate IS NOT NULL)T
GROUP BY T.ID,T.JoiningDate,T.BonusRate,t.IsBasicOrGross

END