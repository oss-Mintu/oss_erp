USE [Erp]
GO
/****** Object:  StoredProcedure [dbo].[EffectIncreamentedSalaryToPayroll]    Script Date: 2019-07-10 12:17:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROC [dbo].[EffectIncreamentedSalaryToPayroll]
AS 
BEGIN
DECLARE @TEMP AS TABLE(
	ID INT,
	PreviousSalaryID INT
);


INSERT INTO @TEMP(ID,PreviousSalaryID) (SELECT ID,PreviousSalaryRecord FROM Payroll_EODRecordMaster WHERE IsIncreamentedSalary=1 AND [Status]= 2 AND Active=0 AND AffectedDate<=GETDATE())

UPDATE Payroll_EODRecordMaster SET Active=1 WHERE ID IN (SELECT ID FROM @TEMP)
UPDATE Payroll_EODRecord SET Active=1 WHERE Payroll_EODRecordMasterFk IN (SELECT ID FROM @TEMP)

UPDATE Payroll_EODRecordMaster SET Active=0 WHERE ID IN (SELECT PreviousSalaryID FROM @TEMP)
UPDATE Payroll_EODRecord SET Active=0 WHERE Payroll_EODRecordMasterFk IN (SELECT PreviousSalaryID FROM @TEMP)

END
