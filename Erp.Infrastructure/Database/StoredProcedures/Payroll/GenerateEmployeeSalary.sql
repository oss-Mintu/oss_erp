USE [Erp]
GO
/****** Object:  StoredProcedure [dbo].[GenerateEmployeeSalary]    Script Date: 2019-07-10 12:18:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[GenerateEmployeeSalary] (
	@PayrollTitle nvarchar(max)=NULL,
	@Paymentdate datetime=NULL,
	@Remarks nvarchar(max)='',
	@FromDate nvarchar(8)=NULL,
	@Todate nvarchar(8)=NULL,
	@User  nvarchar(20),
	@BusinessUnit int=0,
	@PayrollDetailsId int=0
	)
AS
BEGIN
	
	DECLARE @PayrollMasterFk int;
	DECLARE @EmployeeFk int=0;
	DECLARE @ApprovalStatus int=1;
	DECLARE @PaymentStatus int=1;

	--Get and Delete data from particular employee salary reprocess
	IF @PayrollDetailsId !=0
	BEGIN
		SELECT	@PayrollMasterFk=pm.ID,
			@EmployeeFk=pd.EmployeeFk,
			@FromDate=CONVERT(nvarchar,pm.FromDate,112),
			@ToDate=CONVERT(nvarchar,pm.ToDate,112)
			FROM Payroll_PayrollDetails pd JOIN Payroll_PayrollMaster pm ON pd.Payroll_PayrollMasterFk=pm.ID
			WHERE pd.Active=1 AND pm.Active=1 AND pd.ID=@PayrollDetailsId
		SET	@ApprovalStatus=5;
		SET	@PaymentStatus=2;
	END

	ELSE 
	BEGIN
		--Insert Data into Payroll Master Table
		INSERT INTO Payroll_PayrollMaster (PayrollTitle,FromDate,ToDate,HRMS_BusinessUnitFK,PaymentDate,Remarks,[Time],Active,[User],[Status])
		VALUES (@PayrollTitle,CONVERT(datetime2,@FromDate,126),CONVERT(datetime2,@Todate,126),@BusinessUnit,@Paymentdate,@Remarks,GETDATE(),1,@User,1)

		SET @PayrollMasterFk=(select SCOPE_IDENTITY());
	END

	--Insert Data into Payroll Details Table
		INSERT INTO Payroll_PayrollDetails 
		(Active,
		[User],
		[Time],
		Remarks,
		Payroll_PayrollMasterFk,
		EmployeeFk,
		GrossSalary,
		BasicSalary,
		HouseRent,
		OtherAllowance,
		TotalPresents,
		HolidayOffdays,
		LeaveDays,
		LateDays,
		AbsentDays,
		TotalDeduction,
		DeductedSalary,
		AttendanceBonus,
		OverTimeRate,
		PayableOverTimeHours,
		PayableOverTimeAmount,
		ExtraOverTimeHours,
		ExtraOverTimeAmount,
		StampCharge,
		FinalSalary,
		EligibleFullNight,
		EligibleHalfNight,
		NightBill,
		EligibleHoliday,
		HolidayRate,
		HolidayBill,
		ApprovalStatus,
		PaymentStatus)
		
		SELECT
		1,
		@User,
		GETDATE(),
		@Remarks,
		@PayrollMasterFk,
		A.HRMS_EmployeeFK,
		GrossPay,
		BasicPay,
		HouseRent,
		MedicalFoodTransportCost,
		(OK+LateDays+LeaveDays+HolidayOffdays),
		HolidayOffdays,
		LeaveDays,
		LateDays,
		AbsentDays,
		CASE	WHEN (A.PresentStatus=2 OR A.PresentStatus=3) AND A.QuitDate IS NOT NULL AND A.QuitDate BETWEEN @FromDate AND DATEADD(DAY,-1, @Todate)  THEN AbsentDays*GrossPayRate
		ELSE AbsentDays*BasicPayRate END AS TotalDeduction,

		CASE	WHEN (A.PresentStatus=2 OR A.PresentStatus=3) AND A.QuitDate IS NOT NULL AND A.QuitDate BETWEEN @FromDate AND DATEADD(DAY,-1, @Todate)  THEN (GrossPayRate*(OK+LateDays+LeaveDays+HolidayOffdays+AbsentDays))-(AbsentDays*GrossPayRate)
				WHEN A.PresentStatus=1 AND A.JoiningDate BETWEEN @FromDate AND @Todate THEN (GrossPayRate*(OK+LateDays+LeaveDays+HolidayOffdays+AbsentDays))-(AbsentDays*BasicPayRate)
		ELSE GrossPay-(AbsentDays*BasicPayRate) END AS DeductedSalary,

		CASE	WHEN (A.PresentStatus=2 OR A.PresentStatus=3) AND A.QuitDate IS NOT NULL AND CAST(A.QuitDate AS date) BETWEEN @FromDate AND DATEADD(DAY,-1, @Todate)  THEN 0
				WHEN (A.PresentStatus=2 OR A.PresentStatus=3) AND A.QuitDate IS NOT NULL AND CAST(A.QuitDate AS date) = @Todate THEN 
				CASE WHEN (AbsentDays=0 AND LeaveDays=0 AND LateDays<=2) THEN AttendanceBonus
				ELSE 0 END
		ELSE CASE WHEN (AbsentDays=0 AND LeaveDays=0 AND LateDays<=2) THEN AttendanceBonus
				ELSE 0 END
		END	AS  AttBonus,
		OverTimeRate,
		LegalOverTime,
		LegalOverTime*OverTimeRate,
		ExtraOverTime,
		ExtraOverTime*OverTimeRate,
		StampDeduction,

		CASE	WHEN A.PresentStatus=1 AND A.JoiningDate < @FromDate THEN
				CASE WHEN AbsentDays=0 AND LeaveDays=0 AND LateDays<=2 THEN (LegalOverTime*OverTimeRate)+(GrossPay-(AbsentDays*BasicPayRate)+AttendanceBonus)-StampDeduction 
				ELSE (LegalOverTime*OverTimeRate)+(GrossPay-(AbsentDays*BasicPayRate))-StampDeduction
				END
				WHEN A.PresentStatus=1 AND A.JoiningDate BETWEEN @FromDate AND @Todate  THEN
				CASE WHEN AbsentDays=0 AND LeaveDays=0 AND LateDays<=2 THEN (LegalOverTime*OverTimeRate)+((GrossPayRate*(OK+LateDays+LeaveDays+HolidayOffdays+AbsentDays))-(AbsentDays*BasicPayRate)+AttendanceBonus)-StampDeduction 
				ELSE (LegalOverTime*OverTimeRate)+((GrossPayRate*(OK+LateDays+LeaveDays+HolidayOffdays+AbsentDays))-(AbsentDays*BasicPayRate))-StampDeduction
				END
				WHEN (A.PresentStatus=2 OR A.PresentStatus=3) AND A.QuitDate IS NOT NULL AND A.QuitDate BETWEEN @FromDate AND DATEADD(DAY,-1, @Todate) THEN (LegalOverTime*OverTimeRate)+((GrossPayRate*(OK+LateDays+LeaveDays+HolidayOffdays+AbsentDays))-(AbsentDays*GrossPayRate))-StampDeduction 
				WHEN (A.PresentStatus=2 OR A.PresentStatus=3) AND A.QuitDate IS NOT NULL AND CAST(A.QuitDate AS date)= @Todate THEN
				CASE WHEN (AbsentDays=0 AND LeaveDays=0 AND LateDays<=2) THEN (LegalOverTime*OverTimeRate)+(GrossPay-(AbsentDays*BasicPayRate)+AttendanceBonus)-StampDeduction 
				ELSE (LegalOverTime*OverTimeRate)+(GrossPay-(AbsentDays*BasicPayRate))-StampDeduction
				END
				
		END AS FinalAmount,

		NumberOfFullNight,
		NumberOfHalfNight,
		(NumberOfFullNight*NightRate)+(NumberOfHalfNight*(NightRate/2)),
		NumberOfEligibleHolidayBill,
		HolidayRate,
		HolidayRate*NumberOfEligibleHolidayBill,
		@ApprovalStatus,
		@PaymentStatus
FROM 
(SELECT		TT.HRMS_EmployeeFK,
			TT.JoiningDate,
			TT.PresentStatus,
			TT.QuitDate,
			SUM(TT.LegalOverTime) AS LegalOverTime,
			SUM(TT.ExtraOverTime) AS ExtraOverTime, 
			SUM(TT.LegalOverTime)+SUM(TT.ExtraOverTime) AS ToatalOverTime,
			SUM(OK) OK,
			SUM(LateDays) LateDays,
			SUM(AbsentDays) AbsentDays,
			SUM(LeaveDays) LeaveDays,
			SUM(HolidayOffdays) HolidayOffdays,
			SUM(NumberOfHalfNight) NumberOfHalfNight ,
			SUM(NumberOfFullNight) NumberOfFullNight,
			SUM(NumberOfEligibleHolidayBill) NumberOfEligibleHolidayBill
FROM
(SELECT HAH.HRMS_EmployeeFK,
		HE.JoiningDate,
		HE.PresentStatus,
		HE.QuitDate,
			CASE	WHEN HE.PresentStatus=1 AND HE.JoiningDate BETWEEN @FromDate AND @Todate AND HAH.[Date] BETWEEN HE.JoiningDate AND @Todate
					THEN CASE WHEN HAH.PayableOverTime<=2 THEN HAH.PayableOverTime
					WHEN HAH.PayableOverTime>2 THEN 2
					ELSE 0 END
					WHEN HE.PresentStatus=1 AND HE.JoiningDate < @FromDate 
					THEN CASE WHEN HAH.PayableOverTime<=2 THEN HAH.PayableOverTime
					WHEN HAH.PayableOverTime>2 THEN 2
					ELSE 0 END
					WHEN HE.PresentStatus=2 AND HE.QuitDate IS NOT NULL AND HE.QuitDate BETWEEN @FromDate AND @Todate AND HAH.[Date] BETWEEN @FromDate AND HE.QuitDate
					THEN CASE WHEN HAH.PayableOverTime<=2 THEN HAH.PayableOverTime
					WHEN HAH.PayableOverTime>2 THEN 2
					ELSE 0 END
			END AS LegalOverTime,

			CASE	WHEN HE.PresentStatus=1 AND HE.JoiningDate BETWEEN @FromDate AND @Todate AND HAH.[Date] BETWEEN HE.JoiningDate AND @Todate
					THEN CASE WHEN HAH.PayableOverTime<=2 THEN 0
					WHEN HAH.PayableOverTime>2 THEN HAH.PayableOverTime-2
					ELSE 0 END
					WHEN HE.PresentStatus=1 AND HE.JoiningDate < @FromDate
					THEN CASE WHEN HAH.PayableOverTime<=2 THEN 0
					WHEN HAH.PayableOverTime>2 THEN HAH.PayableOverTime-2
					ELSE 0 END
					WHEN (HE.PresentStatus=2 OR HE.PresentStatus=3) AND HE.QuitDate IS NOT NULL AND HE.QuitDate BETWEEN @FromDate AND @Todate AND HAH.[Date] BETWEEN @FromDate AND HE.QuitDate
					THEN CASE WHEN HAH.PayableOverTime<=2 THEN 0
					WHEN HAH.PayableOverTime>2 THEN HAH.PayableOverTime-2
					ELSE 0 END
			END AS ExtraOverTime,

			CASE	WHEN HE.PresentStatus=1 AND HE.JoiningDate BETWEEN @FromDate AND @Todate AND HAH.[Date] BETWEEN HE.JoiningDate AND @Todate
					THEN CASE WHEN  HAH.AttendanceStatus IN (1,2) AND  (HAH.PayableOverTime>=7 AND HAH.PayableOverTime<9) THEN 1
					WHEN  HAH.AttendanceStatus IN (8,9,10,12,13,14) AND  HAH.PayableOverTime>=15 AND HAH.PayableOverTime<17 THEN 1
					ELSE 0 END
					WHEN HE.PresentStatus=1 AND HE.JoiningDate < @FromDate
					THEN CASE WHEN  HAH.AttendanceStatus IN (1,2) AND  (HAH.PayableOverTime>=7 AND HAH.PayableOverTime<9) THEN 1
					WHEN  HAH.AttendanceStatus IN (8,9,10,12,13,14) AND  HAH.PayableOverTime>=15 AND HAH.PayableOverTime<17 THEN 1
					ELSE 0 END
					WHEN (HE.PresentStatus=2 OR HE.PresentStatus=3) AND HE.QuitDate IS NOT NULL AND HE.QuitDate BETWEEN @FromDate AND @Todate AND HAH.[Date] BETWEEN @FromDate AND HE.QuitDate
					THEN CASE WHEN  HAH.AttendanceStatus IN (1,2) AND  (HAH.PayableOverTime>=7 AND HAH.PayableOverTime<9) THEN 1
					WHEN  HAH.AttendanceStatus IN (8,9,10,12,13,14) AND  HAH.PayableOverTime>=15 AND HAH.PayableOverTime<17 THEN 1
					ELSE 0 END
			END AS NumberOfHalfNight,
			
			CASE	WHEN HE.PresentStatus=1 AND HE.JoiningDate BETWEEN @FromDate AND @Todate AND HAH.[Date] BETWEEN HE.JoiningDate AND @Todate
					THEN CASE WHEN HAH.AttendanceStatus IN (1,2) AND  HAH.PayableOverTime>=9  THEN 1
					WHEN HAH.AttendanceStatus IN (8,9,10,12,13,14) AND  HAH.PayableOverTime>=17 THEN 1
					ELSE 0 END
					WHEN HE.PresentStatus=1 AND HE.JoiningDate < @FromDate
					THEN CASE WHEN HAH.AttendanceStatus IN (1,2) AND  HAH.PayableOverTime>=9  THEN 1
					WHEN HAH.AttendanceStatus IN (8,9,10,12,13,14) AND  HAH.PayableOverTime>=17 THEN 1
					ELSE 0 END
					WHEN (HE.PresentStatus=2 OR HE.PresentStatus=3) AND HE.QuitDate IS NOT NULL AND HE.QuitDate BETWEEN @FromDate AND @Todate AND HAH.[Date] BETWEEN @FromDate AND HE.QuitDate
					THEN CASE WHEN HAH.AttendanceStatus IN (1,2) AND  HAH.PayableOverTime>=9  THEN 1
					WHEN HAH.AttendanceStatus IN (8,9,10,12,13,14) AND  HAH.PayableOverTime>=17 THEN 1
					ELSE 0 END
			END AS NumberOfFullNight, 

			CASE	WHEN HE.PresentStatus=1 AND HE.JoiningDate BETWEEN @FromDate AND @Todate AND HAH.[Date] BETWEEN HE.JoiningDate AND @Todate
					THEN CASE WHEN HAH.AttendanceStatus IN (8,9,10,12,13,14) AND  HAH.PayableOverTime>=7  THEN 1
					ELSE 0 END
					WHEN HE.PresentStatus=1 AND HE.JoiningDate < @FromDate
					THEN CASE WHEN HAH.AttendanceStatus IN (8,9,10,12,13,14) AND  HAH.PayableOverTime>=7  THEN 1
					ELSE 0 END
					WHEN (HE.PresentStatus=2 OR HE.PresentStatus=3) AND HE.QuitDate IS NOT NULL AND HE.QuitDate BETWEEN @FromDate AND @Todate AND HAH.[Date] BETWEEN @FromDate AND HE.QuitDate
					THEN CASE WHEN HAH.AttendanceStatus IN (8,9,10,12,13,14) AND  HAH.PayableOverTime>=7  THEN 1
					ELSE 0 END
			END AS NumberOfEligibleHolidayBill,

			CASE	WHEN HE.PresentStatus=1 AND HE.JoiningDate BETWEEN @FromDate AND @Todate AND HAH.[Date] BETWEEN HE.JoiningDate AND @Todate
					THEN	CASE WHEN AttendanceStatus=1 THEN 1 ELSE 0 END 
					WHEN HE.PresentStatus=1 AND HE.JoiningDate <= @FromDate
					THEN	CASE WHEN AttendanceStatus=1  THEN 1 ELSE 0 END
					WHEN (HE.PresentStatus=2 OR HE.PresentStatus=3) AND HE.QuitDate IS NOT NULL AND  HE.QuitDate BETWEEN @FromDate AND @Todate AND HAH.[Date] BETWEEN @FromDate AND HE.QuitDate
					THEN	CASE WHEN AttendanceStatus=1 THEN 1 ELSE 0 END 
					END 
			AS OK,

			CASE	WHEN HE.PresentStatus=1 AND HE.JoiningDate BETWEEN @FromDate AND @Todate AND HAH.[Date] BETWEEN HE.JoiningDate AND @Todate
					THEN	CASE WHEN AttendanceStatus=2 THEN 1 ELSE 0 END
					WHEN HE.PresentStatus=1 AND HE.JoiningDate < @FromDate
					THEN	CASE WHEN AttendanceStatus=2 THEN 1 ELSE 0 END
					WHEN (HE.PresentStatus=2 OR HE.PresentStatus=3) AND HE.QuitDate IS NOT NULL AND  HE.QuitDate BETWEEN @FromDate AND @Todate AND HAH.[Date] BETWEEN @FromDate AND HE.QuitDate
					THEN	CASE WHEN AttendanceStatus=2 THEN 1 ELSE 0 END
			END  AS LateDays,

			CASE	WHEN HE.PresentStatus=1 AND HE.JoiningDate BETWEEN @FromDate AND @Todate AND HAH.[Date] BETWEEN HE.JoiningDate AND @Todate
					THEN	CASE WHEN AttendanceStatus=3  THEN 1 ELSE 0 END  
					WHEN HE.PresentStatus=1 AND HE.JoiningDate < @FromDate
					THEN	CASE WHEN AttendanceStatus=3  THEN 1 ELSE 0 END
					WHEN (HE.PresentStatus=2 OR HE.PresentStatus=3) AND HE.QuitDate IS NOT NULL AND  HE.QuitDate BETWEEN @FromDate AND @Todate AND HAH.[Date] BETWEEN @FromDate AND HE.QuitDate
					THEN	CASE WHEN AttendanceStatus=3  THEN 1 ELSE 0 END  
			END AS AbsentDays,

			CASE	WHEN HE.PresentStatus=1 AND HE.JoiningDate BETWEEN @FromDate AND @Todate AND HAH.[Date] BETWEEN HE.JoiningDate AND @Todate
					THEN	CASE WHEN AttendanceStatus=4  THEN 1 ELSE 0 END  
					WHEN HE.PresentStatus=1 AND HE.JoiningDate < @FromDate
					THEN	CASE WHEN AttendanceStatus=4  THEN 1 ELSE 0 END
					WHEN (HE.PresentStatus=2OR HE.PresentStatus=3) AND HE.QuitDate IS NOT NULL AND  HE.QuitDate BETWEEN @FromDate AND @Todate AND HAH.[Date] BETWEEN @FromDate AND HE.QuitDate
					THEN	CASE WHEN AttendanceStatus=4  THEN 1 ELSE 0 END
			END AS LeaveDays,

			CASE	WHEN HE.PresentStatus=1 AND HE.JoiningDate BETWEEN @FromDate AND @Todate AND HAH.[Date] BETWEEN HE.JoiningDate AND @Todate
					THEN	CASE WHEN AttendanceStatus IN (5,6,7,8,9,10,12,13,14) THEN 1 ELSE 0 END
					WHEN HE.PresentStatus=1 AND HE.JoiningDate < @FromDate 
					THEN	CASE WHEN AttendanceStatus IN (5,6,7,8,9,10,12,13,14) THEN 1 ELSE 0 END
					WHEN (HE.PresentStatus=2 OR HE.PresentStatus=3) AND HE.QuitDate IS NOT NULL AND  HE.QuitDate BETWEEN @FromDate AND @Todate AND HAH.[Date] BETWEEN @FromDate AND HE.QuitDate
					THEN	CASE WHEN AttendanceStatus IN (5,6,7,8,9,10,12,13,14) THEN 1 ELSE 0 END
			END AS HolidayOffdays

FROM HRMS_AttendanceHistory as HAH
JOIN HRMS_Employee HE on HAH.HRMS_EmployeeFK=HE.ID
WHERE  HE.Active=1  
	AND (CASE WHEN (@BusinessUnit IS NULL OR @BusinessUnit=0) OR (HE.HRMS_SectionFK IN (SELECT S.ID FROM HRMS_Section S
																LEFT OUTER JOIN User_Department D ON S.User_DepartmentFK=D.ID
																LEFT OUTER JOIN HRMS_Unit U ON D.HRMS_UnitFK=U.ID
																LEFT OUTER JOIN HRMS_BusinessUnit BU ON U.HRMS_BusinessUnitFK=BU.ID
																WHERE BU.ID=@BusinessUnit)) THEN 1 ELSE 0 END)=1
	AND (HAH.[Date] BETWEEN @FromDate AND @Todate)
	AND (CASE WHEN (@EmployeeFk IS NULL OR @EmployeeFk=0) OR HE.ID=@EmployeeFk THEN 1 ELSE 0 END)=1
	AND (CASE WHEN HE.QuitDate IS NULL OR (HE.QuitDate IS NOT NULL AND (HE.QuitDate BETWEEN @FromDate AND @Todate)) THEN 1 ELSE 0 END)=1
	AND HE.JoiningDate<=@Todate
	AND HE.JoiningDate IS NOT NULL) TT
GROUP BY TT.HRMS_EmployeeFK,TT.JoiningDate,TT.PresentStatus,TT.QuitDate)  A
JOIN 
(SELECT
	T.ID,
	SUM(BasicPay) BasicPay,
	MAX(GrossPay) AS GrossPay,
	SUM(HouseRent) HouseRent,
	SUM(MedicalFoodTransportCost) MedicalFoodTransportCost,
	SUM(BasicPay)/30 BasicPayRate,
	MAX(GrossPay)/30 GrossPayRate,
	SUM(OverTimeRate) OverTimeRate,
	SUM(AttendanceBonus) AttendanceBonus,
	SUM(HolidayRate) HolidayRate,
	SUM(NightRate) NightRate,
	SUM(StampDeduction) StampDeduction
	
FROM (SELECT e.ID,
	CASE WHEN 
	ER.EODReferenceFk=1 THEN Amount END AS BasicPay,
	EM.GrossSalary AS GrossPay,
	CASE WHEN 
	ER.EODReferenceFk=22  THEN Amount ELSE 0  END AS HouseRent,
	CASE WHEN 
	ER.EODReferenceFk=2 OR er.EODReferenceFk=3 OR er.EODReferenceFk=4 THEN Amount ELSE 0  END AS MedicalFoodTransportCost,
	CASE WHEN 
	ER.EODReferenceFk=19  THEN Amount ELSE 0  END AS OverTimeRate,
	CASE WHEN 
	ER.EODReferenceFk=21  THEN Amount ELSE 0  END AS AttendanceBonus,
	CASE WHEN 
	ER.EODReferenceFk=23  THEN Amount ELSE 0  END AS HolidayRate,
	CASE WHEN 
	ER.EODReferenceFk=24 THEN Amount ELSE 0  END AS NightRate,
	CASE WHEN 
	ER.EODReferenceFk=8 THEN Amount ELSE 0  END AS StampDeduction
FROM Payroll_EODRecordMaster EM JOIN HRMS_Employee E on EM.EmployeeFk=e.ID
JOIN Payroll_EODRecord ER on EM.ID=ER.Payroll_EODRecordMasterFk
WHERE EM.Active=1 
		AND ER.Active=1 
		AND (CASE WHEN (@BusinessUnit IS NULL OR @BusinessUnit=0) OR (E.HRMS_SectionFK IN (SELECT S.ID FROM HRMS_Section S
																LEFT OUTER JOIN User_Department D ON S.User_DepartmentFK=D.ID
																LEFT OUTER JOIN HRMS_Unit U ON D.HRMS_UnitFK=U.ID
																LEFT OUTER JOIN HRMS_BusinessUnit BU ON U.HRMS_BusinessUnitFK=BU.ID
																WHERE BU.ID=@BusinessUnit)) THEN 1 ELSE 0 END)=1
		AND JoiningDate<=@Todate 
		AND JoiningDate IS NOT NULL
		AND (CASE WHEN (@EmployeeFk IS NULL OR @EmployeeFk=0) OR @EmployeeFk=E.ID THEN 1 ELSE 0 END)=1
		AND (CASE WHEN E.QuitDate IS NULL OR (E.QuitDate IS NOT NULL AND (E.QuitDate BETWEEN @FromDate AND @Todate)) THEN 1 ELSE 0 END)=1
) t 
group by t.ID) B ON A.HRMS_EmployeeFK=B.ID
	
	--Delete Previous Payroll Details Data For Specific Employee
    IF @PayrollDetailsId !=0 AND  @@ROWCOUNT>0
	BEGIN
		DELETE FROM Payroll_PayrollDetails WHERE ID=@PayrollDetailsId
	END
END