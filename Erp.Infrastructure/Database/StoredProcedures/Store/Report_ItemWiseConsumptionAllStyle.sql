


		-- Select * From Store_ItemWiseConsumptionAllStyle Where ID = 1 and Merchandising_StyleFK = 25  Order by Common_RawItemFK
		-- Select * From Store_ItemWiseConsumptionAllStyle Where ID = 2 and Merchandising_StyleFK = 25  Order by Common_RawItemFK
		-- Select * From Store_ItemWiseConsumptionAllStyle Where ID = 3 and Merchandising_StyleFK = 25  Order by Common_RawItemFK
		-- Select * From Store_ItemWiseConsumptionAllStyle Where ID = 6 and Merchandising_StyleFK = 25  Order by Common_RawItemFK

		-- EXEC [Sp_ItemWiseConsumptionAllStyle]  Select * From Store_ItemWiseConsumptionAllStyle Order by Merchandising_StyleFK Where Active = 1 and id = 2

ALTER Procedure [dbo].[Sp_ItemWiseConsumptionAllStyle]

 AS
		
BEGIN 	

		DELETE FROM Store_ItemWiseConsumptionAllStyle
		INSERT INTO Store_ItemWiseConsumptionAllStyle		

Select 1 [ID], 1 [Active],1 [User], getdate() [Time], '' [Remarks], 1 [UserID],
	 
	 (Select CID + StyleName From Merchandising_Style Where ID = A.Merchandising_StyleFK) StyleName ,
	 A.Merchandising_StyleFK,
	 A.Common_RawItemFK,
	 A.Common_ColorFK,
	 A.Common_Size,
	 (A.Name +'-'+ Isnull((Select Name From Common_Color Where ID = A.Common_ColorFK),'Non Color')+'-'+ Cast(cast(A.Common_Size as int)as nvarchar)) Name, 
	 A.UnitName,
	 0 ConsumptionDz,
	 0 OrderConsumQty,
	 A.PurchasingPrice FinishQtyDz,
	 A.[Main Store Received Qty] TotalRecMain,
	Isnull(B.[Cutting Store Received Qty],0) TotalRecCutting,
	ISNULL(CASE WHEN B.[Cutting Store Received Qty] > 0 THEN 0 ELSE C.[Sewing Store Received Qty] END, 0) TotalRecSewing, 
	ISNULL(CASE WHEN C.[Sewing Store Received Qty] > 0 THEN 0 ELSE D.[Iron Store Received Qty] END, 0) TotalRecIroning, 
	ISNULL(CASE WHEN D.[Iron Store Received Qty] > 0 THEN 0 ELSE F.[Packing Store Received Qty] END, 0) TotalRecPacking,	
	
	 ISNULL(G.TotalConsumption, 0) TotalRecFinish, 

	 ISNULL(CASE
			WHEN B.[Cutting Store Received Qty] > 0 THEN (B.[Cutting Store Received Qty])
			WHEN C.[Sewing Store Received Qty] > 0 THEN ( C.[Sewing Store Received Qty])
			WHEN D.[Iron Store Received Qty] > 0 THEN (D.[Iron Store Received Qty])
			ELSE (F.[Packing Store Received Qty])
			END, 0) TotalOutMain,
	 0 TotalOutCutting,
	 0 TotalOutSewing,
	 0 TotalOutIroning,
	 0 TotalOutPacking,
	 ISNULL(CASE
				WHEN B.[Cutting Store Received Qty] > 0 THEN (B.[Cutting Store Received Qty])
				WHEN C.[Sewing Store Received Qty] > 0 THEN ( C.[Sewing Store Received Qty])
				WHEN D.[Iron Store Received Qty] > 0 THEN (D.[Iron Store Received Qty])
				ELSE (F.[Packing Store Received Qty])
				END, 0) TotalOutFinish
	--INTO Store_ItemWiseConsumptionAllStyle

from (		Select  si.Merchandising_StyleFK, si.Common_RawItemFK
				,si.Procurement_PurchaseOrderSlaveFK
				, ISNULL(ss.Common_ColorFK, 0) Common_ColorFK
				, Max(ss.GSM) Common_Size
				, i.Name 
				, Max(u.Name) UnitName, isnull( Round(Max(si.ReceivedQty),2), 0) [Main Store Received Qty]
				, Max(ms.PurchasingPrice) PurchasingPrice
				from Store_StockIn si
				join Common_RawItem i on si.Common_RawItemFK = 	i.ID 			
				Join Store_General sg on sg.ID = si.Store_GeneralFK
				join Common_Unit u on u.ID = si.CommonUnitFK 
				join Procurement_PurchaseOrderSlave ms on ms.ID = si.Procurement_PurchaseOrderSlaveFK 
				join Merchandising_StyleSlave ss on ss.ID = ms.Merchandising_StyleSlaveFK and ss.Common_RawItemFK = si.Common_RawItemFK
				where si.Active = 1 and si.Store_GeneralFK = 2 and si.Merchandising_StyleFK IN (Select ID From Merchandising_Style where Active = 1)
				group by si.Merchandising_StyleFK, si.Common_RawItemFK, Procurement_PurchaseOrderSlaveFK, i.Name, ss.Common_ColorFK, ss.Common_SizeFk		

				) A
Left Join (
				Select  si.Merchandising_StyleFK, si.Common_RawItemFK, ISNULL(ss.Common_ColorFK, 0) Common_ColorFK, Max(ss.GSM) GSM, i.Name, Round(SUM(si.ReceivedQty),2) [Cutting Store Received Qty]
				from Store_StockIn si
				join Common_RawItem i on si.Common_RawItemFK = 	i.ID			
				Join Store_General sg on sg.ID  = si.Store_GeneralFK
				join Procurement_PurchaseOrderSlave ms on ms.ID = si.Procurement_PurchaseOrderSlaveFK
				join Merchandising_StyleSlave ss on ss.ID = si.Merchandising_StyleSlaveFK and ss.Common_RawItemFK = si.Common_RawItemFK
				where si.Active = 1 and si.Store_GeneralFK = 1 and si.Merchandising_StyleFK IN (Select ID From Merchandising_Style where Active = 1)
				group by si.Merchandising_StyleFK,si.Common_RawItemFK, ss.Common_ColorFK,i.Name--, ss.Common_SizeFk	

				) B on B.Merchandising_StyleFK = A.Merchandising_StyleFK and B.Common_RawItemFK = A.Common_RawItemFK
Left Join (
				Select  si.Merchandising_StyleFK, si.Common_RawItemFK, ISNULL(ss.Common_ColorFK, 0) Common_ColorFK,  Max(ss.GSM) GSM, i.Name, Round(SUM(si.ReceivedQty),2) [Sewing Store Received Qty]
				from Store_StockIn si
				join Common_RawItem i on si.Common_RawItemFK = 	i.ID			
				Join Store_General sg on sg.ID  = si.Store_GeneralFK
				join Procurement_PurchaseOrderSlave ms on ms.ID = si.Procurement_PurchaseOrderSlaveFK
				join Merchandising_StyleSlave ss on ss.ID = ms.Merchandising_StyleSlaveFK and ss.Common_RawItemFK = si.Common_RawItemFK
				where si.Active = 1 and si.Store_GeneralFK = 3 and si.Merchandising_StyleFK IN (Select ID From Merchandising_Style where Active = 1)
				group by si.Merchandising_StyleFK,si.Common_RawItemFK, ss.Common_ColorFK,i.Name

				) C on C.Merchandising_StyleFK = A.Merchandising_StyleFK and C.Common_RawItemFK = A.Common_RawItemFK
Left Join (
			select  si.Merchandising_StyleFK, si.Common_RawItemFK, ISNULL(ss.Common_ColorFK, 0) Common_ColorFK,  Max(ss.GSM) GSM, i.Name, Round(SUM(si.ReceivedQty),2) [Iron Store Received Qty]
				from Store_StockIn si
				join Common_RawItem i on si.Common_RawItemFK = 	i.ID			
				Join Store_General sg on sg.ID  = si.Store_GeneralFK
				join Procurement_PurchaseOrderSlave ms on ms.ID = si.Procurement_PurchaseOrderSlaveFK
				join Merchandising_StyleSlave ss on ss.ID = si.Merchandising_StyleSlaveFK and ss.Common_RawItemFK = si.Common_RawItemFK
				where si.Active = 1 and si.Store_GeneralFK = 4 and si.Merchandising_StyleFK IN (Select ID From Merchandising_Style where Active = 1)
				group by si.Merchandising_StyleFK,si.Common_RawItemFK, ss.Common_ColorFK,i.Name 
			) D on D.Merchandising_StyleFK = A.Merchandising_StyleFK and D.Common_RawItemFK = A.Common_RawItemFK

Left Join (			
			Select  si.Merchandising_StyleFK, si.Common_RawItemFK, ISNULL(ss.Common_ColorFK, 0) Common_ColorFK,  Max(ss.GSM) GSM, i.Name, Round(SUM(si.ReceivedQty),2) [Packing Store Received Qty]
				from Store_StockIn si
				join Common_RawItem i on si.Common_RawItemFK = 	i.ID			
				Join Store_General sg on sg.ID  = si.Store_GeneralFK
				join Procurement_PurchaseOrderSlave ms on ms.ID = si.Procurement_PurchaseOrderSlaveFK
				join Merchandising_StyleSlave ss on ss.ID = si.Merchandising_StyleSlaveFK and ss.Common_RawItemFK = si.Common_RawItemFK
				where si.Active = 1 and si.Store_GeneralFK = 5 and si.Merchandising_StyleFK IN (Select ID From Merchandising_Style where Active = 1)
				group by si.Merchandising_StyleFK, si.Common_RawItemFK, ss.Common_ColorFK,i.Name  
			) F on F.Merchandising_StyleFK = A.Merchandising_StyleFK and F.Common_RawItemFK = A.Common_RawItemFK
			
Left Join ( Select 3 [ID], 0 [Active], 1 [User], getdate() [Time], '' [Remarks], 1 [UserID],	
				A.*, 
				ISNULL(Round(A.ConsumptionPerDz/12 * B.ReceivedQty,0),0) TotalConsumption						
		 From (
				Select ms.Merchandising_StyleFK, Common_RawItemFK, Isnull(ms.Common_ColorFK, 0) Common_ColorFK,  Max(ms.GSM) GSM, (Max(ri.Name) + '-'+ CAST(Common_RawItemFK as varchar)) Name, 
				Max(u.Name) as UnitName, Max(ms.Consumption) ConsumptionPerDz				
				from Merchandising_StyleSlave ms
				left join Merchandising_Style m on m.id = ms.Merchandising_StyleFK
				left join Common_RawItem ri on ms.Common_RawItemFK = ri.ID
				left join Common_Unit u on u.ID = ri.Common_UnitFK
				Where ms.Active =1 And Merchandising_StyleFK IN (Select ID From Merchandising_Style where Active = 1)
				Group by ms.Merchandising_StyleFK, ms.Common_RawItemFK, ms.Common_ColorFK) A   --, ms.GSM
		LEFT JOIN (Select si.Merchandising_StyleFK, si.Common_RawItemFK, SUM(si.ReceivedQty * s.PackPieceQty) ReceivedQty
					from Store_StockIn si
					join Merchandising_Style s  on si.Merchandising_StyleFK = s.ID
					where si.Active = 1 and si.Store_GeneralFK = 6 and si.Merchandising_StyleFK IN (Select ID From Merchandising_Style where Active = 1)
					group by si.Merchandising_StyleFK, si.Common_RawItemFK) B on B.Merchandising_StyleFK = A.Merchandising_StyleFK		
				
				) G on G.Merchandising_StyleFK = A.Merchandising_StyleFK And G.Common_RawItemFK = A.Common_RawItemFK

	Union All
			--- finish Item ---			
	Select 
				0 [ID], 0 [Active], 1 [User], getdate() [Time], '' [Remarks], 1 [UserID],
				(Select CID + StyleName From Merchandising_Style where ID = si.Merchandising_StyleFK) StylesName ,
				si.Merchandising_StyleFK,
				Max(si.ID) Common_RawItemFK,
				0, --ss.Common_ColorFK,
				0, --Max(ss.GSM) GSM,
				s.CID +'/'+ s.StyleName StyleName,
				finItem.Name  as [Item Name],				 
		         Round(SUM(si.ReceivedQty),2) [Finishing Item] ,
				(Round(SUM(si.ReceivedQty),2) * s.UnitPrice) as [Finish Item Value],
				0,
				0 TotalRecMain,
				 0 TotalRecCutting,
				 0 TotalRecSewing, 
				 0 TotalRecIroning ,
				 0 TotalRecPacking, 
				 0 TotalRecFinish,
				 0 TotalOutMain,

				 0 TotalOutCutting,
				 0 TotalOutSewing,
				 0 TotalOutIroning,
				 0 TotalOutPacking,
				 0 TotalOutFinish
				from Store_StockIn si
				join Merchandising_Style s  on si.Merchandising_StyleFK = s.ID	
				join Common_FinishItem finItem  on s.Common_FinishItemFK = finItem.ID			
						
				Join Store_General sg on sg.ID  = si.Store_GeneralFK				
				--join Procurement_PurchaseOrderSlave ms on ms.ID = si.Procurement_PurchaseOrderSlaveFK 
				--join Merchandising_StyleSlave ss on ss.ID = si.Merchandising_StyleSlaveFK and ss.Common_RawItemFK = si.Common_RawItemFK
				where si.Active = 1 and si.Merchandising_StyleFK IN (Select ID From Merchandising_Style where Active = 1) and si.Store_GeneralFK = 6 
				group by si.Merchandising_StyleFK, s.UnitPrice, finItem.Name,s.CID,s.StyleName --ss.Common_ColorFK,
	
	Union All
				
	Select 2 [ID], 1 [Active],1 [User], getdate() [Time], '' [Remarks], 1 [UserID],
					 
					 (Select CID + StyleName From Merchandising_Style where ID = A.Merchandising_StyleFK) StylesName ,
					 A.Merchandising_StyleFK,
					 A.Common_RawItemFK ,
					 Isnull(A.Common_ColorFK, 0) Common_ColorFK,
					 ISnull(A.GSM ,0) GSM,
					 (A.Name +'-'+ Isnull((Select Name From Common_Color Where ID = A.Common_ColorFK), 'Non Color') +'-'+ Cast(cast(A.GSM as int)as nvarchar)) Name,
					 A.UnitName,
					 0,
					 0,
					 A.PurchasingPrice,
					 A.[Main Store Received Qty] TotalRecMain,
					 Isnull(B.[Cutting Store Received Qty],0) TotalRecCutting,
					 ISNULL(CASE WHEN B.[Cutting Store Received Qty] > 0 THEN 0 ELSE C.[Sewing Store Received Qty] END, 0) TotalRecSewing, 
					 ISNULL(CASE WHEN C.[Sewing Store Received Qty] > 0 THEN 0 ELSE D.[Iron Store Received Qty] END, 0) TotalRecIroning, 
					 ISNULL(CASE WHEN D.[Iron Store Received Qty] > 0 THEN 0 ELSE F.[Packing Store Received Qty] END, 0) TotalRecPacking,	
					 ISNULL(G.TotalConsumption, 0) TotalRecFinish,
					 ISNULL(CASE
							 WHEN B.[Cutting Store Received Qty] > 0 THEN (B.[Cutting Store Received Qty])
							 WHEN C.[Sewing Store Received Qty] > 0 THEN ( C.[Sewing Store Received Qty])
							 WHEN D.[Iron Store Received Qty] > 0 THEN (D.[Iron Store Received Qty])
							 ELSE (F.[Packing Store Received Qty])
							 END, 0) TotalOutMain,
					
					 0 TotalOutCutting,
					 0 TotalOutSewing,
					 0 TotalOutIroning,
					 0 TotalOutPacking,
					 ISNULL(CASE
							 WHEN B.[Cutting Store Received Qty] > 0 THEN (B.[Cutting Store Received Qty])
							 WHEN C.[Sewing Store Received Qty] > 0 THEN ( C.[Sewing Store Received Qty])
							 WHEN D.[Iron Store Received Qty] > 0 THEN (D.[Iron Store Received Qty])
							 ELSE (F.[Packing Store Received Qty])
							 END, 0) TotalOutFinish

				from (
						Select  si.Merchandising_StyleFK, si.Common_RawItemFK, ss.Common_ColorFK, ss.GSM GSM, i.Name, Max(u.Name) UnitName, isnull( Round(SUM(si.ReceivedQty),2), 0) [Main Store Received Qty]
								, Max(ms.PurchasingPrice) PurchasingPrice
								from Store_StockIn si
								join Common_RawItem i on si.Common_RawItemFK = 	i.ID			
								Join Store_General sg on sg.ID  = si.Store_GeneralFK
								join Common_Unit u on u.ID = si.CommonUnitFK 
								join Procurement_PurchaseOrderSlave ms on ms.ID = si.Procurement_PurchaseOrderSlaveFK 
								join Merchandising_StyleSlave ss on ss.ID = si.Merchandising_StyleSlaveFK and ss.Common_RawItemFK = si.Common_RawItemFK
								where si.Active = 1 and si.Store_GeneralFK = 2 and si.Merchandising_StyleFK IN (Select ID From Merchandising_Style where Active = 1)
								group by si.Merchandising_StyleFK,si.Common_RawItemFK, ss.Common_ColorFK, i.Name , ss.GSM 	 
								) A
				Left Join (
								Select  si.Merchandising_StyleFK, si.Common_RawItemFK, ss.Common_ColorFK, Max(ss.GSM) GSM,  i.Name, Round(SUM(si.ReceivedQty),2) [Cutting Store Received Qty]
								from Store_StockIn si
								join Common_RawItem i on si.Common_RawItemFK = 	i.ID			
								Join Store_General sg on sg.ID  = si.Store_GeneralFK
								join Procurement_PurchaseOrderSlave ms on ms.ID = si.Procurement_PurchaseOrderSlaveFK 
								join Merchandising_StyleSlave ss on ss.ID = si.Merchandising_StyleSlaveFK and ss.Common_RawItemFK = si.Common_RawItemFK
								where si.Active = 1 and si.Store_GeneralFK = 1 and si.Merchandising_StyleFK IN (Select ID From Merchandising_Style where Active = 1)
								group by si.Merchandising_StyleFK,si.Common_RawItemFK, ss.Common_ColorFK, i.Name
								) B on B.Merchandising_StyleFK = A.Merchandising_StyleFK and B.Common_RawItemFK = A.Common_RawItemFK
				Left Join (
								Select  si.Merchandising_StyleFK, si.Common_RawItemFK, ss.Common_ColorFK, Max(ss.GSM) GSM, i.Name, Round(SUM(si.ReceivedQty),2) [Sewing Store Received Qty]
								from Store_StockIn si
								join Common_RawItem i on si.Common_RawItemFK = 	i.ID			
								Join Store_General sg on sg.ID  = si.Store_GeneralFK
								join Procurement_PurchaseOrderSlave ms on ms.ID = si.Procurement_PurchaseOrderSlaveFK 
								join Merchandising_StyleSlave ss on ss.ID = si.Merchandising_StyleSlaveFK and ss.Common_RawItemFK = si.Common_RawItemFK
								where si.Active = 1 and si.Store_GeneralFK = 3 and si.Merchandising_StyleFK IN (Select ID From Merchandising_Style where Active = 1)
								group by si.Merchandising_StyleFK,si.Common_RawItemFK, ss.Common_ColorFK, i.Name
								) C on C.Merchandising_StyleFK = A.Merchandising_StyleFK and C.Common_RawItemFK = A.Common_RawItemFK
				Left Join (
							Select  si.Merchandising_StyleFK, si.Common_RawItemFK, ss.Common_ColorFK, Max(ss.GSM) GSM, i.Name, Round(SUM(si.ReceivedQty),2) [Iron Store Received Qty]
								from Store_StockIn si
								join Common_RawItem i on si.Common_RawItemFK = 	i.ID			
								Join Store_General sg on sg.ID  = si.Store_GeneralFK
								join Procurement_PurchaseOrderSlave ms on ms.ID = si.Procurement_PurchaseOrderSlaveFK 
								join Merchandising_StyleSlave ss on ss.ID = si.Merchandising_StyleSlaveFK and ss.Common_RawItemFK = si.Common_RawItemFK
								where si.Active = 1 and si.Store_GeneralFK = 4 and si.Merchandising_StyleFK IN (Select ID From Merchandising_Style where Active = 1)
								group by si.Merchandising_StyleFK,si.Common_RawItemFK, ss.Common_ColorFK, i.Name  
							) D on D.Merchandising_StyleFK = A.Merchandising_StyleFK and D.Common_RawItemFK = A.Common_RawItemFK

				Left Join (			
							Select  si.Merchandising_StyleFK, si.Common_RawItemFK, ss.Common_ColorFK, Max(ss.GSM) GSM, i.Name, Round(SUM(si.ReceivedQty),2) [Packing Store Received Qty]
								from Store_StockIn si
								join Common_RawItem i on si.Common_RawItemFK = 	i.ID			
								Join Store_General sg on sg.ID  = si.Store_GeneralFK
								join Procurement_PurchaseOrderSlave ms on ms.ID = si.Procurement_PurchaseOrderSlaveFK 
								join Merchandising_StyleSlave ss on ss.ID = si.Merchandising_StyleSlaveFK and ss.Common_RawItemFK = si.Common_RawItemFK
								where si.Active = 1 and si.Store_GeneralFK = 5 and si.Merchandising_StyleFK IN (Select ID From Merchandising_Style where Active = 1)
								group by si.Merchandising_StyleFK,si.Common_RawItemFK, ss.Common_ColorFK, si.Common_Size, i.Name 
							) F on F.Merchandising_StyleFK = A.Merchandising_StyleFK and F.Common_RawItemFK = A.Common_RawItemFK
							
				Left Join ( Select 3 [ID], 0 [Active], 1 [User], getdate() [Time], '' [Remarks], 1 [UserID],	
									A.*, 
									ISNULL(Round(A.ConsumptionPerDz/12 * B.ReceivedQty,0),0) TotalConsumption												
							 From (
									Select ms.Merchandising_StyleFK, Common_RawItemFK, Isnull(ms.Common_ColorFK, 0) Common_ColorFK, Max(ms.GSM) GSM, (Max(ri.Name) + '-'+ CAST(Common_RawItemFK as varchar)) Name, 
									Max(u.Name) as UnitName, Max(ms.Consumption) ConsumptionPerDz				
									from Merchandising_StyleSlave ms
									left join Merchandising_Style m on m.id = ms.Merchandising_StyleFK
									left join Common_RawItem ri on ms.Common_RawItemFK = ri.ID
									left join Common_Unit u on u.ID = ri.Common_UnitFK
									Where ms.Active =1 And Merchandising_StyleFK IN (Select ID From Merchandising_Style where Active = 1)
									Group by ms.Merchandising_StyleFK, ms.Common_RawItemFK, ms.Common_ColorFK) A

							LEFT JOIN ( Select si.Merchandising_StyleFK, si.Common_RawItemFK, SUM(si.ReceivedQty * s.PackPieceQty) ReceivedQty
										from Store_StockIn si
										join Merchandising_Style s  on si.Merchandising_StyleFK = s.ID
										where si.Active = 1 and si.Store_GeneralFK = 6 and si.Merchandising_StyleFK IN (Select ID From Merchandising_Style where Active = 1)
										group by si.Merchandising_StyleFK, si.Common_RawItemFK) B on B.Merchandising_StyleFK = A.Merchandising_StyleFK

									) G on G.Merchandising_StyleFK = A.Merchandising_StyleFK And G.Common_RawItemFK = A.Common_RawItemFK

		Union All  -- Item Wise --
				
		Select 3 [ID], 0 [Active], 1 [User], getdate() [Time], '' [Remarks], 1 [UserID],	
				
				(Select CID + StyleName From Merchandising_Style where ID = A.Merchandising_StyleFK) StylesName ,
				A.Merchandising_StyleFK,
				A.Common_RawItemFK,
				ISnull(A.Common_ColorFK, 0) Common_ColorFK,
				ISnull(A.GSM, 0) GSM,
				(A.Name +'-'+ Isnull((Select Name From Common_Color Where ID = A.Common_ColorFK), 'Non Color')+'-'+ Cast(cast(A.GSM as int)as nvarchar)) Name,
				A.UnitName,
				A.ConsumptionPerDz,
				ISNULL(Round(A.ConsumptionPerDz/12 * B.ReceivedQty,0),0) TotalConsumption,				
				0 OrderConsumQty,
				0 TotalRecMain,
				 0 TotalRecCutting,
				 0 TotalRecSewing, 
				 0 TotalRecIroning ,
				 0 TotalRecPacking, 
				 0 TotalRecFinish,
				 0 TotalOutMain,

				 0 TotalOutCutting,
				 0 TotalOutSewing,
				 0 TotalOutIroning,
				 0 TotalOutPacking,
				 0 TotalOutFinish
		 From (
				Select ms.Merchandising_StyleFK, Common_RawItemFK, ms.Common_ColorFK, ms.GSM
				, Max(ri.Name) Name, 
				--,(Max(ri.Name) +'-'+ (Select Name From Common_Color Where ID = ms.Common_ColorFK)+'-'+ Cast(cast(ms.GSM as int)as nvarchar)) Name,
									Max(u.Name) as UnitName, Max(ms.Consumption) ConsumptionPerDz				
									from Merchandising_StyleSlave ms
									left join Merchandising_Style m on m.id = ms.Merchandising_StyleFK
									left join Common_RawItem ri on ms.Common_RawItemFK = ri.ID
									left join Common_Unit u on u.ID = ri.Common_UnitFK
									Where ms.Active =1 And Merchandising_StyleFK IN (Select ID From Merchandising_Style where Active = 1)
									Group by ms.Merchandising_StyleFK, ms.Common_RawItemFK, ms.Common_ColorFK, ms.GSM) A
		LEFT JOIN (Select si.Merchandising_StyleFK, si.Common_RawItemFK, SUM(si.ReceivedQty * s.PackPieceQty) ReceivedQty
							from Store_StockIn si
							join Merchandising_Style s  on si.Merchandising_StyleFK = s.ID
							where si.Active = 1 and si.Store_GeneralFK = 6 and si.Merchandising_StyleFK IN (Select ID From Merchandising_Style where Active = 1)
							group by si.Merchandising_StyleFK, si.Common_RawItemFK) B on B.Merchandising_StyleFK = A.Merchandising_StyleFK
		

		----- Bellow Item Wise -----
		INSERT INTO Store_ItemWiseConsumptionAllStyle	

		Select 5 [ID], 0 [Active], 1 [User], getdate() [Time], '' [Remarks], 1 [UserID],
				'' StylesName ,
				0 Merchandising_StyleFK,
				A.Common_RawItemFK,
				Max(A.Common_ColorFK) Common_ColorFK,			
				Max(A.Common_Size) Common_Size,
				Max(A.Name) Name,
				--(Max(A.Name) + '-'+ CAST(A.Common_RawItemFK as varchar)+'-Color: '+ISNULL( (Select Name From Common_Color Where ID = Max(A.Common_ColorFK)), 'Non'))  Name,
				Max(A.UnitName) UnitName,
				0 ConsumptionPerDz,
				0 TotalConsumption,				
				0 OrderConsumQty,

				 Sum(A.TotalRecMain) TotalRecMain,
				 Sum(A.TotalRecCutting) TotalRecCutting,
				 Sum(A.TotalRecSewing) TotalRecSewing, 
				 Sum(A.TotalRecIroning) TotalRecIroning,
				 Sum(A.TotalRecPacking) TotalRecPacking, 
				 Sum(A.TotalRecFinish) TotalRecFinish,
				 Sum(A.TotalOutMain) TotalOutMain,
				 
				 Sum(A.TotalOutCutting) TotalOutCutting,
				 Sum(A.TotalOutSewing) TotalOutSewing,
				 Sum(A.TotalOutIroning) TotalOutIroning,
				 Sum(A.TotalOutPacking) TotalOutPacking,
				 Sum(A.TotalOutFinish) TotalOutFinish
			From Store_ItemWiseConsumptionAllStyle A 
			Where ID = 2 
			Group By Common_RawItemFK 
			Order By Name


	INSERT INTO Store_ItemWiseConsumptionAllStyle

		Select 6 [ID], 0 [Active], 1 [User], getdate() [Time], '' [Remarks], 1 [UserID],
				'' StylesName ,
				0 Merchandising_StyleFK,
				A.Common_RawItemFK,
				Max(A.Common_ColorFK) Common_ColorFK,			
				Max(A.Common_Size) Common_Size,
				Max(A.Name) Name, 
				--(Max(A.Name) +'-'+ISNULL( (Select Name From Common_Color Where ID = Max(A.Common_ColorFK)), 'Non'))  Name,
				Max(A.UnitName) UnitName,
				0 ConsumptionPerDz,
				0 TotalConsumption,				
				0 OrderConsumQty,

				 Sum(A.TotalRecMain) TotalRecMain,
				 Sum(A.TotalRecCutting) TotalRecCutting,
				 Sum(A.TotalRecSewing) TotalRecSewing, 
				 Sum(A.TotalRecIroning) TotalRecIroning,
				 Sum(A.TotalRecPacking) TotalRecPacking, 
				 Sum(A.TotalRecFinish) TotalRecFinish,
				 Sum(A.TotalOutMain) TotalOutMain,
				 
				 Sum(A.TotalOutCutting) TotalOutCutting,
				 Sum(A.TotalOutSewing) TotalOutSewing,
				 Sum(A.TotalOutIroning) TotalOutIroning,
				 Sum(A.TotalOutPacking) TotalOutPacking,
				 Sum(A.TotalOutFinish) TotalOutFinish
			From Store_ItemWiseConsumptionAllStyle A 
			Where ID = 3
			Group By Common_RawItemFK 
			Order By Name


	END

///////////////////////////////////////

ALTER Procedure [dbo].[Sp_ItemWiseConsumptionAllStyle]

 AS
		
BEGIN 	

		DELETE FROM Store_ItemWiseConsumptionAllStyle
		INSERT INTO Store_ItemWiseConsumptionAllStyle		

Select 1 [ID], 1 [Active],1 [User], getdate() [Time], '' [Remarks], 1 [UserID],
	 
	 (Select CID + StyleName From Merchandising_Style Where ID = A.Merchandising_StyleFK) StyleName ,
	 A.Merchandising_StyleFK,
	 A.Common_RawItemFK,
	 A.Common_ColorFK,
	 A.Common_Size,
	 (A.Name +'-'+ (Select Name From Common_Color Where ID = A.Common_ColorFK)+'-'+ Cast(cast(A.Common_Size as int)as nvarchar)) Name, 
	 A.UnitName,
	 0 ConsumptionDz,
	 0 OrderConsumQty,
	 A.PurchasingPrice FinishQtyDz,
	 A.[Main Store Received Qty] TotalRecMain,
	Isnull(B.[Cutting Store Received Qty],0) TotalRecCutting,
	ISNULL(CASE WHEN B.[Cutting Store Received Qty] > 0 THEN 0 ELSE C.[Sewing Store Received Qty] END, 0) TotalRecSewing, 
	ISNULL(CASE WHEN C.[Sewing Store Received Qty] > 0 THEN 0 ELSE D.[Iron Store Received Qty] END, 0) TotalRecIroning, 
	ISNULL(CASE WHEN D.[Iron Store Received Qty] > 0 THEN 0 ELSE F.[Packing Store Received Qty] END, 0) TotalRecPacking,	
	
	 ISNULL(G.TotalConsumption, 0) TotalRecFinish, 

	 ISNULL(CASE
			WHEN B.[Cutting Store Received Qty] > 0 THEN (B.[Cutting Store Received Qty])
			WHEN C.[Sewing Store Received Qty] > 0 THEN ( C.[Sewing Store Received Qty])
			WHEN D.[Iron Store Received Qty] > 0 THEN (D.[Iron Store Received Qty])
			ELSE (F.[Packing Store Received Qty])
			END, 0) TotalOutMain,
	 0 TotalOutCutting,
	 0 TotalOutSewing,
	 0 TotalOutIroning,
	 0 TotalOutPacking,
	 ISNULL(CASE
				WHEN B.[Cutting Store Received Qty] > 0 THEN (B.[Cutting Store Received Qty])
				WHEN C.[Sewing Store Received Qty] > 0 THEN ( C.[Sewing Store Received Qty])
				WHEN D.[Iron Store Received Qty] > 0 THEN (D.[Iron Store Received Qty])
				ELSE (F.[Packing Store Received Qty])
				END, 0) TotalOutFinish
	--INTO Store_ItemWiseConsumptionAllStyle

from (		Select  si.Merchandising_StyleFK, si.Common_RawItemFK
				,si.Procurement_PurchaseOrderSlaveFK
				, ISNULL(ss.Common_ColorFK, 0) Common_ColorFK
				, Max(ss.GSM) Common_Size
				, i.Name 
				, Max(u.Name) UnitName, isnull( Round(Max(si.ReceivedQty),2), 0) [Main Store Received Qty]
				, Max(ms.PurchasingPrice) PurchasingPrice
				from Store_StockIn si
				join Common_RawItem i on si.Common_RawItemFK = 	i.ID 			
				Join Store_General sg on sg.ID = si.Store_GeneralFK
				join Common_Unit u on u.ID = si.CommonUnitFK 
				join Procurement_PurchaseOrderSlave ms on ms.ID = si.Procurement_PurchaseOrderSlaveFK 
				join Merchandising_StyleSlave ss on ss.ID = ms.Merchandising_StyleSlaveFK and ss.Common_RawItemFK = si.Common_RawItemFK
				where si.Active = 1 and si.Store_GeneralFK = 2 and si.Merchandising_StyleFK IN (Select ID From Merchandising_Style where Active = 1)
				group by si.Merchandising_StyleFK, si.Common_RawItemFK, Procurement_PurchaseOrderSlaveFK, i.Name, ss.Common_ColorFK, ss.Common_SizeFk		

				) A
Left Join (
				Select  si.Merchandising_StyleFK, si.Common_RawItemFK, ISNULL(ss.Common_ColorFK, 0) Common_ColorFK, Max(ss.GSM) GSM, i.Name, Round(SUM(si.ReceivedQty),2) [Cutting Store Received Qty]
				from Store_StockIn si
				join Common_RawItem i on si.Common_RawItemFK = 	i.ID			
				Join Store_General sg on sg.ID  = si.Store_GeneralFK
				join Procurement_PurchaseOrderSlave ms on ms.ID = si.Procurement_PurchaseOrderSlaveFK
				join Merchandising_StyleSlave ss on ss.ID = ms.Merchandising_StyleSlaveFK and ss.Common_RawItemFK = si.Common_RawItemFK
				where si.Active = 1 and si.Store_GeneralFK = 1 and si.Merchandising_StyleFK IN (Select ID From Merchandising_Style where Active = 1)
				group by si.Merchandising_StyleFK,si.Common_RawItemFK, ss.Common_ColorFK,i.Name--, ss.Common_SizeFk	

				) B on B.Merchandising_StyleFK = A.Merchandising_StyleFK and B.Common_RawItemFK = A.Common_RawItemFK
Left Join (
				Select  si.Merchandising_StyleFK, si.Common_RawItemFK, ISNULL(ss.Common_ColorFK, 0) Common_ColorFK,  Max(ss.GSM) GSM, i.Name, Round(SUM(si.ReceivedQty),2) [Sewing Store Received Qty]
				from Store_StockIn si
				join Common_RawItem i on si.Common_RawItemFK = 	i.ID			
				Join Store_General sg on sg.ID  = si.Store_GeneralFK
				join Procurement_PurchaseOrderSlave ms on ms.ID = si.Procurement_PurchaseOrderSlaveFK
				join Merchandising_StyleSlave ss on ss.ID = ms.Merchandising_StyleSlaveFK and ss.Common_RawItemFK = si.Common_RawItemFK
				where si.Active = 1 and si.Store_GeneralFK = 3 and si.Merchandising_StyleFK IN (Select ID From Merchandising_Style where Active = 1)
				group by si.Merchandising_StyleFK,si.Common_RawItemFK, ss.Common_ColorFK,i.Name

				) C on C.Merchandising_StyleFK = A.Merchandising_StyleFK and C.Common_RawItemFK = A.Common_RawItemFK
Left Join (
			select  si.Merchandising_StyleFK, si.Common_RawItemFK, ISNULL(ss.Common_ColorFK, 0) Common_ColorFK,  Max(ss.GSM) GSM, i.Name, Round(SUM(si.ReceivedQty),2) [Iron Store Received Qty]
				from Store_StockIn si
				join Common_RawItem i on si.Common_RawItemFK = 	i.ID			
				Join Store_General sg on sg.ID  = si.Store_GeneralFK
				join Procurement_PurchaseOrderSlave ms on ms.ID = si.Procurement_PurchaseOrderSlaveFK
				join Merchandising_StyleSlave ss on ss.ID = ms.Merchandising_StyleSlaveFK and ss.Common_RawItemFK = si.Common_RawItemFK
				where si.Active = 1 and si.Store_GeneralFK = 4 and si.Merchandising_StyleFK IN (Select ID From Merchandising_Style where Active = 1)
				group by si.Merchandising_StyleFK,si.Common_RawItemFK, ss.Common_ColorFK,i.Name 
			) D on D.Merchandising_StyleFK = A.Merchandising_StyleFK and D.Common_RawItemFK = A.Common_RawItemFK

Left Join (			
			Select  si.Merchandising_StyleFK, si.Common_RawItemFK, ISNULL(ss.Common_ColorFK, 0) Common_ColorFK,  Max(ss.GSM) GSM, i.Name, Round(SUM(si.ReceivedQty),2) [Packing Store Received Qty]
				from Store_StockIn si
				join Common_RawItem i on si.Common_RawItemFK = 	i.ID			
				Join Store_General sg on sg.ID  = si.Store_GeneralFK
				join Procurement_PurchaseOrderSlave ms on ms.ID = si.Procurement_PurchaseOrderSlaveFK
				join Merchandising_StyleSlave ss on ss.ID = ms.Merchandising_StyleSlaveFK and ss.Common_RawItemFK = si.Common_RawItemFK
				where si.Active = 1 and si.Store_GeneralFK = 5 and si.Merchandising_StyleFK IN (Select ID From Merchandising_Style where Active = 1)
				group by si.Merchandising_StyleFK, si.Common_RawItemFK, ss.Common_ColorFK,i.Name  
			) F on F.Merchandising_StyleFK = A.Merchandising_StyleFK and F.Common_RawItemFK = A.Common_RawItemFK
			
Left Join (Select 3 [ID], 0 [Active], 1 [User], getdate() [Time], '' [Remarks], 1 [UserID],	
				A.*, 
				ISNULL(Round(A.ConsumptionPerDz/12 * B.ReceivedQty,0),0) TotalConsumption						
		 From (
				Select ms.Merchandising_StyleFK, Common_RawItemFK, Isnull(ms.Common_ColorFK, 0) Common_ColorFK,  Max(ms.GSM) GSM, (Max(ri.Name) + '-'+ CAST(Common_RawItemFK as varchar)) Name, 
				Max(u.Name) as UnitName, Max(ms.Consumption) ConsumptionPerDz				
				from Merchandising_StyleSlave ms
				left join Merchandising_Style m on m.id = ms.Merchandising_StyleFK
				left join Common_RawItem ri on ms.Common_RawItemFK = ri.ID
				left join Common_Unit u on u.ID = ri.Common_UnitFK
				Where ms.Active =1 And Merchandising_StyleFK IN (Select ID From Merchandising_Style where Active = 1)
				Group by ms.Merchandising_StyleFK, ms.Common_RawItemFK, ms.Common_ColorFK) A   --, ms.GSM
		LEFT JOIN (Select si.Merchandising_StyleFK, si.Common_RawItemFK, SUM(si.ReceivedQty * s.PackPieceQty) ReceivedQty
					from Store_StockIn si
					join Merchandising_Style s  on si.Merchandising_StyleFK = s.ID
					where si.Active = 1 and si.Store_GeneralFK = 6 and si.Merchandising_StyleFK IN (Select ID From Merchandising_Style where Active = 1)
					group by si.Merchandising_StyleFK, si.Common_RawItemFK) B on B.Merchandising_StyleFK = A.Merchandising_StyleFK		
				
				) G on G.Merchandising_StyleFK = A.Merchandising_StyleFK And G.Common_RawItemFK = A.Common_RawItemFK

	Union All
			
	Select 
				0 [ID], 0 [Active], 1 [User], getdate() [Time], '' [Remarks], 1 [UserID],
				(Select CID + StyleName From Merchandising_Style where ID = si.Merchandising_StyleFK) StylesName ,
				si.Merchandising_StyleFK,
				Max(si.ID) Common_RawItemFK,
				ss.Common_ColorFK,
				Max(ss.GSM) GSM,
				s.CID +'/'+ s.StyleName StyleName,
				finItem.Name  as [Item Name],				 
		         Round(SUM(si.ReceivedQty),2) [Finishing Item] ,
				(Round(SUM(si.ReceivedQty),2) * s.UnitPrice) as [Finish Item Value],
				0,
				0 TotalRecMain,
				 0 TotalRecCutting,
				 0 TotalRecSewing, 
				 0 TotalRecIroning ,
				 0 TotalRecPacking, 
				 0 TotalRecFinish,
				 0 TotalOutMain,

				 0 TotalOutCutting,
				 0 TotalOutSewing,
				 0 TotalOutIroning,
				 0 TotalOutPacking,
				 0 TotalOutFinish
				from Store_StockIn si
				join Merchandising_Style s  on si.Merchandising_StyleFK = s.ID	
				join Common_FinishItem finItem  on s.Common_FinishItemFK = finItem.ID			
						
				Join Store_General sg on sg.ID  = si.Store_GeneralFK				
				join Procurement_PurchaseOrderSlave ms on ms.ID = si.Procurement_PurchaseOrderSlaveFK 
				join Merchandising_StyleSlave ss on ss.ID = ms.Merchandising_StyleSlaveFK and ss.Common_RawItemFK = si.Common_RawItemFK
				where si.Active = 1 and si.Store_GeneralFK = 6 and si.Merchandising_StyleFK IN (Select ID From Merchandising_Style where Active = 1)
				group by si.Merchandising_StyleFK, s.UnitPrice, ss.Common_ColorFK,finItem.Name,s.CID,s.StyleName

				Union All
				
	Select 2 [ID], 1 [Active],1 [User], getdate() [Time], '' [Remarks], 1 [UserID],
					 
					 (Select CID + StyleName From Merchandising_Style where ID = A.Merchandising_StyleFK) StylesName ,
					 A.Merchandising_StyleFK,
					 A.Common_RawItemFK ,
					 Isnull(A.Common_ColorFK, 0) Common_ColorFK,
					 ISnull(A.GSM ,0) GSM,
					 (A.Name +'-'+ (Select Name From Common_Color Where ID = A.Common_ColorFK)+'-'+ Cast(cast(A.GSM as int)as nvarchar)) Name,
					 A.UnitName,
					 0,
					 0,
					 A.PurchasingPrice,
					 A.[Main Store Received Qty] TotalRecMain,
					 Isnull(B.[Cutting Store Received Qty],0) TotalRecCutting,
					 ISNULL(CASE WHEN B.[Cutting Store Received Qty] > 0 THEN 0 ELSE C.[Sewing Store Received Qty] END, 0) TotalRecSewing, 
					 ISNULL(CASE WHEN C.[Sewing Store Received Qty] > 0 THEN 0 ELSE D.[Iron Store Received Qty] END, 0) TotalRecIroning, 
					 ISNULL(CASE WHEN D.[Iron Store Received Qty] > 0 THEN 0 ELSE F.[Packing Store Received Qty] END, 0) TotalRecPacking,	
					 ISNULL(G.TotalConsumption, 0) TotalRecFinish,
					 ISNULL(CASE
							 WHEN B.[Cutting Store Received Qty] > 0 THEN (B.[Cutting Store Received Qty])
							 WHEN C.[Sewing Store Received Qty] > 0 THEN ( C.[Sewing Store Received Qty])
							 WHEN D.[Iron Store Received Qty] > 0 THEN (D.[Iron Store Received Qty])
							 ELSE (F.[Packing Store Received Qty])
							 END, 0) TotalOutMain,
					
					 0 TotalOutCutting,
					 0 TotalOutSewing,
					 0 TotalOutIroning,
					 0 TotalOutPacking,
					 ISNULL(CASE
							 WHEN B.[Cutting Store Received Qty] > 0 THEN (B.[Cutting Store Received Qty])
							 WHEN C.[Sewing Store Received Qty] > 0 THEN ( C.[Sewing Store Received Qty])
							 WHEN D.[Iron Store Received Qty] > 0 THEN (D.[Iron Store Received Qty])
							 ELSE (F.[Packing Store Received Qty])
							 END, 0) TotalOutFinish

				from (
						Select  si.Merchandising_StyleFK, si.Common_RawItemFK, ss.Common_ColorFK, ss.GSM GSM, i.Name, Max(u.Name) UnitName, isnull( Round(SUM(si.ReceivedQty),2), 0) [Main Store Received Qty]
								, Max(ms.PurchasingPrice) PurchasingPrice
								from Store_StockIn si
								join Common_RawItem i on si.Common_RawItemFK = 	i.ID			
								Join Store_General sg on sg.ID  = si.Store_GeneralFK
								join Common_Unit u on u.ID = si.CommonUnitFK 
								join Procurement_PurchaseOrderSlave ms on ms.ID = si.Procurement_PurchaseOrderSlaveFK 
								join Merchandising_StyleSlave ss on ss.ID = ms.Merchandising_StyleSlaveFK and ss.Common_RawItemFK = si.Common_RawItemFK
								where si.Active = 1 and si.Store_GeneralFK = 2 and si.Merchandising_StyleFK IN (Select ID From Merchandising_Style where Active = 1)
								group by si.Merchandising_StyleFK,si.Common_RawItemFK, ss.Common_ColorFK, i.Name , ss.GSM 	 
								) A
				Left Join (
								Select  si.Merchandising_StyleFK, si.Common_RawItemFK, ss.Common_ColorFK, Max(ss.GSM) GSM,  i.Name, Round(SUM(si.ReceivedQty),2) [Cutting Store Received Qty]
								from Store_StockIn si
								join Common_RawItem i on si.Common_RawItemFK = 	i.ID			
								Join Store_General sg on sg.ID  = si.Store_GeneralFK
								join Procurement_PurchaseOrderSlave ms on ms.ID = si.Procurement_PurchaseOrderSlaveFK 
								join Merchandising_StyleSlave ss on ss.ID = ms.Merchandising_StyleSlaveFK and ss.Common_RawItemFK = si.Common_RawItemFK
								where si.Active = 1 and si.Store_GeneralFK = 1 and si.Merchandising_StyleFK IN (Select ID From Merchandising_Style where Active = 1)
								group by si.Merchandising_StyleFK,si.Common_RawItemFK, ss.Common_ColorFK, i.Name
								) B on B.Merchandising_StyleFK = A.Merchandising_StyleFK and B.Common_RawItemFK = A.Common_RawItemFK
				Left Join (
								Select  si.Merchandising_StyleFK, si.Common_RawItemFK, ss.Common_ColorFK, Max(ss.GSM) GSM, i.Name, Round(SUM(si.ReceivedQty),2) [Sewing Store Received Qty]
								from Store_StockIn si
								join Common_RawItem i on si.Common_RawItemFK = 	i.ID			
								Join Store_General sg on sg.ID  = si.Store_GeneralFK
								join Procurement_PurchaseOrderSlave ms on ms.ID = si.Procurement_PurchaseOrderSlaveFK 
								join Merchandising_StyleSlave ss on ss.ID = ms.Merchandising_StyleSlaveFK and ss.Common_RawItemFK = si.Common_RawItemFK
								where si.Active = 1 and si.Store_GeneralFK = 3 and si.Merchandising_StyleFK IN (Select ID From Merchandising_Style where Active = 1)
								group by si.Merchandising_StyleFK,si.Common_RawItemFK, ss.Common_ColorFK, i.Name
								) C on C.Merchandising_StyleFK = A.Merchandising_StyleFK and C.Common_RawItemFK = A.Common_RawItemFK
				Left Join (
							Select  si.Merchandising_StyleFK, si.Common_RawItemFK, ss.Common_ColorFK, Max(ss.GSM) GSM, i.Name, Round(SUM(si.ReceivedQty),2) [Iron Store Received Qty]
								from Store_StockIn si
								join Common_RawItem i on si.Common_RawItemFK = 	i.ID			
								Join Store_General sg on sg.ID  = si.Store_GeneralFK
								join Procurement_PurchaseOrderSlave ms on ms.ID = si.Procurement_PurchaseOrderSlaveFK 
								join Merchandising_StyleSlave ss on ss.ID = ms.Merchandising_StyleSlaveFK and ss.Common_RawItemFK = si.Common_RawItemFK
								where si.Active = 1 and si.Store_GeneralFK = 4 and si.Merchandising_StyleFK IN (Select ID From Merchandising_Style where Active = 1)
								group by si.Merchandising_StyleFK,si.Common_RawItemFK, ss.Common_ColorFK, i.Name  
							) D on D.Merchandising_StyleFK = A.Merchandising_StyleFK and D.Common_RawItemFK = A.Common_RawItemFK

				Left Join (			
							Select  si.Merchandising_StyleFK, si.Common_RawItemFK, ss.Common_ColorFK, Max(ss.GSM) GSM, i.Name, Round(SUM(si.ReceivedQty),2) [Packing Store Received Qty]
								from Store_StockIn si
								join Common_RawItem i on si.Common_RawItemFK = 	i.ID			
								Join Store_General sg on sg.ID  = si.Store_GeneralFK
								join Procurement_PurchaseOrderSlave ms on ms.ID = si.Procurement_PurchaseOrderSlaveFK 
								join Merchandising_StyleSlave ss on ss.ID = ms.Merchandising_StyleSlaveFK and ss.Common_RawItemFK = si.Common_RawItemFK
								where si.Active = 1 and si.Store_GeneralFK = 5 and si.Merchandising_StyleFK IN (Select ID From Merchandising_Style where Active = 1)
								group by si.Merchandising_StyleFK,si.Common_RawItemFK, ss.Common_ColorFK, si.Common_Size, i.Name 
							) F on F.Merchandising_StyleFK = A.Merchandising_StyleFK and F.Common_RawItemFK = A.Common_RawItemFK
							
				Left Join ( Select 3 [ID], 0 [Active], 1 [User], getdate() [Time], '' [Remarks], 1 [UserID],	
									A.*, 
									ISNULL(Round(A.ConsumptionPerDz/12 * B.ReceivedQty,0),0) TotalConsumption												
							 From (
									Select ms.Merchandising_StyleFK, Common_RawItemFK, Isnull(ms.Common_ColorFK, 0) Common_ColorFK, Max(ms.GSM) GSM, (Max(ri.Name) + '-'+ CAST(Common_RawItemFK as varchar)) Name, 
									Max(u.Name) as UnitName, Max(ms.Consumption) ConsumptionPerDz				
									from Merchandising_StyleSlave ms
									left join Merchandising_Style m on m.id = ms.Merchandising_StyleFK
									left join Common_RawItem ri on ms.Common_RawItemFK = ri.ID
									left join Common_Unit u on u.ID = ri.Common_UnitFK
									Where ms.Active =1 And Merchandising_StyleFK IN (Select ID From Merchandising_Style where Active = 1)
									Group by ms.Merchandising_StyleFK, ms.Common_RawItemFK, ms.Common_ColorFK) A

							LEFT JOIN ( Select si.Merchandising_StyleFK, si.Common_RawItemFK, SUM(si.ReceivedQty * s.PackPieceQty) ReceivedQty
										from Store_StockIn si
										join Merchandising_Style s  on si.Merchandising_StyleFK = s.ID
										where si.Active = 1 and si.Store_GeneralFK = 6 and si.Merchandising_StyleFK IN (Select ID From Merchandising_Style where Active = 1)
										group by si.Merchandising_StyleFK, si.Common_RawItemFK) B on B.Merchandising_StyleFK = A.Merchandising_StyleFK

									) G on G.Merchandising_StyleFK = A.Merchandising_StyleFK And G.Common_RawItemFK = A.Common_RawItemFK

		Union All  -- Item Wise --
				
		Select 3 [ID], 0 [Active], 1 [User], getdate() [Time], '' [Remarks], 1 [UserID],	
				
				(Select CID + StyleName From Merchandising_Style where ID = A.Merchandising_StyleFK) StylesName ,
				A.Merchandising_StyleFK,
				A.Common_RawItemFK,
				ISnull(A.Common_ColorFK, 0) Common_ColorFK,
				ISnull(A.GSM, 0) GSM,
				(A.Name +'-'+ (Select Name From Common_Color Where ID = A.Common_ColorFK)+'-'+ Cast(cast(A.GSM as int)as nvarchar)) Name,
				A.UnitName,
				A.ConsumptionPerDz,
				ISNULL(Round(A.ConsumptionPerDz/12 * B.ReceivedQty,0),0) TotalConsumption,				
				0 OrderConsumQty,
				0 TotalRecMain,
				 0 TotalRecCutting,
				 0 TotalRecSewing, 
				 0 TotalRecIroning ,
				 0 TotalRecPacking, 
				 0 TotalRecFinish,
				 0 TotalOutMain,

				 0 TotalOutCutting,
				 0 TotalOutSewing,
				 0 TotalOutIroning,
				 0 TotalOutPacking,
				 0 TotalOutFinish
		 From (
				Select ms.Merchandising_StyleFK, Common_RawItemFK, ms.Common_ColorFK, ms.GSM
				, Max(ri.Name) Name, 
				--,(Max(ri.Name) +'-'+ (Select Name From Common_Color Where ID = ms.Common_ColorFK)+'-'+ Cast(cast(ms.GSM as int)as nvarchar)) Name,
									Max(u.Name) as UnitName, Max(ms.Consumption) ConsumptionPerDz				
									from Merchandising_StyleSlave ms
									left join Merchandising_Style m on m.id = ms.Merchandising_StyleFK
									left join Common_RawItem ri on ms.Common_RawItemFK = ri.ID
									left join Common_Unit u on u.ID = ri.Common_UnitFK
									Where ms.Active =1 And Merchandising_StyleFK IN (Select ID From Merchandising_Style where Active = 1)
									Group by ms.Merchandising_StyleFK, ms.Common_RawItemFK, ms.Common_ColorFK, ms.GSM) A
		LEFT JOIN (Select si.Merchandising_StyleFK, si.Common_RawItemFK, SUM(si.ReceivedQty * s.PackPieceQty) ReceivedQty
							from Store_StockIn si
							join Merchandising_Style s  on si.Merchandising_StyleFK = s.ID
							where si.Active = 1 and si.Store_GeneralFK = 6 and si.Merchandising_StyleFK IN (Select ID From Merchandising_Style where Active = 1)
							group by si.Merchandising_StyleFK, si.Common_RawItemFK) B on B.Merchandising_StyleFK = A.Merchandising_StyleFK
		

		----- Bellow Item Wise -----
		INSERT INTO Store_ItemWiseConsumptionAllStyle	

		Select 5 [ID], 0 [Active], 1 [User], getdate() [Time], '' [Remarks], 1 [UserID],
				'' StylesName ,
				0 Merchandising_StyleFK,
				A.Common_RawItemFK,
				Max(A.Common_ColorFK) Common_ColorFK,			
				Max(A.Common_Size) Common_Size,
				Max(A.Name) Name,
				--(Max(A.Name) + '-'+ CAST(A.Common_RawItemFK as varchar)+'-Color: '+ISNULL( (Select Name From Common_Color Where ID = Max(A.Common_ColorFK)), 'Non'))  Name,
				Max(A.UnitName) UnitName,
				0 ConsumptionPerDz,
				0 TotalConsumption,				
				0 OrderConsumQty,

				 Sum(A.TotalRecMain) TotalRecMain,
				 Sum(A.TotalRecCutting) TotalRecCutting,
				 Sum(A.TotalRecSewing) TotalRecSewing, 
				 Sum(A.TotalRecIroning) TotalRecIroning,
				 Sum(A.TotalRecPacking) TotalRecPacking, 
				 Sum(A.TotalRecFinish) TotalRecFinish,
				 Sum(A.TotalOutMain) TotalOutMain,
				 
				 Sum(A.TotalOutCutting) TotalOutCutting,
				 Sum(A.TotalOutSewing) TotalOutSewing,
				 Sum(A.TotalOutIroning) TotalOutIroning,
				 Sum(A.TotalOutPacking) TotalOutPacking,
				 Sum(A.TotalOutFinish) TotalOutFinish
			From Store_ItemWiseConsumptionAllStyle A 
			Where ID = 2 
			Group By Common_RawItemFK 
			Order By Name


	INSERT INTO Store_ItemWiseConsumptionAllStyle

		Select 6 [ID], 0 [Active], 1 [User], getdate() [Time], '' [Remarks], 1 [UserID],
				'' StylesName ,
				0 Merchandising_StyleFK,
				A.Common_RawItemFK,
				Max(A.Common_ColorFK) Common_ColorFK,			
				Max(A.Common_Size) Common_Size,
				Max(A.Name) Name, 
				--(Max(A.Name) +'-'+ISNULL( (Select Name From Common_Color Where ID = Max(A.Common_ColorFK)), 'Non'))  Name,
				Max(A.UnitName) UnitName,
				0 ConsumptionPerDz,
				0 TotalConsumption,				
				0 OrderConsumQty,

				 Sum(A.TotalRecMain) TotalRecMain,
				 Sum(A.TotalRecCutting) TotalRecCutting,
				 Sum(A.TotalRecSewing) TotalRecSewing, 
				 Sum(A.TotalRecIroning) TotalRecIroning,
				 Sum(A.TotalRecPacking) TotalRecPacking, 
				 Sum(A.TotalRecFinish) TotalRecFinish,
				 Sum(A.TotalOutMain) TotalOutMain,
				 
				 Sum(A.TotalOutCutting) TotalOutCutting,
				 Sum(A.TotalOutSewing) TotalOutSewing,
				 Sum(A.TotalOutIroning) TotalOutIroning,
				 Sum(A.TotalOutPacking) TotalOutPacking,
				 Sum(A.TotalOutFinish) TotalOutFinish
			From Store_ItemWiseConsumptionAllStyle A 
			Where ID = 3
			Group By Common_RawItemFK 
			Order By Name


	END



		-- Select * From Store_ItemWiseConsumptionAllStyle Where Merchandising_StyleFK = 18 And ID = 3  Order by Merchandising_StyleFK 

		-- EXEC [Sp_ItemWiseConsumptionAllStyle]  Select * From Store_ItemWiseConsumptionAllStyle Order by Merchandising_StyleFK Where Active = 1 and id = 2

ALTER Procedure [dbo].[Sp_ItemWiseConsumptionAllStyle]

 AS
		
BEGIN 	

		DELETE FROM Store_ItemWiseConsumptionAllStyle
		INSERT INTO Store_ItemWiseConsumptionAllStyle		

Select 1 [ID], 1 [Active],1 [User], getdate() [Time], '' [Remarks], 1 [UserID],
	 
	 (Select CID + StyleName From Merchandising_Style where ID = A.Merchandising_StyleFK) StylesName ,
	 A.Merchandising_StyleFK,
	 A.Common_RawItemFK,
	 A.Name, 
	 A.UnitName,
	 0 ConsumptionDz,
	 0 OrderConsumQty,
	 A.PurchasingPrice,
	 A.[Main Store Received Qty] TotalRecMain,
	Isnull(B.[Cutting Store Received Qty],0) TotalRecCutting,
	ISNULL(CASE WHEN B.[Cutting Store Received Qty] > 0 THEN 0 ELSE C.[Sewing Store Received Qty] END, 0) TotalRecSewing, 
	ISNULL(CASE WHEN C.[Sewing Store Received Qty] > 0 THEN 0 ELSE D.[Iron Store Received Qty] END, 0) TotalRecIroning, 
	ISNULL(CASE WHEN D.[Iron Store Received Qty] > 0 THEN 0 ELSE F.[Packing Store Received Qty] END, 0) TotalRecPacking,	
	
	 ISNULL(G.TotalConsumption, 0) TotalRecFinish, 

	 ISNULL(CASE
			WHEN B.[Cutting Store Received Qty] > 0 THEN (B.[Cutting Store Received Qty])
			WHEN C.[Sewing Store Received Qty] > 0 THEN ( C.[Sewing Store Received Qty])
			WHEN D.[Iron Store Received Qty] > 0 THEN (D.[Iron Store Received Qty])
			ELSE (F.[Packing Store Received Qty])
			END, 0) TotalOutMain,
	 0 TotalOutCutting,
	 0 TotalOutSewing,
	 0 TotalOutIroning,
	 0 TotalOutPacking,
	 ISNULL(CASE
				WHEN B.[Cutting Store Received Qty] > 0 THEN (B.[Cutting Store Received Qty])
				WHEN C.[Sewing Store Received Qty] > 0 THEN ( C.[Sewing Store Received Qty])
				WHEN D.[Iron Store Received Qty] > 0 THEN (D.[Iron Store Received Qty])
				ELSE (F.[Packing Store Received Qty])
				END, 0) TotalOutFinish
	--INTO Store_ItemWiseConsumptionAllStyle

from (
		select  si.Merchandising_StyleFK, si.Common_RawItemFK,i.Name, Max(u.Name) UnitName, isnull( Round(SUM(si.ReceivedQty),2), 0) [Main Store Received Qty]
				, Max(ms.PurchasingPrice) PurchasingPrice
				from Store_StockIn si
				join Common_RawItem i on si.Common_RawItemFK = 	i.ID			
				Join Store_General sg on sg.ID  = si.Store_GeneralFK
				join Common_Unit u on u.ID = si.CommonUnitFK 
				join Procurement_PurchaseOrderSlave ms on ms.ID = si.Procurement_PurchaseOrderSlaveFK
				where si.Active = 1 and si.Store_GeneralFK = 2 and si.Merchandising_StyleFK IN (Select ID From Merchandising_Style where Active = 1)
				group by si.Merchandising_StyleFK,si.Common_RawItemFK, i.Name  

				) A
Left Join (
				select  si.Merchandising_StyleFK, si.Common_RawItemFK,i.Name, Round(SUM(si.ReceivedQty),2) [Cutting Store Received Qty]
				from Store_StockIn si
				join Common_RawItem i on si.Common_RawItemFK = 	i.ID			
				Join Store_General sg on sg.ID  = si.Store_GeneralFK
				join Procurement_PurchaseOrderSlave ms on ms.ID = si.Procurement_PurchaseOrderSlaveFK
				where si.Active = 1 and si.Store_GeneralFK = 1 and si.Merchandising_StyleFK IN (Select ID From Merchandising_Style where Active = 1)
				group by si.Merchandising_StyleFK,si.Common_RawItemFK,i.Name
				) B on B.Merchandising_StyleFK = A.Merchandising_StyleFK and B.Common_RawItemFK = A.Common_RawItemFK
Left Join (
				select  si.Merchandising_StyleFK, si.Common_RawItemFK,i.Name, Round(SUM(si.ReceivedQty),2) [Sewing Store Received Qty]
				from Store_StockIn si
				join Common_RawItem i on si.Common_RawItemFK = 	i.ID			
				Join Store_General sg on sg.ID  = si.Store_GeneralFK
				join Procurement_PurchaseOrderSlave ms on ms.ID = si.Procurement_PurchaseOrderSlaveFK
				where si.Active = 1 and si.Store_GeneralFK = 3 and si.Merchandising_StyleFK IN (Select ID From Merchandising_Style where Active = 1)
				group by si.Merchandising_StyleFK,si.Common_RawItemFK,i.Name
				) C on C.Merchandising_StyleFK = A.Merchandising_StyleFK and C.Common_RawItemFK = A.Common_RawItemFK
Left Join (
			select  si.Merchandising_StyleFK, si.Common_RawItemFK,i.Name, Round(SUM(si.ReceivedQty),2) [Iron Store Received Qty]
				from Store_StockIn si
				join Common_RawItem i on si.Common_RawItemFK = 	i.ID			
				Join Store_General sg on sg.ID  = si.Store_GeneralFK
				join Procurement_PurchaseOrderSlave ms on ms.ID = si.Procurement_PurchaseOrderSlaveFK
				where si.Active = 1 and si.Store_GeneralFK = 4 and si.Merchandising_StyleFK IN (Select ID From Merchandising_Style where Active = 1)
				group by si.Merchandising_StyleFK,si.Common_RawItemFK,i.Name 
			) D on D.Merchandising_StyleFK = A.Merchandising_StyleFK and D.Common_RawItemFK = A.Common_RawItemFK

Left Join (			
			select  si.Merchandising_StyleFK, si.Common_RawItemFK,i.Name, Round(SUM(si.ReceivedQty),2) [Packing Store Received Qty]
				from Store_StockIn si
				join Common_RawItem i on si.Common_RawItemFK = 	i.ID			
				Join Store_General sg on sg.ID  = si.Store_GeneralFK
				join Procurement_PurchaseOrderSlave ms on ms.ID = si.Procurement_PurchaseOrderSlaveFK
				where si.Active = 1 and si.Store_GeneralFK = 5 and si.Merchandising_StyleFK IN (Select ID From Merchandising_Style where Active = 1)
				group by si.Merchandising_StyleFK,si.Common_RawItemFK,i.Name  
			) F on F.Merchandising_StyleFK = A.Merchandising_StyleFK and F.Common_RawItemFK = A.Common_RawItemFK
			
Left Join (Select 3 [ID], 0 [Active], 1 [User], getdate() [Time], '' [Remarks], 1 [UserID],	
				A.*, 
				ISNULL(Round(A.ConsumptionPerDz/12 * B.ReceivedQty,0),0) TotalConsumption
				--Round(A.ConsumptionPerDz /12 * 				
				--		(Select SUM(si.ReceivedQty * s.PackPieceQty) ReceivedQty
				--		from Store_StockIn si
				--		join Merchandising_Style s  on si.Merchandising_StyleFK = s.ID
				--		where si.Active = 1 and si.Store_GeneralFK = 6 and si.Merchandising_StyleFK IN (Select ID From Merchandising_Style where Active = 1)
				--		--group by si.Merchandising_StyleFK
				--		),0) TotalConsumption				
		 From (
				Select ms.Merchandising_StyleFK, Common_RawItemFK, (Max(ri.Name) + '-'+ CAST(Common_RawItemFK as varchar)) Name, 
				Max(u.Name) as UnitName, Max(ms.Consumption) ConsumptionPerDz				
				from Merchandising_StyleSlave ms
				left join Merchandising_Style m on m.id = ms.Merchandising_StyleFK
				left join Common_RawItem ri on ms.Common_RawItemFK = ri.ID
				left join Common_Unit u on u.ID = ri.Common_UnitFK
				Where ms.Active =1 And Merchandising_StyleFK IN (Select ID From Merchandising_Style where Active = 1)
				Group by ms.Merchandising_StyleFK, ms.Common_RawItemFK) A  
		LEFT JOIN (Select si.Merchandising_StyleFK, si.Common_RawItemFK, SUM(si.ReceivedQty * s.PackPieceQty) ReceivedQty
					from Store_StockIn si
					join Merchandising_Style s  on si.Merchandising_StyleFK = s.ID
					where si.Active = 1 and si.Store_GeneralFK = 6 and si.Merchandising_StyleFK IN (Select ID From Merchandising_Style where Active = 1)
					group by si.Merchandising_StyleFK, si.Common_RawItemFK) B on B.Merchandising_StyleFK = A.Merchandising_StyleFK		
				
				) G on G.Merchandising_StyleFK = A.Merchandising_StyleFK And G.Common_RawItemFK = A.Common_RawItemFK

			Union All
			
	Select 
				0 [ID], 0 [Active], 1 [User], getdate() [Time], '' [Remarks], 1 [UserID],
				(Select CID + StyleName From Merchandising_Style where ID = si.Merchandising_StyleFK) StylesName ,
				si.Merchandising_StyleFK,
				Max(si.ID) Common_RawItemFK,
				s.CID +'/'+ s.StyleName StyleName,
				finItem.Name  as [Item Name],				 
		         Round(SUM(si.ReceivedQty),2) [Finishing Item] ,
				(Round(SUM(si.ReceivedQty),2) * s.UnitPrice) as [Finish Item Value],
				0,
				0 TotalRecMain,
				 0 TotalRecCutting,
				 0 TotalRecSewing, 
				 0 TotalRecIroning ,
				 0 TotalRecPacking, 
				 0 TotalRecFinish,
				 0 TotalOutMain,

				 0 TotalOutCutting,
				 0 TotalOutSewing,
				 0 TotalOutIroning,
				 0 TotalOutPacking,
				 0 TotalOutFinish
				from Store_StockIn si
				join Merchandising_Style s  on si.Merchandising_StyleFK = s.ID	
				join Common_FinishItem finItem  on s.Common_FinishItemFK = finItem.ID			
						
				Join Store_General sg on sg.ID  = si.Store_GeneralFK				
				join Procurement_PurchaseOrderSlave ms on ms.ID = si.Procurement_PurchaseOrderSlaveFK
				where si.Active = 1 and si.Store_GeneralFK = 6 and si.Merchandising_StyleFK IN (Select ID From Merchandising_Style where Active = 1)
				group by si.Merchandising_StyleFK, s.UnitPrice,finItem.Name,s.CID,s.StyleName

				Union All
				
				Select 2 [ID], 1 [Active],1 [User], getdate() [Time], '' [Remarks], 1 [UserID],
					 
					 (Select CID + StyleName From Merchandising_Style where ID = A.Merchandising_StyleFK) StylesName ,
					 A.Merchandising_StyleFK,
					 A.Common_RawItemFK,
					 A.Name, 
					 A.UnitName,
					 0,
					 0,
					 A.PurchasingPrice,
					 A.[Main Store Received Qty] TotalRecMain,
					 Isnull(B.[Cutting Store Received Qty],0) TotalRecCutting,
					 ISNULL(CASE WHEN B.[Cutting Store Received Qty] > 0 THEN 0 ELSE C.[Sewing Store Received Qty] END, 0) TotalRecSewing, 
					 ISNULL(CASE WHEN C.[Sewing Store Received Qty] > 0 THEN 0 ELSE D.[Iron Store Received Qty] END, 0) TotalRecIroning, 
					 ISNULL(CASE WHEN D.[Iron Store Received Qty] > 0 THEN 0 ELSE F.[Packing Store Received Qty] END, 0) TotalRecPacking,	
					 ISNULL(G.TotalConsumption, 0) TotalRecFinish,
					 ISNULL(CASE
							 WHEN B.[Cutting Store Received Qty] > 0 THEN (B.[Cutting Store Received Qty])
							 WHEN C.[Sewing Store Received Qty] > 0 THEN ( C.[Sewing Store Received Qty])
							 WHEN D.[Iron Store Received Qty] > 0 THEN (D.[Iron Store Received Qty])
							 ELSE (F.[Packing Store Received Qty])
							 END, 0) TotalOutMain,
					
					 0 TotalOutCutting,
					 0 TotalOutSewing,
					 0 TotalOutIroning,
					 0 TotalOutPacking,
					 ISNULL(CASE
							 WHEN B.[Cutting Store Received Qty] > 0 THEN (B.[Cutting Store Received Qty])
							 WHEN C.[Sewing Store Received Qty] > 0 THEN ( C.[Sewing Store Received Qty])
							 WHEN D.[Iron Store Received Qty] > 0 THEN (D.[Iron Store Received Qty])
							 ELSE (F.[Packing Store Received Qty])
							 END, 0) TotalOutFinish

				from (
						select  si.Merchandising_StyleFK, si.Common_RawItemFK,i.Name, Max(u.Name) UnitName, isnull( Round(SUM(si.ReceivedQty),2), 0) [Main Store Received Qty]
								, Max(ms.PurchasingPrice) PurchasingPrice
								from Store_StockIn si
								join Common_RawItem i on si.Common_RawItemFK = 	i.ID			
								Join Store_General sg on sg.ID  = si.Store_GeneralFK
								join Common_Unit u on u.ID = si.CommonUnitFK 
								join Procurement_PurchaseOrderSlave ms on ms.ID = si.Procurement_PurchaseOrderSlaveFK
								where si.Active = 1 and si.Store_GeneralFK = 2 and si.Merchandising_StyleFK IN (Select ID From Merchandising_Style where Active = 1)
								group by si.Merchandising_StyleFK,si.Common_RawItemFK,i.Name  
								) A
				Left Join (
								select  si.Merchandising_StyleFK, si.Common_RawItemFK,i.Name, Round(SUM(si.ReceivedQty),2) [Cutting Store Received Qty]
								from Store_StockIn si
								join Common_RawItem i on si.Common_RawItemFK = 	i.ID			
								Join Store_General sg on sg.ID  = si.Store_GeneralFK
								join Procurement_PurchaseOrderSlave ms on ms.ID = si.Procurement_PurchaseOrderSlaveFK
								where si.Active = 1 and si.Store_GeneralFK = 1 and si.Merchandising_StyleFK IN (Select ID From Merchandising_Style where Active = 1)
								group by si.Merchandising_StyleFK,si.Common_RawItemFK,i.Name
								) B on B.Merchandising_StyleFK = A.Merchandising_StyleFK and B.Common_RawItemFK = A.Common_RawItemFK
				Left Join (
								select  si.Merchandising_StyleFK, si.Common_RawItemFK,i.Name, Round(SUM(si.ReceivedQty),2) [Sewing Store Received Qty]
								from Store_StockIn si
								join Common_RawItem i on si.Common_RawItemFK = 	i.ID			
								Join Store_General sg on sg.ID  = si.Store_GeneralFK
								join Procurement_PurchaseOrderSlave ms on ms.ID = si.Procurement_PurchaseOrderSlaveFK
								where si.Active = 1 and si.Store_GeneralFK = 3 and si.Merchandising_StyleFK IN (Select ID From Merchandising_Style where Active = 1)
								group by si.Merchandising_StyleFK,si.Common_RawItemFK,i.Name
								) C on C.Merchandising_StyleFK = A.Merchandising_StyleFK and C.Common_RawItemFK = A.Common_RawItemFK
				Left Join (
							select  si.Merchandising_StyleFK, si.Common_RawItemFK,i.Name, Round(SUM(si.ReceivedQty),2) [Iron Store Received Qty]
								from Store_StockIn si
								join Common_RawItem i on si.Common_RawItemFK = 	i.ID			
								Join Store_General sg on sg.ID  = si.Store_GeneralFK
								join Procurement_PurchaseOrderSlave ms on ms.ID = si.Procurement_PurchaseOrderSlaveFK
								where si.Active = 1 and si.Store_GeneralFK = 4 and si.Merchandising_StyleFK IN (Select ID From Merchandising_Style where Active = 1)
								group by si.Merchandising_StyleFK,si.Common_RawItemFK,i.Name  
							) D on D.Merchandising_StyleFK = A.Merchandising_StyleFK and D.Common_RawItemFK = A.Common_RawItemFK

				Left Join (			
							select  si.Merchandising_StyleFK, si.Common_RawItemFK,i.Name, Round(SUM(si.ReceivedQty),2) [Packing Store Received Qty]
								from Store_StockIn si
								join Common_RawItem i on si.Common_RawItemFK = 	i.ID			
								Join Store_General sg on sg.ID  = si.Store_GeneralFK
								join Procurement_PurchaseOrderSlave ms on ms.ID = si.Procurement_PurchaseOrderSlaveFK
								where si.Active = 1 and si.Store_GeneralFK = 5 and si.Merchandising_StyleFK IN (Select ID From Merchandising_Style where Active = 1)
								group by si.Merchandising_StyleFK,si.Common_RawItemFK,i.Name 
							) F on F.Merchandising_StyleFK = A.Merchandising_StyleFK and F.Common_RawItemFK = A.Common_RawItemFK
							
				Left Join (Select 3 [ID], 0 [Active], 1 [User], getdate() [Time], '' [Remarks], 1 [UserID],	
									A.*, 
									ISNULL(Round(A.ConsumptionPerDz/12 * B.ReceivedQty,0),0) TotalConsumption
									--Round(A.ConsumptionPerDz /12 * 				
									--		(Select SUM(si.ReceivedQty * s.PackPieceQty) ReceivedQty
									--		from Store_StockIn si
									--		join Merchandising_Style s  on si.Merchandising_StyleFK = s.ID
									--		where si.Active = 1 and si.Store_GeneralFK = 6 and si.Merchandising_StyleFK IN (Select ID From Merchandising_Style where Active = 1)
									--		--group by si.Merchandising_StyleFK
									--		),0) TotalConsumption				
							 From (
									Select ms.Merchandising_StyleFK, Common_RawItemFK, (Max(ri.Name) + '-'+ CAST(Common_RawItemFK as varchar)) Name, 
									Max(u.Name) as UnitName, Max(ms.Consumption) ConsumptionPerDz				
									from Merchandising_StyleSlave ms
									left join Merchandising_Style m on m.id = ms.Merchandising_StyleFK
									left join Common_RawItem ri on ms.Common_RawItemFK = ri.ID
									left join Common_Unit u on u.ID = ri.Common_UnitFK
									Where ms.Active =1 And Merchandising_StyleFK IN (Select ID From Merchandising_Style where Active = 1)
									Group by ms.Merchandising_StyleFK, ms.Common_RawItemFK) A
							LEFT JOIN (Select si.Merchandising_StyleFK, si.Common_RawItemFK, SUM(si.ReceivedQty * s.PackPieceQty) ReceivedQty
										from Store_StockIn si
										join Merchandising_Style s  on si.Merchandising_StyleFK = s.ID
										where si.Active = 1 and si.Store_GeneralFK = 6 and si.Merchandising_StyleFK IN (Select ID From Merchandising_Style where Active = 1)
										group by si.Merchandising_StyleFK, si.Common_RawItemFK) B on B.Merchandising_StyleFK = A.Merchandising_StyleFK

									) G on G.Merchandising_StyleFK = A.Merchandising_StyleFK And G.Common_RawItemFK = A.Common_RawItemFK

				Union All
				
		Select 3 [ID], 0 [Active], 1 [User], getdate() [Time], '' [Remarks], 1 [UserID],	
				
				(Select CID + StyleName From Merchandising_Style where ID = A.Merchandising_StyleFK) StylesName ,
				A.Merchandising_StyleFK,
				A.Common_RawItemFK,
				A.Name,
				A.UnitName,
				A.ConsumptionPerDz,
				ISNULL(Round(A.ConsumptionPerDz/12 * B.ReceivedQty,0),0) TotalConsumption,
				--ISNULL(Round(A.ConsumptionPerDz /12 * 				
				--			(Select SUM(si.ReceivedQty * s.PackPieceQty) ReceivedQty
				--			from Store_StockIn si
				--			join Merchandising_Style s  on si.Merchandising_StyleFK = s.ID
				--			where si.Active = 1 and si.Store_GeneralFK = 6 and si.Merchandising_StyleFK IN (Select ID From Merchandising_Style where Active = 1)
				--			group by si.Merchandising_StyleFK
				--			),0),0) TotalConsumption,
				0 OrderConsumQty,
				0 TotalRecMain,
				 0 TotalRecCutting,
				 0 TotalRecSewing, 
				 0 TotalRecIroning ,
				 0 TotalRecPacking, 
				 0 TotalRecFinish,
				 0 TotalOutMain,

				 0 TotalOutCutting,
				 0 TotalOutSewing,
				 0 TotalOutIroning,
				 0 TotalOutPacking,
				 0 TotalOutFinish
		 From (
				Select ms.Merchandising_StyleFK, Common_RawItemFK, (Max(ri.Name) + '-'+ CAST(Common_RawItemFK as varchar)) Name, 
									Max(u.Name) as UnitName, Max(ms.Consumption) ConsumptionPerDz				
									from Merchandising_StyleSlave ms
									left join Merchandising_Style m on m.id = ms.Merchandising_StyleFK
									left join Common_RawItem ri on ms.Common_RawItemFK = ri.ID
									left join Common_Unit u on u.ID = ri.Common_UnitFK
									Where ms.Active =1 And Merchandising_StyleFK IN (Select ID From Merchandising_Style where Active = 1)
									Group by ms.Merchandising_StyleFK, ms.Common_RawItemFK) A
		LEFT JOIN (Select si.Merchandising_StyleFK, si.Common_RawItemFK, SUM(si.ReceivedQty * s.PackPieceQty) ReceivedQty
							from Store_StockIn si
							join Merchandising_Style s  on si.Merchandising_StyleFK = s.ID
							where si.Active = 1 and si.Store_GeneralFK = 6 and si.Merchandising_StyleFK IN (Select ID From Merchandising_Style where Active = 1)
							group by si.Merchandising_StyleFK, si.Common_RawItemFK) B on B.Merchandising_StyleFK = A.Merchandising_StyleFK
		
			

	END

	--BEGIN 	
			
	--		DECLARE @Iteration INT, @MaxValue INT, @Sql Nvarchar(4000)
	--		Select @MaxValue = Max(Id) From Merchandising_Style Where Active = 1
	--		Print @MaxValue

	--		SET @Iteration = 1
	--		--WHILE @Iteration <= @MaxValue
				
	--			BEGIN					
					
	--				Set @Iteration = (Select ID From Merchandising_Style Where Active = 1 and ID = @Iteration)	

	--				EXEC sp_executesql [Sp_ItemWiseConsumption];
					
	--				PRINT (@Sql)
	--				--EXEC (@Sql)
	--				SET @Iteration = @Iteration + 1
	--			END
	--END
	