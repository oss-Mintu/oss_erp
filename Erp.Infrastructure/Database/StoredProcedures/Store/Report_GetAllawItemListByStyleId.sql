USE [ERP]
GO
/****** Object:  StoredProcedure [dbo].[Sp_ItemWiseConsumption]    Script Date: 22/9/2019 5:22:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		--EXEC [Sp_ItemWiseConsumption]  Select * From Store_ItemWiseConsumption
ALTER Procedure [dbo].[Sp_ItemWiseConsumption]
 AS
		
	BEGIN 
		
		DELETE FROM Store_ItemWiseConsumption
		INSERT INTO Store_ItemWiseConsumption

			Select  0 [ID], 1 [Active],1 [User], getdate() [Time], '' [Remarks], 1 [UserID],
			bom.*, sf.FinishQtyDz,
					ISNULL(sm.FinishQtyDz,0) TotalRecMain,
			ISNULL(sc.FinishQtyDz,0) TotalRecCutting,
			ISNULL(ss.FinishQtyDz,0) TotalRecSewing,
			ISNULL(si.FinishQtyDz,0) TotalRecIroning,
			ISNULL(sp.FinishQtyDz,0) TotalRecPacking,
			ISNULL(sf.FinishQtyDz,0) TotalRecFinish,

			ISNULL(sc.FinishOutQtyDz,0) TotalOutMain,
			ISNULL(sc.FinishOutQtyDz,0) TotalOutCutting,
			ISNULL(ss.FinishOutQtyDz,0) TotalOutSewing,
			ISNULL(si.FinishOutQtyDz,0) TotalOutIroning,
			ISNULL(sp.FinishOutQtyDz,0) TotalOutPacking,
			ISNULL(sf.FinishOutQtyDz,0) TotalOutFinish
					--Round((bom.ConsumptionDz * ISNULL(sm.FinishQtyDz,0)),2) TotalRecMain,
					--Round((bom.ConsumptionDz * ISNULL(sc.FinishQtyDz,0)),2) TotalRecCutting,
					--Round((bom.ConsumptionDz * ISNULL(ss.FinishQtyDz,0)),2) TotalRecSewing,
					--Round((bom.ConsumptionDz * ISNULL(si.FinishQtyDz,0)),2) TotalRecIroning,
					--Round((bom.ConsumptionDz * ISNULL(sp.FinishQtyDz,0)),2) TotalRecPacking,
					--Round((bom.ConsumptionDz * ISNULL(sf.FinishQtyDz,0)),2) TotalRecFinish,
			
					--Round((bom.ConsumptionDz * ISNULL(sc.FinishOutQtyDz,0)),2) TotalOutMain,
					--Round((bom.ConsumptionDz * ISNULL(sc.FinishOutQtyDz,0)),2) TotalOutCutting,
					--Round((bom.ConsumptionDz * ISNULL(ss.FinishOutQtyDz,0)),2) TotalOutSewing,
					--Round((bom.ConsumptionDz * ISNULL(si.FinishOutQtyDz,0)),2) TotalOutIroning,
					--Round((bom.ConsumptionDz * ISNULL(sp.FinishOutQtyDz,0)),2) TotalOutPacking,
					--Round((bom.ConsumptionDz * ISNULL(sf.FinishOutQtyDz,0)),2) TotalOutFinish
				-- INTO Store_ItemWiseConsumption
			from (
				Select ms.Merchandising_StyleFK, Common_RawItemFK, ri.Name, u.Name as UnitName, ms.Consumption ConsumptionDz, 
				ms.Consumption * (m.PieceQty/12) OrderConsumQty
				from Merchandising_StyleSlave ms
				left join Merchandising_Style m on m.id = ms.Merchandising_StyleFK
				left join Common_RawItem ri on ms.Common_RawItemFK = ri.ID
				left join Common_Unit u on u.ID = ri.Common_UnitFK
				Where Merchandising_StyleFK = 1
				) bom
			left Join (	
				select si.Merchandising_StyleFK, Max(ri.Name) FinishItemName, SUM(si.ReceivedQty)/12 FinishQtyDz, Round(SUM(so.ReceivedQty)/12,2) FinishOutQtyDz,  
				(SUM(si.ReceivedQty)/12 - Round(SUM(so.ReceivedQty)/12,2)) FinishRemQtyDz
				from Store_StockIn si
				Left Join Store_StockOut so on so.Common_RawItemFK = si.Common_RawItemFK
				Left Join Common_RawItem ri on ri.ID = si.Common_RawItemFK 
				Left Join Store_General sg on sg.ID  = si.Store_GeneralFK
				where sg.Common_StoreTypeFK = 3 --And Merchandising_StyleFK = 1 and 
				group by si.Merchandising_StyleFK ) sf on sf.Merchandising_StyleFK = bom.Merchandising_StyleFK

			left Join (	
				select si.Merchandising_StyleFK, si.Common_RawItemFK, Max(ri.Name) RowItemName, Round(SUM(si.ReceivedQty)/12,2) FinishQtyDz, Round(SUM(so.ReceivedQty)/12,2) FinishOutQtyDz,  
				(SUM(si.ReceivedQty)/12 - Round(SUM(so.ReceivedQty)/12,2)) FinishRemQtyDz
				from Store_StockIn si
				Left Join Store_StockOut so on so.Common_RawItemFK = si.Common_RawItemFK
				Left Join Common_RawItem ri on ri.ID = si.Common_RawItemFK 
				Left Join Store_General sg on sg.ID  = si.Store_GeneralFK
				left join Merchandising_StyleSlave ms on ms.ID = si.Merchandising_StyleFK
				where si.Active = 1 and si.Store_GeneralFK = 5 --and si.Merchandising_StyleFK = 1 and ms.IsFabric = 1
				group by si.Merchandising_StyleFK,  si.Common_RawItemFK) sp on sp.Merchandising_StyleFK = bom.Merchandising_StyleFK
			left Join (	
				select si.Merchandising_StyleFK, si.Common_RawItemFK, Max(ri.Name) RowItemName, Round(SUM(si.ReceivedQty)/12,2) FinishQtyDz, Round(SUM(so.ReceivedQty)/12,2) FinishOutQtyDz,  
				(SUM(si.ReceivedQty)/12 - Round(SUM(so.ReceivedQty)/12,2)) FinishRemQtyDz
				from Store_StockIn si
				Left Join Store_StockOut so on so.Common_RawItemFK = si.Common_RawItemFK
				Left Join Common_RawItem ri on ri.ID = si.Common_RawItemFK 
				Left Join Store_General sg on sg.ID  = si.Store_GeneralFK
				left join Merchandising_StyleSlave ms on ms.ID = si.Merchandising_StyleFK
				where si.Active = 1 and si.Store_GeneralFK = 4 --and si.Merchandising_StyleFK = 1 and ms.IsFabric = 1
				group by si.Merchandising_StyleFK, si.Common_RawItemFK ) si on si.Merchandising_StyleFK = bom.Merchandising_StyleFK		
			left Join (	
				select si.Merchandising_StyleFK, si.Common_RawItemFK, Max(ri.Name) RowItemName, Round(SUM(si.ReceivedQty)/12,2) FinishQtyDz, Round(SUM(so.ReceivedQty)/12,2) FinishOutQtyDz,  
				(SUM(si.ReceivedQty)/12 - Round(SUM(so.ReceivedQty)/12,2)) FinishRemQtyDz
				from Store_StockIn si
				Left Join Store_StockOut so on so.Common_RawItemFK = si.Common_RawItemFK
				Left Join Common_RawItem ri on ri.ID = si.Common_RawItemFK 
				Left Join Store_General sg on sg.ID  = si.Store_GeneralFK
				left join Merchandising_StyleSlave ms on ms.ID = si.Merchandising_StyleFK
				where si.Active = 1 and si.Store_GeneralFK = 3 --and si.Merchandising_StyleFK = 1 and ms.IsFabric = 1 // Sewing
				group by si.Merchandising_StyleFK,  si.Common_RawItemFK) ss on ss.Merchandising_StyleFK = bom.Merchandising_StyleFK and ss.Common_RawItemFK = bom.Common_RawItemFK
			left Join (	
				select si.Merchandising_StyleFK, si.Common_RawItemFK, Max(ri.Name) RowItemName, Round(SUM(si.ReceivedQty)/12,2) FinishQtyDz, Round(SUM(so.ReceivedQty)/12,2) FinishOutQtyDz,   
				(SUM(si.ReceivedQty)/12 - Round(SUM(so.ReceivedQty)/12,2)) FinishRemQtyDz
				from Store_StockIn si
				Left Join Store_StockOut so on so.Common_RawItemFK = si.Common_RawItemFK
				Left Join Common_RawItem ri on ri.ID = si.Common_RawItemFK 
				Left Join Store_General sg on sg.ID  = si.Store_GeneralFK
				left join Merchandising_StyleSlave ms on ms.ID = si.Merchandising_StyleFK
				where si.Active = 1 and ms.IsFabric = 1 and si.Store_GeneralFK = 1 --and si.Merchandising_StyleFK = 1 // Cutting
				group by si.Merchandising_StyleFK, si.Common_RawItemFK ) sc on sc.Merchandising_StyleFK = bom.Merchandising_StyleFK	and sc.Common_RawItemFK = bom.Common_RawItemFK 
			left Join (	
				select si.Merchandising_StyleFK, si.Common_RawItemFK, Max(ri.Name) RowItemName, Round(SUM(si.ReceivedQty)/12,2) FinishQtyDz, Round(SUM(so.ReceivedQty)/12,2) FinishOutQtyDz,   
				(SUM(si.ReceivedQty)/12 - Round(SUM(so.ReceivedQty)/12,2)) FinishRemQtyDz
				from Store_StockIn si
				Left Join Store_StockOut so on so.Common_RawItemFK = si.Common_RawItemFK
				Left Join Common_RawItem ri on ri.ID = si.Common_RawItemFK 
				Left Join Store_General sg on sg.ID  = si.Store_GeneralFK
				left join Merchandising_StyleSlave ms on ms.ID = si.Merchandising_StyleFK
				where si.Active = 1 and si.Store_GeneralFK = 2 --and si.Merchandising_StyleFK = 1 -- //Main
				group by si.Merchandising_StyleFK, si.Common_RawItemFK ) sm on sm.Merchandising_StyleFK = bom.Merchandising_StyleFK and sm.Common_RawItemFK = bom.Common_RawItemFK 
				order by Merchandising_StyleFK 
	END


	
		

		





--ISNULL(wipItem.Common_StoreTypeFK, 0) WipCommon_StoreTypeFK, ISNULL(finishItem.Common_StoreTypeFK, 0) Common_StoreTypeFK, 
Select rawItem.*,  ISNULL(wipItem.ReceivedQty, 0) WipReceivedQty, ISNULL(finishItem.ReceivedQty, 0) FinishReceivedQty from (
SELECT si.Merchandising_StyleFK, si.Common_RawItemFK, ri.Name, si.Procurement_PurchaseOrderSlaveFK, sg.Common_StoreTypeFK, si.ReceivedQty  from Store_StockIn si
join Store_General sg on si.Store_GeneralFK = sg.ID
join Common_RawItem ri on si.Common_RawItemFK = ri.ID
join Procurement_PurchaseOrderSlave pos on si.Procurement_PurchaseOrderSlaveFK = pos.ID
join Common_Unit u on si.CommonUnitFK = u.ID
where sg.Common_StoreTypeFK = 1) rawItem

Left Join (
SELECT si.Merchandising_StyleFK, si.Common_RawItemFK, ri.Name, si.ID, si.Procurement_PurchaseOrderSlaveFK, sg.Common_StoreTypeFK, si.ReceivedQty from Store_StockIn si
join Store_General sg on si.Store_GeneralFK = sg.ID
join Common_RawItem ri on si.Common_RawItemFK = ri.ID
join Procurement_PurchaseOrderSlave pos on si.Procurement_PurchaseOrderSlaveFK = pos.ID
join Common_Unit u on si.CommonUnitFK = u.ID
where sg.Common_StoreTypeFK = 2) wipItem on wipItem.Common_RawItemFK = rawItem.Common_RawItemFK

Left Join (
SELECT si.Merchandising_StyleFK, si.Common_RawItemFK, ri.Name, si.ID, si.Procurement_PurchaseOrderSlaveFK, sg.Common_StoreTypeFK, si.ReceivedQty from Store_StockIn si
join Store_General sg on si.Store_GeneralFK = sg.ID
join Common_RawItem ri on si.Common_RawItemFK = ri.ID
join Procurement_PurchaseOrderSlave pos on si.Procurement_PurchaseOrderSlaveFK = pos.ID
join Common_Unit u on si.CommonUnitFK = u.ID
where sg.Common_StoreTypeFK = 3) finishItem on rawItem.Common_RawItemFK = finishItem.Common_RawItemFK

order by rawItem.Merchandising_StyleFK




=================

Select rawItem.*, wipItem.ReceivedQty WipReceivedQty, ISNULL(finishItem.ReceivedQty, 0) FinishReceivedQty from (
SELECT si.Merchandising_StyleFK, si.Common_RawItemFK, si.Procurement_PurchaseOrderSlaveFK, sg.Common_StoreTypeFK, si.ReceivedQty  from Store_StockIn si
join Store_General sg on si.Store_GeneralFK = sg.ID
join Common_RawItem ri on si.Common_RawItemFK = ri.ID
join Procurement_PurchaseOrderSlave pos on si.Procurement_PurchaseOrderSlaveFK = pos.ID
join Common_Unit u on si.CommonUnitFK = u.ID
where si.Merchandising_StyleFK = 2 AND sg.Common_StoreTypeFK = 1) rawItem

Left Join (
SELECT si.Merchandising_StyleFK, si.Common_RawItemFK, si.ID, si.Procurement_PurchaseOrderSlaveFK, sg.Common_StoreTypeFK, si.ReceivedQty from Store_StockIn si
join Store_General sg on si.Store_GeneralFK = sg.ID
join Common_RawItem ri on si.Common_RawItemFK = ri.ID
join Procurement_PurchaseOrderSlave pos on si.Procurement_PurchaseOrderSlaveFK = pos.ID
join Common_Unit u on si.CommonUnitFK = u.ID
where si.Merchandising_StyleFK = 2 AND sg.Common_StoreTypeFK = 2) wipItem on wipItem.Common_RawItemFK = rawItem.Common_RawItemFK

Left Join (
SELECT si.Merchandising_StyleFK, si.Common_RawItemFK, si.ID, si.Procurement_PurchaseOrderSlaveFK, sg.Common_StoreTypeFK, si.ReceivedQty from Store_StockIn si
join Store_General sg on si.Store_GeneralFK = sg.ID
join Common_RawItem ri on si.Common_RawItemFK = ri.ID
join Procurement_PurchaseOrderSlave pos on si.Procurement_PurchaseOrderSlaveFK = pos.ID
join Common_Unit u on si.CommonUnitFK = u.ID
where si.Merchandising_StyleFK = 1 AND sg.Common_StoreTypeFK = 3) finishItem on rawItem.Common_RawItemFK = finishItem.Common_RawItemFK





=======================================

SELECT si.Common_RawItemFK, si.Procurement_PurchaseOrderSlaveFK, si.Common_RawItemFK, sg.Common_StoreTypeFK, si.ReceivedQty  from Store_StockIn si
join Store_General sg on si.Store_GeneralFK = sg.ID
--join Common_RawItem ri on si.Common_RawItemFK = ri.ID
--join Procurement_PurchaseOrder po on si.IPOFK = po.ID
join Common_Unit u on si.CommonUnitFK = u.ID
where si.Merchandising_StyleFK = 2 AND sg.Common_StoreTypeFK != 3
Order by si.Common_RawItemFK

SELECT si.Common_RawItemFK, si.ID, si.Procurement_PurchaseOrderSlaveFK, sg.Common_StoreTypeFK, si.ReceivedQty from Store_StockIn si
join Store_General sg on si.Store_GeneralFK = sg.ID
--join Common_RawItem ri on si.Common_RawItemFK = ri.ID
join Procurement_PurchaseOrderSlave pos on si.Procurement_PurchaseOrderSlaveFK = pos.ID
join Common_Unit u on si.CommonUnitFK = u.ID
where si.Merchandising_StyleFK = 1 AND sg.Common_StoreTypeFK = 2
Order by si.Common_RawItemFK


SELECT si.Common_RawItemFK, si.ID, si.Procurement_PurchaseOrderSlaveFK, si.Common_RawItemFK, sg.Common_StoreTypeFK, si.ReceivedQty from Store_StockIn si
join Store_General sg on si.Store_GeneralFK = sg.ID
--join Common_RawItem ri on si.Common_RawItemFK = ri.ID
--join Procurement_PurchaseOrderSlave pos on si.Procurement_PurchaseOrderSlaveFK = pos.ID
join Common_Unit u on si.CommonUnitFK = u.ID
where si.Merchandising_StyleFK = 1 AND sg.Common_StoreTypeFK = 3
Order by si.Common_RawItemFK







-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
-- EXECUTE Report_GetAllawItemListByStyleId 1

ALTER PROCEDURE Report_GetAllawItemListByStyleId
	  @StyleID nvarchar(MAX)  
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	--SELECT * FROM Procurement_PurchaseOrderSlave ppos

	--SELECT ssi.*, ISNULL(sg.StoreName, 'No Store Name Selected') StoreName FROM Store_StockIn ssi
	--LEFT JOIN Store_General sg ON sg.ID = ssi.CommonStoreFK

	--SELECT sso.*, ISNULL(sg.StoreName, 'No Store Name Selected') StoreName FROM Store_StockOut sso 
	--LEFT JOIN Store_General sg ON sg.ID = sso.CommonStoreFK
	--SELECT * FROM Merchandising_Style
	--SELECT * FROM Merchandising_StyleSlave mss
	--from t1 in _db.Merchandising_Style
 --                        join t2 in _db.Merchandising_BuyerOrder on t1.Merchandising_BuyerOrderFK equals t2.ID
 --                        join t3 in _db.Common_FinishItem on t1.Common_FinishItemFK equals t3.ID
	 
	
	SELECT ROW_NUMBER() OVER(ORDER BY cri.Name) AS ID, ( CAST(mbo.BuyerPO AS varchar(500)) +' '+ ms.CID + ' '+ cfi.Name) StyleID, cri.Name, ppos.PurchaseQuantity, (ssi.ReceivedQty * ppos.PurchasingPrice) PurchasingValue, ISNULL(ssi.ReceivedQty, 0) StoreReceivedQty, ISNULL(sso.ReceivedQty, 0) StoreOutQty 
	, ssi.StoreInName, sso.StoreOutName --, SUM(ssi.ReceivedQty * ppos.PurchasingPrice) TotalValue
	FROM Procurement_PurchaseOrderSlave ppos
	LEFT JOIN Common_RawItem cri ON cri.ID = ppos.Common_RawItemFK
	LEFT JOIN Merchandising_Style ms ON ms.ID = ppos.Merchandising_StyleID
	LEFT JOIN Merchandising_BuyerOrder mbo ON mbo.ID = ms.Merchandising_BuyerOrderFK
	LEFT JOIN Common_FinishItem cfi ON cfi.ID = ms.Common_FinishItemFK
	
	LEFT JOIN Store_StockIn ssi ON ssi.ItemIDFK = cri.ID
	LEFT JOIN Store_General sg ON sg.ID = ssi.CommonStoreFK
	LEFT JOIN Store_StockOut sso ON sso.ItemIDFK = cri.ID

	WHERE ppos.Merchandising_StyleID = @StyleID

END
GO
