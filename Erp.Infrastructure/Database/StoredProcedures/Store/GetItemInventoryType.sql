USE [ERP]
GO
/****** Object:  StoredProcedure [dbo].[GetItemInventoryType]    Script Date: 9/3/2019 12:49:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Mintu
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[GetItemInventoryType]
	
AS
BEGIN
		SELECT Max(sg.StoreName) StoreName,
		(CASE sg.Common_StoreTypeFK WHEN 1 THEN 'Row' WHEN 2 THEN 'WIP/Row' WHEN 3 THEN 'WIP' ELSE 'Finish' END) StoreType,
		(MAX(ri.Name) +'-( '+CAST( Common_RawItemFK AS VARCHAR)+' )') Name,  
		sg.Common_StoreTypeFK, Store_GeneralFK, Merchandising_StyleFK,  SUM(ReceivedQty) ReceivedQty, si.CommonUnitFK
		--Max(u.Name) Name, 
		FROM Store_StockIn si
		JOIN Store_General sg ON si.Store_GeneralFK = sg.ID	
		JOIN Common_RawItem ri ON si.Common_RawItemFK  = ri.ID
		--JOIN Common_Unit u ON si.CommonUnitFK = u.ID
		WHERE  si.Active = 1 AND sg.Active = 1 AND ri.Active = 1  --sg.ID = 1 AND
		GROUP BY Store_GeneralFK, Merchandising_StyleFK, Common_RawItemFK, sg.Common_StoreTypeFK, CommonUnitFK
END
