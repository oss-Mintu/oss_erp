CREATE NONCLUSTERED INDEX [NonClusteredIndex-20190528-121818] ON [dbo].[HRMS_AttendanceHistory]
(
	[HRMS_EmployeeFK] ASC,
	[Date] ASC,
	[AttendanceStatus] ASC
)
INCLUDE ( 	[TotalTime],
	[Late],
	[LeavePaidType],
	[PayableOverTime]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO


