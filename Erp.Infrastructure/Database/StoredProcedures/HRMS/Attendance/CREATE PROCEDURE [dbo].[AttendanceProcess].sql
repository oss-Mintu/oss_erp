CREATE PROCEDURE [dbo].[AttendanceProcess] 
@ProcessStartDate NVARCHAR(8)='',
@ProcessEndDate NVARCHAR(8)='',
@EmployeeID INT=0,
@ShiftID INT=0,
@UserID INT=0

AS 
BEGIN

	
	--DECLARE @ProcessStartDate NVARCHAR(8)='20181001',@ProcessEndDate NVARCHAR(8)='20181001',@EmployeeID INT=0,@ShiftID INT=0,@UserID INT=0

	IF @ProcessStartDate IS NULL OR @ProcessStartDate=''
	BEGIN
			SET @ProcessStartDate=CONVERT(NVARCHAR(8),GETDATE(),112)
	END
	IF @ProcessEndDate IS NULL OR @ProcessEndDate=''
	BEGIN
			SET @ProcessEndDate=CONVERT(NVARCHAR(8),GETDATE(),112)
	END
	IF @EmployeeID IS NULL
	BEGIN
			SET @EmployeeID=0
	END
	IF @ShiftID IS NULL
	BEGIN
			SET @ShiftID=0
	END
	IF @UserID IS NULL OR @UserID<=0
	BEGIN
			SET @UserID=1
	END


	PRINT 'ProcessStartDate '+ CAST(@ProcessStartDate AS NVARCHAR(11))
	PRINT 'ProcessEndDate '+ CAST(@ProcessEndDate AS NVARCHAR(11))
	PRINT 'START Process ' + CAST(CAST(GETDATE() AS TIME) AS NVARCHAR(MAX))

	DECLARE @ProcessDate NVARCHAR(8)--=CAST('2019-05-07' AS DATE)
	
	DELETE FROM HRMS_AttendanceHistory 
	WHERE (IsEdited IS NULL OR IsEdited=0)
	AND CAST([Date] AS DATE) BETWEEN @ProcessStartDate AND @ProcessEndDate 
	AND (CASE WHEN (@EmployeeID IS NULL OR @EmployeeID=0) OR @EmployeeID=HRMS_EmployeeFK THEN 1 ELSE 0 END)=1
	AND (CASE WHEN (@ShiftID IS NULL OR @ShiftID=0) OR @ShiftID=HRMS_ShiftFK THEN 1 ELSE 0 END)=1
	PRINT 'DELETE Previous Redundant Data ' + CAST(CAST(GETDATE() AS TIME) AS NVARCHAR(MAX))
	

	/*
	AttendanceStatus
	Present=1,
	Late=2,
	Absent=3,
	Leave=4,
	Holiday=5,
	OffDay=6,
	Weekend=7,
	HolidayPresent=8,
	WeekendPresent=9,
	OffDayPresent=10,
	LeavePresent=11,
	HolidayLate=12,
	WeekendLate=13,
	OffDayLate=14,
	LeaveLate=15,
	Invalid=20
	
	*/

	DECLARE @tblAttendance AS TABLE (ID INT IDENTITY(1,1)
									,ProcessDate NVARCHAR(8)
									,Employeefk INT
									,EmployeeCardNo NVARCHAR(MAX)
									,ShiftFK INT
									,ShiftName NVARCHAR(MAX)
									,ShiftBreakTime INT
									,ShiftStartTime NVARCHAR(23)
									,ShiftEndTime NVARCHAR(23)
									,IsLeave BIT
									,LeavePaidType INT
									,LeaveName NVARCHAR(MAX)
									,IsOffDay BIT
									,IsWeekend BIT
									,IsHoliday BIT
									,HolidayName NVARCHAR(MAX)
									,FirstPunch NVARCHAR(23) NULL
									,LastPunch NVARCHAR(23) NULL
									,TotalTime INT NULL
									,Late INT NULL
									,OverTimeMinute FLOAT NULL
									,PayableOverTimeHour FLOAT NULL
									,AttendanceStatus INT NULL
									,Remarks NVARCHAR(MAX) NULL)
	
	SET @ProcessDate=@ProcessStartDate
	
	
	
	DECLARE @ProcessYear INT=DATEPART(YEAR,@ProcessDate),@ProcessMonth INT=DATEPART(MONTH,@ProcessDate),@ProcessDay INT=DATEPART(DAY,@ProcessDate)

	WHILE CAST(@ProcessDate AS DATE)<=CAST(@ProcessEndDate AS DATE)
	BEGIN
			SET @ProcessYear =DATEPART(YEAR,@ProcessDate)
			SET @ProcessMonth =DATEPART(MONTH,@ProcessDate)
			SET @ProcessDay =DATEPART(DAY,@ProcessDate)
			
			
			INSERT INTO @TBLAttendance 
			(ProcessDate
			,Employeefk 
			,EmployeeCardNo
			,ShiftFK 
			,ShiftName 
			,ShiftBreakTime
			,ShiftStartTime
			,ShiftEndTime
			,IsLeave
			,IsOffDay
			,IsWeekend
			,IsHoliday
			,FirstPunch
			,LastPunch)
			SELECT	@ProcessDate
					,E.ID 
					,E.CardNo 
					,SH.ID
					,SH.ShiftName
					,ISNULL(SH.BreakTime,0)
					,CONVERT(NVARCHAR(23),DATETIMEFROMPARTS(@ProcessYear,@ProcessMonth,@ProcessDay,DATEPART(HOUR,CAST(SH.StartTime AS TIME)),DATEPART(MINUTE,CAST(SH.StartTime AS TIME)),0,0),126)
					,CONVERT(NVARCHAR(23),DATETIMEFROMPARTS(@ProcessYear,@ProcessMonth,@ProcessDay,DATEPART(HOUR,CAST(SH.EndTime AS TIME)),DATEPART(MINUTE,CAST(SH.EndTime AS TIME)),0,0),126)
					,(SELECT 1 WHERE EXISTS(SELECT * FROM HRMS_LeaveApplication LA WHERE LA.Active=1 AND LA.HRMS_EmployeeFK=E.ID AND LA.LeaveDeptStatus=5 AND LA.LeaveStatus=2 AND @ProcessDate BETWEEN CAST(LA.[From] AS DATE) AND CAST(LA.[To] AS DATE)))
					,(SELECT 1 WHERE EXISTS(SELECT * FROM HRMS_OffDay OD WHERE OD.Active=1 AND OD.ApplicableDate=@ProcessDate))
					,(SELECT 1 WHERE EXISTS(SELECT * FROM HRMS_EmployeeWeekendAssign WA LEFT OUTER JOIN HRMS_EmployeeWeekend W ON WA.ID=W.HRMS_EmployeeWeekendAssignFK WHERE WA.Active=1 AND WA.HRMS_EmployeeFK=E.ID AND W.WeekendDay=DATENAME(WEEKDAY,@ProcessDate)))
					,(SELECT 1 WHERE EXISTS(SELECT * FROM HRMS_Holiday H WHERE H.Active=1 AND @ProcessDate BETWEEN CAST(H.FromDate AS DATE) AND CAST(H.ToDate AS DATE)))
					,CONVERT(NVARCHAR(23),(SELECT MIN(IOS.CheckTime) FROM HRMS_CheckInOuts IOS WHERE IOS.CardNo=E.CardNo AND CAST(IOS.CheckTime AS DATE)=@ProcessDate AND IOS.CheckTime BETWEEN CONVERT(NVARCHAR(23),DATEADD(HOUR,-2,DATETIMEFROMPARTS(@ProcessYear,@ProcessMonth,@ProcessDay,DATEPART(HOUR,CAST(SH.StartTime AS TIME)),DATEPART(MINUTE,CAST(SH.StartTime AS TIME)),0,0)),126) AND CONVERT(NVARCHAR(23),DATEADD(HOUR,+22,DATETIMEFROMPARTS(@ProcessYear,@ProcessMonth,@ProcessDay,DATEPART(HOUR,CAST(SH.StartTime AS TIME)),DATEPART(MINUTE,CAST(SH.StartTime AS TIME)),0,0)),126) GROUP BY IOS.CardNo,CAST(IOS.CheckTime AS DATE)),126)
					,CONVERT(NVARCHAR(23),(SELECT MAX(IOS.CheckTime) FROM HRMS_CheckInOuts IOS WHERE IOS.CardNo=E.CardNo AND CAST(IOS.CheckTime AS DATE)=@ProcessDate AND IOS.CheckTime BETWEEN CONVERT(NVARCHAR(23),DATEADD(HOUR,-2,DATETIMEFROMPARTS(@ProcessYear,@ProcessMonth,@ProcessDay,DATEPART(HOUR,CAST(SH.StartTime AS TIME)),DATEPART(MINUTE,CAST(SH.StartTime AS TIME)),0,0)),126) AND CONVERT(NVARCHAR(23),DATEADD(HOUR,+22,DATETIMEFROMPARTS(@ProcessYear,@ProcessMonth,@ProcessDay,DATEPART(HOUR,CAST(SH.StartTime AS TIME)),DATEPART(MINUTE,CAST(SH.StartTime AS TIME)),0,0)),126) GROUP BY IOS.CardNo,CAST(IOS.CheckTime AS DATE)),126)
					
			FROM HRMS_ShiftAssign SA
			LEFT OUTER JOIN HRMS_Employee E ON SA.HRMS_EmployeeFK = E.ID  
			LEFT OUTER JOIN HRMS_Shift SH ON SA.HRMS_ShiftFK = SH.ID
			LEFT OUTER JOIN (	SELECT AH.Date,AH.HRMS_EmployeeFK,AH.HRMS_ShiftFK 
								FROM HRMS_AttendanceHistory AH
								WHERE (AH.IsEdited IS NOT NULL AND AH.IsEdited=1)
								AND CAST(AH.[Date] AS DATE) BETWEEN @ProcessStartDate AND @ProcessEndDate 
								AND (CASE WHEN (@EmployeeID IS NULL OR @EmployeeID=0) OR @EmployeeID=AH.HRMS_EmployeeFK THEN 1 ELSE 0 END)=1
								AND (CASE WHEN (@ShiftID IS NULL OR @ShiftID=0) OR @ShiftID=AH.HRMS_ShiftFK THEN 1 ELSE 0 END)=1) 
								TT ON TT.[Date]=SA.AssignedDate AND TT.HRMS_EmployeeFK=SA.HRMS_EmployeeFK AND TT.HRMS_ShiftFK=SA.HRMS_ShiftFK
			WHERE CAST(SA.AssignedDate AS DATE)=@ProcessDate 
			AND (CASE WHEN (@EmployeeID IS NULL OR @EmployeeID=0) OR @EmployeeID=E.ID THEN 1 ELSE 0 END)=1
			AND (CASE WHEN (@ShiftID IS NULL OR @ShiftID=0) OR @ShiftID=SH.ID THEN 1 ELSE 0 END)=1
			AND TT.[Date] IS NULL AND TT.HRMS_EmployeeFK IS NULL AND TT.HRMS_ShiftFK IS NULL
	
			
			PRINT 'INSERT Into Temp Table For ' + CAST(@ProcessDate AS NVARCHAR(11)) +' '+ CAST(CAST(GETDATE() AS TIME) AS NVARCHAR(MAX))
			SET @ProcessDate= CONVERT(NVARCHAR(8),DATEADD(DAY,1,@ProcessDate),112)
	END

	
	--LEAVE PRIORITY. IF LEAVE THEN IsHoliday=0,IsWeekend=0,IsOffDay=0
	UPDATE @tblAttendance SET	LeaveName=ISNULL(LA.Name,'')
								,LeavePaidType=LA.LeavePaidType
								,IsHoliday=0
								,IsWeekend=0
								,IsOffDay=0
	FROM HRMS_LeaveApplication LA 
	WHERE IsLeave IS NOT NULL AND IsLeave=1
	AND Employeefk=LA.HRMS_EmployeeFK
	AND LA.LeaveStatus=1 
	AND ProcessDate BETWEEN CAST(LA.[From] AS DATE) AND CAST(LA.[To] AS DATE)
	PRINT 'UPDATE LeaveName,LeavePaidType,IsHoliday,IsWeekend,IsOffDay ' + CAST(CAST(GETDATE() AS TIME) AS NVARCHAR(MAX))
	
	UPDATE @tblAttendance SET	HolidayName=ISNULL(H.HoildayName,'')

	FROM HRMS_Holiday H
	WHERE IsHoliday IS NOT NULL AND IsHoliday=1
	AND ProcessDate BETWEEN CAST(H.FromDate AS DATE) AND CAST(H.ToDate AS DATE)
	PRINT 'UPDATE HolidayName ' + CAST(CAST(GETDATE() AS TIME) AS NVARCHAR(MAX))
	
	
	UPDATE @tblAttendance SET	TotalTime=CASE	WHEN TBL.FirstPunch IS NOT NULL THEN	CASE	WHEN	TBL.FirstPunch<TBL.ShiftStartTime 
																								THEN	(DATEDIFF(MINUTE,TBL.ShiftStartTime,TBL.LastPunch)-TBL.ShiftBreakTime)/60 
																								WHEN	TBL.FirstPunch>=TBL.ShiftStartTime 
																								THEN	(DATEDIFF(MINUTE,TBL.FirstPunch,TBL.LastPunch)-TBL.ShiftBreakTime)/60  
																								ELSE 0
																								END
												ELSE 0
												END
								,Late=CASE	WHEN TBL.FirstPunch IS NOT NULL AND DATEDIFF(MINUTE,TBL.ShiftStartTime,TBL.FirstPunch)>0 THEN DATEDIFF(MINUTE,TBL.ShiftStartTime,TBL.FirstPunch)
											ELSE 0
											END
								,OverTimeMinute=CASE	WHEN (TBL.IsWeekend IS NOT NULL AND TBL.IsWeekend=1) OR (TBL.IsOffDay IS NOT NULL AND TBL.IsOffDay=1) OR (TBL.IsHoliday IS NOT NULL AND TBL.IsHoliday=1)
														THEN	CASE	WHEN TBL.FirstPunch IS NOT NULL THEN	CASE	WHEN	TBL.FirstPunch<TBL.ShiftStartTime 
																														THEN	(DATEDIFF(MINUTE,TBL.ShiftStartTime,TBL.LastPunch)-TBL.ShiftBreakTime) 
																														WHEN	TBL.FirstPunch>=TBL.ShiftStartTime 
																														THEN	(DATEDIFF(MINUTE,TBL.FirstPunch,TBL.LastPunch)-TBL.ShiftBreakTime) 
																														ELSE 0
																														END
																ELSE 0
																END
														ELSE	CASE	WHEN TBL.LastPunch IS NOT NULL AND DATEDIFF(MINUTE,TBL.ShiftEndTime,TBL.LastPunch)>0 THEN DATEDIFF(MINUTE,TBL.ShiftEndTime,TBL.LastPunch)
																		ELSE 0
																		END
														END
	FROM @tblAttendance TBL WHERE TBL.ID=ID
	PRINT 'UPDATE TotalTime,Late,OverTimeMinute ' + CAST(CAST(GETDATE() AS TIME) AS NVARCHAR(MAX))

	
	UPDATE @tblAttendance SET	PayableOverTimeHour=CASE	WHEN TBL.OverTimeMinute IS NOT NULL AND TBL.OverTimeMinute>29 THEN ROUND((TBL.OverTimeMinute/60),0)
															ELSE 0
															END
								,AttendanceStatus=CASE	WHEN TBL.FirstPunch IS NOT NULL 
														THEN	CASE	WHEN TBL.Late IS NOT NULL AND TBL.Late>0 
																		THEN CASE	WHEN TBL.IsLeave IS NOT NULL AND TBL.IsLeave=1 THEN 15
																					WHEN TBL.IsOffDay IS NOT NULL AND TBL.IsOffDay=1 THEN 14
																					WHEN TBL.IsWeekend IS NOT NULL AND TBL.IsWeekend=1 THEN 13
																					WHEN TBL.IsHoliday IS NOT NULL AND TBL.IsHoliday=1 THEN 12
																					ELSE 2
																					END
																		WHEN TBL.Late IS NULL OR TBL.Late<=0 
																		THEN CASE	WHEN TBL.IsLeave IS NOT NULL AND TBL.IsLeave=1 THEN 11
																					WHEN TBL.IsOffDay IS NOT NULL AND TBL.IsOffDay=1 THEN 10
																					WHEN TBL.IsWeekend IS NOT NULL AND TBL.IsWeekend=1 THEN 9
																					WHEN TBL.IsHoliday IS NOT NULL AND TBL.IsHoliday=1 THEN 8
																					ELSE 1
																					END
																		ELSE 20
																		END
														ELSE	CASE	WHEN TBL.IsLeave IS NOT NULL AND TBL.IsLeave=1 THEN 4
																		WHEN TBL.IsOffDay IS NOT NULL AND TBL.IsOffDay=1 THEN 6
																		WHEN TBL.IsWeekend IS NOT NULL AND TBL.IsWeekend=1 THEN 7
																		WHEN TBL.IsHoliday IS NOT NULL AND TBL.IsHoliday=1 THEN 5
																		ELSE 3
																		END
														END
								,Remarks=CASE	WHEN TBL.FirstPunch IS NOT NULL 
												THEN	CASE	WHEN TBL.Late IS NOT NULL AND TBL.Late>0 
																THEN CASE	WHEN TBL.IsLeave IS NOT NULL AND TBL.IsLeave=1 THEN 'Late, '+TBL.LeaveName
																			WHEN TBL.IsOffDay IS NOT NULL AND TBL.IsOffDay=1 THEN 'Late, OffDay'
																			WHEN TBL.IsWeekend IS NOT NULL AND TBL.IsWeekend=1 THEN 'Late, Weekend'
																			WHEN TBL.IsHoliday IS NOT NULL AND TBL.IsHoliday=1 THEN 'Late, '+TBL.HolidayName
																			ELSE 'Late'
																			END
																WHEN TBL.Late IS NULL OR TBL.Late<=0 
																THEN CASE	WHEN TBL.IsLeave IS NOT NULL AND TBL.IsLeave=1 THEN 'OK, '+TBL.LeaveName
																			WHEN TBL.IsOffDay IS NOT NULL AND TBL.IsOffDay=1 THEN 'OK, OffDay'
																			WHEN TBL.IsWeekend IS NOT NULL AND TBL.IsWeekend=1 THEN 'OK, Weekend'
																			WHEN TBL.IsHoliday IS NOT NULL AND TBL.IsHoliday=1 THEN 'OK, '+TBL.HolidayName
																			ELSE 'OK'
																			END
																ELSE 'Invalid'
																END
												ELSE	CASE	WHEN TBL.IsLeave IS NOT NULL AND TBL.IsLeave=1 THEN TBL.LeaveName
																WHEN TBL.IsOffDay IS NOT NULL AND TBL.IsOffDay=1 THEN 'OffDay'
																WHEN TBL.IsWeekend IS NOT NULL AND TBL.IsWeekend=1 THEN 'Weekend'
																WHEN TBL.IsHoliday IS NOT NULL AND TBL.IsHoliday=1 THEN TBL.HolidayName
																ELSE 'Absent'
																END
												END
																		
	FROM @tblAttendance TBL WHERE TBL.ID=ID
	PRINT 'UPDATE PayableOverTimeHour,AttendanceStatus,Remarks ' + CAST(CAST(GETDATE() AS TIME) AS NVARCHAR(MAX))
	

	BEGIN TRY
	BEGIN TRANSACTION
						
				
				INSERT INTO [dbo].[HRMS_AttendanceHistory]
					   ([User]
					   ,[Active]
					   ,[Time]
					   ,[HRMS_EmployeeFK]
					   ,[Date]
					   ,[InTime]
					   ,[OutTime]
					   ,[TotalTime]
					   ,[Late]
					   ,[OverTime]
					   ,[AttendanceStatus]
					   ,[CardNo]
					   ,[Remarks]
					   ,[HRMS_ShiftFK]
					   ,[LeavePaidType]
					   ,[EntryDate]
					   ,[EntryByUserFK]
					   ,[PayableOverTime]
					   ,[UpdateDate])
				SELECT	'Admin'
						,1
						,GETDATE()
						,TBL.Employeefk
						,TBL.ProcessDate
						,ISNULL(TBL.FirstPunch,'19000101')
						,ISNULL(TBL.LastPunch,'19000101')
						,ISNULL(TBL.TotalTime,0)
						,ISNULL(TBL.Late,0)
						,ISNULL(TBL.OverTimeMinute,0)
						,ISNULL(TBL.AttendanceStatus,20)
						,TBL.EmployeeCardNo
						,TBL.Remarks
						,TBL.ShiftFK
						,TBL.LeavePaidType
						,GETDATE()
						,1
						,ISNULL(TBL.PayableOverTimeHour,0)
						,GETDATE()

				FROM @tblAttendance TBL 
				
				PRINT 'Final Insert Into HRMS_AttendanceHistory ' + CAST(CAST(GETDATE() AS TIME) AS NVARCHAR(MAX))		
				PRINT 'END ' + CAST(CAST(GETDATE() AS TIME) AS NVARCHAR(MAX))

	COMMIT TRANSACTION
	END TRY
	BEGIN CATCH 
				IF (@@TRANCOUNT > 0)
				BEGIN
						ROLLBACK TRANSACTION 
						PRINT 'Error detected, all changes reversed'
				END 
				   
				DECLARE @ErrorNumber INT=ERROR_NUMBER()
						,@ErrorSeverity INT=ERROR_SEVERITY()
						,@ErrorLine INT=ERROR_LINE()
						,@ErrorMessage NVARCHAR(MAX)=ERROR_MESSAGE() 
						,@ERROR_STATE INT=ERROR_STATE()
				
				RAISERROR(@ErrorMessage,@ErrorSeverity,@ERROR_STATE)

	END CATCH
END
GO


