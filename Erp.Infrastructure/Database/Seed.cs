﻿using Microsoft.Extensions.DependencyInjection;
using System.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using Erp.Core.Entity.Accounting;
using Erp.Core.Entity.MIS;
using Erp.Core.Entity.Common;
using Erp.Core.Entity.User;
using Erp.Core.Services.Commercial;
using Erp.Core.Entity.Merchandising;
using Erp.Core.Entity.Production;
using Erp.Core.Entity.HRMS;
using Erp.Core.Entity.Store;
using Erp.Core.Entity.Payroll;

namespace Erp.Infrastructure.Database
{
    public static class Seed
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            var context = serviceProvider.GetRequiredService<InfrastructureDbContext>();
            context.Database.EnsureCreated();

            //Do not Delete this seed process. Never ever

            if (!context.User_Role.Any())
            {
                context.User_Role.Add(new User_Role() { Active = true, User = "OneUser", Time = DateTime.Now, Name = "Admin" });

                context.SaveChanges();
            }
            if (!context.User_AccessLevel.Any())
            {
                context.User_AccessLevel.Add(new User_AccessLevel() { Active = true, User = "User", Time = DateTime.Now, Name = "Basic" });
                context.User_AccessLevel.Add(new User_AccessLevel() { Active = true, User = "User", Time = DateTime.Now, Name = "Ultimate" });


                context.SaveChanges();
            }
            if (!context.User_Department.Any())
            {
                context.User_Department.Add(new User_Department() { Name = "Admin" });
                context.User_Department.Add(new User_Department() { Name = "Commercial" });
                context.User_Department.Add(new User_Department() { Name = "Procurement" });
                context.User_Department.Add(new User_Department() { Name = "Merchandising" });
                context.User_Department.Add(new User_Department() { Name = "Sewing" });
                context.User_Department.Add(new User_Department() { Name = "Construction" });
                context.User_Department.Add(new User_Department() { Name = "Accounts" });
                context.User_Department.Add(new User_Department() { Name = "Cutting" });
                context.SaveChanges();
            }
            if (!context.User_User.Any())
            {
                context.User_User.Add(new User_User()
                {
                    Active = true,
                    User = "OneUser",
                    Time = DateTime.Now,
                    Name = "Admin",
                    UserName = "Admin",
                    Password = "96e79218965eb72c92a549dd5a330112",
                    User_AccessLevelFK = 1,
                    User_RoleFK = 1,
                    Mobile = "09876543212",
                    Email = "mail@mail.com",
                    Locked = false,
                    Address = "Dhaka",
                    User_DepartmentFK = 1
                });

                context.SaveChanges();
            }
            if (!context.User_Menu.Any())
            {
                context.User_Menu.Add(new User_Menu() { Name = "User" });

                context.SaveChanges();
            }
            if (!context.User_SubMenu.Any())
            {
                context.User_SubMenu.Add(new User_SubMenu() { Name = "User", User_MenuFk = 1 });

                context.SaveChanges();
            }
            if (!context.User_MenuItem.Any())
            {
                context.User_MenuItem.Add(new User_MenuItem() { Priority = 4, Name = "User List", Method = "User/UserList", IsAlone = false, User_SubMenuFk = 1 });
                context.User_MenuItem.Add(new User_MenuItem() { Priority = 1, Name = "Department", Method = "User/UserDepartment", IsAlone = false, User_SubMenuFk = 1 });
                context.User_MenuItem.Add(new User_MenuItem() { Priority = 2, Name = "Access Level", Method = "User/UserAccessLevel", IsAlone = false, User_SubMenuFk = 1 });
                context.User_MenuItem.Add(new User_MenuItem() { Priority = 3, Name = "Role", Method = "User/UserRole", IsAlone = false, User_SubMenuFk = 1 });
                context.User_MenuItem.Add(new User_MenuItem() { Priority = 5, Name = "Menu", Method = "User/UserMenu", IsAlone = false, User_SubMenuFk = 1 });
                context.User_MenuItem.Add(new User_MenuItem() { Priority = 6, Name = "Submenu", Method = "User/UserSubMenu", IsAlone = false, User_SubMenuFk = 1 });
                context.User_MenuItem.Add(new User_MenuItem() { Priority = 7, Name = "Menu Item", Method = "User/UserMenuItem", IsAlone = false, User_SubMenuFk = 1 });
                context.User_MenuItem.Add(new User_MenuItem() { Priority = 8, Name = "Role Menu Item", Method = "User/UserRoleMenuItem", IsAlone = true, User_SubMenuFk = 1 });
                context.User_MenuItem.Add(new User_MenuItem() { Priority = 9, Name = "User Update", Method = "User/UserUpdate", IsAlone = true, User_SubMenuFk = 1 });

                context.SaveChanges();
            }

            if (!context.User_RoleMenuItem.Any())
            {
                context.User_RoleMenuItem.Add(new User_RoleMenuItem() { User_MenuItemFk = 1, IsAllowed = true, User_RoleFK = 1 });
                context.User_RoleMenuItem.Add(new User_RoleMenuItem() { User_MenuItemFk = 2, IsAllowed = true, User_RoleFK = 1 });
                context.User_RoleMenuItem.Add(new User_RoleMenuItem() { User_MenuItemFk = 3, IsAllowed = true, User_RoleFK = 1 });
                context.User_RoleMenuItem.Add(new User_RoleMenuItem() { User_MenuItemFk = 4, IsAllowed = true, User_RoleFK = 1 });
                context.User_RoleMenuItem.Add(new User_RoleMenuItem() { User_MenuItemFk = 5, IsAllowed = true, User_RoleFK = 1 });
                context.User_RoleMenuItem.Add(new User_RoleMenuItem() { User_MenuItemFk = 6, IsAllowed = true, User_RoleFK = 1 });
                context.User_RoleMenuItem.Add(new User_RoleMenuItem() { User_MenuItemFk = 7, IsAllowed = true, User_RoleFK = 1 });
                context.User_RoleMenuItem.Add(new User_RoleMenuItem() { User_MenuItemFk = 8, IsAllowed = true, User_RoleFK = 1 });
                context.User_RoleMenuItem.Add(new User_RoleMenuItem() { User_MenuItemFk = 9, IsAllowed = true, User_RoleFK = 1 });
                context.SaveChanges();
            }

            if (!context.Accounting_Type.Any())
            {
                context.Accounting_Type.Add(new Accounting_Type() { Name = "ASSETS", DrIncrease = true, ID = 1 });
                context.Accounting_Type.Add(new Accounting_Type() { Name = "LIABILITIES", DrIncrease = false, ID = 2 });
                context.Accounting_Type.Add(new Accounting_Type() { Name = "OWNER'S EQUITY", DrIncrease = false, ID = 3 });
                context.Accounting_Type.Add(new Accounting_Type() { Name = "INCOME", DrIncrease = false, ID = 4 });
                context.Accounting_Type.Add(new Accounting_Type() { Name = "EXPENSES", DrIncrease = true, ID = 5 });
                context.SaveChanges();
            }

            if (!context.Accounting_Chart1.Any())
            {
                context.Accounting_Chart1.Add(new Accounting_Chart1() { Name = "Investment", Accounting_TypeFK = 1 });
                context.Accounting_Chart1.Add(new Accounting_Chart1() { Name = "Current", Accounting_TypeFK = 1 });
                context.Accounting_Chart1.Add(new Accounting_Chart1() { Name = "Fixed", Accounting_TypeFK = 1 });
                context.Accounting_Chart1.Add(new Accounting_Chart1() { Name = "Share Capital", Accounting_TypeFK = 3 });
                context.Accounting_Chart1.Add(new Accounting_Chart1() { Name = "Reserve & Surplus", Accounting_TypeFK = 3 });
                context.Accounting_Chart1.Add(new Accounting_Chart1() { Name = "Long-Term Liabilities", Accounting_TypeFK = 2 });
                context.Accounting_Chart1.Add(new Accounting_Chart1() { Name = "Current Liabilities", Accounting_TypeFK = 2 });
                context.Accounting_Chart1.Add(new Accounting_Chart1() { Name = "Revenue Income", Accounting_TypeFK = 4 });
                context.Accounting_Chart1.Add(new Accounting_Chart1() { Name = "Others Income", Accounting_TypeFK = 4 });
                context.Accounting_Chart1.Add(new Accounting_Chart1() { Name = "Financial Overhead", Accounting_TypeFK = 4 });
                context.Accounting_Chart1.Add(new Accounting_Chart1() { Name = "Selling & Distribution Overhead", Accounting_TypeFK = 4 });

                context.Accounting_Chart1.Add(new Accounting_Chart1() { Name = "Administrative Overhead", Accounting_TypeFK = 5 });
                context.Accounting_Chart1.Add(new Accounting_Chart1() { Name = "Factory Overhead", Accounting_TypeFK = 5 });

                context.SaveChanges();
            }

            if (!context.Accounting_Chart2.Any())
            {
                context.Accounting_Chart2.Add(new Accounting_Chart2() { Name = "Investment in Share", Accounting_Chart1FK = 1 });
                context.Accounting_Chart2.Add(new Accounting_Chart2() { Name = "Land and Land Development", Accounting_Chart1FK = 2 });
                context.Accounting_Chart2.Add(new Accounting_Chart2() { Name = "Cash in Hand", Accounting_Chart1FK = 2 });
                context.Accounting_Chart2.Add(new Accounting_Chart2() { Name = "Cash at Bank", Accounting_Chart1FK = 2 });
                context.Accounting_Chart2.Add(new Accounting_Chart2() { Name = "Accounts Receivable", Accounting_Chart1FK = 4 });

                context.SaveChanges();
            }
            if (!context.Accounting_Head.Any())
            {
                context.Accounting_Head.Add(new Accounting_Head() { Name = "Cash HQ", Accounting_Chart2FK = 3 });
                context.Accounting_Head.Add(new Accounting_Head() { Name = "Petty Cash", Accounting_Chart2FK = 2 });
                context.Accounting_Head.Add(new Accounting_Head() { Name = "Cash at Bank", Accounting_Chart2FK = 2 });
                context.Accounting_Head.Add(new Accounting_Head() { Name = "Accounts Receivable", Accounting_Chart2FK = 3 });

                context.Accounting_Head.Add(new Accounting_Head() { Name = "Accounts Receivable", Accounting_Chart2FK = 3 });

                context.SaveChanges();
            }

            if (!context.Common_Supplier.Any())
            {
                context.Common_Supplier.Add(new Common_Supplier() { Name = "E R ACCESSORIES" });
                context.Common_Supplier.Add(new Common_Supplier() { Name = "PAXER BANGLADESH LTD" });
                context.Common_Supplier.Add(new Common_Supplier() { Name = "Uniglory LTD" });
                context.Common_Supplier.Add(new Common_Supplier() { Name = "Paxar Bangladesh Ltd" });
                context.Common_Supplier.Add(new Common_Supplier() { Name = "FOTRUST CO., LTD" });
                context.Common_Supplier.Add(new Common_Supplier() { Name = "HRS ENTERPRISE" });
                context.Common_Supplier.Add(new Common_Supplier() { Name = "LE NOUVEA TEX" });
                context.Common_Supplier.Add(new Common_Supplier() { Name = "Nusrat Trading Co." });
                context.SaveChanges();
            }
            if (!context.Common_Buyer.Any())
            {
                context.Common_Buyer.Add(new Common_Buyer() { Name = "Multifabs Ltd" });
                context.Common_Buyer.Add(new Common_Buyer() { Name = "NAVIGARE" });
                context.Common_Buyer.Add(new Common_Buyer() { Name = "CALVIN KLEIN" });
                context.Common_Buyer.Add(new Common_Buyer() { Name = "LF CREDIT PTE. LTD" });
                context.Common_Buyer.Add(new Common_Buyer() { Name = "PRIMARK LIMITED" });
                context.Common_Buyer.Add(new Common_Buyer() { Name = "LF CREDIT PTE. LTD" });
                context.Common_Buyer.Add(new Common_Buyer() { Name = "HIRAKI CO.,LTD" });
                context.Common_Buyer.Add(new Common_Buyer() { Name = "NETTO" });
                context.SaveChanges();
            }

            if (!context.Merchandising_BuyerOrder.Any())
            {
                context.Merchandising_BuyerOrder.Add(new Merchandising_BuyerOrder() { Common_BuyerFK = 1, BuyerPO = "P987674" });
                context.Merchandising_BuyerOrder.Add(new Merchandising_BuyerOrder() { Common_BuyerFK = 2, BuyerPO = "ST5434" });
                context.Merchandising_BuyerOrder.Add(new Merchandising_BuyerOrder() { Common_BuyerFK = 3, BuyerPO = "TU9876" });
                context.Merchandising_BuyerOrder.Add(new Merchandising_BuyerOrder() { Common_BuyerFK = 4, BuyerPO = "P987674" });
                context.Merchandising_BuyerOrder.Add(new Merchandising_BuyerOrder() { Common_BuyerFK = 2, BuyerPO = "ST5434" });
                context.Merchandising_BuyerOrder.Add(new Merchandising_BuyerOrder() { Common_BuyerFK = 3, BuyerPO = "TU9876" });
                context.SaveChanges();
            }
            if (!context.User_Department.Any())
            {
                context.User_Department.Add(new User_Department() { Name = "Admin" });
                context.User_Department.Add(new User_Department() { Name = "Commercial" });
                context.User_Department.Add(new User_Department() { Name = "Procurement" });
                context.User_Department.Add(new User_Department() { Name = "Marchendising" });
                context.User_Department.Add(new User_Department() { Name = "Sewing" });
                context.User_Department.Add(new User_Department() { Name = "Constraction" });
                context.User_Department.Add(new User_Department() { Name = "Accounts" });
                context.User_Department.Add(new User_Department() { Name = "Commercial" });
                context.SaveChanges();
            }

            if (!context.Common_Currency.Any())
            {
                context.Common_Currency.Add(new Common_Currency() { Name = "Taka" });
                context.Common_Currency.Add(new Common_Currency() { Name = "Dollar" });
                context.Common_Currency.Add(new Common_Currency() { Name = "Rupee" });
                context.Common_Currency.Add(new Common_Currency() { Name = "Yen" });
                context.Common_Currency.Add(new Common_Currency() { Name = "Euro" });
                context.Common_Currency.Add(new Common_Currency() { Name = "Ruble" });
                context.SaveChanges();
            }
            if (!context.Common_Unit.Any())
            {
                context.Common_Unit.Add(new Common_Unit() { Name = "Pound" });
                context.Common_Unit.Add(new Common_Unit() { Name = "Kilogram" });
                context.SaveChanges();
            }
            if (!context.Common_RawCategory.Any())
            {
                context.Common_RawCategory.Add(new Common_RawCategory() { Name = "Yarn" });
                context.Common_RawCategory.Add(new Common_RawCategory() { Name = "Fabric" });
                context.Common_RawCategory.Add(new Common_RawCategory() { Name = "Wash" });
                context.Common_RawCategory.Add(new Common_RawCategory() { Name = "Print" });
                context.Common_RawCategory.Add(new Common_RawCategory() { Name = "Embroidery" });
                context.Common_RawCategory.Add(new Common_RawCategory() { Name = "Trims and Accessories" });
                context.Common_RawCategory.Add(new Common_RawCategory() { Name = "Carton & Others" });
                context.Common_RawCategory.Add(new Common_RawCategory() { Name = "Electric" });
                context.SaveChanges();
            }

            if (!context.Common_RawSubCategory.Any())
            {
                //Yarn
                context.Common_RawSubCategory.Add(new Common_RawSubCategory() { Name = "100% Cotton", Common_RawCategoryFK = 1 });
                context.Common_RawSubCategory.Add(new Common_RawSubCategory() { Name = "CVC 60% Cotton 40% Polyester", Common_RawCategoryFK = 1 });
                context.Common_RawSubCategory.Add(new Common_RawSubCategory() { Name = "65%Polyester 35% Cotton", Common_RawCategoryFK = 1 });
                context.Common_RawSubCategory.Add(new Common_RawSubCategory() { Name = "100% Viscose Yarn", Common_RawCategoryFK = 1 });
                context.Common_RawSubCategory.Add(new Common_RawSubCategory() { Name = "100% Cotton Slub yarn", Common_RawCategoryFK = 1 });
                context.Common_RawSubCategory.Add(new Common_RawSubCategory() { Name = "100% Viscose Slub Yarn", Common_RawCategoryFK = 1 });
                context.Common_RawSubCategory.Add(new Common_RawSubCategory() { Name = "Indian Yarn", Common_RawCategoryFK = 1 });
                //Fabric
                context.Common_RawSubCategory.Add(new Common_RawSubCategory() { Name = "100 % Cotton/Jersey", Common_RawCategoryFK = 2 });
                context.Common_RawSubCategory.Add(new Common_RawSubCategory() { Name = "100 % Cotton Rib 1x1", Common_RawCategoryFK = 2 });
                context.Common_RawSubCategory.Add(new Common_RawSubCategory() { Name = "100 % Cotton Rib 2x2", Common_RawCategoryFK = 2 });
                context.Common_RawSubCategory.Add(new Common_RawSubCategory() { Name = "100 % Cotton Pique", Common_RawCategoryFK = 2 });
                context.Common_RawSubCategory.Add(new Common_RawSubCategory() { Name = "95% Cotton 5% Lycra", Common_RawCategoryFK = 2 });
                context.Common_RawSubCategory.Add(new Common_RawSubCategory() { Name = "102 % Cotton Interlock", Common_RawCategoryFK = 2 });
                //Wash
                context.Common_RawSubCategory.Add(new Common_RawSubCategory() { Name = "Normal wash", Common_RawCategoryFK = 3 });
                context.Common_RawSubCategory.Add(new Common_RawSubCategory() { Name = "Pigment wash", Common_RawCategoryFK = 3 });
                context.Common_RawSubCategory.Add(new Common_RawSubCategory() { Name = "Bleach wash", Common_RawCategoryFK = 3 });
                context.Common_RawSubCategory.Add(new Common_RawSubCategory() { Name = "Stone wash with or without bleach", Common_RawCategoryFK = 3 });
                context.Common_RawSubCategory.Add(new Common_RawSubCategory() { Name = "Acid wash", Common_RawCategoryFK = 3 });
                context.Common_RawSubCategory.Add(new Common_RawSubCategory() { Name = "Caustic wash", Common_RawCategoryFK = 3 });
                context.Common_RawSubCategory.Add(new Common_RawSubCategory() { Name = "Garment wash and over-dye", Common_RawCategoryFK = 3 });
                context.Common_RawSubCategory.Add(new Common_RawSubCategory() { Name = "Whitening", Common_RawCategoryFK = 3 });
                //Print
                context.Common_RawSubCategory.Add(new Common_RawSubCategory() { Name = "Hand block printing", Common_RawCategoryFK = 4 });
                context.Common_RawSubCategory.Add(new Common_RawSubCategory() { Name = "Perrotine printing", Common_RawCategoryFK = 4 });
                context.Common_RawSubCategory.Add(new Common_RawSubCategory() { Name = "Engraved copperplate printing", Common_RawCategoryFK = 4 });
                context.Common_RawSubCategory.Add(new Common_RawSubCategory() { Name = "Roller, Cylinder, or machine printing", Common_RawCategoryFK = 4 });
                context.Common_RawSubCategory.Add(new Common_RawSubCategory() { Name = "Stencil printing", Common_RawCategoryFK = 4 });
                context.Common_RawSubCategory.Add(new Common_RawSubCategory() { Name = "Screen printing", Common_RawCategoryFK = 4 });
                context.Common_RawSubCategory.Add(new Common_RawSubCategory() { Name = "Digital textile printing", Common_RawCategoryFK = 4 });
                context.Common_RawSubCategory.Add(new Common_RawSubCategory() { Name = "Flexo textile printing", Common_RawCategoryFK = 4 });
                context.Common_RawSubCategory.Add(new Common_RawSubCategory() { Name = "Discharge Printing", Common_RawCategoryFK = 4 });
                //Trims and Accessories
                context.Common_RawSubCategory.Add(new Common_RawSubCategory() { Name = "Poly Bag", Common_RawCategoryFK = 6 });
                context.Common_RawSubCategory.Add(new Common_RawSubCategory() { Name = "Down", Common_RawCategoryFK = 6 });
                context.Common_RawSubCategory.Add(new Common_RawSubCategory() { Name = "Piping Cord", Common_RawCategoryFK = 6 });
                context.Common_RawSubCategory.Add(new Common_RawSubCategory() { Name = "Emblem", Common_RawCategoryFK = 6 });
                context.Common_RawSubCategory.Add(new Common_RawSubCategory() { Name = "Logo Print", Common_RawCategoryFK = 6 });
                context.Common_RawSubCategory.Add(new Common_RawSubCategory() { Name = "Swivel Hook", Common_RawCategoryFK = 6 });
                context.Common_RawSubCategory.Add(new Common_RawSubCategory() { Name = "Eyelet/ Grommet", Common_RawCategoryFK = 6 });
                context.Common_RawSubCategory.Add(new Common_RawSubCategory() { Name = "Collar Stay", Common_RawCategoryFK = 6 });
                context.Common_RawSubCategory.Add(new Common_RawSubCategory() { Name = "Cord Bell", Common_RawCategoryFK = 6 });
                context.Common_RawSubCategory.Add(new Common_RawSubCategory() { Name = "Buckle", Common_RawCategoryFK = 6 });
                context.Common_RawSubCategory.Add(new Common_RawSubCategory() { Name = "Weaving Belt", Common_RawCategoryFK = 6 });
                context.Common_RawSubCategory.Add(new Common_RawSubCategory() { Name = "Cable (steel ware)", Common_RawCategoryFK = 6 });
                context.Common_RawSubCategory.Add(new Common_RawSubCategory() { Name = "Adjuster", Common_RawCategoryFK = 6 });
                context.Common_RawSubCategory.Add(new Common_RawSubCategory() { Name = "Recco", Common_RawCategoryFK = 6 });
                context.Common_RawSubCategory.Add(new Common_RawSubCategory() { Name = "Elastic Threads", Common_RawCategoryFK = 6 });
                //Embroidery
                context.Common_RawSubCategory.Add(new Common_RawSubCategory() { Name = "Whitework embroidery", Common_RawCategoryFK = 5 });
                context.Common_RawSubCategory.Add(new Common_RawSubCategory() { Name = "Candlewick embroidery", Common_RawCategoryFK = 5 });
                context.Common_RawSubCategory.Add(new Common_RawSubCategory() { Name = "Cross stitch embroidery", Common_RawCategoryFK = 5 });
                context.Common_RawSubCategory.Add(new Common_RawSubCategory() { Name = "Pulled thread embroidery", Common_RawCategoryFK = 5 });
                context.Common_RawSubCategory.Add(new Common_RawSubCategory() { Name = "Hedebo embroidery", Common_RawCategoryFK = 5 });
                context.Common_RawSubCategory.Add(new Common_RawSubCategory() { Name = "Drawn thread embroidery", Common_RawCategoryFK = 5 });
                context.Common_RawSubCategory.Add(new Common_RawSubCategory() { Name = "Hardanger embroidery", Common_RawCategoryFK = 5 });
                context.Common_RawSubCategory.Add(new Common_RawSubCategory() { Name = "Crewel embroidery", Common_RawCategoryFK = 5 });
                context.Common_RawSubCategory.Add(new Common_RawSubCategory() { Name = "Surface embroidery", Common_RawCategoryFK = 5 });
                context.Common_RawSubCategory.Add(new Common_RawSubCategory() { Name = "Goldwork embroidery", Common_RawCategoryFK = 5 });
                context.Common_RawSubCategory.Add(new Common_RawSubCategory() { Name = "Redwork embroidery", Common_RawCategoryFK = 5 });
                context.Common_RawSubCategory.Add(new Common_RawSubCategory() { Name = "Blackwork embroidery", Common_RawCategoryFK = 5 });
                context.Common_RawSubCategory.Add(new Common_RawSubCategory() { Name = "Bluework embroidery", Common_RawCategoryFK = 5 });
                context.Common_RawSubCategory.Add(new Common_RawSubCategory() { Name = "Sashiko embroidery", Common_RawCategoryFK = 5 });
                //Carton
                context.Common_RawSubCategory.Add(new Common_RawSubCategory() { Name = "Carton", Common_RawCategoryFK = 7 });

                context.SaveChanges();
            }
            if (!context.Common_RawItem.Any())
            {
                //FabricItem
                context.Common_RawItem.Add(new Common_RawItem() { Name = "24/1", Common_RawSubCategoryFK = 8, Common_UnitFK = 1, Quantity = 0, GSM = "170-190" });
                context.Common_RawItem.Add(new Common_RawItem() { Name = "26/1", Common_RawSubCategoryFK = 8, Common_UnitFK = 1, Quantity = 7, GSM = "160" });
                context.Common_RawItem.Add(new Common_RawItem() { Name = "30/1", Common_RawSubCategoryFK = 8, Common_UnitFK = 1, Quantity = 7, GSM = "200" });
                context.Common_RawItem.Add(new Common_RawItem() { Name = "36/1", Common_RawSubCategoryFK = 8, Common_UnitFK = 1, Quantity = 7, GSM = "180" });

                //Accessories
                context.Common_RawItem.Add(new Common_RawItem() { Name = "Plain poly bags", Common_RawSubCategoryFK = 31, Common_UnitFK = 2, Quantity = 8 });
                context.Common_RawItem.Add(new Common_RawItem() { Name = "Hanger bags", Common_RawSubCategoryFK = 31, Common_UnitFK = 2, Quantity = 3 });

                context.Common_RawItem.Add(new Common_RawItem() { Name = "Pack 11", Common_RawSubCategoryFK = 60, Common_UnitFK = 2, Quantity = 10 });
                context.Common_RawItem.Add(new Common_RawItem() { Name = "Pack 12", Common_RawSubCategoryFK = 60, Common_UnitFK = 2, Quantity = 56 });
                context.Common_RawItem.Add(new Common_RawItem() { Name = "Carton 11", Common_RawSubCategoryFK = 60, Common_UnitFK = 1, Quantity = 45 });
                context.Common_RawItem.Add(new Common_RawItem() { Name = "Carton 12", Common_RawSubCategoryFK = 60, Common_UnitFK = 1, Quantity = 45 });
                context.SaveChanges();
            }
            if (!context.Mis_AccountsPayable.Any())
            {
                context.Mis_AccountsPayable.Add(new Mis_AccountsPayable() { Common_SupplierFk = 1, Date = DateTime.Now.AddDays(-11), Value = 50000 });
                context.Mis_AccountsPayable.Add(new Mis_AccountsPayable() { Common_SupplierFk = 2, Date = DateTime.Now, Value = 55000 });
                context.Mis_AccountsPayable.Add(new Mis_AccountsPayable() { Common_SupplierFk = 3, Date = DateTime.Now.AddDays(-15), Value = 80000 });
                context.Mis_AccountsPayable.Add(new Mis_AccountsPayable() { Common_SupplierFk = 4, Date = DateTime.Now, Value = 70000 });
                context.Mis_AccountsPayable.Add(new Mis_AccountsPayable() { Common_SupplierFk = 5, Date = DateTime.Now.AddDays(-2), Value = 40000 });
                context.Mis_AccountsPayable.Add(new Mis_AccountsPayable() { Common_SupplierFk = 1, Date = DateTime.Now, Value = 90000 });
                context.Mis_AccountsPayable.Add(new Mis_AccountsPayable() { Common_SupplierFk = 3, Date = DateTime.Now.AddDays(-12), Value = 10000 });
                context.Mis_AccountsPayable.Add(new Mis_AccountsPayable() { Common_SupplierFk = 7, Date = DateTime.Now, Value = 30000 });
                context.Mis_AccountsPayable.Add(new Mis_AccountsPayable() { Common_SupplierFk = 8, Date = DateTime.Now, Value = 50000 });
                context.Mis_AccountsPayable.Add(new Mis_AccountsPayable() { Common_SupplierFk = 4, Date = DateTime.Now.AddDays(-5), Value = 40000 });
                context.Mis_AccountsPayable.Add(new Mis_AccountsPayable() { Common_SupplierFk = 3, Date = DateTime.Now, Value = 70000 });
                context.Mis_AccountsPayable.Add(new Mis_AccountsPayable() { Common_SupplierFk = 2, Date = DateTime.Now.AddDays(-9), Value = 90000 });
                context.Mis_AccountsPayable.Add(new Mis_AccountsPayable() { Common_SupplierFk = 5, Date = DateTime.Now, Value = 70000 });
                context.SaveChanges();
            }
            if (!context.Mis_AccountsReceivable.Any())
            {
                context.Mis_AccountsReceivable.Add(new Mis_AccountsReceivable() { Common_BuyerFk = 1, Date = DateTime.Now, Value = 25000 });
                context.Mis_AccountsReceivable.Add(new Mis_AccountsReceivable() { Common_BuyerFk = 2, Date = DateTime.Now, Value = 85000 });
                context.Mis_AccountsReceivable.Add(new Mis_AccountsReceivable() { Common_BuyerFk = 3, Date = DateTime.Now, Value = 27000 });
                context.Mis_AccountsReceivable.Add(new Mis_AccountsReceivable() { Common_BuyerFk = 4, Date = DateTime.Now, Value = 70000 });
                context.Mis_AccountsReceivable.Add(new Mis_AccountsReceivable() { Common_BuyerFk = 5, Date = DateTime.Now.AddDays(-2), Value = 40000 });
                context.Mis_AccountsReceivable.Add(new Mis_AccountsReceivable() { Common_BuyerFk = 1, Date = DateTime.Now, Value = 90000 });
                context.Mis_AccountsReceivable.Add(new Mis_AccountsReceivable() { Common_BuyerFk = 3, Date = DateTime.Now.AddDays(-12), Value = 10000 });
                context.Mis_AccountsReceivable.Add(new Mis_AccountsReceivable() { Common_BuyerFk = 7, Date = DateTime.Now, Value = 30000 });
                context.Mis_AccountsReceivable.Add(new Mis_AccountsReceivable() { Common_BuyerFk = 8, Date = DateTime.Now, Value = 50000 });
                context.Mis_AccountsReceivable.Add(new Mis_AccountsReceivable() { Common_BuyerFk = 4, Date = DateTime.Now.AddDays(-5), Value = 40000 });
                context.Mis_AccountsReceivable.Add(new Mis_AccountsReceivable() { Common_BuyerFk = 3, Date = DateTime.Now, Value = 70000 });
                context.Mis_AccountsReceivable.Add(new Mis_AccountsReceivable() { Common_BuyerFk = 2, Date = DateTime.Now.AddDays(-9), Value = 90000 });
                context.Mis_AccountsReceivable.Add(new Mis_AccountsReceivable() { Common_BuyerFk = 5, Date = DateTime.Now, Value = 70000 });
                context.SaveChanges();
            }
            if (!context.Mis_BalanceSheet.Any())
            {
                context.Mis_BalanceSheet.Add(new Mis_BalanceSheet() { Accounting_Head = "Export Sales", FromDate = DateTime.Now.AddMonths(-6), ToDate = DateTime.Now, Value = 54000 });
                context.Mis_BalanceSheet.Add(new Mis_BalanceSheet() { Accounting_Head = "Local Sales/ Subcontract Income", FromDate = DateTime.Now.AddMonths(-6), ToDate = DateTime.Now, Value = 37000 });
                context.Mis_BalanceSheet.Add(new Mis_BalanceSheet() { Accounting_Head = "Interest Income", FromDate = DateTime.Now.AddMonths(-6), ToDate = DateTime.Now, Value = 55000 });
                context.Mis_BalanceSheet.Add(new Mis_BalanceSheet() { Accounting_Head = "Leftover Sales", FromDate = DateTime.Now.AddMonths(-6), ToDate = DateTime.Now, Value = 54000 });
                context.Mis_BalanceSheet.Add(new Mis_BalanceSheet() { Accounting_Head = "Miscellaneous Income", FromDate = DateTime.Now.AddMonths(-6), ToDate = DateTime.Now, Value = 37000 });
                context.Mis_BalanceSheet.Add(new Mis_BalanceSheet() { Accounting_Head = "Scrap Sales", FromDate = DateTime.Now.AddMonths(-6), ToDate = DateTime.Now, Value = 55000 });
                context.Mis_BalanceSheet.Add(new Mis_BalanceSheet() { Accounting_Head = "Wastage Sales", FromDate = DateTime.Now.AddMonths(-6), ToDate = DateTime.Now, Value = 54000 });

                context.SaveChanges();
            }
            if (!context.Mis_BTBLCStatus.Any())
            {
                context.Mis_BTBLCStatus.Add(new Mis_BTBLCStatus() {  UDNo = "MLC001",  MasterLCTotalValue = 500800, BTBLimit = 350560, BTBIssued = 200000, BTBAcceptedValue = 100000, BTBLiabilites = 100000 });
                context.Mis_BTBLCStatus.Add(new Mis_BTBLCStatus() { UDNo = "MLC002", MasterLCTotalValue = 200900, BTBLimit = 140630, BTBIssued = 100000, BTBAcceptedValue = 50000, BTBLiabilites = 50000 });
                context.Mis_BTBLCStatus.Add(new Mis_BTBLCStatus() { UDNo = "MLC003", MasterLCTotalValue = 500800, BTBLimit = 350560, BTBIssued = 200000, BTBAcceptedValue = 100000, BTBLiabilites = 100000 });
                context.Mis_BTBLCStatus.Add(new Mis_BTBLCStatus() { UDNo = "MLC004", MasterLCTotalValue = 200900, BTBLimit = 140630, BTBIssued = 100000, BTBAcceptedValue = 50000, BTBLiabilites = 50000 });
                context.Mis_BTBLCStatus.Add(new Mis_BTBLCStatus() { UDNo = "MLC005", MasterLCTotalValue = 500800, BTBLimit = 350560, BTBIssued = 200000, BTBAcceptedValue = 100000, BTBLiabilites = 100000 });
                context.Mis_BTBLCStatus.Add(new Mis_BTBLCStatus() { UDNo = "MLC006", MasterLCTotalValue = 200900, BTBLimit = 140630, BTBIssued = 100000, BTBAcceptedValue = 50000, BTBLiabilites = 50000 });
                context.Mis_BTBLCStatus.Add(new Mis_BTBLCStatus() { UDNo = "MLC007", MasterLCTotalValue = 500800, BTBLimit = 350560, BTBIssued = 200000, BTBAcceptedValue = 100000, BTBLiabilites = 100000 });
                context.Mis_BTBLCStatus.Add(new Mis_BTBLCStatus() { UDNo = "MLC008", MasterLCTotalValue = 200900, BTBLimit = 140630, BTBIssued = 100000, BTBAcceptedValue = 50000, BTBLiabilites = 50000 });
                context.Mis_BTBLCStatus.Add(new Mis_BTBLCStatus() { UDNo = "MLC009", MasterLCTotalValue = 500800, BTBLimit = 350560, BTBIssued = 200000, BTBAcceptedValue = 100000, BTBLiabilites = 100000 });
                context.Mis_BTBLCStatus.Add(new Mis_BTBLCStatus() { UDNo = "MLC010", MasterLCTotalValue = 200900, BTBLimit = 140630, BTBIssued = 100000, BTBAcceptedValue = 50000, BTBLiabilites = 50000 });
                context.SaveChanges();
            }

            if (!context.Mis_CashAtBank.Any())
            {
                context.Mis_CashAtBank.Add(new Mis_CashAtBank() { Accounting_Head = "JBL CD A/C-15327", Date = DateTime.Now, Value = 50700 });
                context.Mis_CashAtBank.Add(new Mis_CashAtBank() { Accounting_Head = "JBL ERQ A/C-23340", Date = DateTime.Now, Value = 30100 });
                context.Mis_CashAtBank.Add(new Mis_CashAtBank() { Accounting_Head = "SBL CD A/C-68014", Date = DateTime.Now, Value = 40900 });
                context.Mis_CashAtBank.Add(new Mis_CashAtBank() { Accounting_Head = "IBBL FDR A/C-00152", Date = DateTime.Now, Value = 50700 });
                context.Mis_CashAtBank.Add(new Mis_CashAtBank() { Accounting_Head = "IBBL PC A/C-00152", Date = DateTime.Now, Value = 30100 });
                context.Mis_CashAtBank.Add(new Mis_CashAtBank() { Accounting_Head = "EBL FDR A/C-56454", Date = DateTime.Now, Value = 40900 });
                context.Mis_CashAtBank.Add(new Mis_CashAtBank() { Accounting_Head = "Prime Bank CD A/C-12511", Date = DateTime.Now, Value = 50700 });
                context.Mis_CashAtBank.Add(new Mis_CashAtBank() { Accounting_Head = "JBL FDR A/C-033003", Date = DateTime.Now, Value = 30100 });
                context.Mis_CashAtBank.Add(new Mis_CashAtBank() { Accounting_Head = "SBL FC Held A/C-23123", Date = DateTime.Now, Value = 40900 });
                context.Mis_CashAtBank.Add(new Mis_CashAtBank() { Accounting_Head = "SBL FDR A/C-00152", Date = DateTime.Now, Value = 40900 });
                context.SaveChanges();
            }
            if (!context.Mis_CashInHand.Any())
            {
                context.Mis_CashInHand.Add(new Mis_CashInHand() { Accounting_Head = "Cash-HO", Date = DateTime.Now, Value = 10000 });
                context.Mis_CashInHand.Add(new Mis_CashInHand() { Accounting_Head = "Cash-Construction", Date = DateTime.Now, Value = 40000 });
                context.Mis_CashInHand.Add(new Mis_CashInHand() { Accounting_Head = "Cash Office 1", Date = DateTime.Now, Value = 57000 });
                context.Mis_CashInHand.Add(new Mis_CashInHand() { Accounting_Head = "Cash Office 2", Date = DateTime.Now, Value = 10000 });
                context.Mis_CashInHand.Add(new Mis_CashInHand() { Accounting_Head = "Petty Cash", Date = DateTime.Now, Value = 40000 });
                context.SaveChanges();
            }

            if (!context.Mis_IncomeStatement.Any())
            {
                context.Mis_IncomeStatement.Add(new Mis_IncomeStatement() { Accounting_Head = "Export Sales", FromDate = DateTime.Now.AddMonths(-6), ToDate = DateTime.Now, Value = 54000 });
                context.Mis_IncomeStatement.Add(new Mis_IncomeStatement() { Accounting_Head = "Local Sales/ Subcontract Income", FromDate = DateTime.Now.AddMonths(-6), ToDate = DateTime.Now, Value = 37000 });
                context.Mis_IncomeStatement.Add(new Mis_IncomeStatement() { Accounting_Head = "Interest Income", FromDate = DateTime.Now.AddMonths(-6), ToDate = DateTime.Now, Value = 55000 });
                context.Mis_IncomeStatement.Add(new Mis_IncomeStatement() { Accounting_Head = "Leftover Sales", FromDate = DateTime.Now.AddMonths(-6), ToDate = DateTime.Now, Value = 54000 });
                context.Mis_IncomeStatement.Add(new Mis_IncomeStatement() { Accounting_Head = "Miscellaneous Income", FromDate = DateTime.Now.AddMonths(-6), ToDate = DateTime.Now, Value = 37000 });
                context.Mis_IncomeStatement.Add(new Mis_IncomeStatement() { Accounting_Head = "Scrap Sales", FromDate = DateTime.Now.AddMonths(-6), ToDate = DateTime.Now, Value = 55000 });
                context.Mis_IncomeStatement.Add(new Mis_IncomeStatement() { Accounting_Head = "Wastage Sales", FromDate = DateTime.Now.AddMonths(-6), ToDate = DateTime.Now, Value = 54000 });

                context.SaveChanges();
            }
            if (!context.Mis_FinancialStatus.Any())
            {
                context.Mis_FinancialStatus.Add(new Mis_FinancialStatus() { Month = "June", Year = "2018", Inflow = 20000, Outflow = 40050, RevenueProjection = 20394, ExpenditureProjection = 23411 });
                context.Mis_FinancialStatus.Add(new Mis_FinancialStatus() { Month = "July", Year = "2018", Inflow = 50000, Outflow = 30050, RevenueProjection = 10394, ExpenditureProjection = 73411 });
                context.Mis_FinancialStatus.Add(new Mis_FinancialStatus() { Month = "August", Year = "2018", Inflow = 30000, Outflow = 20050, RevenueProjection = 40394, ExpenditureProjection = 43411 });
                context.Mis_FinancialStatus.Add(new Mis_FinancialStatus() { Month = "September", Year = "2018", Inflow = 50000, Outflow = 50050, RevenueProjection = 20394, ExpenditureProjection = 63411 });
                context.Mis_FinancialStatus.Add(new Mis_FinancialStatus() { Month = "October", Year = "2018", Inflow = 60000, Outflow = 60050, RevenueProjection = 60394, ExpenditureProjection = 53411 });
                context.Mis_FinancialStatus.Add(new Mis_FinancialStatus() { Month = "November", Year = "2018", Inflow = 40000, Outflow = 30050, RevenueProjection = 30394, ExpenditureProjection = 23411 });
                context.Mis_FinancialStatus.Add(new Mis_FinancialStatus() { Month = "December", Year = "2018", Inflow = 50000, Outflow = 70050, RevenueProjection = 70394, ExpenditureProjection = 33411 });
                context.Mis_FinancialStatus.Add(new Mis_FinancialStatus() { Month = "January", Year = "2019", Inflow = 10000, Outflow = 40050, RevenueProjection = 50394, ExpenditureProjection = 73411 });
                context.Mis_FinancialStatus.Add(new Mis_FinancialStatus() { Month = "February", Year = "2019", Inflow = 30000, Outflow = 60050, RevenueProjection = 60394, ExpenditureProjection = 1411 });
                context.Mis_FinancialStatus.Add(new Mis_FinancialStatus() { Month = "March", Year = "2019", Inflow = 70000, Outflow = 50050, RevenueProjection = 30394, ExpenditureProjection = 33411 });
                context.SaveChanges();
            }
            if (!context.Mis_CMEarned.Any())
            {
                context.Mis_CMEarned.Add(new Mis_CMEarned() { Month = "June", Year = "2018", Quantity = 5000, Value = 35000 });
                context.Mis_CMEarned.Add(new Mis_CMEarned() { Month = "July", Year = "2018", Quantity = 8000, Value = 95000 });
                context.Mis_CMEarned.Add(new Mis_CMEarned() { Month = "August", Year = "2018", Quantity = 9000, Value = 75000 });
                context.Mis_CMEarned.Add(new Mis_CMEarned() { Month = "September", Year = "2018", Quantity = 7000, Value = 65000 });
                context.Mis_CMEarned.Add(new Mis_CMEarned() { Month = "October", Year = "2018", Quantity = 3000, Value = 45000 });
                context.Mis_CMEarned.Add(new Mis_CMEarned() { Month = "November", Year = "2018", Quantity = 7000, Value = 95000 });
                context.Mis_CMEarned.Add(new Mis_CMEarned() { Month = "December", Year = "2018", Quantity = 5000, Value = 75000 });
                context.Mis_CMEarned.Add(new Mis_CMEarned() { Month = "January", Year = "2019", Quantity = 8000, Value = 85000 });
                context.Mis_CMEarned.Add(new Mis_CMEarned() { Month = "February", Year = "2019", Quantity = 2000, Value = 20000 });
                context.Mis_CMEarned.Add(new Mis_CMEarned() { Month = "March", Year = "2019", Quantity = 4000, Value = 32000 });
                context.SaveChanges();
            }

            if (!context.Common_LcType.Any())
            {
                context.Common_LcType.Add(new Common_LcType() { Active = true, User = "OneUser", Time = DateTime.Now, Name = "Export LC" });
                context.Common_LcType.Add(new Common_LcType() { Active = true, User = "OneUser", Time = DateTime.Now, Name = "TT" });
                context.Common_LcType.Add(new Common_LcType() { Active = true, User = "OneUser", Time = DateTime.Now, Name = "Sales Contact" });
                context.SaveChanges();
            }

            if (!context.Common_BuyerBank.Any())
            {
                context.Common_BuyerBank.Add(new Common_BuyerBank() { Active = true, User = "OneUser", Time = DateTime.Now, Name = "COMMERZBANK AG" });
                context.Common_BuyerBank.Add(new Common_BuyerBank() { Active = true, User = "OneUser", Time = DateTime.Now, Name = "CITI BANK ,N.A." });
                context.Common_BuyerBank.Add(new Common_BuyerBank() { Active = true, User = "OneUser", Time = DateTime.Now, Name = "DEUTSCHE BANK AG" });
                context.SaveChanges();
            }

            if (!context.Common_LienBank.Any())
            {
                context.Common_LienBank.Add(new Common_LienBank() { Active = true, User = "OneUser", Time = DateTime.Now, Name = "JAMUNA BANK LIMITED" });
                context.Common_LienBank.Add(new Common_LienBank() { Active = true, User = "OneUser", Time = DateTime.Now, Name = "SOUTHEAST BANK LTD" });
                context.Common_LienBank.Add(new Common_LienBank() { Active = true, User = "OneUser", Time = DateTime.Now, Name = "DDEUTSCHE BANK LTD" });
                context.SaveChanges();
            }
            if (!context.Common_CompanyBank.Any())
            {
                context.Common_CompanyBank.Add(new Common_CompanyBank() { Active = true, User = "OneUser", Time = DateTime.Now, Name = "JAMUNA BANK LIMITED" });
                context.Common_CompanyBank.Add(new Common_CompanyBank() { Active = true, User = "OneUser", Time = DateTime.Now, Name = "SOUTHEAST BANK LTD" });
                context.Common_CompanyBank.Add(new Common_CompanyBank() { Active = true, User = "OneUser", Time = DateTime.Now, Name = "DDEUTSCHE BANK LTD" });
                context.SaveChanges();
            }
            if (!context.Common_TCTitle.Any())
            {
                context.Common_TCTitle.Add(new Common_TCTitle() { Active = true, User = "OneUser", Time = DateTime.Now, Name = "T & C- Foreign" });
                context.Common_TCTitle.Add(new Common_TCTitle() { Active = true, User = "OneUser", Time = DateTime.Now, Name = "T & C- Local" });
                context.Common_TCTitle.Add(new Common_TCTitle() { Active = true, User = "OneUser", Time = DateTime.Now, Name = "Knitting Program" });
                context.SaveChanges();
            }
            if (!context.Merchandising_Style.Any())
            {
                context.Merchandising_Style.Add(new Merchandising_Style() { Active = true, User = "OneUser", Time = DateTime.Now, StyleName = "KIK TEXTILIEN UND NON FOOD GMBH/19/026/P92091 (4500156608)/P92091" });
                context.Merchandising_Style.Add(new Merchandising_Style() { Active = true, User = "OneUser", Time = DateTime.Now, StyleName = "KIK TEXTILIEN UND NON FOOD GMBH/19/026/P92091 (4500156608)/P92092" });
                context.Merchandising_Style.Add(new Merchandising_Style() { Active = true, User = "OneUser", Time = DateTime.Now, StyleName = "KIK TEXTILIEN UND NON FOOD GMBH/19/026/P92091 (4500156608)/P92093" });
                context.SaveChanges();
            }

            if (!context.Common_RawSubCategory.Any())
            {
                context.Common_RawSubCategory.Add(new Common_RawSubCategory() { Active = true, User = "OneUser", Time = DateTime.Now, Name = "Cotton 1", Common_RawCategoryFK = 1, });
                context.Common_RawSubCategory.Add(new Common_RawSubCategory() { Active = true, User = "OneUser", Time = DateTime.Now, Name = "Cotton 2", Common_RawCategoryFK = 1 });
                context.Common_RawSubCategory.Add(new Common_RawSubCategory() { Active = true, User = "OneUser", Time = DateTime.Now, Name = "PC 1", Common_RawCategoryFK = 2 });
                context.Common_RawSubCategory.Add(new Common_RawSubCategory() { Active = true, User = "OneUser", Time = DateTime.Now, Name = "PC 2", Common_RawCategoryFK = 2 });
                context.Common_RawSubCategory.Add(new Common_RawSubCategory() { Active = true, User = "OneUser", Time = DateTime.Now, Name = "Pack 1", Common_RawCategoryFK = 3 });
                context.Common_RawSubCategory.Add(new Common_RawSubCategory() { Active = true, User = "OneUser", Time = DateTime.Now, Name = "Pack 2", Common_RawCategoryFK = 3 });
                context.Common_RawSubCategory.Add(new Common_RawSubCategory() { Active = true, User = "OneUser", Time = DateTime.Now, Name = "Carton 1", Common_RawCategoryFK = 4 });
                context.Common_RawSubCategory.Add(new Common_RawSubCategory() { Active = true, User = "OneUser", Time = DateTime.Now, Name = "Carton 2", Common_RawCategoryFK = 4 });
                context.SaveChanges();
            }
            if (!context.Common_RawItem.Any())
            {
                context.Common_RawItem.Add(new Common_RawItem() { Active = true, User = "OneUser", Time = DateTime.Now, Name = "Cotton 11", Common_RawSubCategoryFK = 1 });
                context.Common_RawItem.Add(new Common_RawItem() { Active = true, User = "OneUser", Time = DateTime.Now, Name = "Cotton 12", Common_RawSubCategoryFK = 1 });
                context.Common_RawItem.Add(new Common_RawItem() { Active = true, User = "OneUser", Time = DateTime.Now, Name = "PC 11", Common_RawSubCategoryFK = 2 });
                context.Common_RawItem.Add(new Common_RawItem() { Active = true, User = "OneUser", Time = DateTime.Now, Name = "PC 12", Common_RawSubCategoryFK = 2 });
                context.Common_RawItem.Add(new Common_RawItem() { Active = true, User = "OneUser", Time = DateTime.Now, Name = "Pack 11", Common_RawSubCategoryFK = 3 });
                context.Common_RawItem.Add(new Common_RawItem() { Active = true, User = "OneUser", Time = DateTime.Now, Name = "Pack 12", Common_RawSubCategoryFK = 3 });
                context.Common_RawItem.Add(new Common_RawItem() { Active = true, User = "OneUser", Time = DateTime.Now, Name = "Carton 11", Common_RawSubCategoryFK = 4 });
                context.Common_RawItem.Add(new Common_RawItem() { Active = true, User = "OneUser", Time = DateTime.Now, Name = "Carton 12", Common_RawSubCategoryFK = 4 });
                context.SaveChanges();
            }
            if (!context.Mis_AccountsPayable.Any())
            {
                context.Mis_AccountsPayable.Add(new Mis_AccountsPayable() { Active = true, User = "OneUser", Time = DateTime.Now, Common_SupplierFk = 1, Date = DateTime.Now.AddDays(-11), Value = 50000 });
                context.Mis_AccountsPayable.Add(new Mis_AccountsPayable() { Active = true, User = "OneUser", Time = DateTime.Now, Common_SupplierFk = 2, Date = DateTime.Now, Value = 55000 });
                context.Mis_AccountsPayable.Add(new Mis_AccountsPayable() { Active = true, User = "OneUser", Time = DateTime.Now, Common_SupplierFk = 3, Date = DateTime.Now.AddDays(-15), Value = 80000 });
                context.Mis_AccountsPayable.Add(new Mis_AccountsPayable() { Active = true, User = "OneUser", Time = DateTime.Now, Common_SupplierFk = 4, Date = DateTime.Now, Value = 70000 });
                context.Mis_AccountsPayable.Add(new Mis_AccountsPayable() { Active = true, User = "OneUser", Time = DateTime.Now, Common_SupplierFk = 5, Date = DateTime.Now.AddDays(-2), Value = 40000 });
                context.Mis_AccountsPayable.Add(new Mis_AccountsPayable() { Active = true, User = "OneUser", Time = DateTime.Now, Common_SupplierFk = 1, Date = DateTime.Now, Value = 90000 });
                context.Mis_AccountsPayable.Add(new Mis_AccountsPayable() { Active = true, User = "OneUser", Time = DateTime.Now, Common_SupplierFk = 3, Date = DateTime.Now.AddDays(-12), Value = 10000 });
                context.Mis_AccountsPayable.Add(new Mis_AccountsPayable() { Active = true, User = "OneUser", Time = DateTime.Now, Common_SupplierFk = 7, Date = DateTime.Now, Value = 30000 });
                context.Mis_AccountsPayable.Add(new Mis_AccountsPayable() { Active = true, User = "OneUser", Time = DateTime.Now, Common_SupplierFk = 8, Date = DateTime.Now, Value = 50000 });
                context.Mis_AccountsPayable.Add(new Mis_AccountsPayable() { Active = true, User = "OneUser", Time = DateTime.Now, Common_SupplierFk = 4, Date = DateTime.Now.AddDays(-5), Value = 40000 });
                context.Mis_AccountsPayable.Add(new Mis_AccountsPayable() { Active = true, User = "OneUser", Time = DateTime.Now, Common_SupplierFk = 3, Date = DateTime.Now, Value = 70000 });
                context.Mis_AccountsPayable.Add(new Mis_AccountsPayable() { Active = true, User = "OneUser", Time = DateTime.Now, Common_SupplierFk = 2, Date = DateTime.Now.AddDays(-9), Value = 90000 });
                context.Mis_AccountsPayable.Add(new Mis_AccountsPayable() { Active = true, User = "OneUser", Time = DateTime.Now, Common_SupplierFk = 5, Date = DateTime.Now, Value = 70000 });
                context.SaveChanges();
            }

            if (!context.Mis_OrderConfirm.Any())
            {
                context.Mis_OrderConfirm.Add(new Mis_OrderConfirm() { Active = true, User = "OneUser", Time = DateTime.Now, Common_BuyerFk = 1, FinalDeliveryDate = DateTime.Now.AddDays(5), OrderQty = 5000, OrderValue = 75000, CmValue = 55000 });
                context.Mis_OrderConfirm.Add(new Mis_OrderConfirm() { Active = true, User = "OneUser", Time = DateTime.Now, OrderNo = "PQ12001", Common_BuyerFk = 2, FinalDeliveryDate = DateTime.Now.AddDays(6), OrderQty = 2000, OrderValue = 32000, CmValue = 20000 });
                context.Mis_OrderConfirm.Add(new Mis_OrderConfirm() { Active = true, User = "OneUser", Time = DateTime.Now, OrderNo = "PQ12002", Common_BuyerFk = 3, FinalDeliveryDate = DateTime.Now.AddDays(8), OrderQty = 4000, OrderValue = 75000, CmValue = 50000 });
                context.Mis_OrderConfirm.Add(new Mis_OrderConfirm() { Active = true, User = "OneUser", Time = DateTime.Now, OrderNo = "PQ12003", Common_BuyerFk = 1, FinalDeliveryDate = DateTime.Now.AddDays(5), OrderQty = 5000, OrderValue = 75000, CmValue = 55000 });
                context.Mis_OrderConfirm.Add(new Mis_OrderConfirm() { Active = true, User = "OneUser", Time = DateTime.Now, OrderNo = "PQ12004", Common_BuyerFk = 2, FinalDeliveryDate = DateTime.Now.AddDays(6), OrderQty = 2000, OrderValue = 32000, CmValue = 20000 });
                context.Mis_OrderConfirm.Add(new Mis_OrderConfirm() { Active = true, User = "OneUser", Time = DateTime.Now, OrderNo = "PQ12005", Common_BuyerFk = 4, FinalDeliveryDate = DateTime.Now.AddDays(8), OrderQty = 4000, OrderValue = 75000, CmValue = 50000 });
                context.Mis_OrderConfirm.Add(new Mis_OrderConfirm() { Active = true, User = "OneUser", Time = DateTime.Now, OrderNo = "PQ12006", Common_BuyerFk = 1, FinalDeliveryDate = DateTime.Now.AddDays(5), OrderQty = 5000, OrderValue = 75000, CmValue = 55000 });
                context.Mis_OrderConfirm.Add(new Mis_OrderConfirm() { Active = true, User = "OneUser", Time = DateTime.Now, OrderNo = "PQ12007", Common_BuyerFk = 4, FinalDeliveryDate = DateTime.Now.AddDays(6), OrderQty = 2000, OrderValue = 32000, CmValue = 20000 });
                context.Mis_OrderConfirm.Add(new Mis_OrderConfirm() { Active = true, User = "OneUser", Time = DateTime.Now, OrderNo = "PQ12008", Common_BuyerFk = 3, FinalDeliveryDate = DateTime.Now.AddDays(8), OrderQty = 4000, OrderValue = 75000, CmValue = 50000 });
                context.Mis_OrderConfirm.Add(new Mis_OrderConfirm() { Active = true, User = "OneUser", Time = DateTime.Now, OrderNo = "PQ12009", Common_BuyerFk = 4, FinalDeliveryDate = DateTime.Now.AddDays(5), OrderQty = 5000, OrderValue = 75000, CmValue = 55000 });
                context.SaveChanges();
            }
            if (!context.Mis_DailyAttendance.Any())
            {
                context.Mis_DailyAttendance.Add(new Mis_DailyAttendance() { Active = true, User = "OneUser", Time = DateTime.Now, User_DepartmentFk = 1, TotalEmployee = 500, Present = 490, AttendanceDate = DateTime.Now, OnLeave = 8, WorkingHour = 8, Off = 2 });
                context.Mis_DailyAttendance.Add(new Mis_DailyAttendance() { Active = true, User = "OneUser", Time = DateTime.Now, User_DepartmentFk = 2, TotalEmployee = 500, Present = 480, AttendanceDate = DateTime.Now, OnLeave = 16, WorkingHour = 10, Off = 4 });
                context.Mis_DailyAttendance.Add(new Mis_DailyAttendance() { Active = true, User = "OneUser", Time = DateTime.Now, User_DepartmentFk = 3, TotalEmployee = 500, Present = 495, AttendanceDate = DateTime.Now, OnLeave = 3, WorkingHour = 10, Off = 2 });
                context.Mis_DailyAttendance.Add(new Mis_DailyAttendance() { Active = true, User = "OneUser", Time = DateTime.Now, User_DepartmentFk = 4, TotalEmployee = 500, Present = 490, AttendanceDate = DateTime.Now, OnLeave = 8, WorkingHour = 10, Off = 2 });
                context.Mis_DailyAttendance.Add(new Mis_DailyAttendance() { Active = true, User = "OneUser", Time = DateTime.Now, User_DepartmentFk = 2, TotalEmployee = 500, Present = 495, AttendanceDate = DateTime.Now.AddDays(-1), OnLeave = 3, WorkingHour = 8, Off = 2 });
                context.Mis_DailyAttendance.Add(new Mis_DailyAttendance() { Active = true, User = "OneUser", Time = DateTime.Now, User_DepartmentFk = 3, TotalEmployee = 500, Present = 490, AttendanceDate = DateTime.Now.AddDays(-1), OnLeave = 8, WorkingHour = 8, Off = 2 });
                context.Mis_DailyAttendance.Add(new Mis_DailyAttendance() { Active = true, User = "OneUser", Time = DateTime.Now, User_DepartmentFk = 4, TotalEmployee = 500, Present = 480, AttendanceDate = DateTime.Now.AddDays(-1), OnLeave = 16, WorkingHour = 8, Off = 4 });
                context.Mis_DailyAttendance.Add(new Mis_DailyAttendance() { Active = true, User = "OneUser", Time = DateTime.Now, User_DepartmentFk = 1, TotalEmployee = 500, Present = 495, AttendanceDate = DateTime.Now.AddDays(-1), OnLeave = 3, WorkingHour = 8, Off = 2 });
                context.Mis_DailyAttendance.Add(new Mis_DailyAttendance() { Active = true, User = "OneUser", Time = DateTime.Now, User_DepartmentFk = 2, TotalEmployee = 500, Present = 490, AttendanceDate = DateTime.Now.AddDays(-2), OnLeave = 8, WorkingHour = 10, Off = 2 });
                context.Mis_DailyAttendance.Add(new Mis_DailyAttendance() { Active = true, User = "OneUser", Time = DateTime.Now, User_DepartmentFk = 1, TotalEmployee = 500, Present = 480, AttendanceDate = DateTime.Now.AddDays(-2), OnLeave = 16, WorkingHour = 10, Off = 4 });

                context.SaveChanges();
            }
            if (!context.Mis_DailyProduction.Any())
            {
                context.Mis_DailyProduction.Add(new Mis_DailyProduction() { Active = true, SMV = 8.60M, User = "OneUser", Time = DateTime.Now, OrderNo = "PL0001", Common_BuyerFk = 1, Knitting = 3000, KnittingDone = 2900, Dyeing = 2900, DyeingDone = 2900, Cutting = 2900, CuttingDone = 2000, Sewing = 2000, SewingDone = 2000, Finishing = 2000, FinishingDone = 2000, OrderQty = 5000, ProductionDate = DateTime.Now });
                context.Mis_DailyProduction.Add(new Mis_DailyProduction() { Active = true, SMV = 5.60M, User = "OneUser", Time = DateTime.Now, OrderNo = "PL0002", Common_BuyerFk = 2, Knitting = 3000, KnittingDone = 2900, Dyeing = 2900, DyeingDone = 2900, Cutting = 2900, CuttingDone = 2000, Sewing = 2000, SewingDone = 2000, Finishing = 2000, FinishingDone = 2000, OrderQty = 5000, ProductionDate = DateTime.Now.AddDays(-1) });
                context.Mis_DailyProduction.Add(new Mis_DailyProduction() { Active = true, SMV = 4.60M, User = "OneUser", Time = DateTime.Now, OrderNo = "PL0003", Common_BuyerFk = 3, Knitting = 3000, KnittingDone = 2900, Dyeing = 2900, DyeingDone = 2900, Cutting = 2900, CuttingDone = 2000, Sewing = 2000, SewingDone = 2000, Finishing = 2000, FinishingDone = 2000, OrderQty = 5000, ProductionDate = DateTime.Now.AddDays(-2) });
                context.Mis_DailyProduction.Add(new Mis_DailyProduction() { Active = true, SMV = 5.60M, User = "OneUser", Time = DateTime.Now, OrderNo = "PL0004", Common_BuyerFk = 1, Knitting = 3000, KnittingDone = 2900, Dyeing = 2900, DyeingDone = 2900, Cutting = 2900, CuttingDone = 2000, Sewing = 2000, SewingDone = 2000, Finishing = 2000, FinishingDone = 2000, OrderQty = 5000, ProductionDate = DateTime.Now });
                context.Mis_DailyProduction.Add(new Mis_DailyProduction() { Active = true, SMV = 6.60M, User = "OneUser", Time = DateTime.Now, OrderNo = "PL0005", Common_BuyerFk = 2, Knitting = 3000, KnittingDone = 2900, Dyeing = 2900, DyeingDone = 2900, Cutting = 2900, CuttingDone = 2000, Sewing = 2000, SewingDone = 2000, Finishing = 2000, FinishingDone = 2000, OrderQty = 5000, ProductionDate = DateTime.Now.AddDays(-1) });
                context.Mis_DailyProduction.Add(new Mis_DailyProduction() { Active = true, SMV = 1.60M, User = "OneUser", Time = DateTime.Now, OrderNo = "PL0006", Common_BuyerFk = 4, Knitting = 3000, KnittingDone = 2900, Dyeing = 2900, DyeingDone = 2900, Cutting = 2900, CuttingDone = 2000, Sewing = 2000, SewingDone = 2000, Finishing = 2000, FinishingDone = 2000, OrderQty = 5000, ProductionDate = DateTime.Now.AddDays(-2) });
                context.Mis_DailyProduction.Add(new Mis_DailyProduction() { Active = true, SMV = 2.60M, User = "OneUser", Time = DateTime.Now, OrderNo = "PL0007", Common_BuyerFk = 1, Knitting = 3000, KnittingDone = 2900, Dyeing = 2900, DyeingDone = 2900, Cutting = 2900, CuttingDone = 2000, Sewing = 2000, SewingDone = 2000, Finishing = 2000, FinishingDone = 2000, OrderQty = 5000, ProductionDate = DateTime.Now });
                context.Mis_DailyProduction.Add(new Mis_DailyProduction() { Active = true, SMV = 3.60M, User = "OneUser", Time = DateTime.Now, OrderNo = "PL0008", Common_BuyerFk = 4, Knitting = 3000, KnittingDone = 2900, Dyeing = 2900, DyeingDone = 2900, Cutting = 2900, CuttingDone = 2000, Sewing = 2000, SewingDone = 2000, Finishing = 2000, FinishingDone = 2000, OrderQty = 5000, ProductionDate = DateTime.Now.AddDays(-1) });
                context.Mis_DailyProduction.Add(new Mis_DailyProduction() { Active = true, SMV = 9.60M, User = "OneUser", Time = DateTime.Now, OrderNo = "PL0009", Common_BuyerFk = 3, Knitting = 3000, KnittingDone = 2900, Dyeing = 2900, DyeingDone = 2900, Cutting = 2900, CuttingDone = 2000, Sewing = 2000, SewingDone = 2000, Finishing = 2000, FinishingDone = 2000, OrderQty = 5000, ProductionDate = DateTime.Now.AddDays(-2) });
                context.Mis_DailyProduction.Add(new Mis_DailyProduction() { Active = true, SMV = 5.60M, User = "OneUser", Time = DateTime.Now, OrderNo = "PL00010", Common_BuyerFk = 4, Knitting = 3000, KnittingDone = 2900, Dyeing = 2900, DyeingDone = 2900, Cutting = 2900, CuttingDone = 2000, Sewing = 2000, SewingDone = 2000, Finishing = 2000, FinishingDone = 2000, OrderQty = 5000, ProductionDate = DateTime.Now });
                context.SaveChanges();
            }
            if (!context.Mis_ShipmentStatus.Any())
            {
                context.Mis_ShipmentStatus.Add(new Mis_ShipmentStatus() { Active = true, User = "OneUser", Time = DateTime.Now, Common_BuyerFk = 1, OrderNo = "PQ098764", OrderQuantity = 5000, Date = DateTime.Now, DeliveredQuantity = 2000, Exfactory = 1000, DueQty = 2000 });
                context.Mis_ShipmentStatus.Add(new Mis_ShipmentStatus() { Active = true, User = "OneUser", Time = DateTime.Now, Common_BuyerFk = 2, OrderNo = "BQ098766", OrderQuantity = 2000, Date = DateTime.Now.AddDays(-3), DeliveredQuantity = 2000, Exfactory = 0, DueQty = 0 });
                context.Mis_ShipmentStatus.Add(new Mis_ShipmentStatus() { Active = true, User = "OneUser", Time = DateTime.Now, Common_BuyerFk = 3, OrderNo = "PQ098763", OrderQuantity = 4000, Date = DateTime.Now.AddDays(-5), DeliveredQuantity = 3500, Exfactory = 500, DueQty = 0 });
                context.Mis_ShipmentStatus.Add(new Mis_ShipmentStatus() { Active = true, User = "OneUser", Time = DateTime.Now, Common_BuyerFk = 1, OrderNo = "LQ098762", OrderQuantity = 5000, Date = DateTime.Now, DeliveredQuantity = 100, Exfactory = 1000, DueQty = 3000 });
                context.Mis_ShipmentStatus.Add(new Mis_ShipmentStatus() { Active = true, User = "OneUser", Time = DateTime.Now, Common_BuyerFk = 2, OrderNo = "PQ098711", OrderQuantity = 2000, Date = DateTime.Now, DeliveredQuantity = 2000, Exfactory = 0, DueQty = 0 });
                context.Mis_ShipmentStatus.Add(new Mis_ShipmentStatus() { Active = true, User = "OneUser", Time = DateTime.Now, Common_BuyerFk = 3, OrderNo = "PQ098760", OrderQuantity = 4000, Date = DateTime.Now, DeliveredQuantity = 3500, Exfactory = 500, DueQty = 0 });
                context.Mis_ShipmentStatus.Add(new Mis_ShipmentStatus() { Active = true, User = "OneUser", Time = DateTime.Now, Common_BuyerFk = 1, OrderNo = "RQ098765", OrderQuantity = 5000, Date = DateTime.Now.AddDays(-15), DeliveredQuantity = 4000, Exfactory = 1000, DueQty = 0 });
                context.Mis_ShipmentStatus.Add(new Mis_ShipmentStatus() { Active = true, User = "OneUser", Time = DateTime.Now, Common_BuyerFk = 2, OrderNo = "PQ098767", OrderQuantity = 2000, Date = DateTime.Now, DeliveredQuantity = 2000, Exfactory = 0, DueQty = 0 });
                context.Mis_ShipmentStatus.Add(new Mis_ShipmentStatus() { Active = true, User = "OneUser", Time = DateTime.Now, Common_BuyerFk = 4, OrderNo = "BQ098769", OrderQuantity = 4000, Date = DateTime.Now, DeliveredQuantity = 3500, Exfactory = 500, DueQty = 0 });
                context.Mis_ShipmentStatus.Add(new Mis_ShipmentStatus() { Active = true, User = "OneUser", Time = DateTime.Now, Common_BuyerFk = 1, OrderNo = "LQ098768", OrderQuantity = 5000, Date = DateTime.Now, DeliveredQuantity = 4000, Exfactory = 1000, DueQty = 0 });
                context.SaveChanges();
            }
            if (!context.Mis_RawMaterials.Any())
            {
                context.Mis_RawMaterials.Add(new Mis_RawMaterials() { Active = true, User = "OneUser", Time = DateTime.Now, Common_RawCategoryFk = 1, RawValue = 50000, WIPValue = 2000 });
                context.Mis_RawMaterials.Add(new Mis_RawMaterials() { Active = true, User = "OneUser", Time = DateTime.Now, Common_RawCategoryFk = 2, RawValue = 70000, WIPValue = 3000 });
                context.Mis_RawMaterials.Add(new Mis_RawMaterials() { Active = true, User = "OneUser", Time = DateTime.Now, Common_RawCategoryFk = 3, RawValue = 40000, WIPValue = 1000 });
                context.SaveChanges();
            }
            if (!context.Commercial_ECI.Any())
            {
                context.Commercial_ECI.Add(new Commercial_ECI() { Active = true, User = "OneUser", Time = DateTime.Now, Common_BuyerFK = 1, Common_BuyerBankFK = 1, Common_CompanyBankFK = 1, Common_CurrencyFK = 1, Common_ECITypeFK = 1, Common_LienBankFK = 1, Commercial_UDFk = 1, ECINo = "ECI001", ECIDate = DateTime.Now });
                context.Commercial_ECI.Add(new Commercial_ECI() { Active = true, User = "OneUser", Time = DateTime.Now, Common_BuyerFK = 1, Common_BuyerBankFK = 1, Common_CompanyBankFK = 3, Common_CurrencyFK = 2, Common_ECITypeFK = 1, Common_LienBankFK = 2, Commercial_UDFk = 1, ECINo = "ECI001", ECIDate = DateTime.Now });
                context.Commercial_ECI.Add(new Commercial_ECI() { Active = true, User = "OneUser", Time = DateTime.Now, Common_BuyerFK = 1, Common_BuyerBankFK = 1, Common_CompanyBankFK = 1, Common_CurrencyFK = 1, Common_ECITypeFK = 3, Common_LienBankFK = 1, Commercial_UDFk = 2, ECINo = "ECI001", ECIDate = DateTime.Now });

                context.SaveChanges();
            }
            if (!context.Merchandising_BuyerOrder.Any())
            {
                context.Merchandising_BuyerOrder.Add(new Merchandising_BuyerOrder() { Active = true, User = "OneUser", Time = DateTime.Now, BuyerPO = "1240", Agent = "AB Ltd.", CID = "0001", Commission = 5, Common_BuyerFK = 1, InspectionAgent = "ABC", IsComplete = false, OrderDate = DateTime.Now, Season = "AW50" });
                context.Merchandising_BuyerOrder.Add(new Merchandising_BuyerOrder() { Active = true, User = "OneUser", Time = DateTime.Now, BuyerPO = "1240", Agent = "XY Ltd.", CID = "0002", Commission = 5, Common_BuyerFK = 2, InspectionAgent = "ABC", IsComplete = false, OrderDate = DateTime.Now, Season = "AW50" });
                context.Merchandising_BuyerOrder.Add(new Merchandising_BuyerOrder() { Active = true, User = "OneUser", Time = DateTime.Now, BuyerPO = "1240", Agent = "AB Ltd.", CID = "0003", Commission = 5, Common_BuyerFK = 3, InspectionAgent = "ABC", IsComplete = false, OrderDate = DateTime.Now, Season = "AW50" });

                context.SaveChanges();
            }

            #region fahim store
            //if (!context.Store_General.Any())
            //{
            //    context.Store_General.Add(new Store_General() { StoreName = "in" });
            //    context.Store_General.Add(new Store_General() { StoreName = "out" });

            //    context.SaveChanges();
            //}
            #endregion

            #region fahim hrms employee
            //if (!context.Common_Country.Any())
            //{
            //    context.Common_Country.Add(new Common_Country() { Name = "Investment" });

            //    context.SaveChanges();
            //}
            //if (!context.HRMS_Designation.Any())
            //{
            //    context.HRMS_Designation.Add(new HRMS_Designation() { Name = "Investment" });

            //    context.SaveChanges();
            //}
            //if (!context.HRMS_BusinessUnit.Any())
            //{
            //    context.HRMS_BusinessUnit.Add(new HRMS_BusinessUnit() { Name = "Investment" });

            //    context.SaveChanges();
            //}
            //if (!context.HRMS_Unit.Any())
            //{
            //    context.HRMS_Unit.Add(new HRMS_Unit() { Name = "Investment" });

            //    context.SaveChanges();
            //}
            //if (!context.HRMS_Section.Any())
            //{
            //    context.HRMS_Section.Add(new HRMS_Section() { Name = "Investment" });

            //    context.SaveChanges();
            //}
            //if (!context.Common_ProductionLine.Any())
            //{
            //    context.Common_ProductionLine.Add(new Common_ProductionLine() { Name = "Investment" });

            //    context.SaveChanges();
            //}

            #endregion

            #region fahim hrms employee
            //if (!context.Common_Country.Any())
            //{
            //    context.Common_Country.Add(new Common_Country() { Name = "Investment" });

            //    context.SaveChanges();
            //}
            //if (!context.HRMS_Designation.Any())
            //{
            //    context.HRMS_Designation.Add(new HRMS_Designation() { Name = "Investment" });

            //    context.SaveChanges();
            //}
            //if (!context.HRMS_BusinessUnit.Any())
            //{
            //    context.HRMS_BusinessUnit.Add(new HRMS_BusinessUnit() { Name = "Investment" });

            //    context.SaveChanges();
            //}
            //if (!context.HRMS_Unit.Any())
            //{
            //    context.HRMS_Unit.Add(new HRMS_Unit() { Name = "Investment" });

            //    context.SaveChanges();
            //}
            //if (!context.HRMS_Section.Any())
            //{
            //    context.HRMS_Section.Add(new HRMS_Section() { Name = "Investment" });

            //    context.SaveChanges();
            //}
            //if (!context.Common_ProductionLine.Any())
            //{
            //    context.Common_ProductionLine.Add(new Common_ProductionLine() { Name = "Investment" });

            //    context.SaveChanges();
            //}

            #endregion


            //if (!context.Store_Requisition.Any())
            //{
            //    context.Store_Requisition.Add(new Store_Requisition
            //    {
            //        CID = "ISR/0001/May/2019",
            //        ID = 1,
            //        Active = true,
            //        User = "UserOne",
            //        Time = DateTime.Now.ToLocalTime(),
            //        Remarks = "Test Data",
            //        Description = "Description",
            //        Status = 1,
            //        CreationDate = DateTime.Now,
            //        CancellationRemaks = "Tested",
            //        ClosedByUserFK = 1,
            //        RequisitionType = 2,
            //        StoreInFK = 1,
            //        StoreOutFK = 1
            //    });
            //    context.Store_Requisition.Add(new Store_Requisition
            //    {
            //        CID = "ISR/0002/May/2019",
            //        ID = 2,
            //        Active = true,
            //        User = "UserOne",
            //        Time = DateTime.Now.ToLocalTime(),
            //        Remarks = "Test Data",
            //        Description = "Description",
            //        Status = 1,
            //        CreationDate = DateTime.Now,
            //        CancellationRemaks = "Tested",
            //        ClosedByUserFK = 1,
            //        RequisitionType = 2,
            //        StoreInFK = 1,
            //        StoreOutFK = 1
            //    });
            //    context.Store_Requisition.Add(new Store_Requisition
            //    {
            //        CID = "ISR/0003/May/2019",
            //        ID = 3,
            //        Active = true,
            //        User = "UserOne",
            //        Time = DateTime.Now.ToLocalTime(),
            //        Remarks = "Test Data",
            //        Description = "Description",
            //        Status = 1,
            //        CreationDate = DateTime.Now,
            //        CancellationRemaks = "Tested",
            //        ClosedByUserFK = 1,
            //        RequisitionType = 2,
            //        StoreInFK = 1,
            //        StoreOutFK = 1
            //    });
            //    context.SaveChanges();
            //}
            //if (!context.Store_RequisitionSlave.Any())
            //{
            //    context.Store_RequisitionSlave.Add(new Store_RequisitionSlave
            //    {
            //        ID = 1,
            //        Active = true,
            //        User = "UserOne",
            //        Time = DateTime.Now.ToLocalTime(),
            //        Remarks = "Test",
            //        Store_RequisitionFK = 1,
            //        Common_EmployeeFK = 1,
            //        RequiredDate = DateTime.Now,
            //        Common_RawItemFK = 1,
            //        Description = "Description",
            //        RequisitionQuantity = 100,
            //        InspectionRequired = true,
            //    });
            //    context.Store_RequisitionSlave.Add(new Store_RequisitionSlave
            //    {
            //        ID = 2,
            //        Active = true,
            //        User = "UserOne",
            //        Time = DateTime.Now.ToLocalTime(),
            //        Remarks = "Test",
            //        Store_RequisitionFK = 1,
            //        Common_EmployeeFK = 1,
            //        RequiredDate = DateTime.Now,
            //        Common_RawItemFK = 1,
            //        Description = "Description",
            //        RequisitionQuantity = 100,
            //        InspectionRequired = true,
            //    });
            //    context.Store_RequisitionSlave.Add(new Store_RequisitionSlave
            //    {
            //        ID = 3,
            //        Active = true,
            //        User = "UserOne",
            //        Time = DateTime.Now.ToLocalTime(),
            //        Remarks = "Test",
            //        Store_RequisitionFK = 1,
            //        Common_EmployeeFK = 1,
            //        RequiredDate = DateTime.Now,
            //        Common_RawItemFK = 1,
            //        Description = "Description",
            //        RequisitionQuantity = 100,
            //        InspectionRequired = true,
            //    });

            //    context.Store_RequisitionSlave.Add(new Store_RequisitionSlave
            //    {
            //        ID = 4,
            //        Active = true,
            //        User = "UserOne",
            //        Time = DateTime.Now.ToLocalTime(),
            //        Remarks = "Test",
            //        Store_RequisitionFK = 2,
            //        Common_EmployeeFK = 1,
            //        RequiredDate = DateTime.Now,
            //        Common_RawItemFK = 1,
            //        Description = "Description",
            //        RequisitionQuantity = 100,
            //        InspectionRequired = true,
            //    });
            //    context.Store_RequisitionSlave.Add(new Store_RequisitionSlave
            //    {
            //        ID = 5,
            //        Active = true,
            //        User = "UserOne",
            //        Time = DateTime.Now.ToLocalTime(),
            //        Remarks = "Test",
            //        Store_RequisitionFK = 2,
            //        Common_EmployeeFK = 1,
            //        RequiredDate = DateTime.Now,
            //        Common_RawItemFK = 1,
            //        Description = "Description",
            //        RequisitionQuantity = 100,
            //        InspectionRequired = true,
            //    });
            //    context.Store_RequisitionSlave.Add(new Store_RequisitionSlave
            //    {
            //        ID = 6,
            //        Active = true,
            //        User = "UserOne",
            //        Time = DateTime.Now.ToLocalTime(),
            //        Remarks = "Test",
            //        Store_RequisitionFK = 2,
            //        Common_EmployeeFK = 1,
            //        RequiredDate = DateTime.Now,
            //        Common_RawItemFK = 1,
            //        Description = "Description",
            //        RequisitionQuantity = 100,
            //        InspectionRequired = true,
            //    });
            //    context.SaveChanges();
            //}


            #region Emrul
            if (!context.Common_FinishCategory.Any())
            {
                context.Common_FinishCategory.Add(new Common_FinishCategory() { Name = "Boys", Time = DateTime.Now, Remarks = "Boys Knitted Vest" });
                context.Common_FinishCategory.Add(new Common_FinishCategory() { Name = "Girls", Time = DateTime.Now, Remarks = "Girls" });
                context.Common_FinishCategory.Add(new Common_FinishCategory() { Name = "Kids", Time = DateTime.Now, Remarks = "Kids T-Shirt" });
                context.Common_FinishCategory.Add(new Common_FinishCategory() { Name = "Junior", Time = DateTime.Now, Remarks = "Junior T-Shirt" });

                context.SaveChanges();
            }
            if (!context.Common_FinishSubCategory.Any())
            {
                context.Common_FinishSubCategory.Add(new Common_FinishSubCategory() { Common_FinishCategoryFK = 1, Name = "Boys", Time = DateTime.Now, Remarks = "Boys Knitted Vest" });
                context.Common_FinishSubCategory.Add(new Common_FinishSubCategory() { Common_FinishCategoryFK = 1, Name = "Boys Boxer", Time = DateTime.Now, Remarks = "Boys Boxer" });
                context.Common_FinishSubCategory.Add(new Common_FinishSubCategory() { Common_FinishCategoryFK = 1, Name = "Underwear set", Time = DateTime.Now, Remarks = "Underwear set" });

                context.Common_FinishSubCategory.Add(new Common_FinishSubCategory() { Common_FinishCategoryFK = 2, Name = "Girls", Time = DateTime.Now, Remarks = "Girls" });
                context.Common_FinishSubCategory.Add(new Common_FinishSubCategory() { Common_FinishCategoryFK = 2, Name = "GU 2PK THERMAL LEGGING", Time = DateTime.Now, Remarks = "GU 2PK THERMAL LEGGING" });
                context.SaveChanges();
            }
            if (!context.Common_FinishItem.Any())
            {
                context.Common_FinishItem.Add(new Common_FinishItem() { Common_FinishSubCategoryFK = 1, Common_UnitFK = 2, Name = "BOY'S BOXER", Time = DateTime.Now, Remarks = "N/A" });
                context.Common_FinishItem.Add(new Common_FinishItem() { Common_FinishSubCategoryFK = 1, Common_UnitFK = 2, Name = "BOY'S BRIEF", Time = DateTime.Now, Remarks = "N/A" });
                context.Common_FinishItem.Add(new Common_FinishItem() { Common_FinishSubCategoryFK = 2, Common_UnitFK = 2, Name = "DTR BU 2PK BATMAN TRUNKS", Time = DateTime.Now, Remarks = "N/A" });
                context.Common_FinishItem.Add(new Common_FinishItem() { Common_FinishSubCategoryFK = 2, Common_UnitFK = 2, Name = "Boys Trunk Harry Potter", Time = DateTime.Now, Remarks = "N/A" });
                context.Common_FinishItem.Add(new Common_FinishItem() { Common_FinishSubCategoryFK = 3, Common_UnitFK = 2, Name = "Boy's T-Shirt + Brief", Time = DateTime.Now, Remarks = "N/A" });
                context.Common_FinishItem.Add(new Common_FinishItem() { Common_FinishSubCategoryFK = 3, Common_UnitFK = 2, Name = "Boy's T-Shirt + Boxer", Time = DateTime.Now, Remarks = "N/A" });

                context.Common_FinishItem.Add(new Common_FinishItem() { Common_FinishSubCategoryFK = 4, Common_UnitFK = 2, Name = "Girls Knitted Panty", Time = DateTime.Now, Remarks = "N/A" });
                context.Common_FinishItem.Add(new Common_FinishItem() { Common_FinishSubCategoryFK = 4, Common_UnitFK = 2, Name = "Girls Legging", Time = DateTime.Now, Remarks = "N/A" });
                context.Common_FinishItem.Add(new Common_FinishItem() { Common_FinishSubCategoryFK = 5, Common_UnitFK = 2, Name = "GU 2PK THERMAL LEGGING", Time = DateTime.Now, Remarks = "N/A" });
                context.SaveChanges();
            }

            if (!context.Plan_PlanConfig.Any())
            {
                context.Plan_PlanConfig.Add(new Plan_PlanConfig() { Active = true, User = "OneUser", Time = DateTime.Now, CuttingBFShip = 30, SewingAFCutting = 3, PPSampleBFCutting = 20, AccessoriesINBFCutting = 15, FabricINBFCutting = 10, ReservedDate = 3, Remarks = "N/A", IsRunning = true });
                context.SaveChanges();
            }
            if (!context.Common_ProductionLine.Any())
            {
                context.Common_ProductionLine.Add(new Common_ProductionLine() { Active = true, User = "OneUser", Time = DateTime.Now, Name = "Line-A", ProductionStepFK = 2, Priority = 1, Remarks = "N/A" });
                context.Common_ProductionLine.Add(new Common_ProductionLine() { Active = true, User = "OneUser", Time = DateTime.Now, Name = "Line-B", ProductionStepFK = 2, Priority = 2, Remarks = "N/A" });
                context.Common_ProductionLine.Add(new Common_ProductionLine() { Active = true, User = "OneUser", Time = DateTime.Now, Name = "Line-C", ProductionStepFK = 2, Priority = 3, Remarks = "N/A" });
                context.Common_ProductionLine.Add(new Common_ProductionLine() { Active = true, User = "OneUser", Time = DateTime.Now, Name = "Line-D", ProductionStepFK = 2, Priority = 4, Remarks = "N/A" });
                context.Common_ProductionLine.Add(new Common_ProductionLine() { Active = true, User = "OneUser", Time = DateTime.Now, Name = "Line-E", ProductionStepFK = 2, Priority = 5, Remarks = "N/A" });
                context.Common_ProductionLine.Add(new Common_ProductionLine() { Active = true, User = "OneUser", Time = DateTime.Now, Name = "Line-F", ProductionStepFK = 2, Priority = 6, Remarks = "N/A" });
                context.SaveChanges();
            }
            if (!context.Common_LineChief.Any())
            {
                context.Common_LineChief.Add(new Common_LineChief() { Active = true, User = "OneUser", Time = DateTime.Now, Name = "Mikel" });
                context.SaveChanges();
            }
            if (!context.Common_LineSuperVisor.Any())
            {
                context.Common_LineSuperVisor.Add(new Common_LineSuperVisor() { Active = true, User = "OneUser", Time = DateTime.Now, Name = "Mikel" });
                context.SaveChanges();
            }
            if (!context.Common_Country.Any())
            {
                context.Common_Country.Add(new Common_Country() { Name = "USA", Time = DateTime.Now, User = "e" });
                context.Common_Country.Add(new Common_Country() { Name = "UK", Time = DateTime.Now, User = "e" });
                context.Common_Country.Add(new Common_Country() { Name = "UAE", Time = DateTime.Now, User = "e" });
                context.SaveChanges();
            }
            if (!context.Common_Color.Any())
            {
                context.Common_Color.Add(new Common_Color() { Name = "Red", Time = DateTime.Now, User = "e" });
                context.Common_Color.Add(new Common_Color() { Name = "Green", Time = DateTime.Now, User = "e" });
                context.Common_Color.Add(new Common_Color() { Name = "Blue", Time = DateTime.Now, User = "e" });
                context.Common_Color.Add(new Common_Color() { Name = "Gray", Time = DateTime.Now, User = "e" });
                context.Common_Color.Add(new Common_Color() { Name = "Black", Time = DateTime.Now, User = "e" });
                context.SaveChanges();
            }
            if (!context.Common_Size.Any())
            {
                context.Common_Size.Add(new Common_Size() { Name = "S", Time = DateTime.Now, User = "e" });
                context.Common_Size.Add(new Common_Size() { Name = "M", Time = DateTime.Now, User = "e" });
                context.Common_Size.Add(new Common_Size() { Name = "L", Time = DateTime.Now, User = "e" });
                context.Common_Size.Add(new Common_Size() { Name = "XL", Time = DateTime.Now, User = "e" });
                context.Common_Size.Add(new Common_Size() { Name = "XXL", Time = DateTime.Now, User = "e" });
                context.SaveChanges();
            }
            #endregion
        }
    }
}
