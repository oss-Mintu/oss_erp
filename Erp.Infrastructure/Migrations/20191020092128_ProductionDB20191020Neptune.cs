﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Erp.Infrastructure.Migrations
{
    public partial class ProductionDB20191020Neptune : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "AccountHead",
                table: "Common_RawItem",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CategoryTypeFK",
                table: "Common_RawCategory",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "ConversionRateToBDT",
                table: "Accounting_JournalSlave",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "ForeignCurrencyAmount",
                table: "Accounting_JournalSlave",
                nullable: false,
                defaultValue: 0m);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AccountHead",
                table: "Common_RawItem");

            migrationBuilder.DropColumn(
                name: "CategoryTypeFK",
                table: "Common_RawCategory");

            migrationBuilder.DropColumn(
                name: "ConversionRateToBDT",
                table: "Accounting_JournalSlave");

            migrationBuilder.DropColumn(
                name: "ForeignCurrencyAmount",
                table: "Accounting_JournalSlave");
        }
    }
}
