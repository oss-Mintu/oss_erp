﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Erp.Infrastructure.Migrations
{
    public partial class ProductionDB2019070310am : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "ProcessLoss",
                table: "Merchandising_YarnCalculation",
                type: "decimal(18,5)",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "Price",
                table: "Merchandising_YarnCalculation",
                type: "decimal(18,5)",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "Lycra",
                table: "Merchandising_YarnCalculation",
                type: "decimal(18,5)",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "Consumption",
                table: "Merchandising_YarnCalculation",
                type: "decimal(18,5)",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.AddColumn<string>(
                name: "ImagePath",
                table: "HRMS_Employee",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "User_DepartmentFK",
                table: "HRMS_Designation",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Procurement_PurchaseInvoice",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    CID = table.Column<string>(nullable: true),
                    Common_SupplierFK = table.Column<int>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Status = table.Column<int>(nullable: false),
                    CreationDate = table.Column<DateTime>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    InvoiceDate = table.Column<DateTime>(nullable: false),
                    ApprovedDate = table.Column<DateTime>(nullable: false),
                    PaymentMethod = table.Column<int>(nullable: false),
                    CancellationRemaks = table.Column<string>(nullable: true),
                    ClosedByUserFK = table.Column<int>(nullable: true),
                    TotalInvoiceValue = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Procurement_PurchaseInvoice", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Procurement_PurchaseInvoice_User_User_ClosedByUserFK",
                        column: x => x.ClosedByUserFK,
                        principalTable: "User_User",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Procurement_PurchaseInvoice_Common_Supplier_Common_SupplierFK",
                        column: x => x.Common_SupplierFK,
                        principalTable: "Common_Supplier",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Procurement_PurchaseInvoiceSlave",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    InvoiceQuantity = table.Column<double>(nullable: false),
                    InvoicePrice = table.Column<double>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    Procurement_PurchaseInvoiceFK = table.Column<int>(nullable: false),
                    Procurement_PurchaseOrderFK = table.Column<int>(nullable: true),
                    Store_StockInFK = table.Column<int>(nullable: true),
                    Common_RawItemFK = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Procurement_PurchaseInvoiceSlave", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Procurement_PurchaseInvoiceSlave_Common_RawItem_Common_RawItemFK",
                        column: x => x.Common_RawItemFK,
                        principalTable: "Common_RawItem",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Procurement_PurchaseInvoiceSlave_Procurement_PurchaseInvoice_Procurement_PurchaseInvoiceFK",
                        column: x => x.Procurement_PurchaseInvoiceFK,
                        principalTable: "Procurement_PurchaseInvoice",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Procurement_PurchaseInvoiceSlave_Procurement_PurchaseOrder_Procurement_PurchaseOrderFK",
                        column: x => x.Procurement_PurchaseOrderFK,
                        principalTable: "Procurement_PurchaseOrder",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Procurement_PurchaseInvoiceSlave_Store_StockIn_Store_StockInFK",
                        column: x => x.Store_StockInFK,
                        principalTable: "Store_StockIn",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_HRMS_Designation_User_DepartmentFK",
                table: "HRMS_Designation",
                column: "User_DepartmentFK");

            migrationBuilder.CreateIndex(
                name: "IX_Procurement_PurchaseInvoice_ClosedByUserFK",
                table: "Procurement_PurchaseInvoice",
                column: "ClosedByUserFK");

            migrationBuilder.CreateIndex(
                name: "IX_Procurement_PurchaseInvoice_Common_SupplierFK",
                table: "Procurement_PurchaseInvoice",
                column: "Common_SupplierFK");

            migrationBuilder.CreateIndex(
                name: "IX_Procurement_PurchaseInvoiceSlave_Common_RawItemFK",
                table: "Procurement_PurchaseInvoiceSlave",
                column: "Common_RawItemFK");

            migrationBuilder.CreateIndex(
                name: "IX_Procurement_PurchaseInvoiceSlave_Procurement_PurchaseInvoiceFK",
                table: "Procurement_PurchaseInvoiceSlave",
                column: "Procurement_PurchaseInvoiceFK");

            migrationBuilder.CreateIndex(
                name: "IX_Procurement_PurchaseInvoiceSlave_Procurement_PurchaseOrderFK",
                table: "Procurement_PurchaseInvoiceSlave",
                column: "Procurement_PurchaseOrderFK");

            migrationBuilder.CreateIndex(
                name: "IX_Procurement_PurchaseInvoiceSlave_Store_StockInFK",
                table: "Procurement_PurchaseInvoiceSlave",
                column: "Store_StockInFK");

            migrationBuilder.AddForeignKey(
                name: "FK_HRMS_Designation_User_Department_User_DepartmentFK",
                table: "HRMS_Designation",
                column: "User_DepartmentFK",
                principalTable: "User_Department",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_HRMS_Designation_User_Department_User_DepartmentFK",
                table: "HRMS_Designation");

            migrationBuilder.DropTable(
                name: "Procurement_PurchaseInvoiceSlave");

            migrationBuilder.DropTable(
                name: "Procurement_PurchaseInvoice");

            migrationBuilder.DropIndex(
                name: "IX_HRMS_Designation_User_DepartmentFK",
                table: "HRMS_Designation");

            migrationBuilder.DropColumn(
                name: "ImagePath",
                table: "HRMS_Employee");

            migrationBuilder.DropColumn(
                name: "User_DepartmentFK",
                table: "HRMS_Designation");

            migrationBuilder.AlterColumn<decimal>(
                name: "ProcessLoss",
                table: "Merchandising_YarnCalculation",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,5)");

            migrationBuilder.AlterColumn<decimal>(
                name: "Price",
                table: "Merchandising_YarnCalculation",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,5)");

            migrationBuilder.AlterColumn<decimal>(
                name: "Lycra",
                table: "Merchandising_YarnCalculation",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,5)");

            migrationBuilder.AlterColumn<decimal>(
                name: "Consumption",
                table: "Merchandising_YarnCalculation",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,5)");
        }
    }
}
