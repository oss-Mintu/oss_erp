﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Erp.Infrastructure.Migrations
{
    public partial class ProductionDB20191014Saturn : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Common_LineDevice_Common_ProductionLine_Common_ProductionLineID",
                table: "Common_LineDevice");

            migrationBuilder.DropIndex(
                name: "IX_Common_LineDevice_Common_ProductionLineID",
                table: "Common_LineDevice");

            migrationBuilder.DropColumn(
                name: "Common_ProductionLineID",
                table: "Common_LineDevice");

            migrationBuilder.AddColumn<decimal>(
                name: "ConversionRateToBDT",
                table: "Common_Currency",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.CreateTable(
                name: "Common_CurrencyChangesLog",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Common_CurrencyFK = table.Column<int>(nullable: false),
                    PreviousConversionRateToBDT = table.Column<decimal>(nullable: false),
                    ChangesConversionRateToBDT = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_CurrencyChangesLog", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Common_CurrencyChangesLog_Common_Currency_Common_CurrencyFK",
                        column: x => x.Common_CurrencyFK,
                        principalTable: "Common_Currency",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Prod_QCOrder",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Prod_QCTypeFK = table.Column<int>(nullable: false),
                    Merchandising_StyleFK = table.Column<int>(nullable: false),
                    Priority = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Prod_QCOrder", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Prod_QCType",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    IsDeprecate = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Prod_QCType", x => x.ID);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Common_CurrencyChangesLog_Common_CurrencyFK",
                table: "Common_CurrencyChangesLog",
                column: "Common_CurrencyFK");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Common_CurrencyChangesLog");

            migrationBuilder.DropTable(
                name: "Prod_QCOrder");

            migrationBuilder.DropTable(
                name: "Prod_QCType");

            migrationBuilder.DropColumn(
                name: "ConversionRateToBDT",
                table: "Common_Currency");

            migrationBuilder.AddColumn<int>(
                name: "Common_ProductionLineID",
                table: "Common_LineDevice",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Common_LineDevice_Common_ProductionLineID",
                table: "Common_LineDevice",
                column: "Common_ProductionLineID");

            migrationBuilder.AddForeignKey(
                name: "FK_Common_LineDevice_Common_ProductionLine_Common_ProductionLineID",
                table: "Common_LineDevice",
                column: "Common_ProductionLineID",
                principalTable: "Common_ProductionLine",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
