﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Erp.Infrastructure.Migrations
{
    public partial class ProductionDB20190825Spider : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Date",
                table: "Mis_IncomeStatement",
                newName: "ToDate");

            migrationBuilder.RenameColumn(
                name: "Date",
                table: "Mis_BalanceSheet",
                newName: "ToDate");

            migrationBuilder.AddColumn<bool>(
                name: "IsMultiLayerOption",
                table: "User_MenuItem",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "FromDate",
                table: "Mis_IncomeStatement",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "FromDate",
                table: "Mis_BalanceSheet",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "CID",
                table: "Commercial_ECI",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CID",
                table: "Commercial_BBLC",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsMultiLayerOption",
                table: "User_MenuItem");

            migrationBuilder.DropColumn(
                name: "FromDate",
                table: "Mis_IncomeStatement");

            migrationBuilder.DropColumn(
                name: "FromDate",
                table: "Mis_BalanceSheet");

            migrationBuilder.DropColumn(
                name: "CID",
                table: "Commercial_ECI");

            migrationBuilder.DropColumn(
                name: "CID",
                table: "Commercial_BBLC");

            migrationBuilder.RenameColumn(
                name: "ToDate",
                table: "Mis_IncomeStatement",
                newName: "Date");

            migrationBuilder.RenameColumn(
                name: "ToDate",
                table: "Mis_BalanceSheet",
                newName: "Date");
        }
    }
}
