﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Erp.Infrastructure.Migrations
{
    public partial class ProductionDB2019071012pm : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Shipment_DeliveryChallanSlave_Shipment_DeliveryChallan_Shipment_DeliveryChallanFk",
                table: "Shipment_DeliveryChallanSlave");

            migrationBuilder.DropForeignKey(
                name: "FK_Shipment_DeliveryChallanSlave_Shipment_ShipmentInstruction_Shipment_ShipmentInstructionFk",
                table: "Shipment_DeliveryChallanSlave");

            migrationBuilder.DropForeignKey(
                name: "FK_Store_SRequisitionSlave_Store_SRequisition_Store_RequisitionFK",
                table: "Store_SRequisitionSlave");

            migrationBuilder.DropTable(
                name: "Payroll_GeneratedSections");

            migrationBuilder.DropIndex(
                name: "IX_Shipment_DeliveryChallanSlave_Shipment_DeliveryChallanFk",
                table: "Shipment_DeliveryChallanSlave");

            migrationBuilder.DropIndex(
                name: "IX_Shipment_DeliveryChallanSlave_Shipment_ShipmentInstructionFk",
                table: "Shipment_DeliveryChallanSlave");

            migrationBuilder.DropColumn(
                name: "Common_CountryFk",
                table: "Shipment_ShipmentInvoice");

            migrationBuilder.DropColumn(
                name: "Common_CountryOfOriginFk",
                table: "Shipment_ShipmentInvoice");

            migrationBuilder.DropColumn(
                name: "FirstCreatedBy",
                table: "Shipment_ShipmentInvoice");

            migrationBuilder.DropColumn(
                name: "IsIncrease",
                table: "Shipment_ShipmentInvoice");

            migrationBuilder.DropColumn(
                name: "LastEditedBy",
                table: "Shipment_ShipmentInvoice");

            migrationBuilder.DropColumn(
                name: "Shipment_PortOfDischargeFk",
                table: "Shipment_ShipmentInvoice");

            migrationBuilder.DropColumn(
                name: "Common_SupplierFk",
                table: "Shipment_DeliveryChallan");

            migrationBuilder.DropColumn(
                name: "DeliverdQty",
                table: "Shipment_DeliveryChallan");

            migrationBuilder.DropColumn(
                name: "Description",
                table: "Shipment_DeliveryChallan");

            migrationBuilder.DropColumn(
                name: "PackQty",
                table: "Shipment_DeliveryChallan");

            migrationBuilder.RenameColumn(
                name: "Store_RequisitionFK",
                table: "Store_SRequisitionSlave",
                newName: "Store_SRequisitionID");

            migrationBuilder.RenameIndex(
                name: "IX_Store_SRequisitionSlave_Store_RequisitionFK",
                table: "Store_SRequisitionSlave",
                newName: "IX_Store_SRequisitionSlave_Store_SRequisitionID");

            migrationBuilder.RenameColumn(
                name: "Shipment_ShipmentInstructionFk",
                table: "Shipment_ShipmentInvoiceSlave",
                newName: "Shipment_InstructionSlaveFk");

            migrationBuilder.RenameColumn(
                name: "TermsOfShipment",
                table: "Shipment_ShipmentInvoice",
                newName: "Shipment_TermsOfShipmentFK");

            migrationBuilder.RenameColumn(
                name: "IncDecDescription",
                table: "Shipment_ShipmentInvoice",
                newName: "AdjustedNote");

            migrationBuilder.RenameColumn(
                name: "IncDecAmount",
                table: "Shipment_ShipmentInvoice",
                newName: "AdjustedValue");

            migrationBuilder.RenameColumn(
                name: "Quantity",
                table: "Shipment_ShipmentInstructionSlave",
                newName: "InstructionQuantity");

            migrationBuilder.RenameColumn(
                name: "Shipment_ShipmentInstructionFk",
                table: "Shipment_DeliveryChallanSlave",
                newName: "Shipment_InvoiceSlaveFk");

            migrationBuilder.RenameColumn(
                name: "TransporterCommon_SupplierFk",
                table: "Shipment_DeliveryChallan",
                newName: "Common_TransporterSupplierFk");

            migrationBuilder.RenameColumn(
                name: "CtnQty",
                table: "Shipment_DeliveryChallan",
                newName: "Common_CNFSupplierFk");

            migrationBuilder.RenameColumn(
                name: "OverTimeHours",
                table: "Payroll_PayrollDetails",
                newName: "PayableOverTimeAmount");

            migrationBuilder.RenameColumn(
                name: "OverTimeAmount",
                table: "Payroll_PayrollDetails",
                newName: "NightBill");

            migrationBuilder.RenameColumn(
                name: "QntyPcs",
                table: "Merchandising_YarnCalculation",
                newName: "OrderQuantityPCS");

            migrationBuilder.AddColumn<int>(
                name: "Common_ColorFK",
                table: "Store_StockOut",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Common_RawItemFK",
                table: "Store_StockOut",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Common_SizeFK",
                table: "Store_StockOut",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<bool>(
                name: "IsLocked",
                table: "Store_StockOut",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "Merchandising_StyleFK",
                table: "Store_StockOut",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Merchandising_StyleSlaveFK",
                table: "Store_StockOut",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Procurement_PurchaseOrderSlaveFK",
                table: "Store_StockOut",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<double>(
                name: "TotalPrice",
                table: "Store_StockOut",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "UnitPrice",
                table: "Store_StockOut",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<int>(
                name: "Common_ColorFK",
                table: "Store_StockIn",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Common_RawItemFK",
                table: "Store_StockIn",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<decimal>(
                name: "Common_Size",
                table: "Store_StockIn",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<bool>(
                name: "IsLocked",
                table: "Store_StockIn",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "Merchandising_StyleFK",
                table: "Store_StockIn",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Merchandising_StyleSlaveFK",
                table: "Store_StockIn",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Procurement_PurchaseOrderSlaveFK",
                table: "Store_StockIn",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<double>(
                name: "PurchaseQuantity",
                table: "Store_StockIn",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "TotalPrice",
                table: "Store_StockIn",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "UnitPrice",
                table: "Store_StockIn",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<int>(
                name: "StoreInFK",
                table: "Store_SRequisition",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "StoreOutFK",
                table: "Store_SRequisition",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Common_StoreTypeFK",
                table: "Store_General",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "HRMS_BusinessUnitFK",
                table: "Store_General",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "HRMS_UnitFK",
                table: "Store_General",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "InvoicedQuantity",
                table: "Shipment_ShipmentInvoiceSlave",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Merchandising_StyleFk",
                table: "Shipment_ShipmentInvoiceSlave",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Merchandising_StyleShipmentScheduleFk",
                table: "Shipment_ShipmentInvoiceSlave",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<string>(
                name: "InvoiceIdNo",
                table: "Shipment_ShipmentInvoice",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 100,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "ErcNo",
                table: "Shipment_ShipmentInvoice",
                maxLength: 15,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "BLNo",
                table: "Shipment_ShipmentInvoice",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 150,
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "Merchandising_StyleShipmentScheduleFk",
                table: "Shipment_ShipmentInstructionSlave",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "DeliveryChallanQuantity",
                table: "Shipment_DeliveryChallanSlave",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Merchandising_StyleFk",
                table: "Shipment_DeliveryChallanSlave",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Merchandising_StyleShipmentScheduleFk",
                table: "Shipment_DeliveryChallanSlave",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Shipment_InstructionSlaveFk",
                table: "Shipment_DeliveryChallanSlave",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "HRMS_BusinessUnitFK",
                table: "Payroll_PayrollMaster",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Status",
                table: "Payroll_PayrollMaster",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ApprovalStatus",
                table: "Payroll_PayrollDetails",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "EligibleFullNight",
                table: "Payroll_PayrollDetails",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "EligibleHalfNight",
                table: "Payroll_PayrollDetails",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "EligibleHoliday",
                table: "Payroll_PayrollDetails",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<decimal>(
                name: "ExtraOverTimeAmount",
                table: "Payroll_PayrollDetails",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<int>(
                name: "ExtraOverTimeHours",
                table: "Payroll_PayrollDetails",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<decimal>(
                name: "HolidayBill",
                table: "Payroll_PayrollDetails",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "HolidayRate",
                table: "Payroll_PayrollDetails",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<int>(
                name: "LateDays",
                table: "Payroll_PayrollDetails",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "PayableOverTimeHours",
                table: "Payroll_PayrollDetails",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "PaymentStatus",
                table: "Payroll_PayrollDetails",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "BillEligibily",
                table: "Payroll_EODRecordMaster",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "PreviousSalaryRecord",
                table: "Payroll_EODRecordMaster",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "SalaryStructureFk",
                table: "Payroll_EODRecordMaster",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Status",
                table: "Payroll_EODRecordMaster",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "Date",
                table: "Mis_CMEarned",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Merchandising_StyleFK",
                table: "Mis_CMEarned",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "SelfReferenceFK",
                table: "Merchandising_StyleSlave",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "MakingCost",
                table: "Merchandising_Style",
                type: "decimal(18,5)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "OverheadRate",
                table: "Merchandising_Style",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<int>(
                name: "Common_AgentFK",
                table: "Merchandising_BuyerOrder",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Common_InspectionAgentFK",
                table: "Merchandising_BuyerOrder",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<decimal>(
                name: "RequiredQuantity",
                table: "Merchandising_BOF",
                type: "decimal(18,5)",
                nullable: true,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "Price",
                table: "Merchandising_BOF",
                type: "decimal(18,5)",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "Merchandising_BOF",
                maxLength: 200,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 500,
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "Consumption",
                table: "Merchandising_BOF",
                type: "decimal(18,5)",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.AddColumn<int>(
                name: "Common_UnitFK",
                table: "Merchandising_BOF",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsFabric",
                table: "Merchandising_BOF",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<decimal>(
                name: "NightBill",
                table: "HRMS_Designation",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<int>(
                name: "BaseHeadId",
                table: "Accounting_Head",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "HeadType",
                table: "Accounting_Head",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Common_CompanySetup",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    CompanyName = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    ContactPerson = table.Column<string>(nullable: true),
                    AddressLine1 = table.Column<string>(nullable: true),
                    AddressLine2 = table.Column<string>(nullable: true),
                    AddressLine3 = table.Column<string>(nullable: true),
                    AddressLine4 = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_CompanySetup", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "HRMS_EmployeeImage",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    HRMS_EmployeeFK = table.Column<int>(nullable: false),
                    Image = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HRMS_EmployeeImage", x => x.ID);
                    table.ForeignKey(
                        name: "FK_HRMS_EmployeeImage_HRMS_Employee_HRMS_EmployeeFK",
                        column: x => x.HRMS_EmployeeFK,
                        principalTable: "HRMS_Employee",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Payroll_BonusMaster",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    BonusTitle = table.Column<string>(nullable: true),
                    PaymentDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Payroll_BonusMaster", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Payroll_BonusSettings",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    FromMonth = table.Column<int>(nullable: false),
                    ToMonth = table.Column<int>(nullable: false),
                    BonusRate = table.Column<decimal>(nullable: false),
                    IsBasicOrGross = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Payroll_BonusSettings", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Payroll_EmployeePromotionalHistory",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    HRMS_EmployeeFK = table.Column<int>(nullable: false),
                    HRMS_SectionFK = table.Column<int>(nullable: true),
                    HRMS_DesignationFK = table.Column<int>(nullable: true),
                    Common_ProductionLineFK = table.Column<int>(nullable: true),
                    Grade = table.Column<string>(nullable: true),
                    PromotionType = table.Column<int>(nullable: false),
                    PromotionDate = table.Column<DateTime>(nullable: false),
                    PreviousPromotionDate = table.Column<DateTime>(nullable: false),
                    Payroll_EODRecordMasterFk = table.Column<int>(nullable: true),
                    PreviousPayroll_EODRecordMasterFk = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Payroll_EmployeePromotionalHistory", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Payroll_EmployeePromotionalHistory_Common_ProductionLine_Common_ProductionLineFK",
                        column: x => x.Common_ProductionLineFK,
                        principalTable: "Common_ProductionLine",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Payroll_EmployeePromotionalHistory_HRMS_Designation_HRMS_DesignationFK",
                        column: x => x.HRMS_DesignationFK,
                        principalTable: "HRMS_Designation",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Payroll_EmployeePromotionalHistory_HRMS_Employee_HRMS_EmployeeFK",
                        column: x => x.HRMS_EmployeeFK,
                        principalTable: "HRMS_Employee",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Payroll_EmployeePromotionalHistory_HRMS_Section_HRMS_SectionFK",
                        column: x => x.HRMS_SectionFK,
                        principalTable: "HRMS_Section",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Payroll_EmployeePromotionalHistory_Payroll_EODRecordMaster_Payroll_EODRecordMasterFk",
                        column: x => x.Payroll_EODRecordMasterFk,
                        principalTable: "Payroll_EODRecordMaster",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Payroll_EmployeePromotionalHistory_Payroll_EODRecordMaster_PreviousPayroll_EODRecordMasterFk",
                        column: x => x.PreviousPayroll_EODRecordMasterFk,
                        principalTable: "Payroll_EODRecordMaster",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Payroll_SalaryStructure",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    StructerType = table.Column<int>(nullable: false),
                    MedicalAllowance = table.Column<decimal>(nullable: false),
                    FoodlAllowance = table.Column<decimal>(nullable: false),
                    TransportAllowance = table.Column<decimal>(nullable: false),
                    StampCharge = table.Column<decimal>(nullable: false),
                    BasicInPercent = table.Column<decimal>(nullable: false),
                    MonthlyWorkingDays = table.Column<int>(nullable: false),
                    DailyWorkingHour = table.Column<int>(nullable: false),
                    OvertimeRate = table.Column<decimal>(nullable: false),
                    HouseRentInPercent = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Payroll_SalaryStructure", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Payroll_BonusDetails",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Payroll_BonusMasterFk = table.Column<int>(nullable: false),
                    HRMS_EmployeeFK = table.Column<int>(nullable: false),
                    GrossSalary = table.Column<decimal>(nullable: false),
                    BasicSalary = table.Column<decimal>(nullable: false),
                    StampCharge = table.Column<decimal>(nullable: false),
                    JoiningDate = table.Column<DateTime>(nullable: false),
                    Tenure = table.Column<string>(nullable: true),
                    BonusRate = table.Column<decimal>(nullable: false),
                    BonusAmount = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Payroll_BonusDetails", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Payroll_BonusDetails_HRMS_Employee_HRMS_EmployeeFK",
                        column: x => x.HRMS_EmployeeFK,
                        principalTable: "HRMS_Employee",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Payroll_BonusDetails_Payroll_BonusMaster_Payroll_BonusMasterFk",
                        column: x => x.Payroll_BonusMasterFk,
                        principalTable: "Payroll_BonusMaster",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Store_SRequisitionSlave_Store_SRequisitionFK",
                table: "Store_SRequisitionSlave",
                column: "Store_SRequisitionFK");

            migrationBuilder.CreateIndex(
                name: "IX_Payroll_PayrollMaster_HRMS_BusinessUnitFK",
                table: "Payroll_PayrollMaster",
                column: "HRMS_BusinessUnitFK");

            migrationBuilder.CreateIndex(
                name: "IX_Payroll_EODRecordMaster_SalaryStructureFk",
                table: "Payroll_EODRecordMaster",
                column: "SalaryStructureFk");

            migrationBuilder.CreateIndex(
                name: "IX_HRMS_EmployeeImage_HRMS_EmployeeFK",
                table: "HRMS_EmployeeImage",
                column: "HRMS_EmployeeFK");

            migrationBuilder.CreateIndex(
                name: "IX_Payroll_BonusDetails_HRMS_EmployeeFK",
                table: "Payroll_BonusDetails",
                column: "HRMS_EmployeeFK");

            migrationBuilder.CreateIndex(
                name: "IX_Payroll_BonusDetails_Payroll_BonusMasterFk",
                table: "Payroll_BonusDetails",
                column: "Payroll_BonusMasterFk");

            migrationBuilder.CreateIndex(
                name: "IX_Payroll_EmployeePromotionalHistory_Common_ProductionLineFK",
                table: "Payroll_EmployeePromotionalHistory",
                column: "Common_ProductionLineFK");

            migrationBuilder.CreateIndex(
                name: "IX_Payroll_EmployeePromotionalHistory_HRMS_DesignationFK",
                table: "Payroll_EmployeePromotionalHistory",
                column: "HRMS_DesignationFK");

            migrationBuilder.CreateIndex(
                name: "IX_Payroll_EmployeePromotionalHistory_HRMS_EmployeeFK",
                table: "Payroll_EmployeePromotionalHistory",
                column: "HRMS_EmployeeFK");

            migrationBuilder.CreateIndex(
                name: "IX_Payroll_EmployeePromotionalHistory_HRMS_SectionFK",
                table: "Payroll_EmployeePromotionalHistory",
                column: "HRMS_SectionFK");

            migrationBuilder.CreateIndex(
                name: "IX_Payroll_EmployeePromotionalHistory_Payroll_EODRecordMasterFk",
                table: "Payroll_EmployeePromotionalHistory",
                column: "Payroll_EODRecordMasterFk");

            migrationBuilder.CreateIndex(
                name: "IX_Payroll_EmployeePromotionalHistory_PreviousPayroll_EODRecordMasterFk",
                table: "Payroll_EmployeePromotionalHistory",
                column: "PreviousPayroll_EODRecordMasterFk");

            migrationBuilder.AddForeignKey(
                name: "FK_Payroll_EODRecordMaster_Payroll_SalaryStructure_SalaryStructureFk",
                table: "Payroll_EODRecordMaster",
                column: "SalaryStructureFk",
                principalTable: "Payroll_SalaryStructure",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Payroll_PayrollMaster_HRMS_BusinessUnit_HRMS_BusinessUnitFK",
                table: "Payroll_PayrollMaster",
                column: "HRMS_BusinessUnitFK",
                principalTable: "HRMS_BusinessUnit",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Store_SRequisitionSlave_Store_Requisition_Store_SRequisitionFK",
                table: "Store_SRequisitionSlave",
                column: "Store_SRequisitionFK",
                principalTable: "Store_Requisition",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Store_SRequisitionSlave_Store_SRequisition_Store_SRequisitionID",
                table: "Store_SRequisitionSlave",
                column: "Store_SRequisitionID",
                principalTable: "Store_SRequisition",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Payroll_EODRecordMaster_Payroll_SalaryStructure_SalaryStructureFk",
                table: "Payroll_EODRecordMaster");

            migrationBuilder.DropForeignKey(
                name: "FK_Payroll_PayrollMaster_HRMS_BusinessUnit_HRMS_BusinessUnitFK",
                table: "Payroll_PayrollMaster");

            migrationBuilder.DropForeignKey(
                name: "FK_Store_SRequisitionSlave_Store_Requisition_Store_SRequisitionFK",
                table: "Store_SRequisitionSlave");

            migrationBuilder.DropForeignKey(
                name: "FK_Store_SRequisitionSlave_Store_SRequisition_Store_SRequisitionID",
                table: "Store_SRequisitionSlave");

            migrationBuilder.DropTable(
                name: "Common_CompanySetup");

            migrationBuilder.DropTable(
                name: "HRMS_EmployeeImage");

            migrationBuilder.DropTable(
                name: "Payroll_BonusDetails");

            migrationBuilder.DropTable(
                name: "Payroll_BonusSettings");

            migrationBuilder.DropTable(
                name: "Payroll_EmployeePromotionalHistory");

            migrationBuilder.DropTable(
                name: "Payroll_SalaryStructure");

            migrationBuilder.DropTable(
                name: "Payroll_BonusMaster");

            migrationBuilder.DropIndex(
                name: "IX_Store_SRequisitionSlave_Store_SRequisitionFK",
                table: "Store_SRequisitionSlave");

            migrationBuilder.DropIndex(
                name: "IX_Payroll_PayrollMaster_HRMS_BusinessUnitFK",
                table: "Payroll_PayrollMaster");

            migrationBuilder.DropIndex(
                name: "IX_Payroll_EODRecordMaster_SalaryStructureFk",
                table: "Payroll_EODRecordMaster");

            migrationBuilder.DropColumn(
                name: "Common_ColorFK",
                table: "Store_StockOut");

            migrationBuilder.DropColumn(
                name: "Common_RawItemFK",
                table: "Store_StockOut");

            migrationBuilder.DropColumn(
                name: "Common_SizeFK",
                table: "Store_StockOut");

            migrationBuilder.DropColumn(
                name: "IsLocked",
                table: "Store_StockOut");

            migrationBuilder.DropColumn(
                name: "Merchandising_StyleFK",
                table: "Store_StockOut");

            migrationBuilder.DropColumn(
                name: "Merchandising_StyleSlaveFK",
                table: "Store_StockOut");

            migrationBuilder.DropColumn(
                name: "Procurement_PurchaseOrderSlaveFK",
                table: "Store_StockOut");

            migrationBuilder.DropColumn(
                name: "TotalPrice",
                table: "Store_StockOut");

            migrationBuilder.DropColumn(
                name: "UnitPrice",
                table: "Store_StockOut");

            migrationBuilder.DropColumn(
                name: "Common_ColorFK",
                table: "Store_StockIn");

            migrationBuilder.DropColumn(
                name: "Common_RawItemFK",
                table: "Store_StockIn");

            migrationBuilder.DropColumn(
                name: "Common_Size",
                table: "Store_StockIn");

            migrationBuilder.DropColumn(
                name: "IsLocked",
                table: "Store_StockIn");

            migrationBuilder.DropColumn(
                name: "Merchandising_StyleFK",
                table: "Store_StockIn");

            migrationBuilder.DropColumn(
                name: "Merchandising_StyleSlaveFK",
                table: "Store_StockIn");

            migrationBuilder.DropColumn(
                name: "Procurement_PurchaseOrderSlaveFK",
                table: "Store_StockIn");

            migrationBuilder.DropColumn(
                name: "PurchaseQuantity",
                table: "Store_StockIn");

            migrationBuilder.DropColumn(
                name: "TotalPrice",
                table: "Store_StockIn");

            migrationBuilder.DropColumn(
                name: "UnitPrice",
                table: "Store_StockIn");

            migrationBuilder.DropColumn(
                name: "StoreInFK",
                table: "Store_SRequisition");

            migrationBuilder.DropColumn(
                name: "StoreOutFK",
                table: "Store_SRequisition");

            migrationBuilder.DropColumn(
                name: "Common_StoreTypeFK",
                table: "Store_General");

            migrationBuilder.DropColumn(
                name: "HRMS_BusinessUnitFK",
                table: "Store_General");

            migrationBuilder.DropColumn(
                name: "HRMS_UnitFK",
                table: "Store_General");

            migrationBuilder.DropColumn(
                name: "InvoicedQuantity",
                table: "Shipment_ShipmentInvoiceSlave");

            migrationBuilder.DropColumn(
                name: "Merchandising_StyleFk",
                table: "Shipment_ShipmentInvoiceSlave");

            migrationBuilder.DropColumn(
                name: "Merchandising_StyleShipmentScheduleFk",
                table: "Shipment_ShipmentInvoiceSlave");

            migrationBuilder.DropColumn(
                name: "DeliveryChallanQuantity",
                table: "Shipment_DeliveryChallanSlave");

            migrationBuilder.DropColumn(
                name: "Merchandising_StyleFk",
                table: "Shipment_DeliveryChallanSlave");

            migrationBuilder.DropColumn(
                name: "Merchandising_StyleShipmentScheduleFk",
                table: "Shipment_DeliveryChallanSlave");

            migrationBuilder.DropColumn(
                name: "Shipment_InstructionSlaveFk",
                table: "Shipment_DeliveryChallanSlave");

            migrationBuilder.DropColumn(
                name: "HRMS_BusinessUnitFK",
                table: "Payroll_PayrollMaster");

            migrationBuilder.DropColumn(
                name: "Status",
                table: "Payroll_PayrollMaster");

            migrationBuilder.DropColumn(
                name: "ApprovalStatus",
                table: "Payroll_PayrollDetails");

            migrationBuilder.DropColumn(
                name: "EligibleFullNight",
                table: "Payroll_PayrollDetails");

            migrationBuilder.DropColumn(
                name: "EligibleHalfNight",
                table: "Payroll_PayrollDetails");

            migrationBuilder.DropColumn(
                name: "EligibleHoliday",
                table: "Payroll_PayrollDetails");

            migrationBuilder.DropColumn(
                name: "ExtraOverTimeAmount",
                table: "Payroll_PayrollDetails");

            migrationBuilder.DropColumn(
                name: "ExtraOverTimeHours",
                table: "Payroll_PayrollDetails");

            migrationBuilder.DropColumn(
                name: "HolidayBill",
                table: "Payroll_PayrollDetails");

            migrationBuilder.DropColumn(
                name: "HolidayRate",
                table: "Payroll_PayrollDetails");

            migrationBuilder.DropColumn(
                name: "LateDays",
                table: "Payroll_PayrollDetails");

            migrationBuilder.DropColumn(
                name: "PayableOverTimeHours",
                table: "Payroll_PayrollDetails");

            migrationBuilder.DropColumn(
                name: "PaymentStatus",
                table: "Payroll_PayrollDetails");

            migrationBuilder.DropColumn(
                name: "BillEligibily",
                table: "Payroll_EODRecordMaster");

            migrationBuilder.DropColumn(
                name: "PreviousSalaryRecord",
                table: "Payroll_EODRecordMaster");

            migrationBuilder.DropColumn(
                name: "SalaryStructureFk",
                table: "Payroll_EODRecordMaster");

            migrationBuilder.DropColumn(
                name: "Status",
                table: "Payroll_EODRecordMaster");

            migrationBuilder.DropColumn(
                name: "Date",
                table: "Mis_CMEarned");

            migrationBuilder.DropColumn(
                name: "Merchandising_StyleFK",
                table: "Mis_CMEarned");

            migrationBuilder.DropColumn(
                name: "SelfReferenceFK",
                table: "Merchandising_StyleSlave");

            migrationBuilder.DropColumn(
                name: "MakingCost",
                table: "Merchandising_Style");

            migrationBuilder.DropColumn(
                name: "OverheadRate",
                table: "Merchandising_Style");

            migrationBuilder.DropColumn(
                name: "Common_AgentFK",
                table: "Merchandising_BuyerOrder");

            migrationBuilder.DropColumn(
                name: "Common_InspectionAgentFK",
                table: "Merchandising_BuyerOrder");

            migrationBuilder.DropColumn(
                name: "Common_UnitFK",
                table: "Merchandising_BOF");

            migrationBuilder.DropColumn(
                name: "IsFabric",
                table: "Merchandising_BOF");

            migrationBuilder.DropColumn(
                name: "NightBill",
                table: "HRMS_Designation");

            migrationBuilder.DropColumn(
                name: "BaseHeadId",
                table: "Accounting_Head");

            migrationBuilder.DropColumn(
                name: "HeadType",
                table: "Accounting_Head");

            migrationBuilder.RenameColumn(
                name: "Store_SRequisitionID",
                table: "Store_SRequisitionSlave",
                newName: "Store_RequisitionFK");

            migrationBuilder.RenameIndex(
                name: "IX_Store_SRequisitionSlave_Store_SRequisitionID",
                table: "Store_SRequisitionSlave",
                newName: "IX_Store_SRequisitionSlave_Store_RequisitionFK");

            migrationBuilder.RenameColumn(
                name: "Shipment_InstructionSlaveFk",
                table: "Shipment_ShipmentInvoiceSlave",
                newName: "Shipment_ShipmentInstructionFk");

            migrationBuilder.RenameColumn(
                name: "Shipment_TermsOfShipmentFK",
                table: "Shipment_ShipmentInvoice",
                newName: "TermsOfShipment");

            migrationBuilder.RenameColumn(
                name: "AdjustedValue",
                table: "Shipment_ShipmentInvoice",
                newName: "IncDecAmount");

            migrationBuilder.RenameColumn(
                name: "AdjustedNote",
                table: "Shipment_ShipmentInvoice",
                newName: "IncDecDescription");

            migrationBuilder.RenameColumn(
                name: "InstructionQuantity",
                table: "Shipment_ShipmentInstructionSlave",
                newName: "Quantity");

            migrationBuilder.RenameColumn(
                name: "Shipment_InvoiceSlaveFk",
                table: "Shipment_DeliveryChallanSlave",
                newName: "Shipment_ShipmentInstructionFk");

            migrationBuilder.RenameColumn(
                name: "Common_TransporterSupplierFk",
                table: "Shipment_DeliveryChallan",
                newName: "TransporterCommon_SupplierFk");

            migrationBuilder.RenameColumn(
                name: "Common_CNFSupplierFk",
                table: "Shipment_DeliveryChallan",
                newName: "CtnQty");

            migrationBuilder.RenameColumn(
                name: "PayableOverTimeAmount",
                table: "Payroll_PayrollDetails",
                newName: "OverTimeHours");

            migrationBuilder.RenameColumn(
                name: "NightBill",
                table: "Payroll_PayrollDetails",
                newName: "OverTimeAmount");

            migrationBuilder.RenameColumn(
                name: "OrderQuantityPCS",
                table: "Merchandising_YarnCalculation",
                newName: "QntyPcs");

            migrationBuilder.AlterColumn<string>(
                name: "InvoiceIdNo",
                table: "Shipment_ShipmentInvoice",
                maxLength: 100,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "ErcNo",
                table: "Shipment_ShipmentInvoice",
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 15,
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "BLNo",
                table: "Shipment_ShipmentInvoice",
                maxLength: 150,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Common_CountryFk",
                table: "Shipment_ShipmentInvoice",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Common_CountryOfOriginFk",
                table: "Shipment_ShipmentInvoice",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "FirstCreatedBy",
                table: "Shipment_ShipmentInvoice",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<bool>(
                name: "IsIncrease",
                table: "Shipment_ShipmentInvoice",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "LastEditedBy",
                table: "Shipment_ShipmentInvoice",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Shipment_PortOfDischargeFk",
                table: "Shipment_ShipmentInvoice",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<int>(
                name: "Merchandising_StyleShipmentScheduleFk",
                table: "Shipment_ShipmentInstructionSlave",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "Common_SupplierFk",
                table: "Shipment_DeliveryChallan",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "DeliverdQty",
                table: "Shipment_DeliveryChallan",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "Shipment_DeliveryChallan",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PackQty",
                table: "Shipment_DeliveryChallan",
                nullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "RequiredQuantity",
                table: "Merchandising_BOF",
                nullable: true,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,5)",
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "Price",
                table: "Merchandising_BOF",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,5)");

            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "Merchandising_BOF",
                maxLength: 500,
                nullable: true,
                oldClrType: typeof(string),
                oldMaxLength: 200,
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "Consumption",
                table: "Merchandising_BOF",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,5)");

            migrationBuilder.CreateTable(
                name: "Payroll_GeneratedSections",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    HRMS_SectionFk = table.Column<int>(nullable: false),
                    Payroll_PayrollMasterFk = table.Column<int>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Payroll_GeneratedSections", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Payroll_GeneratedSections_Payroll_PayrollMaster_Payroll_PayrollMasterFk",
                        column: x => x.Payroll_PayrollMasterFk,
                        principalTable: "Payroll_PayrollMaster",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Shipment_DeliveryChallanSlave_Shipment_DeliveryChallanFk",
                table: "Shipment_DeliveryChallanSlave",
                column: "Shipment_DeliveryChallanFk");

            migrationBuilder.CreateIndex(
                name: "IX_Shipment_DeliveryChallanSlave_Shipment_ShipmentInstructionFk",
                table: "Shipment_DeliveryChallanSlave",
                column: "Shipment_ShipmentInstructionFk");

            migrationBuilder.CreateIndex(
                name: "IX_Payroll_GeneratedSections_Payroll_PayrollMasterFk",
                table: "Payroll_GeneratedSections",
                column: "Payroll_PayrollMasterFk");

            migrationBuilder.AddForeignKey(
                name: "FK_Shipment_DeliveryChallanSlave_Shipment_DeliveryChallan_Shipment_DeliveryChallanFk",
                table: "Shipment_DeliveryChallanSlave",
                column: "Shipment_DeliveryChallanFk",
                principalTable: "Shipment_DeliveryChallan",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Shipment_DeliveryChallanSlave_Shipment_ShipmentInstruction_Shipment_ShipmentInstructionFk",
                table: "Shipment_DeliveryChallanSlave",
                column: "Shipment_ShipmentInstructionFk",
                principalTable: "Shipment_ShipmentInstruction",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Store_SRequisitionSlave_Store_SRequisition_Store_RequisitionFK",
                table: "Store_SRequisitionSlave",
                column: "Store_RequisitionFK",
                principalTable: "Store_SRequisition",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
