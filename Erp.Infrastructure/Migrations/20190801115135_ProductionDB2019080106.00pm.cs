﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Erp.Infrastructure.Migrations
{
    public partial class ProductionDB201908010600pm : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CID",
                table: "Prod_ReferencePlan",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Merchandising_StyleSetPackFK",
                table: "Prod_ReferencePlan",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "ProductionDate",
                table: "Prod_ReferencePlan",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<int>(
                name: "Merchandising_StyleSetPackFK",
                table: "Plan_MasterOrderPlan",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Merchandising_StyleSetPackFK",
                table: "Plan_DraftOrderPlan",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Plan_OrderAction",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    ActionRange = table.Column<int>(nullable: false),
                    Priority = table.Column<int>(nullable: false),
                    IsStop = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Plan_OrderAction", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Plan_OrderActionStep",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Plan_OrderActionFK = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Priority = table.Column<int>(nullable: false),
                    DefaultRange = table.Column<int>(nullable: false),
                    SetColor = table.Column<string>(nullable: true),
                    ExtendedRange = table.Column<int>(nullable: false),
                    ExtendColor = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Plan_OrderActionStep", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Plan_OrderActionStep_Plan_OrderAction_Plan_OrderActionFK",
                        column: x => x.Plan_OrderActionFK,
                        principalTable: "Plan_OrderAction",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Plan_OrderProcess",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Plan_OrderActionStepFK = table.Column<int>(nullable: false),
                    Merchandising_StyleFK = table.Column<int>(nullable: false),
                    PlanStartDate = table.Column<DateTime>(nullable: false),
                    PlanFinishDate = table.Column<DateTime>(nullable: false),
                    ActualStartDate = table.Column<DateTime>(nullable: false),
                    ActualFinishDate = table.Column<DateTime>(nullable: false),
                    ExtendDate = table.Column<DateTime>(nullable: false),
                    AssignedTo = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Plan_OrderProcess", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Plan_OrderProcess_Merchandising_Style_Merchandising_StyleFK",
                        column: x => x.Merchandising_StyleFK,
                        principalTable: "Merchandising_Style",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Plan_OrderProcess_Plan_OrderActionStep_Plan_OrderActionStepFK",
                        column: x => x.Plan_OrderActionStepFK,
                        principalTable: "Plan_OrderActionStep",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Plan_OrderActionStep_Plan_OrderActionFK",
                table: "Plan_OrderActionStep",
                column: "Plan_OrderActionFK");

            migrationBuilder.CreateIndex(
                name: "IX_Plan_OrderProcess_Merchandising_StyleFK",
                table: "Plan_OrderProcess",
                column: "Merchandising_StyleFK");

            migrationBuilder.CreateIndex(
                name: "IX_Plan_OrderProcess_Plan_OrderActionStepFK",
                table: "Plan_OrderProcess",
                column: "Plan_OrderActionStepFK");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Plan_OrderProcess");

            migrationBuilder.DropTable(
                name: "Plan_OrderActionStep");

            migrationBuilder.DropTable(
                name: "Plan_OrderAction");

            migrationBuilder.DropColumn(
                name: "CID",
                table: "Prod_ReferencePlan");

            migrationBuilder.DropColumn(
                name: "Merchandising_StyleSetPackFK",
                table: "Prod_ReferencePlan");

            migrationBuilder.DropColumn(
                name: "ProductionDate",
                table: "Prod_ReferencePlan");

            migrationBuilder.DropColumn(
                name: "Merchandising_StyleSetPackFK",
                table: "Plan_MasterOrderPlan");

            migrationBuilder.DropColumn(
                name: "Merchandising_StyleSetPackFK",
                table: "Plan_DraftOrderPlan");
        }
    }
}
