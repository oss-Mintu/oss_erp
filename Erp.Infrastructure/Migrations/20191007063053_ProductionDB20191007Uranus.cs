﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Erp.Infrastructure.Migrations
{
    public partial class ProductionDB20191007Uranus : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Store_GeneralInFK",
                table: "Store_StockOut",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Store_GeneralOutFK",
                table: "Store_StockOut",
                nullable: true);

            migrationBuilder.AddColumn<double>(
                name: "RemainingQty",
                table: "Store_RequisitionSlave",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Store_GeneralInFK",
                table: "Store_StockOut");

            migrationBuilder.DropColumn(
                name: "Store_GeneralOutFK",
                table: "Store_StockOut");

            migrationBuilder.DropColumn(
                name: "RemainingQty",
                table: "Store_RequisitionSlave");
        }
    }
}
