﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Erp.Infrastructure.Migrations
{
    public partial class ProductionDB2019071505pm : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Common_DepartmentFk",
                table: "Mis_DailyAttendance",
                newName: "User_DepartmentFk");

            migrationBuilder.RenameColumn(
                name: "MasterLCValue",
                table: "Mis_BTBLCStatus",
                newName: "MasterLCTotalValue");

            migrationBuilder.RenameColumn(
                name: "MasterLCNo",
                table: "Mis_BTBLCStatus",
                newName: "UDNo");

            migrationBuilder.AddColumn<string>(
                name: "StyleName",
                table: "Mis_CMEarned",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UdID",
                table: "Mis_BTBLCStatus",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Common_SectionLineFK",
                table: "Common_ProductionLine",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Common_SectionLineID",
                table: "Common_ProductionLine",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsClosed",
                table: "Common_ProductionLine",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateTable(
                name: "Common_SectionLine",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: true),
                    BuildingFk = table.Column<int>(nullable: false),
                    FloorFk = table.Column<int>(nullable: false),
                    HRMS_UnitFK = table.Column<int>(nullable: false),
                    IsClosed = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_SectionLine", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Shipment_ExportRealisation",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Date = table.Column<DateTime>(nullable: false),
                    BillNo = table.Column<string>(maxLength: 100, nullable: true),
                    FDBC = table.Column<string>(maxLength: 100, nullable: true),
                    BillValue = table.Column<decimal>(nullable: false),
                    RealisedAmount = table.Column<decimal>(nullable: false),
                    DollarValue = table.Column<decimal>(nullable: false),
                    Shipment_BillOfExchangeFK = table.Column<int>(nullable: false),
                    FDBCDate = table.Column<DateTime>(nullable: false),
                    ValueDate = table.Column<DateTime>(nullable: false),
                    IsFinal = table.Column<bool>(nullable: false),
                    FcbParAmt = table.Column<decimal>(nullable: false),
                    Others = table.Column<decimal>(nullable: false),
                    BillPurchase = table.Column<decimal>(nullable: false),
                    ERQAmt = table.Column<decimal>(nullable: false),
                    FxTrading = table.Column<decimal>(nullable: false),
                    PcAdjust = table.Column<decimal>(nullable: false),
                    BuyingCommission = table.Column<decimal>(nullable: false),
                    FdrAmount = table.Column<decimal>(nullable: false),
                    CourierExpense = table.Column<decimal>(nullable: false),
                    RmgAmount = table.Column<decimal>(nullable: false),
                    AitAmount = table.Column<decimal>(nullable: false),
                    SodAcAmount = table.Column<decimal>(nullable: false),
                    CdAmount = table.Column<decimal>(nullable: false),
                    TimeLoanAmount = table.Column<decimal>(nullable: false),
                    DocHandlingCharge = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Shipment_ExportRealisation", x => x.ID);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Common_ProductionLine_Common_SectionLineID",
                table: "Common_ProductionLine",
                column: "Common_SectionLineID");

            migrationBuilder.AddForeignKey(
                name: "FK_Common_ProductionLine_Common_SectionLine_Common_SectionLineID",
                table: "Common_ProductionLine",
                column: "Common_SectionLineID",
                principalTable: "Common_SectionLine",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Common_ProductionLine_Common_SectionLine_Common_SectionLineID",
                table: "Common_ProductionLine");

            migrationBuilder.DropTable(
                name: "Common_SectionLine");

            migrationBuilder.DropTable(
                name: "Shipment_ExportRealisation");

            migrationBuilder.DropIndex(
                name: "IX_Common_ProductionLine_Common_SectionLineID",
                table: "Common_ProductionLine");

            migrationBuilder.DropColumn(
                name: "StyleName",
                table: "Mis_CMEarned");

            migrationBuilder.DropColumn(
                name: "UdID",
                table: "Mis_BTBLCStatus");

            migrationBuilder.DropColumn(
                name: "Common_SectionLineFK",
                table: "Common_ProductionLine");

            migrationBuilder.DropColumn(
                name: "Common_SectionLineID",
                table: "Common_ProductionLine");

            migrationBuilder.DropColumn(
                name: "IsClosed",
                table: "Common_ProductionLine");

            migrationBuilder.RenameColumn(
                name: "User_DepartmentFk",
                table: "Mis_DailyAttendance",
                newName: "Common_DepartmentFk");

            migrationBuilder.RenameColumn(
                name: "UDNo",
                table: "Mis_BTBLCStatus",
                newName: "MasterLCNo");

            migrationBuilder.RenameColumn(
                name: "MasterLCTotalValue",
                table: "Mis_BTBLCStatus",
                newName: "MasterLCValue");
        }
    }
}
