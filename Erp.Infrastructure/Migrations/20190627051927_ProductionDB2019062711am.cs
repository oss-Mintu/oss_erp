﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Erp.Infrastructure.Migrations
{
    public partial class ProductionDB2019062711am : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_HRMS_PoliceStation_HRMS_District_HRMS_DistrictFK",
                table: "HRMS_PoliceStation");

            migrationBuilder.DropForeignKey(
                name: "FK_HRMS_PostOffice_HRMS_PoliceStation_HRMS_PoliceStationFK",
                table: "HRMS_PostOffice");

            migrationBuilder.AlterColumn<decimal>(
                name: "Consumption",
                table: "Merchandising_StyleSlave",
                type: "decimal(18,5)",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.AddColumn<string>(
                name: "DestNo",
                table: "Merchandising_StyleShipmentSchedule",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Common_CurrencyFK",
                table: "Merchandising_Style",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<decimal>(
                name: "Consumption",
                table: "Merchandising_CBSSlave",
                type: "decimal(18,5)",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<int>(
                name: "HRMS_PoliceStationFK",
                table: "HRMS_PostOffice",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "Common_CountryFK",
                table: "HRMS_PostOffice",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "HRMS_DistrictFK",
                table: "HRMS_PostOffice",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "HRMS_DistrictFK",
                table: "HRMS_PoliceStation",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "Common_CountryFK",
                table: "HRMS_PoliceStation",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Common_CountryFK",
                table: "HRMS_District",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_HRMS_PostOffice_Common_CountryFK",
                table: "HRMS_PostOffice",
                column: "Common_CountryFK");

            migrationBuilder.CreateIndex(
                name: "IX_HRMS_PostOffice_HRMS_DistrictFK",
                table: "HRMS_PostOffice",
                column: "HRMS_DistrictFK");

            migrationBuilder.CreateIndex(
                name: "IX_HRMS_PoliceStation_Common_CountryFK",
                table: "HRMS_PoliceStation",
                column: "Common_CountryFK");

            migrationBuilder.CreateIndex(
                name: "IX_HRMS_District_Common_CountryFK",
                table: "HRMS_District",
                column: "Common_CountryFK");

            migrationBuilder.AddForeignKey(
                name: "FK_HRMS_District_Common_Country_Common_CountryFK",
                table: "HRMS_District",
                column: "Common_CountryFK",
                principalTable: "Common_Country",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_HRMS_PoliceStation_Common_Country_Common_CountryFK",
                table: "HRMS_PoliceStation",
                column: "Common_CountryFK",
                principalTable: "Common_Country",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_HRMS_PoliceStation_HRMS_District_HRMS_DistrictFK",
                table: "HRMS_PoliceStation",
                column: "HRMS_DistrictFK",
                principalTable: "HRMS_District",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_HRMS_PostOffice_Common_Country_Common_CountryFK",
                table: "HRMS_PostOffice",
                column: "Common_CountryFK",
                principalTable: "Common_Country",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_HRMS_PostOffice_HRMS_District_HRMS_DistrictFK",
                table: "HRMS_PostOffice",
                column: "HRMS_DistrictFK",
                principalTable: "HRMS_District",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_HRMS_PostOffice_HRMS_PoliceStation_HRMS_PoliceStationFK",
                table: "HRMS_PostOffice",
                column: "HRMS_PoliceStationFK",
                principalTable: "HRMS_PoliceStation",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_HRMS_District_Common_Country_Common_CountryFK",
                table: "HRMS_District");

            migrationBuilder.DropForeignKey(
                name: "FK_HRMS_PoliceStation_Common_Country_Common_CountryFK",
                table: "HRMS_PoliceStation");

            migrationBuilder.DropForeignKey(
                name: "FK_HRMS_PoliceStation_HRMS_District_HRMS_DistrictFK",
                table: "HRMS_PoliceStation");

            migrationBuilder.DropForeignKey(
                name: "FK_HRMS_PostOffice_Common_Country_Common_CountryFK",
                table: "HRMS_PostOffice");

            migrationBuilder.DropForeignKey(
                name: "FK_HRMS_PostOffice_HRMS_District_HRMS_DistrictFK",
                table: "HRMS_PostOffice");

            migrationBuilder.DropForeignKey(
                name: "FK_HRMS_PostOffice_HRMS_PoliceStation_HRMS_PoliceStationFK",
                table: "HRMS_PostOffice");

            migrationBuilder.DropIndex(
                name: "IX_HRMS_PostOffice_Common_CountryFK",
                table: "HRMS_PostOffice");

            migrationBuilder.DropIndex(
                name: "IX_HRMS_PostOffice_HRMS_DistrictFK",
                table: "HRMS_PostOffice");

            migrationBuilder.DropIndex(
                name: "IX_HRMS_PoliceStation_Common_CountryFK",
                table: "HRMS_PoliceStation");

            migrationBuilder.DropIndex(
                name: "IX_HRMS_District_Common_CountryFK",
                table: "HRMS_District");

            migrationBuilder.DropColumn(
                name: "DestNo",
                table: "Merchandising_StyleShipmentSchedule");

            migrationBuilder.DropColumn(
                name: "Common_CurrencyFK",
                table: "Merchandising_Style");

            migrationBuilder.DropColumn(
                name: "Common_CountryFK",
                table: "HRMS_PostOffice");

            migrationBuilder.DropColumn(
                name: "HRMS_DistrictFK",
                table: "HRMS_PostOffice");

            migrationBuilder.DropColumn(
                name: "Common_CountryFK",
                table: "HRMS_PoliceStation");

            migrationBuilder.DropColumn(
                name: "Common_CountryFK",
                table: "HRMS_District");

            migrationBuilder.AlterColumn<decimal>(
                name: "Consumption",
                table: "Merchandising_StyleSlave",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,5)");

            migrationBuilder.AlterColumn<decimal>(
                name: "Consumption",
                table: "Merchandising_CBSSlave",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,5)");

            migrationBuilder.AlterColumn<int>(
                name: "HRMS_PoliceStationFK",
                table: "HRMS_PostOffice",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "HRMS_DistrictFK",
                table: "HRMS_PoliceStation",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_HRMS_PoliceStation_HRMS_District_HRMS_DistrictFK",
                table: "HRMS_PoliceStation",
                column: "HRMS_DistrictFK",
                principalTable: "HRMS_District",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_HRMS_PostOffice_HRMS_PoliceStation_HRMS_PoliceStationFK",
                table: "HRMS_PostOffice",
                column: "HRMS_PoliceStationFK",
                principalTable: "HRMS_PoliceStation",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
