﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Erp.Infrastructure.Migrations
{
    public partial class ProductionDB20190929Battle : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Prod_ReferencePlanFollowup_Merchandising_StyleShipmentRatio_Merchandising_StyleShipmentRatioFK",
                table: "Prod_ReferencePlanFollowup");

            migrationBuilder.DropForeignKey(
                name: "FK_Prod_ReferencePlanFollowup_Merchandising_StyleShipmentSchedule_Merchandising_StyleShipmentScheduleFK",
                table: "Prod_ReferencePlanFollowup");

            migrationBuilder.DropForeignKey(
                name: "FK_Store_Bin_Common_RawItem_Common_RawItemFK",
                table: "Store_Bin");

            migrationBuilder.DropIndex(
                name: "IX_Store_Bin_Common_RawItemFK",
                table: "Store_Bin");

            migrationBuilder.DropIndex(
                name: "IX_Prod_ReferencePlanFollowup_Merchandising_StyleShipmentRatioFK",
                table: "Prod_ReferencePlanFollowup");

            migrationBuilder.DropIndex(
                name: "IX_Prod_ReferencePlanFollowup_Merchandising_StyleShipmentScheduleFK",
                table: "Prod_ReferencePlanFollowup");

            migrationBuilder.AddColumn<int>(
                name: "Merchandising_StyleSetPackFK",
                table: "Store_RequisitionSlave",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Prod_ReferencePlanFk",
                table: "Prod_DailyAchivement",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DeliveryAddress",
                table: "Procurement_PurchaseOrder",
                nullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "FinalDeliveryDate",
                table: "Mis_OrderConfirm",
                nullable: true,
                oldClrType: typeof(DateTime));

            migrationBuilder.AddColumn<int>(
                name: "Ironing",
                table: "Mis_DailyProduction",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "IroningDone",
                table: "Mis_DailyProduction",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Common_SizeFk",
                table: "Merchandising_StyleSlave",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "TotalGoodReceiveValue",
                table: "Common_Notification",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "TotalPOValue",
                table: "Common_Notification",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "TotalPRValue",
                table: "Common_Notification",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<int>(
                name: "BaseHeadId",
                table: "Commercial_BBLC",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "Store_ItemWiseConsumption",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Merchandising_StyleFK = table.Column<int>(nullable: false),
                    Common_RawItemFK = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    UnitName = table.Column<string>(nullable: true),
                    ConsumptionDz = table.Column<double>(nullable: false),
                    OrderConsumQty = table.Column<double>(nullable: false),
                    FinishQtyDz = table.Column<double>(nullable: false),
                    TotalRecMain = table.Column<double>(nullable: false),
                    TotalRecCutting = table.Column<double>(nullable: false),
                    TotalRecSewing = table.Column<double>(nullable: false),
                    TotalRecIroning = table.Column<double>(nullable: false),
                    TotalRecPacking = table.Column<double>(nullable: false),
                    TotalRecFinish = table.Column<double>(nullable: false),
                    TotalOutMain = table.Column<double>(nullable: false),
                    TotalOutCutting = table.Column<double>(nullable: false),
                    TotalOutSewing = table.Column<double>(nullable: false),
                    TotalOutIroning = table.Column<double>(nullable: false),
                    TotalOutPacking = table.Column<double>(nullable: false),
                    TotalOutFinish = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Store_ItemWiseConsumption", x => x.ID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Store_ItemWiseConsumption");

            migrationBuilder.DropColumn(
                name: "Merchandising_StyleSetPackFK",
                table: "Store_RequisitionSlave");

            migrationBuilder.DropColumn(
                name: "Prod_ReferencePlanFk",
                table: "Prod_DailyAchivement");

            migrationBuilder.DropColumn(
                name: "DeliveryAddress",
                table: "Procurement_PurchaseOrder");

            migrationBuilder.DropColumn(
                name: "Ironing",
                table: "Mis_DailyProduction");

            migrationBuilder.DropColumn(
                name: "IroningDone",
                table: "Mis_DailyProduction");

            migrationBuilder.DropColumn(
                name: "Common_SizeFk",
                table: "Merchandising_StyleSlave");

            migrationBuilder.DropColumn(
                name: "TotalGoodReceiveValue",
                table: "Common_Notification");

            migrationBuilder.DropColumn(
                name: "TotalPOValue",
                table: "Common_Notification");

            migrationBuilder.DropColumn(
                name: "TotalPRValue",
                table: "Common_Notification");

            migrationBuilder.DropColumn(
                name: "BaseHeadId",
                table: "Commercial_BBLC");

            migrationBuilder.AlterColumn<DateTime>(
                name: "FinalDeliveryDate",
                table: "Mis_OrderConfirm",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Store_Bin_Common_RawItemFK",
                table: "Store_Bin",
                column: "Common_RawItemFK");

            migrationBuilder.CreateIndex(
                name: "IX_Prod_ReferencePlanFollowup_Merchandising_StyleShipmentRatioFK",
                table: "Prod_ReferencePlanFollowup",
                column: "Merchandising_StyleShipmentRatioFK");

            migrationBuilder.CreateIndex(
                name: "IX_Prod_ReferencePlanFollowup_Merchandising_StyleShipmentScheduleFK",
                table: "Prod_ReferencePlanFollowup",
                column: "Merchandising_StyleShipmentScheduleFK");

            migrationBuilder.AddForeignKey(
                name: "FK_Prod_ReferencePlanFollowup_Merchandising_StyleShipmentRatio_Merchandising_StyleShipmentRatioFK",
                table: "Prod_ReferencePlanFollowup",
                column: "Merchandising_StyleShipmentRatioFK",
                principalTable: "Merchandising_StyleShipmentRatio",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Prod_ReferencePlanFollowup_Merchandising_StyleShipmentSchedule_Merchandising_StyleShipmentScheduleFK",
                table: "Prod_ReferencePlanFollowup",
                column: "Merchandising_StyleShipmentScheduleFK",
                principalTable: "Merchandising_StyleShipmentSchedule",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Store_Bin_Common_RawItem_Common_RawItemFK",
                table: "Store_Bin",
                column: "Common_RawItemFK",
                principalTable: "Common_RawItem",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
