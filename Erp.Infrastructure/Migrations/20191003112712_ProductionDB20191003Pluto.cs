﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Erp.Infrastructure.Migrations
{
    public partial class ProductionDB20191003Pluto : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_HRMS_Section_User_Department_User_DepartmentFK",
                table: "HRMS_Section");

            migrationBuilder.DropForeignKey(
                name: "FK_User_Department_HRMS_Unit_HRMS_UnitFK",
                table: "User_Department");

            migrationBuilder.DropIndex(
                name: "IX_User_Department_HRMS_UnitFK",
                table: "User_Department");

            migrationBuilder.DropColumn(
                name: "HRMS_UnitFK",
                table: "User_Department");

            migrationBuilder.RenameColumn(
                name: "User_DepartmentFK",
                table: "HRMS_Section",
                newName: "HRMS_UnitFK");

            migrationBuilder.RenameIndex(
                name: "IX_HRMS_Section_User_DepartmentFK",
                table: "HRMS_Section",
                newName: "IX_HRMS_Section_HRMS_UnitFK");

            migrationBuilder.AddColumn<int>(
                name: "User_DepartmentFK",
                table: "HRMS_Employee",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Common_LineDevice",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    DeviceID = table.Column<string>(nullable: true),
                    IsClossed = table.Column<bool>(nullable: false),
                    Common_ProductionLineFK = table.Column<int>(nullable: false),
                    Common_ProductionLineID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_LineDevice", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Common_LineDevice_Common_ProductionLine_Common_ProductionLineID",
                        column: x => x.Common_ProductionLineID,
                        principalTable: "Common_ProductionLine",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Common_Notify",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    StyleID = table.Column<int>(nullable: false),
                    IsCloseShipment = table.Column<bool>(nullable: false),
                    NoOfShipment = table.Column<int>(nullable: false),
                    TShipmentDone = table.Column<int>(nullable: false),
                    IsBOMCreate = table.Column<bool>(nullable: false),
                    IsBOMComplete = table.Column<bool>(nullable: false),
                    TDraftPR = table.Column<int>(nullable: false),
                    TSubmittedPR = table.Column<int>(nullable: false),
                    THoldPR = table.Column<int>(nullable: false),
                    TCancelPR = table.Column<int>(nullable: false),
                    TPartialPR = table.Column<int>(nullable: false),
                    TCompletePR = table.Column<int>(nullable: false),
                    PApprovedPR = table.Column<int>(nullable: false),
                    SApprovedPR = table.Column<int>(nullable: false),
                    FApprovedPR = table.Column<int>(nullable: false),
                    TDraftPO = table.Column<int>(nullable: false),
                    TSubmittedPO = table.Column<int>(nullable: false),
                    THoldPO = table.Column<int>(nullable: false),
                    TCancelPO = table.Column<int>(nullable: false),
                    TPartialPO = table.Column<int>(nullable: false),
                    TCompletePO = table.Column<int>(nullable: false),
                    PApprovedPO = table.Column<int>(nullable: false),
                    SApprovedPO = table.Column<int>(nullable: false),
                    FApprovedPO = table.Column<int>(nullable: false),
                    PartialReceived = table.Column<int>(nullable: false),
                    TGoodReceived = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_Notify", x => x.ID);
                });

            migrationBuilder.CreateIndex(
                name: "IX_HRMS_Employee_User_DepartmentFK",
                table: "HRMS_Employee",
                column: "User_DepartmentFK");

            migrationBuilder.CreateIndex(
                name: "IX_Common_LineDevice_Common_ProductionLineID",
                table: "Common_LineDevice",
                column: "Common_ProductionLineID");

            migrationBuilder.AddForeignKey(
                name: "FK_HRMS_Employee_User_Department_User_DepartmentFK",
                table: "HRMS_Employee",
                column: "User_DepartmentFK",
                principalTable: "User_Department",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_HRMS_Section_HRMS_Unit_HRMS_UnitFK",
                table: "HRMS_Section",
                column: "HRMS_UnitFK",
                principalTable: "HRMS_Unit",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_HRMS_Employee_User_Department_User_DepartmentFK",
                table: "HRMS_Employee");

            migrationBuilder.DropForeignKey(
                name: "FK_HRMS_Section_HRMS_Unit_HRMS_UnitFK",
                table: "HRMS_Section");

            migrationBuilder.DropTable(
                name: "Common_LineDevice");

            migrationBuilder.DropTable(
                name: "Common_Notify");

            migrationBuilder.DropIndex(
                name: "IX_HRMS_Employee_User_DepartmentFK",
                table: "HRMS_Employee");

            migrationBuilder.DropColumn(
                name: "User_DepartmentFK",
                table: "HRMS_Employee");

            migrationBuilder.RenameColumn(
                name: "HRMS_UnitFK",
                table: "HRMS_Section",
                newName: "User_DepartmentFK");

            migrationBuilder.RenameIndex(
                name: "IX_HRMS_Section_HRMS_UnitFK",
                table: "HRMS_Section",
                newName: "IX_HRMS_Section_User_DepartmentFK");

            migrationBuilder.AddColumn<int>(
                name: "HRMS_UnitFK",
                table: "User_Department",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_User_Department_HRMS_UnitFK",
                table: "User_Department",
                column: "HRMS_UnitFK");

            migrationBuilder.AddForeignKey(
                name: "FK_HRMS_Section_User_Department_User_DepartmentFK",
                table: "HRMS_Section",
                column: "User_DepartmentFK",
                principalTable: "User_Department",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_User_Department_HRMS_Unit_HRMS_UnitFK",
                table: "User_Department",
                column: "HRMS_UnitFK",
                principalTable: "HRMS_Unit",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
