﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Erp.Infrastructure.Migrations
{
    public partial class ProductionDB20190912Horse : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Priority",
                table: "User_SubMenu",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Priority",
                table: "User_Menu",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ShipmentCountDay",
                table: "Common_Notification",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ShipmentCountMonth",
                table: "Common_Notification",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "TCancelPO",
                table: "Common_Notification",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "TCancelPR",
                table: "Common_Notification",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "TCompletePO",
                table: "Common_Notification",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "TCompletePR",
                table: "Common_Notification",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "TCompleteStyle",
                table: "Common_Notification",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "TPartialPO",
                table: "Common_Notification",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "TPartialPR",
                table: "Common_Notification",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "Common_LienBank",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "Common_BankFK",
                table: "Common_CompanyBank",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Common_CompanySetupFK",
                table: "Common_CompanyBank",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "Common_CompanyBank",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Common_BuyerFK",
                table: "Common_BuyerBank",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "Common_BuyerBank",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Common_CountryFK",
                table: "Common_Bank",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "Common_Bank",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Store_AssetDepreciation",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    CID = table.Column<string>(nullable: true),
                    ItemSLNO = table.Column<string>(nullable: true),
                    LotQty = table.Column<double>(nullable: false),
                    UnitPrice = table.Column<double>(nullable: false),
                    MinPrice = table.Column<double>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    HREmployeeDepartmentFK = table.Column<int>(nullable: false),
                    HREmployeeFK = table.Column<int>(nullable: false),
                    HREmployeeReceivedDate = table.Column<DateTime>(nullable: false),
                    CommonStoreFK = table.Column<int>(nullable: false),
                    StockInFK = table.Column<int>(nullable: false),
                    CommonUnitFK = table.Column<int>(nullable: false),
                    CommonBrandFK = table.Column<int>(nullable: false),
                    CommonModelFK = table.Column<int>(nullable: false),
                    CommonDepreciationFK = table.Column<int>(nullable: false),
                    RawItemFK = table.Column<int>(nullable: false),
                    LifeTime = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Store_AssetDepreciation", x => x.ID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Store_AssetDepreciation");

            migrationBuilder.DropColumn(
                name: "Priority",
                table: "User_SubMenu");

            migrationBuilder.DropColumn(
                name: "Priority",
                table: "User_Menu");

            migrationBuilder.DropColumn(
                name: "ShipmentCountDay",
                table: "Common_Notification");

            migrationBuilder.DropColumn(
                name: "ShipmentCountMonth",
                table: "Common_Notification");

            migrationBuilder.DropColumn(
                name: "TCancelPO",
                table: "Common_Notification");

            migrationBuilder.DropColumn(
                name: "TCancelPR",
                table: "Common_Notification");

            migrationBuilder.DropColumn(
                name: "TCompletePO",
                table: "Common_Notification");

            migrationBuilder.DropColumn(
                name: "TCompletePR",
                table: "Common_Notification");

            migrationBuilder.DropColumn(
                name: "TCompleteStyle",
                table: "Common_Notification");

            migrationBuilder.DropColumn(
                name: "TPartialPO",
                table: "Common_Notification");

            migrationBuilder.DropColumn(
                name: "TPartialPR",
                table: "Common_Notification");

            migrationBuilder.DropColumn(
                name: "Email",
                table: "Common_LienBank");

            migrationBuilder.DropColumn(
                name: "Common_CompanySetupFK",
                table: "Common_CompanyBank");

            migrationBuilder.DropColumn(
                name: "Email",
                table: "Common_CompanyBank");

            migrationBuilder.DropColumn(
                name: "Common_BuyerFK",
                table: "Common_BuyerBank");

            migrationBuilder.DropColumn(
                name: "Email",
                table: "Common_BuyerBank");

            migrationBuilder.DropColumn(
                name: "Common_CountryFK",
                table: "Common_Bank");

            migrationBuilder.DropColumn(
                name: "Email",
                table: "Common_Bank");

            migrationBuilder.AlterColumn<int>(
                name: "Common_BankFK",
                table: "Common_CompanyBank",
                nullable: true,
                oldClrType: typeof(int));
        }
    }
}
