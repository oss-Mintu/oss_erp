﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Erp.Infrastructure.Migrations
{
    public partial class ProductionDB2019072510am : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Store_Bin_Store_General_Store_GeneralID",
                table: "Store_Bin");

            migrationBuilder.DropForeignKey(
                name: "FK_Store_General_User_Department_AUser_DepartmentID",
                table: "Store_General");

            migrationBuilder.DropForeignKey(
                name: "FK_Store_Requisition_User_User_ClosedByUserFK",
                table: "Store_Requisition");

            migrationBuilder.DropForeignKey(
                name: "FK_Store_RequisitionSlave_HRMS_Employee_HRMS_EmployeeFK",
                table: "Store_RequisitionSlave");

            migrationBuilder.DropForeignKey(
                name: "FK_Store_Section_Store_General_AStoreGeneralID",
                table: "Store_Section");

            migrationBuilder.DropForeignKey(
                name: "FK_Store_Section_Store_Section_Store_SectionID",
                table: "Store_Section");

            migrationBuilder.DropTable(
                name: "Store_SRequisitionSlave");

            migrationBuilder.DropTable(
                name: "Store_SRequisition");

            migrationBuilder.DropIndex(
                name: "IX_Store_Section_AStoreGeneralID",
                table: "Store_Section");

            migrationBuilder.DropIndex(
                name: "IX_Store_Section_Store_SectionID",
                table: "Store_Section");

            migrationBuilder.DropIndex(
                name: "IX_Store_Requisition_ClosedByUserFK",
                table: "Store_Requisition");

            migrationBuilder.DropIndex(
                name: "IX_Store_General_AUser_DepartmentID",
                table: "Store_General");

            migrationBuilder.DropIndex(
                name: "IX_Store_Bin_Store_GeneralID",
                table: "Store_Bin");

            migrationBuilder.DropColumn(
                name: "AStoreGeneralID",
                table: "Store_Section");

            migrationBuilder.DropColumn(
                name: "Store_SectionID",
                table: "Store_Section");

            migrationBuilder.DropColumn(
                name: "Procurement_PurchaseOrderSlaveFK",
                table: "Store_RequisitionSlave");

            migrationBuilder.DropColumn(
                name: "StoreInFK",
                table: "Store_Requisition");

            migrationBuilder.DropColumn(
                name: "StoreOutFK",
                table: "Store_Requisition");

            migrationBuilder.DropColumn(
                name: "StyleId",
                table: "Store_Requisition");

            migrationBuilder.DropColumn(
                name: "AUser_DepartmentID",
                table: "Store_General");

            migrationBuilder.DropColumn(
                name: "CommonUnitFK",
                table: "Store_General");

            migrationBuilder.DropColumn(
                name: "CommonUnitFK",
                table: "Store_Bin");

            migrationBuilder.DropColumn(
                name: "Store_GeneralID",
                table: "Store_Bin");

            migrationBuilder.RenameColumn(
                name: "StockInFk",
                table: "Store_StockRegister",
                newName: "StockInFK");

            migrationBuilder.RenameColumn(
                name: "StoreGeneralFK",
                table: "Store_Bin",
                newName: "Store_GeneralFK");

            migrationBuilder.RenameColumn(
                name: "ComnonRowItemFk",
                table: "Store_Bin",
                newName: "Common_RawItemFK");

            migrationBuilder.AddColumn<int>(
                name: "Common_SupplierFK",
                table: "Store_StockOut",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Store_GeneralFK",
                table: "Store_StockOut",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Store_StockOutMasterFK",
                table: "Store_StockOut",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Common_SupplierFK",
                table: "Store_StockIn",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Store_GeneralFK",
                table: "Store_StockIn",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Store_StockInMasterFK",
                table: "Store_StockIn",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "Store_GeneralFK",
                table: "Store_Section",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "HRMS_EmployeeFK",
                table: "Store_RequisitionSlave",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Store_GeneralInFK",
                table: "Store_Requisition",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Store_GeneralOutFK",
                table: "Store_Requisition",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "User_UserFK",
                table: "Store_Requisition",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "User_DepartmentFK",
                table: "Store_General",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "HRMS_UnitFK",
                table: "Store_General",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "HRMS_BusinessUnitFK",
                table: "Store_General",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "HRMS_PostOfficeFK",
                table: "HRMS_Employee",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Store_StockInMaster",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    CID = table.Column<string>(nullable: true),
                    ChallanNo = table.Column<string>(nullable: true),
                    ChallanDate = table.Column<DateTime>(nullable: false),
                    ReceivedDate = table.Column<DateTime>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    StockType = table.Column<string>(nullable: true),
                    StoreInType = table.Column<string>(nullable: true),
                    PurchaseOrderFK = table.Column<int>(nullable: true),
                    Common_SupplierFK = table.Column<int>(nullable: true),
                    Store_GeneralFK = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Store_StockInMaster", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Store_StockInMaster_Common_Supplier_Common_SupplierFK",
                        column: x => x.Common_SupplierFK,
                        principalTable: "Common_Supplier",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Store_StockInMaster_Procurement_PurchaseOrder_PurchaseOrderFK",
                        column: x => x.PurchaseOrderFK,
                        principalTable: "Procurement_PurchaseOrder",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Store_StockInMaster_Store_General_Store_GeneralFK",
                        column: x => x.Store_GeneralFK,
                        principalTable: "Store_General",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Store_StockOutMaster",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    CID = table.Column<string>(nullable: true),
                    ChallanNo = table.Column<string>(nullable: true),
                    ChallanDate = table.Column<DateTime>(nullable: false),
                    ReceivedDate = table.Column<DateTime>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    StockType = table.Column<string>(nullable: true),
                    StoreOutType = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Store_StockOutMaster", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Store_StockInSlave",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    PurchaseQuantity = table.Column<double>(nullable: false),
                    RemainingQty = table.Column<double>(nullable: false),
                    DamagedQty = table.Column<double>(nullable: false),
                    ReceivedQty = table.Column<double>(nullable: false),
                    UnitPrice = table.Column<double>(nullable: false),
                    TotalPrice = table.Column<double>(nullable: false),
                    IsLocked = table.Column<bool>(nullable: false),
                    FixAsset = table.Column<bool>(nullable: false),
                    BinDefine = table.Column<bool>(nullable: false),
                    Common_RawItemFK = table.Column<int>(nullable: false),
                    Store_StockInMasterFK = table.Column<int>(nullable: false),
                    Common_Size = table.Column<decimal>(nullable: false),
                    Merchandising_StyleFK = table.Column<int>(nullable: false),
                    Merchandising_StyleSlaveFK = table.Column<int>(nullable: false),
                    PurchaseOrderSlaveFK = table.Column<int>(nullable: false),
                    Store_BinFK = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Store_StockInSlave", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Store_StockInSlave_Common_RawItem_Common_RawItemFK",
                        column: x => x.Common_RawItemFK,
                        principalTable: "Common_RawItem",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Store_StockInSlave_Store_StockInMaster_Store_StockInMasterFK",
                        column: x => x.Store_StockInMasterFK,
                        principalTable: "Store_StockInMaster",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Store_StockOutSlave",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    PurchaseQuantity = table.Column<double>(nullable: false),
                    RemainingQty = table.Column<double>(nullable: false),
                    DamagedQty = table.Column<double>(nullable: false),
                    ReceivedQty = table.Column<double>(nullable: false),
                    UnitPrice = table.Column<double>(nullable: false),
                    TotalPrice = table.Column<double>(nullable: false),
                    IsLocked = table.Column<bool>(nullable: false),
                    FixAsset = table.Column<bool>(nullable: false),
                    BinDefine = table.Column<bool>(nullable: false),
                    Common_RawItemFK = table.Column<int>(nullable: false),
                    Store_StockOutMasterFK = table.Column<int>(nullable: false),
                    Common_SupplierFK = table.Column<int>(nullable: false),
                    Store_GeneralFK = table.Column<int>(nullable: false),
                    Common_ColorFK = table.Column<int>(nullable: false),
                    Common_Size = table.Column<decimal>(nullable: false),
                    Merchandising_StyleFK = table.Column<int>(nullable: false),
                    Merchandising_StyleSlaveFK = table.Column<int>(nullable: false),
                    PurchaseOrderSlaveFK = table.Column<int>(nullable: false),
                    Store_BinFK = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Store_StockOutSlave", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Store_StockOutSlave_Common_Color_Common_ColorFK",
                        column: x => x.Common_ColorFK,
                        principalTable: "Common_Color",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Store_StockOutSlave_Common_RawItem_Common_RawItemFK",
                        column: x => x.Common_RawItemFK,
                        principalTable: "Common_RawItem",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Store_StockOutSlave_Common_Supplier_Common_SupplierFK",
                        column: x => x.Common_SupplierFK,
                        principalTable: "Common_Supplier",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Store_StockOutSlave_Store_General_Store_GeneralFK",
                        column: x => x.Store_GeneralFK,
                        principalTable: "Store_General",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Store_StockOutSlave_Store_StockOutMaster_Store_StockOutMasterFK",
                        column: x => x.Store_StockOutMasterFK,
                        principalTable: "Store_StockOutMaster",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Store_StockOut_Common_ColorFK",
                table: "Store_StockOut",
                column: "Common_ColorFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_StockOut_Common_RawItemFK",
                table: "Store_StockOut",
                column: "Common_RawItemFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_StockOut_Common_SupplierFK",
                table: "Store_StockOut",
                column: "Common_SupplierFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_StockOut_Store_GeneralFK",
                table: "Store_StockOut",
                column: "Store_GeneralFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_StockOut_Store_StockOutMasterFK",
                table: "Store_StockOut",
                column: "Store_StockOutMasterFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_StockIn_Common_ColorFK",
                table: "Store_StockIn",
                column: "Common_ColorFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_StockIn_Common_RawItemFK",
                table: "Store_StockIn",
                column: "Common_RawItemFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_StockIn_Common_SupplierFK",
                table: "Store_StockIn",
                column: "Common_SupplierFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_StockIn_Store_GeneralFK",
                table: "Store_StockIn",
                column: "Store_GeneralFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_StockIn_Store_StockInMasterFK",
                table: "Store_StockIn",
                column: "Store_StockInMasterFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_Section_Store_GeneralFK",
                table: "Store_Section",
                column: "Store_GeneralFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_RequisitionSlave_Common_ColorFK",
                table: "Store_RequisitionSlave",
                column: "Common_ColorFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_Requisition_Store_GeneralInFK",
                table: "Store_Requisition",
                column: "Store_GeneralInFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_Requisition_Store_GeneralOutFK",
                table: "Store_Requisition",
                column: "Store_GeneralOutFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_Requisition_User_UserFK",
                table: "Store_Requisition",
                column: "User_UserFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_General_HRMS_BusinessUnitFK",
                table: "Store_General",
                column: "HRMS_BusinessUnitFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_General_HRMS_UnitFK",
                table: "Store_General",
                column: "HRMS_UnitFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_General_User_DepartmentFK",
                table: "Store_General",
                column: "User_DepartmentFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_Bin_Common_RawItemFK",
                table: "Store_Bin",
                column: "Common_RawItemFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_Bin_Store_GeneralFK",
                table: "Store_Bin",
                column: "Store_GeneralFK");

            migrationBuilder.CreateIndex(
                name: "IX_HRMS_Employee_HRMS_PostOfficeFK",
                table: "HRMS_Employee",
                column: "HRMS_PostOfficeFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_StockInMaster_Common_SupplierFK",
                table: "Store_StockInMaster",
                column: "Common_SupplierFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_StockInMaster_PurchaseOrderFK",
                table: "Store_StockInMaster",
                column: "PurchaseOrderFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_StockInMaster_Store_GeneralFK",
                table: "Store_StockInMaster",
                column: "Store_GeneralFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_StockInSlave_Common_RawItemFK",
                table: "Store_StockInSlave",
                column: "Common_RawItemFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_StockInSlave_Store_StockInMasterFK",
                table: "Store_StockInSlave",
                column: "Store_StockInMasterFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_StockOutSlave_Common_ColorFK",
                table: "Store_StockOutSlave",
                column: "Common_ColorFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_StockOutSlave_Common_RawItemFK",
                table: "Store_StockOutSlave",
                column: "Common_RawItemFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_StockOutSlave_Common_SupplierFK",
                table: "Store_StockOutSlave",
                column: "Common_SupplierFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_StockOutSlave_Store_GeneralFK",
                table: "Store_StockOutSlave",
                column: "Store_GeneralFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_StockOutSlave_Store_StockOutMasterFK",
                table: "Store_StockOutSlave",
                column: "Store_StockOutMasterFK");

            migrationBuilder.AddForeignKey(
                name: "FK_HRMS_Employee_HRMS_PostOffice_HRMS_PostOfficeFK",
                table: "HRMS_Employee",
                column: "HRMS_PostOfficeFK",
                principalTable: "HRMS_PostOffice",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Store_Bin_Common_RawItem_Common_RawItemFK",
                table: "Store_Bin",
                column: "Common_RawItemFK",
                principalTable: "Common_RawItem",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Store_Bin_Store_General_Store_GeneralFK",
                table: "Store_Bin",
                column: "Store_GeneralFK",
                principalTable: "Store_General",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Store_General_HRMS_BusinessUnit_HRMS_BusinessUnitFK",
                table: "Store_General",
                column: "HRMS_BusinessUnitFK",
                principalTable: "HRMS_BusinessUnit",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Store_General_HRMS_Unit_HRMS_UnitFK",
                table: "Store_General",
                column: "HRMS_UnitFK",
                principalTable: "HRMS_Unit",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Store_General_User_Department_User_DepartmentFK",
                table: "Store_General",
                column: "User_DepartmentFK",
                principalTable: "User_Department",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Store_Requisition_Store_General_Store_GeneralInFK",
                table: "Store_Requisition",
                column: "Store_GeneralInFK",
                principalTable: "Store_General",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Store_Requisition_Store_General_Store_GeneralOutFK",
                table: "Store_Requisition",
                column: "Store_GeneralOutFK",
                principalTable: "Store_General",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Store_Requisition_User_User_User_UserFK",
                table: "Store_Requisition",
                column: "User_UserFK",
                principalTable: "User_User",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Store_RequisitionSlave_Common_Color_Common_ColorFK",
                table: "Store_RequisitionSlave",
                column: "Common_ColorFK",
                principalTable: "Common_Color",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Store_RequisitionSlave_HRMS_Employee_HRMS_EmployeeFK",
                table: "Store_RequisitionSlave",
                column: "HRMS_EmployeeFK",
                principalTable: "HRMS_Employee",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Store_Section_Store_General_Store_GeneralFK",
                table: "Store_Section",
                column: "Store_GeneralFK",
                principalTable: "Store_General",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Store_StockIn_Common_Color_Common_ColorFK",
                table: "Store_StockIn",
                column: "Common_ColorFK",
                principalTable: "Common_Color",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Store_StockIn_Common_RawItem_Common_RawItemFK",
                table: "Store_StockIn",
                column: "Common_RawItemFK",
                principalTable: "Common_RawItem",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Store_StockIn_Common_Supplier_Common_SupplierFK",
                table: "Store_StockIn",
                column: "Common_SupplierFK",
                principalTable: "Common_Supplier",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Store_StockIn_Store_General_Store_GeneralFK",
                table: "Store_StockIn",
                column: "Store_GeneralFK",
                principalTable: "Store_General",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Store_StockIn_Store_StockInMaster_Store_StockInMasterFK",
                table: "Store_StockIn",
                column: "Store_StockInMasterFK",
                principalTable: "Store_StockInMaster",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Store_StockOut_Common_Color_Common_ColorFK",
                table: "Store_StockOut",
                column: "Common_ColorFK",
                principalTable: "Common_Color",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Store_StockOut_Common_RawItem_Common_RawItemFK",
                table: "Store_StockOut",
                column: "Common_RawItemFK",
                principalTable: "Common_RawItem",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Store_StockOut_Common_Supplier_Common_SupplierFK",
                table: "Store_StockOut",
                column: "Common_SupplierFK",
                principalTable: "Common_Supplier",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Store_StockOut_Store_General_Store_GeneralFK",
                table: "Store_StockOut",
                column: "Store_GeneralFK",
                principalTable: "Store_General",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Store_StockOut_Store_StockOutMaster_Store_StockOutMasterFK",
                table: "Store_StockOut",
                column: "Store_StockOutMasterFK",
                principalTable: "Store_StockOutMaster",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_HRMS_Employee_HRMS_PostOffice_HRMS_PostOfficeFK",
                table: "HRMS_Employee");

            migrationBuilder.DropForeignKey(
                name: "FK_Store_Bin_Common_RawItem_Common_RawItemFK",
                table: "Store_Bin");

            migrationBuilder.DropForeignKey(
                name: "FK_Store_Bin_Store_General_Store_GeneralFK",
                table: "Store_Bin");

            migrationBuilder.DropForeignKey(
                name: "FK_Store_General_HRMS_BusinessUnit_HRMS_BusinessUnitFK",
                table: "Store_General");

            migrationBuilder.DropForeignKey(
                name: "FK_Store_General_HRMS_Unit_HRMS_UnitFK",
                table: "Store_General");

            migrationBuilder.DropForeignKey(
                name: "FK_Store_General_User_Department_User_DepartmentFK",
                table: "Store_General");

            migrationBuilder.DropForeignKey(
                name: "FK_Store_Requisition_Store_General_Store_GeneralInFK",
                table: "Store_Requisition");

            migrationBuilder.DropForeignKey(
                name: "FK_Store_Requisition_Store_General_Store_GeneralOutFK",
                table: "Store_Requisition");

            migrationBuilder.DropForeignKey(
                name: "FK_Store_Requisition_User_User_User_UserFK",
                table: "Store_Requisition");

            migrationBuilder.DropForeignKey(
                name: "FK_Store_RequisitionSlave_Common_Color_Common_ColorFK",
                table: "Store_RequisitionSlave");

            migrationBuilder.DropForeignKey(
                name: "FK_Store_RequisitionSlave_HRMS_Employee_HRMS_EmployeeFK",
                table: "Store_RequisitionSlave");

            migrationBuilder.DropForeignKey(
                name: "FK_Store_Section_Store_General_Store_GeneralFK",
                table: "Store_Section");

            migrationBuilder.DropForeignKey(
                name: "FK_Store_StockIn_Common_Color_Common_ColorFK",
                table: "Store_StockIn");

            migrationBuilder.DropForeignKey(
                name: "FK_Store_StockIn_Common_RawItem_Common_RawItemFK",
                table: "Store_StockIn");

            migrationBuilder.DropForeignKey(
                name: "FK_Store_StockIn_Common_Supplier_Common_SupplierFK",
                table: "Store_StockIn");

            migrationBuilder.DropForeignKey(
                name: "FK_Store_StockIn_Store_General_Store_GeneralFK",
                table: "Store_StockIn");

            migrationBuilder.DropForeignKey(
                name: "FK_Store_StockIn_Store_StockInMaster_Store_StockInMasterFK",
                table: "Store_StockIn");

            migrationBuilder.DropForeignKey(
                name: "FK_Store_StockOut_Common_Color_Common_ColorFK",
                table: "Store_StockOut");

            migrationBuilder.DropForeignKey(
                name: "FK_Store_StockOut_Common_RawItem_Common_RawItemFK",
                table: "Store_StockOut");

            migrationBuilder.DropForeignKey(
                name: "FK_Store_StockOut_Common_Supplier_Common_SupplierFK",
                table: "Store_StockOut");

            migrationBuilder.DropForeignKey(
                name: "FK_Store_StockOut_Store_General_Store_GeneralFK",
                table: "Store_StockOut");

            migrationBuilder.DropForeignKey(
                name: "FK_Store_StockOut_Store_StockOutMaster_Store_StockOutMasterFK",
                table: "Store_StockOut");

            migrationBuilder.DropTable(
                name: "Store_StockInSlave");

            migrationBuilder.DropTable(
                name: "Store_StockOutSlave");

            migrationBuilder.DropTable(
                name: "Store_StockInMaster");

            migrationBuilder.DropTable(
                name: "Store_StockOutMaster");

            migrationBuilder.DropIndex(
                name: "IX_Store_StockOut_Common_ColorFK",
                table: "Store_StockOut");

            migrationBuilder.DropIndex(
                name: "IX_Store_StockOut_Common_RawItemFK",
                table: "Store_StockOut");

            migrationBuilder.DropIndex(
                name: "IX_Store_StockOut_Common_SupplierFK",
                table: "Store_StockOut");

            migrationBuilder.DropIndex(
                name: "IX_Store_StockOut_Store_GeneralFK",
                table: "Store_StockOut");

            migrationBuilder.DropIndex(
                name: "IX_Store_StockOut_Store_StockOutMasterFK",
                table: "Store_StockOut");

            migrationBuilder.DropIndex(
                name: "IX_Store_StockIn_Common_ColorFK",
                table: "Store_StockIn");

            migrationBuilder.DropIndex(
                name: "IX_Store_StockIn_Common_RawItemFK",
                table: "Store_StockIn");

            migrationBuilder.DropIndex(
                name: "IX_Store_StockIn_Common_SupplierFK",
                table: "Store_StockIn");

            migrationBuilder.DropIndex(
                name: "IX_Store_StockIn_Store_GeneralFK",
                table: "Store_StockIn");

            migrationBuilder.DropIndex(
                name: "IX_Store_StockIn_Store_StockInMasterFK",
                table: "Store_StockIn");

            migrationBuilder.DropIndex(
                name: "IX_Store_Section_Store_GeneralFK",
                table: "Store_Section");

            migrationBuilder.DropIndex(
                name: "IX_Store_RequisitionSlave_Common_ColorFK",
                table: "Store_RequisitionSlave");

            migrationBuilder.DropIndex(
                name: "IX_Store_Requisition_Store_GeneralInFK",
                table: "Store_Requisition");

            migrationBuilder.DropIndex(
                name: "IX_Store_Requisition_Store_GeneralOutFK",
                table: "Store_Requisition");

            migrationBuilder.DropIndex(
                name: "IX_Store_Requisition_User_UserFK",
                table: "Store_Requisition");

            migrationBuilder.DropIndex(
                name: "IX_Store_General_HRMS_BusinessUnitFK",
                table: "Store_General");

            migrationBuilder.DropIndex(
                name: "IX_Store_General_HRMS_UnitFK",
                table: "Store_General");

            migrationBuilder.DropIndex(
                name: "IX_Store_General_User_DepartmentFK",
                table: "Store_General");

            migrationBuilder.DropIndex(
                name: "IX_Store_Bin_Common_RawItemFK",
                table: "Store_Bin");

            migrationBuilder.DropIndex(
                name: "IX_Store_Bin_Store_GeneralFK",
                table: "Store_Bin");

            migrationBuilder.DropIndex(
                name: "IX_HRMS_Employee_HRMS_PostOfficeFK",
                table: "HRMS_Employee");

            migrationBuilder.DropColumn(
                name: "Common_SupplierFK",
                table: "Store_StockOut");

            migrationBuilder.DropColumn(
                name: "Store_GeneralFK",
                table: "Store_StockOut");

            migrationBuilder.DropColumn(
                name: "Store_StockOutMasterFK",
                table: "Store_StockOut");

            migrationBuilder.DropColumn(
                name: "Common_SupplierFK",
                table: "Store_StockIn");

            migrationBuilder.DropColumn(
                name: "Store_GeneralFK",
                table: "Store_StockIn");

            migrationBuilder.DropColumn(
                name: "Store_StockInMasterFK",
                table: "Store_StockIn");

            migrationBuilder.DropColumn(
                name: "Store_GeneralInFK",
                table: "Store_Requisition");

            migrationBuilder.DropColumn(
                name: "Store_GeneralOutFK",
                table: "Store_Requisition");

            migrationBuilder.DropColumn(
                name: "User_UserFK",
                table: "Store_Requisition");

            migrationBuilder.DropColumn(
                name: "HRMS_PostOfficeFK",
                table: "HRMS_Employee");

            migrationBuilder.RenameColumn(
                name: "StockInFK",
                table: "Store_StockRegister",
                newName: "StockInFk");

            migrationBuilder.RenameColumn(
                name: "Store_GeneralFK",
                table: "Store_Bin",
                newName: "StoreGeneralFK");

            migrationBuilder.RenameColumn(
                name: "Common_RawItemFK",
                table: "Store_Bin",
                newName: "ComnonRowItemFk");

            migrationBuilder.AlterColumn<int>(
                name: "Store_GeneralFK",
                table: "Store_Section",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "AStoreGeneralID",
                table: "Store_Section",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Store_SectionID",
                table: "Store_Section",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "HRMS_EmployeeFK",
                table: "Store_RequisitionSlave",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "Procurement_PurchaseOrderSlaveFK",
                table: "Store_RequisitionSlave",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "StoreInFK",
                table: "Store_Requisition",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "StoreOutFK",
                table: "Store_Requisition",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "StyleId",
                table: "Store_Requisition",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<int>(
                name: "User_DepartmentFK",
                table: "Store_General",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "HRMS_UnitFK",
                table: "Store_General",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "HRMS_BusinessUnitFK",
                table: "Store_General",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "AUser_DepartmentID",
                table: "Store_General",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CommonUnitFK",
                table: "Store_General",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "CommonUnitFK",
                table: "Store_Bin",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Store_GeneralID",
                table: "Store_Bin",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Store_SRequisition",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    CID = table.Column<string>(nullable: true),
                    CancellationRemaks = table.Column<string>(nullable: true),
                    ClosedByUserFK = table.Column<int>(nullable: true),
                    CreationDate = table.Column<DateTime>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    OriginType = table.Column<int>(nullable: false),
                    PRType = table.Column<int>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    Status = table.Column<int>(nullable: false),
                    StoreInFK = table.Column<int>(nullable: false),
                    StoreOutFK = table.Column<int>(nullable: false),
                    StyleId = table.Column<int>(nullable: false),
                    Time = table.Column<DateTime>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Store_SRequisition", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Store_SRequisition_User_User_ClosedByUserFK",
                        column: x => x.ClosedByUserFK,
                        principalTable: "User_User",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Store_SRequisitionSlave",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    Common_RawItemFK = table.Column<int>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    HRMS_EmployeeFK = table.Column<int>(nullable: true),
                    InspectionRequired = table.Column<bool>(nullable: false),
                    Merchandising_BOFFK = table.Column<int>(nullable: true),
                    Merchandising_StyleID = table.Column<int>(nullable: true),
                    Merchandising_StyleSlaveFK = table.Column<int>(nullable: true),
                    Remarks = table.Column<string>(nullable: true),
                    RequiredDate = table.Column<DateTime>(nullable: false),
                    RequisitionQuantity = table.Column<double>(nullable: false),
                    Store_SRequisitionFK = table.Column<int>(nullable: false),
                    Store_SRequisitionID = table.Column<int>(nullable: true),
                    SupplierNames = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Store_SRequisitionSlave", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Store_SRequisitionSlave_Common_RawItem_Common_RawItemFK",
                        column: x => x.Common_RawItemFK,
                        principalTable: "Common_RawItem",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Store_SRequisitionSlave_HRMS_Employee_HRMS_EmployeeFK",
                        column: x => x.HRMS_EmployeeFK,
                        principalTable: "HRMS_Employee",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Store_SRequisitionSlave_Merchandising_BOF_Merchandising_BOFFK",
                        column: x => x.Merchandising_BOFFK,
                        principalTable: "Merchandising_BOF",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Store_SRequisitionSlave_Merchandising_StyleSlave_Merchandising_StyleSlaveFK",
                        column: x => x.Merchandising_StyleSlaveFK,
                        principalTable: "Merchandising_StyleSlave",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Store_SRequisitionSlave_Store_Requisition_Store_SRequisitionFK",
                        column: x => x.Store_SRequisitionFK,
                        principalTable: "Store_Requisition",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Store_SRequisitionSlave_Store_SRequisition_Store_SRequisitionID",
                        column: x => x.Store_SRequisitionID,
                        principalTable: "Store_SRequisition",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Store_Section_AStoreGeneralID",
                table: "Store_Section",
                column: "AStoreGeneralID");

            migrationBuilder.CreateIndex(
                name: "IX_Store_Section_Store_SectionID",
                table: "Store_Section",
                column: "Store_SectionID");

            migrationBuilder.CreateIndex(
                name: "IX_Store_Requisition_ClosedByUserFK",
                table: "Store_Requisition",
                column: "ClosedByUserFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_General_AUser_DepartmentID",
                table: "Store_General",
                column: "AUser_DepartmentID");

            migrationBuilder.CreateIndex(
                name: "IX_Store_Bin_Store_GeneralID",
                table: "Store_Bin",
                column: "Store_GeneralID");

            migrationBuilder.CreateIndex(
                name: "IX_Store_SRequisition_ClosedByUserFK",
                table: "Store_SRequisition",
                column: "ClosedByUserFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_SRequisitionSlave_Common_RawItemFK",
                table: "Store_SRequisitionSlave",
                column: "Common_RawItemFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_SRequisitionSlave_HRMS_EmployeeFK",
                table: "Store_SRequisitionSlave",
                column: "HRMS_EmployeeFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_SRequisitionSlave_Merchandising_BOFFK",
                table: "Store_SRequisitionSlave",
                column: "Merchandising_BOFFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_SRequisitionSlave_Merchandising_StyleSlaveFK",
                table: "Store_SRequisitionSlave",
                column: "Merchandising_StyleSlaveFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_SRequisitionSlave_Store_SRequisitionFK",
                table: "Store_SRequisitionSlave",
                column: "Store_SRequisitionFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_SRequisitionSlave_Store_SRequisitionID",
                table: "Store_SRequisitionSlave",
                column: "Store_SRequisitionID");

            migrationBuilder.AddForeignKey(
                name: "FK_Store_Bin_Store_General_Store_GeneralID",
                table: "Store_Bin",
                column: "Store_GeneralID",
                principalTable: "Store_General",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Store_General_User_Department_AUser_DepartmentID",
                table: "Store_General",
                column: "AUser_DepartmentID",
                principalTable: "User_Department",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Store_Requisition_User_User_ClosedByUserFK",
                table: "Store_Requisition",
                column: "ClosedByUserFK",
                principalTable: "User_User",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Store_RequisitionSlave_HRMS_Employee_HRMS_EmployeeFK",
                table: "Store_RequisitionSlave",
                column: "HRMS_EmployeeFK",
                principalTable: "HRMS_Employee",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Store_Section_Store_General_AStoreGeneralID",
                table: "Store_Section",
                column: "AStoreGeneralID",
                principalTable: "Store_General",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Store_Section_Store_Section_Store_SectionID",
                table: "Store_Section",
                column: "Store_SectionID",
                principalTable: "Store_Section",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
