﻿using Erp.Core.Entity.Accounting;
using Erp.Core.Entity.Common;
using Erp.Core.Entity.Merchandising;
using Erp.Core.Entity.MIS;
using Erp.Core.Entity.HRMS;
using Erp.Core.Entity.Procurement;
using Erp.Core.Entity.Production;
using Erp.Core.Entity.Shipment;
using Erp.Core.Entity.Store;
using Erp.Core.Entity.User;
using Erp.Core.Services;
using Erp.Core.Services.Commercial;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System.Threading;
using System.Threading.Tasks;
using Erp.Core.Entity.Payroll;

namespace Erp.Infrastructure
{
    public class InfrastructureDbContext : DbContext, IErpDbContext
    {
        public InfrastructureDbContext()
        {
        }

        public InfrastructureDbContext(DbContextOptions<InfrastructureDbContext> options) : base(options)
        {

        }
        public virtual DbSet<Accounting_Type> Accounting_Type { get; set; }
        public virtual DbSet<Accounting_Chart1> Accounting_Chart1 { get; set; }
        public virtual DbSet<Accounting_Chart2> Accounting_Chart2 { get; set; }
        public virtual DbSet<Accounting_Head> Accounting_Head { get; set; }
        public virtual DbSet<Accounting_Journal> Accounting_Journal { get; set; }
        public virtual DbSet<Accounting_JournalSlave> Accounting_JournalSlave { get; set; }
        public virtual DbSet<Accounting_CostCenter> Accounting_CostCenter { get; set; }

        //Common Setting
        public virtual DbSet<Common_Order> Common_Order { get; set; }
        public virtual DbSet<Common_RawCategory> Common_RawCategory { get; set; }
        public virtual DbSet<Common_RawSubCategory> Common_RawSubCategory { get; set; }
        public virtual DbSet<Common_RawItem> Common_RawItem { get; set; }
        public virtual DbSet<Common_Brand> Common_Brand { get; set; }
        public virtual DbSet<Common_Model> Common_Model { get; set; } 

        public virtual DbSet<Common_FinishCategory> Common_FinishCategory { get; set; }
        public virtual DbSet<Common_FinishSubCategory> Common_FinishSubCategory { get; set; }
        public virtual DbSet<Common_FinishItem> Common_FinishItem { get; set; }
        public virtual DbSet<Common_Buyer> Common_Buyer { get; set; }
        public virtual DbSet<Common_Supplier> Common_Supplier { get; set; }
        public virtual DbSet<Common_Unit> Common_Unit { get; set; }
        public virtual DbSet<Common_Currency> Common_Currency { get; set; }
        public virtual DbSet<Common_Country> Common_Country { get; set; }
        public virtual DbSet<Common_Color> Common_Color { get; set; }
        public virtual DbSet<Common_Size> Common_Size { get; set; }
        
        public virtual DbSet<Common_CountryPort> Common_CountryPort { get; set; }
        public virtual DbSet<Common_CountrySize> Common_CountrySize { get; set; }
        public virtual DbSet<Common_Product> common_product { get; set; }

        //Mis
        public virtual DbSet<Mis_FinancialStatus> Mis_FinancialStatus { get; set; }
        public virtual DbSet<Mis_CMEarned> Mis_CMEarned { get; set; }
        public virtual DbSet<Mis_CashInHand> Mis_CashInHand { get; set; }
        public virtual DbSet<Mis_CashAtBank> Mis_CashAtBank { get; set; }
        public virtual DbSet<Mis_IncomeStatement> Mis_IncomeStatement { get; set; }
        public virtual DbSet<Mis_BalanceSheet> Mis_BalanceSheet { get; set; }
        public virtual DbSet<Mis_AccountsReceivable> Mis_AccountsReceivable { get; set; }
        public virtual DbSet<Mis_AccountsPayable> Mis_AccountsPayable { get; set; }
        public virtual DbSet<Mis_OrderConfirm> Mis_OrderConfirm { get; set; }
        public virtual DbSet<Mis_RawMaterials> Mis_RawMaterials { get; set; }
        public virtual DbSet<Mis_ShipmentStatus> Mis_ShipmentStatus { get; set; }
        public virtual DbSet<Mis_DailyProduction> Mis_DailyProduction { get; set; }
        public virtual DbSet<Mis_DailyAttendance> Mis_DailyAttendance { get; set; }
        public virtual DbSet<Mis_BTBLCStatus> Mis_BTBLCStatus { get; set; }

        //User Credentials
        public virtual DbSet<User_Department> User_Department { get; set; }
        public virtual DbSet<User_User> User_User { get; set; }
        public virtual DbSet<User_AccessLevel> User_AccessLevel { get; set; }
        public virtual DbSet<User_Menu> User_Menu { get; set; }
        public virtual DbSet<User_MenuItem> User_MenuItem { get; set; }
        public virtual DbSet<User_Role> User_Role { get; set; }
        public virtual DbSet<User_RoleMenuItem> User_RoleMenuItem { get; set; }
        public virtual DbSet<User_SubMenu> User_SubMenu { get; set; }


        
        public virtual DbSet<Shipment_TermsOfShipment> Shipment_TermsOfShipment { get; set; }

        public virtual DbSet<Shipment_ShipmentInstruction> Shipment_ShipmentInstruction { get; set; }
        public virtual DbSet<Shipment_ShipmentInstructionSlave> Shipment_ShipmentInstructionSlave { get; set; }
        public virtual DbSet<Shipment_DeliveryChallan> Shipment_DeliveryChallan { get; set; }
        public virtual DbSet<Shipment_DeliveryChallanSlave> Shipment_DeliveryChallanSlave { get; set; }
        public virtual DbSet<Shipment_ShipmentInvoice> Shipment_ShipmentInvoice { get; set; }
        public virtual DbSet<Shipment_ShipmentInvoiceSlave> Shipment_ShipmentInvoiceSlave { get; set; }
        public virtual DbSet<Shipment_BillOfExchange> Shipment_BillOfExchange { get; set; }
        public virtual DbSet<Shipment_BillOfExchangeSlave> Shipment_BillOfExchangeSlave { get; set; }

        /// <summary>
        /// Commercial
        /// </summary>
        public virtual DbSet<Commercial_UDBuyerOrder> Commercial_UDBuyerOrder { get; set; }
        public virtual DbSet<Commercial_Buyer> Commercial_Buyer { get; set; } 
        public virtual DbSet<Commercial_BuyerOrder> Commercial_BuyerOrder { get; set; }
        public virtual DbSet<Commercial_PurchaseOrder> Commercial_PurchaseOrder { get; set; }
        public virtual DbSet<Commercial_ECI> Commercial_ECI { get; set; } 
        public virtual DbSet<Commercial_BBLC> Commercial_BBLC { get; set; }

        public virtual DbSet<Common_LcType> Common_LcType { get; set; }
        public virtual DbSet<Common_BuyerBank> Common_BuyerBank { get; set; }
        public virtual DbSet<Common_LienBank> Common_LienBank { get; set; }
        public virtual DbSet<Common_CompanyBank> Common_CompanyBank { get; set; } 
        
        public virtual DbSet<Common_Style> Common_Style { get; set; }
        public virtual DbSet<Common_UD> Common_UD { get; set; }
        public virtual DbSet<Common_TCTitle> Common_TCTitle { get; set; }

        //Procurement
        public virtual DbSet<Procurement_PurchaseRequisition> Procurement_PurchaseRequisition { get; set; }
        public virtual DbSet<Procurement_PurchaseRequisitionSlave> Procurement_PurchaseRequisitionSlave { get; set; }
        
        public virtual DbSet<Procurement_PurchaseOrder> Procurement_PurchaseOrder { get; set; }
        public virtual DbSet<Procurement_PurchaseOrderSlave> Procurement_PurchaseOrderSlave { get; set; }

        /// Merchandising
        public virtual DbSet<Merchandising_BuyerOrder> Merchandising_BuyerOrder { get; set; }
        public virtual DbSet<Merchandising_Style> Merchandising_Style { get; set; }
        public virtual DbSet<Merchandising_StyleMeasurement> Merchandising_StyleMeasurement { get; set; }
        public virtual DbSet<Merchandising_StyleSetPack> Merchandising_StyleSetPack { get; set; }
        public virtual DbSet<Merchandising_StyleSlave> Merchandising_StyleSlave { get; set; }
        public virtual DbSet<Merchandising_StyleShipmentSchedule> Merchandising_StyleShipmentSchedule { get; set; }
        public virtual DbSet<Merchandising_StyleShipmentRatio> Merchandising_StyleShipmentRatio { get; set; }
        public virtual DbSet<Merchandising_CBSStyle> Merchandising_CBSStyle { get; set; }
        public virtual DbSet<Merchandising_CBS> Merchandising_CBS { get; set; }
        public virtual DbSet<Merchandising_CBSSlave> Merchandising_CBSSlave { get; set; }

        //ProductionPlan
        public virtual DbSet<Common_ProductionLine> Common_ProductionLine { get; set; }
        public virtual DbSet<Plan_PlanConfig> Plan_PlanConfig { get; set; }
        public virtual DbSet<Plan_SMVDraftLayout> Plan_SMVDraftLayout { get; set; }
        public virtual DbSet<Plan_DraftOrderPlan> Plan_DraftOrderPlan { get; set; }
        public virtual DbSet<Plan_DraftLinePlan> Plan_DraftLinePlan { get; set; }
        public virtual DbSet<Plan_SMVMasterLayout> Plan_SMVMasterLayout { get; set; }
        public virtual DbSet<Plan_MasterOrderPlan> Plan_MasterOrderPlan { get; set; }
        public virtual DbSet<Plan_MasterLinePlan> Plan_MasterLinePlan { get; set; }

        //Production
        public virtual DbSet<Common_LineChief> Common_LineChief { get; set; }
        public virtual DbSet<Common_LineSuperVisor> Common_LineSuperVisor { get; set; }
        public virtual DbSet<Prod_Reference> Prod_Reference { get; set; }
        public virtual DbSet<Prod_ReferencePlan> Prod_ReferencePlan { get; set; }
        public virtual DbSet<Prod_ReferencePlanFollowup> Prod_ReferencePlanFollowup { get; set; }
        public virtual DbSet<Prod_ReferencePlanFollowupDetails> Prod_ReferencePlanFollowupDetails { get; set; }
        public virtual DbSet<Prod_PlanAchievment> Prod_PlanAchievment { get; set; }
        public virtual DbSet<Common_LineChiefLine> Common_LineChiefLine { get; set; }
       

        //Store
        public virtual DbSet<Store_General> Store_General { get; set; }
        public virtual DbSet<Store_Bin> Store_Bin { get; set; }
        public virtual DbSet<Store_StockIn> Store_StockIn { get; set; }
        public virtual DbSet<Store_StockRegister> Store_StockRegister { get; set; }
        public virtual DbSet<Store_StockOut> Store_StockOut { get; set; }
        public virtual DbSet<Store_Requisition> Store_Requisition { get; set; }
        public virtual DbSet<Store_RequisitionSlave> Store_RequisitionSlave { get; set; }
        public virtual DbSet<Store_Section> Store_Section { get; set; }

        //HRMS
        public virtual DbSet<HRMS_BusinessUnit> HRMS_BusinessUnit { get; set; }
        public virtual DbSet<HRMS_Unit> HRMS_Unit { get; set; }
        public virtual DbSet<HRMS_Section> HRMS_Section { get; set; }
        public virtual DbSet<HRMS_Designation> HRMS_Designation { get; set; }
        public virtual DbSet<HRMS_Shift> HRMS_Shift { get; set; }
        public virtual DbSet<HRMS_EarnLeave> HRMS_EarnLeave { get; set; }
        public virtual DbSet<HRMS_Employee> HRMS_Employee { get; set; }
        public virtual DbSet<HRMS_EmployeeDependent> HRMS_EmployeeDependent { get; set; }
        public virtual DbSet<HRMS_EmployeeEducation> HRMS_EmployeeEducation { get; set; }
        public virtual DbSet<HRMS_EmployeeTraining> HRMS_EmployeeTraining { get; set; }
        public virtual DbSet<HRMS_EmployeeExperience> HRMS_EmployeeExperience { get; set; }
        public virtual DbSet<HRMS_EmployeeSkill> HRMS_EmployeeSkill { get; set; }
        public virtual DbSet<HRMS_EmployeeReference> HRMS_EmployeeReference { get; set; }
        public virtual DbSet<HRMS_ShiftAssign> HRMS_ShiftAssign { get; set; }
        public virtual DbSet<HRMS_OffDay> HRMS_OffDay { get; set; }
        public virtual DbSet<HRMS_Holiday> HRMS_Holiday { get; set; }
        public virtual DbSet<HRMS_EmployeeLeaveAssign> HRMS_EmployeeLeaveAssign { get; set; }
        public virtual DbSet<HRMS_Leave> HRMS_Leave { get; set; }
        public virtual DbSet<HRMS_LeaveApplication> HRMS_LeaveApplication { get; set; }
        public virtual DbSet<HRMS_EmployeeWeekendAssign> HRMS_EmployeeWeekendAssign { get; set; }
        public virtual DbSet<HRMS_EmployeeWeekend> HRMS_EmployeeWeekend { get; set; }

        //Payroll
        public virtual DbSet<Payroll_EODReference> Payroll_EODReference { get; set; }
        public virtual DbSet<Payroll_EODRecord> Payroll_EODRecord { get; set; }

        public override Task<EntityEntry> AddAsync(object entity, CancellationToken cancellationToken)
        {
            return base.AddAsync(entity);
        }
        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken)
        {
            return base.SaveChangesAsync();
        }
    }
}
