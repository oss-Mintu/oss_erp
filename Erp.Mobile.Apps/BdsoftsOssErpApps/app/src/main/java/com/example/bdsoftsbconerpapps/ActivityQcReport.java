package com.example.bdsoftsbconerpapps;

import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ListView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import java.util.ArrayList;

public class ActivityQcReport extends AppCompatActivity {

    String[] mobileArray = {"Android", "IPhone", "WindowsMobile", "Blackberry",
            "WebOS", "Ubuntu", "Windows7", "Max OS X"};
    private ArrayList<String> qclist;

    SQLiteDatabase mDatabase;
    public static final String DATABASE_NAME = "erp";
    private ArrayList<Model.QCModel> modelarraylist;
    private static ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qc_report);
        getSupportActionBar().hide();

        showSimpleProgressDialog(this, "Loading...", "Fetching Data From Api", false);

        final String deviceId = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
        int count;
        String sql = "SELECT ID, StyleName, Color, Size, Subset, RejectQuantityTotal, QCQuantity FROM Store_QCInformation Where DeviceId = '" + deviceId + "' and ID = (SELECT Max(ID) FROM Store_QCInformation);";
        try {
            mDatabase = openOrCreateDatabase(DATABASE_NAME, MODE_PRIVATE, null);
            Cursor cursorStyles = mDatabase.rawQuery(sql, null);
            cursorStyles.moveToFirst();
            count = cursorStyles.getInt(0);

            if (count > 0) {
                if (cursorStyles.moveToFirst()) {
                    modelarraylist = new ArrayList<>();
                    qclist = new ArrayList<>();
                    Model.QCModel modeled = new Model.QCModel(0, "Style Name", "Color", "Size", "Sub Set", "Total Reject", "Total QC");
                    modelarraylist.add(modeled);

                    do {

                        Model.QCModel model = new Model.QCModel(
                                cursorStyles.getInt(0),
                                cursorStyles.getString(1),
                                cursorStyles.getString(2),
                                cursorStyles.getString(3),
                                cursorStyles.getString(4),
                                cursorStyles.getString(5),
                                cursorStyles.getString(6));
                        modelarraylist.add(model);

                    } while (cursorStyles.moveToNext());
                }

                cursorStyles.close();

                for (int i = 0; i < modelarraylist.size(); i++) {
                    qclist.add(modelarraylist.get(i).getStyle() + " | " + modelarraylist.get(i).getColor() + " | " + modelarraylist.get(i).getSize() + " | " + modelarraylist.get(i).getSubset() + " | " + modelarraylist.get(i).getRejecttotal() + " | " + modelarraylist.get(i).getQctotal());
                }

                //SixColumnArrayAdapter adapter;
                //adapter = new SixColumnArrayAdapter<>(this, R.layout.activity_listview, qclist);

                ListView report_listView = findViewById(R.id.report_listView);
                final ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, android.R.id.text1, qclist);
                report_listView.setAdapter(adapter);
            }
        } catch (Exception e) {
            Toast toast = Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT);
            toast.show();
        }
        removeSimpleProgressDialog();
    }

    public static void removeSimpleProgressDialog() {
        try {
            if (mProgressDialog != null) {
                if (mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                    mProgressDialog = null;
                }
            }
        } catch (IllegalArgumentException ie) {
            ie.printStackTrace();

        } catch (RuntimeException re) {
            re.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void showSimpleProgressDialog(Context context, String title, String msg, boolean isCancelable) {
        try {
            if (mProgressDialog == null) {
                mProgressDialog = ProgressDialog.show(context, title, msg);
                mProgressDialog.setCancelable(isCancelable);
            }

            if (!mProgressDialog.isShowing()) {
                mProgressDialog.show();
            }

        } catch (IllegalArgumentException ie) {
            ie.printStackTrace();
        } catch (RuntimeException re) {
            re.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
