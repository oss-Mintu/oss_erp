package com.example.bdsoftsbconerpapps;

public class RestService {
    //You need to change the IP if you testing environment is not local machine
    //or you may have different URL than we have here
    private static final String URL = "http://178.128.57.120/Apps/QCInformation";
    private retrofit.RestAdapter restAdapter;
    private QCService apiService;

    public RestService()
    {
        restAdapter = new retrofit.RestAdapter.Builder()
                .setEndpoint(URL)
                .setLogLevel(retrofit.RestAdapter.LogLevel.FULL)
                .build();

        apiService = restAdapter.create(QCService.class);
    }

    public QCService getService()
    {
        return apiService;
    }
}
