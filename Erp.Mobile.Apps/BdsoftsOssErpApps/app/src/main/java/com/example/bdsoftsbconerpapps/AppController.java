package com.example.bdsoftsbconerpapps;
import android.app.Application;
import android.text.TextUtils;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import static android.content.ContentValues.TAG;

public class AppController extends Application {

    private static AppController mInstance;
    private RequestQueue mRequestQueue;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
    }
    public static synchronized AppController getInstance() {
        return mInstance;
    }
    public void addToRequestQueue(JsonObjectRequest jsonObjReq, String tag_json_obj) {
        jsonObjReq.setTag(TextUtils.isEmpty(tag_json_obj) ? TAG : tag_json_obj);
        getRequestQueue().add(jsonObjReq);
    }
    private RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }
        return mRequestQueue;
    }

}

