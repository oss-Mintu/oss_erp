package com.example.bdsoftsbconerpapps;

import java.sql.Date;

public class Model {
    private int id;
    private Date date;
    private boolean isActive;
    private String remark;

    public Model() {
    }

    public int getId() { return id; }
    public void setId(int userId) {
        this.id = userId;
    }

    public Date getDate() {
        return date;
    }
    public void setDate(Date date) { this.date = date; }

    public boolean getIsActive() {
        return isActive;
    }
    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public String getRemark() { return remark; }
    public void setRemark(String remark) { this.remark = remark;}

    public static class StyleModel extends Model {
        public StyleModel(){}
        private String name;
        public String getName() {
            return name;
        }
        public void setName(String name) {
            this.name = name;
        }
    }
    public static class ColorModel extends Model {
        public ColorModel(){}
        private String name;
        public String getName() {
            return name;
        }
        public void setName(String name) {
            this.name = name;
        }
    }
    public static class SizeModel extends Model {
        public SizeModel(){}
        private String name;
        public String getName() {
            return name;
        }
        public void setName(String name) {
            this.name = name;
        }
    }
    public static class SetPackModel extends Model {
        public SetPackModel(){}
        private String name;
        public String getName() {
            return name;
        }
        public void setName(String name) {
            this.name = name;
        }
    }

    public static class UserModel {

    /*public string id {get;set;}
    public string user {get;set;}
    public string userID {get;set;}
    public string actionEum {get;set;}
    public string actionId {get;set;}
    public string journalEnum {get;set;}
    public string journalType {get;set;}
    public string isActive {get;set;}
    public string error {get;set;}

    public string userName {get;set;}
    public string password {get;set;}
    public string lblError {get;set;}
    public string locked {get;set;}
    public string userAccessLevelId {get;set;}
    public string userDepartmentId {get;set;}
    public string employeeId {get;set;}*/

        private int id;
        private int userId;
        private String userName;
        private String passWord;
        private String user;
        private boolean isActive;
        private int departmentId;

        public UserModel() {
        }
        public String getUser() { return user; }
        public void setUser(String user) {
            this.user = user;
        }

        public int getUserId() { return userId; }
        public void setUserId(int userId) {
            this.userId = userId;
        }

        public String getUserName() {
            return userName;
        }
        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getPassword() {
            return passWord;
        }
        public void setPassword(String password) {
            this.passWord = password;
        }

        public boolean getIsActive() {
            return isActive;
        }
        public void setIsActive(boolean isActive) {
            this.isActive = isActive;
        }

        public int getDepartmentId() { return departmentId; }
        public void setDepartmentId(int userId) {
            this.departmentId = departmentId;
        }
    }
    public static class EmployeeModel {

        int id;
        String name, dept, joiningDate;
        double salary;

        public EmployeeModel(int id, String name, String dept, String joiningDate, double salary) {
            this.id = id;
            this.name = name;
            this.dept = dept;
            this.joiningDate = joiningDate;
            this.salary = salary;
        }
        public int getId() { return id; }
        public String getName() { return name; }
        public String getDept() { return dept; }
        public String getJoiningDate() { return joiningDate; }
        public double getSalary() { return salary; }
    }
    public static class GoodModel {

        private int id;
        private String name;
        private String country, city, imgURL;

        public GoodModel(int id, String name) {
            this.id = id;
            this.name = name;
        }
        public GoodModel() {
        }
        public String getImgURL(){
            return imgURL;
        }
        public void setImgURL(String imgURL){
            this.imgURL = imgURL;
        }
        public void setId(int id){
            this.id = id;
        }
        public int getId(){
            return id;
        }
        public String getName() {
            return name;
        }
        public void setName(String name) {
            this.name = name;
        }
        public String getCountry() {
            return country;
        }
        public void setCountry(String country) {
            this.country = country;
        }
        public String getCity() {
            return city;
        }
        public void setCity(String city) {
            this.city = city;
        }
    }

    public static class QCModel {

        public int id;
        public String style;
        public String color;
        public String size;
        public String subset;
        public String qctotal;
        public String rejecttotal;
        public String name;

        public int Id;
        public String DeviceId;
        public String Merchandising_StyleIdFK;
        public String Merchandising_StyleSetPackFK;
        public String Common_ColorFK;
        public String Common_SizeFK;
        public String Common_ProductionLineFK;
        public String UserId;
        public String Date;
        public String Time;

        public boolean Active;
        public String Remark;

        public String QCQuantity;
        public String RejectQuantityOne;
        public String RejectQuantityTwo;
        public String RejectQuantityThree;
        public String RejectQuantityFour;
        public String RejectQuantityFive;
        public String RejectQuantitySix;
        public String RejectQuantityTotal;


        public QCModel(){}
        public QCModel(int id, String style, String color, String size, String subset, String rejecttotal, String qctotal) {
            this.id = id;
            this.style = style;
            this.color = color;
            this.size = size;
            this.subset = subset;
            this.rejecttotal = rejecttotal;
            this.qctotal = qctotal;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getStyle() {
            return style;
        }

        public void setStyle(String style) {
            this.style = style;
        }

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        public String getSize() {
            return size;
        }

        public void setSize(String size) {
            this.size = size;
        }

        public String getSubset() {
            return subset;
        }

        public void setSubset(String subset) {
            this.subset = subset;
        }

        public String getQctotal() {
            return qctotal;
        }

        public void setQctotal(String qctotal) {
            this.qctotal = qctotal;
        }

        public String getRejecttotal() {
            return rejecttotal;
        }

        public void setRejecttotal(String rejecttotal) {
            this.rejecttotal = rejecttotal;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
