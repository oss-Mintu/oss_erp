package com.example.bdsoftsbconerpapps;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import static com.example.bdsoftsbconerpapps.Model.*;

public class MainActivity extends AppCompatActivity {

    String URLasting = "";
    Button login, cancel, register, report; 
    TextView txtUser, txtPass, infoMsg, txtEntryBy;
    int counter = 5;
    SQLiteDatabase mDatabase;
    private static final String DATABASE_NAME = "erp";
    private static ProgressDialog mProgressDialog;
    private ArrayList<UserModel> userModelArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();

        report = findViewById(R.id.btnReport);
        infoMsg = findViewById(R.id.infoMsg);
        login = findViewById(R.id.btnLogin);
        register = findViewById(R.id.btnRegister);
        cancel = findViewById(R.id.btnCancel);
        txtUser = findViewById(R.id.txtName);
        txtPass = findViewById(R.id.txtpassWord);
        txtEntryBy = findViewById(R.id.txtEntryBy);

        txtUser.setText("Admin");
        txtPass.setText("111111");


        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                URLasting = "http://178.128.57.120/Apps/GetUser?userName=" + txtUser.getText().toString() + "&password=" + txtPass.getText().toString();
                retrieveUserPass(URLasting);
            }
        });

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, EmployeeActivity.class));
            }
        });
        report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, ActivityQcReport.class));
            }
        });


        mDatabase = openOrCreateDatabase(DATABASE_NAME, MODE_PRIVATE, null);
        CreateAllTables();
        AddStyleName();
        AddColorName();
        AddSize();
        AddSetPack();
    }

    public static void removeSimpleProgressDialog() {
        try {
            if (mProgressDialog != null) {
                if (mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                    mProgressDialog = null;
                }
            }
        } catch (IllegalArgumentException ie) {
            ie.printStackTrace();

        } catch (RuntimeException re) {
            re.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    public static void showSimpleProgressDialog(Context context, String title, String msg, boolean isCancelable) {
        try {
            if (mProgressDialog == null) {
                mProgressDialog = ProgressDialog.show(context, title, msg);
                mProgressDialog.setCancelable(isCancelable);
            }

            if (!mProgressDialog.isShowing()) {
                mProgressDialog.show();
            }

        } catch (IllegalArgumentException ie) {
            ie.printStackTrace();
        } catch (RuntimeException re) {
            re.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void retrieveUserPass(String urlLogin) {

        showSimpleProgressDialog(this, "Loading...", "Fetching Data From Api", false);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, urlLogin,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("string", ">>" + response);

                        try {

                            userModelArrayList = new ArrayList<>();
                            JSONArray dataArray = new JSONArray(response);

                            for (int i = 0; i < dataArray.length(); i++) {

                                UserModel userModel = new UserModel();
                                JSONObject data = dataArray.getJSONObject(i);

                                userModel.setIsActive(data.getBoolean("isActive"));
                                userModel.setUserName(data.getString("userName"));
                                userModel.setDepartmentId(data.getInt("userDepartmentId"));
                                userModel.setUserId(data.getInt("userID"));
                                userModelArrayList.add(userModel);

                                if (userModel.getIsActive()) {

                                    int count = 0;
                                    String u = txtUser.getText().toString(), p = txtPass.getText().toString();
                                    String sql = "SELECT * FROM employees WHERE name = '" + u + "' and pass = '" + p + "';";
                                    try {
                                        mDatabase = openOrCreateDatabase(DATABASE_NAME, MODE_PRIVATE, null);
                                        Cursor cursorEmployees = mDatabase.rawQuery(sql, null);
                                        cursorEmployees.moveToFirst();
                                        count = cursorEmployees.getInt(0);
                                    } catch (Exception e) {
                                        Toast toast = Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_SHORT);
                                        toast.show();
                                    }

                                    if (count > 0) {

                                        ///////////// go qc ///////////
                                        Intent n = new Intent(MainActivity.this, ActivityQcInspector.class);
                                        n.putExtra("UserName", txtUser.getText().toString());
                                        startActivity(n);

                                    } else {
                                        infoMsg.setVisibility(View.VISIBLE);
                                        infoMsg.setBackgroundColor(Color.RED);
                                        infoMsg.setTextColor(Color.WHITE);
                                        counter--;
                                        infoMsg.setText(Integer.toString(counter));
                                        AddEmployee(u, p, userModel.getDepartmentId());
                                        Toast.makeText(getApplicationContext(), "Local database Updated", Toast.LENGTH_SHORT).show();

                                        if (counter == 0) {
                                            login.setEnabled(false);
                                            startActivity(new Intent(MainActivity.this, EmployeeActivity.class));
                                        }
                                    }
                                } else {
                                    Toast.makeText(getApplicationContext(), "Login Failed", Toast.LENGTH_SHORT).show();
                                }
                            }
                            removeSimpleProgressDialog();
                            //}
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //displaying the error in toast if occurrs
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

        // request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
    private void CreateAllTables() {

        mDatabase.execSQL("DROP TABLE IF EXISTS employees");
        mDatabase.execSQL("DROP TABLE IF EXISTS styles");
        mDatabase.execSQL("DROP TABLE IF EXISTS colors");
        mDatabase.execSQL("DROP TABLE IF EXISTS sizes");
        mDatabase.execSQL("DROP TABLE IF EXISTS subsets");

        String emp = "CREATE TABLE IF NOT EXISTS employees (" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT, name varchar(200) NOT NULL," +
                "department varchar(200) NOT NULL, joiningdate datetime NOT NULL, pass varchar(200) NOT NULL, departmentId varchar(200) NOT NULL);";
        mDatabase.execSQL(emp);

        String style = "CREATE TABLE IF NOT EXISTS styles (" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT, styleId varchar(200) NOT NULL, name varchar(200) NOT NULL, entrydate datetime, remarks varchar(200));";
        mDatabase.execSQL(style);

        String color = "CREATE TABLE IF NOT EXISTS colors (" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT, colorId varchar(200) NOT NULL, name varchar(200) NOT NULL, entrydate datetime, remarks varchar(200));";
        mDatabase.execSQL(color);

        String size = "CREATE TABLE IF NOT EXISTS sizes (" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT, sizeId varchar(200) NOT NULL, name varchar(200) NOT NULL, entrydate datetime, remarks varchar(200));";
        mDatabase.execSQL(size);

        String subset = "CREATE TABLE IF NOT EXISTS subsets (" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT, setpackId varchar(200) NOT NULL, name varchar(200) NOT NULL, entrydate datetime, remarks varchar(200));";
        mDatabase.execSQL(subset);

    }

    private void AddEmployee(String username, String password, int departmentId) {

        String name = username.trim();
        String pass = password.trim();
        String deptId = String.valueOf(departmentId).trim();

        //getting the current time for joining date
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
        String joining = sdf.format(cal.getTime());
        String date = sdf.format(cal.getTime());

        String insertSQL = "INSERT INTO employees (id, name, department, joiningdate, pass, departmentId) VALUES (?, ?, ?, ?, ?, ?);";
        mDatabase.execSQL(insertSQL, new String[]{null, name, "Admin", joining, pass, deptId});

    }
    private void AddStyleName() {

        showSimpleProgressDialog(this, "Loading...", "Fetching Data From Api", false);

        String ulster = "http://178.128.57.120/Apps/GetallStyle";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, ulster,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("string", ">>" + response);

                        try {
                            JSONArray dataArray = new JSONArray(response);
                            for (int i = 0; i < dataArray.length(); i++) {

                                StyleModel model = new StyleModel();
                                JSONObject data = dataArray.getJSONObject(i);
                                model.setId(data.getInt("id"));
                                model.setName(data.getString("name"));
                                //model.setDate(new Date());
                                model.setRemark("remarks");

                                String insertSQL = "INSERT INTO styles (id, styleId, name, entrydate, remarks) VALUES (?, ?, ?, ?, ?);";
                                String[] styledata = new String[]{null, String.valueOf(model.getId()), model.getName(), null, model.getRemark()};
                                mDatabase.execSQL(insertSQL, styledata);
                            }
                            removeSimpleProgressDialog();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //displaying the error in toast if occurrs
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
    private void AddColorName() {

        showSimpleProgressDialog(this, "Loading...", "Fetching Data From Api", false);

        String ulcer = "http://178.128.57.120/Apps/GetAllColor";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, ulcer,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("string", ">>" + response);

                        try {

                            JSONArray dataArray = new JSONArray(response);
                            for (int i = 0; i < dataArray.length(); i++) {

                                ColorModel model = new ColorModel();
                                JSONObject data = dataArray.getJSONObject(i);
                                model.setId(data.getInt("id"));
                                model.setName(data.getString("name"));
                                //model.setDate(new Date());
                                model.setRemark("remarks");

                                String insertSQL = "INSERT INTO colors (id, colorId, name, entrydate, remarks) VALUES (?, ?, ?, ?, ?);";
                                String[] colordata = new String[]{null, String.valueOf(model.getId()), model.getName(), null, model.getRemark()};
                                mDatabase.execSQL(insertSQL, colordata);
                            }
                            removeSimpleProgressDialog();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //displaying the error in toast if occurrs
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

        // request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
    private void AddSize() {

        showSimpleProgressDialog(this, "Loading...", "Fetching Data From Api", false);

        String ursine = "http://178.128.57.120/Apps/GetAllSize";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, ursine,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("string", ">>" + response);

                        try {
                            JSONArray dataArray = new JSONArray(response);
                            for (int i = 0; i < dataArray.length(); i++) {

                                SizeModel model = new SizeModel();
                                JSONObject data = dataArray.getJSONObject(i);
                                model.setId(data.getInt("id"));
                                model.setName(data.getString("name"));
                                //model.setDate(new Date());
                                model.setRemark("remarks");

                                String insertSQL = "INSERT INTO sizes (id, sizeId, name, entrydate, remarks) VALUES (?, ?, ?, ?, ?);";
                                String[] sizedata = new String[]{null, String.valueOf(model.getId()), model.getName(), null, model.getRemark()};
                                mDatabase.execSQL(insertSQL, sizedata);
                            }
                            removeSimpleProgressDialog();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //displaying the error in toast if occurrs
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

        // request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
    private void AddSetPack() {

        showSimpleProgressDialog(this, "Loading...", "Fetching Data From Api", false);

        String unstack = "http://178.128.57.120/Apps/GetAllSetPack";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, unstack,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("string", ">>" + response);

                        try {
                            JSONArray dataArray = new JSONArray(response);
                            for (int i = 0; i < dataArray.length(); i++) {

                                SetPackModel model = new SetPackModel();
                                JSONObject data = dataArray.getJSONObject(i);
                                model.setId(data.getInt("id"));
                                model.setName(data.getString("setPackName"));
                                //model.setDate(new Date());
                                model.setRemark("remarks");

                                String insertSQL = "INSERT INTO subsets (id, setpackId, name, entrydate, remarks) VALUES (?, ?, ?, ?, ?);";
                                String[] setpackdata = new String[]{null, String.valueOf(model.getId()), model.getName(), null, model.getRemark()};
                                mDatabase.execSQL(insertSQL, setpackdata);
                            }
                            removeSimpleProgressDialog();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //displaying the error in toast if occurrs
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

        // request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}
