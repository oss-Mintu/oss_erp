package com.example.bdsoftsbconerpapps;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/*
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import org.jetbrains.annotations.NotNull;
import android.widget.AdapterView;
import java.util.List;
*/
import javax.net.ssl.HttpsURLConnection;

import retrofit.Callback;
import retrofit.RetrofitError;

import static android.R.layout.simple_spinner_item;

public class ActivityQcInspector extends AppCompatActivity {

    private Button btnCount, btnCorrection, btnRejectOne, btnRejectTwo, btnRejectThree, btnRejectFour, btnRejectFive, btnRejectSix;
    private TextView txtSyncAll, btnSync, txtRejectOne, txtRejectTwo, txtRejectThree, txtRejectFour, txtRejectFive, txtRejectSix, txtTotalQC;
    private TextView txtRejectTotal, txtNote, txtCounter, txtTime, txtQCPass, txtUser;
    private TextView txtCountOKTime, txtRejectOneTime, txtRejectTwoTime, txtRejectThreeTime, txtRejectFourTime, txtRejectFiveTime, txtRejectSixTime;
    private EditText showmsg;

    long MillisecondTime, StartTime, TimeBuff, UpdateTime = 0L;
    int Seconds, Minutes, MilliSeconds;

    String msg;
    Handler handler;
    Spinner spinnerStyle, spinnerColor, spinnerSize, spinnerSetback, spinnerLine, spinner;

    public int total = 0, totalReject = 0, totalQC = 0, count = 0;
    public int totalRejectOne = 0, totalRejectTwo = 0, totalRejectThree = 0, totalRejectFour = 0, totalRejectFive = 0, totalRejectSix = 0;
    int id = 1;

    private static ProgressDialog mProgressDialog;
    private ArrayList<Model.GoodModel> goodModelArrayList;

    private ArrayList<String> names = new ArrayList<>();
    private ArrayList<String> styles = new ArrayList<>();
    private ArrayList<String> colors = new ArrayList<>();
    private ArrayList<String> sizes = new ArrayList<>();
    private ArrayList<String> setpack = new ArrayList<>();
    private ArrayList<String> line = new ArrayList<>();

    SQLiteDatabase mDatabase;
    public static final String DATABASE_NAME = "erp";
    RestService restService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qc_inspector);
        getSupportActionBar().hide();

        txtUser = findViewById(R.id.txtEntryBy);
        handler = new Handler();

        btnCount = findViewById(R.id.btnCount);
        txtCounter = findViewById(R.id.txtCountOK);
        txtTime = findViewById(R.id.txtTime);
        btnCorrection = findViewById(R.id.btnCorrection);

        spinner = findViewById(R.id.spCompany);
        spinnerStyle = findViewById(R.id.spinnerStyle);
        spinnerColor = findViewById(R.id.color);
        spinnerSize = findViewById(R.id.size);
        spinnerSetback = findViewById(R.id.subset);
        spinnerLine = findViewById(R.id.line);
        txtNote = findViewById(R.id.txtNote);

        txtTime = findViewById(R.id.txtTime);
        txtRejectTotal = findViewById(R.id.txtRejectTotal);
        txtTotalQC = findViewById(R.id.txtTotalQC);

        txtRejectOne = findViewById(R.id.txtReject1Count);
        txtRejectTwo = findViewById(R.id.txtReject2Count);
        txtRejectThree = findViewById(R.id.txtReject3Count);
        txtRejectFour = findViewById(R.id.txtReject4Count);
        txtRejectFive = findViewById(R.id.txtReject5Count);
        txtRejectSix = findViewById(R.id.txtReject6Count);

        txtRejectOneTime = findViewById(R.id.txtReject1Time);
        txtRejectTwoTime = findViewById(R.id.txtReject2Time);
        txtRejectThreeTime = findViewById(R.id.txtReject3Time);
        txtRejectFourTime = findViewById(R.id.txtReject4Time);
        txtRejectFiveTime = findViewById(R.id.txtReject5Time);
        txtRejectSixTime = findViewById(R.id.txtReject6Time);
        txtCountOKTime = findViewById(R.id.txtCountOKTime);

        btnRejectOne = findViewById(R.id.txtReject1);
        btnRejectTwo = findViewById(R.id.txtReject2);
        btnRejectThree = findViewById(R.id.txtReject3);
        btnRejectFour = findViewById(R.id.txtReject4);
        btnRejectFive = findViewById(R.id.txtReject5);
        btnRejectSix = findViewById(R.id.txtReject6);

        btnSync = findViewById(R.id.sync);
        txtSyncAll = findViewById(R.id.syncAll);
        showmsg = findViewById(R.id.showmsg);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            txtUser.setText(extras.getString("UserName"));
        }

        /////// Data save ////////

        //creating a database
        mDatabase = openOrCreateDatabase(DATABASE_NAME, MODE_PRIVATE, null);
        //creating a table
        createQCInfoTable();

        /////// End of Data Save////////

        /////// Condition ////////
        txtSyncAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //startActivity(new Intent(ActivityQcInspector.this, MainActivity.class));
            }
        });

        btnSync.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // SyncQCInfoToDB();
                startActivity(new Intent(ActivityQcInspector.this, MainActivity.class));
            }
        });

        btnCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SetStyleColor() == false) {
                    ShowMassage(msg);
                    return;
                } else {
                    GetTotalCount();
                    count = 0;
                    SetStopWatch();
                }
            }
        });
        txtCounter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SetStyleColor() == false) {
                    ShowMassage(msg);
                    return;
                } else {
                    GetTotalCount();
                    count = 0;
                    SetStopWatch();
                }
            }
        });

        btnCorrection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (total > 0) {
                    ResetReject();
                    SetStopWatch();
                }
            }
        });


        //SetReject();
        btnRejectOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SetStyleColor() == false) {
                    ShowMassage(msg);
                    return;
                } else {
                    count = 1;
                    totalQC++;
                    totalReject++;
                    totalRejectOne++;
                    txtRejectOne.setText(String.valueOf(totalRejectOne));
                    ResetTime(1);
                    SetStopWatch();
                }
            }
        });
        btnRejectTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SetStyleColor() == false) {
                    ShowMassage(msg);
                    return;
                } else {
                    count = 2;
                    totalQC++;
                    totalReject++;
                    totalRejectTwo++;
                    txtRejectTwo.setText(String.valueOf(totalRejectTwo));
                    ResetTime(2);
                    SetStopWatch();
                }
            }
        });

        btnRejectThree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SetStyleColor() == false) {
                    ShowMassage(msg);
                    return;
                } else {
                    count = 3;
                    totalQC++;
                    totalReject++;
                    totalRejectThree++;
                    txtRejectThree.setText(String.valueOf(totalRejectThree));
                    ResetTime(3);
                    SetStopWatch();
                }
            }
        });

        btnRejectFour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SetStyleColor() == false) {
                    ShowMassage(msg);
                    return;
                } else {
                    count = 4;
                    totalQC++;
                    totalReject++;
                    totalRejectFour++;
                    txtRejectFour.setText(String.valueOf(totalRejectFour));
                    ResetTime(4);
                    SetStopWatch();
                }
            }
        });

        btnRejectFive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SetStyleColor() == false) {
                    ShowMassage(msg);
                    return;
                } else {
                    count = 5;
                    totalQC++;
                    totalReject++;
                    totalRejectFive++;
                    txtRejectFive.setText(String.valueOf(totalRejectFive));
                    ResetTime(5);
                    SetStopWatch();
                }
            }
        });

        btnRejectSix.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SetStyleColor() == false) {
                    ShowMassage(msg);
                    return;
                } else {
                    count = 6;
                    totalQC++;
                    totalReject++;
                    totalRejectSix++;
                    txtRejectSix.setText(String.valueOf(totalRejectSix));
                    ResetTime(6);
                    SetStopWatch();
                }
            }
        });


        //SetRejectCount();
        txtRejectOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SetStyleColor() == false) {
                    ShowMassage(msg);
                    return;
                } else {
                    count = 1;
                    totalQC++;
                    totalReject++;
                    totalRejectOne++;
                    txtRejectOne.setText(String.valueOf(totalRejectOne));
                    ResetTime(1);
                    SetStopWatch();
                }
            }
        });
        txtRejectTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SetStyleColor() == false) {
                    ShowMassage(msg);
                    return;
                } else {
                    count = 2;
                    totalQC++;
                    totalReject++;
                    totalRejectTwo++;
                    txtRejectTwo.setText(String.valueOf(totalRejectTwo));
                    ResetTime(2);
                    SetStopWatch();
                }
            }
        });

        txtRejectThree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SetStyleColor() == false) {
                    ShowMassage(msg);
                    return;
                } else {
                    count = 3;
                    totalQC++;
                    totalReject++;
                    totalRejectThree++;
                    txtRejectThree.setText(String.valueOf(totalRejectThree));
                    ResetTime(3);
                    SetStopWatch();
                }
            }
        });

        txtRejectFour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SetStyleColor() == false) {
                    ShowMassage(msg);
                    return;
                } else {
                    count = 4;
                    totalQC++;
                    totalReject++;
                    totalRejectFour++;
                    txtRejectFour.setText(String.valueOf(totalRejectFour));
                    ResetTime(4);
                    SetStopWatch();
                }
            }
        });

        txtRejectFive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SetStyleColor() == false) {
                    ShowMassage(msg);
                    return;
                } else {
                    count = 5;
                    totalQC++;
                    totalReject++;
                    totalRejectFive++;
                    txtRejectFive.setText(String.valueOf(totalRejectFive));
                    ResetTime(5);
                    SetStopWatch();
                }
            }
        });

        txtRejectSix.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SetStyleColor() == false) {
                    ShowMassage(msg);
                    return;
                } else {
                    count = 6;
                    totalQC++;
                    totalReject++;
                    totalRejectSix++;
                    txtRejectSix.setText(String.valueOf(totalRejectSix));
                    ResetTime(6);
                    SetStopWatch();
                }
            }
        });

        /////// End of Condition ////////

        retrieveJSON();
        retrieveStyleName();
        retrieveColorName();
        retrieveSize();
        retrieveSetPack();
        //retrieveLine();
    }

    private void SetStopWatch() {
        StartTime = SystemClock.uptimeMillis();
        handler.postDelayed(runnable, 0);
    }

    private void retrieveJSON() {

        showSimpleProgressDialog(this, "Loading...", "Fetching Json", false);

        //"https://jsonplaceholder.typicode.com/posts";//
        String URLstring = "https://demonuts.com/Demonuts/JsonTest/Tennis/json_parsing.php";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URLstring,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("strrrrr", ">>" + response);

                        try {

                            JSONObject obj = new JSONObject(response);
                            if (obj.optString("status").equals("true")) {

                                goodModelArrayList = new ArrayList<>();
                                Model.GoodModel model = new Model.GoodModel();
                                model.setName("Style Name");
                                goodModelArrayList.add(model);
                                JSONArray dataArray = obj.getJSONArray("data");

                                for (int i = 0; i < dataArray.length(); i++) {

                                    Model.GoodModel playerModel = new Model.GoodModel();
                                    JSONObject dataobj = dataArray.getJSONObject(i);

                                    playerModel.setName(dataobj.getString("name"));
                                    playerModel.setCountry(dataobj.getString("country"));
                                    playerModel.setCity(dataobj.getString("city"));
                                    playerModel.setImgURL(dataobj.getString("imgURL"));

                                    goodModelArrayList.add(playerModel);
                                }

                                for (int i = 0; i < goodModelArrayList.size(); i++) {
                                    names.add(goodModelArrayList.get(i).getName());
                                }

                                ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(ActivityQcInspector.this, simple_spinner_item, names);
                                spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
                                spinner.setAdapter(spinnerArrayAdapter);
                                removeSimpleProgressDialog();

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //displaying the error in toast if occurrs
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

        // request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void retrieveStyleName() {

        showSimpleProgressDialog(this, "Loading...", "Fetching Data From Api", false);
        int count = 0;
        //String u = txtUser.getText().toString(), p = txtPass.getText().toString();
        String sql = "SELECT Styleid, Name FROM styles;";
        try {
            mDatabase = openOrCreateDatabase(DATABASE_NAME, MODE_PRIVATE, null);
            Cursor cursorStyles = mDatabase.rawQuery(sql, null);
            cursorStyles.moveToFirst();
            count = cursorStyles.getInt(0);

            if (count > 0) {
                if (cursorStyles.moveToFirst()) {
                    goodModelArrayList = new ArrayList<>();
                    Model.GoodModel model = new Model.GoodModel();
                    model.setId(0);
                    model.setName("Style Name");
                    goodModelArrayList.add(model);

                    do {
                        Model.GoodModel playerModel = new Model.GoodModel();
                        playerModel.setId(cursorStyles.getInt(0));
                        playerModel.setName(cursorStyles.getString(1));
                        goodModelArrayList.add(playerModel);

                    } while (cursorStyles.moveToNext());
                }
                //closing the cursor
                cursorStyles.close();

                for (int i = 0; i < goodModelArrayList.size(); i++) {
                    styles.add(goodModelArrayList.get(i).getName());
                }

                ArrayAdapter<String> spinnerArrayAdapter;
                spinnerArrayAdapter = new ArrayAdapter<>(ActivityQcInspector.this, simple_spinner_item, styles);
                spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
                spinnerStyle.setAdapter(spinnerArrayAdapter);
                removeSimpleProgressDialog();

            }
        } catch (Exception e) {
            Toast toast = Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT);
            toast.show();
        }

        /*showSimpleProgressDialog(this, "Loading...", "Fetching Data From Api", false);
        String urlstyle = "http://178.128.57.120/Merchandising/GetallStyle";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, urlstyle,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("strrrrr", ">>" + response);

                        try {

                            //JSONObject obj = new JSONObject(response);
                            //if(obj.optString("status").equals("true")){

                            goodModelArrayList = new ArrayList<Model.GoodModel>();
                            Model.GoodModel model = new Model.GoodModel();
                            model.setId(0);
                            model.setName("Style Name");
                            goodModelArrayList.add(model);

                            //String datastring = new JSONObject(response).toString();
                            JSONArray dataArray = new JSONArray(response);
                            //JSONArray dataArray = obj.getJSONArray("data");

                            for (int i = 0; i < dataArray.length(); i++) {

                                Model.GoodModel playerModel = new Model.GoodModel();
                                JSONObject data = dataArray.getJSONObject(i);
                                playerModel.setId(data.getInt("id"));
                                playerModel.setName(data.getString("name"));

                               *//* playerModel.setCountry(data.getString("country"));
                                playerModel.setCity(data.getString("city"));
                                playerModel.setImgURL(data.getString("imgURL"));*//*

                                goodModelArrayList.add(playerModel);
                            }

                            for (int i = 0; i < goodModelArrayList.size(); i++) {
                               *//* GoodModel goodModel = new GoodModel();
                                goodModel.setId(goodModelArrayList.get(i).getId());
                                goodModel.setName(goodModelArrayList.get(i).getName());*//*
                                styles.add(goodModelArrayList.get(i).getName());
                            }

                            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(ActivityQcInspector.this, simple_spinner_item, styles);
                            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
                            spinnerStyle.setAdapter(spinnerArrayAdapter);
                            removeSimpleProgressDialog();
                            //}

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //displaying the error in toast if occurrs
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);*/
    }

    private void retrieveColorName() {

        showSimpleProgressDialog(this, "Loading...", "Fetching Data From Api", false);
        int count = 0;
        //String u = txtUser.getText().toString(), p = txtPass.getText().toString();
        String sql = "SELECT ColorId, Name FROM colors;";
        try {
            mDatabase = openOrCreateDatabase(DATABASE_NAME, MODE_PRIVATE, null);
            Cursor cursorStyles = mDatabase.rawQuery(sql, null);
            cursorStyles.moveToFirst();
            count = cursorStyles.getInt(0);

            if (count > 0) {
                if (cursorStyles.moveToFirst()) {
                    goodModelArrayList = new ArrayList<>();
                    Model.GoodModel model = new Model.GoodModel();
                    model.setId(0);
                    model.setName("Color");
                    goodModelArrayList.add(model);

                    do {
                        Model.GoodModel playerModel = new Model.GoodModel();
                        playerModel.setId(cursorStyles.getInt(0));
                        playerModel.setName(cursorStyles.getString(1));
                        goodModelArrayList.add(playerModel);

                    } while (cursorStyles.moveToNext());
                }
                //closing the cursor
                cursorStyles.close();

                for (int i = 0; i < goodModelArrayList.size(); i++) {
                    colors.add(goodModelArrayList.get(i).getName());
                }

                ArrayAdapter<String> spinnerArrayAdapter;
                spinnerArrayAdapter = new ArrayAdapter<>(ActivityQcInspector.this, simple_spinner_item, colors);
                spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
                spinnerColor.setAdapter(spinnerArrayAdapter);
                removeSimpleProgressDialog();

            }
        } catch (Exception e) {
            Toast toast = Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT);
            toast.show();
        }

        /*showSimpleProgressDialog(this, "Loading...", "Fetching Data From Api", false);

        String ulcer = "http://178.128.57.120/Home/GetAllColor";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, ulcer,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("strrrrr", ">>" + response);

                        try {

                            //JSONObject obj = new JSONObject(response);
                            //if(obj.optString("status").equals("true")){

                            goodModelArrayList = new ArrayList<>();
                            Model.GoodModel model = new Model.GoodModel();
                            model.setName("Style Color");
                            goodModelArrayList.add(model);

                            //String datastring = new JSONObject(response).toString();
                            JSONArray dataArray = new JSONArray(response);
                            //JSONArray dataArray = obj.getJSONArray("data");

                            for (int i = 0; i < dataArray.length(); i++) {

                                Model.GoodModel playerModel = new Model.GoodModel();
                                JSONObject data = dataArray.getJSONObject(i);
                                playerModel.setName(data.getString("name"));

                               *//* playerModel.setCountry(data.getString("country"));
                                playerModel.setCity(data.getString("city"));
                                playerModel.setImgURL(data.getString("imgURL"));*//*

                                goodModelArrayList.add(playerModel);
                            }

                            for (int i = 0; i < goodModelArrayList.size(); i++) {
                                colors.add(goodModelArrayList.get(i).getName());
                            }

                            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>(ActivityQcInspector.this, simple_spinner_item, colors);
                            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
                            spinnerColor.setAdapter(spinnerArrayAdapter);
                            removeSimpleProgressDialog();
                            //}

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //displaying the error in toast if occurrs
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

        // request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        requestQueue.add(stringRequest);*/


    }

    private void retrieveSize() {

        showSimpleProgressDialog(this, "Loading...", "Fetching Data From Api", false);
        int count = 0;
        //String u = txtUser.getText().toString(), p = txtPass.getText().toString();
        String sql = "SELECT SizeId, Name FROM sizes;";
        try {
            mDatabase = openOrCreateDatabase(DATABASE_NAME, MODE_PRIVATE, null);
            Cursor cursorStyles = mDatabase.rawQuery(sql, null);
            cursorStyles.moveToFirst();
            count = cursorStyles.getInt(0);

            if (count > 0) {
                if (cursorStyles.moveToFirst()) {
                    goodModelArrayList = new ArrayList<>();
                    Model.GoodModel model = new Model.GoodModel();
                    model.setId(0);
                    model.setName("Size");
                    goodModelArrayList.add(model);

                    do {
                        Model.GoodModel playerModel = new Model.GoodModel();
                        playerModel.setId(cursorStyles.getInt(0));
                        playerModel.setName(cursorStyles.getString(1));
                        goodModelArrayList.add(playerModel);

                    } while (cursorStyles.moveToNext());
                }
                //closing the cursor
                cursorStyles.close();

                for (int i = 0; i < goodModelArrayList.size(); i++) {
                    sizes.add(goodModelArrayList.get(i).getName());
                }

                ArrayAdapter<String> spinnerArrayAdapter;
                spinnerArrayAdapter = new ArrayAdapter<>(ActivityQcInspector.this, simple_spinner_item, sizes);
                spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
                spinnerSize.setAdapter(spinnerArrayAdapter);
                removeSimpleProgressDialog();

            }
        } catch (Exception e) {
            Toast toast = Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT);
            toast.show();
        }

        /*showSimpleProgressDialog(this, "Loading...", "Fetching Data From Api", false);

        String ursine = "http://178.128.57.120/Home/GetAllSize";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, ursine,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("strrrrr", ">>" + response);

                        try {

                            //JSONObject obj = new JSONObject(response);
                            //if(obj.optString("status").equals("true")){

                            goodModelArrayList = new ArrayList<>();
                            Model.GoodModel model = new Model.GoodModel();
                            model.setName("Style Size");
                            goodModelArrayList.add(model);

                            //String datastring = new JSONObject(response).toString();
                            JSONArray dataArray = new JSONArray(response);
                            //JSONArray dataArray = obj.getJSONArray("data");

                            for (int i = 0; i < dataArray.length(); i++) {

                                Model.GoodModel playerModel = new Model.GoodModel();
                                JSONObject data = dataArray.getJSONObject(i);
                                playerModel.setName(data.getString("name"));

                               *//* playerModel.setCountry(data.getString("country"));
                                playerModel.setCity(data.getString("city"));
                                playerModel.setImgURL(data.getString("imgURL"));*//*

                                goodModelArrayList.add(playerModel);
                            }

                            for (int i = 0; i < goodModelArrayList.size(); i++) {
                                sizes.add(goodModelArrayList.get(i).getName());
                            }

                            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>(ActivityQcInspector.this, simple_spinner_item, sizes);
                            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
                            spinnerSize.setAdapter(spinnerArrayAdapter);
                            removeSimpleProgressDialog();
                            //}

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //displaying the error in toast if occurrs
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

        // request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);*/
    }

    private void retrieveSetPack() {

        showSimpleProgressDialog(this, "Loading...", "Fetching Data From Api", false);
        int count = 0;
        //String u = txtUser.getText().toString(), p = txtPass.getText().toString();
        String sql = "SELECT SetpackId, name FROM subsets;";
        try {
            mDatabase = openOrCreateDatabase(DATABASE_NAME, MODE_PRIVATE, null);
            Cursor cursorStyles = mDatabase.rawQuery(sql, null);
            cursorStyles.moveToFirst();
            count = cursorStyles.getInt(0);

            if (count > 0) {
                if (cursorStyles.moveToFirst()) {
                    goodModelArrayList = new ArrayList<>();
                    Model.GoodModel model = new Model.GoodModel();
                    model.setId(0);
                    model.setName("Set Pack");
                    goodModelArrayList.add(model);

                    do {
                        Model.GoodModel playerModel = new Model.GoodModel();
                        playerModel.setId(cursorStyles.getInt(0));
                        playerModel.setName(cursorStyles.getString(1));
                        goodModelArrayList.add(playerModel);

                    } while (cursorStyles.moveToNext());
                }
                //closing the cursor
                cursorStyles.close();

                for (int i = 0; i < goodModelArrayList.size(); i++) {
                    setpack.add(goodModelArrayList.get(i).getName());
                }

                ArrayAdapter<String> spinnerArrayAdapter;
                spinnerArrayAdapter = new ArrayAdapter<>(ActivityQcInspector.this, simple_spinner_item, setpack);
                spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
                spinnerSetback.setAdapter(spinnerArrayAdapter);
                removeSimpleProgressDialog();

            }
        } catch (Exception e) {
            Toast toast = Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT);
            toast.show();
        }

        /*showSimpleProgressDialog(this, "Loading...", "Fetching Data From Api", false);

        String urlsetpack = "http://178.128.57.120/Merchandising/GetAllSetPack";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, urlsetpack,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("strrrrr", ">>" + response);

                        try {

                            //JSONObject obj = new JSONObject(response);
                            //if(obj.optString("status").equals("true")){

                            goodModelArrayList = new ArrayList<>();
                            Model.GoodModel model = new Model.GoodModel();
                            model.setName("Style Subset");
                            goodModelArrayList.add(model);

                            //String datastring = new JSONObject(response).toString();
                            JSONArray dataArray = new JSONArray(response);
                            //JSONArray dataArray = obj.getJSONArray("data");

                            for (int i = 0; i < dataArray.length(); i++) {

                                Model.GoodModel playerModel = new Model.GoodModel();
                                JSONObject data = dataArray.getJSONObject(i);
                                playerModel.setName(data.getString("setPackName"));

                                goodModelArrayList.add(playerModel);
                            }

                            for (int i = 0; i < goodModelArrayList.size(); i++) {
                                setpack.add(goodModelArrayList.get(i).getName());
                            }

                            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>(ActivityQcInspector.this, simple_spinner_item, setpack);
                            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
                            spinnerSetback.setAdapter(spinnerArrayAdapter);
                            removeSimpleProgressDialog();
                            //}

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //displaying the error in toast if occurrs
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

        // request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);*/
    }

    private void retrieveLine() {

        showSimpleProgressDialog(this, "Loading...", "Fetching Data From Api", false);
        String unlike = "http://178.128.57.120/Apps/GetAllSize";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, unlike,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d("string", ">>" + response);

                        try {

                            //JSONObject obj = new JSONObject(response);
                            //if(obj.optString("status").equals("true")){

                            goodModelArrayList = new ArrayList<>();
                            Model.GoodModel model = new Model.GoodModel();
                            model.setName("Style Line");
                            goodModelArrayList.add(model);
                            JSONArray dataArray = new JSONArray(response);

                            for (int i = 0; i < dataArray.length(); i++) {

                                Model.GoodModel playerModel = new Model.GoodModel();
                                JSONObject data = dataArray.getJSONObject(i);
                                playerModel.setName(data.getString("name"));

                                goodModelArrayList.add(playerModel);
                            }

                            for (int i = 0; i < goodModelArrayList.size(); i++)
                                line.add(goodModelArrayList.get(i).getName());

                            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<>(ActivityQcInspector.this, simple_spinner_item, line);
                            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
                            spinnerLine.setAdapter(spinnerArrayAdapter);
                            removeSimpleProgressDialog();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //displaying the error in toast if occurrs
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

        // request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void ResetReject() {

        if (count == 0) {
            total--;
            txtCounter.setText(String.valueOf(total));
        }
        if (count == 1 && totalRejectOne > 0) {
            totalReject--;
            totalRejectOne--;
            txtRejectOne.setText(String.valueOf(totalRejectOne));
        }
        if (count == 2 && totalRejectTwo > 0) {
            totalReject--;
            totalRejectTwo--;
            txtRejectTwo.setText(String.valueOf(totalRejectTwo));
        }
        if (count == 3 && totalRejectThree > 0) {
            totalReject--;
            totalRejectThree--;
            txtRejectThree.setText(String.valueOf(totalRejectThree));
        }
        if (count == 4 && totalRejectFour > 0) {
            totalReject--;
            totalRejectFour--;
            txtRejectFour.setText(String.valueOf(totalRejectFour));
        }
        if (count == 5 && totalRejectFive > 0) {
            totalReject--;
            totalRejectFive--;
            txtRejectFive.setText(String.valueOf(totalRejectFive));
        }
        if (count == 6 && totalRejectSix > 0) {
            totalReject--;
            totalRejectSix--;
            txtRejectSix.setText(String.valueOf(totalRejectSix));
        }
        ResetTime(count);
    }

    private void GetTotalCount() {
        String styleName = spinnerStyle.getSelectedItem().toString();
        String color = spinnerColor.getSelectedItem().toString();
        String size = spinnerSize.getSelectedItem().toString();
        String user = txtUser.getText().toString();

        Long styleId = spinnerStyle.getSelectedItemId();
        Long colorId = spinnerColor.getSelectedItemId();
        Long sizeId = spinnerSize.getSelectedItemId();

        Long userId = Long.valueOf(1);
        Long stylevalue = new Long(0);
        Long colorvlaue = new Long(0);
        Long sizevlaue = new Long(0);
        String uservalue = "";

        if (user.equals(uservalue)) {
            ShowMassage("Please Login Again");
            startActivity(new Intent(ActivityQcInspector.this, MainActivity.class));
            //return;
        }
        if (styleId.equals(stylevalue)) {       //styleName.equals("Style Name") ||  styleId.equals(0)
            ShowMassage("Please Select Style Name");
            spinnerStyle.requestFocus();
            return;
        }
        if (colorId.equals(colorvlaue)) {       //color.equals("Style Color") ||
            ShowMassage("Please Select Color Name");
            spinnerColor.requestFocus();
            return;
        }
        if (sizeId.equals(sizevlaue)) {         //size.equals("Style Size") ||
            ShowMassage("Please Select Size");
            spinnerSize.requestFocus();
            return;
        } else {
            if (total >= 0) {
                total++;
                txtCounter.setText(String.valueOf(total));
            }
            AddQCInformation();
            ResetTime(0);
        }
    }

    private void createQCInfoTable() {
        String sql = "CREATE TABLE IF NOT EXISTS Store_QCInformation (" +
                "ID INTEGER PRIMARY KEY AUTOINCREMENT, StyleName varchar(200) NOT NULL, Color varchar(200) NOT NULL, Size varchar(200) NOT NULL, Subset varchar(200) NOT NULL," +
                "Merchandising_StyleIdFK INTEGER NOT NULL, Merchandising_ColorIdFK INTEGER NOT NULL, Merchandising_SizeIdFK INTEGER NOT NULL, Merchandising_SubsetIdFK INTEGER NOT NULL," +
                "EntryUserID INTEGER NOT NULL, EntryUser varchar(200) NOT NULL, BuyerName varchar(200) NOT NULL, EntryDate Datetime NOT NULL, " +
                "Time varchar(20) NOT NULL, Remarks varchar(400), QCQuantity double NOT NULL, RejectQuantityOne double NOT NULL, RejectQuantityTwo double NOT NULL, " +
                "RejectQuantityThree double NOT NULL, RejectQuantityFour double NOT NULL, RejectQuantityFive double NOT NULL, RejectQuantitySix double NOT NULL, " +
                "RejectQuantityTotal double NOT NULL, DeviceId varchar(200) NOT NULL);";
        mDatabase.execSQL(sql);
    }

    private void ShowMassage(String s) {
        Toast toast =
                Toast.makeText(this, s, Toast.LENGTH_SHORT);//.show();
        View v = toast.getView();
        v.setBackgroundColor(Color.RED);
        TextView view = toast.getView().findViewById(android.R.id.message);
        view.setTextColor(Color.WHITE);
        toast.show();
    }

    private void ResetTime(int id) {

        //handler.postDelayed(runnable, 0);

        totalQC = total + totalReject;
        txtRejectTotal.setText("Total Reject: " + String.valueOf(totalReject));
        txtTotalQC.setText("Total QC: " + String.valueOf(totalQC));

        if (id == 1) {
            //txtRejectOneTime.setText(strDate);
            txtRejectOneTime.setVisibility(TextView.VISIBLE);

            txtCountOKTime.setVisibility(TextView.INVISIBLE);
            txtRejectTwoTime.setVisibility(TextView.INVISIBLE);
            txtRejectThreeTime.setVisibility(TextView.INVISIBLE);
            txtRejectFourTime.setVisibility(TextView.INVISIBLE);
            txtRejectFiveTime.setVisibility(TextView.INVISIBLE);
            txtRejectSixTime.setVisibility(TextView.INVISIBLE);
            //btnRejectTotal.setText(String.valueOf(totalReject));
        } else if (id == 2) {
            //txtRejectTwoTime.setText(strDate);
            txtRejectTwoTime.setVisibility(TextView.VISIBLE);

            txtCountOKTime.setVisibility(TextView.INVISIBLE);
            txtRejectOneTime.setVisibility(TextView.INVISIBLE);
            txtRejectThreeTime.setVisibility(TextView.INVISIBLE);
            txtRejectFourTime.setVisibility(TextView.INVISIBLE);
            txtRejectFiveTime.setVisibility(TextView.INVISIBLE);
            txtRejectSixTime.setVisibility(TextView.INVISIBLE);
            //btnRejectTotal.setText(String.valueOf(totalReject));
        } else if (id == 3) {
            //txtRejectThreeTime.setText(strDate);
            txtRejectThreeTime.setVisibility(TextView.VISIBLE);

            txtCountOKTime.setVisibility(TextView.INVISIBLE);
            txtRejectOneTime.setVisibility(TextView.INVISIBLE);
            txtRejectTwoTime.setVisibility(TextView.INVISIBLE);
            txtRejectFourTime.setVisibility(TextView.INVISIBLE);
            txtRejectFiveTime.setVisibility(TextView.INVISIBLE);
            txtRejectSixTime.setVisibility(TextView.INVISIBLE);
            //btnRejectTotal.setText(String.valueOf(totalReject));
        } else if (id == 4) {
            //txtRejectFourTime.setText(strDate);
            txtRejectFourTime.setVisibility(TextView.VISIBLE);

            txtCountOKTime.setVisibility(TextView.INVISIBLE);
            txtRejectOneTime.setVisibility(TextView.INVISIBLE);
            txtRejectTwoTime.setVisibility(TextView.INVISIBLE);
            txtRejectThreeTime.setVisibility(TextView.INVISIBLE);
            txtRejectFiveTime.setVisibility(TextView.INVISIBLE);
            txtRejectSixTime.setVisibility(TextView.INVISIBLE);
            //btnRejectTotal.setText(String.valueOf(totalReject));
        } else if (id == 5) {
            //txtRejectFiveTime.setText(strDate);
            txtRejectFiveTime.setVisibility(TextView.VISIBLE);

            txtCountOKTime.setVisibility(TextView.INVISIBLE);
            txtRejectOneTime.setVisibility(TextView.INVISIBLE);
            txtRejectTwoTime.setVisibility(TextView.INVISIBLE);
            txtRejectThreeTime.setVisibility(TextView.INVISIBLE);
            txtRejectFourTime.setVisibility(TextView.INVISIBLE);
            txtRejectSixTime.setVisibility(TextView.INVISIBLE);
            //btnRejectTotal.setText(String.valueOf(totalReject));
        } else if (id == 6) {
            //txtRejectSixTime.setText(strDate);
            txtRejectSixTime.setVisibility(TextView.VISIBLE);

            txtCountOKTime.setVisibility(TextView.INVISIBLE);
            txtRejectOneTime.setVisibility(TextView.INVISIBLE);
            txtRejectTwoTime.setVisibility(TextView.INVISIBLE);
            txtRejectThreeTime.setVisibility(TextView.INVISIBLE);
            txtRejectFourTime.setVisibility(TextView.INVISIBLE);
            txtRejectFiveTime.setVisibility(TextView.INVISIBLE);
            //btnRejectTotal.setText(String.valueOf(totalReject));
        } else {
            //txtCountOKTime.setText(strDate);
            txtCountOKTime.setVisibility(TextView.VISIBLE);

            txtRejectOneTime.setVisibility(TextView.INVISIBLE);
            txtRejectTwoTime.setVisibility(TextView.INVISIBLE);
            txtRejectThreeTime.setVisibility(TextView.INVISIBLE);
            txtRejectFourTime.setVisibility(TextView.INVISIBLE);
            txtRejectFiveTime.setVisibility(TextView.INVISIBLE);
            txtRejectSixTime.setVisibility(TextView.INVISIBLE);
            //txtTotalQC.setText(total);
        }
        //count=100;
    }

    private void AddQCInformation() {

        String styleName = spinnerStyle.getSelectedItem().toString().trim();
        String color = spinnerColor.getSelectedItem().toString().trim();
        String size = spinnerSize.getSelectedItem().toString().trim();
        String set = spinnerSetback.getSelectedItem().toString().trim();
        String user = txtUser.getText().toString().trim();

        final String styleId = String.valueOf(spinnerStyle.getSelectedItemId());
        final String colorId = String.valueOf(spinnerColor.getSelectedItemId());
        final String sizeId = String.valueOf(spinnerSize.getSelectedItemId());
        final String setId = String.valueOf(spinnerSetback.getSelectedItemId());
        final String userId = String.valueOf(1);

        String buyer = "Buyer Name";
        final String qcPass = txtCounter.getText().toString().trim();
        final String rejection = String.valueOf(totalReject);
        final String time = txtTime.getText().toString().trim();
        final String rejectone = txtRejectOne.getText().toString().trim();
        final String rejecttwo = txtRejectTwo.getText().toString().trim();
        final String rejectthree = txtRejectThree.getText().toString().trim();
        final String rejectfour = txtRejectFour.getText().toString().trim();
        final String rejectfive = txtRejectFive.getText().toString().trim();
        final String rejectsix = txtRejectSix.getText().toString().trim();

        String note = txtNote.getText().toString().trim();

        //getting the current time for joining date
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
        final String date = sdf.format(cal.getTime());
        final String device = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);

        //mDatabase.execSQL("DROP TABLE IF EXISTS subsets");
        String insertSQL = "INSERT INTO Store_QCInformation (ID, StyleName, Color, Size, Subset, Merchandising_StyleIdFK, Merchandising_ColorIdFK, Merchandising_SizeIdFK, Merchandising_SubsetIdFK, EntryUserID, EntryUser, BuyerName, EntryDate, Time, Remarks," +
                " QCQuantity, RejectQuantityOne, RejectQuantityTwo, RejectQuantityThree, RejectQuantityFour, RejectQuantityFive, RejectQuantitySix, RejectQuantityTotal, DeviceId) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
        mDatabase.execSQL(insertSQL, new String[]{null, styleName, color, size, set, styleId, colorId, sizeId, setId, userId, user, buyer, date, time, note,
                qcPass, rejectone, rejecttwo, rejectthree, rejectfour, rejectfive, rejectsix, rejection, device});
        id = id + 1;

        showSimpleProgressDialog(this, "Working...", "Data Send Throwing  Api", false);
        restService = new RestService();
        Model.QCModel qcModel = new Model.QCModel();

        qcModel.Id = 0;
        qcModel.DeviceId = device;
        qcModel.Merchandising_StyleIdFK = styleId;
        qcModel.Merchandising_StyleSetPackFK = setId;
        qcModel.Common_ColorFK = colorId;
        qcModel.Common_SizeFK = sizeId;
        qcModel.Common_ProductionLineFK = "1";

        qcModel.UserId = userId;
        qcModel.Date = date;
        qcModel.Time = time;
        qcModel.Active = true;
        qcModel.Remark = note;

        qcModel.QCQuantity = qcPass;
        qcModel.RejectQuantityOne = rejectone;
        qcModel.RejectQuantityTwo = rejecttwo;
        qcModel.RejectQuantityThree = rejectthree;
        qcModel.RejectQuantityFour = rejectfour;
        qcModel.RejectQuantityFive = rejectfive;
        qcModel.RejectQuantitySix = rejectsix;
        qcModel.RejectQuantityTotal = rejection;

        if (true) {
            restService.getService().addStudent(qcModel, new Callback<Model.QCModel>() {
                @Override
                public void success(Model.QCModel qcModel, retrofit.client.Response response) {
                    Toast.makeText(ActivityQcInspector.this, "QC Information Inserted.", Toast.LENGTH_LONG).show();
                    removeSimpleProgressDialog();
                }

                @Override
                public void failure(RetrofitError error) {
                    Toast.makeText(ActivityQcInspector.this, error.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            restService.getService().updateStudentById(1, qcModel, new Callback<Model.QCModel>() {
                @Override
                public void success(Model.QCModel student, retrofit.client.Response response) {
                    Toast.makeText(ActivityQcInspector.this, "QC Information Inserted.", Toast.LENGTH_LONG).show();
                }

                @Override
                public void failure(RetrofitError error) {
                    Toast.makeText(ActivityQcInspector.this, error.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
            removeSimpleProgressDialog();
        }
    }

    public boolean checkNetworkConnection() {
        ConnectivityManager connMgr = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        boolean isConnected = false;
        if (networkInfo != null && (isConnected = networkInfo.isConnected())) {
            // show "Connected" & type of network "WIFI or MOBILE"
            showmsg.setText("Connected " + networkInfo.getTypeName());
            // change background color to red
            showmsg.setBackgroundColor(0xFF7CCC26);


        } else {
            // show "Not Connected"
            showmsg.setText("Not Connected");
            // change background color to green
            showmsg.setBackgroundColor(0xFFFF0000);
        }

        return isConnected;
    }

    private boolean SetStyleColor() {
        String user = txtUser.getText().toString();
        Long styleId = spinnerStyle.getSelectedItemId();
        Long colorId = spinnerColor.getSelectedItemId();
        Long sizeId = spinnerSize.getSelectedItemId();
        Long stylevalue = new Long(0);
        Long colorvlaue = new Long(0);
        Long sizevlaue = new Long(0);
        String uservalue = "";

        if (user.equals(uservalue)) {
            ShowMassage("Please Login Again");
            startActivity(new Intent(ActivityQcInspector.this, MainActivity.class));
            //return false;
        }
        if (styleId.equals(stylevalue)) {       //styleName.equals("Style Name") ||  styleId.equals(0)
            msg = "Please Select Style Name";
            //spinnerStyle.requestFocus();
            return false;
        }
        if (colorId.equals(colorvlaue)) {       //color.equals("Style Color") ||
            msg = "Please Select Color Name";
            //spinnerColor.requestFocus();
            return false;
        }
        if (sizeId.equals(sizevlaue)) {         //size.equals("Style Size") ||
            msg = "Please Select Size";
            //spinnerSize.requestFocus();
            return false;
        }
        return true;
    }

    public static void removeSimpleProgressDialog() {
        try {
            if (mProgressDialog != null) {
                if (mProgressDialog.isShowing()) {
                    mProgressDialog.dismiss();
                    mProgressDialog = null;
                }
            }
        } catch (IllegalArgumentException ie) {
            ie.printStackTrace();

        } catch (RuntimeException re) {
            re.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void showSimpleProgressDialog(Context context, String title, String msg, boolean isCancelable) {
        try {
            if (mProgressDialog == null) {
                mProgressDialog = ProgressDialog.show(context, title, msg);
                mProgressDialog.setCancelable(isCancelable);
            }

            if (!mProgressDialog.isShowing()) {
                mProgressDialog.show();
            }

        } catch (IllegalArgumentException ie) {
            ie.printStackTrace();
        } catch (RuntimeException re) {
            re.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Runnable runnable = new Runnable() {

        public void run() {
            MillisecondTime = SystemClock.uptimeMillis() - StartTime;
            UpdateTime = TimeBuff + MillisecondTime;
            Seconds = (int) (UpdateTime / 1000);
            Minutes = Seconds / 60;
            Seconds = Seconds % 60;
            MilliSeconds = (int) (UpdateTime % 1000);
            String time = String.format("%02d", Minutes) + ":" + String.format("%02d", Seconds); //+ ":" + String.format("%03d", MilliSeconds);
            SetTime(time);
            handler.postDelayed(this, 0);
        }

        private void SetTime(String time) {

            if (count == 1) {
                txtRejectOneTime.setText(time);
            }
            if (count == 2) {
                txtRejectTwoTime.setText(time);
            }
            if (count == 3) {
                txtRejectThreeTime.setText(time);
            }
            if (count == 4) {
                txtRejectFourTime.setText(time);
            }
            if (count == 5) {
                txtRejectFiveTime.setText(time);
            }
            if (count == 6) {
                txtRejectSixTime.setText(time);
            } else {
                txtCountOKTime.setText(time);
            }
        }
    };
}
