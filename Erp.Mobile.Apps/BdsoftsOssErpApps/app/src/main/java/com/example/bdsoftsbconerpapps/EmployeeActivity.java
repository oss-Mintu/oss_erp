package com.example.bdsoftsbconerpapps;

import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class EmployeeActivity extends AppCompatActivity {

    public static final String DATABASE_NAME = "erp";
    TextView textViewViewEmployees;
    EditText editTextName, editTextPass;
    Spinner spinnerDepartment;
    Button btnAddEmp;
    int id;
    SQLiteDatabase mDatabase;
    List<Model.EmployeeModel> employeeList;
    ListView listViewEmployees;

    public EmployeeActivity() {
        id = 1;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee);

        textViewViewEmployees = findViewById(R.id.textViewViewEmployees);
        editTextName = findViewById(R.id.editTextName);
        editTextPass = findViewById(R.id.editTextPass);
        spinnerDepartment = findViewById(R.id.spinnerDepartment);
        btnAddEmp = findViewById(R.id.buttonAddEmployee);

        mDatabase = openOrCreateDatabase(DATABASE_NAME, MODE_PRIVATE, null);
        createEmployeeTable();
        ShowMessage("Employee Table Created");

        btnAddEmp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddEmployee();
            }
        });

        textViewViewEmployees.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        listViewEmployees = findViewById(R.id.listViewEmployees);
        employeeList = new ArrayList<>();
    }
    private void createEmployeeTable() {
        String sql = "CREATE TABLE IF NOT EXISTS employees (" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT, name varchar(200) NOT NULL," +
                "department varchar(200) NOT NULL, joiningdate datetime NOT NULL," +
                "pass varchar(200) NOT NULL);";
        mDatabase.execSQL(sql);
    }
    private void AddEmployee() {

        String name = editTextName.getText().toString().trim();
        String pass = editTextPass.getText().toString().trim();
        String dept = spinnerDepartment.getSelectedItem().toString();

        //getting the current time for joining date
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
        String joining = sdf.format(cal.getTime());

        //validating the inputs
        //if (inputsAreCorrect(name, pass)) {
        String insertSQL = "INSERT INTO employees (id, name, department, joiningdate, pass) VALUES (?, ?, ?, ?, ?);";
        mDatabase.execSQL(insertSQL, new String[]{ null, name, dept, joining, pass});

        ShowMessage("Employee Added Successfully");
        //}
    }
    private void ShowMessage(String s) {
        Toast toast = Toast.makeText(this, s, Toast.LENGTH_SHORT);//.show();
        View v = toast.getView();
        v.setBackgroundColor(Color.RED);
        TextView view = toast.getView().findViewById(android.R.id.message);
        view.setTextColor(Color.WHITE);
        toast.show();
    }
}
