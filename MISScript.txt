USE [Erp.TempTest]
GO
/****** Object:  StoredProcedure [dbo].[Sp_MIS]    Script Date: 7/9/2019 4:15:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


ALTER Procedure [dbo].[Sp_MIS]
 AS
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION
				
				
			--- CM Earn Start
				BEGIN TRY
				BEGIN TRANSACTION
					DELETE [dbo].[Mis_CMEarned]
					INSERT INTO [dbo].[Mis_CMEarned]([Active],[Time],[Quantity],[Value],[Date],[Merchandising_StyleFK],[StyleName]) 
					SELECT
						1,
						GETDATE(),
						SUM(t1.Quantity) AS [Quantity],
						(t4.PieceQty * t4.UnitPrice) -   -- As [Order Value]
						(((isnull(t4.MakingCost,0) / 12) * SUM(t1.Quantity))  + --AS [Total Making Cost]
						(SELECT (ISNULL(SUM(ts1.TotalPrice),0) / t4.PieceQty) * SUM(t1.Quantity)
					FROM Merchandising_StyleSlave ts1 WHERE ts1.Active = 1 AND ts1.Merchandising_StyleFK =  t4.ID)  + -- [Total BOM Cost]
					(t4.PieceQty * t4.UnitPrice) * (isnull(cast(t4.PieceQty as decimal) ,0) * isnull(cast(t4.UnitPrice AS decimal),0) / 100 * isnull(t5.Commission , 0) / 100)) + --AS [Total Buyer Commission]
					((SUM(t1.Quantity) * t4.UnitPrice)/ 100) *(isnull(t4.OverheadRate,0)) AS [Value],
					GETDATE(),
					t4.ID,
					t5.BuyerPO + '/' + t4.CID + '/' + t4.StyleName
					FROM Prod_ReferencePlanFollowup t1
					JOIN Prod_ReferencePlan t2 ON t1.Prod_ReferencePlanFK = t2.ID
					JOIN Merchandising_StyleShipmentSchedule t3  ON t1.Merchandising_StyleShipmentScheduleFK =  t3.ID
					JOIN Merchandising_Style t4 ON t3.Merchandising_StyleFK =  t4.ID
					JOIN Merchandising_BuyerOrder t5 ON t4.Merchandising_BuyerOrderFK = t5.ID   
				  
					where t1.Active = 1 and t2.Active = 1 and t3.Active = 1 and 
					t4.Active = 1 and t5.Active = 1 and t2.SectionId = 2
					group by t4.ID,t4.PieceQty , t4.UnitPrice, t4.MakingCost,t5.Commission,t4.OverheadRate, t4.CID, t4.StyleName, t5.BuyerPO 

					
					   
					--SELECT					
					----1					
					-- convert(varchar, getdate(), 23)
					--,t4.StyleName
					--,t4.PieceQty AS [Order Quantity]
					--,t1.Quantity AS [Production Quantity]

					--,(isnull(t4.UnitPrice,0)) AS [Unit Price]

					--,(isnull(t4.OverheadRate,0)) AS [Overhead Rate]

					--,((t1.Quantity * t4.UnitPrice)/ 100) *(isnull(t4.OverheadRate,0)) AS [Overhead Value]

					--,((((t4.PieceQty * t4.UnitPrice)/ 100) *(isnull(t4.OverheadRate,0))) / t4.PieceQty) AS [Overhead Unit Value]

					--,(isnull(t4.MakingCost,0)) AS [DZ Making Cost]

					--,((isnull(t4.MakingCost,0) / 12) * t1.Quantity) AS [Total Making Cost]

					--,(isnull(t4.MakingCost,0) / 12) AS [Unit Making Cost]

					--,(SELECT (ISNULL(SUM(ts1.TotalPrice),0) / t4.PieceQty) 
					--FROM Merchandising_StyleSlave ts1 WHERE ts1.Active = 1 AND ts1.Merchandising_StyleFK =  t4.ID) [Unit BOM Cost]

					--,(SELECT (ISNULL(SUM(ts1.TotalPrice),0) / t4.PieceQty) * t1.Quantity
					--FROM Merchandising_StyleSlave ts1 WHERE ts1.Active = 1 AND ts1.Merchandising_StyleFK =  t4.ID) [Total BOM Cost]

					--,t5.Commission AS [Buyer Commission] -- for one qty

					--,(t4.PieceQty * t4.UnitPrice) * (isnull(cast(t4.PieceQty as decimal) ,0) * isnull(cast(t4.UnitPrice AS decimal),0) / 100 * isnull(t5.Commission , 0) / 100) AS [Total Buyer Commission] -- for one qty

					----,GETDATE()
					----,t4.ID
				 --  FROM Prod_ReferencePlanFollowup t1
				 --  JOIN Prod_ReferencePlan t2 ON t1.Prod_ReferencePlanFK = t2.ID
				 --  JOIN Merchandising_StyleShipmentSchedule t3  ON t1.Merchandising_StyleShipmentScheduleFK =  t3.ID
				 --  JOIN Merchandising_Style t4 ON t3.Merchandising_StyleFK =  t4.ID
				 --  JOIN Merchandising_BuyerOrder t5 ON t4.Merchandising_BuyerOrderFK = t5.ID   
				  
					--where t1.Active = 1 and t2.Active = 1 and t3.Active = 1 and 
					--t4.Active = 1 and t5.Active = 1 and t2.SectionId = 2
			COMMIT TRANSACTION 
			
			END TRY
			BEGIN CATCH
				ROLLBACK TRANSACTION
			END CATCH
			--- CM Earn END

			--- Cash In Hand
				BEGIN TRY
				BEGIN TRANSACTION
					DELETE [dbo].[Mis_CashInHand]
					INSERT INTO [dbo].[Mis_CashInHand]([Active],[Time],[Date],[Accounting_Head],[Value])
					

						SELECT 1,GETDATE(),GETDATE(),t3.Name ,(sum(isnull(t1.Debit,0)) - sum(isnull(t1.Credit,0))) as TotalBalance
						FROM Accounting_JournalSlave t1
						JOIN Accounting_Journal t2 ON t1.Accounting_JournalFK = t2.ID
						JOIN Accounting_Head t3 ON t1.Accounting_HeadFK = t3.ID
						JOIN Accounting_Chart2 t4 ON t3.Accounting_Chart2FK = t4.ID
						WHERE t4.ID = 1287 and t1.Active = 1 and t2.Active = 1 and t3.Active = 1 and t4.Active = 1
						and t2.Approved = 1 and t2.Finalized = 1
						group by t3.Name

					
					   
					
			COMMIT TRANSACTION 
			
			END TRY
			BEGIN CATCH
				ROLLBACK TRANSACTION
			END CATCH
			--- Cash In Hand

			--- Cash In Bank
				BEGIN TRY
				BEGIN TRANSACTION
					DELETE [dbo].[Mis_CashAtBank]
					INSERT INTO [dbo].[Mis_CashAtBank]([Active],[Time],[Date],[Accounting_Head],[Value])					

						SELECT 1,GETDATE(),GETDATE(),t3.Name ,(sum(isnull(t1.Debit,0)) - sum(isnull(t1.Credit,0))) as TotalBalance
						FROM Accounting_JournalSlave t1
						JOIN Accounting_Journal t2 ON t1.Accounting_JournalFK = t2.ID
						JOIN Accounting_Head t3 ON t1.Accounting_HeadFK = t3.ID
						JOIN Accounting_Chart2 t4 ON t3.Accounting_Chart2FK = t4.ID
						WHERE t4.ID = 1288 and t1.Active = 1 and t2.Active = 1 and t3.Active = 1 and t4.Active = 1
						and t2.Approved = 1 and t2.Finalized = 1
						group by t3.Name

					
					   
					
			COMMIT TRANSACTION 
			
			END TRY
			BEGIN CATCH
				ROLLBACK TRANSACTION
			END CATCH
			--- Cash In Bank

			--- Accounts Receivable
				BEGIN TRY
				BEGIN TRANSACTION
					DELETE [dbo].[Mis_AccountsReceivable]
					INSERT INTO [dbo].[Mis_AccountsReceivable]([Active],[Time],[Date],[Common_BuyerFk],[Value])					

						SELECT 1,GETDATE(),GETDATE(),Isnull(t3.BaseHeadId,0),(sum(isnull(t1.Debit,0)) - sum(isnull(t1.Credit,0))) as TotalBalance
						FROM Accounting_JournalSlave t1
						JOIN Accounting_Journal t2 ON t1.Accounting_JournalFK = t2.ID
						JOIN Accounting_Head t3 ON t1.Accounting_HeadFK = t3.ID
						JOIN Accounting_Chart2 t4 ON t3.Accounting_Chart2FK = t4.ID
						WHERE t4.ID = 1289 and t1.Active = 1 and t2.Active = 1 and t3.Active = 1 and t4.Active = 1
						and t2.Approved = 1 and t2.Finalized = 1  and t3.BaseHeadId  is not null
						group by t3.BaseHeadId,t3.Name		   
					
			COMMIT TRANSACTION 
			
			END TRY
			BEGIN CATCH
				ROLLBACK TRANSACTION
			END CATCH
			--- Accounts Receivable

			--- Accounts Payable
				BEGIN TRY
				BEGIN TRANSACTION
					DELETE [dbo].[Mis_AccountsPayable]
					INSERT INTO [dbo].[Mis_AccountsPayable]([Active],[Time],[Date],[Common_SupplierFk],[Value])					

						SELECT 1,GETDATE(),GETDATE(),t3.BaseHeadId,( sum(isnull(t1.Credit,0)) -sum(isnull(t1.Debit,0))) as TotalBalance
						FROM Accounting_JournalSlave t1
						JOIN Accounting_Journal t2 ON t1.Accounting_JournalFK = t2.ID
						JOIN Accounting_Head t3 ON t1.Accounting_HeadFK = t3.ID
						JOIN Accounting_Chart2 t4 ON t3.Accounting_Chart2FK = t4.ID
						WHERE t4.ID = 1300 and t1.Active = 1 and t2.Active = 1 and t3.Active = 1 and t4.Active = 1
						and t2.Approved = 1 and t2.Finalized = 1 and t3.BaseHeadId  is not null
						group by t3.BaseHeadId	,t3.Name 
					
			COMMIT TRANSACTION 
			
			END TRY
			BEGIN CATCH
				ROLLBACK TRANSACTION
			END CATCH
			--- Accounts Payable


			--- Accounts Payable
				BEGIN TRY
				BEGIN TRANSACTION
					DELETE [dbo].[Mis_OrderConfirm]
					INSERT INTO [dbo].[Mis_OrderConfirm]
							([Active],[Time],[OrderNo],[FinalDeliveryDate],[Common_BuyerFk],[OrderValue],[CmValue],[OrderQty])			

				SELECT					
					1	
					,GETDATE()					
					,t4.Name + '/'+t1.CID +'/'+t1.StyleName
					,GETDATE()
					,t4.ID
					,SUM(t1.PackQty * t1.PackPrice) as [Order Value]
					,SUM(t1.PackQty * t1.PackPrice) -(((isnull(t1.MakingCost,0) / 12) * t1.PieceQty)-- AS [Total Making Cost]
					+((SUM(t1.PieceQty) * t1.UnitPrice)/ 100) *(isnull(t1.OverheadRate,0))-- AS [Overhead Value]
					+(SELECT ISNULL(SUM(ts1.TotalPrice),0)
					FROM Merchandising_StyleSlave ts1 WHERE ts1.Active = 1 AND ts1.Merchandising_StyleFK =  t1.ID)-- [Total BOM Cost]
					+ (isnull(cast(t1.PieceQty as decimal) ,0) * isnull(cast(t1.UnitPrice AS decimal),0) / 100 * isnull(t2.Commission , 0))) AS [CM] -- for one qty
					,COUNT(t1.ID)
				   FROM Merchandising_Style t1				  
				   JOIN Merchandising_BuyerOrder t2 ON t1.Merchandising_BuyerOrderFK = t2.ID  
				   JOIN Common_Buyer t4 ON t2.Common_BuyerFK = t4.ID 				  
					where t1.Active = 1  and t2.Active = 1
					group by t4.Name,t4.ID, t1.CID, t1.StyleName,t1.PackQty , t1.PackPrice ,t1.ID ,t1.MakingCost,t1.PieceQty,t1.OverheadRate,t1.UnitPrice,t2.Commission

			COMMIT TRANSACTION 
			
			END TRY
			BEGIN CATCH
				ROLLBACK TRANSACTION
			END CATCH
			--- Accounts Payable
			COMMIT TRANSACTION 
		END TRY
		BEGIN CATCH
			ROLLBACK TRANSACTION
		END CATCH
	END




------------------------------------Daily Production

USE [Erp.TempM]
GO

/****** Object:  StoredProcedure [dbo].[Sp_DailyProduction]    Script Date: 9/26/2019 2:25:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
	ALTER Procedure [dbo].[Sp_DailyProduction]
	AS
	BEGIN
	BEGIN TRY
	BEGIN TRANSACTION
		DELETE [dbo].[Mis_DailyProduction]
					DECLARE @referenceDate DateTime
					--DECLARE @merchandising_StyleFK int

	Set @referenceDate = (SELECT distinct top 1 CONVERT(VARCHAR(10),pRef.ReferenceDate,126) 
						  FROM Prod_Reference pRef
						  JOIN Prod_ReferencePlan t2 ON pRef.ID = t2.Prod_ReferenceFK
						  JOIN Merchandising_Style t3 ON t2.Merchandising_StyleFK = t3.ID
						  JOIN Merchandising_BuyerOrder t4 ON t3.Merchandising_BuyerOrderFK = t4.ID
						  JOIN Common_Buyer t5 ON t4.Common_BuyerFK = t5.ID  where pRef.Active = 1 
						  order By CONVERT(VARCHAR(10),pRef.ReferenceDate,126) desc)

						  DECLARE @merchandising_StyleFK table (id int);
						INSERT @merchandising_StyleFK(id)  (SELECT distinct  x3.ID
						  FROM Prod_Reference x1
						  JOIN Prod_ReferencePlan x2 ON x1.ID = x2.Prod_ReferenceFK
						  JOIN Merchandising_Style x3 ON x2.Merchandising_StyleFK = x3.ID
						  JOIN Merchandising_BuyerOrder x4 ON x3.Merchandising_BuyerOrderFK = x4.ID
						  JOIN Common_Buyer x5 ON x4.Common_BuyerFK = x5.ID  where x1.Active = 1 );

						

Delete from [dbo].[Mis_DailyProduction]
INSERT INTO [dbo].[Mis_DailyProduction]
           ([Active],[User],[Time],[Remarks],[ProductionDate],[Common_BuyerFk],[OrderNo] ,
		   [Cutting] ,[CuttingDone] ,[Sewing] ,[SewingDone] ,[Ironing] ,[IroningDone] ,[Finishing] ,[FinishingDone]
           ,[Knitting],[KnittingDone],[Dyeing],[DyeingDone] ,[OrderQty],[SMV] ,[UserID])

    Select
           1,  'System'  ,GetDate() ,NULL,Convert(datetime, @referenceDate, 121),Common_Buyer.ID,Merchandising_BuyerOrder.BuyerPO+'/'+ Merchandising_Style.StyleName

           ,(SELECT FLOOR(IsNull(SUM(Prod_ReferencePlanFollowup.Quantity),0)/ Merchandising_Style.SetQuantity)
				FROM Prod_ReferencePlanFollowup 
				JOIN Prod_ReferencePlan  ON Prod_ReferencePlanFollowup.Prod_ReferencePlanFK = Prod_ReferencePlan.ID
				JOIN Prod_Reference  ON Prod_ReferencePlan.Prod_ReferenceFK = Prod_Reference.ID
				WHERE Prod_ReferencePlan.Merchandising_StyleFK = Merchandising_Style.ID and CONVERT(VARCHAR(10), Prod_Reference.ReferenceDate,126) = @referenceDate	and				  
				Prod_ReferencePlan.SectionId = 1) AS  [Cutting Plan]

			,(SELECT FLOOR(IsNull(SUM(Prod_ReferencePlanFollowup.Quantity),0)/ Merchandising_Style.SetQuantity)
				FROM Prod_ReferencePlanFollowup 
				JOIN Prod_ReferencePlan  ON Prod_ReferencePlanFollowup.Prod_ReferencePlanFK = Prod_ReferencePlan.ID
				JOIN Prod_Reference  ON Prod_ReferencePlan.Prod_ReferenceFK = Prod_Reference.ID
				WHERE Prod_ReferencePlan.Merchandising_StyleFK = Merchandising_Style.ID and CONVERT(VARCHAR(10), Prod_Reference.ReferenceDate,126) = @referenceDate	and							  
				Prod_ReferencePlan.SectionId = 1) AS [Cutting Done] --<CuttingDone, int,>

           ,(SELECT IsNull(SUM(Prod_ReferencePlan.PlanQuantity),0)
				FROM Prod_ReferencePlan
				join Prod_Reference on Prod_ReferencePlan.Prod_ReferenceFK = Prod_Reference.ID
				WHERE Prod_ReferencePlan.Merchandising_StyleFK = Merchandising_Style.ID and CONVERT(VARCHAR(10), Prod_Reference.ReferenceDate,126) = @referenceDate	and								  
				Prod_ReferencePlan.SectionId = 2) AS [Sewing Plan]--<Sewing, int,>

			,(SELECT FLOOR(IsNull(SUM(Prod_ReferencePlanFollowup.Quantity),0)/ Merchandising_Style.SetQuantity)
				FROM Prod_ReferencePlanFollowup 
				JOIN Prod_ReferencePlan  ON Prod_ReferencePlanFollowup.Prod_ReferencePlanFK = Prod_ReferencePlan.ID
				JOIN Prod_Reference  ON Prod_ReferencePlan.Prod_ReferenceFK = Prod_Reference.ID
				WHERE Prod_ReferencePlan.Merchandising_StyleFK = Merchandising_Style.ID and CONVERT(VARCHAR(10), Prod_Reference.ReferenceDate,126) = @referenceDate	and							  
				Prod_ReferencePlan.SectionId = 2) AS [Sewing Done]--<SewingDone, int,>

				,((SELECT FLOOR(IsNull(SUM(Prod_ReferencePlanFollowup.Quantity),0)/ Merchandising_Style.SetQuantity)
				FROM Prod_ReferencePlanFollowup 
				JOIN Prod_ReferencePlan  ON Prod_ReferencePlanFollowup.Prod_ReferencePlanFK = Prod_ReferencePlan.ID
				JOIN Prod_Reference  ON Prod_ReferencePlan.Prod_ReferenceFK = Prod_Reference.ID
				WHERE Prod_ReferencePlan.Merchandising_StyleFK = Merchandising_Style.ID and CONVERT(VARCHAR(10), Prod_Reference.ReferenceDate,126) = @referenceDate	and							  
				Prod_ReferencePlan.SectionId = 2)
			-
			(SELECT FLOOR(IsNull(SUM(Prod_ReferencePlanFollowup.Quantity),0)/ Merchandising_Style.SetQuantity)
			    FROM Prod_ReferencePlanFollowup 
			    JOIN Prod_ReferencePlan  ON Prod_ReferencePlanFollowup.Prod_ReferencePlanFK = Prod_ReferencePlan.ID
				JOIN Prod_Reference  ON Prod_ReferencePlan.Prod_ReferenceFK = Prod_Reference.ID
			    WHERE Prod_ReferencePlan.Merchandising_StyleFK = Merchandising_Style.ID and CONVERT(VARCHAR(10), Prod_Reference.ReferenceDate,126) = @referenceDate	and						  
			    Prod_ReferencePlan.SectionId = 3)) AS [Iron Plan] -- Ironing

			,(SELECT FLOOR(IsNull(SUM(Prod_ReferencePlanFollowup.Quantity),0)/ Merchandising_Style.SetQuantity)
				FROM Prod_ReferencePlanFollowup 
				JOIN Prod_ReferencePlan  ON Prod_ReferencePlanFollowup.Prod_ReferencePlanFK = Prod_ReferencePlan.ID
				JOIN Prod_Reference  ON Prod_ReferencePlan.Prod_ReferenceFK = Prod_Reference.ID
				WHERE Prod_ReferencePlan.Merchandising_StyleFK = Merchandising_Style.ID and CONVERT(VARCHAR(10), Prod_Reference.ReferenceDate,126) = @referenceDate	and						  
				Prod_ReferencePlan.SectionId = 3) AS [Iron Done] -- Ironin gDone
	

			,((SELECT FLOOR(IsNull(SUM(Prod_ReferencePlanFollowup.Quantity),0)/ Merchandising_Style.SetQuantity)
				FROM Prod_ReferencePlanFollowup 
				JOIN Prod_ReferencePlan  ON Prod_ReferencePlanFollowup.Prod_ReferencePlanFK = Prod_ReferencePlan.ID
				JOIN Prod_Reference  ON Prod_ReferencePlan.Prod_ReferenceFK = Prod_Reference.ID
				WHERE Prod_ReferencePlan.Merchandising_StyleFK = Merchandising_Style.ID and CONVERT(VARCHAR(10), Prod_Reference.ReferenceDate,126) = @referenceDate	and						  
				Prod_ReferencePlan.SectionId = 3)
			-
			(SELECT FLOOR(IsNull(SUM(Prod_ReferencePlanFollowup.Quantity ),0) / Merchandising_Style.SetQuantity)
				FROM Prod_ReferencePlanFollowup 
				JOIN Prod_ReferencePlan  ON Prod_ReferencePlanFollowup.Prod_ReferencePlanFK = Prod_ReferencePlan.ID
				JOIN Prod_Reference  ON Prod_ReferencePlan.Prod_ReferenceFK = Prod_Reference.ID
				WHERE Prod_ReferencePlan.Merchandising_StyleFK = Merchandising_Style.ID and CONVERT(VARCHAR(10), Prod_Reference.ReferenceDate,126) = @referenceDate	and						  
				Prod_ReferencePlan.SectionId = 4)) AS [Finishing Plan]--<Finishing, int,>

			,(SELECT FLOOR(IsNull(SUM(Prod_ReferencePlanFollowup.Quantity ),0) / Merchandising_Style.SetQuantity)
				FROM Prod_ReferencePlanFollowup 
				JOIN Prod_ReferencePlan  ON Prod_ReferencePlanFollowup.Prod_ReferencePlanFK = Prod_ReferencePlan.ID
				JOIN Prod_Reference  ON Prod_ReferencePlan.Prod_ReferenceFK = Prod_Reference.ID
				WHERE Prod_ReferencePlan.Merchandising_StyleFK = Merchandising_Style.ID and CONVERT(VARCHAR(10), Prod_Reference.ReferenceDate,126) = @referenceDate	and							  
				Prod_ReferencePlan.SectionId = 4) AS [Finishing Done]--<FinishingDone, int,>

			,0 AS [Knitting]--<Knitting, int,>          
            ,0 AS [Knitting Done]--<KnittingDone, int,>
		    ,0 AS [Dyeing]--<Dyeing, int,>
            ,0 AS [Dyeing Done]--<DyeingDone, int,>
            ,Merchandising_Style.PieceQty AS [Order Qty]--<OrderQty, int,>
            ,IsNull((Select top 1 Prod_ReferencePlan.GSMORSMV from Prod_ReferencePlan where Prod_ReferencePlan.Active = 1 and Prod_ReferencePlan.Merchandising_StyleFK = Merchandising_Style.ID and Prod_ReferencePlan.SectionId = 2),0)--<SMV, decimal(18,2),>
            ,1--<UserID, int,>

			
from Merchandising_Style 
join Merchandising_BuyerOrder on Merchandising_Style.Merchandising_BuyerOrderFK = Merchandising_BuyerOrder.ID
join Common_Buyer on Merchandising_BuyerOrder.Common_BuyerFK = Common_Buyer.ID
join @merchandising_StyleFK sp on Merchandising_Style.ID = sp.id
where Merchandising_Style.Active = 1 and Merchandising_BuyerOrder.Active = 1





	COMMIT TRANSACTION
	INSERT INTO [dbo].[Mis_Log]([Active],[Time],[UserID],[TableName],[Status])
	SELECT 1,GETDATE(),0,'Daily Production','Success'
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		
	INSERT INTO [dbo].[Mis_Log]([Active],[Time],[UserID],[TableName],[Status])
	SELECT 1,GETDATE(),0,'Daily Production','Failure'
	END CATCH
	END




	
		