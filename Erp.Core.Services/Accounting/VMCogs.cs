﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Erp.Core.Services.Accounting
{
   public class VMCogs:BaseVM
    {
        [Required(ErrorMessage = "Account Type is Required.")]
        public int Accounting_TypeFK { get; set; }
        public string Accounting_TypeName { get; set; }

        [Required(ErrorMessage = "Account Chart1 is Required.")]
        public int Accounting_Chart1FK { get; set; }
        public string Accounting_Chart1Name { get; set; }

        [Required(ErrorMessage = "Account Chart2 is Required.")]
        public int Accounting_Chart2FK { get; set; }
        public string Accounting_Chart2Name { get; set; }

        [Required(ErrorMessage = "Accounting Head is Required.")]
        public int Accounting_HeadId { get; set; }

        public string AccountHeadDetails { get; set; }

        public int Sl { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public decimal Total { get; set; }
        public decimal SubTotal { get; set; }
        ////public DateTime Date { get; set; }
        ////public DateTime PostedDate { get; set; }
        //public string Head { get; set; }
        //public string Description { get; set; }
        public decimal Debit { get; set; }
        public decimal Credit { get; set; }
        public decimal Balance { get; set; }
        public IEnumerable<VMCogs> DataList { get; set; }
        public decimal OpeningBalance { get; internal set; }
        public decimal ClosingBalance { get; internal set; }
        public string Accounting_HeadName { get; internal set; }
        public string Accounting_CostCenterName { get; internal set; }
        public bool DrIncrease { get; internal set; }
        public int EntryCount { get; set; }


        public decimal RawMaterialOpeningStock { get;  set; }
        public decimal RawMaterialPurchase { get;  set; }
        public decimal RawMaterialClosing { get; set; }
        public decimal RawMaterialConsumed { get; set; }
        public decimal FactoryOverhead { get;  set; }
        public decimal CostofGoodsManufactured { get; set; }
        public decimal RawMaterialClosingStock { get; set; }
        public decimal OpeningStockWorkinProcess { get; set; }
        public decimal OpeningStockofFinishGoods { get; set; }
        public decimal CostofGoodsAvaiableforSales { get;set; }
        public decimal ClosingStockWorkinProcess { get; set; }
        public decimal ClosingStockofFinishGoods { get; set; }
        public decimal CostofGoodsSold { get; set; }
    }
}
