﻿using Erp.Core.Entity.Accounting;
using System.Linq;
using System.Threading.Tasks;
using System;
using System.Collections.Generic;
using Erp.Core.Services.Integration;
using Microsoft.AspNetCore.Http;

namespace Erp.Core.Services.Accounting
{
    public class AccountingPartialService : BaseService
    {
        private IntegrationService _integrationService;
        public AccountingPartialService(IErpDbContext db, HttpContext httpContext)
        {
            _db = db;
            _integrationService = new IntegrationService(db, httpContext);
            
        }

        #region Accounting Partial Services
        public async Task<VMAccountingTransaction> AccountingTransactionGetforChairmanview( DateTime date)
        {
            VMAccountingTransaction vMAccountingTransaction = new VMAccountingTransaction();
            vMAccountingTransaction.DataList = await Task.Run(() => (from t1 in _db.Accounting_Transaction
                                                                     join t2 in _db.Accounting_CostCenter on t1.Accounting_CostCenterFK equals t2.ID
                                                                     join t3 in _db.Accounting_Head on t1.From_Acc_NameFK equals t3.ID
                                                                     join t4 in _db.Accounting_Head on t1.To_Acc_NameFK equals t4.ID
                                                                     where t1.Active == true  && t1.Date.Date == date
                                                                     select new VMAccountingTransaction
                                                                     {
                                                                         ID = t1.ID,
                                                                         CID = t1.CID,
                                                                         Date = t1.Date,
                                                                         ChequeDate = t1.ChequeDate,
                                                                         TransactionType = t1.TransactionType,
                                                                         ChequeNo = t1.ChequeNo,
                                                                         Amount = t1.Amount,
                                                                         Description = t1.Description,
                                                                         From_Acc_NameFK = t1.From_Acc_NameFK,
                                                                         From_Acc_Name = t3.Name,
                                                                         To_Acc_NameFK = t1.To_Acc_NameFK,
                                                                         To_Acc_Name = t4.Name,
                                                                         Accounting_CostCenterFK = t1.Accounting_CostCenterFK,
                                                                         Accounting_CostCenterName = t2.Title,
                                                                         Status = t1.Status
                                                                     }).OrderByDescending(x => x.ID).ToList());
            return vMAccountingTransaction;
        }



        public async Task<VMAccountingTransaction> AccountingTransactionGet()
        {
            VMAccountingTransaction vMAccountingTransaction = new VMAccountingTransaction();
            vMAccountingTransaction.DataList = await Task.Run(() => (from t1 in _db.Accounting_Transaction
                                          join t2 in _db.Accounting_CostCenter on t1.Accounting_CostCenterFK equals t2.ID
                                          join t3 in _db.Accounting_Head on t1.From_Acc_NameFK equals t3.ID
                                          join t4 in _db.Accounting_Head on t1.To_Acc_NameFK equals t4.ID
                                          where t1.Active == true
                                          select new VMAccountingTransaction
                                          {
                                              ID = t1.ID,
                                              CID = t1.CID,
                                              Date = t1.Date,
                                              ChequeDate = t1.ChequeDate,
                                              TransactionType = t1.TransactionType,
                                              ChequeNo = t1.ChequeNo,
                                              Amount = t1.Amount,
                                              Description = t1.Description,
                                              From_Acc_NameFK = t1.From_Acc_NameFK,
                                              From_Acc_Name = t3.Name,
                                              To_Acc_NameFK = t1.To_Acc_NameFK,
                                              To_Acc_Name = t4.Name,
                                              Accounting_CostCenterFK = t1.Accounting_CostCenterFK,
                                              Accounting_CostCenterName = t2.Title,
                                              Status = t1.Status
                                          }).OrderByDescending(x => x.ID).ToList());
            return vMAccountingTransaction;
        }

        public async Task<VMAccountingTransaction> AccountingTransactionGetSpecific(int Id)
        {
            VMAccountingTransaction vMAccountingTransaction = new VMAccountingTransaction();
            vMAccountingTransaction = await Task.Run(() => (from t1 in _db.Accounting_Transaction
                                          join t2 in _db.Accounting_CostCenter on t1.Accounting_CostCenterFK equals t2.ID
                                          join t3 in _db.Accounting_Head on t1.From_Acc_NameFK equals t3.ID
                                          join t4 in _db.Accounting_Head on t1.To_Acc_NameFK equals t4.ID
                                          where t1.Active == true && t1.ID==Id
                                          select new VMAccountingTransaction
                                          {
                                              ID = t1.ID,
                                              CID = t1.CID,
                                              Date = t1.Date,
                                              ChequeDate = t1.ChequeDate,
                                              TransactionType = t1.TransactionType,
                                              ChequeNo = t1.ChequeNo,
                                              Amount = t1.Amount,
                                              Description = t1.Description,
                                              From_Acc_NameFK = t1.From_Acc_NameFK,
                                              From_Acc_Name = t3.Name,
                                              To_Acc_NameFK = t1.To_Acc_NameFK,
                                              To_Acc_Name = t4.Name,
                                              Accounting_CostCenterFK = t1.Accounting_CostCenterFK,
                                              Accounting_CostCenterName = t2.Title,
                                              Status = t1.Status
                                          }).FirstOrDefault());
            return vMAccountingTransaction;
        }


        public  VMAccountingTransaction AccountingTransactionWithBalance()
        {
            var frmdt = "2000-01-01";
            var fromDate = Convert.ToDateTime(frmdt);
            int[] l = { 1,3 };
            var JournalData = _integrationService.GetJournalHistory();
            VMAccountingTransaction vMAccountingTransaction = new VMAccountingTransaction();
            var vData =  (from t1 in _db.Accounting_Transaction
                        join t2 in _db.Accounting_CostCenter on t1.Accounting_CostCenterFK equals t2.ID
                        join t3 in _db.Accounting_Head on t1.From_Acc_NameFK equals t3.ID
                        join t4 in _db.Accounting_Head on t1.To_Acc_NameFK equals t4.ID
                        where t1.Active == true && l.Contains(t1.Status)
                        select new VMAccountingTransaction
                        {
                            ID = t1.ID,
                            CID = t1.CID,
                            Date = t1.Date,
                            ChequeDate = t1.ChequeDate,
                            TransactionType = t1.TransactionType,
                            ChequeNo = t1.ChequeNo,
                            Amount = t1.Amount,
                            FromAccountBalance =0,//_integrationService.getHeadWiseTrialBalance(t1.From_Acc_NameFK, fromDate, t1.Date),
                            ToAccountBalance =0, //_integrationService.getHeadWiseTrialBalance(t1.To_Acc_NameFK, fromDate, t1.Date),
                            Description = t1.Description,
                            From_Acc_NameFK = t1.From_Acc_NameFK,
                            From_Acc_Name = t3.Name,
                            To_Acc_NameFK = t1.To_Acc_NameFK,
                            To_Acc_Name = t4.Name,
                            Accounting_CostCenterFK = t1.Accounting_CostCenterFK,
                            Accounting_CostCenterName = t2.Title,
                            Status = t1.Status
                        }).OrderByDescending(x => x.ID).ToList();

            var ActualList = (from t1 in vData
                              // join t2 in JournalData
                             // on t1.To_Acc_NameFK equals t2.Accounting_HeadId
                              select new VMAccountingTransaction
                               {
                                   ID = t1.ID,
                                   CID = t1.CID,
                                   Date = t1.Date,
                                   ChequeDate = t1.ChequeDate,
                                   TransactionType = t1.TransactionType,
                                   ChequeNo = t1.ChequeNo,
                                   Amount = t1.Amount,
                                   FromAccountBalance = (from t2 in JournalData where t2.Accounting_HeadId== t1.From_Acc_NameFK
                                                         select new {
                                                             balance = t2.Credit-t2.Debit
                                                         }).Sum(x=>x.balance),
                                   ToAccountBalance = (from t2 in JournalData
                                                       where t2.Accounting_HeadId == t1.To_Acc_NameFK
                                                       select new
                                                       {
                                                           balance = t2.Credit - t2.Debit
                                                       }).Sum(x => x.balance),
                                  Description = t1.Description,
                                   From_Acc_NameFK = t1.From_Acc_NameFK,
                                   From_Acc_Name = t1.From_Acc_Name,
                                   To_Acc_NameFK = t1.To_Acc_NameFK,
                                   To_Acc_Name = t1.To_Acc_Name,
                                   Accounting_CostCenterFK = t1.Accounting_CostCenterFK,
                                   Accounting_CostCenterName = t1.Accounting_CostCenterName,
                                   Status = t1.Status
                               }).OrderByDescending(x => x.ID).ToList();

              vMAccountingTransaction.DataList = ActualList;
            return vMAccountingTransaction;
        }

        public string VaoucherNoGet()
        {
            int count = _db.Accounting_Transaction.Count();
            count++;
            string code = count.ToString().PadLeft(2, '0');
            return code;
        }


        public async Task<int> AccountingTransactionAdd(VMAccountingTransaction vMAccountingTransaction)
        {
            int result = -1;
            string code = VaoucherNoGet();
            if (_db.Accounting_Transaction.Where(x => x.Active == true).Any(o => o.ChequeNo.Trim().ToLower() == vMAccountingTransaction.ChequeNo.Trim().ToLower()))
            {
                result = -1;
            }
            else
            {
                Accounting_Transaction accounting_Transaction = new Accounting_Transaction
                {
                    CID = code,
                    TransactionType = vMAccountingTransaction.TransactionType,
                    Date = DateTime.Now,
                    ChequeNo = vMAccountingTransaction.ChequeNo,
                    ChequeDate = vMAccountingTransaction.ChequeDate,
                    From_Acc_NameFK = vMAccountingTransaction.From_Acc_NameFK,
                    To_Acc_NameFK = vMAccountingTransaction.To_Acc_NameFK,
                    Accounting_CostCenterFK = vMAccountingTransaction.Accounting_CostCenterFK,
                    Amount = vMAccountingTransaction.Amount,
                    Description = vMAccountingTransaction.Description,
                    Status =1,
                    Active = true,
                    Editable = true,
                    User = vMAccountingTransaction.User,
                    Remarks = "N/A"
                };
                _db.Accounting_Transaction.Add(accounting_Transaction);
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = accounting_Transaction.ID;
                }
            }
            return result;
        }


        public async Task<int> AccountingTransactionEdit(VMAccountingTransaction vMAccountingTransaction)
        {
            int result = -1;
            Accounting_Transaction accounting_Transaction = _db.Accounting_Transaction.Find(vMAccountingTransaction.ID);
            // accounting_Transaction.Date = vMAccountingTransaction.Date;
            accounting_Transaction.ChequeDate = vMAccountingTransaction.ChequeDate;
            accounting_Transaction.ChequeNo = vMAccountingTransaction.ChequeNo;
            accounting_Transaction.From_Acc_NameFK = vMAccountingTransaction.From_Acc_NameFK;
            accounting_Transaction.To_Acc_NameFK = vMAccountingTransaction.To_Acc_NameFK;
            accounting_Transaction.Accounting_CostCenterFK = vMAccountingTransaction.Accounting_CostCenterFK;
            accounting_Transaction.Amount = vMAccountingTransaction.Amount;
            accounting_Transaction.Description = vMAccountingTransaction.Description;
            if (await _db.SaveChangesAsync() > 0)
            {
                result = accounting_Transaction.ID;
                
            }
            return result;
        }

        public async Task<int> AccountingTransactionApproval(int id, int type)
        {
            int result = -1;
            Accounting_Transaction accounting_Transaction = _db.Accounting_Transaction.Find(id);
            if(type == 1)
            {
                //Approved
                accounting_Transaction.Status = 2;
                await _integrationService.AccountingJournalPush(accounting_Transaction.To_Acc_NameFK, accounting_Transaction.From_Acc_NameFK, accounting_Transaction.Amount, 0, 0, accounting_Transaction.Description, accounting_Transaction.Accounting_CostCenterFK);
               // await _integrationService.AccountingJournalPush(accounting_Transaction.To_Acc_NameFK, accounting_Transaction.From_Acc_NameFK, accounting_Transaction.Amount, accounting_Transaction.Description, accounting_Transaction.Accounting_CostCenterFK);
            }
            else if(type ==2)
            {
                //Hold
                accounting_Transaction.Status = 3;
            }
            else if (type == 3)
            {
                //Hold
                accounting_Transaction.Status = 4;
            }
            if (await _db.SaveChangesAsync() > 0)
            {
                result = accounting_Transaction.ID;

            }
            return result;
        }
        public async Task<int> AccountTransactionDelete(int id)
        {
            int result = -1;
            Accounting_Transaction accounting_Transaction = _db.Accounting_Transaction.Find(id);
            accounting_Transaction.Active = false;
            if (await _db.SaveChangesAsync() > 0)
            {
                result = accounting_Transaction.ID;
            }
            return result;
        }

        //public async Task<int> AccountTransactionforChairmanView(int id)
        //{
        //    int result = -1;
        //    Accounting_Transaction accounting_Transaction = _db.Accounting_Transaction.Find(id);
        //    accounting_Transaction.Approved = true;
        //    accounting_Transaction.Finalized = true;
        //    if (await _db.SaveChangesAsync() > 0)
        //    {
        //        result = accounting_Transaction.ID;
        //    }
        //    return result;
        //}

        // Accounting CostCenter List from  Accounting_CostCenter Table
        public List<object> GetCreditAccountDropDownList()
        {
            List<object> CostCenterList = new List<object>();
            int[] l = { 1288, 1298, 1299, 1305 };
            var v = (from t1 in _db.Accounting_Head
                     join t2 in _db.Accounting_Chart2 on t1.Accounting_Chart2FK equals t2.ID
                     join t3 in _db.Accounting_Chart1 on t2.Accounting_Chart1FK equals t3.ID
                     join t4 in _db.Accounting_Type on t3.Accounting_TypeFK equals t4.ID
                     where t1.Active == true && l.Contains(t2.ID)
                     select new
                     {
                         Text = t1.Name,
                         Value = t1.ID
                     }).ToList();

            foreach (var t in v)
            {
                CostCenterList.Add(new { Text =t.Text, Value = t.Value });
            }
            return CostCenterList;
        }


        public List<object> GetDebitAccountDropDownList()
        {
            List<object> CostCenterList = new List<object>();
            int[] l = { 1288, 1298, 1299, 1305 }; //&& l.Contains(t2.ID)
            var v = (from t1 in _db.Accounting_Head
                     join t2 in _db.Accounting_Chart2 on t1.Accounting_Chart2FK equals t2.ID
                     join t3 in _db.Accounting_Chart1 on t2.Accounting_Chart1FK equals t3.ID
                     join t4 in _db.Accounting_Type on t3.Accounting_TypeFK equals t4.ID
                     where t1.Active == true 
                     select new
                     {
                         Text = t1.Name,
                         Value = t1.ID
                     }).ToList();

            foreach (var t in v)
            {
                CostCenterList.Add(new { Text = t.Text, Value = t.Value });
            }
            return CostCenterList;
        }

        #endregion




    }
}
