﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Erp.Core.Services.Accounting
{
   public class VMLedger:BaseVM
    {

        [Required(ErrorMessage = "Account Type is Required.")]
        public int Accounting_TypeFK { get; set; }
        public string Accounting_TypeName { get; set; }

        [Required(ErrorMessage = "Account Chart1 is Required.")]
        public int Accounting_Chart1FK { get; set; }
        public string Accounting_Chart1Name { get; set; }

        [Required(ErrorMessage = "Account Chart2 is Required.")]
        public int Accounting_Chart2FK { get; set; }
        public string Accounting_Chart2Name { get; set; }

        [Required(ErrorMessage = "Accounting Head is Required.")]
        public int Accounting_HeadId { get; set; }
        // public string Accounting_Chart2Name { get; set; }


        public  int  Sl { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public DateTime Date { get; set; }
        public DateTime PostedDate { get; set; }
        public string Head { get; set; }
        public string Description { get; set; }
        public decimal Debit { get; set; }
        public decimal Credit { get; set; }
        public decimal Balance { get; set; }
        public IEnumerable<VMLedger> DataList { get; set; }
        public decimal OpeningBalance { get; set; }
        public decimal ClosingBalance { get;  set; }
        public decimal ClosingBalanceCr { get; set; }
        public decimal ClosingBalanceDr { get; set; }
        public string Accounting_HeadName { get; set; }
        public string Accounting_CostCenterName { get; set; }
        public bool DrIncrease { get; set; }
        public decimal OpeningBalanceCr { get; set; }
        public decimal OpeningBalanceDr { get; set; }
    }
}
