﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Erp.Core.Services.Accounting
{
    public  class VMChart : BaseVM
    {
        public string Name { get; set; }
        //public string Code { get; set; }
        public string Remark { get; set; }
       
    }
    public class VMChart1 : VMChart
    {
        public string Accounting_TypeName { get; set; }
        [Required(ErrorMessage = "Account Type is Required.")]
        public int Accounting_TypeFK { get; set; }
        public IEnumerable<VMChart1> DataList { get; set; }
        public bool Entryable { get; set; } = true;
        public bool Addable { get; set; } = true;

    }
    public class VMChart2 : VMChart
    {
        public string Accounting_TypeName { get; set; }
        [Required(ErrorMessage = "Account Type is Required.")]
        public int Accounting_TypeFK { get; set; }
        public string Accounting_Chart1Name { get; set; }
        [Required(ErrorMessage = "Account Chart is Required.")]
        public int Accounting_Chart1FK { get; set; }
        public IEnumerable<VMChart2> DataList { get; set; }
        public bool Entryable { get; set; } = true;
        public bool Addable { get; set; } = true;
    }
    public class VMAccountHead : VMChart
    {
        [Required(ErrorMessage = "Account Type is Required.")]
        public int Accounting_TypeFK { get; set; }
        public string Accounting_TypeName { get; set; }
     
        [Required(ErrorMessage = "Account Chart1 is Required.")]
        public int Accounting_Chart1FK { get; set; }
        public string Accounting_Chart1Name { get; set; }

        [Required(ErrorMessage = "Account Chart2 is Required.")]
        public int Accounting_Chart2FK { get; set; }
        public string Accounting_Chart2Name { get; set; }

        public bool Editable { get; set; } = true;
        public bool Journalable { get; set; } = true;

        public int? BaseHeadId { get; set; }
        public int? HeadType { get; set; }

        public IEnumerable<VMAccountHead> DataList { get; set; }
    }


}
