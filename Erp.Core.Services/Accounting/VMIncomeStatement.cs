﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Erp.Core.Services.Accounting
{
     public class VMIncomeStatement  :BaseVM
    {

        [Required(ErrorMessage = "Account Type is Required.")]
        public int Accounting_TypeFK { get; set; }
        public string Accounting_TypeName { get; set; }

        [Required(ErrorMessage = "Account Chart1 is Required.")]
        public int Accounting_Chart1FK { get; set; }
        public string Accounting_Chart1Name { get; set; }

        [Required(ErrorMessage = "Account Chart2 is Required.")]
        public int Accounting_Chart2FK { get; set; }
        public string Accounting_Chart2Name { get; set; }

        [Required(ErrorMessage = "Accounting Head is Required.")]
        public int Accounting_HeadId { get; set; }

        public string AccountHeadDetails { get; set; }

        public int Sl { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public decimal Total { get; set; }
        public decimal SubTotal { get; set; }
        public decimal Debit { get; set; }
        public decimal Credit { get; set; }
        public decimal Balance { get; set; }
        public IEnumerable<VMIncomeStatement> DataList { get; set; }
        public decimal OpeningBalance { get; internal set; }
        public decimal ClosingBalance { get; internal set; }
        public string Accounting_HeadName { get; internal set; }
        public string Accounting_CostCenterName { get; internal set; }
        public bool DrIncrease { get; internal set; }
        public int EntryCount { get; set; }


        public decimal ExportSales { get; set; }
        public decimal SubContractIncome { get; set; }
        public decimal TotalRevenueIncome { get; set; }
        public decimal CostofGoodsSold { get; set; }

        public decimal GrossProfit { get; set; }
        public decimal OperatingExpense { get; set; }
        public decimal AdministrativeExpense { get; set; }
        public decimal SellingAndDistribuionExpense { get; set; }
        public decimal OperatingProfit { get; set; }
        public decimal FinancialExpense { get; set; }
        public decimal FactoryOverHead { get; set; }
        public decimal TotalExpense { get; set; }

        public decimal NetProfit { get; set; }


        public decimal ProfitBeforeTax { get; set; }
        public decimal IncomeTaxExpense { get; set; }
        public decimal ProfitfortheYearafterTax { get; set; }
        public decimal OtherComprehensiveIncome { get; set; }
        public decimal TotalComprehensiveIncome { get; set; }
        public decimal TotalOperatingExpense { get; set; }
        public decimal AccumulatedDepreciation { get; set; }
    }
}
