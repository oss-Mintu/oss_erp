﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Erp.Core.Services.Accounting
{
    public class VMCashFlow:BaseVM
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public IEnumerable<VMCashFlow> DataList { get; set; }


        public decimal CashflowFromOperatingActivities { get; set; }
        public decimal NetProfitBeforeTax { get; set; }
        public decimal AdjustmentforNonCashItemtoDetermineCashflowfromOperatingActivities { get; set; }
        public decimal Depreciation { get; set; }
        public decimal ChangesinWorkingCapital { get; set; }
        public decimal AccountsReceivable { get; set; }
        public decimal Inventory { get; set; }
        public decimal AdvanceDepositandPrePayment { get; set; }
        public decimal OthersCurrentAssets { get; set; }
        public decimal AccountsPayable { get; set; }
        public decimal ShortTermLoan { get; set; }
        public decimal AcceptedBillLiabilities { get; set; }
        public decimal ProvisionforExpenses { get; set; }
        public decimal TotalChangesInWorkingCapital { get; set; }
        public decimal IncomeTaxPaid { get; set; }
        public decimal NetCashFromOperatingActivities { get; set; }
        public decimal CashFlowfromInvestingActivities { get; set; }
        public decimal AcquisitionofPropertyPlantandEquipment { get; set; }
        public decimal NetCashUsedinInvestingActivities { get; set; }
        public decimal CashFlowsfromFinancingActivities { get; set; }
        public decimal LongTermLoan { get; set; }
        public decimal NetCashFromFinancingActivities { get; set; }
        public decimal NetChangesinCashandCashEquivalents { get; set; }
        public decimal OpeningCashandCashEquivalents { get; set; }
        public decimal ClosingCashandCashEquivalents { get; set; }
        public decimal ProfitfortheYearafterTax { get; set; }
        public decimal ShortTermLiabilities { get; set; }
    }
}
