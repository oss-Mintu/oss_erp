﻿using Erp.Core.Entity.Accounting;
using Erp.Core.Services.Home;
using Erp.Core.Services.Shipment;
using System;
using System.Collections.Generic;
///using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Threading.Tasks;

namespace Erp.Core.Services.Accounting
{
    public class AccountingService : BaseService
    {

        public AccountingService(IErpDbContext db)
        {
            _db = db;
            //  CreateTheJournalHistory();
        }

        public async Task<string> Do()
        {
            Accounting_Type xxx = await Task.Run(() => _db.Accounting_Type.FirstOrDefault());
            xxx.Name = " xc new name";
            await _db.SaveChangesAsync();
            return xxx.Name;


        }
        public async Task<IEnumerable<VMChart>> GetAccountTypes()
        {
            return await Task.Run(() => _db.Accounting_Type.ToList().ConvertAll(x => new VMChart { ID = x.ID, Name = x.Name }));
        }

        public List<object> CurrencyDropDownList()
        {
            var List = new List<object>();
            _db.Common_Currency
        .Where(x => x.Active).Select(x => x).ToList()
        .ForEach(x => List.Add(new
        {
            Value = x.ConversionRateToBDT,
            Text = x.Name + " (" + x.ConversionRateToBDT + " )"
        }));

            return List;
        }

        #region Accounting Chart One Portion Services
        public async Task<VMChart1> AccountChart1Get()
        {
            VMChart1 vMChart1 = new VMChart1
            {
                DataList = await Task.Run(() => _db.Accounting_Chart1.Where(x => x.Active == true).OrderByDescending(x => x.ID).ToList().ConvertAll(
                    x => new VMChart1 { ID = x.ID, Name = x.Name, Code = x.Code, Remark = x.Remarks, Accounting_TypeName = _db.Accounting_Type.Where(y => y.ID == x.Accounting_TypeFK).Select(y => y.Name).FirstOrDefault(), Accounting_TypeFK = x.Accounting_TypeFK }))
            };
            return vMChart1;
        }


        public async Task<int> AccountChart1Add(VMChart1 vMChart1)
        {
            int result = -1;
            int count = _db.Accounting_Chart1.Count();

            count++;
            string code = count.ToString().PadLeft(2, '0');
            if (_db.Accounting_Chart1.Where(x => x.Active == true).Any(o => o.Name.Trim().ToLower() == vMChart1.Name.Trim().ToLower()))
            {
                result = -1;
            }
            else
            {
                Accounting_Chart1 chartOne = new Accounting_Chart1
                {
                    Accounting_TypeFK = vMChart1.Accounting_TypeFK,
                    Name = vMChart1.Name,
                    Code = code,
                    Remarks = vMChart1.Remark,
                    User = vMChart1.User
                };
                _db.Accounting_Chart1.Add(chartOne);
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = chartOne.ID;
                }
            }
            return result;
        }
        public async Task<int> AccountChart1Edit(VMChart1 vMChart1)
        {
            int result = -1;
            //to select Accountining_Chart_One data.....
            if (_db.Accounting_Chart1.Where(x => x.Active == true).Any(o => o.Name.Trim().ToLower() == vMChart1.Name.Trim().ToLower() && o.Remarks == vMChart1.Remark))
            {
                result = -1;
            }
            else
            {
                Accounting_Chart1 accountingchartone = _db.Accounting_Chart1.Find(vMChart1.ID);

                accountingchartone.Name = vMChart1.Name;
                accountingchartone.Remarks = vMChart1.Remark;

                if (await _db.SaveChangesAsync() > 0)
                {
                    result = accountingchartone.ID;
                }
            }
            return result;
        }
        public async Task<int> AccountChart1Delete(int id)
        {
            int result = -1;
            Accounting_Chart1 accountingchartone = _db.Accounting_Chart1.Find(id);
            if (_db.Accounting_Chart2.Any(o => o.Accounting_Chart1FK == id))
            {
                result = -1;
                //Session["warning_div"] = "true";
                //Session["warning_msg"] = "Some item found under this category. You can not delete this!!";
            }
            else
            {
                accountingchartone.Active = false;

                if (await _db.SaveChangesAsync() > 0)
                {
                    result = accountingchartone.ID;
                }
            }
            return result;
        }
        #endregion
        #region Accounting Chart Two Portion Services

        public async Task<VMChart2> AccountChart2Get()
        {
            VMChart2 vMChart2 = new VMChart2();
            List<VMChart2> a = await Task.Run(() => (from t1 in _db.Accounting_Chart2
                                                     join t2 in _db.Accounting_Chart1 on t1.Accounting_Chart1FK equals t2.ID
                                                     join t3 in _db.Accounting_Type on t2.Accounting_TypeFK equals t3.ID
                                                     where t1.Active == true
                                                     select new VMChart2
                                                     {
                                                         ID = t1.ID,
                                                         Name = t1.Name,
                                                         Code = t1.Code,
                                                         Remark = t1.Remarks,
                                                         Accounting_Chart1Name = t2.Name,
                                                         Accounting_TypeName = t3.Name

                                                     }).OrderByDescending(x => x.ID).ToList());

            vMChart2.DataList = a;
            return vMChart2;
        }

        public async Task<int> AccountChart2Add(VMChart2 vMChart2)
        {
            int result = -1;
            int count = _db.Accounting_Chart2.Count();
            count++;
            string code = count.ToString().PadLeft(2, '0');
            if (_db.Accounting_Chart2.Where(x => x.Active == true).Any(o => o.Name.Trim().ToLower() == vMChart2.Name.Trim().ToLower()))
            {
                result = -1;
            }
            else
            {
                Accounting_Chart2 charttwo = new Accounting_Chart2
                {
                    Accounting_Chart1FK = vMChart2.Accounting_Chart1FK,
                    Name = vMChart2.Name,
                    Code = code,
                    Remarks = vMChart2.Remark,
                    User = vMChart2.User,
                };
                _db.Accounting_Chart2.Add(charttwo);
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = charttwo.ID;
                }
            }
            return result;
        }
        public async Task<int> AccountChart2Edit(VMChart2 vMChart2)
        {
            int result = -1;
            //to select Accountining_Chart_Two data.....
            if (_db.Accounting_Chart2.Where(x => x.Active == true).Any(o => o.Name.Trim().ToLower() == vMChart2.Name.Trim().ToLower() && o.Remarks == vMChart2.Remark))
            {
                result = -1;
            }
            else
            {
                Accounting_Chart2 accountingcharttwo = _db.Accounting_Chart2.Find(vMChart2.ID);
                accountingcharttwo.Name = vMChart2.Name;
                accountingcharttwo.Remarks = vMChart2.Remark;
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = accountingcharttwo.ID;
                }
            }
            return result;
        }
        public async Task<int> AccountChart2Delete(int id)
        {
            int result = -1;
            Accounting_Chart2 accountingcharttwo = _db.Accounting_Chart2.Find(id);
            if (_db.Accounting_Head.Any(o => o.Accounting_Chart2FK == id))
            {
                result = -1;
                //Session["warning_div"] = "true";
                //Session["warning_msg"] = "Some item found under this category. You can not delete this!!";
            }
            else
            {
                accountingcharttwo.Active = false;
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = accountingcharttwo.ID;
                }
            }
            return result;
        }
        #endregion
        #region Accounting Head Create,Update, Delete Portion Services


        public async Task<VMAccountHead> AccountHeadGet()
        {
            VMAccountHead vMAccountHead = new VMAccountHead
            {
                DataList = await Task.Run(() => (from t1 in _db.Accounting_Head
                                                 join t2 in _db.Accounting_Chart2 on t1.Accounting_Chart2FK equals t2.ID
                                                 join t3 in _db.Accounting_Chart1 on t2.Accounting_Chart1FK equals t3.ID
                                                 join t4 in _db.Accounting_Type on t3.Accounting_TypeFK equals t4.ID
                                                 where t1.Active == true
                                                 select new VMAccountHead
                                                 {
                                                     ID = t1.ID,
                                                     Name = t1.Name,
                                                     Code = t1.Code,
                                                     Remark = t1.Remarks,
                                                     Editable = t1.Editable,
                                                     Accounting_Chart1Name = t3.Name,
                                                     Accounting_Chart2Name = t2.Name,
                                                     Accounting_TypeName = t4.Name
                                                 }).OrderByDescending(x => x.ID).ToList())
            };


            return vMAccountHead;
        }

        public async Task<VMAccountHead> AccountHeadGetWithDetails()
        {
            VMAccountHead vMAccountHead = new VMAccountHead
            {
                DataList = await Task.Run(() => (from t1 in _db.Accounting_Head
                                                 join t2 in _db.Accounting_Chart2 on t1.Accounting_Chart2FK equals t2.ID
                                                 join t3 in _db.Accounting_Chart1 on t2.Accounting_Chart1FK equals t3.ID
                                                 join t4 in _db.Accounting_Type on t3.Accounting_TypeFK equals t4.ID
                                                 where t1.Active == true
                                                 select new VMAccountHead
                                                 {
                                                     ID = t1.ID,
                                                     Name = t1.Name,
                                                     Code = t1.Code,
                                                     Remark = t1.Remarks,
                                                     Editable = t1.Editable,
                                                     Accounting_Chart1Name = t3.Name,
                                                     Accounting_Chart1FK = t3.ID,
                                                     Accounting_Chart2Name = t2.Name,
                                                     Accounting_Chart2FK = t2.ID,
                                                     Accounting_TypeName = t4.Name,
                                                     Accounting_TypeFK = t4.ID
                                                 }).OrderByDescending(x => x.ID).ToList())
            };

            return vMAccountHead;
        }

        public int AccountHeadIdGet(VMAccountHead vMAccountHead)
        {
            int result = -1;
            var vData = _db.Accounting_Head.Where(i => i.Name.ToLower().Trim() == vMAccountHead.Name.ToLower().Trim());
            if (vData.Any())
            {
                result = vData.FirstOrDefault().ID;
            }
            return result;
        }

        public async Task<int> AccountHeadAdd(VMAccountHead vMAccountHead)
        {
            int result = -1;
            int count = _db.Accounting_Head.Count();
            count++;
            string code = count.ToString().PadLeft(2, '0');
            if (_db.Accounting_Head.Where(x => x.Active == true).Any(o => o.Name.Trim().ToLower() == vMAccountHead.Name.Trim().ToLower()))
            {
                result = _db.Accounting_Head.Where(x => x.Active == true && x.Name.Trim().ToLower() == vMAccountHead.Name.Trim().ToLower()).Select(x => x.ID).FirstOrDefault();
               // result = -1;
            }
            else
            {
                Accounting_Head accountinghead = new Accounting_Head
                {
                    Accounting_Chart2FK = vMAccountHead.Accounting_Chart2FK,
                    Name = vMAccountHead.Name,
                    Code = code,
                    Remarks = vMAccountHead.Remark,
                    User = vMAccountHead.User,
                    //Editable = true,
                    Balance = 0,
                    BaseHeadId= vMAccountHead.BaseHeadId,
                    HeadType=vMAccountHead.HeadType
                };
                _db.Accounting_Head.Add(accountinghead);
                // _db.SaveChangesAsync();

                if (await _db.SaveChangesAsync() > 0)

                {
                    result = accountinghead.ID;
                }
            }
            return result;
        }
        public async Task<int> AccountHeadEdit(VMAccountHead vMAccountHead)
        {
            int result = -1;
            //to select AccountHead data.....
            var aHeadId = _db.Accounting_Head.Where(x => x.Active == true &&  x.Name.Trim().ToLower() == vMAccountHead.Name.Trim().ToLower()).FirstOrDefault();
            if (aHeadId != null)
            {
                result = aHeadId.ID;
            }
            else
            {
                Accounting_Head accountinghead = _db.Accounting_Head.Where(a=>a.ID == vMAccountHead.ID).FirstOrDefault();
                accountinghead.Name = vMAccountHead.Name;
                if(vMAccountHead.Remark != null)
                accountinghead.Remarks = vMAccountHead.Remark;
                _db.Accounting_Head.Update(accountinghead);
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = accountinghead.ID;
                }
            }
            return result;
        }

        public async Task<int> AccountHeadAdjust(VMAccountHead vMAccountHead)
        {
            int result = -1;

            Accounting_Head accountinghead = _db.Accounting_Head.Find(vMAccountHead.ID);
            accountinghead.BaseHeadId = vMAccountHead.BaseHeadId;
            accountinghead.HeadType = vMAccountHead.HeadType;
            _db.Accounting_Head.Update(accountinghead);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = accountinghead.ID;
            }
            return result;
        }
        public async Task<int> AccountHeadDelete(int id)
        {
            int result = -1;
            Accounting_Head accountinghead = _db.Accounting_Head.Find(id);
            if (_db.Accounting_JournalSlave.Any(o => o.Accounting_HeadFK == id))
            {
                result = -1;
            }
            else
            {
                accountinghead.Active = false;
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = accountinghead.ID;
                }
            }
            return result;
        }
        #endregion

        //#region Ledger
        public VMLedger LeadgerGetByDate(VMLedger vMLedger)
        {
            bool drIncrease = GetHeadDrIncreaseOrDecreaseType(vMLedger.Accounting_HeadId, AccountTypeEnum.Head);
            // var openingBaalanceInfo = GetOpeningBlance(vMLedger.Accounting_HeadId, AccountTypeEnum.Head, vMLedger.FromDate);
            (decimal balance, decimal balanceDr, decimal balanceCr, int sl) openingBaalanceInfo = GetOpeningBlanceNew(vMLedger.Accounting_HeadId, AccountTypeEnum.Head, vMLedger.FromDate);
            vMLedger.OpeningBalance = openingBaalanceInfo.balance;
            List<VMLedger> theLedger = GetTheLedger(vMLedger.Accounting_HeadId, AccountTypeEnum.Head, vMLedger.FromDate, vMLedger.ToDate, openingBaalanceInfo.balance, openingBaalanceInfo.sl).ToList();
            //var(LedgerWithBalance, closingBalance) = GetLedgerWithBalance(theLedger, openingBaalanceInfo.balance, openingBaalanceInfo.sl, drIncrease);
           //if (theLedger.Count == 1)
           // {
           //     if(theLedger.Select(x => x.Balance).LastOrDefault() ==0)
           //     {
           //         vMLedger.ClosingBalance = vMLedger.OpeningBalance;
           //     }     
           // }
           // else
           // {
           //     vMLedger.ClosingBalance = theLedger.Select(x => x.Balance).LastOrDefault();
           // }

            vMLedger.ClosingBalanceDr = theLedger.Select(x => x.ClosingBalanceDr).LastOrDefault();
            vMLedger.ClosingBalanceCr = theLedger.Select(x => x.ClosingBalanceCr).LastOrDefault();
            //vMLedger.ClosingBalance = theLedger.Select(x => x.Balance).LastOrDefault();

            if (vMLedger.OpeningBalance > 0 || vMLedger.OpeningBalance < 0)
            {
                var a = theLedger.Select(x => x.Balance).LastOrDefault();
                vMLedger.ClosingBalance = a;
            }
            else
            {
                var a = theLedger.Select(x => x.Balance).LastOrDefault();
                vMLedger.ClosingBalance = a;
            }
            vMLedger.DataList = theLedger;
            return vMLedger;
        }

        private IEnumerable<VMLedger> GetInitialJournalHistory()
        {
            return (from t1 in _db.Accounting_Head
                    join t2 in _db.Accounting_JournalSlave
                    on t1.ID equals t2.Accounting_HeadFK
                    join t3 in _db.Accounting_Journal
                    on t2.Accounting_JournalFK equals t3.ID
                    where //t1.Active==true && 
                    t2.Active == true && t3.Active == true && t3.Approved == true && t3.Finalized == true
                    select new VMLedger
                    {
                        Accounting_HeadId = t1.ID,
                        Date = t3.Date,
                        Debit = t2.Debit,
                        Credit = t2.Credit
                    }).AsEnumerable();

        }

        private IEnumerable<VMLedger> GetInitialJournalHistoryByDate(IEnumerable<VMLedger> theJournalHistory, DateTime fromDate, DateTime toDate)
        {
            return (from t1 in theJournalHistory
                    where t1.Date >= fromDate && t1.Date <= toDate
                    group t1 by t1.Accounting_HeadId into theGroup
                    select new VMLedger
                    {
                        Accounting_HeadId = theGroup.First().Accounting_HeadId,
                        Debit = theGroup.Sum(x => x.Debit),
                        Credit = theGroup.Sum(x => x.Credit),
                    }).AsEnumerable();


        }

        public VMTrialBalance TrialBalanceGet(VMTrialBalance vMTrialBalance)
        {
            IEnumerable<VMLedger> theJournalHistory, trailBalancePre, trailBalancePost;
            IEnumerable<VMTrialBalance> tempTrailBalance;

            theJournalHistory = GetInitialJournalHistory();

            trailBalancePre = GetInitialJournalHistoryByDate(theJournalHistory, DateTime.MinValue, vMTrialBalance.FromDate.AddDays(-1)).ToList();

            trailBalancePost = GetInitialJournalHistoryByDate(theJournalHistory, vMTrialBalance.FromDate, vMTrialBalance.ToDate).ToList();
            List<VMLedger> headDetails = (from t1 in _db.Accounting_Head
                                          join t5 in _db.Accounting_Chart2
                                         on t1.Accounting_Chart2FK equals t5.ID
                                          join t6 in _db.Accounting_Chart1
                                         on t5.Accounting_Chart1FK equals t6.ID
                                          join t7 in _db.Accounting_Type
                                          on t6.Accounting_TypeFK equals t7.ID
                                          //where t1.Active ==true && t5.Active==true && t6.Active==true 
                                          select new VMLedger
                                          {
                                              Accounting_TypeFK = t7.ID,
                                              Accounting_Chart1FK = t6.ID,
                                              Accounting_Chart2FK = t5.ID,
                                              Accounting_HeadId = t1.ID,
                                              Accounting_HeadName = t1.Name,
                                              Accounting_TypeName = t7.Name,
                                              Accounting_Chart1Name = t6.Name,
                                              Accounting_Chart2Name = t5.Name,
                                              DrIncrease = t7.DrIncrease
                                          }).ToList();


            IEnumerable<VMLedger> variabale = (from t1 in headDetails
                                               join t2 in trailBalancePre
                                               on t1.Accounting_HeadId equals t2.Accounting_HeadId
                                               into newTbl
                                               from preData in newTbl.DefaultIfEmpty(new VMLedger())
                                               join t3 in trailBalancePost
                                               on t1.Accounting_HeadId equals t3.Accounting_HeadId
                                               into finalTbl
                                               from data in finalTbl.DefaultIfEmpty(new VMLedger())
                                               select new VMLedger
                                               {
                                                   Accounting_TypeFK = t1.Accounting_TypeFK,
                                                   Accounting_Chart1FK = t1.Accounting_Chart1FK,
                                                   Accounting_Chart2FK = t1.Accounting_Chart2FK,
                                                   Accounting_HeadId = t1.Accounting_HeadId,
                                                   Accounting_HeadName = t1.Accounting_HeadName,
                                                   Accounting_TypeName = t1.Accounting_TypeName,
                                                   Accounting_Chart1Name = t1.Accounting_Chart1Name,
                                                   Accounting_Chart2Name = t1.Accounting_Chart2Name,
                                                   OpeningBalanceDr = (preData.Debit) - (preData.Credit),
                                                   OpeningBalanceCr = 0,
                                                   Debit = data.Debit,
                                                   Credit = data.Credit,
                                                   ClosingBalanceDr = (preData.Debit) - (preData.Credit) + data.Debit - data.Credit,
                                                   ClosingBalanceCr = 0,
                                                   DrIncrease = t1.DrIncrease
                                               }).AsEnumerable();


            if (vMTrialBalance.SearchTypeId == 1)
            {
                tempTrailBalance = (from t1 in variabale
                                    group t1 by t1.Accounting_Chart1FK into theGroup
                                    select new VMTrialBalance
                                    {
                                        ID = theGroup.First().Accounting_Chart1FK,
                                        AccountHeadDetails = theGroup.First().Accounting_TypeName,
                                        Accounting_HeadName = theGroup.First().Accounting_Chart1Name,
                                        OpeningBalanceDr = theGroup.Sum(x => x.OpeningBalanceDr),
                                        OpeningBalanceCr = 0,//theGroup.Sum(x => x.Credit),
                                        Debit = theGroup.Sum(x => x.Debit),
                                        Credit = theGroup.Sum(x => x.Credit),
                                        ClosingBalanceDr = theGroup.Sum(x => x.ClosingBalanceDr),
                                        ClosingBalanceCr = 0,//theGroup.Sum(x => x.ClosingBalanceCr)
                                        DrIncrease = theGroup.First().DrIncrease
                                    }).AsEnumerable();

            }
            else if (vMTrialBalance.SearchTypeId == 2)
            {
                tempTrailBalance = (from t1 in variabale
                                    group t1 by t1.Accounting_Chart2FK into theGroup
                                    select new VMTrialBalance
                                    {
                                        ID = theGroup.First().Accounting_Chart2FK,
                                        AccountHeadDetails = theGroup.First().Accounting_Chart1Name,
                                        Accounting_HeadName = theGroup.First().Accounting_Chart2Name,
                                        OpeningBalanceDr = theGroup.Sum(x => x.OpeningBalanceDr),
                                        OpeningBalanceCr = 0,//theGroup.Sum(x => x.Credit),
                                        Debit = theGroup.Sum(x => x.Debit),
                                        Credit = theGroup.Sum(x => x.Credit),
                                        ClosingBalanceDr = theGroup.Sum(x => x.ClosingBalanceDr),
                                        ClosingBalanceCr = 0,//theGroup.Sum(x => x.ClosingBalanceCr)
                                        DrIncrease = theGroup.First().DrIncrease
                                    }).AsEnumerable();
            }
            else
            {
                tempTrailBalance = (from t1 in variabale

                                    group t1 by t1.Accounting_HeadId into theGroup
                                    select new VMTrialBalance
                                    {
                                        ID = theGroup.First().Accounting_HeadId,
                                        AccountHeadDetails = theGroup.First().Accounting_Chart1Name + "/" + theGroup.First().Accounting_Chart2Name,
                                        Accounting_HeadName = theGroup.First().Accounting_HeadName,
                                        OpeningBalanceDr = theGroup.Sum(x => x.OpeningBalanceDr),
                                        OpeningBalanceCr = 0,//theGroup.Sum(x => x.Credit),
                                        Debit = theGroup.Sum(x => x.Debit),
                                        Credit = theGroup.Sum(x => x.Credit),
                                        ClosingBalanceDr = theGroup.Sum(x => x.ClosingBalanceDr),
                                        ClosingBalanceCr = 0,//theGroup.Sum(x => x.ClosingBalanceCr)
                                        DrIncrease = theGroup.First().DrIncrease
                                    }).AsEnumerable();
            }
            List<VMTrialBalance> theFinalList = new List<VMTrialBalance>();
            //Not Here Bottom of all 
            foreach (VMTrialBalance entry in tempTrailBalance)
            {
                if (!entry.DrIncrease)
                {
                    entry.OpeningBalanceCr = entry.OpeningBalanceDr * -1;
                    entry.OpeningBalanceDr = 0;
                    entry.ClosingBalanceCr = entry.ClosingBalanceDr * -1;
                    entry.ClosingBalanceDr = 0;

                }

                theFinalList.Add(entry);
            }
            vMTrialBalance.DataList = theFinalList;
            return vMTrialBalance;
        }





        #region Trial Balance Final Code OLD (Not Used Right Now)

        public VMTrialBalance TrialBalanceGetByDate(VMTrialBalance vMTrialBalance)
        {
            AccountTypeEnum accountTypeEnum = new AccountTypeEnum();
            if (vMTrialBalance.SearchTypeId == 1)
            {
                accountTypeEnum = AccountTypeEnum.C1;
                vMTrialBalance.DataList = (from t1 in _db.Accounting_Chart1
                                           join t2 in _db.Accounting_Type
                                           on t1.Accounting_TypeFK equals t2.ID
                                           where t1.Active == true
                                           select new VMTrialBalance
                                           {
                                               ID = t1.ID,
                                               DrIncrease = t2.DrIncrease,
                                               AccountHeadDetails = t2.Name + " / " + t1.Name,
                                               OpeningBalanceDr = 0,
                                               OpeningBalanceCr = 0,
                                               Debit = 0,
                                               Credit = 0,
                                               ClosingBalanceDr = 0,
                                               ClosingBalanceCr = 0,
                                               EntryCount = 0
                                           }).ToList();
            }
            else if (vMTrialBalance.SearchTypeId == 2)
            {
                accountTypeEnum = AccountTypeEnum.C2;
                vMTrialBalance.DataList = (from t1 in _db.Accounting_Chart2
                                           join t2 in _db.Accounting_Chart1
                                           on t1.Accounting_Chart1FK equals t2.ID
                                           join t3 in _db.Accounting_Type
                                           on t2.Accounting_TypeFK equals t3.ID
                                           where t1.Active == true
                                           select new VMTrialBalance
                                           {
                                               ID = t1.ID,
                                               DrIncrease = t3.DrIncrease,
                                               AccountHeadDetails = t3.Name + " / " + t2.Name + " / " + t1.Name,
                                               OpeningBalanceDr = 0,
                                               OpeningBalanceCr = 0,
                                               Debit = 0,
                                               Credit = 0,
                                               ClosingBalanceDr = 0,
                                               ClosingBalanceCr = 0,
                                               EntryCount = 0
                                           }).ToList();
            }
            else if (vMTrialBalance.SearchTypeId == 3)
            {
                accountTypeEnum = AccountTypeEnum.Head;
                vMTrialBalance.DataList = (from t1 in _db.Accounting_Head
                                           join t5 in _db.Accounting_Chart2
                                           on t1.Accounting_Chart2FK equals t5.ID
                                           join t6 in _db.Accounting_Chart1
                                           on t5.Accounting_Chart1FK equals t6.ID
                                           join t7 in _db.Accounting_Type
                                           on t6.Accounting_TypeFK equals t7.ID
                                           where t1.Active == true
                                           select new VMTrialBalance
                                           {
                                               ID = t1.ID,
                                               DrIncrease = t7.DrIncrease,
                                               AccountHeadDetails = t7.Name + " / " + t6.Name + " / " + t5.Name + " / " + t1.Name,
                                               OpeningBalanceDr = 0,
                                               OpeningBalanceCr = 0,
                                               Debit = 0,
                                               Credit = 0,
                                               ClosingBalanceDr = 0,
                                               ClosingBalanceCr = 0,
                                               EntryCount = 0
                                           }).ToList();
            }

            foreach (VMTrialBalance head in vMTrialBalance.DataList)
            {
                //var (openingBalance, dr, cr, closingBalance, entryCount) = GetTrialBalanceInfo(head.ID, accountTypeEnum, vMTrialBalance.FromDate, vMTrialBalance.ToDate);
                (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal OpeningBalanceDr, decimal OpeningBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr) = GetTrialBalanceInfoforTrialBalance(head.ID, accountTypeEnum, vMTrialBalance.FromDate, vMTrialBalance.ToDate);
                head.OpeningBalanceDr = OpeningBalanceDr;
                head.OpeningBalanceCr = OpeningBalanceCr;
                head.Debit = dr;
                head.Credit = cr;
                head.ClosingBalanceDr = closingBalanceDr;
                head.ClosingBalanceCr = closingBalanceCr;
                head.EntryCount = entryCount;
            }
            return vMTrialBalance;
        }

        private (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr) GetTrialBalanceInfoforTrialBalance(int iD, AccountTypeEnum accountTypeEnum, DateTime fromDate, DateTime toDate)
        {
            // var openingBalance = GetOpeningBlanceOLD(iD, accountTypeEnum, fromDate);
            (decimal balance, decimal balanceDr, decimal balanceCr, int sl) openingBalance = GetOpeningBlanceNew(iD, accountTypeEnum, fromDate);
            List<VMLedger> ledgerInfo = GetTheLedgerforTrialBalance(iD, accountTypeEnum, fromDate, toDate, openingBalance.balance, openingBalance.sl).ToList();
            decimal dr = ledgerInfo.Select(x => x.Debit).Sum();
            decimal cr = ledgerInfo.Select(x => x.Credit).Sum();
            decimal closingBalance = ledgerInfo.Select(x => x.Balance).LastOrDefault();

            decimal closingBalanceDr = ledgerInfo.Select(x => x.ClosingBalanceDr).LastOrDefault();
            decimal closingBalanceCr = ledgerInfo.Select(x => x.ClosingBalanceCr).LastOrDefault();

            int entryCount = ledgerInfo.Count();
            return (openingBalance.balance, dr, cr, closingBalance, entryCount, openingBalance.balanceDr, openingBalance.balanceCr, closingBalanceDr, closingBalanceCr);
            //return (openingBalance.balance, dr, cr, closingBalance, entryCount);
        }

        private IEnumerable<VMLedger> GetTheLedgerforTrialBalance(int accountingId, AccountTypeEnum accountTypeEnum, DateTime fromDate, DateTime toDate, decimal openingBalance, int sl)
        {
            decimal closingBalance = openingBalance;
            bool drIncrease = false;
            List<VMLedger> theLedger;
            if (accountTypeEnum == AccountTypeEnum.Type)
            {
                theLedger = (from t1 in _db.Accounting_Head
                             join t2 in _db.Accounting_JournalSlave
                             on t1.ID equals t2.Accounting_HeadFK
                             join t3 in _db.Accounting_Journal
                             on t2.Accounting_JournalFK equals t3.ID
                             join t4 in _db.Accounting_CostCenter
                            on t3.Accounting_CostCenterFK equals t4.ID
                             join t5 in _db.Accounting_Chart2
                            on t1.Accounting_Chart2FK equals t5.ID
                             join t6 in _db.Accounting_Chart1
                            on t5.Accounting_Chart1FK equals t6.ID
                             join t7 in _db.Accounting_Type
                             on t6.Accounting_TypeFK equals t7.ID
                             where t1.ID == accountingId &&
                             t3.Date >= fromDate && t3.Date <= toDate
                             && t2.Active == true && t3.Active == true && t3.Approved == true && t3.Finalized == true
                             select new VMLedger
                             {
                                 Accounting_TypeFK = t7.ID,
                                 Accounting_Chart1FK = t6.ID,
                                 Accounting_Chart2FK = t5.ID,
                                 Accounting_HeadId = t1.ID,
                                 Accounting_HeadName = t1.Name,
                                 Accounting_TypeName = t7.Name,
                                 Accounting_Chart1Name = t6.Name,
                                 Accounting_Chart2Name = t5.Name,
                                 Accounting_CostCenterName = t4.Title,
                                 Date = t3.Date,
                                 PostedDate = t2.Time,
                                 Description = t3.CID + " , " + t3.Title + " , " + t2.Description,
                                 Debit = t2.Debit,
                                 Credit = t2.Credit,
                                 Balance = 0,
                                 ClosingBalanceDr = 0,
                                 ClosingBalanceCr = 0,
                                 DrIncrease = t7.DrIncrease
                             }).OrderBy(x => x.Date).ToList();
                // theTempLedger.Where(x => x.Accounting_TypeFK == accountingId).OrderBy(x => x.Date).ToList();
                drIncrease = theLedger.Where(x => x.Accounting_TypeFK == accountingId).Select(x => x.DrIncrease).FirstOrDefault();
            }
            else if (accountTypeEnum == AccountTypeEnum.C1)
            {
                theLedger = (from t1 in _db.Accounting_Head
                             join t2 in _db.Accounting_JournalSlave
                             on t1.ID equals t2.Accounting_HeadFK
                             join t3 in _db.Accounting_Journal
                             on t2.Accounting_JournalFK equals t3.ID
                             join t4 in _db.Accounting_CostCenter
                            on t3.Accounting_CostCenterFK equals t4.ID
                             join t5 in _db.Accounting_Chart2
                            on t1.Accounting_Chart2FK equals t5.ID
                             join t6 in _db.Accounting_Chart1
                            on t5.Accounting_Chart1FK equals t6.ID
                             join t7 in _db.Accounting_Type
                             on t6.Accounting_TypeFK equals t7.ID
                             where t6.ID == accountingId &&
                             t3.Date >= fromDate && t3.Date <= toDate
                             && t2.Active == true && t3.Active == true && t3.Approved == true && t3.Finalized == true
                             select new VMLedger
                             {
                                 Accounting_TypeFK = t7.ID,
                                 Accounting_Chart1FK = t6.ID,
                                 Accounting_Chart2FK = t5.ID,
                                 Accounting_HeadId = t1.ID,
                                 Accounting_HeadName = t1.Name,
                                 Accounting_TypeName = t7.Name,
                                 Accounting_Chart1Name = t6.Name,
                                 Accounting_Chart2Name = t5.Name,
                                 Accounting_CostCenterName = t4.Title,
                                 Date = t3.Date,
                                 PostedDate = t2.Time,
                                 Description = t3.CID + " , " + t3.Title + " , " + t2.Description,
                                 Debit = t2.Debit,
                                 Credit = t2.Credit,
                                 Balance = 0,
                                 ClosingBalanceDr = 0,
                                 ClosingBalanceCr = 0,
                                 DrIncrease = t7.DrIncrease
                             }).OrderBy(x => x.Date).ToList();
                //theLedger = theTempLedger.Where(x => x.Accounting_Chart1FK == accountingId).OrderBy(x => x.Date).ToList();
                drIncrease = theLedger.Where(x => x.Accounting_Chart1FK == accountingId).Select(x => x.DrIncrease).FirstOrDefault();
            }
            else if (accountTypeEnum == AccountTypeEnum.C2)
            {
                theLedger = (from t1 in _db.Accounting_Head
                             join t2 in _db.Accounting_JournalSlave
                             on t1.ID equals t2.Accounting_HeadFK
                             join t3 in _db.Accounting_Journal
                             on t2.Accounting_JournalFK equals t3.ID
                             join t4 in _db.Accounting_CostCenter
                            on t3.Accounting_CostCenterFK equals t4.ID
                             join t5 in _db.Accounting_Chart2
                            on t1.Accounting_Chart2FK equals t5.ID
                             join t6 in _db.Accounting_Chart1
                            on t5.Accounting_Chart1FK equals t6.ID
                             join t7 in _db.Accounting_Type
                             on t6.Accounting_TypeFK equals t7.ID
                             where t5.ID == accountingId &&
                             t3.Date >= fromDate && t3.Date <= toDate
                             && t2.Active == true && t3.Active == true && t3.Approved == true && t3.Finalized == true
                             select new VMLedger
                             {
                                 Accounting_TypeFK = t7.ID,
                                 Accounting_Chart1FK = t6.ID,
                                 Accounting_Chart2FK = t5.ID,
                                 Accounting_HeadId = t1.ID,
                                 Accounting_HeadName = t1.Name,
                                 Accounting_TypeName = t7.Name,
                                 Accounting_Chart1Name = t6.Name,
                                 Accounting_Chart2Name = t5.Name,
                                 Accounting_CostCenterName = t4.Title,
                                 Date = t3.Date,
                                 PostedDate = t2.Time,
                                 Description = t3.CID + " , " + t3.Title + " , " + t2.Description,
                                 Debit = t2.Debit,
                                 Credit = t2.Credit,
                                 Balance = 0,
                                 ClosingBalanceDr = 0,
                                 ClosingBalanceCr = 0,
                                 DrIncrease = t7.DrIncrease
                             }).OrderBy(x => x.Date).ToList();
                //theLedger = theTempLedger.Where(x => x.Accounting_Chart2FK == accountingId).OrderBy(x => x.Date).ToList();
                drIncrease = theLedger.Where(x => x.Accounting_Chart2FK == accountingId).Select(x => x.DrIncrease).FirstOrDefault();
            }
            else
            {
                theLedger = (from t1 in _db.Accounting_Head
                             join t2 in _db.Accounting_JournalSlave
                             on t1.ID equals t2.Accounting_HeadFK
                             join t3 in _db.Accounting_Journal
                             on t2.Accounting_JournalFK equals t3.ID
                             join t4 in _db.Accounting_CostCenter
                            on t3.Accounting_CostCenterFK equals t4.ID
                             join t5 in _db.Accounting_Chart2
                            on t1.Accounting_Chart2FK equals t5.ID
                             join t6 in _db.Accounting_Chart1
                            on t5.Accounting_Chart1FK equals t6.ID
                             join t7 in _db.Accounting_Type
                             on t6.Accounting_TypeFK equals t7.ID
                             where t1.ID == accountingId &&
                             t3.Date >= fromDate && t3.Date <= toDate
                             && t2.Active == true && t3.Active == true && t3.Approved == true && t3.Finalized == true
                             select new VMLedger
                             {
                                 Accounting_TypeFK = t7.ID,
                                 Accounting_Chart1FK = t6.ID,
                                 Accounting_Chart2FK = t5.ID,
                                 Accounting_HeadId = t1.ID,
                                 Accounting_HeadName = t1.Name,
                                 Accounting_TypeName = t7.Name,
                                 Accounting_Chart1Name = t6.Name,
                                 Accounting_Chart2Name = t5.Name,
                                 Accounting_CostCenterName = t4.Title,
                                 Date = t3.Date,
                                 PostedDate = t2.Time,
                                 Description = t3.CID + " , " + t3.Title + " , " + t2.Description,
                                 Debit = t2.Debit,
                                 Credit = t2.Credit,
                                 Balance = 0,
                                 ClosingBalanceDr = 0,
                                 ClosingBalanceCr = 0,
                                 DrIncrease = t7.DrIncrease
                             }).OrderBy(x => x.Date).ToList();
                //theLedger = theTempLedger.Where(x => x.Accounting_HeadId == accountingId).OrderBy(x => x.Date).ToList();
                drIncrease = theLedger.Where(x => x.Accounting_HeadId == accountingId).Select(x => x.DrIncrease).FirstOrDefault();
            }
            int serial = sl;
            //  var drIncrease = GetHeadDrIncreaseOrDecreaseType(accountingId, accountTypeEnum);
            if (drIncrease)
            {
                foreach (VMLedger entry in theLedger)
                {
                    entry.Sl = ++serial;
                    entry.Balance = entry.Debit - entry.Credit + closingBalance;
                    closingBalance = entry.Balance;
                    entry.ClosingBalanceDr = entry.Balance;

                }
            }
            else
            {
                foreach (VMLedger entry in theLedger)
                {
                    entry.Sl = ++serial;
                    entry.Balance = entry.Credit - entry.Debit + closingBalance;
                    closingBalance = entry.Balance;
                    entry.ClosingBalanceCr = entry.Balance;
                }
            }
            return (theLedger);
        }





        #endregion End Trial Balance Final
        public VMCogs CogsGetByDate(DateTime fromdate, DateTime todate)
        {
            return GetCostofGoodsSoldInfo(fromdate, todate);
        }

        public VMIncomeStatement ComprehensiveIncomeGetByDate(DateTime fromdate, DateTime todate)
        {
            //return GetComprehensiveIncomeInfoOLD(fromdate, todate);
            return GetComprehensiveIncomeInfo(fromdate, todate);
        }

        public VMFinancialPosition FinancialPositionGetByDate(DateTime fromdate, DateTime todate)
        {
            return GetFinanCialPositionInfo(fromdate, todate);
        }


        public VMCashFlow CashFlowGetByDate(DateTime fromdate, DateTime todate)
        {
            return GetCashFlowInfo(fromdate, todate);
        }

        #region Journal Create,Update, Delete Portion Services
        public async Task<VMJournal> JournalGet(int journaltype)
        {
            VMJournal vMJournal = new VMJournal
            {
                DataList = await Task.Run(() => (from t1 in _db.Accounting_Journal
                                                 join t2 in _db.Accounting_CostCenter on t1.Accounting_CostCenterFK equals t2.ID
                                                 where (t1.Active == true && t1.JournalType == journaltype)
                                                 select new VMJournal
                                                 {
                                                     ID = t1.ID,
                                                     CID = t1.CID,
                                                     Title = t1.Title,
                                                     Description = t1.Description,
                                                     Date = t1.Date,
                                                     Remark = t1.Remark,
                                                     Finalized = t1.Finalized,
                                                     Approved = t1.Approved,
                                                     JournalType = t1.JournalType,
                                                     Accounting_CostCenterFK = t2.ID,
                                                     Accounting_CostCenterName = t2.Title
                                                 }).OrderByDescending(x => x.ID).ToList())
            };
            return vMJournal;
        }

        public async Task<VMJournal> JournalGetSpecific(int id)
        {
            VMJournal vMJournal = new VMJournal();
            vMJournal = await Task.Run(() => (from t1 in _db.Accounting_Journal
                                              join t2 in _db.Accounting_CostCenter on t1.Accounting_CostCenterFK equals t2.ID
                                              where (t1.ID == id && t1.Active == true)
                                              select new VMJournal
                                              {
                                                  ID = t1.ID,
                                                  CID = t1.CID,
                                                  Title = t1.Title,
                                                  Description = t1.Description,
                                                  Date = t1.Date,
                                                  Remark = t1.Remark,
                                                  Finalized = t1.Finalized,
                                                  Approved = t1.Approved,
                                                  Accounting_CostCenterFK = t2.ID,
                                                  Accounting_CostCenterName = t2.Title,
                                                  JournalType = t1.JournalType
                                              }).FirstOrDefault());
            return vMJournal;
        }
        

        public async Task<VMJournal> RealisationJournalGetSpecific(int? id)
        {
            VMJournal vMJournal = new VMJournal();
            vMJournal = await Task.Run(() => (from t1 in _db.Accounting_Journal
                                              join t2 in _db.Accounting_CostCenter on t1.Accounting_CostCenterFK equals t2.ID
                                              where (t1.Remark == Convert.ToString(id) && t1.Active == true)
                                              select new VMJournal
                                              {
                                                  ID = t1.ID,
                                                  CID = t1.CID,
                                                  Title = t1.Title,
                                                  Description = t1.Description,
                                                  Date = t1.Date,
                                                  Remark = t1.Remark,
                                                  Finalized = t1.Finalized,
                                                  Approved = t1.Approved,
                                                  Accounting_CostCenterFK = t2.ID,
                                                  Accounting_CostCenterName = t2.Title,
                                                  JournalType = t1.JournalType
                                              }).FirstOrDefault());
            return vMJournal;
        }

        public IEnumerable<VMShipmentBillOfExchange> ShipmentBillOfExchangesGetJoinVoucher()
        {

            VMShipmentBillOfExchange model = new VMShipmentBillOfExchange();
            model.DataList = (from t1 in _db.Shipment_BillOfExchange
                              join t2 in _db.Commercial_ECI on t1.Commercial_ECIFK equals t2.ID
                              join t3 in _db.Common_Buyer on t2.Common_BuyerFK equals t3.ID
                              join av in _db.Accounting_Journal on t1.ID equals Convert.ToInt32(av.Remark) into av_Join
                              from av in av_Join.DefaultIfEmpty()
                              where t1.Active
                              select new VMShipmentBillOfExchange
                              {
                                  IsFinal = t1.IsFinal,
                                  BuyerName = t3.Name,
                                  ID = t1.ID,
                                  BoEDate = t1.BoEDate,
                                  BoECID = t1.BoECID,
                                  ECINO = t2.ECINo,
                                  Commercial_ECIFK = t1.Commercial_ECIFK,
                                  CourierDate = t1.CourierDate,
                                  CourierNO = t1.CourierNO,
                                  Description = t1.Description,
                                  FDBCDate = t1.FDBCDate,
                                  FDBCNO = t1.FDBCNO,
                                  Remarks = t1.Remarks,
                                  ReceivedType = t1.ReceivedType,
                                  AccID = av.ID,
                                  AccFinalize = av.Finalized,
                                  AccApproved = av.Approved,

                                  BillValue = (from x in _db.Shipment_BillOfExchangeSlave
                                               join z in _db.Shipment_ShipmentInvoiceSlave on x.Shipment_ShipmentInvoiceFk equals z.Shipment_InvoiceFk
                                               join a in _db.Merchandising_Style on z.Merchandising_StyleFk equals a.ID
                                               where x.Active && x.Shipment_BillOfExchangeFk == t1.ID
                                               select z.InvoicedQuantity * a.PackPrice).DefaultIfEmpty(0).Sum()

                              }).OrderByDescending(x => x.ID).AsEnumerable();

            return model.DataList;
        }

        private string GetMaxJournalId(int type)
        {
            // int count =  _db.Accounting_Journal.Where(x => x.JournalType == type).Select(x => x.ID).ToList().DefaultIfEmpty(0).Count();
            // _db.Accounting_Journal.Where(x => x.JournalType == type).ToList();
            //  int count = result.Count();
            int count = _db.Accounting_Journal.Where(x => x.JournalType == type).Count();
            count++;
            string journaltype = "";
            if (type == 1)
            {
                journaltype = "JRV";
            }
            else if (type == 2)
            {
                journaltype = "DRV";
            }
            else if (type == 3)
            {
                journaltype = "CRV";
            }
            else if (type == 4)
            {
                journaltype = "CNV";
            }
            else if (type == 5)
            {
                journaltype = "PRV";
            }
            else if (type == 6)
            {
                journaltype = "SLV";
            }
            else if (type == 7)
            {
                journaltype = "INT";
            }
            else if (type == 8)
            {
                journaltype = "QIC";
            }
            else if (type == 9)
            {
                journaltype = "RLV";
            }
            string voucherType = journaltype + "-" + count.ToString().PadLeft(5, '0');
            return voucherType;
        }


        public async Task<int> JournalAdd(VMJournal vMJournal)
        {
            int result = -1;
            string type = GetMaxJournalId(vMJournal.JournalType);
            Accounting_Journal accounting_Journal = new Accounting_Journal
            {
                Accounting_CostCenterFK = vMJournal.Accounting_CostCenterFK,
                JournalType = vMJournal.JournalType,
                CID = type,
                Date = vMJournal.Date,
                Title = vMJournal.Title,
                Description = vMJournal.Description,
                Remark = vMJournal.Remark,
                User = vMJournal.User,
                Finalized = vMJournal.Finalized,
                Approved = vMJournal.Approved,
            };
            _db.Accounting_Journal.Add(accounting_Journal);
            if(vMJournal.isRealization == true)
            {
                VMJournalSlave vMJS = new VMJournalSlave()
                {
                    AmountType = 1,
                    Accounting_JournalFK = accounting_Journal.ID,
                    Accounting_HeadFK = vMJournal.ERAccounting_HeadFK,
                    ConversionRateToBDT = vMJournal.ConversionRateToBDT,
                    ForeignCurrencyAmount = vMJournal.CreditAmount,
                    Amount = vMJournal.CreditAmount * vMJournal.ConversionRateToBDT,
                };
                await JournalDetailsAdd(vMJS);
            }

            if (await _db.SaveChangesAsync() > 0)
            {
                result = accounting_Journal.ID;
            }
            return result;
        }
        public async Task<int> JournalEdit(VMJournal vMJournal)
        {
            int result = -1;
            //to select Account Journal data.....
            Accounting_Journal accounting_Journal = _db.Accounting_Journal.Find(vMJournal.ID);
            accounting_Journal.Accounting_CostCenterFK = vMJournal.Accounting_CostCenterFK;
            accounting_Journal.Date = vMJournal.Date;
            accounting_Journal.Title = vMJournal.Title;
            accounting_Journal.Description = vMJournal.Description;
            accounting_Journal.Remark = vMJournal.Remark;
            if (await _db.SaveChangesAsync() > 0)
            {
                result = accounting_Journal.ID;
            }
            return result;
        }
        public async Task<int> JournalDelete(int id)
        {
            int result = -1;
            Accounting_Journal accounting_Journal = _db.Accounting_Journal.Find(id);
            accounting_Journal.Active = false;
            if (await _db.SaveChangesAsync() > 0)
            {
                result = accounting_Journal.ID;
            }
            return result;
        }

        public async Task<VMJournalSlave> JournalDetailsGet(int id)
        {
            //VMJournalWithJournalSlave model = new VMJournalWithJournalSlave();
            VMJournalSlave VMJournalSlave = new VMJournalSlave
            {
                vMJournal = new VMJournal()
            };
            // VMJournalSlave vMJournalSlave = new VMJournalSlave();
            VMJournalSlave = await Task.Run(() => JournalSlaveGetSpecific(id));
            VMJournalSlave.Accounting_JournalFK = id;
            //VMJournal vMJournal = new VMJournal();
            VMJournalSlave.vMJournal = await Task.Run(() => JournalGetSpecific(id));
            return VMJournalSlave;
        }

        public async Task<VMJournalSlave> RealisationJournalDetailsGet(int? id)
        {
            //VMJournalWithJournalSlave model = new VMJournalWithJournalSlave();
            Accounting_Journal RID = _db.Accounting_Journal.Where(r => r.Remark == Convert.ToString(id)).FirstOrDefault();
            VMJournalSlave VMJournalSlave = new VMJournalSlave
            {
                vMJournal = new VMJournal()
            };
            // VMJournalSlave vMJournalSlave = new VMJournalSlave();
            if(RID != null)
            {
                VMJournalSlave = await Task.Run(() => JournalSlaveGetSpecific(RID.ID));
                VMJournalSlave.Accounting_JournalFK = RID.ID;
            }
            //VMJournal vMJournal = new VMJournal();
            VMJournalSlave.vMJournal = await Task.Run(() => RealisationJournalGetSpecific(id));
            return VMJournalSlave;
        }

        public VMJournalSlave JournalSlaveGetSpecific(int id)
        {
            VMJournalSlave vMJournalSlave = new VMJournalSlave
            {
                TotalCredit = _db.Accounting_JournalSlave.Where(x => x.Accounting_JournalFK == id && x.Active == true).Select(x => x.Credit).Sum(),
                TotalDebit = _db.Accounting_JournalSlave.Where(x => x.Accounting_JournalFK == id && x.Active == true).Select(x => x.Debit).Sum(),

                DataList1 = (from t1 in _db.Accounting_JournalSlave
                             join t6 in _db.Accounting_Journal on t1.Accounting_JournalFK equals t6.ID
                             join t2 in _db.Accounting_Head on t1.Accounting_HeadFK equals t2.ID
                             join t3 in _db.Accounting_Journal on t1.Accounting_JournalFK equals t3.ID
                             join t4 in _db.Accounting_Chart2 on t2.Accounting_Chart2FK equals t4.ID
                             join t5 in _db.Accounting_Chart1 on t4.Accounting_Chart1FK equals t5.ID
                             where (t1.Accounting_JournalFK == id && t1.Active == true)
                             select new VMJournalSlave
                             {
                                 ID = t1.ID,
                                 Title = t1.Title,
                                 Description = t1.Description,
                                 Debit = t1.Debit,
                                 Credit = t1.Credit,
                                 Remark = t1.Remark,
                                 Accounting_HeadFK = t1.Accounting_HeadFK,
                                 Accounting_HeadName = t2.Name,
                                 Accounting_Chart1FK = t5.ID,
                                 Accounting_Chart1Name = t5.Name,
                                 Accounting_Chart2FK = t4.ID,
                                 Accounting_Chart2Name = t4.Name,
                                 Accounting_TypeFK = t5.Accounting_TypeFK,
                                 Accounting_JournalFK = t1.Accounting_JournalFK,
                                 Finalized = t6.Finalized,
                                 Approved = t6.Approved

                             }).AsEnumerable()
            };

            return vMJournalSlave;
        }

        public async Task<int> JournalDetailsAdd(VMJournalSlave vMJournalSlave)
        {
            int result = -1;
            //GET Account Journal and Update Status.....
            Accounting_Journal accountingJournal = _db.Accounting_Journal.Find(vMJournalSlave.Accounting_JournalFK);
            accountingJournal.Finalized = vMJournalSlave.Finalized;
            if (vMJournalSlave.AmountType == 1)
            {
                vMJournalSlave.Credit = vMJournalSlave.Amount;
                vMJournalSlave.Debit = 0;
                vMJournalSlave.ConversionRateToBDT = vMJournalSlave.ConversionRateToBDT;
                vMJournalSlave.ForeignCurrencyAmount = vMJournalSlave.ForeignCurrencyAmount;
            }
            else if (vMJournalSlave.AmountType == 2)
            {
                vMJournalSlave.Debit = vMJournalSlave.Amount;
                vMJournalSlave.Credit = 0;
                vMJournalSlave.ConversionRateToBDT = vMJournalSlave.ConversionRateToBDT;
                vMJournalSlave.ForeignCurrencyAmount = vMJournalSlave.ForeignCurrencyAmount;
            }
            Accounting_JournalSlave accountingJournalSlave = new Accounting_JournalSlave
            {
                Title = vMJournalSlave.Title,
                Description = vMJournalSlave.Description,
                Remark = vMJournalSlave.Remark,
                Debit = vMJournalSlave.Debit,
                Credit = vMJournalSlave.Credit,
                Accounting_HeadFK = vMJournalSlave.Accounting_HeadFK,
                Accounting_JournalFK = vMJournalSlave.Accounting_JournalFK,
                User = vMJournalSlave.User,
                ForeignCurrencyAmount = vMJournalSlave.ForeignCurrencyAmount,
                ConversionRateToBDT = vMJournalSlave.ConversionRateToBDT
            };
            _db.Accounting_JournalSlave.Add(accountingJournalSlave);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = accountingJournalSlave.ID;
            }
            return result;
        }


        public async Task<int> AccountingJournalMasterPush(VMJournalSlave vmJournalSlave)
        {
            //IntegratedJournalTypeEnum typeEnum, int HeadID, decimal amount, int nNUserID, int accountingCostCenterFk = 0
            //JournalMasterAndSlaveAdd

            int result = -1;
            //GET Account Journal and Update Status.....
            //string integrationType = "";
           // int drHeadId = 0, crHeadId = 0;

            Accounting_Journal accountingJournal = new Accounting_Journal();
            accountingJournal.Accounting_CostCenterFK = vmJournalSlave.Accounting_CostCenterFK;
            accountingJournal.JournalType = (int)JournalEnum.IntegrationVoucher;
            accountingJournal.CID = GetMaxJournalId((int)JournalEnum.IntegrationVoucher);
            accountingJournal.Date = DateTime.Now;
            accountingJournal.Title = vmJournalSlave.Title; //"Integrated Journal: " + integrationType;
            accountingJournal.Description = vmJournalSlave.Description; //"Integrated Journal: " + integrationType;
            accountingJournal.UserID = vmJournalSlave.UserID;
            accountingJournal.Finalized = true;
            accountingJournal.Approved = true;

            accountingJournal.Accounting_JournalSlave = new List<Accounting_JournalSlave>();
            accountingJournal.Accounting_JournalSlave = vmJournalSlave.DataListSlave.Select(x => new Accounting_JournalSlave
            {
                Title = x.Title,
                Description = x.Description,
                Remark = x.Remark,
                Debit = x.Debit,
                Credit = x.Credit,
                ForeignCurrencyAmount = x.ForeignCurrencyAmount,
                ConversionRateToBDT = x.ConversionRateToBDT,
                Accounting_HeadFK = x.Accounting_HeadFK,
                User = x.User
            }).ToList();
            await _db.Accounting_Journal.AddRangeAsync(accountingJournal);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = accountingJournal.ID;
            }
            return result;
        }
                     
        //public async Task<int> AccountingJournalPush(IntegratedJournalTypeEnum typeEnum, int HeadID, decimal amount, int nNUserID, int accountingCostCenterFk = 0)
        //{
        //    int result = -1;
        //    //GET Account Journal and Update Status.....
        //    string integrationType = "";
        //    int drHeadId = 0, crHeadId = 0;


        //    switch (typeEnum)
        //    {
        //        case IntegratedJournalTypeEnum.BuyerCollection:
        //            integrationType = "Buyer Receivable";
        //            crHeadId = HeadID;
        //            drHeadId=
        //            break;
        //        case IntegratedJournalTypeEnum.SupplierPayment:
        //            integrationType = "Supplier Payable"; break;
        //        default:
        //            integrationType = ""; break;
        //    }



        //    vMJournalSlave.DataListSlave = new List<VMJournalSlave>();
        //    vMJournalSlave.DataListSlave.Add(new VMJournalSlave
        //    {
        //        Description = vMJournalSlave.Title,
        //        Debit = amount,
        //        Credit = 0,
        //        Accounting_HeadFK = drHeadID,

        //    });
        //    vMJournalSlave.DataListSlave.Add(new VMJournalSlave
        //    {
        //        Description = vMJournalSlave.Title,
        //        Debit = 0,
        //        Credit = amount,
        //        Accounting_HeadFK = crHeadID,

        //    });



        //    Accounting_Journal accountingJournal = new Accounting_Journal();
        //    accountingJournal.Accounting_CostCenterFK = accountingCostCenterFk;
        //    accountingJournal.JournalType = (int)JournalEnum.IntegrationVoucher;
        //    accountingJournal.CID = GetMaxJournalId((int)JournalEnum.IntegrationVoucher);
        //    accountingJournal.Date = DateTime.Now;
        //    accountingJournal.Title = "Integrated Journal: " + integrationType;
        //    accountingJournal.Description = "Integrated Journal: " + integrationType;
        //     accountingJournal.UserID = nNUserID;
        //    accountingJournal.Finalized = true;
        //    accountingJournal.Approved = true;

        //    accountingJournal.Accounting_JournalSlave = new List<Accounting_JournalSlave>();
        //    accountingJournal.Accounting_JournalSlave.Add(new Accounting_JournalSlave
        //    {
        //        Description = "Integrated Journal: " + integrationType,
        //        Debit = amount,
        //        Credit = 0,
        //        Accounting_HeadFK = drHeadID,

        //    });
        //    vMJournalSlave.DataListSlave.Add(new VMJournalSlave
        //    {
        //        Description = vMJournalSlave.Title,
        //        Debit = 0,
        //        Credit = amount,
        //        Accounting_HeadFK = crHeadID,

        //    });

        //    accountingJournal.Accounting_JournalSlave = vMJournalSlave.DataListSlave.Select(x => new Accounting_JournalSlave {
        //        Title = x.Title,
        //        Description = x.Description,
        //        Remark = x.Remark,
        //        Debit = x.Debit,
        //        Credit = x.Credit,
        //        Accounting_HeadFK = x.Accounting_HeadFK,
        //        User = x.User
        //    }).ToList();


        //    //if (vMJournalSlave.AmountType == 1)
        //    //{
        //    //    vMJournalSlave.Credit = vMJournalSlave.Amount;
        //    //    vMJournalSlave.Debit = 0;
        //    //}
        //    //else if (vMJournalSlave.AmountType == 2)
        //    //{
        //    //    vMJournalSlave.Debit = vMJournalSlave.Amount;
        //    //    vMJournalSlave.Credit = 0;
        //    //}

        //    //Accounting_JournalSlave accountingJournalSlave = new Accounting_JournalSlave
        //    //{
        //    //    Title = vMJournalSlave.Title,
        //    //    Description = vMJournalSlave.Description,
        //    //    Remark = vMJournalSlave.Remark,
        //    //    Debit = vMJournalSlave.Debit,
        //    //    Credit = vMJournalSlave.Credit,
        //    //    Accounting_HeadFK = vMJournalSlave.Accounting_HeadFK,
        //    //    Accounting_JournalFK = vMJournalSlave.Accounting_JournalFK,
        //    //    User = vMJournalSlave.User
        //    //};
        //    await _db.Accounting_Journal.AddRangeAsync(accountingJournal);
        //    if (await _db.SaveChangesAsync() > 0)
        //    {
        //        result = accountingJournal.ID;
        //    }
        //    return result;
        //}
        public async Task<int> JournalDetailsEdit(VMJournalSlave vMJournalSlave)
        {
            int result = -1;
            //GET Account Journal and Update Status.....
            Accounting_Journal accountingJournal = _db.Accounting_Journal.Find(vMJournalSlave.Accounting_JournalFK);
            accountingJournal.Finalized = vMJournalSlave.Finalized;
            if (vMJournalSlave.AmountType == 1)
            {
                vMJournalSlave.Credit = vMJournalSlave.Amount;
                vMJournalSlave.Debit = 0;
            }
            else if (vMJournalSlave.AmountType == 2)
            {
                vMJournalSlave.Debit = vMJournalSlave.Amount;
                vMJournalSlave.Credit = 0;
            }
            //to select Account Journal slave data.....
            Accounting_JournalSlave accountingJournalSlave = _db.Accounting_JournalSlave.Find(vMJournalSlave.ID);
            accountingJournalSlave.Accounting_HeadFK = vMJournalSlave.Accounting_HeadFK;
            accountingJournalSlave.Title = vMJournalSlave.Title;
            accountingJournalSlave.Description = vMJournalSlave.Description;
            accountingJournalSlave.Remark = vMJournalSlave.Remark;
            accountingJournalSlave.Debit = vMJournalSlave.Debit;
            accountingJournalSlave.Credit = vMJournalSlave.Credit;
            if (await _db.SaveChangesAsync() > 0)
            {
                result = accountingJournalSlave.ID;
            }
            return result;
        }
        public async Task<int> JournalDetailsDelete(int id)
        {
            int result = -1;
            Accounting_JournalSlave accountingJournalSlave = _db.Accounting_JournalSlave.Find(id);
            accountingJournalSlave.Active = false;
            if (await _db.SaveChangesAsync() > 0)
            {
                result = accountingJournalSlave.Accounting_JournalFK;
            }
            return result;
        }

        public async Task<int> JournalFinalized(int id)
        {
            Accounting_Journal accountingJournal = _db.Accounting_Journal.Find(id);
            int result = -1;
            decimal totalcredit = _db.Accounting_JournalSlave.Where(x => x.Accounting_JournalFK == id && x.Active == true).Select(x => x.Credit).Sum();
            decimal totaldebit = _db.Accounting_JournalSlave.Where(x => x.Accounting_JournalFK == id && x.Active == true).Select(x => x.Debit).Sum();
            if (totalcredit == totaldebit)
            {
                accountingJournal.Finalized = true;
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = 1;
                }
            }
            else
            {
                result = 2;
            }

            return result;
        }

        public async Task<int> JournalApproved(int id)
        {
            Accounting_Journal accountingJournal = _db.Accounting_Journal.Find(id);
            int result = -1;
            //var totalcredit = _db.Accounting_JournalSlave.Where(x => x.Accounting_JournalFK == id && x.Active == true).Select(x => x.Credit).Sum();
            //var totaldebit = _db.Accounting_JournalSlave.Where(x => x.Accounting_JournalFK == id && x.Active == true).Select(x => x.Debit).Sum();
            //if (totalcredit == totaldebit)
            //{
            accountingJournal.Approved = true;
            if (await _db.SaveChangesAsync() > 0)
            {
                result = 1;
            }
            //}
            //else
            //{
            //    result = 2;
            //}

            return result;
        }
        #endregion

        #region Private Method


        private (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) GetTrialBalanceInfo(int iD, AccountTypeEnum accountTypeEnum, DateTime fromDate, DateTime toDate)
        {
            // var openingBalance = GetOpeningBlanceOLD(iD, accountTypeEnum, fromDate);
            (decimal balance, decimal balanceDr, decimal balanceCr, int sl) openingBalance = GetOpeningBlanceNew(iD, accountTypeEnum, fromDate);
            List<VMLedger> ledgerInfo = GetTheLedger(iD, accountTypeEnum, fromDate, toDate, openingBalance.balance, openingBalance.sl).ToList();
            decimal dr = ledgerInfo.Select(x => x.Debit).Sum();
            decimal cr = ledgerInfo.Select(x => x.Credit).Sum();
            decimal closingBalance = ledgerInfo.Select(x => x.ClosingBalance).LastOrDefault();
            decimal balance = ledgerInfo.Select(x => x.Balance).LastOrDefault();

            decimal closingBalanceDr = ledgerInfo.Select(x => x.ClosingBalanceDr).LastOrDefault();
            decimal closingBalanceCr = ledgerInfo.Select(x => x.ClosingBalanceCr).LastOrDefault();


            int entryCount = ledgerInfo.Count();
            return (openingBalance.balance, dr, cr, closingBalance, entryCount, openingBalance.balanceDr, openingBalance.balanceCr, closingBalanceDr, closingBalanceCr, balance);
            //return (openingBalance.balance, dr, cr, closingBalance, entryCount);
        }

        private IEnumerable<VMLedger> GetTheLedger(int accountingId, AccountTypeEnum accountTypeEnum, DateTime fromDate, DateTime toDate, decimal openingBal, int sl)
        {
            var accountingHeadName = "";
            bool drIncrease = false;
            IEnumerable<VMLedger> theTempLedger = (from t1 in _db.Accounting_Head
                                                   join t2 in _db.Accounting_JournalSlave
                                                   on t1.ID equals t2.Accounting_HeadFK
                                                   join t3 in _db.Accounting_Journal
                                                   on t2.Accounting_JournalFK equals t3.ID
                                                   join t4 in _db.Accounting_CostCenter
                                                  on t3.Accounting_CostCenterFK equals t4.ID
                                                   join t5 in _db.Accounting_Chart2
                                                  on t1.Accounting_Chart2FK equals t5.ID
                                                   join t6 in _db.Accounting_Chart1
                                                  on t5.Accounting_Chart1FK equals t6.ID
                                                   join t7 in _db.Accounting_Type
                                                   on t6.Accounting_TypeFK equals t7.ID
                                                   where
                                                   // t1.ID == accountingId && 
                                                   t3.Date >= fromDate && t3.Date <= toDate
                                                   && t2.Active == true && t3.Active == true && t3.Approved == true && t3.Finalized == true
                                                   select new VMLedger
                                                   {
                                                       Accounting_TypeFK = t7.ID,
                                                       Accounting_Chart1FK = t6.ID,
                                                       Accounting_Chart2FK = t5.ID,
                                                       Accounting_HeadId = t1.ID,
                                                       Accounting_HeadName = t1.Name,
                                                       Accounting_TypeName = t7.Name,
                                                       Accounting_Chart1Name = t6.Name,
                                                       Accounting_Chart2Name = t5.Name,
                                                       Accounting_CostCenterName = t4.Title,
                                                       Date = t3.Date,
                                                       PostedDate = t2.Time,
                                                       Description = t3.CID + " , " + t3.Title + " , " + t2.Description,
                                                       Debit = t2.Debit,
                                                       Credit = t2.Credit,
                                                       Balance = 0,
                                                       ClosingBalance = 0,
                                                       ClosingBalanceDr = 0,
                                                       ClosingBalanceCr = 0,
                                                       DrIncrease = t7.DrIncrease
                                                   }).AsEnumerable();
            decimal closingBalance = openingBal;
            List<VMLedger> theLedger;
            if (accountTypeEnum == AccountTypeEnum.Type)
            {
                theLedger = theTempLedger.Where(x => x.Accounting_TypeFK == accountingId).OrderBy(x => x.Date).ToList();
                drIncrease = theLedger.Where(x => x.Accounting_TypeFK == accountingId).Select(x => x.DrIncrease).FirstOrDefault();
            }
            else if (accountTypeEnum == AccountTypeEnum.C1)
            {
                theLedger = theTempLedger.Where(x => x.Accounting_Chart1FK == accountingId).OrderBy(x => x.Date).ToList();
                drIncrease = theLedger.Where(x => x.Accounting_Chart1FK == accountingId).Select(x => x.DrIncrease).FirstOrDefault();
            }
            else if (accountTypeEnum == AccountTypeEnum.C2)
            {
                theLedger = theTempLedger.Where(x => x.Accounting_Chart2FK == accountingId).OrderBy(x => x.Date).ToList();
                drIncrease = theLedger.Where(x => x.Accounting_Chart2FK == accountingId).Select(x => x.DrIncrease).FirstOrDefault();
            }
            else
            {
                theLedger = theTempLedger.Where(x => x.Accounting_HeadId == accountingId).OrderBy(x => x.Date).ToList();
                accountingHeadName = _db.Accounting_Head.Where(x => x.ID == accountingId).Select(x => x.Name).FirstOrDefault();
                drIncrease = theLedger.Where(x => x.Accounting_HeadId == accountingId).Select(x => x.DrIncrease).FirstOrDefault();
            }
            int serial = sl;
            // bool drIncrease = GetHeadDrIncreaseOrDecreaseType(accountingId, accountTypeEnum);
            if (drIncrease)
            {
                if (theLedger.Count > 0)
                {
                    foreach (VMLedger entry in theLedger)
                    {
                        entry.Sl = ++serial;
                        entry.ClosingBalance = entry.Debit - entry.Credit + closingBalance;
                        closingBalance = entry.ClosingBalance;
                        entry.ClosingBalanceDr = entry.ClosingBalance;
                        //previous
                        //entry.Balance = closingBalance - openingBal;
                        //updated By BONO
                        entry.Balance = entry.ClosingBalance;
                    }
                }
                else
                {
                    VMLedger ledger = new VMLedger();
                    ledger.ClosingBalance = closingBalance;
                    ledger.Balance = 0;
                    //ledger.Balance = closingBalance;
                    ledger.Accounting_HeadName = accountingHeadName;
                    theLedger.Add(ledger);
                }
            }
            else
            {
                if (theLedger.Count > 0)
                {
                    foreach (VMLedger entry in theLedger)
                    {
                        //In this block
                        entry.Sl = ++serial;
                        //entry.ClosingBalance = entry.Credit - entry.Debit + closingBalance;
                        entry.ClosingBalance = entry.Debit - entry.Credit + closingBalance;
                        closingBalance = entry.ClosingBalance;
                        entry.ClosingBalanceCr = entry.ClosingBalance;
                        //previous
                        //entry.Balance = openingBal - closingBalance;
                        //updated By BONO
                        entry.Balance = entry.ClosingBalance;

                        //-----------
                    }
                }
                else
                {
                    VMLedger ledger = new VMLedger();
                    ledger.ClosingBalance = closingBalance;
                    ledger.Balance = 0;
                    // ledger.Balance = closingBalance;
                    ledger.Accounting_HeadName = accountingHeadName;
                    theLedger.Add(ledger);
                }
            }
            return (theLedger);
        }

        private (decimal balance, decimal balanceDr, decimal balanceCr, int sl) GetOpeningBlanceNew(int accountingId, AccountTypeEnum accountTypeEnum, DateTime fromDate)
        {
            IEnumerable<VMLedger> theLedger = GetTheLedger(accountingId, accountTypeEnum, DateTime.MinValue, fromDate.AddDays(-1), 0, 0);
            //var theLedger = GetTheLedger(accountingId, accountTypeEnum, DateTime.MinValue, fromDate.AddDays(-1), 0, 0);
            decimal openingBalance = 0;
            decimal openingBalanceDr = 0;
            decimal openingBalanceCr = 0;
            int cc = theLedger.Count();
            decimal dr = theLedger.Select(x => x.Debit).Sum();
            decimal cr = theLedger.Select(x => x.Credit).Sum();
            bool drIncrease = GetHeadDrIncreaseOrDecreaseType(accountingId, accountTypeEnum);
            if (drIncrease)
            {
                openingBalance = dr - cr;
                openingBalanceDr = openingBalance;
            }
            else
            {
                //previous
                //openingBalance = cr - dr;

                //changing for credit minus value (need to under observation) by BONO
                openingBalance = dr - cr;
                openingBalanceCr = openingBalance;
            }
            return (openingBalance, openingBalanceDr, openingBalanceCr, theLedger.Count());
        }

        private bool GetHeadDrIncreaseOrDecreaseType(int accountingId, AccountTypeEnum accountTypeEnum)
        {
            bool value = false;
            if (accountTypeEnum == AccountTypeEnum.Type)
            {
                // value = journalHistory.Where(x=>x.Accounting_TypeFK == accountingId).Select(x => x.DrIncrease).FirstOrDefault();

                value = (from t6 in _db.Accounting_Type
                         select new
                         {
                             t6.ID,
                             t6.DrIncrease
                         }).Where(x => x.ID == accountingId).Select(x => x.DrIncrease).FirstOrDefault();

            }
            else if (accountTypeEnum == AccountTypeEnum.C1)
            {
                //  value = journalHistory.Where(x => x.Accounting_Chart1FK == accountingId).Select(x => x.DrIncrease).FirstOrDefault();

                value = (from t5 in _db.Accounting_Chart1
                         join t6 in _db.Accounting_Type on t5.Accounting_TypeFK equals t6.ID
                         select new
                         {
                             t5.ID,
                             t6.DrIncrease
                         }).Where(x => x.ID == accountingId).Select(x => x.DrIncrease).FirstOrDefault();
            }
            else if (accountTypeEnum == AccountTypeEnum.C2)
            {
                //value = journalHistory.Where(x => x.Accounting_Chart2FK == accountingId).Select(x => x.DrIncrease).FirstOrDefault();

                value = (from t4 in _db.Accounting_Chart2
                         join t5 in _db.Accounting_Chart1 on t4.Accounting_Chart1FK equals t5.ID
                         join t6 in _db.Accounting_Type on t5.Accounting_TypeFK equals t6.ID
                         select new
                         {
                             t4.ID,
                             t6.DrIncrease
                         }).Where(x => x.ID == accountingId).Select(x => x.DrIncrease).FirstOrDefault();
            }
            else
            {
                // value = journalHistory.Where(x => x.Accounting_HeadId == accountingId).Select(x => x.DrIncrease).FirstOrDefault();

                value = (from t3 in _db.Accounting_Head
                         join t4 in _db.Accounting_Chart2 on t3.Accounting_Chart2FK equals t4.ID
                         join t5 in _db.Accounting_Chart1 on t4.Accounting_Chart1FK equals t5.ID
                         join t6 in _db.Accounting_Type on t5.Accounting_TypeFK equals t6.ID
                         select new
                         {
                             t3.ID,
                             t6.DrIncrease
                         }).Where(x => x.ID == accountingId).Select(x => x.DrIncrease).FirstOrDefault();
            }
            return value;
        }

        private VMCogs GetCostofGoodsSoldInfo(DateTime fromdate, DateTime todate)
        {
            //var (rawOpening, rawClosing) = GetMaterialsInventoryValue(InventoryTypeEnum.Raw, fromdate, todate);
            //var (wipOpening, wipClosing) = GetMaterialsInventoryValue(InventoryTypeEnum.Wip, fromdate, todate);
            //var (finishedOpening, finishedClosing) = GetMaterialsInventoryValue(InventoryTypeEnum.Finished, fromdate, todate);

            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) rawInventory = GetTrialBalanceInfo(74, AccountTypeEnum.Head, fromdate, todate);
            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) wipInventory = GetTrialBalanceInfo(75, AccountTypeEnum.Head, fromdate, todate);
            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) finInventory = GetTrialBalanceInfo(76, AccountTypeEnum.Head, fromdate, todate);

            //HardCoded ID Should Go
            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) rawMaterialsPurchaseDetails = GetTrialBalanceInfo(16, AccountTypeEnum.C1, fromdate, todate);
            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) factoryOverhead = GetTrialBalanceInfo(12, AccountTypeEnum.C1, fromdate, todate);
            //---------------------
            VMCogs vMCogs = new VMCogs
            {
                RawMaterialOpeningStock = rawInventory.openingBalance,
                RawMaterialPurchase = rawInventory.dr,
                RawMaterialClosing = rawInventory.closingBalance //vMCogs.RawMaterialOpeningStock + vMCogs.RawMaterialPurchase;// as per direction of farhad bhai---rawInventory.closingBalance;
            };
            vMCogs.RawMaterialConsumed = vMCogs.RawMaterialOpeningStock + vMCogs.RawMaterialPurchase - vMCogs.RawMaterialClosing;
            vMCogs.FactoryOverhead = factoryOverhead.Balance;//factoryOverhead.closingBalance - factoryOverhead.openingBalance;//factoryOverhead.dr-factoryOverhead.cr; //// as per direction of farhad bhai--- factoryOverhead.openingBalance - fac
            //vMCogs.RawMaterialPurchase = rawMaterialsPurchaseDetails.closingBalance; // as per direction of farhad bhai--- rawMaterialsPurchaseDetails.openingBalance - rawMaterialsPurchaseDetails.closingBalance;
            //vMCogs.RawMaterialClosing = 0; // as per direction of farhad bhai---rawInventory.closingBalance;
            //vMCogs.RawMaterialConsumed = 0;//vMCogs.RawMaterialOpeningStock + vMCogs.RawMaterialPurchase - vMCogs.RawMaterialClosing;
            //vMCogs.FactoryOverhead = factoryOverhead.closingBalance; //// as per direction of farhad bhai--- factoryOverhead.openingBalance - factoryOverhead.closingBalance;
            vMCogs.CostofGoodsManufactured = vMCogs.RawMaterialConsumed + vMCogs.FactoryOverhead;
            vMCogs.OpeningStockWorkinProcess = wipInventory.openingBalance;
            vMCogs.ClosingStockWorkinProcess = wipInventory.closingBalance;
            vMCogs.CostofGoodsAvaiableforSales = wipInventory.openingBalance + vMCogs.CostofGoodsManufactured - vMCogs.ClosingStockWorkinProcess;

            vMCogs.OpeningStockofFinishGoods = finInventory.openingBalance;
            vMCogs.ClosingStockofFinishGoods = finInventory.closingBalance;
            vMCogs.CostofGoodsSold = vMCogs.CostofGoodsAvaiableforSales + vMCogs.OpeningStockofFinishGoods - vMCogs.ClosingStockofFinishGoods;
            // vMCogs.CostofGoodsSold = vMCogs.CostofGoodsManufactured + vMCogs.CostofGoodsAvaiableforSales + vMCogs.OpeningStockofFinishGoods - vMCogs.ClosingStockofFinishGoods;

            return vMCogs;
        }
        private VMIncomeStatement GetComprehensiveIncomeInfo(DateTime fromdate, DateTime todate)
        {

            //HardCoded ID Should Go
            //var accumulateddepreciation = GetTrialBalanceInfo(1329, AccountTypeEnum.C2, fromdate, todate);
            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) accumulateddepreciation = GetTrialBalanceInfo(1294, AccountTypeEnum.C2, fromdate, todate);
            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) exportsaleIncome = GetTrialBalanceInfo(1306, AccountTypeEnum.C2, fromdate, todate);
            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) subcontractIncome = GetTrialBalanceInfo(1307, AccountTypeEnum.C2, fromdate, todate);
            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) othercomprehensiveIncome = GetTrialBalanceInfo(11, AccountTypeEnum.C1, fromdate, todate);

            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) administrativeExpense = GetTrialBalanceInfo(13, AccountTypeEnum.C1, fromdate, todate);
            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) sellinganddistributionexpense = GetTrialBalanceInfo(14, AccountTypeEnum.C1, fromdate, todate);
            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) financialexpense = GetTrialBalanceInfo(15, AccountTypeEnum.C1, fromdate, todate);
            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) factoryoverhead = GetTrialBalanceInfo(12, AccountTypeEnum.C1, fromdate, todate);
            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) incometaxexpense = GetTrialBalanceInfo(1409, AccountTypeEnum.C2, fromdate, todate);

            VMCogs costofGoodsSold = GetCostofGoodsSoldInfo(fromdate, todate);
            //---------------------
            VMIncomeStatement vMIncomeStatement = new VMIncomeStatement
            {
                //previous
                ExportSales = exportsaleIncome.Balance * -1,
                SubContractIncome = subcontractIncome.Balance * -1
                //changes by BONO
                //ExportSales = exportsaleIncome.Balance,
                //SubContractIncome = subcontractIncome.Balance
            };
            vMIncomeStatement.TotalRevenueIncome = vMIncomeStatement.ExportSales + vMIncomeStatement.SubContractIncome;
            //previous
            vMIncomeStatement.OtherComprehensiveIncome = othercomprehensiveIncome.Balance * -1;
            //added by BONO
            //vMIncomeStatement.OtherComprehensiveIncome = othercomprehensiveIncome.Balance;
            //vMIncomeStatement.TotalRevenueIncome = vMIncomeStatement.ExportSales + vMIncomeStatement.SubContractIncome + vMIncomeStatement.OtherComprehensiveIncome;
            vMIncomeStatement.CostofGoodsSold = costofGoodsSold.CostofGoodsSold;


            vMIncomeStatement.GrossProfit = vMIncomeStatement.TotalRevenueIncome - vMIncomeStatement.CostofGoodsSold; // GetCalculatedFieldResult(vMIncomeStatement.TotalRevenueIncome, vMIncomeStatement.CostofGoodsSold);
            //vMIncomeStatement.OperatingExpense = 0;
            vMIncomeStatement.AdministrativeExpense = administrativeExpense.Balance;
            vMIncomeStatement.SellingAndDistribuionExpense = sellinganddistributionexpense.Balance;
            vMIncomeStatement.TotalOperatingExpense = vMIncomeStatement.AdministrativeExpense + vMIncomeStatement.SellingAndDistribuionExpense;
            vMIncomeStatement.OperatingProfit = vMIncomeStatement.GrossProfit - vMIncomeStatement.TotalOperatingExpense;
            //vMIncomeStatement
            vMIncomeStatement.FinancialExpense = financialexpense.Balance;//financialexpense.closingBalance;
            // vMIncomeStatement.ProfitBeforeTax = vMIncomeStatement.OperatingProfit;
            // vMIncomeStatement.IncomeTaxExpense = incometaxexpense.closingBalance;
            //vMIncomeStatement.AccumulatedDepreciation = accumulateddepreciation.closingBalance;
            vMIncomeStatement.ProfitfortheYearafterTax = vMIncomeStatement.OperatingProfit - vMIncomeStatement.FinancialExpense;
            vMIncomeStatement.TotalComprehensiveIncome = vMIncomeStatement.ProfitfortheYearafterTax + vMIncomeStatement.OtherComprehensiveIncome;
            return vMIncomeStatement;
        }

        private VMIncomeStatement GetComprehensiveIncomeInforFinancialPosition(DateTime fromdate, DateTime todate)
        {

            //HardCoded ID Should Go
            //var accumulateddepreciation = GetTrialBalanceInfo(1329, AccountTypeEnum.C2, fromdate, todate);
            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) accumulateddepreciation = GetTrialBalanceInfo(1294, AccountTypeEnum.C2, fromdate, todate);
            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) exportsaleIncome = GetTrialBalanceInfo(1306, AccountTypeEnum.C2, fromdate, todate);
            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) subcontractIncome = GetTrialBalanceInfo(1307, AccountTypeEnum.C2, fromdate, todate);
            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) othercomprehensiveIncome = GetTrialBalanceInfo(11, AccountTypeEnum.C1, fromdate, todate);

            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) administrativeExpense = GetTrialBalanceInfo(13, AccountTypeEnum.C1, fromdate, todate);
            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) sellinganddistributionexpense = GetTrialBalanceInfo(14, AccountTypeEnum.C1, fromdate, todate);
            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) financialexpense = GetTrialBalanceInfo(15, AccountTypeEnum.C1, fromdate, todate);
            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) factoryoverhead = GetTrialBalanceInfo(12, AccountTypeEnum.C1, fromdate, todate);
            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) incometaxexpense = GetTrialBalanceInfo(1409, AccountTypeEnum.C2, fromdate, todate);

            VMCogs costofGoodsSold = GetCostofGoodsSoldInfo(fromdate, todate);
            //---------------------
            VMIncomeStatement vMIncomeStatement = new VMIncomeStatement
            {
                ExportSales = exportsaleIncome.closingBalance,
                SubContractIncome = subcontractIncome.closingBalance
            };
            vMIncomeStatement.TotalRevenueIncome = vMIncomeStatement.ExportSales + vMIncomeStatement.SubContractIncome;
            vMIncomeStatement.OtherComprehensiveIncome = othercomprehensiveIncome.closingBalance;
            //vMIncomeStatement.TotalRevenueIncome = vMIncomeStatement.ExportSales + vMIncomeStatement.SubContractIncome + vMIncomeStatement.OtherComprehensiveIncome;
            vMIncomeStatement.CostofGoodsSold = costofGoodsSold.CostofGoodsSold;


            vMIncomeStatement.GrossProfit = vMIncomeStatement.TotalRevenueIncome - vMIncomeStatement.CostofGoodsSold; // GetCalculatedFieldResult(vMIncomeStatement.TotalRevenueIncome, vMIncomeStatement.CostofGoodsSold);
            //vMIncomeStatement.OperatingExpense = 0;
            vMIncomeStatement.AdministrativeExpense = administrativeExpense.closingBalance;
            vMIncomeStatement.SellingAndDistribuionExpense = sellinganddistributionexpense.closingBalance;
            vMIncomeStatement.TotalOperatingExpense = vMIncomeStatement.AdministrativeExpense + vMIncomeStatement.SellingAndDistribuionExpense;
            vMIncomeStatement.OperatingProfit = vMIncomeStatement.GrossProfit - vMIncomeStatement.TotalOperatingExpense;
            //vMIncomeStatement
            vMIncomeStatement.FinancialExpense = financialexpense.closingBalance;//financialexpense.closingBalance;
            // vMIncomeStatement.ProfitBeforeTax = vMIncomeStatement.OperatingProfit;
            // vMIncomeStatement.IncomeTaxExpense = incometaxexpense.closingBalance;
            //vMIncomeStatement.AccumulatedDepreciation = accumulateddepreciation.closingBalance;
            vMIncomeStatement.ProfitfortheYearafterTax = vMIncomeStatement.OperatingProfit - vMIncomeStatement.FinancialExpense;
            vMIncomeStatement.TotalComprehensiveIncome = vMIncomeStatement.ProfitfortheYearafterTax + vMIncomeStatement.OtherComprehensiveIncome;
            return vMIncomeStatement;
        }

        private VMFinancialPosition GetFinanCialPositionInfo(DateTime fromdate, DateTime todate)
        {

            //HardCoded ID Should Go
            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) propertyplantandequipment = GetTrialBalanceInfo(3, AccountTypeEnum.C1, fromdate, todate);
            //var accumulateddepreciation = GetTrialBalanceInfo(1329, AccountTypeEnum.C2, fromdate, todate);
            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) accumulateddepreciation = GetTrialBalanceInfo(1294, AccountTypeEnum.C2, fromdate, todate);
            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) accountreceivable = GetTrialBalanceInfo(1289, AccountTypeEnum.C2, fromdate, todate);
            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) inventory = GetTrialBalanceInfo(1290, AccountTypeEnum.C2, fromdate, todate);
            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) advancedepositandprepayment = GetTrialBalanceInfo(1291, AccountTypeEnum.C2, fromdate, todate);
            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) cashinhand = GetTrialBalanceInfo(1287, AccountTypeEnum.C2, fromdate, todate);
            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) cashatbank = GetTrialBalanceInfo(1288, AccountTypeEnum.C2, fromdate, todate);
            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) othercurrentasset = GetTrialBalanceInfo(1292, AccountTypeEnum.C2, fromdate, todate);
            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) sharecapital = GetTrialBalanceInfo(6, AccountTypeEnum.C1, fromdate, todate);
            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) generalreserve = GetTrialBalanceInfo(1295, AccountTypeEnum.C2, fromdate, todate);
            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) revaluationreserve = GetTrialBalanceInfo(1296, AccountTypeEnum.C2, fromdate, todate);
            //var retainedearnings = GetComprehensiveIncomeInfo(fromdate, todate).NetProfit;
            decimal retainedearnings = GetComprehensiveIncomeInforFinancialPosition(fromdate, todate).TotalComprehensiveIncome;
            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) longtermloan = GetTrialBalanceInfo(8, AccountTypeEnum.C1, fromdate, todate);
            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) accountpayabletrade = GetTrialBalanceInfo(1300, AccountTypeEnum.C2, fromdate, todate);
            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) accountpayableother = GetTrialBalanceInfo(1301, AccountTypeEnum.C2, fromdate, todate);
            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) shorttermloan = GetTrialBalanceInfo(1304, AccountTypeEnum.C2, fromdate, todate);
            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) acceptedbillliabilities = GetTrialBalanceInfo(1302, AccountTypeEnum.C2, fromdate, todate);
            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) provisionforexpense = GetTrialBalanceInfo(1303, AccountTypeEnum.C2, fromdate, todate);
            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) shorttermliabilities = GetTrialBalanceInfo(1305, AccountTypeEnum.C2, fromdate, todate);
            //---------------------
            VMFinancialPosition vMFinancialPosition = new VMFinancialPosition
            {
                PropertyPlantandEquipment = propertyplantandequipment.closingBalance,
                AccumulatedDepreciation = accumulateddepreciation.closingBalance
            };
            vMFinancialPosition.TotalFixedAsset = vMFinancialPosition.PropertyPlantandEquipment; //vMFinancialPosition.PropertyPlantandEquipment- vMFinancialPosition.AccumulatedDepreciation;
            vMFinancialPosition.AccountReceivable = accountreceivable.closingBalance;
            vMFinancialPosition.Inventory = inventory.closingBalance;
            vMFinancialPosition.AdvanceDepositandPrePayments = advancedepositandprepayment.closingBalance;
            vMFinancialPosition.CashinHand = cashinhand.closingBalance;
            vMFinancialPosition.CashatBank = cashatbank.closingBalance;
            vMFinancialPosition.OthersCurrentAssets = othercurrentasset.closingBalance;
            vMFinancialPosition.TotalCurrentAssets = vMFinancialPosition.AccountReceivable + vMFinancialPosition.Inventory + vMFinancialPosition.AdvanceDepositandPrePayments + vMFinancialPosition.CashinHand + vMFinancialPosition.CashatBank + vMFinancialPosition.OthersCurrentAssets;
            vMFinancialPosition.TotalAssets = vMFinancialPosition.TotalFixedAsset + vMFinancialPosition.TotalCurrentAssets;

            vMFinancialPosition.ShareCapital = sharecapital.closingBalance;
            vMFinancialPosition.GeneralReserve = generalreserve.closingBalance;
            vMFinancialPosition.RevaluationReserve = revaluationreserve.closingBalance;
            vMFinancialPosition.RetainedEarnings = retainedearnings;
            vMFinancialPosition.TotalEquity = vMFinancialPosition.ShareCapital + vMFinancialPosition.GeneralReserve + vMFinancialPosition.RevaluationReserve + vMFinancialPosition.RetainedEarnings + vMFinancialPosition.AccumulatedDepreciation;

            vMFinancialPosition.LongTermLoan = longtermloan.closingBalance;
            vMFinancialPosition.TotalLongTermLiabilities = vMFinancialPosition.LongTermLoan;

            vMFinancialPosition.AccountPayable = (accountpayabletrade.closingBalance) + (accountpayableother.closingBalance);
            vMFinancialPosition.ShortTermLoan = shorttermloan.closingBalance;
            vMFinancialPosition.ShortTermliabilities = shorttermliabilities.closingBalance;
            vMFinancialPosition.AcceptedBillLiabilities = acceptedbillliabilities.closingBalance;
            vMFinancialPosition.ProvisionforExpense = provisionforexpense.closingBalance;
            vMFinancialPosition.TotalCurrentLiabilities = vMFinancialPosition.AccountPayable + vMFinancialPosition.ShortTermLoan + vMFinancialPosition.AcceptedBillLiabilities + vMFinancialPosition.ProvisionforExpense + vMFinancialPosition.ShortTermliabilities;
            vMFinancialPosition.TotalEquityandLiabilities = vMFinancialPosition.TotalEquity + vMFinancialPosition.TotalCurrentLiabilities + vMFinancialPosition.TotalLongTermLiabilities;

            return vMFinancialPosition;
        }


        private VMCashFlow GetCashFlowInfo(DateTime fromdate, DateTime todate)
        {

            //HardCoded ID Should Go
            //net profit befor tax
            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) netprofitbeforetax = GetTrialBalanceInfo(1305, AccountTypeEnum.C2, fromdate, todate);
            // depreciation  is ok now, but here comes also administrative expenses depreciation head when created
            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) depreciation = GetTrialBalanceInfo(1329, AccountTypeEnum.C2, fromdate, todate);

            // start current assets
            // account receive able, advance deposit and prepayment, inventory and others current assets
            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) accountsreceivable = GetTrialBalanceInfo(1289, AccountTypeEnum.C2, fromdate, todate);
            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) inventory = GetTrialBalanceInfo(1290, AccountTypeEnum.C2, fromdate, todate);
            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) advancedepositandprepayment = GetTrialBalanceInfo(1291, AccountTypeEnum.C2, fromdate, todate);
            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) othercurrentasset = GetTrialBalanceInfo(1292, AccountTypeEnum.C2, fromdate, todate);

            // start current liabilities
            // accepted bill liabilities UD No 123, Account payable trade, Account payable Others, provision for expenses, short term liabilities and short term loan from individuals 
            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) accountpayabletrade = GetTrialBalanceInfo(1300, AccountTypeEnum.C2, fromdate, todate);
            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) accountpayableother = GetTrialBalanceInfo(1301, AccountTypeEnum.C2, fromdate, todate);
            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) acceptedbillliabilities = GetTrialBalanceInfo(1302, AccountTypeEnum.C2, fromdate, todate);
            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) provisionforexpense = GetTrialBalanceInfo(1303, AccountTypeEnum.C2, fromdate, todate);
            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) shorttermloan = GetTrialBalanceInfo(1304, AccountTypeEnum.C2, fromdate, todate);
            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) shorttermliabilities = GetTrialBalanceInfo(1305, AccountTypeEnum.C2, fromdate, todate);


            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) incometaxexpense = GetTrialBalanceInfo(1409, AccountTypeEnum.C2, fromdate, todate);
            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) propertyplantandequipment = GetTrialBalanceInfo(3, AccountTypeEnum.C1, fromdate, todate);
            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) longtermloan = GetTrialBalanceInfo(8, AccountTypeEnum.C1, fromdate, todate);
            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) cashinhand = GetTrialBalanceInfo(1287, AccountTypeEnum.C2, fromdate, todate);
            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) cashatbank = GetTrialBalanceInfo(1288, AccountTypeEnum.C2, fromdate, todate);

            //var cashinhand = GetTrialBalanceInfo(1287, AccountTypeEnum.C2, fromdate, todate);
            //var cashatbank = GetTrialBalanceInfo(1288, AccountTypeEnum.C2, fromdate, todate);

            //---------------------
            VMCashFlow vMCashFlow = new VMCashFlow
            {
                ProfitfortheYearafterTax = GetComprehensiveIncomeInfo(fromdate, todate).TotalComprehensiveIncome,
                // NetProfitBeforeTax = GetComprehensiveIncomeInfo(fromdate, todate).ProfitfortheYearafterTax,
                Depreciation = depreciation.Balance,//depreciation.closingBalance, "change by as per direction of Shahinoor Bhai Romo Accountant"
                AccountsReceivable = accountsreceivable.openingBalance - accountsreceivable.closingBalance,//accountsreceivable.closingBalance, "change by as per direction of Shahinoor Bhai Romo Accountant"
                Inventory = inventory.openingBalance - inventory.closingBalance,//inventory.closingBalance, "change by as per direction of Shahinoor Bhai Romo Accountant"
                AdvanceDepositandPrePayment = advancedepositandprepayment.openingBalance - advancedepositandprepayment.closingBalance, // advancedepositandprepayment.closingBalance, "change by as per direction of Shahinoor Bhai Romo Accountant"
                OthersCurrentAssets = othercurrentasset.openingBalance - othercurrentasset.closingBalance,//othercurrentasset.closingBalance, "change by as per direction of Shahinoor Bhai Romo Accountant"
                AccountsPayable = (accountpayabletrade.closingBalance - accountpayabletrade.openingBalance) + (accountpayableother.closingBalance - accountpayableother.openingBalance), //accountpayabletrade.closingBalance + accountpayableother.closingBalance,
                ShortTermLoan = shorttermloan.closingBalance - shorttermloan.openingBalance,
                ShortTermLiabilities = shorttermliabilities.closingBalance - shorttermliabilities.openingBalance,//shorttermloan.closingBalance, "change by as per direction of Shahinoor Bhai Romo Accountant"
                AcceptedBillLiabilities = acceptedbillliabilities.closingBalance - acceptedbillliabilities.openingBalance,//acceptedbillliabilities.closingBalance,  "change by as per direction of Shahinoor Bhai Romo Accountant"
                ProvisionforExpenses = provisionforexpense.closingBalance - provisionforexpense.openingBalance //provisionforexpense.closingBalance "change by as per direction of Shahinoor Bhai Romo Accountant"
            };
            vMCashFlow.TotalChangesInWorkingCapital = vMCashFlow.AccountsReceivable + vMCashFlow.Inventory + vMCashFlow.AdvanceDepositandPrePayment + vMCashFlow.OthersCurrentAssets + vMCashFlow.AccountsPayable + vMCashFlow.ShortTermLoan + vMCashFlow.ShortTermLiabilities + vMCashFlow.AcceptedBillLiabilities + vMCashFlow.ProvisionforExpenses;
            //vMCashFlow.IncomeTaxPaid = incometaxexpense.Balance; //incometaxexpense.closingBalance; "change by as per direction of Shahinoor Bhai Romo Accountant"
            vMCashFlow.NetCashFromOperatingActivities = vMCashFlow.ProfitfortheYearafterTax + vMCashFlow.TotalChangesInWorkingCapital + vMCashFlow.Depreciation;

            vMCashFlow.AcquisitionofPropertyPlantandEquipment = propertyplantandequipment.openingBalance - propertyplantandequipment.closingBalance; //propertyplantandequipment.closingBalance; "change by as per direction of Shahinoor Bhai Romo Accountant"
            vMCashFlow.NetCashUsedinInvestingActivities = vMCashFlow.AcquisitionofPropertyPlantandEquipment;

            vMCashFlow.LongTermLoan = longtermloan.Balance;//longtermloan.closingBalance; "change by as per direction of Shahinoor Bhai Romo Accountant"
            vMCashFlow.NetCashFromFinancingActivities = vMCashFlow.LongTermLoan;
            vMCashFlow.NetChangesinCashandCashEquivalents = vMCashFlow.NetCashFromOperatingActivities + vMCashFlow.NetCashUsedinInvestingActivities + vMCashFlow.NetCashFromFinancingActivities;

            vMCashFlow.OpeningCashandCashEquivalents = cashinhand.openingBalance + cashatbank.openingBalance; //cashinhand.openingBalance + cashatbank.openingBalance; "change by as per direction of Shahinoor Bhai Romo Accountant"
            vMCashFlow.ClosingCashandCashEquivalents = vMCashFlow.NetChangesinCashandCashEquivalents + vMCashFlow.OpeningCashandCashEquivalents;
            // cashatbank.closingBalance + cashinhand.closingBalance;
            //vMCashFlow.NetChangesinCashandCashEquivalents + vMCashFlow.OpeningCashandCashEquivalents;
            // vMCashFlow.ClosingCashandCashEquivalents = cashinhand.closingBalance + cashatbank.closingBalance;
            return vMCashFlow;
        }

        private VMCashFlow GetCashFlowInfoOLD(DateTime fromdate, DateTime todate)
        {

            //HardCoded ID Should Go
            //net profit befor tax
            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) netprofitbeforetax = GetTrialBalanceInfo(1305, AccountTypeEnum.C2, fromdate, todate);
            // depreciation  is ok now, but here comes also administrative expenses depreciation head when created
            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) depreciation = GetTrialBalanceInfo(1329, AccountTypeEnum.C2, fromdate, todate);

            // start current assets
            // account receive able, advance deposit and prepayment, inventory and others current assets
            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) accountsreceivable = GetTrialBalanceInfo(1289, AccountTypeEnum.C2, fromdate, todate);
            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) inventory = GetTrialBalanceInfo(1290, AccountTypeEnum.C2, fromdate, todate);
            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) advancedepositandprepayment = GetTrialBalanceInfo(1291, AccountTypeEnum.C2, fromdate, todate);
            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) othercurrentasset = GetTrialBalanceInfo(1292, AccountTypeEnum.C2, fromdate, todate);

            // start current liabilities
            // accepted bill liabilities UD No 123, Account payable trade, Account payable Others, provision for expenses, short term liabilities and short term loan from individuals 
            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) accountpayabletrade = GetTrialBalanceInfo(1300, AccountTypeEnum.C2, fromdate, todate);
            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) accountpayableother = GetTrialBalanceInfo(1301, AccountTypeEnum.C2, fromdate, todate);
            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) acceptedbillliabilities = GetTrialBalanceInfo(1302, AccountTypeEnum.C2, fromdate, todate);
            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) provisionforexpense = GetTrialBalanceInfo(1303, AccountTypeEnum.C2, fromdate, todate);
            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) shorttermloan = GetTrialBalanceInfo(1304, AccountTypeEnum.C2, fromdate, todate);
            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) shorttermliabilities = GetTrialBalanceInfo(1305, AccountTypeEnum.C2, fromdate, todate);


            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) incometaxexpense = GetTrialBalanceInfo(1409, AccountTypeEnum.C2, fromdate, todate);
            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) propertyplantandequipment = GetTrialBalanceInfo(3, AccountTypeEnum.C1, fromdate, todate);
            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) longtermloan = GetTrialBalanceInfo(8, AccountTypeEnum.C1, fromdate, todate);
            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) cashinhand = GetTrialBalanceInfo(1287, AccountTypeEnum.C2, fromdate, todate);
            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) cashatbank = GetTrialBalanceInfo(1288, AccountTypeEnum.C2, fromdate, todate);

            //var cashinhand = GetTrialBalanceInfo(1287, AccountTypeEnum.C2, fromdate, todate);
            //var cashatbank = GetTrialBalanceInfo(1288, AccountTypeEnum.C2, fromdate, todate);

            //---------------------
            VMCashFlow vMCashFlow = new VMCashFlow
            {
                // vMCashFlow.NetProfitBeforeTax = GetComprehensiveIncomeInfo(fromdate, todate).ProfitBeforeTax;
                ProfitfortheYearafterTax = GetComprehensiveIncomeInfo(fromdate, todate).TotalComprehensiveIncome,
                NetProfitBeforeTax = GetComprehensiveIncomeInfo(fromdate, todate).ProfitfortheYearafterTax,
                Depreciation = depreciation.Balance,//depreciation.closingBalance, "change by as per direction of Shahinoor Bhai Romo Accountant"
                AccountsReceivable = accountsreceivable.Balance,//accountsreceivable.closingBalance, "change by as per direction of Shahinoor Bhai Romo Accountant"
                Inventory = inventory.Balance,//inventory.closingBalance, "change by as per direction of Shahinoor Bhai Romo Accountant"
                AdvanceDepositandPrePayment = advancedepositandprepayment.Balance, // advancedepositandprepayment.closingBalance, "change by as per direction of Shahinoor Bhai Romo Accountant"
                OthersCurrentAssets = othercurrentasset.Balance,//othercurrentasset.closingBalance, "change by as per direction of Shahinoor Bhai Romo Accountant"
                AccountsPayable = accountpayabletrade.Balance + accountpayableother.Balance, //accountpayabletrade.closingBalance + accountpayableother.closingBalance,
                ShortTermLoan = shorttermloan.Balance,//shorttermloan.closingBalance, "change by as per direction of Shahinoor Bhai Romo Accountant"
                AcceptedBillLiabilities = acceptedbillliabilities.Balance,//acceptedbillliabilities.closingBalance,  "change by as per direction of Shahinoor Bhai Romo Accountant"
                ProvisionforExpenses = provisionforexpense.Balance //provisionforexpense.closingBalance "change by as per direction of Shahinoor Bhai Romo Accountant"
            };
            vMCashFlow.TotalChangesInWorkingCapital = vMCashFlow.AccountsPayable + vMCashFlow.Inventory + vMCashFlow.AdvanceDepositandPrePayment + vMCashFlow.OthersCurrentAssets + vMCashFlow.AccountsPayable + vMCashFlow.ShortTermLoan + vMCashFlow.AcceptedBillLiabilities + vMCashFlow.ProvisionforExpenses;
            vMCashFlow.IncomeTaxPaid = incometaxexpense.Balance; //incometaxexpense.closingBalance; "change by as per direction of Shahinoor Bhai Romo Accountant"
            vMCashFlow.NetCashFromOperatingActivities = vMCashFlow.ProfitfortheYearafterTax + vMCashFlow.IncomeTaxPaid + vMCashFlow.TotalChangesInWorkingCapital;
            //vMCashFlow.ProfitfortheYearafterTax + vMCashFlow.Depreciation + vMCashFlow.IncomeTaxPaid + vMCashFlow.TotalChangesInWorkingCapital;
            //vMCashFlow.NetCashFromOperatingActivities = vMCashFlow.NetProfitBeforeTax + vMCashFlow.Depreciation + vMCashFlow.AccountsReceivable + vMCashFlow.Inventory + vMCashFlow.AdvanceDepositandPrePayment + vMCashFlow.OthersCurrentAssets + vMCashFlow.AccountsPayable + vMCashFlow.ShortTermLoan + vMCashFlow.AcceptedBillLiabilities + vMCashFlow.ProvisionforExpenses + vMCashFlow.IncomeTaxPaid;

            vMCashFlow.AcquisitionofPropertyPlantandEquipment = propertyplantandequipment.Balance; //propertyplantandequipment.closingBalance; "change by as per direction of Shahinoor Bhai Romo Accountant"
            vMCashFlow.NetCashUsedinInvestingActivities = vMCashFlow.AcquisitionofPropertyPlantandEquipment;

            vMCashFlow.LongTermLoan = longtermloan.Balance;//longtermloan.closingBalance; "change by as per direction of Shahinoor Bhai Romo Accountant"
            vMCashFlow.NetCashFromFinancingActivities = vMCashFlow.LongTermLoan;
            vMCashFlow.NetChangesinCashandCashEquivalents = vMCashFlow.NetCashFromOperatingActivities + vMCashFlow.NetCashUsedinInvestingActivities + vMCashFlow.NetCashFromFinancingActivities;

            vMCashFlow.OpeningCashandCashEquivalents = cashinhand.Balance + cashatbank.Balance; //cashinhand.openingBalance + cashatbank.openingBalance; "change by as per direction of Shahinoor Bhai Romo Accountant"
            vMCashFlow.ClosingCashandCashEquivalents = vMCashFlow.NetChangesinCashandCashEquivalents + vMCashFlow.OpeningCashandCashEquivalents;
            // cashatbank.closingBalance + cashinhand.closingBalance;
            //vMCashFlow.NetChangesinCashandCashEquivalents + vMCashFlow.OpeningCashandCashEquivalents;
            // vMCashFlow.ClosingCashandCashEquivalents = cashinhand.closingBalance + cashatbank.closingBalance;
            return vMCashFlow;
        }


        #endregion

        #region head wise trial balance 
        public decimal TrialBalanceGetByDateHeadWise(int headId, DateTime fromdate, DateTime todate)
        {
            decimal result;
            (decimal openingBalance, decimal dr, decimal cr, decimal closingBalance, int entryCount, decimal openingBalanceDr, decimal openingBalanceCr, decimal closingBalanceDr, decimal closingBalanceCr, decimal Balance) trialbalance = GetTrialBalanceInfo(headId, AccountTypeEnum.Head, fromdate, todate);
            result = trialbalance.Balance;
            return result;
        }
        public IEnumerable<VMLedger> GetJournalHistory()
        {

            return (from t1 in _db.Accounting_Head
                    join t2 in _db.Accounting_JournalSlave
                    on t1.ID equals t2.Accounting_HeadFK
                    join t3 in _db.Accounting_Journal
                    on t2.Accounting_JournalFK equals t3.ID
                    where t2.Active == true && t3.Active == true && t3.Approved == true && t3.Finalized == true
                    group new { t1, t2 } by new { t1.ID, t2.Debit, t2.Credit } into theGroup
                    select new VMLedger
                    {
                        Accounting_HeadId = theGroup.First().t1.ID,
                        Debit = theGroup.Sum(x => x.t2.Debit),
                        Credit = theGroup.Sum(x => x.t2.Credit)
                    }).AsEnumerable();

        }

        #endregion

        #region Accounting Cost Center Create,Update, Delete Portion Services
        public async Task<VmAccountingCostCenter> AccountingCostCenterGet()
        {
            VmAccountingCostCenter vmAccountingCostCenter = new VmAccountingCostCenter
            {
                DataList = await Task.Run(() => (from t1 in _db.Accounting_CostCenter
                                                 where t1.Active == true
                                                 select new VmAccountingCostCenter
                                                 {
                                                     ID = t1.ID,
                                                     Title = t1.Title,
                                                     Description = t1.Description,
                                                     Remark = t1.Remark
                                                 }).OrderByDescending(x => x.ID).ToList())
            };


            return vmAccountingCostCenter;
        }

        public async Task<int> AccountingCostCenterAdd(VmAccountingCostCenter vmAccountingCostCenter)
        {
            int result = -1;
            if (_db.Accounting_CostCenter.Where(x => x.Active == true).Any(o => o.Title.Trim().ToLower() == vmAccountingCostCenter.Title.Trim().ToLower()))
            {
                result = -1;
            }
            else
            {
                Accounting_CostCenter accountingCostCenter = new Accounting_CostCenter
                {
                    Title = vmAccountingCostCenter.Title,
                    Description = vmAccountingCostCenter.Description,
                    Remark = vmAccountingCostCenter.Remark,
                    User = vmAccountingCostCenter.User,
                };
                _db.Accounting_CostCenter.Add(accountingCostCenter);
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = accountingCostCenter.ID;
                }
            }
            return result;
        }
        public async Task<int> AccountingCostCenterEdit(VmAccountingCostCenter vmAccountingCostCenter)
        {
            int result = -1;
            //to select AccountHead data.....
            Accounting_CostCenter accountingCostCenter = _db.Accounting_CostCenter.Find(vmAccountingCostCenter.ID);
            if (_db.Accounting_CostCenter.Where(x => x.Active == true).Any(o => o.Title.Trim().ToLower() == vmAccountingCostCenter.Title.Trim().ToLower()))
            {
                result = -1;
            }
            else
            {
                accountingCostCenter.Title = vmAccountingCostCenter.Title;
                accountingCostCenter.Description = vmAccountingCostCenter.Description;
                accountingCostCenter.Remark = vmAccountingCostCenter.Remark;
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = accountingCostCenter.ID;
                }
            }
            return result;
        }
        public async Task<int> AccountingCostCenterDelete(int id)
        {
            int result = -1;
            Accounting_CostCenter accountingCostCenter = _db.Accounting_CostCenter.Find(id);
            if (_db.Accounting_Journal.Any(o => o.Accounting_CostCenterFK == id))
            {
                result = -1;
            }
            else
            {
                accountingCostCenter.Active = false;
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = accountingCostCenter.ID;
                }
            }
            return result;
        }
        #endregion


        #region Dropdown List for Accounting Services
        // Chart Type List from  Accounting_Type Table
        public List<object> AccountTypeDropDownList()
        {
            List<object> AccountTypeList = new List<object>();
            foreach (Accounting_Type AccountTypes in _db.Accounting_Type.ToList())
            {
                AccountTypeList.Add(new { Text = AccountTypes.Name, Value = AccountTypes.ID });
            }
            return AccountTypeList;
        }

        // Chart One List from  Accounting_Chart1 Table
        public List<object> ChartOneDropDownList()
        {
            List<object> ChartOneList = new List<object>();
            foreach (Accounting_Chart1 ChartOne in _db.Accounting_Chart1.Where(x => x.Active == true).ToList())
            {
                ChartOneList.Add(new { Text = ChartOne.Name, Value = ChartOne.ID });
            }
            return ChartOneList;
        }

        // Chart Onne List from  Accounting_Chart1 Table
        public List<object> ChartOneDropDownSpecific(int id)
        {
            List<object> ChartOneList = new List<object>();
            foreach (Accounting_Chart1 ChartOne in _db.Accounting_Chart1.Where(x => x.Accounting_TypeFK == id && x.Active == true).ToList())
            {
                ChartOneList.Add(new { Text = ChartOne.Name, Value = ChartOne.ID });
            }
            return ChartOneList;
        }




        // Chart Two List from  Accounting_Chart2 Table
        public List<object> ChartTwoDropDownList()
        {
            var List = new List<Object>();
            _db.Accounting_Chart2.Where(x => x.Active == true).ToList()
                .ForEach(x => List.Add(new
                {
                    Value = x.ID,
                    Text = x.Name
                }));
            return List;
        }

        //Get Specific Chart Two List from  Accounting_Chart2 Table 
        public List<object> ChartTwoDropDownSpecific(int id)
        {
            List<object> ChartTwoList = new List<object>();
            foreach (Accounting_Chart2 ChartTwo in _db.Accounting_Chart2.Where(x => x.Accounting_Chart1FK == id && x.Active == true).ToList())
            {
                ChartTwoList.Add(new { Text = ChartTwo.Name, Value = ChartTwo.ID });
            }
            return ChartTwoList;
        }

        //Get Specific Chart Two List from  Accounting_Chart2 Table 
        public List<object> AccountHeadDropDownSpecific(int id)
        {
            List<object> HeadList = new List<object>();
            foreach (Accounting_Head head in _db.Accounting_Head.Where(x => x.Accounting_Chart2FK == id && x.Active == true).ToList())
            {
                HeadList.Add(new { Text = head.Name, Value = head.ID });
            }
            return HeadList;
        }


        //

        // Accounting CostCenter List from  Accounting_CostCenter Table
        public List<object> AccountCostCenterDropDownList()
        {
            List<object> CostCenterList = new List<object>();
            foreach (Accounting_CostCenter center in _db.Accounting_CostCenter.Where(x => x.Active == true).ToList())
            {
                CostCenterList.Add(new { Text = center.Title, Value = center.ID });
            }
            return CostCenterList;
        }


        // Account Head List from  Accounting_Head Table
        public List<object> AccountHeadDropDownList()
        {
            List<object> HeadList = new List<object>();
            foreach (Accounting_Head head in _db.Accounting_Head.Where(x => x.Active == true).ToList())
            {
                HeadList.Add(new { Text = head.Name, Value = head.ID });
            }
            return HeadList;
        }
        //public List<object> AccountHeadDropDownListForBBLC()
        //{
        //    //1424
        //    List<object> HeadList = new List<object>();
        //    foreach (Accounting_Head head in _db.Accounting_Head.Where(x => x.Active == true && x.Accounting_Chart2FK == 1424).ToList())
        //    {
        //        HeadList.Add(new { Text = head.Name, Value = head.ID });
        //    }
        //    return HeadList;
        //}

        //Get Specific Account Head List from  Accounting_Head Table
        public List<object> AccountHeadDropDownListSpecific(int id)
        {
            List<object> HeadList = new List<object>();
            foreach (Accounting_Head head in _db.Accounting_Head.Where(x => x.Accounting_Chart2FK == id && x.Active == true).ToList())
            {
                HeadList.Add(new { Text = head.Name, Value = head.ID });
            }
            return HeadList;
        }

        public List<object> TrialBalanceSearchCharttype()
        {
            List<object> HeadList = new List<object>
            {
                new { Text = "Chart One", Value = "1" },
                new { Text = "Chart Two", Value = "2" },
                new { Text = "Head", Value = "3" }
            };
            return HeadList;
        }

        #endregion

    }

}
