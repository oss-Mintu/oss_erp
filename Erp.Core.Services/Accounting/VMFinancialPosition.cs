﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Erp.Core.Services.Accounting
{
     public class VMFinancialPosition:BaseVM
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public IEnumerable<VMIncomeStatement> DataList { get; set; }


        public decimal PropertyPlantandEquipment { get; set; }
        public decimal AccumulatedDepreciation { get; set; }
        public decimal TotalFixedAsset { get; set; }
        public decimal AccountReceivable { get; set; }
        public decimal Inventory { get; set; }
        public decimal AdvanceDepositandPrePayments { get; set; }
        public decimal CashinHand { get; set; }
        public decimal CashatBank { get; set; }
        public decimal OthersCurrentAssets { get; set; }
        public decimal TotalCurrentAssets { get; set; }
        public decimal TotalAssets { get; set; }
        public decimal ShareCapital { get; set; }
        public decimal GeneralReserve { get; set; }
        public decimal RevaluationReserve { get; set; }
        public decimal RetainedEarnings { get; set; }
        public decimal TotalEquity { get; set; }
        public decimal LongTermLoan { get; set; }
        public decimal ShortTermliabilities { get; set; }
        
        public decimal TotalLongTermLiabilities { get; set; }
        public decimal AccountPayable { get; set; }
        public decimal TotalCurrentLiabilities { get; set; }
        public decimal TotalEquityandLiabilities { get; set; }
        public decimal ShortTermLoan { get; set; }
        public decimal AcceptedBillLiabilities { get; set; }
        public decimal ProvisionforExpense { get; set; }
    }
}
