﻿using Erp.Core.Services.Home;
using Erp.Core.Services.Shipment;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Erp.Core.Services.Accounting
{
    public class VMJournal : BaseVM
    {
        // [DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public string CID { get; set; }
        public DateTime Date { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Remark { get; set; }
        public bool Finalized { get; set; } = false;
        public bool Approved { get; set; } = false;

        public decimal BillofExchangeValue  { get; set; }
        public decimal dollarRate { get; set; }
        public decimal CreditAmount { get; set; }
        // public int JournalType { get; set; }
        [Required(ErrorMessage = "Accounting Cost Center is Required.")]
        public int Accounting_CostCenterFK { get; set; }
        public string Accounting_CostCenterName { get; set; }
        public IEnumerable<VMJournal> DataList { get; set; }


        public decimal ForeignCurrencyAmount { get; set; } = 0;
        public decimal ConversionRateToBDT { get; set; } = 0;

        public bool isRealization { get; set; } = false;
        //public string Coce { get; set; }
        public VMShipmentBillOfExchange vMShipmentBillOfExchange { get; set; }
        public IEnumerable<VMShipmentBillOfExchange> vMShipmentBillOfExchanges { get; set; }
        public VMShipmentBillOfExchangeSlave vMShipmentBillOfExchangeSlave { get; set; }
        //for Export Realization
        public int ERAccounting_HeadFK { get; set; }

    }

    public class VMJournalSlave : VMJournal
    {
        public int ID1 { get; set; }
        //public VMJournal vMJournal { get; set; }
        public VMJournal vMJournal { get; set; }
        //  [Required(ErrorMessage = "Account Type is Required.")]
        public int Accounting_TypeFK1 { get; set; }
        public int Accounting_TypeFK { get; set; }
        public string Accounting_TypeName { get; set; }

        //  [Required(ErrorMessage = "Account Chart1 is Required.")]
        public int Accounting_Chart1FK1 { get; set; }
        public int Accounting_Chart1FK { get; set; }
        public string Accounting_Chart1Name { get; set; }

        //  [Required(ErrorMessage = "Account Chart2 is Required.")]
        public int Accounting_Chart2FK1 { get; set; }
        public int Accounting_Chart2FK { get; set; }
        public string Accounting_Chart2Name { get; set; }

        // [Required(ErrorMessage = "Accounting Journal is Required.")]
        public int Accounting_JournalFK { get; set; }
        public string Accounting_JournalName { get; set; }

        [Required(ErrorMessage = "Accounting Head is Required.")]
        public int Accounting_HeadFK1 { get; set; }
        [Required(ErrorMessage = "Accounting Head is Required.")]
        public int Accounting_HeadFK { get; set; }
        public string Accounting_HeadName { get; set; }

        public int AmountType { get; set; }
        public decimal Amount { get; set; }
        public decimal Amount1 { get; set; }

        public decimal Debit { get; set; } = 0;
        public decimal Credit { get; set; } = 0;

        public decimal TotalDebit { get; set; } = 0;
        public decimal TotalCredit { get; set; } = 0;


        public IEnumerable<VMJournalSlave> DataList1 { get; set; }
        public List<VMJournalSlave> DataListSlave { get; set; }

        public string message { get; set; }

    }

    public class VmAccountingCostCenter : BaseVM
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public string Remark { get; set; }
        public IEnumerable<VmAccountingCostCenter> DataList { get; set; }
    }

}
