﻿using Erp.Core.Entity.Common;
using Erp.Core.Entity.Procurement;
using Erp.Core.Entity.User;
using Erp.Core.Services.Home;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Transactions;
using Erp.Core.Entity.HRMS;

namespace Erp.Core.Services.User
{
    public class UserService : BaseService
    {
        public UserService(IErpDbContext db) => _db = db;


        public string GetHashedPassword(string password)
        {
            char[] array = password.ToCharArray();
            Array.Reverse(array);
            MD5 md5 = MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(array);
            byte[] hash = md5.ComputeHash(inputBytes);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("x2"));
            }
            return sb.ToString();
        }
        public object GetEmployeeInfoJson(int id)
        {
            var v = (from t1 in _db.HRMS_Employee
                     join t2 in _db.HRMS_Section on t1.HRMS_SectionFK equals t2.ID
                     join t3 in _db.User_Department on t1.User_DepartmentFK equals t3.ID
                     where t1.ID == id
                     select new
                     {
                         Name = t1.Name,
                         Email = t1.Email,
                         Mobile = t1.MobileNo,
                         DepartmentId = t3.ID,
                         Address = t1.PresentAddress,
                         ImagePath = t1.ImagePath
                     }).FirstOrDefault();
            // var v = _db.HRMS_Employee.Where(x => x.ID == id).FirstOrDefault();
            return v;
        }
        #region User
        public async Task<VMUser> UserGet()
        {
            VMUser vmUser = new VMUser();
            vmUser.DataList = await Task.Run(() => UserDataLoad());
            vmUser.UserDepartmentList = new SelectList(UserDepartmentDropDownList(), "Value", "Text");
            vmUser.UserAccessLevelList = new SelectList(UserAccessLevelDropDownList(), "Value", "Text");
            vmUser.HRMSEmployee = new SelectList(HRMSEmployeeDropDownList(), "Value", "Text");

            vmUser.UserRoleList = new SelectList(UserRoleDropDownList(), "Value", "Text");

            return vmUser;
        }
        public VMUser UserGetById(int id, int actionId, int typeId)
        {
            var v = (from t1 in _db.User_User
                     where t1.ID == id
                     select new VMUser
                     {
                         ActionId = actionId,
                         TypeId = typeId,
                         ID = t1.ID,
                         HeaderInfo = (typeId == 1) ? "User Activation" : (typeId == 2) ? "User Edit" : (typeId == 1) ? "User Access Control" : "Change Password",
                         Email = t1.Email,
                         Mobile = t1.Mobile,
                         Locked = t1.Locked,
                         Name = t1.Name,
                         Password = t1.Password,
                         Photo = t1.Photo,
                         User_AccessLevelFK = t1.User_AccessLevelFK,
                         User_DepartmentFK = t1.User_DepartmentFK,
                         User_RoleFK = t1.User_RoleFK,
                         User = t1.User,
                         Active = t1.Active,
                         Address = t1.Address,
                         Remarks = t1.Remarks,
                         Time = t1.Time,
                         UserName = t1.UserName,
                         UserDepartmentList = new SelectList(UserDepartmentDropDownList(), "Value", "Text"),
                         UserAccessLevelList = new SelectList(UserAccessLevelDropDownList(), "Value", "Text"),
                         UserRoleList = new SelectList(UserRoleDropDownList(), "Value", "Text"),

                     }).FirstOrDefault();
            return v;
        }

        public IEnumerable<VMUser> UserDataLoad()
        {
            var v = (from t1 in _db.User_User
                     join t2 in _db.User_AccessLevel on t1.User_AccessLevelFK equals t2.ID
                     join t3 in _db.User_Department on t1.User_DepartmentFK equals t3.ID
                     where t1.Active == true
                     select new VMUser
                     {
                         UserRoleName = t1.User_RoleFK != 0 ? _db.User_Role.Where( x => x.ID == t1.User_RoleFK).Select(x => x.Name).FirstOrDefault(): "",
                         Address = t1.Address,
                         ID = t1.ID,
                         Email = t1.Email,
                         Mobile = t1.Mobile,
                         Locked = t1.Locked,
                         Name = t1.Name,
                         Password = t1.Password,
                         Photo = t1.Photo,
                         UserAccessLevelName = t2.Name,
                         UserDepartmentName = t3.Name
                     }).OrderByDescending(x => x.ID).AsEnumerable();
            return v;
        }


        public async Task<int> UserAdd(VMUser vmUser)
        {
            var result = -1;
            User_User userUser = new User_User
            {
                ID = vmUser.ID,
                Name = vmUser.Name,
                UserName = vmUser.UserName,
                Password = vmUser.Password,
                Email = vmUser.Email,
                Mobile = vmUser.Mobile,
                Locked = false,
                Photo = vmUser.Photo,
                Address = vmUser.Address,
                User_DepartmentFK = vmUser.User_DepartmentFK,
                User_RoleFK = vmUser.User_RoleFK,
                User_AccessLevelFK = vmUser.User_AccessLevelFK,
                User = vmUser.User,
                UserID = vmUser.UserID,
                HRMS_EmployeeFK = vmUser.HRMS_EmployeeFK

            };
            _db.User_User.Add(userUser);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = userUser.ID;
            }
            return result;
        }
        public async Task<int> UserEdit(VMUser vmUser)
        {
            var result = -1;
            User_User userUser = _db.User_User.Find(vmUser.ID);
            userUser.Locked = vmUser.Locked;
            userUser.Name = vmUser.Name ?? userUser.Name;
            userUser.Photo = vmUser.Photo ?? userUser.Photo;
            userUser.Password = vmUser.Password ?? userUser.Password;
            userUser.Email = vmUser.Email ?? userUser.Email;
            userUser.Address = vmUser.Address ?? userUser.Address;
            userUser.Mobile = vmUser.Mobile ?? userUser.Mobile;
            userUser.User_DepartmentFK = vmUser.User_DepartmentFK != 0 ? vmUser.User_DepartmentFK : userUser.User_DepartmentFK;
            userUser.User_AccessLevelFK = vmUser.User_AccessLevelFK != 0 ? vmUser.User_AccessLevelFK : userUser.User_AccessLevelFK;
            userUser.User_RoleFK = vmUser.User_RoleFK != 0 ? vmUser.User_RoleFK : userUser.User_RoleFK;
            if (await _db.SaveChangesAsync() > 0)
            {
                result = userUser.ID;
            }
            return result;
        }
        public async Task<int> UserDelete(int id)
        {
            int result = -1;

            if (id != 0)
            {
                User_User userUser = _db.User_User.Find(id);
                userUser.Active = false;

                if (await _db.SaveChangesAsync() > 0)
                {
                    result = userUser.ID;
                }
            }
            return result;
        }
        #endregion

        #region User Department
        public async Task<SelectList> DepartmentDropDownListAsync(int? id = 0)
        {
            return new SelectList(await _db.User_Department.Where(x => x.Active && (id <= 0)).Select(x => new { Value = x.ID, Text = x.Name }).ToListAsync(), "Value", "Text");
        }
        public async Task<List<VMUserDepartment>> DepartmentListGetByUIdAsync(int id = 0)
        {
            return await _db.User_Department.Where(x => x.Active && (id == 0)).Select(x => new VMUserDepartment() { ID = x.ID, Name = x.Name }).ToListAsync();
        }
        public async Task<VMUserDepartment> UserDepartmentGet()
        {
            VMUserDepartment vmUserDepartment = new VMUserDepartment();
            vmUserDepartment.DataList = await Task.Run(() => (from D in _db.User_Department
                                                              //join U in _db.HRMS_Unit on D.HRMS_UnitFK equals U.ID into U_join
                                                              //from U in U_join.DefaultIfEmpty()
                                                              //join BU in _db.HRMS_BusinessUnit on U.HRMS_BusinessUnitFK equals BU.ID into BU_Join
                                                              //from BU in BU_Join.DefaultIfEmpty()
                                                              where D.Active == true 
                                                              select new VMUserDepartment
                                                              {
                                                                  ID = D.ID,
                                                                  Name = D.Name,
                                                                  Code = D.Code,
                                                                  //VMUnitID = D.HRMS_UnitFK,
                                                                  //VMUnitName = U != null ? U.Name : "",
                                                                  //VMBusinessUnitID = U != null ? U.HRMS_BusinessUnitFK : (int?)null,
                                                                  //VMBusinessUnitName = U != null && BU != null ? BU.Name : "",
                                                              }).OrderByDescending(x => x.ID).ToListAsync());
            return vmUserDepartment;
        }




        public async Task<int> UserDepartmentAdd(VMUserDepartment vmUserDepartment)
        {
            var result = -1;
            User_Department userDepartment = new User_Department
            {
                Name = vmUserDepartment.Name,
                User = "UserOne",
                Code = vmUserDepartment.Code,
                //HRMS_UnitFK = vmUserDepartment.VMUnitID,
            };
            _db.User_Department.Add(userDepartment);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = userDepartment.ID;
            }
            return result;
        }
        public async Task<int> UserDepartmentEdit(VMUserDepartment vmUserDepartment)
        {
            var result = -1;
            //to select Accountining_Chart_Two data.....
            User_Department userDepartment = _db.User_Department.Find(vmUserDepartment.ID);
            userDepartment.Name = vmUserDepartment.Name;
            //userDepartment.HRMS_UnitFK = vmUserDepartment.VMUnitID;
            userDepartment.Code = vmUserDepartment.Code;


            if (await _db.SaveChangesAsync() > 0)
            {
                result = userDepartment.ID;
            }
            return result;
        }
        public async Task<int> UserDepartmentDelete(int id)
        {
            int result = -1;

            if (id != 0)
            {
                User_Department userDepartment = _db.User_Department.Find(id);
                userDepartment.Active = false;

                if (await _db.SaveChangesAsync() > 0)
                {
                    result = userDepartment.ID;
                }
            }
            return result;
        }
        #endregion

        #region User Menu
        public async Task<VMUserMenu> UserMenuGet()
        {
            VMUserMenu vmUserMenu = new VMUserMenu();
            vmUserMenu.DataList = await Task.Run(() => UserMenuDataLoad());
            return vmUserMenu;
        }

        public IEnumerable<VMUserMenu> UserMenuDataLoad()
        {
            var v = (from t1 in _db.User_Menu
                     where t1.Active == true
                     select new VMUserMenu
                     {
                         ID = t1.ID,
                         Name = t1.Name

                     }).OrderByDescending(x => x.ID).AsEnumerable();
            return v;
        }


        public async Task<int> UserMenuAdd(VMUserMenu vmUserMenu)
        {
            var result = -1;
            User_Menu userMenu = new User_Menu
            {
                Name = vmUserMenu.Name,
                User = "UserOne",
            };
            _db.User_Menu.Add(userMenu);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = userMenu.ID;
            }
            return result;
        }
        public async Task<int> UserMenuEdit(VMUserMenu vmUserMenu)
        {
            var result = -1;
            //to select Accountining_Chart_Two data.....
            User_Menu userMenu = _db.User_Menu.Find(vmUserMenu.ID);
            userMenu.Name = vmUserMenu.Name;

            if (await _db.SaveChangesAsync() > 0)
            {
                result = userMenu.ID;
            }
            return result;
        }
        public async Task<int> UserMenuDelete(int id)
        {
            int result = -1;

            if (id != 0)
            {
                User_Menu userMenu = _db.User_Menu.Find(id);
                userMenu.Active = false;

                if (await _db.SaveChangesAsync() > 0)
                {
                    result = userMenu.ID;
                }
            }
            return result;
        }
        #endregion

        #region User AccessLevel
        public async Task<VMUserAccessLevel> UserAccessLevelGet()
        {
            VMUserAccessLevel vmUserAccessLevel = new VMUserAccessLevel();
            vmUserAccessLevel.DataList = await Task.Run(() => UserAccessLevelDataLoad());
            return vmUserAccessLevel;
        }

        public IEnumerable<VMUserAccessLevel> UserAccessLevelDataLoad()
        {
            var v = (from t1 in _db.User_AccessLevel
                     where t1.Active == true
                     select new VMUserAccessLevel
                     {
                         ID = t1.ID,
                         Name = t1.Name
                     }).OrderByDescending(x => x.ID).AsEnumerable();
            return v;
        }


        public async Task<int> UserAccessLevelAdd(VMUserAccessLevel vmUserAccessLevel)
        {
            var result = -1;
            User_AccessLevel userAccessLevel = new User_AccessLevel
            {
                Name = vmUserAccessLevel.Name,
                User = "UserOne",
            };
            _db.User_AccessLevel.Add(userAccessLevel);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = userAccessLevel.ID;
            }
            return result;
        }

        public IEnumerable<VMUserSubMenu> GetUserSubMenu(int id)
        {
            var v = (from t1 in _db.User_SubMenu
                     where t1.Active == true && t1.User_MenuFk == id
                     select new VMUserSubMenu
                     {
                         ID = t1.ID,
                         Name = t1.Name
                     }).OrderByDescending(x => x.ID).AsEnumerable();
            return v;
        }

        public async Task<int> UserAccessLevelEdit(VMUserAccessLevel vmUserAccessLevel)
        {
            var result = -1;
            //to select Accountining_Chart_Two data.....
            User_AccessLevel UserAccessLevel = _db.User_AccessLevel.Find(vmUserAccessLevel.ID);
            UserAccessLevel.Name = vmUserAccessLevel.Name;

            if (await _db.SaveChangesAsync() > 0)
            {
                result = UserAccessLevel.ID;
            }
            return result;
        }

        public VMUserRoleMenuItem UserRoleMenuItemById(int id)
        {
            var v = (from t1 in _db.User_RoleMenuItem
                     where t1.Active == true && t1.ID == id
                     select new VMUserRoleMenuItem
                     {
                         ID = t1.ID,
                         IsAllowed = t1.IsAllowed,
                         User_RoleFK = t1.User_RoleFK,
                         User_MenuItemFk = t1.User_MenuItemFk,
                         Remarks = t1.Remarks
                     }).SingleOrDefault();
            return v;
        }

        public async Task<int> UserAccessLevelDelete(int id)
        {
            int result = -1;

            if (id != 0)
            {
                User_AccessLevel userAccessLevel = _db.User_AccessLevel.Find(id);
                userAccessLevel.Active = false;

                if (await _db.SaveChangesAsync() > 0)
                {
                    result = userAccessLevel.ID;
                }
            }
            return result;
        }
        #endregion

        #region User Role
        public async Task<VMUserRole> UserRoleGet()
        {
            VMUserRole vmUserRole = new VMUserRole();
            vmUserRole.DataList = await Task.Run(() => UserRoleDataLoad());
            return vmUserRole;
        }

        public IEnumerable<VMUserRole> UserRoleDataLoad()
        {
            var v = (from t1 in _db.User_Role
                     where t1.Active == true
                     select new VMUserRole
                     {
                         ID = t1.ID,
                         Name = t1.Name
                     }).OrderByDescending(x => x.ID).AsEnumerable();
            return v;
        }


        public async Task<int> UserRoleAdd(VMUserRole vmUserRole)
        {
            var result = -1;
            User_Role userRole = new User_Role
            {
                Name = vmUserRole.Name,
                User = "UserOne",
            };
            _db.User_Role.Add(userRole);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = userRole.ID;
            }
            return result;
        }
        public async Task<int> UserRoleEdit(VMUserRole vmUserRole)
        {
            var result = -1;
            //to select Accountining_Chart_Two data.....
            User_Role userRole = _db.User_Role.Find(vmUserRole.ID);
            userRole.Name = vmUserRole.Name;

            if (await _db.SaveChangesAsync() > 0)
            {
                result = userRole.ID;
            }
            return result;
        }
        public async Task<int> UserRoleDelete(int id)
        {
            int result = -1;

            if (id != 0)
            {
                User_Role userRole = _db.User_Role.Find(id);
                userRole.Active = false;

                if (await _db.SaveChangesAsync() > 0)
                {
                    result = userRole.ID;
                }
            }
            return result;
        }
        #endregion

        #region User Submenu
        public async Task<VMUserSubMenu> UserSubMenuGet()
        {
            VMUserSubMenu vmUserSubMenu = new VMUserSubMenu();
            vmUserSubMenu.DataList = await Task.Run(() => UserSubMenuDataLoad());
            vmUserSubMenu.UserMenuList = new SelectList(UserMenuDropDownList(), "Value", "Text");
            return vmUserSubMenu;
        }

        public IEnumerable<VMUserSubMenu> UserSubMenuDataLoad()
        {
            var v = (from t1 in _db.User_SubMenu
                     join t2 in _db.User_Menu on t1.User_MenuFk equals t2.ID
                     where t1.Active == true
                     select new VMUserSubMenu
                     {
                         ID = t1.ID,
                         Name = t1.Name,
                         UserMenuName = t2.Name,
                         User_MenuFk = t2.ID

                     }).OrderByDescending(x => x.ID).AsEnumerable();
            return v;
        }


        public async Task<int> UserSubMenuAdd(VMUserSubMenu vmUserSubMenu)
        {
            var result = -1;
            User_SubMenu userSubMenu = new User_SubMenu
            {
                Name = vmUserSubMenu.Name,
                User_MenuFk = vmUserSubMenu.User_MenuFk,
                User = vmUserSubMenu.User
            };
            _db.User_SubMenu.Add(userSubMenu);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = userSubMenu.ID;
            }
            return result;
        }
        public async Task<int> UserSubMenuEdit(VMUserSubMenu vmUserSubMenu)
        {
            var result = -1;
            //to select Accountining_Chart_Two data.....
            User_SubMenu userSubMenu = _db.User_SubMenu.Find(vmUserSubMenu.ID);
            userSubMenu.Name = vmUserSubMenu.Name;
            userSubMenu.User_MenuFk = vmUserSubMenu.User_MenuFk;

            if (await _db.SaveChangesAsync() > 0)
            {
                result = userSubMenu.ID;
            }
            return result;
        }
        public async Task<int> UserSubMenuDelete(int id)
        {
            int result = -1;

            if (id != 0)
            {
                User_SubMenu userSubMenu = _db.User_SubMenu.Find(id);
                userSubMenu.Active = false;

                if (await _db.SaveChangesAsync() > 0)
                {
                    result = userSubMenu.ID;
                }
            }
            return result;
        }
        #endregion

        #region User Menuitem
        public async Task<VMUserMenuItem> UserMenuItemGet()
        {
            VMUserMenuItem vmUserMenuItem = new VMUserMenuItem();
            vmUserMenuItem.DataList = await Task.Run(() => UserMenuItemDataLoad());
            vmUserMenuItem.UserMenuList = new SelectList(UserMenuDropDownList(), "Value", "Text");
            vmUserMenuItem.UserSubMenuList = new SelectList(UserSubMenuDropDownList(), "Value", "Text");
            return vmUserMenuItem;
        }

        public IEnumerable<VMUserMenuItem> UserMenuItemDataLoad()
        {
            var v = (from t1 in _db.User_MenuItem
                     join t3 in _db.User_SubMenu on t1.User_SubMenuFk equals t3.ID
                     join t2 in _db.User_Menu on t3.User_MenuFk equals t2.ID

                     where t1.Active == true
                     select new VMUserMenuItem
                     {
                         ID = t1.ID,
                         Name = t1.Name,
                         IsAlone = t1.IsAlone,
                         UserMenuName = t2.Name,
                         UserSubMenuName = t3.Name,
                         Method = t1.Method,
                         Priority = t1.Priority,
                         User_MenuFk = t3.User_MenuFk,
                         User_SubMenuFk = t1.User_SubMenuFk

                     }).OrderByDescending(x => x.ID).AsEnumerable();
            return v;
        }


        public async Task<int> UserMenuItemAdd(VMUserMenuItem vmUserMenuItem)
        {
            var result = -1;
            User_MenuItem userMenuItem = new User_MenuItem
            {
                Name = vmUserMenuItem.Name,
                IsAlone = vmUserMenuItem.IsAlone,
                Method = vmUserMenuItem.Method,
                Priority = vmUserMenuItem.Priority,
                //User_MenuFk = vmUserMenuItem.User_MenuFk,
                User_SubMenuFk = vmUserMenuItem.User_SubMenuFk,
                User = vmUserMenuItem.User
            };
            _db.User_MenuItem.Add(userMenuItem);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = userMenuItem.ID;
                var userRoles = _db.User_Role.Where(x => x.Active == true).Select(x => x.ID).AsEnumerable();
                List<User_RoleMenuItem> userRoleMenuItemList = new List<User_RoleMenuItem>();
                foreach (var userRoleId in userRoles)
                {
                    User_RoleMenuItem userRoleMenuItem = new User_RoleMenuItem();
                    userRoleMenuItem.IsAllowed = false;
                    userRoleMenuItem.User_MenuItemFk = userMenuItem.ID;
                    userRoleMenuItem.User_RoleFK = userRoleId;
                    userRoleMenuItem.Active = true;
                    userRoleMenuItem.User = vmUserMenuItem.User;
                    userRoleMenuItem.Time = DateTime.Now;
                    userRoleMenuItemList.Add(userRoleMenuItem);
                }
                _db.User_RoleMenuItem.AddRange(userRoleMenuItemList);
                await _db.SaveChangesAsync();




            }
            return result;
        }
        public async Task<int> UserMenuItemEdit(VMUserMenuItem vmUserMenuItem)
        {
            var result = -1;
            //to select Accountining_Chart_Two data.....
            User_MenuItem userMenuItem = _db.User_MenuItem.Find(vmUserMenuItem.ID);
            userMenuItem.Name = vmUserMenuItem.Name;
            userMenuItem.IsAlone = vmUserMenuItem.IsAlone;
            userMenuItem.Method = vmUserMenuItem.Method;
            userMenuItem.Priority = vmUserMenuItem.Priority;
            //userMenuItem.User_MenuFk = vmUserMenuItem.User_MenuFk;
            userMenuItem.User_SubMenuFk = vmUserMenuItem.User_SubMenuFk;

            if (await _db.SaveChangesAsync() > 0)
            {
                result = userMenuItem.ID;
            }
            return result;
        }
        public async Task<int> UserMenuItemDelete(int id)
        {
            int result = -1;

            if (id != 0)
            {
                User_MenuItem userMenuItem = _db.User_MenuItem.Find(id);
                userMenuItem.Active = false;

                if (await _db.SaveChangesAsync() > 0)
                {
                    result = userMenuItem.ID;
                }
            }
            return result;
        }
        #endregion

        #region User role Menuitem
        public async Task<VMUserRoleMenuItem> UserRoleMenuItemGet(int id)
        {
            VMUserRoleMenuItem vmUserRoleMenuItem = new VMUserRoleMenuItem();
            int count = _db.User_RoleMenuItem.Where(x => x.User_RoleFK == id && x.Active == true).Count();
            if (count == 0)
            {
                var userMenuItem = (from t1 in _db.User_MenuItem
                                    where t1.Active == true
                                    select new VMUserRoleMenuItem
                                    {
                                        ID = t1.ID
                                    }).AsEnumerable();




                List<User_RoleMenuItem> userRoleMenuItemList = new List<User_RoleMenuItem>();
                foreach (var item in userMenuItem)
                {
                    User_RoleMenuItem userRoleMenuItem = new User_RoleMenuItem();
                    userRoleMenuItem.User_MenuItemFk = item.ID;
                    userRoleMenuItem.User_RoleFK = id;
                    userRoleMenuItem.IsAllowed = false;
                    userRoleMenuItem.Time = DateTime.Now;
                    userRoleMenuItem.Active = true;
                    userRoleMenuItemList.Add(userRoleMenuItem);
                }

                _db.User_RoleMenuItem.AddRange(userRoleMenuItemList);
                await _db.SaveChangesAsync();

            }
            vmUserRoleMenuItem.DataList = await Task.Run(() => UserRoleMenuItemDataLoad(id));
            vmUserRoleMenuItem.UserRoleName = _db.User_Role.Where(x => x.ID == id).Select(x => x.Name).FirstOrDefault();

            return vmUserRoleMenuItem;
        }

        public IEnumerable<VMUserRoleMenuItem> UserRoleMenuItemDataLoad(int id)
        {
            var v = (from t1 in _db.User_RoleMenuItem
                     join t2 in _db.User_MenuItem on t1.User_MenuItemFk equals t2.ID
                     join t3 in _db.User_Role on t1.User_RoleFK equals t3.ID
                     join t4 in _db.User_SubMenu on t2.User_SubMenuFk equals t4.ID
                     join t5 in _db.User_Menu on t4.User_MenuFk equals t5.ID

                     where t1.Active == true && t1.User_RoleFK == id
                     select new VMUserRoleMenuItem
                     {
                         ID = t1.ID,
                         SubmenuName = t4.Name,
                         SubmenuID = t4.ID,
                         IsAllowed = t1.IsAllowed,

                         MenuName = t5.Name,
                         MenuID = t5.ID,

                         MenuPriority = t5.Priority,
                         UserMenuItemName = t2.Name,
                         User_MenuItemFk = t2.ID,

                         Method = t2.Method,
                         UserRoleName = t3.Name,
                         User_RoleFK = t3.ID
                     }).OrderBy(x => x.MenuPriority).AsEnumerable();
            return v;
        }


        public async Task<int> UserRoleMenuItemAdd(VMUserRoleMenuItem vmUserRoleMenuItem)
        {
            var result = -1;
            User_RoleMenuItem userRoleMenuItem = new User_RoleMenuItem
            {
                IsAllowed = vmUserRoleMenuItem.IsAllowed,
                User_MenuItemFk = vmUserRoleMenuItem.ID,
                User_RoleFK = vmUserRoleMenuItem.ID,
                User = "UserOne",
            };
            _db.User_RoleMenuItem.Add(userRoleMenuItem);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = userRoleMenuItem.ID;
            }
            return result;
        }
        public async Task<(int userRoleMenuItemID, bool isAllowed)> UserRoleMenuItemEdit(VMUserRoleMenuItem vmUserRoleMenuItem)
        {
            var result = -1;
            //to select Accountining_Chart_Two data.....
            User_RoleMenuItem userRoleMenuItem = _db.User_RoleMenuItem.Find(vmUserRoleMenuItem.ID);
            userRoleMenuItem.IsAllowed = vmUserRoleMenuItem.IsAllowed;
            userRoleMenuItem.User = vmUserRoleMenuItem.User;

            if (await _db.SaveChangesAsync() > 0)
            {
                result = userRoleMenuItem.ID;
            }
            return (result, userRoleMenuItem.IsAllowed);
        }
        //public async (Task<int>, Task<bool>) UserRoleMenuItemEdit(VMUserRoleMenuItem vmUserRoleMenuItem)
        //{
        //    var result = -1;
        //    //to select Accountining_Chart_Two data.....
        //    User_RoleMenuItem userRoleMenuItem = _db.User_RoleMenuItem.Find(vmUserRoleMenuItem.ID);
        //    userRoleMenuItem.IsAllowed = vmUserRoleMenuItem.IsAllowed;
        //    userRoleMenuItem.User = vmUserRoleMenuItem.User;

        //    if (await _db.SaveChangesAsync() > 0)
        //    {
        //        result = userRoleMenuItem.ID;
        //    }
        //    return result, false;
        //}
        public async Task<int> UserRoleMenuItemDelete(int id)
        {
            int result = -1;

            if (id != 0)
            {
                User_RoleMenuItem userRoleMenuItem = _db.User_RoleMenuItem.Find(id);
                userRoleMenuItem.Active = false;

                if (await _db.SaveChangesAsync() > 0)
                {
                    result = userRoleMenuItem.ID;
                }
            }
            return result;
        }
        #endregion


        public List<object> UserMenuDropDownList()
        {
            var userMenuList = new List<object>();
            var userMenu = _db.User_Menu.Where(a => a.Active == true).ToList();
            foreach (var x in userMenu)
            {
                userMenuList.Add(new { Text = x.Name, Value = x.ID });
            }
            return userMenuList;
        }
        public List<object> UserSubMenuDropDownList()
        {
            var userSubMenuList = new List<object>();
            var userSubMenu = _db.User_SubMenu.Where(a => a.Active == true).ToList();
            foreach (var x in userSubMenu)
            {
                userSubMenuList.Add(new { Text = x.Name, Value = x.ID });
            }
            return userSubMenuList;
        }
        public List<object> UserDepartmentDropDownList()
        {
            var userDepartmentList = new List<object>();
            var userDepartment = _db.User_Department.Where(a => a.Active == true).ToList();
            foreach (var x in userDepartment)
            {
                userDepartmentList.Add(new { Text = x.Name, Value = x.ID });
            }
            return userDepartmentList;
        }
        public List<object> UserAccessLevelDropDownList()
        {
            var userAccessLevelList = new List<object>();
            var userAccessLevel = _db.User_AccessLevel.Where(a => a.Active == true).ToList();
            foreach (var x in userAccessLevel)
            {
                userAccessLevelList.Add(new { Text = x.Name, Value = x.ID });
            }
            return userAccessLevelList;
        }
        public List<object> HRMSEmployeeDropDownList()
        {
            var hrmsEmployeeList = new List<object>();
            var hrmsEmployee = _db.HRMS_Employee.Where(a => a.Active == true).ToList();
            hrmsEmployeeList.Add(new { Text = "Please Select Employee", Value = 0 });
            hrmsEmployeeList.Add(new { Text = "Super User", Value = 0 });



            foreach (var x in hrmsEmployee)
            {
                hrmsEmployeeList.Add(new { Text = x.Name, Value = x.ID });
            }
            return hrmsEmployeeList;
        }
        public List<object> UserRoleDropDownList()
        {
            var userRoleList = new List<object>();
            var userRole = _db.User_Role.Where(a => a.Active == true).ToList();
            foreach (var x in userRole)
            {
                userRoleList.Add(new { Text = x.Name, Value = x.ID });
            }
            return userRoleList;
        }
    }



}