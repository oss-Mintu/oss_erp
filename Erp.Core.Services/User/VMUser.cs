﻿using Erp.Core.Services.Home;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Erp.Core.Services.User
{
    public class VMUser : BaseVM
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public IFormFile File { get; set; }
        public string Photo { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public bool Locked { get; set; }
        public string UserAccessLevelName { get; set; }
        [Required(ErrorMessage = "Access Level is Required.")]
        public int User_AccessLevelFK { get; set; }
        public int HRMS_EmployeeFK { get; set; }
        public string UserRoleName { get; set; }
        [Required(ErrorMessage = "Role is Required.")]
        public int User_RoleFK { get; set; }

        public string UserDepartmentName { get; set; }
        [Required(ErrorMessage = "Department is Required.")]
        public int User_DepartmentFK { get; set; }

        public SelectList UserDepartmentList { get; set; } = new SelectList(new List<object>());
        public SelectList HRMSEmployee { get; set; } = new SelectList(new List<object>());

        public SelectList UserAccessLevelList { get; set; } = new SelectList(new List<object>());
        public SelectList UserRoleList { get; set; } = new SelectList(new List<object>());




        public IEnumerable<VMUser> DataList { get; set; }
        public bool Active { get; set; }
        public string Remarks { get; set; }
        public int TypeId { get; set; }
        public string HeaderInfo { get; set; }
    }
    public class VMUserDepartment : BaseVM
    {
        [Required(ErrorMessage ="Name is Required")]
        [MinLength(1,ErrorMessage ="Name is Required")]
        public string Name { get; set; }
        [Display(Name = "Business Unit")]
        [Required(ErrorMessage = "Business Unit is Required")]
        [Range(1, int.MaxValue, ErrorMessage = "Business Unit is Required")]
        public int? VMBusinessUnitID { get; set; }
        public string VMBusinessUnitName { get; set; }
        public SelectList VMBusinessUnitList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        [Display(Name = "Unit")]
        [Required(ErrorMessage = "Unit is Required")]
        [Range(1, int.MaxValue, ErrorMessage = "Unit is Required")]
        public int? VMUnitID { get; set; }
        
        public string VMUnitName { get; set; }
        public SelectList VMUnitList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        public List<VMUserDepartment> DataList { get; set; }
    }
    public interface IGlobalVariables
    {
        bool DevelopmentMode { get; set; }

    }
    public class VMUserSubMenu : BaseVM
    {
        public string Name { get; set; }
        public string UserMenuName { get; set; }
        public int User_MenuFk { get; set; }
        public SelectList UserMenuList { get; set; } = new SelectList(new List<object>());

        public IEnumerable<VMUserSubMenu> DataList { get; set; }
    }
    public class VMUserRoleMenuItem : BaseVM
    {
        public bool IsAllowed { get; set; }
        public string MenuName { get; set; }
        public int MenuID { get; set; }

        public string SubmenuName { get; set; }
        public int SubmenuID { get; set; }

        public string UserRoleName { get; set; }
        public int User_RoleFK { get; set; }

        public string UserMenuItemName { get; set; }
        public string Method { get; set; }
        public string Remarks { get; set; }
        public int User_MenuItemFk { get; set; }
        public IEnumerable<VMUserRoleMenuItem> DataList { get; set; }
        public int MenuPriority { get; set; }
    }
    public class VMUserRole : BaseVM
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public IEnumerable<VMUserRole> DataList { get; set; }
    }
    public class VMUserMenuItem : BaseVM
    {
        public string Name { get; set; }
        public int Priority { get; set; }
        public string Method { get; set; }
        public bool IsAlone { get; set; }

        public string UserMenuName { get; set; }
        public int User_MenuFk { get; set; }

        public SelectList UserMenuList { get; set; } = new SelectList(new List<object>());
        public SelectList UserSubMenuList { get; set; } = new SelectList(new List<object>());


        public string UserSubMenuName { get; set; }
        public int User_SubMenuFk { get; set; }
        public IEnumerable<VMUserMenuItem> DataList { get; set; }

    }
    public class VMUserMenu : BaseVM
    {
        public string Name { get; set; }
        public IEnumerable<VMUserMenu> DataList { get; set; }
    }
    public class VMUserAccessLevel : BaseVM
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public IEnumerable<VMUserAccessLevel> DataList { get; set; }
    }

    public class SessionHandler : BaseService
    {
        private readonly HttpContext httpContext;
        private readonly HomeService _service;

        public SessionHandler(IErpDbContext db, HttpContext _httpContext)
        {
            _db = db;
            _service = new HomeService(db, _httpContext);
            httpContext = _httpContext;

        }
        public void Adjust()
        {
            if (httpContext.Session.GetString("UserID") == null)
            {
                var userName = httpContext.Request.Cookies["UserNameCookies"];
                var password = httpContext.Request.Cookies["PasswordCookies"];
                if (userName != null && password != null)
                {
                    var user = _service.UserLogin(userName, password);
                    if (user.LblError != null)
                    {
                       // httpContext.Session.SetObjectAsJson("LoginUser", user);
                        httpContext.Session.SetString("User", user.UserName);
                        httpContext.Session.SetString("UserID", user.ID.ToString());
                        httpContext.Session.SetString("UserAccessLevelId", user.UserAccessLevelId.ToString());
                        httpContext.Session.SetString("UserDepartmentId", user.UserDepartmentId.ToString());


                        CookieOptions cookieOptions = new CookieOptions();
                        httpContext.Response.Cookies.Append("UserNameCookies", userName);
                        httpContext.Response.Cookies.Append("PasswordCookies", password);
                        cookieOptions.Expires = DateTime.Now.AddDays(1);
                    }
                }
            }
        }

    }

}