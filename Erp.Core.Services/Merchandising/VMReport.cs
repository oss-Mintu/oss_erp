﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Erp.Core.Services.Merchandising
{
    public class VMReport
    {
        public int ID { get; set; }

        [DisplayName("From Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime FromDate { get; set; }

        [DisplayName("To Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime ToDate { get; set; }

        public int StyleID { get; set; }

        public int ReportID { get; set; }
    }
}
