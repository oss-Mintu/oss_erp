﻿using System;
using System.Collections.Generic;
using System.Text;
using Erp.Core.Entity.Merchandising;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Erp.Core.Services.Merchandising
{
    public class VMStyleView : BaseVM
    {
        public VMStyle vmStyle { get; set; }

        public VMBOM vmBOM { get; set; }

        public VMBOF vmBOF { get; set; }

        public VMShipmentSchedule vmShipmentSchedule { get; set; }

        public List<VMShipmentColorSize> vmColorSizeRatio { get; set; }

        public List<VMColorSize> vmColorSizeSummary { get; set; }

        public VMCBS vmCBS { get; set; }

        public VMCBSSlave vmCBSSlave { get; set; }

        public List<VMColorSize> ListColorSize { get; set; }

        public VMStyleSetPack vmStyleSetPack { get; set; }

        public VMCBSSummary VMCBSSummary { get; set; }

        //public VMCBSSlave 

        public int ImportID { get; set; }

        public int TypeID { get; set; }

        public string Action { get; set; }

        public string Control { get; set; }

        public bool IsBomCreate { get; set; }

        public decimal UpdateRequiredQty { get; set; }
        public decimal UpdateConsumption { get; set; }

        public VMBOM lstCat1 { get; set; }
        public VMBOM lstCat2 { get; set; }
        public VMBOM lstCat3 { get; set; }
        public VMBOM lstCat4 { get; set; }
        public VMBOM lstCat5 { get; set; }
        public VMBOM lstCat6 { get; set; }
        public IEnumerable<VMBOF> BOFDataList { get; set; }
        public MultiSelectList ColorList { get; set; }
        //public SelectList ColorList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
    }
}
