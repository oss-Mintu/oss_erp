﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Erp.Core.Services.Merchandising
{
    public class VMStyleSetPack : BaseVM
    {
        public int Merchandising_StyleFK { get; set; }

        [Display(Name ="Sub-Set Name")]
        public string SetPackName { get; set; }

        public int Common_ColorFK { get; set; }

        public string Color { get; set; }

        public int Quantity { get; set; }

        public List<VMStyleSetPack> DataList { get; set; }
    }
}
