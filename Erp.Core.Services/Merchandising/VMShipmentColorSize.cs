﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Erp.Core.Services.Merchandising
{
    public class VMShipmentColorSize : BaseVM
    {
        public int OrderDeliveryScheduleFk { get; set; }

        public int Merchandising_StyleMeasurementFK { get; set; }

        public int Common_ColorFK { get; set; }

        public int Common_SizeFK { get; set; }

        public int SetPackFK { get; set; }
        public string SubSet { get; set; }
        public int SetPackQty { get; set; }
        public int PackPerPiece { get; set; }
        public string Destination { get; set; }

        public string DestinationNo { get; set; }

        public string PortNo { get; set; }

        public string Color { get; set; }

        public string Size { get; set; }

        public int Ratio { get; set; }

        public int TotalQty { get; set; }

        public int Quantity { get; set; }

        public int ProductionQty { get; set; }

        public IEnumerable<VMShipmentColorSize> DataList { get; set; }
    }
}
