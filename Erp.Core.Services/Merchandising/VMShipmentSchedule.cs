﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Erp.Core.Services.Merchandising
{

    public class VMCompletedShipmentStyle : BaseVM
    {
        public bool IsShipmentComplete { get; set; }

        [DisplayName("Style No")]
        public string CID { get; set; }

       
        [DisplayName("Finish Item")]
        public int Mkt_ItemFK { get; set; }
        
        public string BuyerPO { get; set; }
        
        [DisplayName("Style Name")]
        [Required(ErrorMessage = "Style is Required")]
        [MaxLength(100, ErrorMessage = "Upto 100 Chracter")]
        public string Style { get; set; }

        [DisplayName("Set Per Pack")]
        [RegularExpression(@"^(0*[1-9][0-9]*(\.[0-9]+)?|0+\.[0-9]*[1-9][0-9]*)$", ErrorMessage = "Can't be zero")]
        public int PackPieceQty { get; set; }

        [RegularExpression(@"^(0*[1-9][0-9]*(\.[0-9]+)?|0+\.[0-9]*[1-9][0-9]*)$", ErrorMessage = "Can't be zero")]
        [DisplayName("Pack Quantity")]
        [Required(ErrorMessage = "Pack Quantity is Required")]
        public int PackQty { get; set; }
        
        [DisplayName("Total Piece Quantity")]
        public int PieceQty { get; set; }

        [DisplayName("Invoiced Quantity")]
        public decimal InvoicedQuantity { get; set; }
        
        public string BuyerName { get; set; }
        public string ItemName { get; set; }
        public IEnumerable<VMCompletedShipmentStyle> CompletedShipmentStyleList { get; set; }


    }


    public class VMShipmentSchedule : BaseVM
    {
        [DisplayName("Style")]
        public int Merchandising_StyleFK { get; set; }

        [DisplayName("Destination")]
        public int Common_CountryFK { get; set; }

        [DisplayName("Port Name")]
        public int Common_CountryPortFK { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Delivery Date")]
        public DateTime Date { get; set; }

        [DisplayName("Set Quantity")]
        [Required(ErrorMessage = "Quantity is Required")]
        public int Quantity { get; set; }

        [DisplayName("Pack Quantity")]
        [Required(ErrorMessage = "Quantity is Required")]
        public int PackQuantity { get; set; }

        public int PackPieceQty { get; set; }

        [DisplayName("Piece Quantity")]
        [Required(ErrorMessage = "Quantity is Required")]
        public int PiecsQuantity { get; set; }

        public string Style { get; set; }

        public string Destination { get; set; }

        [DisplayName("DEST# Number")]
        //[Required(ErrorMessage = "Destination No is Required")]
        public string DestinationNo { get; set; }

        public string PortNo { get; set; }

        [DisplayName("Color With Size")]
        [DataType(DataType.MultilineText), Required(ErrorMessage = "Color With Size is Required")]
        public string ColorSize { get; set; }

        [DisplayName("Shipment Color")]
        public int Common_ColorFK { get; set; }

        public int? StyleSetPackFK { get; set; }

        [DisplayName("Color Quantity")]
        public int ColorQty { get; set; }

        public List<string> ColorIds { get; set; }
        public List<string> ColorQtys { get; set; }

        public IEnumerable<VMShipmentSchedule> DataList { get; set; }

        public IEnumerable<VMShipmentColorSize> RatioDataList { get; set; }
    }
}
