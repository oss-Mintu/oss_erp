﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Erp.Core.Services.Merchandising
{
    public class VMStyle : BaseVM
    {
        [DisplayName("Order No")]
        [Required(ErrorMessage = "Order Name is Required")]
        public int Merchandising_BuyerOrderFK { get; set; }

        [DisplayName("Style No")]
        public string CID { get; set; }

        [DisplayName("Buyer")]
        public int Common_BuyerFK { get; set; }

        [DisplayName("Category")]
        public int Mkt_CategoryFK { get; set; }

        [DisplayName("Sub-Category")]
        public int Mkt_SubCategoryFK { get; set; }

        [DisplayName("Finish Item")]
        public int Mkt_ItemFK { get; set; }

        [DisplayName("Currency")]
        public int Common_CurrencyFK { get; set; }

        [DisplayName("First Move")]
        [DisplayFormat(DataFormatString = "{0:d/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime FirstMove { get; set; }

        [DisplayName("Updated")]
        [DisplayFormat(DataFormatString = "{0:d/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime UpdateDate { get; set; }

        [MaxLength(100, ErrorMessage = "Upto 100 Chracter")]
        public string Class { get; set; }

        [DisplayName("Fabrication")]
        [MaxLength(100, ErrorMessage = "Upto 100 Chracter")]
        public string Fabrication { get; set; }

        [DisplayName("Style Reference")]
        [MaxLength(150, ErrorMessage = "Upto 150 Chracter")]
        public string Reference { get; set; }

        public string BuyerPO { get; set; }

        [DisplayName("Size Range")]
        public List<string> SizeIDs { get; set; }

        [DisplayName("Color")]
        public List<string> ColorIDs { get; set; }

        [DisplayName("Style Name")]
        [Required(ErrorMessage = "Style is Required")]
        [MaxLength(100, ErrorMessage = "Upto 100 Chracter")]
        public string Style { get; set; }

        [DisplayName("Set Per Pack")]
        [RegularExpression(@"^(0*[1-9][0-9]*(\.[0-9]+)?|0+\.[0-9]*[1-9][0-9]*)$", ErrorMessage = "Can't be zero")]
        public int PackPieceQty { get; set; }

        [RegularExpression(@"^(0*[1-9][0-9]*(\.[0-9]+)?|0+\.[0-9]*[1-9][0-9]*)$", ErrorMessage = "Can't be zero")]
        [DisplayName("Pack Quantity")]
        [Required(ErrorMessage = "Pack Quantity is Required")]
        public int PackQty { get; set; }

        public int SubSetPieceQty { get; set; }

        public decimal DozonQty { get; set; }

        [DisplayName("Subset Types")]
        [Required(ErrorMessage = "Subset Types is Required")]
        public int SetQuantity { get; set; }

        [DisplayName("Pack Per CTN")]
        [Required(ErrorMessage = "Pack Per Carton is Required")]
        public int SetPackCarton { get; set; }

        [DisplayName("Total Piece Quantity")]
        public int PieceQty { get; set; }

        [DisplayName("Total Piece Quantity")]
        public int ProductionPieceQty { get; set; }

        [DisplayName("Unit Price")]
        [Required(ErrorMessage = "Unit Price is Required")]
        [RegularExpression(@"^(0*[1-9][0-9]*(\.[0-9]+)?|0+\.[0-9]*[1-9][0-9]*)$", ErrorMessage = "Can't be zero")]
        [Range(0.0, Double.MaxValue)]
        [DisplayFormat(DataFormatString = "{0:n5}", ApplyFormatInEditMode = true)]
        public decimal UnitPrice { get; set; }

        [DisplayName("Pack Price")]
        [Required(ErrorMessage = "Pack Price is Required")]
        [RegularExpression(@"^(0*[1-9][0-9]*(\.[0-9]+)?|0+\.[0-9]*[1-9][0-9]*)$", ErrorMessage = "Can't be zero")]
        [Range(0.0, Double.MaxValue)]
        [DisplayFormat(DataFormatString = "{0:n5}", ApplyFormatInEditMode = true)]
        public decimal PackPrice { get; set; }

        public decimal StylePrice { get; set; }

        public decimal OrderBuyingCommission { get; set; }

        [DisplayName("Overhead in %")]
        public decimal OverheadRate { get; set; }

        public decimal MakingCost { get; set; }

        [DisplayName("Item Description")]
        [MaxLength(500, ErrorMessage = "Upto 500 Chracter")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [DisplayName("PO Change History")]
        [MaxLength(300, ErrorMessage = "Upto 300 Chracter")]
        public string POChangeHistory { get; set; }

        [DefaultValue(false)]
        public bool IsComplete { get; set; }

        [DefaultValue(true)]
        public bool IsNew { get; set; }

        [DisplayName("Authorization")]
        [DefaultValue(false)]
        public bool IsAuthorize { get; set; }

        [DefaultValue(false)]
        public bool IsCount { get; set; }

        public string Order { get; set; }
        public string BuyerName { get; set; }
        public string NotifyPartyName { get; set; }
        public string ItemName { get; set; }
        public string UnitName { get; set; }
        public string SizeName { get; set; }


        public string RefShipment { get; set; }
        public string RefBOM { get; set; }
        public string RefSetPack { get; set; }
        public string RefSMV { get; set; }
        public string RefBOF { get; set; }
        public DateTime ShipDate { get; set; }
        public string IsCanDelete { get; set; }
        public DateTime OrderDate { get; set; }
        public string Season { get; set; }
        [DisplayName("From Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime FromDate { get; set; }
        [DisplayName("To Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime ToDate { get; set; }
        public int StyleID { get; set; }
        [DisplayName("Report Type")]
        public int ReportID { get; set; }

        public List<VMStyle> DataList { get; set; }

        public MultiSelectList ColorList { get; set; }
        public MultiSelectList StyleSlaveFabricItemsList { get; set; }


        public MultiSelectList SizeList { get; set; }
    }
}
