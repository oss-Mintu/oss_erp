﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Erp.Core.Services.Merchandising
{
    public class VMPacking : BaseVM
    {
        public int Merchandising_StyleShipmentScheduleFK { get; set; }
        public int Merchandising_StyleShipmentRatioFK { get; set; }
        public int Merchandising_StyleFK { get; set; }
        public decimal CartonRetio { get; set; }
        public decimal GrossWeight { get; set; } // product and Carton Weight.
        public decimal NetWeight { get; set; } // Only Product Weight.
        public decimal CartonMeasurement { get; set; } //CBM.
        public string Size { get; set; }
        public string Color { get; set; }
        public decimal CTNWidth { get; set; }
        public decimal CTNHeight { get; set; }
        public decimal CTNLength { get; set; }
        public List<VMPacking> DataList { get; set; }

        public List<VMPacking> LoadPackList { get; set; }
    }
}
