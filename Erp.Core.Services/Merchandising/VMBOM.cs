﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Erp.Core.Services.Merchandising
{
    public class VMBOM : BaseVM
    {
        [DisplayName("Style")]
        [Required(ErrorMessage = "Style is Required")]
        public int Merchandising_StyleFK { get; set; }

        public int? SelfReferenceFK { get; set; }

        public int? Merchandising_CBSSlaveFK { get; set; }

        [DisplayName("Raw Category")]
        public int Common_RawCategoryFK { get; set; }

        [DisplayName("Raw Sub-Category")]
        public int Common_RawSubCategoryFK { get; set; }

        [DisplayName("Raw Item")]
        public int Common_RawItemFK { get; set; }

        [DisplayName("Currency")]
        public int Common_CurrencyFK { get; set; }

        [Range(0.0, Double.MaxValue)]
        [DisplayName("GSM  ")]
        [Required(ErrorMessage = "GSM is Required")]
        public decimal GSM { get; set; }

        [DisplayName("Color")]
        public int? Common_ColorFK { get; set; }

        [DisplayName("Size")]
        public int? Common_SizeFK { get; set; }

        [Range(0.0, Double.MaxValue)]
        [DisplayName("Required Quantity")]
        [Required(ErrorMessage = "Quantity is Required")]
        public decimal RequiredQty { get; set; }

        [Range(0.0, Double.MaxValue)]
        [DisplayName("Consumption Required Quantity")]
        [Required(ErrorMessage = "Consumption Quantity is Required")]
        public decimal ConRequiredQty { get; set; }

        [DisplayName("Unit Price")]
        [Required(ErrorMessage = "Item Price is Required")]
        [RegularExpression(@"^(0*[1-9][0-9]*(\.[0-9]+)?|0+\.[0-9]*[1-9][0-9]*)$", ErrorMessage = "Can't be zero")]
        [Range(0.0, Double.MaxValue)]
        [DisplayFormat(DataFormatString = "{0:n5}", ApplyFormatInEditMode = true)]
        public decimal UnitPrice { get; set; }

        [Range(0.0, Double.MaxValue)]
        [Required(ErrorMessage = "Tolerance is Required")]
        [DisplayFormat(DataFormatString = "{0:n5}", ApplyFormatInEditMode = true)]
        public decimal Tolerance { get; set; }

        [Range(0.0, Double.MaxValue)]
        [Required(ErrorMessage = "Consumption is Required")]
        [DisplayFormat(DataFormatString = "{0:n5}", ApplyFormatInEditMode = true)]
        public decimal Consumption { get; set; }

        [DisplayName("Total Price")]
        public decimal TotalPrice { get; set; }

        public int TotalPiece { get; set; }

        [DisplayName("Is This Fabrics Consumption Summary?")]
        public bool IsFabric { get; set; }

        [DisplayName("Locked")]
        public bool IsLocked { get; set; }
        //added by fahim on 19/6/2019 to diffrentiate between data from styleslave & bof
        public bool IsBOM { get; set; }
        public string Style { get; set; }
        public string StyleID { get; set; }
        public string Currency { get; set; }
        public string Color { get; set; }
        public string Size { get; set; }
        public string RawCategory { get; set; }
        public string RawSubCategory { get; set; }
        public string RawItem { get; set; }
        public string UnitName { get; set; }
        public string Description { get; set; }

        public string FabricName { get; set; }
        public bool IsApproved { get; set; }
        public int TypeID { get; set; }
        public List<VMBOM> DataList { get; set; }
    }
}
