﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Erp.Core.Services.Merchandising
{
    public class VMCBS : BaseVM
    {
        [DisplayName("CBS ID")]
        public string CID { get; set; }
        public string Style { get; set; }
        [DisplayName("Style")]
        [Required(ErrorMessage = "Style is Required")]
        public int Merchandising_CBSStyleFK { get; set; }
        [DisplayName("Buyer")]
        public int Common_BuyerFK { get; set; }
        [DisplayName("Order Quantity")]
        public int OrderQty { get; set; }
        [DisplayName("Overhead in %")]
        public decimal OverheadRate { get; set; }
        [DisplayName("Buying Commission in %")]
        public decimal BuyingCommission { get; set; }
        [DisplayName("Profit Margin in %")]
        public decimal ProfitMargin { get; set; }
        [DisplayName("Dozon")]
        public decimal DozonQty { get; set; }
        [DisplayName("Style Reference")]
        [MaxLength(150, ErrorMessage = "Upto 150 Chracter")]
        public string Reference { get; set; }
        [StringLength(500, ErrorMessage = "Upto 500 Chracter")]
        [DataType(DataType.MultilineText)]
        public string Remarks { get; set; }
        [DisplayName("Locked")]
        [DefaultValue(false)]
        public bool IsLocked { get; set; }
        public string StyleID { get; set; }
        public string StyleName { get; set; }
        public string BuyerName { get; set; }
        public decimal PieceQty { get; set; }
        public VMCBSSummary VMCBSSummary { get; set; }
        public List<VMCBS> DataList { get; set; }
    }
}
