﻿using System;
using System.Collections.Generic;
using System.Text;
using Erp.Core.Entity.Merchandising;

namespace Erp.Core.Services.Merchandising
{
    public class VMBOFView : BaseVM
    {
        public int Merchandising_StyleFK { get; set; }
        public string StyleNo { get; set; }
        public int Merchandising_YarnCalculationFK { get; set; }
        public int? Common_RawItemFK { get; set; }
        public string RawItem { get; set; }
        public int? Common_CurrencyFK { get; set; }
        public int? Common_UnitFK { get; set; }
        public string Description { get; set; }
        public decimal Consumption { get; set; }
        public decimal? RequiredQuantity { get; set; }
        public decimal Price { get; set; }
        public VMStyle vmStyle { get; set; }
        public int Merchandising_StyleSlaveFK_Fabric { get; set; }
        public VMBOM vmBOM { get; set; }

        public VMShipmentSchedule vmShipmentSchedule { get; set; }

        public List<VMShipmentColorSize> vmColorSizeRatio { get; set; }

        public VMCBS vmCBS { get; set; }

        public VMCBSSlave vmCBSSlave { get; set; }

        public List<VMColorSize> ListColorSize { get; set; }

        public VMStyleSetPack vmStyleSetPack { get; set; }

        public VMCBSSummary VMCBSSummary { get; set; }

        //public VMCBSSlave 

        public int ImportID { get; set; }

        public int TypeID { get; set; }

        public string Action { get; set; }

        public string Control { get; set; }

        public bool IsBomCreate { get; set; }
        public VMYarnCalculation VMYarnCalculation { get; set; }
        public Merchandising_BOF Merchandising_BOF { get; set; }

        public IEnumerable<VMBOF> BOFDataList { get; set; }
        public bool IsFabrics { get;  set; }
        public bool IsFabric { get; set; } = false;
        public int Common_ColorFK { get; internal set; }
    }


}
