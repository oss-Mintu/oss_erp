﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Erp.Core.Services.Merchandising
{
    public class VMCBSSummary
    {
        public int ID { get; set; }
        public decimal StyleValue { get; set; }
        public decimal DzStyleValue { get; set; }


        public decimal CostFabric { get; set; }

        public decimal CostAccessories { get; set; }

        public decimal CostPrint { get; set; }

        public decimal CostWash { get; set; }

        public decimal CostEmbroidiry { get; set; }

        public decimal CostCharges { get; set; }

        public decimal CostTotalValue { get; set; }
        //

        public decimal CostOHValue { get; set; }

        public decimal ProductValueProfitValue { get; set; }

        public decimal CostOrderValue { get; set; }

        public decimal DzCostOrderValue { get; set; }
        //

        public decimal CatFabric { get; set; }

        public decimal CatAccessories { get; set; }

        public decimal CatPrint { get; set; }

        public decimal CatWash { get; set; }

        public decimal CatEmbroidiry { get; set; }

        public decimal CatCharges { get; set; }

        public decimal CatTotalValue { get; set; }

        public decimal OrderTotalMakingCost { get; set; }

        public decimal StyleCostingValue { get; set; }

        public decimal StyleGrossCostValue { get; set; }

        public decimal DzStyleGrossCostValue { get; set; }

        public decimal DzStyleCostingValue { get; set; }

        /// <summary>
        public decimal DzMakingCost { get; set; }
        public decimal DzOHValue { get; set; }

        public decimal DzProductValueProfitValue { get; set; }

        public decimal DzOrderValue { get; set; }

        /// <summary>
        public decimal OverheadRate { get; set; }
        public decimal BuyingComRate { get; set; }
        public decimal ProfitMarginRate { get; set; }
        public decimal DzProfitMarginRate { get; set; }

        public decimal CostOverheadValue { get; set; }
        public decimal CostBuyingComValue { get; set; }
        public decimal CostProfitMarginValue { get; set; }

        public decimal DzOverheadValue { get; set; }
        public decimal DzBuyingComValue { get; set; }
        public decimal DzProfitMarginValue { get; set; }
    }
}
