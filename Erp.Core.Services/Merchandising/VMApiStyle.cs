﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Erp.Core.Services.Merchandising
{
    public class VMApiStyle
    {
        public int ID { get; set; }
        public string DeviceID { get; set; }
        public int LineID { get; set; }
        public string Name { get; set; }
        public string LineName { get; set; }
    }

    public class VMApiStyleSetPack
    {
        public int ID { get; set; }
        public string SetPackName { get; set; }
    }


}
