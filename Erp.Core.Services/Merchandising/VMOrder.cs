﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Erp.Core.Services.Merchandising
{
     public class VMOrder : BaseVM
     {
        public int Mkt_BuyerFK { get; set; }

        [MaxLength(100, ErrorMessage = "Upto 100 Chracter")]
        [DisplayName("Order ID")]
        public string CID { get; set; }

        [DisplayName("Buyer")]
        public string BuyerName { get; set; }
       
        [DisplayName("Buyer PO Number")]
        [Required(ErrorMessage = "Buyer PO Number is Required")]
        [MaxLength(10, ErrorMessage = "PO Number: Maximum 10 Chracter Long")]
        public string BuyerPO { get; set; }

        [DisplayName("Season/Year")]
        [MaxLength(100, ErrorMessage = "Upto 100 Chracter")]
        public string Season { get; set; }

        [DisplayName("Order Date")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime OrderDate { get; set; }
        public int Common_AgentFK { get; set; }
        [DisplayName("Agent")]
        [MaxLength(100, ErrorMessage = "Upto 100 Chracter")]
        public string Agent { get; set; }
        public int Common_InspectionAgentFK { get; set; }
        [DisplayName("Inspection Agent")]
        [MaxLength(100, ErrorMessage = "Upto 100 Chracter")]
        public string InspectionAgent { get; set; }

        [DisplayName("Commission")]
        [RegularExpression(@"^\d+.\d{0,2}$", ErrorMessage = "Commission can't have more than 2 decimal places")]
        public decimal? Commission { get; set; }

        [DisplayName("BuyerPO Value")]
        [Required(ErrorMessage = "BuyerPO is Required")]
        [RegularExpression(@"^(0*[1-9][0-9]*(\.[0-9]+)?|0+\.[0-9]*[1-9][0-9]*)$", ErrorMessage = "Can't be zero")]
        [Range(0.0, Double.MaxValue)]
        [DisplayFormat(DataFormatString = "{0:n5}", ApplyFormatInEditMode = true)]
        public decimal BuyerPOValue { get; set; }

        [DisplayName("Remarks")]
        [DataType(DataType.MultilineText)]
        [MaxLength(200, ErrorMessage = "Upto 200 Chracter")]
        public string Remarks { get; set; }

        public string IsClosed { get; set; }

        public bool IsReference { get; set; }

        public List<VMOrder> DataList { get; set; }
        public IEnumerable<VMOrder> DataListforCommercial { get; set; }
        public int TotalOrderPackQuntity { get; set; }
    }

}
