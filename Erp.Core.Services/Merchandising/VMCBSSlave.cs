﻿using Erp.Core.Entity.Common;
using Erp.Core.Entity.Merchandising;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Erp.Core.Services.Merchandising
{
    public class VMCBSSlave : BaseVM
    {
        public int Merchandising_CBSFK { get; set; }

        [DisplayName("Raw Category")]
        public int Common_RawCategoryFK { get; set; }

        [DisplayName("Raw Sub-Category")]
        public int Common_RawSubCategoryFK { get; set; }

        [DisplayName("Raw Item")]
        public int Common_RawItemFK { get; set; }

        [DisplayName("Currency")]
        public int Common_CurrencyFK { get; set; }

        [Range(0.0, Double.MaxValue)]
        [DisplayName("GSM")]
        [Required(ErrorMessage = "GSM is Required")]
        public decimal GSM { get; set; }

        [DisplayName("Color")]
        public int Common_ColorFK { get; set; }

        [Range(0.0, Double.MaxValue)]
        [DisplayName("Consumption Required Quantity")]
        [Required(ErrorMessage = "Consumption Quantity is Required")]
        public decimal ConRequiredQty { get; set; }

        [DisplayName("Consumption")]
        [Range(0.0, Double.MaxValue)]
        public decimal Consumption { get; set; }

        [DisplayName("Required Quantity")]
        [Range(0.0, Double.MaxValue)]
        public decimal RequiredQty { get; set; }

        [DisplayName("Unit Price")]
        [Range(0.0, Double.MaxValue)]
        public decimal UnitPrice { get; set; }

        [DisplayName("Total Price")]
        public decimal TotalPrice { get; set; }

        public int TotalPieceQty { get; set; }

        public string Currency { get; set; }
        public string RawCategory { get; set; }
        public string RawSubCategory { get; set; }
        public string RawItem { get; set; }
        public string CBSID { get; set; }
        public string UnitType { get; set; }
        public string Color { get; set; }
        public int TypeID { get; set; }

        public List<VMCBSSlave> DataList { get; set; }
    }
}
