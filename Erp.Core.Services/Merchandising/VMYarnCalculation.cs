﻿using Erp.Core.Entity.Common;
using Erp.Core.Entity.Merchandising;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Erp.Core.Services.Merchandising
{
    public class VMYarnCalculation : BaseVM
    {
        public string ReferenceNo { get; set; }       
        public int Merchandising_StyleFk { get; set; }

        //public int FinishFabrics { get; set; }
        //public int QntyDz { get; set; }
        //public int ActualYarn { get; set; }
        //public int YarnQuantity { get; set; }
        //public int TotalLycra { get; set; }
        public bool IsNeedExtraWashing { get; set; }        
        public int Merchandising_YarnTypeFk { get; set; }
        [DisplayName("Color Name")]
        public int Common_ColorFK { get; set; }
        public int Merchandising_StyleSlaveFK_Fabric { get; set; }
        public string YarnTypeName { get; set; }       
        public string Combo { get; set; }        
        public string Color { get; set; }        
        public string Fabrication { get; set; }
        public int GSM { get; set; }
        public int Raw_SubCategoryFK { get; set; }
        public string SubcategoryName { get; set; }       
        public int Raw_ItemFK { get; set; }
        public string ItemName { get; set; }
        public string FinishDIA { get; set; }
        public decimal OrderQuantityPCS { get; set; }       
        public decimal Consumption { get; set; }
        [Column(TypeName = "decimal(18,5)")]
        public decimal ProcessLoss { get; set; }
        [Column(TypeName = "decimal(18,5)")]
        public decimal Lycra { get; set; }       
        public int Common_CurrencyFK { get; set; }
        public string CurrencyName { get; set; }     
        public int Common_UnitFK { get; set; }
        public string UnitName { get; set; }
        [Column(TypeName = "decimal(18,5)")]
        public decimal Price { get; set; }
        public VMStyle VMStyle { get; set; }
        public VMBOFView vmBOFView { get; set; }

        public IEnumerable<VMYarnCalculation> DataList { get; set; }
        [Column(TypeName = "decimal(18,5)")]
        public decimal OrderQuantityDZ { get; set; }
        [Column(TypeName = "decimal(18,5)")]
        public decimal FinishFabric { get; set; }
        [Column(TypeName = "decimal(18,5)")]
        public decimal YarnQnty { get; set; }
        [Column(TypeName = "decimal(18,5)")]
        public decimal TotalLycra { get; set; }
        [Column(TypeName = "decimal(18,5)")]
        public decimal TotalYarn { get; set; }
        public bool IsApproved { get; set; }

        public List<VMBOFView> VMBOFViewList { get; set; }
}
}