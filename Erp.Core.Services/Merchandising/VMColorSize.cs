﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Erp.Core.Services.Merchandising
{
    public class VMColorSize : BaseVM
    {
        public int SetPackFK { set; get; }
        public string SubSetName { set; get; }
        public int SizeId { get; set; }
        public int ColorId { get; set; }
        public string Color { get; set; }
        public string Size { get; set; }
        public int SizeRatio { get; set; }
        public int Quantity { get; set; }
        public List<VMColorSize> SizeList { get; set; }
    }
}
