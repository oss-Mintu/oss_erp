﻿using Erp.Core.Entity.Common;
using Erp.Core.Entity.Merchandising;
using Erp.Core.Services.Home;
using Erp.Core.Services.Merchandising;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Erp.Core.Services.Merchandising
{
    public class MerchandisingService : BaseService
    {
        //private readonly HttpContext _httpContext;

        private readonly VMLogin _vmLogin;
        public MerchandisingService(IErpDbContext db, HttpContext httpContext)
        {
            _db = db;
            CidServices = new CidServices(db);
            _vmLogin = httpContext.Session.GetObjectFromJson<VMLogin>("LoginUser");
        }

        #region ManageOrder
        public async Task<VMOrder> OrderGet()
        {
            var vData = (from t1 in _db.Merchandising_BuyerOrder
                         join t2 in _db.Common_Buyer on t1.Common_BuyerFK equals t2.ID
                         join t3 in _db.Common_Agent on t1.Common_AgentFK equals t3.ID
                         join t4 in _db.Common_InspectionAgent on t1.Common_InspectionAgentFK equals t4.ID
                         where t1.Active == true
                         select new VMOrder
                         {
                             ID = t1.ID,
                             CID = t1.CID,
                             BuyerName = t2.Name,
                             Commission = t1.Commission,
                             BuyerPO = t1.BuyerPO,
                             Common_AgentFK = t3.ID,
                             Agent = t3.Name,
                             Common_InspectionAgentFK = t4.ID,
                             InspectionAgent = t4.Name,
                             OrderDate = t1.OrderDate,
                             Mkt_BuyerFK = t1.Common_BuyerFK,
                             BuyerPOValue = t1.BuyerOrderPOValue,
                             Remarks = t1.Remarks,
                             Season = t1.Season,
                             IsClosed = t1.IsComplete == false ? "Open" : "Closed",
                             UserID = t1.UserID
                         }).OrderByDescending(a => a.ID).ToList();


            if (vData.Any())
            {
                if (_vmLogin.UserAccessLevelId == (int)UserAccessLevel.Basic)
                {
                    vData = vData.Where(x => x.UserID == _vmLogin.ID).ToList();
                }

                foreach (var v in vData)
                {
                    v.IsReference = CheckRefInStyle(v.ID);
                }
            }

            VMOrder vMOrder = new VMOrder
            {
                DataList = await Task.Run(() => vData),
                DataListforCommercial = await Task.Run(() => vData)
            };
            return vMOrder;
        }
        public async Task<VMOrder> GetClosedOrder()
        {
            VMOrder vMOrder = new VMOrder
            {
                DataList = await Task.Run(() => _db.Merchandising_BuyerOrder.Where(x => x.Active == true && x.IsComplete == true)
                .Join(_db.Common_Buyer, a => a.Common_BuyerFK, b => b.ID, (t1, t2) => new VMOrder
                {
                    ID = t1.ID,
                    CID = t1.CID,
                    BuyerName = t2.Name,
                    Commission = t1.Commission,
                    BuyerPO = t1.BuyerPO,
                    Agent = t1.Agent,
                    InspectionAgent = t1.InspectionAgent,
                    OrderDate = t1.OrderDate,
                    Mkt_BuyerFK = t1.Common_BuyerFK,
                    BuyerPOValue = t1.BuyerOrderPOValue,
                    Remarks = t1.Remarks,
                    Season = t1.Season,
                    IsClosed = t1.IsComplete == false ? "Open" : "Closed"
                }).OrderByDescending(a => a.ID).ToList())

            };

            return vMOrder;
        }
        public async Task<int> OrderAdd(VMOrder vMOrder)
        {
            var result = -1;
            Merchandising_BuyerOrder bOrder = new Merchandising_BuyerOrder();
            bOrder.CID = CidServices.OrderCID(vMOrder.Mkt_BuyerFK, vMOrder.BuyerPO);
            bOrder.Common_BuyerFK = vMOrder.Mkt_BuyerFK;
            bOrder.BuyerPO = vMOrder.BuyerPO;
            bOrder.Commission = (decimal)vMOrder.Commission;
            bOrder.Remarks = vMOrder.Remarks;
            bOrder.Common_AgentFK = vMOrder.Common_AgentFK;
            bOrder.Common_InspectionAgentFK = vMOrder.Common_InspectionAgentFK;
            bOrder.OrderDate = vMOrder.OrderDate;
            bOrder.Season = vMOrder.Season;
            bOrder.UserID = vMOrder.UserID;
            bOrder.Time = DateTime.Today;
            bOrder.Active = true;
            bOrder.IsComplete = false;
            _db.Merchandising_BuyerOrder.Add(bOrder);

            if (await _db.SaveChangesAsync() > 0)
            {
                result = bOrder.ID;
            }
            return result;
        }
        public async Task<int> OrderEdit(VMOrder vMOrder)
        {
            var result = -1;

            Merchandising_BuyerOrder bOrder = _db.Merchandising_BuyerOrder.Find(vMOrder.ID);

            bOrder.Common_BuyerFK = vMOrder.Mkt_BuyerFK;
            bOrder.BuyerPO = vMOrder.BuyerPO;
            bOrder.Commission = (decimal)vMOrder.Commission;
            bOrder.Remarks = vMOrder.Remarks;
            bOrder.Common_AgentFK = vMOrder.Common_AgentFK;
            bOrder.Common_InspectionAgentFK = vMOrder.Common_InspectionAgentFK;
            bOrder.OrderDate = vMOrder.OrderDate;
            bOrder.Season = vMOrder.Season;
            bOrder.Time = DateTime.Today;
            bOrder.Active = true;
            bOrder.IsComplete = false;

            if (await _db.SaveChangesAsync() > 0)
            {
                result = bOrder.ID;
            }
            return result;
        }
        public async Task<int> OrderDelete(int id)
        {
            var result = -1;
            if (!_db.Merchandising_Style.Any(a => a.Merchandising_BuyerOrderFK == id && a.Active == true))
            {
                Merchandising_BuyerOrder selectSingle = _db.Merchandising_BuyerOrder.Find(id);
                if (selectSingle != null)
                {
                    selectSingle.Active = false;
                    selectSingle.Time = DateTime.Today;
                    if (await _db.SaveChangesAsync() > 0)
                    {
                        result = selectSingle.ID;
                    }
                }
            }

            return result;
        }
        public VMOrder OrderById(int id)
        {
            var vData = (from t1 in _db.Merchandising_BuyerOrder
                         join t2 in _db.Common_Buyer on t1.Common_BuyerFK equals t2.ID
                         join t3 in _db.Common_Agent on t1.Common_AgentFK equals t3.ID
                         join t4 in _db.Common_InspectionAgent on t1.Common_InspectionAgentFK equals t4.ID
                         where t1.Active == true && t1.ID == id
                         select new VMOrder
                         {
                             ID = t1.ID,
                             CID = t1.CID,
                             BuyerName = t2.Name,
                             Commission = t1.Commission,
                             BuyerPO = t1.BuyerPO,
                             //Agent = t1.Agent,
                             //InspectionAgent = t1.InspectionAgent,
                             Common_AgentFK = t3.ID,
                             Agent = t3.Name,
                             Common_InspectionAgentFK = t4.ID,
                             InspectionAgent = t4.Name,
                             OrderDate = t1.OrderDate,
                             Mkt_BuyerFK = t1.Common_BuyerFK,
                             Remarks = t1.Remarks,
                             Season = t1.Season,
                             BuyerPOValue = t1.BuyerOrderPOValue,
                             ActionId = (int)ActionEnum.Edit
                         }).FirstOrDefault();
            return vData;
        }
        public async Task<int> UpdateBuyerPOValue(int OrderID)
        {
            var result = -1;
            var vData = from t1 in _db.Merchandising_Style
                        where t1.Merchandising_BuyerOrderFK == OrderID && t1.Active == true
                        group new { t1.PackPrice, t1.PackQty } by new { t1.Merchandising_BuyerOrderFK } into all
                        select new
                        {
                            totalValue = all.Sum(a => a.PackPrice * a.PackQty)
                        };
            if (vData.Any())
            {
                Merchandising_BuyerOrder bOrder = _db.Merchandising_BuyerOrder.Find(OrderID);
                bOrder.BuyerOrderPOValue = vData.Sum(a => a.totalValue);
            }
            if (await _db.SaveChangesAsync() > 0)
            {
                result = OrderID;
            }
            return result;
        }

        #endregion

        #region ManageOrderStyle
        public async Task<VMOrderView> OrderViewId(int ID)
        {
            var VM = new VMOrderView();
            VM.vmStyle = new VMStyle();
            VM.vmOrder = new VMOrder();
            VM.vmOrder = OrderById(ID);
            VM.vmStyle = await StyleByOrderId(ID);
            return VM;
        }
        public async Task<VMStyle> GetClosedStyle()
        {
            VMStyle vMStyle = new VMStyle
            {
                DataList = await Task.Run(() => (from t1 in _db.Merchandising_Style
                                                 join t2 in _db.Merchandising_BuyerOrder on t1.Merchandising_BuyerOrderFK equals t2.ID
                                                 join t3 in _db.Common_FinishItem on t1.Common_FinishItemFK equals t3.ID
                                                 join t4 in _db.Common_FinishSubCategory on t3.Common_FinishSubCategoryFK equals t4.ID
                                                 join t5 in _db.Common_FinishCategory on t4.Common_FinishCategoryFK equals t5.ID
                                                 where t1.Active == true && t1.IsComplete == true
                                                 //join t6 in _db.Common_Unit on t3. equals t5.ID
                                                 select new VMStyle
                                                 {
                                                     ID = t1.ID,
                                                     CID = t1.CID,
                                                     FirstMove = t1.FirstMove,
                                                     UpdateDate = t1.Time,
                                                     Merchandising_BuyerOrderFK = t1.Merchandising_BuyerOrderFK,
                                                     Common_BuyerFK = t2.Common_BuyerFK,
                                                     Mkt_CategoryFK = t5.ID,
                                                     Mkt_SubCategoryFK = t4.ID,
                                                     Mkt_ItemFK = t1.Common_FinishItemFK,
                                                     ItemName = t3.Name,
                                                     Class = t1.Class,
                                                     Fabrication = t1.Fabrication,
                                                     Style = t1.StyleName,
                                                     SetQuantity = t1.SetQuantity,
                                                     PackQty = t1.PackQty,
                                                     PackPieceQty = t1.PackPieceQty,
                                                     PieceQty = t1.PieceQty,
                                                     UnitPrice = t1.UnitPrice,
                                                     PackPrice = t1.PackPrice,
                                                     Description = t1.Description,
                                                     POChangeHistory = t1.POChangeHistory,
                                                     IsComplete = false
                                                 }).OrderByDescending(a => a.ID).ToList())
            };

            return vMStyle;
        }
        public async Task<VMStyle> StyleByOrderId(int ID)
        {
            var vData = (from t1 in _db.Merchandising_Style
                         join t2 in _db.Merchandising_BuyerOrder on t1.Merchandising_BuyerOrderFK equals t2.ID
                         join t3 in _db.Common_FinishItem on t1.Common_FinishItemFK equals t3.ID
                         join t4 in _db.Common_FinishSubCategory on t3.Common_FinishSubCategoryFK equals t4.ID
                         join t5 in _db.Common_FinishCategory on t4.Common_FinishCategoryFK equals t5.ID
                         join t6 in _db.Common_Buyer on t2.Common_BuyerFK equals t6.ID
                         where t1.Active == true && t1.Merchandising_BuyerOrderFK == ID
                         select new VMStyle
                         {
                             ID = t1.ID,
                             CID = t1.CID+t1.StyleName,
                             FirstMove = t1.FirstMove,
                             UpdateDate = t1.Time,
                             Merchandising_BuyerOrderFK = t1.Merchandising_BuyerOrderFK,
                             Common_BuyerFK = t2.Common_BuyerFK,
                             Mkt_CategoryFK = t5.ID,
                             Mkt_SubCategoryFK = t4.ID,
                             Mkt_ItemFK = t1.Common_FinishItemFK,
                             ItemName = t3.Name,
                             Class = t1.Class,
                             Fabrication = t1.Fabrication,
                             Reference = t1.ReferenceNo,
                             Style = t1.StyleName,
                             SetQuantity = t1.SetQuantity,
                             SetPackCarton = t1.PackInCTN,
                             PackQty = t1.PackQty,
                             PackPieceQty = t1.PackPieceQty,
                             PieceQty = t1.PieceQty,
                             UnitPrice = t1.UnitPrice,
                             PackPrice = t1.PackPrice,
                             StylePrice = decimal.Multiply(t1.PackPrice, t1.PackQty),
                             Description = t1.Description,
                             POChangeHistory = t1.POChangeHistory,
                             IsComplete = t1.IsComplete
                         }).OrderByDescending(a => a.ID).ToList();
            if (vData.Any())
            {
                foreach (var v in vData)
                {
                    v.SizeName = GetSizeRangeByStyleID(v.ID);
                    v.RefShipment = CheckShipmentByStyleID(v.ID) == true ? "label-success" : "label-danger";
                    v.RefBOM = CheckBomByStyleID(v.ID) == true ? "label-success" : "label-danger";
                    v.RefSetPack = CheckSetPackByStyleID(v.ID) == true ? "label-success" : "label-danger";
                    v.RefSMV = CheckSMVByStyleID(v.ID) == true ? "label-success" : "label-danger";
                    v.RefBOF = CheckBOFByStyleID(v.ID) == true ? "label-success" : "label-danger";
                    v.IsCanDelete = CheckStyleRef(v.ID);
                }
            }
            VMStyle vMStyle = new VMStyle
            {
                DataList = await Task.Run(() => vData)
            };
            return vMStyle;
        }

        public VMStyle GetAllStyle()
        {
            var vData = (from t1 in _db.Merchandising_Style
                         join t2 in _db.Merchandising_BuyerOrder on t1.Merchandising_BuyerOrderFK equals t2.ID
                         join t3 in _db.Common_FinishItem on t1.Common_FinishItemFK equals t3.ID
                         join t4 in _db.Common_FinishSubCategory on t3.Common_FinishSubCategoryFK equals t4.ID
                         join t5 in _db.Common_FinishCategory on t4.Common_FinishCategoryFK equals t5.ID
                         join t6 in _db.Common_Buyer on t2.Common_BuyerFK equals t6.ID
                         where t1.Active == true
                         select new VMStyle
                         {
                             ID = t1.ID,
                             CID = t1.CID + t1.StyleName,
                             FirstMove = t1.FirstMove,
                             UpdateDate = t1.Time,
                             Merchandising_BuyerOrderFK = t1.Merchandising_BuyerOrderFK,
                             Common_BuyerFK = t2.Common_BuyerFK,
                             Mkt_CategoryFK = t5.ID,
                             Mkt_SubCategoryFK = t4.ID,
                             Mkt_ItemFK = t1.Common_FinishItemFK,
                             ItemName = t3.Name,
                             Class = t1.Class,
                             Fabrication = t1.Fabrication,
                             Reference = t1.ReferenceNo,
                             Style = t1.StyleName,
                             SetQuantity = t1.SetQuantity,
                             SetPackCarton = t1.PackInCTN,
                             PackQty = t1.PackQty,
                             PackPieceQty = t1.PackPieceQty,
                             PieceQty = t1.PieceQty,
                             UnitPrice = t1.UnitPrice,
                             PackPrice = t1.PackPrice,
                             StylePrice = decimal.Multiply(t1.PackPrice, t1.PackQty),
                             Description = t1.Description,
                             POChangeHistory = t1.POChangeHistory,
                             IsComplete = t1.IsComplete
                         }).OrderByDescending(a => a.ID).ToList();
            if (vData.Any())
            {
                foreach (var v in vData)
                {
                    v.SizeName = GetSizeRangeByStyleID(v.ID);
                    //v.RefShipment = CheckShipmentByStyleID(v.ID) == true ? "label-success" : "label-danger";
                    //v.RefBOM = CheckBomByStyleID(v.ID) == true ? "label-success" : "label-danger";
                    //v.RefSetPack = CheckSetPackByStyleID(v.ID) == true ? "label-success" : "label-danger";
                    //v.RefSMV = CheckSMVByStyleID(v.ID) == true ? "label-success" : "label-danger";
                    //v.RefBOF = CheckBOFByStyleID(v.ID) == true ? "label-success" : "label-danger";
                    //v.IsCanDelete = CheckStyleRef(v.ID);
                }
            }
            VMStyle vMStyle = new VMStyle
            {
                DataList = vData
            };
            return vMStyle;
        }

        public async Task<int> StyleAdd(VMStyle vMStyle)
        {
            var result = -1;
            Merchandising_Style oStyle = new Merchandising_Style();
            oStyle.CID = CidServices.GetStyleCID(vMStyle.Merchandising_BuyerOrderFK, vMStyle.Mkt_ItemFK);
            oStyle.FirstMove = DateTime.Today;
            oStyle.UpdateDate = DateTime.Today;
            oStyle.Merchandising_BuyerOrderFK = vMStyle.Merchandising_BuyerOrderFK;
            oStyle.Common_FinishItemFK = vMStyle.Mkt_ItemFK;
            oStyle.Class = vMStyle.Class;
            oStyle.Fabrication = vMStyle.Fabrication;
            oStyle.StyleName = vMStyle.Style;
            oStyle.ReferenceNo = vMStyle.Reference;
            oStyle.SetQuantity = vMStyle.SetQuantity;
            oStyle.PackInCTN = vMStyle.SetPackCarton;
            oStyle.PackQty = vMStyle.PackQty;
            oStyle.PackPieceQty = vMStyle.PackPieceQty;
            oStyle.PieceQty = (int)decimal.Multiply(vMStyle.PackPieceQty, vMStyle.PackQty);
            oStyle.UnitPrice = decimal.Divide(vMStyle.PackPrice, vMStyle.PackPieceQty);
            oStyle.PackPrice = vMStyle.PackPrice;
            oStyle.Description = vMStyle.Description;
            oStyle.POChangeHistory = vMStyle.POChangeHistory;
            oStyle.IsComplete = false;
            oStyle.UserID = vMStyle.UserID;
            oStyle.Time = vMStyle.Time;
            _db.Merchandising_Style.Add(oStyle);

            List<Merchandising_StyleMeasurement> lstMeasurement = new List<Merchandising_StyleMeasurement>();
            if (vMStyle.SizeIDs.Any())
            {
                int ColorId = 0;
                int SizeId = 0;
                foreach (var vSize in vMStyle.SizeIDs)
                {
                    SizeId = 0;
                    int.TryParse(vSize, out SizeId);
                    foreach (var vColor in vMStyle.ColorIDs)
                    {
                        ColorId = 0;
                        int.TryParse(vColor, out ColorId);
                        Merchandising_StyleMeasurement osMeas = new Merchandising_StyleMeasurement();
                        osMeas.Merchandising_StyleFK = oStyle.ID;
                        if (ColorId > 0)
                        {
                            osMeas.Common_ColorFK = ColorId;
                        }

                        if (SizeId > 0)
                        {
                            osMeas.Common_SizeFK = SizeId;
                        }
                        lstMeasurement.Add(osMeas);
                    }
                }
                _db.Merchandising_StyleMeasurement.AddRange(lstMeasurement);
            }

            if (await _db.SaveChangesAsync() > 0)
            {
                await UpdateBuyerPOValue(vMStyle.Merchandising_BuyerOrderFK);
                result = vMStyle.Merchandising_BuyerOrderFK;
            }
            return result;
        }
        public async Task<int> StyleEdit(VMStyle vMStyle)
        {
            var result = -1;

            Merchandising_Style oStyle = _db.Merchandising_Style.Find(vMStyle.ID);
            oStyle.Merchandising_BuyerOrderFK = vMStyle.Merchandising_BuyerOrderFK;
            oStyle.Common_FinishItemFK = vMStyle.Mkt_ItemFK;
            oStyle.Class = vMStyle.Class;
            oStyle.Fabrication = vMStyle.Fabrication;
            oStyle.StyleName = vMStyle.Style;
            oStyle.ReferenceNo = vMStyle.Reference;
            oStyle.SetQuantity = vMStyle.SetQuantity;
            oStyle.PackQty = vMStyle.PackQty;
            oStyle.PackPieceQty = vMStyle.PackPieceQty;
            oStyle.PackInCTN = vMStyle.SetPackCarton;
            oStyle.PieceQty = (int)decimal.Multiply(vMStyle.PackPieceQty, vMStyle.PackQty);
            oStyle.UnitPrice = decimal.Divide(vMStyle.PackPrice, vMStyle.PackPieceQty);
            oStyle.PackPrice = vMStyle.PackPrice;
            oStyle.Description = vMStyle.Description;
            oStyle.POChangeHistory = vMStyle.POChangeHistory;
            oStyle.Time = DateTime.Today;
            oStyle.IsComplete = false;

            var vStyleSizeColor = _db.Merchandising_StyleMeasurement.Where(a => a.Merchandising_StyleFK == vMStyle.ID && a.Active == true);

            List<Merchandising_StyleMeasurement> lstMeasurement = new List<Merchandising_StyleMeasurement>();
            if (vStyleSizeColor.Any())
            {
                foreach (var v in vStyleSizeColor)
                {
                    if (!vMStyle.SizeIDs.Any(a => a.Equals(v.Common_SizeFK.ToString())))
                    {
                        //notfound
                        v.Active = false;
                    }
                    if (!vMStyle.ColorIDs.Any(a => a.Equals(v.Common_ColorFK.ToString())))
                    {
                        //notfound
                        v.Active = false;
                    }
                }
            }

            if (vMStyle.SizeIDs.Any())
            {
                int ColorId = 0;
                int SizeId = 0;
                foreach (var vSize in vMStyle.SizeIDs)
                {
                    SizeId = 0;
                    int.TryParse(vSize, out SizeId);
                    foreach (var vColor in vMStyle.ColorIDs)
                    {
                        ColorId = 0;
                        int.TryParse(vColor, out ColorId);

                        if (!vStyleSizeColor.Any(a => a.Common_ColorFK == ColorId && a.Common_SizeFK == SizeId))
                        {
                            Merchandising_StyleMeasurement osMeas = new Merchandising_StyleMeasurement();
                            osMeas.Merchandising_StyleFK = oStyle.ID;

                            if (ColorId > 0)
                            {
                                osMeas.Common_ColorFK = ColorId;
                            }

                            if (SizeId > 0)
                            {
                                osMeas.Common_SizeFK = SizeId;
                            }
                            osMeas.Active = true;
                            lstMeasurement.Add(osMeas);
                        }
                    }
                }
                _db.Merchandising_StyleMeasurement.AddRange(lstMeasurement);
            }

            if (await _db.SaveChangesAsync() > 0)
            {
                await UpdateBuyerPOValue(vMStyle.Merchandising_BuyerOrderFK);
                result = vMStyle.Merchandising_BuyerOrderFK;
            }
            return result;
        }
        public async Task<int> StyleDelete(int Id)
        {
            var result = -1;
            var vCheckBOM = _db.Merchandising_StyleSlave.Any(a => a.Merchandising_StyleFK == Id && a.Active == true);
            var vCheckShipment = _db.Merchandising_StyleShipmentSchedule.Any(a => a.Merchandising_StyleFK == Id && a.Active == true);
            //var vCheckCBS = _db.Merchandising_CBS.Any(a => a.Merchandising_CBSStyleFK == Id && a.Active == true);
            if (!vCheckBOM && !vCheckShipment)
            {
                Merchandising_Style oStyle = _db.Merchandising_Style.Find(Id);

                if (oStyle != null)
                {
                    oStyle.Active = false;
                    if (await _db.SaveChangesAsync() > 0)
                    {
                        await UpdateBuyerPOValue(oStyle.Merchandising_BuyerOrderFK);
                        result = oStyle.Merchandising_BuyerOrderFK;
                    }
                }
            }

            return result;
        }
        public async Task<int> StyleCMOverheadUpdate(VMStyle vMStyle)
        {
            var result = -1;
            Merchandising_Style cBS = _db.Merchandising_Style.Find(vMStyle.ID);
            cBS.OverheadRate = vMStyle.OverheadRate;
            cBS.MakingCost = vMStyle.MakingCost;
            cBS.Active = true;
            cBS.Time = DateTime.Now;

            if (await _db.SaveChangesAsync() > 0)
            {
                result = vMStyle.ID;
            }
            return result;
        }

        public VMStyle StyleById(int Id)
        {
            var vData = (from t1 in _db.Merchandising_Style
                         join t2 in _db.Merchandising_BuyerOrder on t1.Merchandising_BuyerOrderFK equals t2.ID
                         join t3 in _db.Common_FinishItem on t1.Common_FinishItemFK equals t3.ID
                         join t4 in _db.Common_FinishSubCategory on t3.Common_FinishSubCategoryFK equals t4.ID
                         join t5 in _db.Common_FinishCategory on t4.Common_FinishCategoryFK equals t5.ID
                         join t6 in _db.Common_Buyer on t2.Common_BuyerFK equals t6.ID
                         join t7 in _db.Merchandising_StyleSetPack on t1.ID equals t7.Merchandising_StyleFK into Style_Join
                         from t8 in Style_Join.DefaultIfEmpty()
                         where t1.ID == Id && t1.Active == true
                         select new VMStyle
                         {
                             ID = t1.ID,
                             CID = t1.CID + t1.StyleName,
                             FirstMove = t1.FirstMove,
                             UpdateDate = t1.Time,
                             Merchandising_BuyerOrderFK = t1.Merchandising_BuyerOrderFK,
                             Common_BuyerFK = t2.Common_BuyerFK,
                             OrderBuyingCommission = t2.Commission,
                             ItemName = t3.Name,
                             Mkt_CategoryFK = t5.ID,
                             Mkt_SubCategoryFK = t4.ID,
                             Mkt_ItemFK = t1.Common_FinishItemFK,
                             Class = t5.Name,
                             Fabrication = t1.Fabrication,
                             Reference = t1.ReferenceNo,
                             Style = t1.StyleName,
                             SetQuantity = t1.SetQuantity,
                             PackQty = t1.PackQty,
                             PackPieceQty = t1.PackPieceQty,
                             PieceQty = t1.PieceQty,
                             //ProductionPieceQty = (int)decimal.Multiply(decimal.Multiply(t1.PackQty, t1.PackPieceQty), t1.SetQuantity),
                             SetPackCarton = t1.PackInCTN,
                             DozonQty = decimal.Divide(t1.PieceQty, 12),
                             BuyerPO = t2.BuyerPO,
                             OrderDate = t2.OrderDate,
                             Season = t2.Season,
                             BuyerName = t6.Name,
                             UnitPrice = t1.UnitPrice,
                             PackPrice = t1.PackPrice,
                             StylePrice = decimal.Multiply(t1.PackPrice, t1.PackQty),
                             Description = t1.Description,
                             POChangeHistory = t1.POChangeHistory,
                             MakingCost = t1.MakingCost,
                             OverheadRate = t1.OverheadRate,
                             UserID = t1.UserID,
                             IsComplete = t1.IsComplete,
                             ActionId = (int)ActionEnum.Edit
                         }).FirstOrDefault();

            var SizeRange = (from t1 in _db.Merchandising_StyleMeasurement
                             where t1.Merchandising_StyleFK == Id && t1.Active == true
                             join t2 in _db.Common_Size on t1.Common_SizeFK equals t2.ID
                             join t3 in _db.Common_Color on t1.Common_ColorFK equals t3.ID
                             where t1.Active == true
                             select new
                             {
                                 SizeId = t1.Common_SizeFK,
                                 ColorId = t1.Common_ColorFK,
                                 Size = t2.Name,
                                 Color = t3.Name
                             }).ToList();
            var SubSet = _db.Merchandising_StyleSetPack.Where(a => a.Merchandising_StyleFK == Id && a.Active == true);
            if (SubSet.Any())
            {
                vData.ProductionPieceQty = (int)decimal.Multiply(decimal.Multiply(vData.PackQty, vData.PackPieceQty), SubSet.Sum(a => a.Quantity));
            }
            if (SizeRange.Any())
            {
                var vSize = SizeRange.GroupBy(a => a.SizeId, (key, group) => new { SizeId = key, Items = group.ToList() }).ToList();
                if (vSize.Any())
                {
                    string[] SizeIds = new string[vSize.Count];
                    string SizeName = string.Empty;
                    for (int i = 0; i < vSize.Count; i++)
                    {
                        SizeIds[i] = vSize[i].SizeId.ToString();
                        SizeName += vSize[i].Items.FirstOrDefault().Size + " ";
                    }
                    MultiSelectList lstSize = new MultiSelectList(SizeDropDownList(), "Value", "Text", SizeIds);
                    vData.SizeList = lstSize;
                    vData.SizeName = SizeName;
                }

                var vColor = SizeRange.GroupBy(a => a.ColorId, (key, group) => new { ColorId = key, Items = group.ToList() }).ToList();
                if (vColor.Any())
                {
                    string[] ColorIds = new string[vColor.Count];
                    for (int i = 0; i < vColor.Count; i++)
                    {
                        ColorIds[i] = vColor[i].ColorId.ToString();
                    }
                    MultiSelectList lstColor = new MultiSelectList(ColorDropDownList(), "Value", "Text", ColorIds);
                    vData.ColorList = lstColor;
                }
            }
            else
            {
                MultiSelectList lstSize = new MultiSelectList(SizeDropDownList(), "Value", "Text");
                vData.SizeList = lstSize;
                MultiSelectList lstColor = new MultiSelectList(ColorDropDownList(), "Value", "Text");
                vData.ColorList = lstColor;
            }
            var vUser = _db.User_User.Where(a => a.ID == vData.UserID);
            vData.User = vUser.Any() == true ? vUser.FirstOrDefault().Name : "No User";
            return vData;
        }

        public List<VMBOFView> GetItemForBOF(int styleId, int userId)
        {
            var insertedYarn = GetInsertedYarn(styleId);


            decimal? lycrarequiredQuantity = (from t1 in _db.Merchandising_BOF
                                              join t2 in _db.Common_RawItem on t1.Common_RawItemFK equals t2.ID
                                              where t1.Active == true && t2.Common_RawSubCategoryFK == 2044
                                              select t1.RequiredQuantity).DefaultIfEmpty(0).Sum();


            var yarns = (from t1 in _db.Merchandising_YarnCalculation
                         join t2 in _db.Common_RawItem on t1.Raw_ItemFK equals t2.ID
                         join t3 in _db.Common_Color on t1.Common_ColorFK equals t3.ID
                         where t1.Active == true && t1.IsApprove == true
                         && t1.Merchandising_StyleFk == styleId
                         && !insertedYarn.Contains(t1.ID)
                         select new VMYarnCalculation
                         {
                             ItemName = t2.Name,
                             Color = t3.Name,
                             Common_ColorFK = t3.ID,
                             Combo = t1.Combo,
                             Common_CurrencyFK = t1.Common_CurrencyFK,
                             Common_UnitFK = t1.Common_UnitFK,
                             Fabrication = t1.Fabrication,
                             FinishDIA = t1.FinishDIA,
                             GSM = t1.GSM,
                             ID = t1.ID,
                             Merchandising_StyleSlaveFK_Fabric = t1.Merchandising_StyleSlaveFK_Fabric,
                             Merchandising_StyleFk = t1.Merchandising_StyleFk,
                             Merchandising_YarnTypeFk = t1.Merchandising_YarnTypeFk,
                             Raw_ItemFK = t1.Raw_ItemFK,
                             ReferenceNo = t1.ReferenceNo,
                             User = t1.User,
                             IsActive = t1.Active,
                             IsApproved = t1.IsApprove,
                             ProcessLoss = t1.ProcessLoss,
                             Lycra = t1.Lycra,
                             Consumption = t1.Consumption,
                             OrderQuantityPCS = t1.OrderQuantityPCS,
                             Price = t1.Price,
                             IsNeedExtraWashing = false// t1.IsNeedExtraWashing ///it should be configure after discution with Shah Alom Bhai


                         }).ToList();

            var unit = yarns.Count() > 0 ? yarns.FirstOrDefault().Common_UnitFK : 0;
            //VmMkt_BOM vmMkt_BOM = new VmMkt_BOM();

            string lycraValue = GetLycraValue(styleId);


            /*This requirement is approved by Mohsin charman of ROMO
             =======================================================


                Solid: 1 Yarn 100%
                       2 Knitting 100%
                       3 Dying 100%
                       4 No
                       5 Extra Finish

           Yarn Dying: 1 Yarn 100%
                       2 Yarn Dying 100%
                       3 Knitting 100% editable
                       4 Yarn Dying Washing 100% of Knitting
                       5 Extra Finish

                  AOP: 1 Yarn 100%
                       2 Knitting 100%
                       3 Dying for AOP 100%
                       4 AOP 100% editable
                       5 Extra Finish */
            //var lycra = _db.Merchandising_BOF.Join(_db.Common_RawItem,
            //                merchandisingBOF => merchandisingBOF.Common_RawItemFK, commonRawItem => commonRawItem.ID,
            //                (merchandisingBOF, commonRawItem) => new VMBOF
            //                {
            //                    ID = merchandisingBOF.ID,
            //                    RawItemName = commonRawItem.Name
            //                }).ToList();
            var lycra = (from t1 in _db.Merchandising_BOF
                         join t2 in _db.Common_RawItem on t1.Common_RawItemFK equals t2.ID
                         where t1.Active == true && t1.Merchandising_StyleFK == styleId && t2.Common_RawSubCategoryFK == 2044
                         select new
                         {
                             BOFID = t1.ID
                         }).FirstOrDefault();
            List<VMBOFView> BOFList = new List<VMBOFView>();
            var itemList = _db.Common_RawItem.AsEnumerable();
            if (yarns.Count() > 0)
            {
                // Serial 1
                foreach (var x in yarns)
                {
                    VMBOFView BOF_1 = new VMBOFView();
                    BOF_1.Merchandising_StyleFK = x.Merchandising_StyleFk;
                    if (x.Common_CurrencyFK != 0)
                        BOF_1.Common_CurrencyFK = x.Common_CurrencyFK;
                    BOF_1.Common_UnitFK = x.Common_UnitFK;
                    BOF_1.RawItem = x.ItemName;
                    //if (x.Merchandising_YarnCalculation.Common_SupplierFK != null)
                    //    x.Merchandising_StyleSlave.Common_SupplierFK = x.Merchandising_YarnCalculation.Common_SupplierFK.Value;
                    //if (x.Merchandising_YarnCalculation.Common_UnitFK != null)
                    //    merchandising_BOF.Common_UnitFK = x.Merchandising_YarnCalculation.Common_UnitFK.Value;
                    BOF_1.Merchandising_StyleSlaveFK_Fabric = x.Merchandising_StyleSlaveFK_Fabric;
                    BOF_1.Merchandising_YarnCalculationFK = x.ID;
                    BOF_1.Consumption = x.Consumption;
                    BOF_1.Description = x.Fabrication + "-GSM:" + x.GSM + "-Dia:" + x.FinishDIA + "-Color:" + x.Color;
                    if (x.Raw_ItemFK != 0)
                    {
                        BOF_1.Common_RawItemFK = x.Raw_ItemFK;
                    }

                    BOF_1.RequiredQuantity = decimal.Subtract(decimal.Divide(((decimal.Multiply((decimal.Divide(x.OrderQuantityPCS, 12)), x.Consumption))), (decimal.Subtract(decimal.Divide(100, 100), decimal.Multiply((decimal.Divide(x.ProcessLoss, 100)), 1)))), decimal.Multiply(decimal.Divide(decimal.Divide(((decimal.Multiply((decimal.Divide(x.OrderQuantityPCS, 12)), x.Consumption))), (decimal.Subtract(decimal.Divide(100, 100), decimal.Multiply((decimal.Divide(x.ProcessLoss, 100)), 1)))), 100), x.Lycra));
                    BOF_1.Price = x.Price;
                    BOF_1.UserID = userId;
                    BOF_1.User = userId.ToString();

                    BOFList.Add(BOF_1);
                    //_db.Merchandising_BOF.Add(merchandising_BOF);
                    //_db.SaveChangesAsync();


                    // Serial 2
                    VMBOFView BOF_2 = new VMBOFView();
                    BOF_2.Merchandising_StyleFK = x.Merchandising_StyleFk;
                    if (x.Common_CurrencyFK != 0)
                        BOF_2.Common_CurrencyFK = x.Common_CurrencyFK;
                    //merchandising_BOF.Common_SupplierFK = 1;
                    //if (x.Merchandising_YarnCalculation.Common_UnitFK != null)
                    BOF_2.Merchandising_StyleSlaveFK_Fabric = x.Merchandising_StyleSlaveFK_Fabric;
                    BOF_2.Common_UnitFK = x.Common_UnitFK;
                    BOF_2.Merchandising_YarnCalculationFK = x.ID;
                    BOF_2.Consumption = x.Consumption;
                    BOF_2.Description = x.Fabrication + "-GSM:" + x.GSM + "-Dia:" + x.FinishDIA + "-Color:" + x.Color;
                    if (x.Merchandising_YarnTypeFk == 1)
                    {
                        if (x.Raw_ItemFK != 0)
                        {
                            BOF_2.Common_RawItemFK = 5484; //Knitting  
                            BOF_2.RawItem = itemList.Where(o => o.ID == 5484).Select(o => o.Name).FirstOrDefault();

                        }
                    }
                    else if (x.Merchandising_YarnTypeFk == 2)
                    {
                        if (x.Raw_ItemFK != 0)
                        {
                            BOF_2.Common_RawItemFK = 5488; //Yarn Dyeing
                            BOF_2.RawItem = itemList.Where(o => o.ID == 5488).Select(o => o.Name).FirstOrDefault();
                        }
                    }
                    else if (x.Merchandising_YarnTypeFk == 3)
                    {
                        if (x.Raw_ItemFK != 0)
                        {
                            BOF_2.Common_RawItemFK = 5485; //Knitting for AOP
                            BOF_2.RawItem = itemList.Where(o => o.ID == 5485).Select(o => o.Name).FirstOrDefault();
                        }
                    }
                    BOF_2.RequiredQuantity = decimal.Divide(((decimal.Multiply((decimal.Divide(x.OrderQuantityPCS, 12)), x.Consumption))), (decimal.Subtract(decimal.Divide(100, 100), decimal.Multiply((decimal.Divide(x.ProcessLoss, 100)), 1))));
                    BOF_2.Price = 0;
                    BOF_2.UserID = userId;
                    BOF_2.User = userId.ToString();

                    BOFList.Add(BOF_2);

                    // Serial 3
                    VMBOFView BOF_3 = new VMBOFView();

                    BOF_3.Merchandising_StyleFK = x.Merchandising_StyleFk;
                    if (x.Common_CurrencyFK != 0)
                        BOF_3.Common_CurrencyFK = x.Common_CurrencyFK;

                    //merchandising_BOF.Common_SupplierFK = 1;
                    //if (x.Merchandising_YarnCalculation.Common_UnitFK != null)
                    BOF_3.Merchandising_StyleSlaveFK_Fabric = x.Merchandising_StyleSlaveFK_Fabric;
                    BOF_3.Common_UnitFK = x.Common_UnitFK;

                    BOF_3.Merchandising_YarnCalculationFK = x.ID;
                    BOF_3.Consumption = x.Consumption;
                    BOF_3.Description = x.Fabrication + "-GSM:" + x.GSM + "-Dia:" + x.FinishDIA + "-Color:" + x.Color;
                    if (x.Merchandising_YarnTypeFk == 1)
                    {
                        if (x.Raw_ItemFK != 0)
                        {
                            BOF_3.Common_RawItemFK = 5487; // Dyeing
                            BOF_3.RawItem = itemList.Where(o => o.ID == 5487).Select(o => o.Name).FirstOrDefault();

                            BOF_3.IsFabric = true; // Dyeing

                        }
                    }
                    else if (x.Merchandising_YarnTypeFk == 2)
                    {
                        if (x.Raw_ItemFK != 0)
                        {
                            BOF_3.Common_RawItemFK = 5486; //Yarn Dyeing Knitting
                            BOF_3.RawItem = itemList.Where(o => o.ID == 5486).Select(o => o.Name).FirstOrDefault();

                        }
                    }
                    else if (x.Merchandising_YarnTypeFk == 3)
                    {
                        if (x.Raw_ItemFK != 0)
                        {
                            BOF_3.Common_RawItemFK = 5489; //Dyeing for AOP
                            BOF_3.RawItem = itemList.Where(o => o.ID == 5489).Select(o => o.Name).FirstOrDefault();

                        }
                    }
                    BOF_3.RequiredQuantity = decimal.Divide(((decimal.Multiply((decimal.Divide(x.OrderQuantityPCS, 12)), x.Consumption))), (decimal.Subtract(decimal.Divide(100, 100), decimal.Multiply((decimal.Divide(x.ProcessLoss, 100)), 1))));
                    BOF_3.Price = 0;
                    BOF_3.UserID = userId;
                    BOF_3.User = userId.ToString();

                    BOFList.Add(BOF_3);
                    // Serial 4
                    if (x.Merchandising_YarnTypeFk != 1)
                    {
                        VMBOFView BOF_4 = new VMBOFView();
                        BOF_4.Merchandising_StyleFK = x.Merchandising_StyleFk;
                        if (x.Common_CurrencyFK != 0)
                            BOF_4.Common_CurrencyFK = x.Common_CurrencyFK;

                        //merchandising_BOF.Common_SupplierFK = 1;
                        //if (x.Merchandising_YarnCalculation.Common_UnitFK != null)
                        BOF_4.Merchandising_StyleSlaveFK_Fabric = x.Merchandising_StyleSlaveFK_Fabric;
                        BOF_4.Common_UnitFK = x.Common_UnitFK;

                        BOF_4.Merchandising_YarnCalculationFK = x.ID;
                        BOF_4.Consumption = x.Consumption;
                        BOF_4.Description = x.Fabrication + "-GSM:" + x.GSM + "-Dia:" + x.FinishDIA + "-Color:" + x.Color;

                        if (x.Merchandising_YarnTypeFk == 2)
                        {
                            if (x.Raw_ItemFK != 0)
                            {
                                BOF_4.Common_RawItemFK = 5490; //yarn Dyeing Washing
                                BOF_4.RawItem = itemList.Where(o => o.ID == 5490).Select(o => o.Name).FirstOrDefault();

                                BOF_4.IsFabric = true; //yarn Dyeing Washing

                            }
                        }
                        else if (x.Merchandising_YarnTypeFk == 3)
                        {
                            if (x.Raw_ItemFK != 0)
                            {
                                BOF_4.Common_RawItemFK = 5491; //All Over Print (AOP)
                                BOF_4.RawItem = itemList.Where(o => o.ID == 5491).Select(o => o.Name).FirstOrDefault();

                                BOF_4.IsFabric = true; //All Over Print (AOP)

                            }
                        }
                        BOF_4.RequiredQuantity = decimal.Divide(((decimal.Multiply((decimal.Divide(x.OrderQuantityPCS, 12)), x.Consumption))), (decimal.Subtract(decimal.Divide(100, 100), decimal.Multiply((decimal.Divide(x.ProcessLoss, 100)), 1))));
                        BOF_4.Price = 0;
                        BOF_4.UserID = userId;
                        BOF_4.User = userId.ToString();

                        BOFList.Add(BOF_4);

                        //_db.Merchandising_BOF.Add(merchandising_BOF);
                        //_db.SaveChangesAsync();
                    }

                    // Serial 5
                    if (x.IsNeedExtraWashing)
                    {
                        VMBOFView BOF_5 = new VMBOFView();
                        BOF_5.Merchandising_StyleFK = x.Merchandising_StyleFk;
                        if (x.Common_CurrencyFK != 0)
                            BOF_5.Common_CurrencyFK = x.Common_CurrencyFK;

                        //merchandising_BOF.Common_SupplierFK = 1;
                        //if (x.Merchandising_YarnCalculation.Common_UnitFK != null)
                        BOF_5.Merchandising_StyleSlaveFK_Fabric = x.Merchandising_StyleSlaveFK_Fabric;

                        BOF_5.Common_UnitFK = x.Common_UnitFK;

                        BOF_5.Merchandising_YarnCalculationFK = x.ID;
                        BOF_5.Consumption = x.Consumption;
                        BOF_5.Description = x.Fabrication + "-GSM:" + x.GSM + "-Dia:" + x.FinishDIA + "-Color:" + x.Color;


                        if (x.Raw_ItemFK != 0)
                        {
                            BOF_5.Common_RawItemFK = 5719; //Normal Washing
                            BOF_5.RawItem = itemList.Where(o => o.ID == 5719).Select(o => o.Name).FirstOrDefault();

                            BOF_5.IsFabric = true; //Normal Washing

                        }


                        BOF_5.RequiredQuantity = decimal.Divide(((decimal.Multiply((decimal.Divide(x.OrderQuantityPCS, 12)), x.Consumption))), (decimal.Subtract(decimal.Divide(100, 100), decimal.Multiply((decimal.Divide(x.ProcessLoss, 100)), 1))));
                        BOF_5.Price = 0;
                        BOF_5.UserID = userId;
                        BOF_5.User = userId.ToString();

                        BOFList.Add(BOF_5);

                        //_db.Merchandising_BOF.Add(merchandising_BOF);
                        //_db.SaveChangesAsync();
                    }

                }


            }
            if (lycra != null)
            {
                VMBOFView BOF_X = new VMBOFView();
                BOF_X.Merchandising_BOF = _db.Merchandising_BOF.Where(x => x.ID == lycra.BOFID).FirstOrDefault();
                BOF_X.RequiredQuantity = Convert.ToDecimal(lycraValue);

            }
            else
            {
                if (Convert.ToDecimal(lycraValue) > 0)
                {

                    VMBOFView BOF_6 = new VMBOFView();


                    BOF_6.Merchandising_StyleFK = styleId;
                    BOF_6.Common_CurrencyFK = 1;
                    BOF_6.Common_UnitFK = unit;
                    BOF_6.Merchandising_StyleSlaveFK_Fabric = 0;
                    //vmBOFView.Merchandising_StyleSlave.Common_UnitFK = 13;
                    BOF_6.Merchandising_YarnCalculationFK = 0;
                    BOF_6.Consumption = 0;
                    BOF_6.Description = "Lycra Yarn 20D";

                    BOF_6.Common_RawItemFK = 5331;
                    BOF_6.RawItem = itemList.Where(o => o.ID == 5331).Select(o => o.Name).FirstOrDefault();

                    BOF_6.RequiredQuantity = Convert.ToDecimal(lycraValue);
                    BOF_6.Price = 0;
                    BOF_6.UserID = userId;
                    BOF_6.User = userId.ToString();
                    BOFList.Add(BOF_6);
                }
            }
            return BOFList;


        }

        public VMBOF GetSingleBOF(int iD)
        {
            var v = (from t1 in _db.Merchandising_BOF
                     join t2 in _db.Common_Unit on t1.Common_UnitFK equals t2.ID
                     where t1.ID == iD
                     select new VMBOF
                     {
                         ID = t1.ID,
                         IsFabric = t1.IsFabric,
                         Merchandising_StyleFK = t1.Merchandising_StyleFK,
                         Merchandising_YarnCalculationFK = t1.Merchandising_YarnCalculationFK,
                         Price = t1.Price,

                         RequiredQuantity = t1.RequiredQuantity,

                         UserID = t1.UserID,
                         Common_CurrencyFK = t1.Common_CurrencyFK,
                         Common_RawItemFK = t1.Common_RawItemFK,
                         Common_UnitFK = t1.Common_UnitFK,
                         Consumption = t1.Consumption,
                         Description = t1.Description,
                         UnitName = t2.Name
                     }).FirstOrDefault();
            return v;
        }

        public VMStyleView StyleGetByIDView(int ID)
        {
            var listofcolor = new MultiSelectList(ColorDropDownList(), "Value", "Text");
            var VM = new VMStyleView();
            VM.vmStyle = new VMStyle();
            VM.vmStyle = StyleById(ID);
            VM.VMCBSSummary = GetBOMSummaryByStyleID(VM.vmStyle);
            VM.IsBomCreate = CheckBOMCreateByStyleID(ID);
            VM.ColorList = listofcolor;
            return VM;
        }
        public async Task<VMStyleView> StyleShipmentViewId(int ID)
        {
            var VM = new VMStyleView();
            VM.vmStyle = new VMStyle();
            VM.vmStyleSetPack = new VMStyleSetPack();
            VM.vmColorSizeRatio = new List<VMShipmentColorSize>();
            VM.vmColorSizeSummary = new List<VMColorSize>();
            VM.vmStyle = StyleById(ID);
            VM.vmStyleSetPack = await SetPackByStyleID(ID);
            VM.vmShipmentSchedule = await ShipmentByStyleId(ID);
            VM.vmColorSizeRatio = ShipmentSizeRatioByStyleId(ID).DataList.ToList();
            VM.vmColorSizeSummary = GetColorSizeSummaryByStyleID(ID);
            return VM;
        }

        private VMCBSSummary GetBOMSummaryByStyleID(VMStyle style)
        {
            var vDataFabric = GetBOMSummary(style.ID, 2);//Fabric
            var vDataWash = GetBOMSummary(style.ID, 3);//Wash
            var vDataPrint = GetBOMSummary(style.ID, 4);//Print
            var vDataEmbroidery = GetBOMSummary(style.ID, 5);//Embroidery
            var vDataTrims = GetBOMSummary(style.ID, 6);//Trims_Accesses
            var vDataCharge = GetBOMSummary(style.ID, 1023);//Trims_Accesses
            VMCBSSummary model = new VMCBSSummary();
            model.StyleValue = style.StylePrice;
            model.DzStyleValue = decimal.Multiply(style.UnitPrice, 12);
            model.OverheadRate = style.OverheadRate;
            model.BuyingComRate = style.OrderBuyingCommission;
            model.DzMakingCost = style.MakingCost;//Making Cost(CM)Per Dozen
            model.OrderTotalMakingCost = decimal.Multiply(model.DzMakingCost, style.DozonQty);
            model.CostBuyingComValue = decimal.Divide(decimal.Multiply(model.StyleValue, model.BuyingComRate), 100);

            model.CostFabric = vDataFabric.TotalPrice;
            model.CostWash = vDataWash.TotalPrice;
            model.CostPrint = vDataPrint.TotalPrice;
            model.CostEmbroidiry = vDataEmbroidery.TotalPrice;
            model.CostAccessories = vDataTrims.TotalPrice;
            model.CostCharges = vDataCharge.TotalPrice;
            model.CostTotalValue = model.CostFabric + model.CostWash + model.CostPrint + model.CostEmbroidiry + model.CostAccessories + model.CostCharges + model.OrderTotalMakingCost;

            model.CostOverheadValue = decimal.Multiply(model.CostTotalValue, decimal.Divide(model.OverheadRate, 100));
            model.StyleCostingValue = decimal.Add(model.CostTotalValue, model.CostOverheadValue);
            //model.StyleGrossCostValue = decimal.Add(model.StyleCostingValue, model.OrderTotalMakingCost);


            model.CostOrderValue = decimal.Add(model.StyleCostingValue, model.CostBuyingComValue);
            model.ProductValueProfitValue = decimal.Subtract(model.StyleValue, model.CostOrderValue);
            //model.ProfitMarginRate = decimal.Multiply(decimal.Divide(model.ProductValueProfitValue, model.StyleValue), 100);
            if (model.CostOrderValue > 0)
            {
                model.ProfitMarginRate = decimal.Multiply(decimal.Divide(model.ProductValueProfitValue, model.CostOrderValue), 100);
            }


            //Dozon
            model.CatFabric = decimal.Multiply(decimal.Divide(model.CostFabric, style.PieceQty), 12);
            model.CatAccessories = decimal.Multiply(decimal.Divide(model.CostAccessories, style.PieceQty), 12);
            model.CatWash = decimal.Multiply(decimal.Divide(model.CostWash, style.PieceQty), 12);
            model.CatPrint = decimal.Multiply(decimal.Divide(model.CostPrint, style.PieceQty), 12);
            model.CatEmbroidiry = decimal.Multiply(decimal.Divide(model.CostEmbroidiry, style.PieceQty), 12);
            model.CatCharges = decimal.Multiply(decimal.Divide(model.CostCharges, style.PieceQty), 12);
            model.CatTotalValue = model.CatFabric + model.CatAccessories + model.CatWash + model.CatPrint + model.CatEmbroidiry + model.CatCharges + model.DzMakingCost;

            model.DzOverheadValue = decimal.Multiply(decimal.Divide(model.CostOverheadValue, style.PieceQty), 12);
            model.DzStyleCostingValue = model.CatTotalValue + model.DzOverheadValue;
            //model.DzStyleGrossCostValue = decimal.Add(model.DzStyleCostingValue, model.DzMakingCost);

            model.DzBuyingComValue = decimal.Multiply(decimal.Divide(model.CostBuyingComValue, style.PieceQty), 12);
            model.DzCostOrderValue = decimal.Add(model.DzStyleCostingValue, model.DzBuyingComValue);
            model.DzProductValueProfitValue = decimal.Subtract(decimal.Multiply(style.UnitPrice, 12), model.DzCostOrderValue);

            //model.DzProfitMarginRate = decimal.Multiply(decimal.Divide(model.DzProductValueProfitValue, decimal.Multiply(style.UnitPrice, 12)), 100);
            if (model.DzCostOrderValue > 0)
            {
                model.DzProfitMarginRate = decimal.Multiply(decimal.Divide(model.DzProductValueProfitValue, model.DzCostOrderValue), 100);
            }
            return model;
        }
        private VMCBSSlave GetBOMSummary(int Id, int CatID)
        {
            VMCBSSlave model = new VMCBSSlave();

            var vData = (from t1 in _db.Merchandising_StyleSlave
                         join t2 in _db.Common_RawItem on t1.Common_RawItemFK equals t2.ID
                         join t3 in _db.Common_RawSubCategory on t2.Common_RawSubCategoryFK equals t3.ID
                         join t4 in _db.Common_RawCategory on t3.Common_RawCategoryFK equals t4.ID
                         where t1.Active == true && t1.Merchandising_StyleFK == Id && t4.ID == CatID
                         select new
                         {
                             totalQty = t1.RequiredQty,
                             totalPrice = t1.TotalPrice,
                         });
            if (vData.Any())
            {
                model.RequiredQty = vData.Sum(a => a.totalQty);
                model.TotalPrice = vData.Sum(a => a.totalPrice);
            }
            return model;
        }

        public List<VMColorSize> GetColorSizeSummaryByStyleID(int StyleID)
        {
            string SetName = string.Empty;
            var vSubSet = _db.Merchandising_StyleSetPack.Where(a => a.Merchandising_StyleFK == StyleID).ToList();
            List<VMColorSize> SizeList = new List<VMColorSize>();
            List<VMColorSize> list = new List<VMColorSize>();
            var vData = (from t1 in _db.Merchandising_StyleShipmentSchedule
                         join t2 in _db.Common_Country on t1.Common_CountryFK equals t2.ID
                         join t3 in _db.Common_CountryPort on t1.Common_CountryPortFK equals t3.ID
                         join t5 in _db.Merchandising_StyleShipmentRatio on t1.ID equals t5.Merchandising_StyleShipmentScheduleFK
                         join t6 in _db.Merchandising_StyleMeasurement on t5.Merchandising_StyleMeasurementFK equals t6.ID
                         join t7 in _db.Common_Color on t6.Common_ColorFK equals t7.ID
                         join t8 in _db.Common_Size on t6.Common_SizeFK equals t8.ID
                         where t1.Active == true && t5.Active == true && t1.Merchandising_StyleFK == StyleID
                         select new VMShipmentColorSize
                         {
                             ID = t5.ID,
                             Merchandising_StyleMeasurementFK = (int)t5.Merchandising_StyleMeasurementFK,
                             Common_ColorFK = t6.Common_ColorFK,
                             Common_SizeFK = t6.Common_SizeFK,
                             Destination = t2.Name,
                             PortNo = t3.Name,
                             Color = t7.Name,
                             Size = t8.Name,
                             Quantity = (int)t5.Quantity,
                             Ratio = t5.Ratio,
                             SetPackFK = (int)t5.Merchandising_StyleSetPackFK
                         }).OrderBy(a => a.ID).ToList();

            var vSize = (from x in vData
                         group x by new { x.Common_SizeFK, x.Size } into asize
                         select new
                         {
                             SizeID = asize.Key.Common_SizeFK,
                             SizeName = asize.Key.Size
                         }).ToList();
            if (vData.Any())
            {
                var vAColor = (from o in vData
                               group o by new { o.Common_ColorFK, o.Color, o.SetPackFK } into ac
                               select new
                               {
                                   ColorID = ac.Key.Color,
                                   ColorName = ac.Key.Color,
                                   SetPackID = ac.Key.SetPackFK,
                                   CQty = ac.Sum(a => a.Quantity),
                                   vASize = (from x in ac.ToList()
                                             group x.Quantity by new { x.Common_SizeFK, x.Size } into asize
                                             select new
                                             {
                                                 SizeID = asize.Key.Common_SizeFK,
                                                 SizeName = asize.Key.Size,
                                                 Qty = asize.Sum()
                                             }).ToList()
                               }).ToList();
                if (vAColor.Any())
                {
                    foreach (var c in vAColor)
                    {
                        VMColorSize mColor = new VMColorSize();
                        mColor.Color = c.ColorName;
                        mColor.SetPackFK = c.SetPackID;
                        mColor.Quantity = c.CQty;
                        if (mColor.SetPackFK != 0)
                        {
                            mColor.SubSetName = vSubSet.Any(a => a.ID == mColor.SetPackFK) == true ? vSubSet.FirstOrDefault(a => a.ID == mColor.SetPackFK).SetPackName : string.Empty;
                        }
                        if (c.vASize.Any())
                        {
                            List<VMColorSize> lstSize = new List<VMColorSize>();
                            if (vSize.Count() == c.vASize.Count())
                            {
                                foreach (var s in c.vASize)
                                {
                                    VMColorSize mSize = new VMColorSize();
                                    mSize.Size = s.SizeName;
                                    mSize.Quantity = s.Qty;
                                    lstSize.Add(mSize);
                                }
                            }
                            else
                            {
                                foreach (var s1 in vSize)
                                {
                                    if (c.vASize.Any(a => a.SizeID == s1.SizeID))
                                    {
                                        var gSize = c.vASize.FirstOrDefault(a => a.SizeID == s1.SizeID);
                                        VMColorSize mSize = new VMColorSize();
                                        mSize.Size = gSize.SizeName;
                                        mSize.Quantity = gSize.Qty;
                                        lstSize.Add(mSize);
                                    }
                                    else
                                    {
                                        VMColorSize mSize = new VMColorSize();
                                        mSize.Size = s1.SizeName;
                                        mSize.Quantity = 0;
                                        lstSize.Add(mSize);
                                    }
                                }

                            }


                            mColor.SizeList = lstSize;
                        }
                        list.Add(mColor);
                    }
                }
            }
            return list;
        }
        #endregion

        #region CBSStyle
        public async Task<VMStyle> StyleCBSGet()
        {
            var vData = (from t1 in _db.Merchandising_CBSStyle
                         join t3 in _db.Common_FinishItem on t1.Common_FinishItemFK equals t3.ID
                         join t4 in _db.Common_FinishSubCategory on t3.Common_FinishSubCategoryFK equals t4.ID
                         join t5 in _db.Common_FinishCategory on t4.Common_FinishCategoryFK equals t5.ID
                         where t1.Active == true
                         select new VMStyle
                         {
                             ID = t1.ID,
                             CID = t1.CID,
                             Mkt_CategoryFK = t5.ID,
                             Mkt_SubCategoryFK = t4.ID,
                             Mkt_ItemFK = t1.Common_FinishItemFK,
                             ItemName = t3.Name,
                             Class = t1.Class,
                             Fabrication = t1.Fabrication,
                             Style = t1.StyleName,
                             SetQuantity = t1.SetQuantity,
                             PackQty = t1.PackQty,
                             PackPieceQty = t1.PackPieceQty,
                             PieceQty = t1.PieceQty,
                             Description = t1.Description
                         }).OrderByDescending(a => a.ID).ToList();
            VMStyle vMStyle = new VMStyle
            {
                DataList = await Task.Run(() => vData)
            };
            return vMStyle;
        }
        public async Task<int> CBSStyleAdd(VMStyle vMStyle)
        {
            var result = -1;
            Merchandising_CBSStyle oStyle = new Merchandising_CBSStyle();
            oStyle.CID = CidServices.CBSStyleCID(vMStyle.Mkt_ItemFK);
            oStyle.FirstMove = DateTime.Today;
            oStyle.UpdateDate = DateTime.Today;
            oStyle.Common_FinishItemFK = vMStyle.Mkt_ItemFK;
            oStyle.Class = vMStyle.Class;
            oStyle.Fabrication = vMStyle.Fabrication;
            oStyle.StyleName = vMStyle.Style;
            oStyle.SetQuantity = vMStyle.SetQuantity;
            oStyle.PackQty = vMStyle.PackQty;
            oStyle.PackPieceQty = vMStyle.PackPieceQty;
            oStyle.PieceQty = (vMStyle.PackPieceQty * vMStyle.PackQty);
            oStyle.UnitPrice = 0;
            oStyle.PackPrice = 0;
            oStyle.Description = vMStyle.Description;

            _db.Merchandising_CBSStyle.Add(oStyle);

            if (await _db.SaveChangesAsync() > 0)
            {
                result = oStyle.ID;
            }
            return result;
        }
        public async Task<int> CBSStyleEdit(VMStyle vMStyle)
        {
            var result = -1;

            Merchandising_CBSStyle oStyle = _db.Merchandising_CBSStyle.Find(vMStyle.ID);
            oStyle.Common_FinishItemFK = vMStyle.Mkt_ItemFK;
            oStyle.Class = vMStyle.Class;
            oStyle.Fabrication = vMStyle.Fabrication;
            oStyle.StyleName = vMStyle.Style;
            oStyle.SetQuantity = vMStyle.SetQuantity;
            oStyle.PackQty = vMStyle.PackQty;
            oStyle.PackPieceQty = vMStyle.PackPieceQty;
            oStyle.PieceQty = (vMStyle.PackPieceQty * vMStyle.PackQty);
            oStyle.UnitPrice = (vMStyle.PackPrice / vMStyle.PackPieceQty);
            oStyle.PackPrice = vMStyle.PackPrice;
            oStyle.Description = vMStyle.Description;
            oStyle.Time = DateTime.Today;

            if (await _db.SaveChangesAsync() > 0)
            {
                result = oStyle.ID;
            }
            return result;
        }
        public async Task<int> CBSStyleDelete(int Id)
        {
            var result = -1;
            var vCheckCBS = _db.Merchandising_CBSStyle.Any(a => a.ID == Id && a.Active == true);
            if (!vCheckCBS)
            {
                Merchandising_CBSStyle oStyle = _db.Merchandising_CBSStyle.Find(Id);
                if (oStyle != null)
                {
                    oStyle.Active = false;
                    if (await _db.SaveChangesAsync() > 0)
                    {
                        result = Id;
                    }
                }
            }
            return result;
        }
        public VMStyle CBSStyleGetById(int Id)
        {
            var vData = (from t1 in _db.Merchandising_CBSStyle
                         join t3 in _db.Common_FinishItem on t1.Common_FinishItemFK equals t3.ID
                         join t4 in _db.Common_FinishSubCategory on t3.Common_FinishSubCategoryFK equals t4.ID
                         join t5 in _db.Common_FinishCategory on t4.Common_FinishCategoryFK equals t5.ID
                         where t1.ID == Id && t1.Active == true
                         select new VMStyle
                         {
                             ID = t1.ID,
                             CID = t1.CID,
                             FirstMove = t1.FirstMove,
                             UpdateDate = t1.Time,
                             ItemName = t3.Name,
                             Mkt_CategoryFK = t5.ID,
                             Mkt_SubCategoryFK = t4.ID,
                             Mkt_ItemFK = t1.Common_FinishItemFK,
                             Class = t1.Class,
                             Fabrication = t1.Fabrication,
                             Style = t1.StyleName,
                             SetQuantity = t1.SetQuantity,
                             PackQty = t1.PackQty,
                             PackPieceQty = t1.PackPieceQty,
                             PieceQty = t1.PieceQty,
                             UnitPrice = t1.UnitPrice,
                             PackPrice = t1.PackPrice,
                             Description = t1.Description,
                             ActionId = (int)ActionEnum.Edit
                         }).FirstOrDefault();

            return vData;
        }
        #endregion

        #region SetPack

        public async Task<int> SetPackAdd(VMStyleSetPack vMPack)
        {
            var result = -1;
            Merchandising_StyleSetPack sPack = new Merchandising_StyleSetPack();
            sPack.Merchandising_StyleFK = vMPack.Merchandising_StyleFK;
            sPack.SetPackName = vMPack.SetPackName;
            sPack.Quantity = vMPack.Quantity;
            sPack.Active = true;
            _db.Merchandising_StyleSetPack.Add(sPack);

            if (await _db.SaveChangesAsync() > 0)
            {
                result = sPack.ID;
            }
            return result;
        }
        public async Task<int> SetPackEdit(VMStyleSetPack vMSetPack)
        {
            var result = -1;
            Merchandising_StyleSetPack sPack = _db.Merchandising_StyleSetPack.Find(vMSetPack.ID);
            sPack.SetPackName = vMSetPack.SetPackName;
            sPack.Quantity = vMSetPack.Quantity;
            sPack.Active = true;

            if (await _db.SaveChangesAsync() > 0)
            {
                result = sPack.ID;
            }
            return result;
        }
        public async Task<int> SetPackDelete(int Id)
        {
            var result = -1;
            Merchandising_StyleSetPack selectSingle = _db.Merchandising_StyleSetPack.Find(Id);
            if (selectSingle != null)
            {
                selectSingle.Active = false;
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = selectSingle.Merchandising_StyleFK;
                }
            }

            return result;
        }
        public VMStyleSetPack StyleSetPackByID(int ID)
        {
            var vData = (from t1 in _db.Merchandising_StyleSetPack
                         where t1.ID == ID
                         select new VMStyleSetPack
                         {
                             ID = t1.ID,
                             Merchandising_StyleFK = t1.Merchandising_StyleFK,
                             SetPackName = t1.SetPackName,
                             Quantity = t1.Quantity,
                         }).FirstOrDefault();

            return vData;
        }

        public bool IsAllow(int StyleID)
        {
            int CountSubSetAllow = _db.Merchandising_Style.Where(a => a.ID == StyleID).FirstOrDefault().SetQuantity;
            var CountSubSetAdd = _db.Merchandising_StyleSetPack.Where(a => a.Merchandising_StyleFK == StyleID && a.Active == true).Count();
            if (CountSubSetAllow > CountSubSetAdd)
            {
                return true;
            }
            else { return false; }
        }

        public async Task<VMStyleSetPack> SetPackByStyleID(int ID)
        {
            var vData = (from t1 in _db.Merchandising_StyleSetPack
                         where t1.Active == true && t1.Merchandising_StyleFK == ID
                         select new VMStyleSetPack
                         {
                             ID = t1.ID,
                             Merchandising_StyleFK = t1.Merchandising_StyleFK,
                             SetPackName = t1.SetPackName,
                             Quantity = t1.Quantity
                         }).OrderByDescending(a => a.ID).ToList();
            List<VMStyleSetPack> lst = new List<VMStyleSetPack>();
            VMStyleSetPack vMSet = new VMStyleSetPack
            {
                DataList = await Task.Run(() => vData.Any() == false ? lst : vData)
            };
            return vMSet;
        }
        public async Task<VMStyleView> SetPackGetByStyleIDView(int ID)
        {
            var VM = new VMStyleView();
            VM.vmStyle = new VMStyle();
            VM.vmStyle = StyleById(ID);
            VM.vmStyleSetPack = await SetPackByStyleID(ID);
            return VM;
        }
        #endregion

        #region ManageBOM

        public async Task<VMBOM> BOMGet()
        {
            VMBOM vMBOM = new VMBOM
            {
                DataList = await Task.Run(() => (from t1 in _db.Merchandising_Style
                                                 join t2 in _db.Merchandising_StyleSlave on t1.ID equals t2.Merchandising_StyleFK
                                                 join t3 in _db.Common_RawItem on t2.Common_RawItemFK equals t3.ID
                                                 join t4 in _db.Common_RawSubCategory on t3.Common_RawSubCategoryFK equals t4.ID
                                                 join t5 in _db.Common_RawCategory on t4.Common_RawCategoryFK equals t5.ID
                                                 //join t7 in _db.Common_Currency on t2.Common_CurrencyFK equals t7.ID
                                                 where t1.Active == true && t1.IsComplete == false
                                                 select new VMBOM
                                                 {
                                                     ID = t2.ID,
                                                     Merchandising_StyleFK = t1.ID,
                                                     Common_RawCategoryFK = t5.ID,
                                                     Common_RawSubCategoryFK = t4.ID,
                                                     Common_RawItemFK = t2.Common_RawItemFK.Value,
                                                     //Common_CurrencyFK = t7.ID,
                                                     RawCategory = t5.Name,
                                                     RawSubCategory = t4.Name,
                                                     RawItem = t3.Name,
                                                     Style = t1.StyleName,
                                                     Tolerance = t2.Tolerance,
                                                     Currency = "$",
                                                     UnitPrice = t2.UnitPrice,
                                                     Consumption = t2.Consumption,
                                                     RequiredQty = t2.RequiredQty,
                                                     TotalPrice = t2.UnitPrice * t2.RequiredQty,
                                                 }).OrderByDescending(a => a.ID).ToList())
            };
            return vMBOM;
        }
        public async Task<int> BOMAdd(VMBOM vMBOM)
        {
            var result = -1;
            if (vMBOM.ConRequiredQty > 0)
            {
                vMBOM.RequiredQty = decimal.Multiply(decimal.Divide(vMBOM.ConRequiredQty, 12), vMBOM.Consumption);
            }
            else
            {
                vMBOM.RequiredQty = decimal.Multiply(decimal.Divide(vMBOM.TotalPiece, 12), vMBOM.Consumption);
                vMBOM.ConRequiredQty = vMBOM.TotalPiece;
            }

            Merchandising_StyleSlave oStyleSlave = new Merchandising_StyleSlave
            {
                Merchandising_StyleFK = vMBOM.Merchandising_StyleFK,
                Common_RawItemFK = vMBOM.Common_RawItemFK,
                SelfReferenceFK = vMBOM.SelfReferenceFK,
                Consumption = vMBOM.Consumption,
                ConRequiredQty = vMBOM.ConRequiredQty,
                RequiredQty = vMBOM.RequiredQty,
                GSM = vMBOM.GSM,
                UnitPrice = vMBOM.UnitPrice,
                Tolerance = vMBOM.Tolerance,
                TotalPrice = decimal.Multiply(vMBOM.RequiredQty, vMBOM.UnitPrice),
                Remarks = vMBOM.Description,
                UserID = vMBOM.UserID,
                Time = vMBOM.Time
            };
            _db.Merchandising_StyleSlave.Add(oStyleSlave);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = oStyleSlave.ID;
            }
            return result;
        }
        public async Task<int> BOMEdit(VMBOM vMBOM)
        {
            var result = -1;
            vMBOM.RequiredQty = decimal.Multiply(decimal.Divide(vMBOM.TotalPiece, 12), vMBOM.Consumption);
            if (vMBOM.ConRequiredQty > 0)
            {
                vMBOM.RequiredQty = decimal.Multiply(decimal.Divide(vMBOM.ConRequiredQty, 12), vMBOM.Consumption);
            }
            else
            {
                vMBOM.RequiredQty = decimal.Multiply(decimal.Divide(vMBOM.TotalPiece, 12), vMBOM.Consumption);
                vMBOM.ConRequiredQty = vMBOM.TotalPiece;
            }
            Merchandising_StyleSlave oStyle = _db.Merchandising_StyleSlave.Find(vMBOM.ID);
            oStyle.Common_RawItemFK = vMBOM.Common_RawItemFK;
            oStyle.SelfReferenceFK = vMBOM.SelfReferenceFK;
            oStyle.GSM = vMBOM.GSM;
            oStyle.Consumption = vMBOM.Consumption;
            oStyle.ConRequiredQty = vMBOM.ConRequiredQty;
            oStyle.RequiredQty = vMBOM.RequiredQty;
            oStyle.UnitPrice = vMBOM.UnitPrice;
            oStyle.Tolerance = vMBOM.Tolerance;
            oStyle.TotalPrice = vMBOM.RequiredQty * vMBOM.UnitPrice;
            oStyle.Remarks = vMBOM.Description;
            if (await _db.SaveChangesAsync() > 0)
            {
                result = oStyle.ID;
            }
            return result;
        }
        //Need Validation for Delete
        public async Task<int> BOMItemDelete(int id)
        {
            var result = -1;
            Merchandising_StyleSlave oStyle = _db.Merchandising_StyleSlave.Find(id);
            if (oStyle != null)
            {
                oStyle.Active = false;
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = oStyle.Merchandising_StyleFK;
                }
            }
            return result;
        }

        public async Task<int> BOMDelete(int id)
        {
            var result = -1;
            var vData = _db.Merchandising_StyleSlave.Where(a => a.Active == false && a.Merchandising_StyleFK == id).Join(_db.Procurement_PurchaseRequisitionSlave.Where(a => a.Active == false), a => a.ID, b => b.Merchandising_StyleSlaveFK, (a, b) => new { a, b });
            if (!vData.Any())
            {
                var oStyle = _db.Merchandising_StyleSlave.Where(a => a.Merchandising_StyleFK == id && a.Active == true);
                foreach (var v in oStyle)
                {
                    v.Active = false;
                }
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = id;
                }
            }

            return result;
        }

        public async Task<int> BOMAccessoriesAdd(VMBOM vMBOM)
        {
            var result = -1;
            if (vMBOM.ConRequiredQty > 0)
            {
                if (vMBOM.Consumption > 0)
                {
                    vMBOM.RequiredQty = decimal.Multiply(decimal.Divide(vMBOM.ConRequiredQty, 12), vMBOM.Consumption);
                }
                else
                {
                    vMBOM.RequiredQty = vMBOM.ConRequiredQty;
                    vMBOM.Consumption = decimal.Divide(vMBOM.RequiredQty, decimal.Divide(vMBOM.TotalPiece, 12));
                    vMBOM.ConRequiredQty = vMBOM.TotalPiece;
                }
            }
            else
            {
                vMBOM.RequiredQty = decimal.Multiply(decimal.Divide(vMBOM.TotalPiece, 12), vMBOM.Consumption);
                vMBOM.ConRequiredQty = vMBOM.TotalPiece;
            }
            Merchandising_StyleSlave oStyleSlave = new Merchandising_StyleSlave
            {
                Merchandising_StyleFK = vMBOM.Merchandising_StyleFK,
                Common_RawItemFK = vMBOM.Common_RawItemFK,
                Consumption = vMBOM.Consumption,
                RequiredQty = vMBOM.RequiredQty,
                ConRequiredQty = vMBOM.ConRequiredQty,
                GSM = vMBOM.GSM,
                Common_SizeFk = vMBOM.Common_SizeFK,
                Common_ColorFK = vMBOM.Common_ColorFK,
                UnitPrice = vMBOM.UnitPrice,
                Tolerance = vMBOM.Tolerance,
                TotalPrice = decimal.Multiply(vMBOM.RequiredQty, vMBOM.UnitPrice),
                Remarks = vMBOM.Description,
                UserID = vMBOM.UserID,
                Time = vMBOM.Time
            };
            _db.Merchandising_StyleSlave.Add(oStyleSlave);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = oStyleSlave.ID;
            }
            return result;
        }
        public async Task<int> BOMAccessoriesEdit(VMBOM vMBOM)
        {
            var result = -1;

            if (vMBOM.ConRequiredQty > 0)
            {
                if (vMBOM.Consumption > 0)
                {
                    vMBOM.RequiredQty = decimal.Multiply(decimal.Divide(vMBOM.ConRequiredQty, 12), vMBOM.Consumption);
                }
                else
                {
                    vMBOM.RequiredQty = vMBOM.ConRequiredQty;
                    vMBOM.Consumption = decimal.Divide(vMBOM.RequiredQty, decimal.Divide(vMBOM.TotalPiece, 12));
                    vMBOM.ConRequiredQty = vMBOM.TotalPiece;
                }
                //vMBOM.RequiredQty = decimal.Multiply(decimal.Divide(vMBOM.ConRequiredQty, 12), vMBOM.Consumption);
            }
            else
            {
                vMBOM.RequiredQty = decimal.Multiply(decimal.Divide(vMBOM.TotalPiece, 12), vMBOM.Consumption);
                vMBOM.ConRequiredQty = vMBOM.TotalPiece;
            }
            Merchandising_StyleSlave oStyle = _db.Merchandising_StyleSlave.Find(vMBOM.ID);
            oStyle.Common_RawItemFK = vMBOM.Common_RawItemFK;
            oStyle.GSM = vMBOM.GSM;
            oStyle.Common_ColorFK = vMBOM.Common_ColorFK;
            oStyle.Common_SizeFk = vMBOM.Common_SizeFK;
            oStyle.Consumption = vMBOM.Consumption;
            oStyle.ConRequiredQty = vMBOM.ConRequiredQty;
            oStyle.RequiredQty = vMBOM.RequiredQty;
            oStyle.UnitPrice = vMBOM.UnitPrice;
            oStyle.Tolerance = vMBOM.Tolerance;
            oStyle.TotalPrice = decimal.Multiply(vMBOM.RequiredQty, vMBOM.UnitPrice);
            oStyle.Remarks = vMBOM.Description;
            if (await _db.SaveChangesAsync() > 0)
            {
                result = oStyle.ID;
            }
            return result;
        }

        public async Task<int> BOMFabricAdd(VMBOM vMBOM)
        {
            var result = -1;
            if (vMBOM.ConRequiredQty > 0)
            {
                vMBOM.RequiredQty = decimal.Multiply(decimal.Divide(vMBOM.ConRequiredQty, 12), vMBOM.Consumption);
            }
            else
            {
                vMBOM.RequiredQty = decimal.Multiply(decimal.Divide(vMBOM.TotalPiece, 12), vMBOM.Consumption);
                vMBOM.ConRequiredQty = vMBOM.TotalPiece;
            }
            Merchandising_StyleSlave oStyleSlave = new Merchandising_StyleSlave
            {
                Merchandising_StyleFK = vMBOM.Merchandising_StyleFK,
                Common_RawItemFK = vMBOM.Common_RawItemFK,
                Common_ColorFK = vMBOM.Common_ColorFK,
                Consumption = vMBOM.Consumption,
                ConRequiredQty = vMBOM.ConRequiredQty,
                RequiredQty = vMBOM.RequiredQty,
                GSM = vMBOM.GSM,
                IsFabric = true,
                UnitPrice = vMBOM.UnitPrice,
                Tolerance = vMBOM.Tolerance,
                TotalPrice = decimal.Multiply(vMBOM.RequiredQty, vMBOM.UnitPrice),
                Remarks = vMBOM.Description,
                UserID = vMBOM.UserID,
                Time = vMBOM.Time
            };
            _db.Merchandising_StyleSlave.Add(oStyleSlave);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = oStyleSlave.ID;
            }
            return result;
        }
        public async Task<int> BOMFabricEdit(VMBOM vMBOM)
        {
            var result = -1;
            if (vMBOM.ConRequiredQty > 0)
            {
                vMBOM.RequiredQty = decimal.Multiply(decimal.Divide(vMBOM.ConRequiredQty, 12), vMBOM.Consumption);
            }
            else
            {
                vMBOM.RequiredQty = decimal.Multiply(decimal.Divide(vMBOM.TotalPiece, 12), vMBOM.Consumption);
                vMBOM.ConRequiredQty = vMBOM.TotalPiece;
            }
            Merchandising_StyleSlave oStyle = _db.Merchandising_StyleSlave.Find(vMBOM.ID);
            oStyle.Common_RawItemFK = vMBOM.Common_RawItemFK;
            oStyle.Common_ColorFK = vMBOM.Common_ColorFK;
            oStyle.GSM = vMBOM.GSM;
            oStyle.IsFabric = true;
            oStyle.Consumption = vMBOM.Consumption;
            oStyle.RequiredQty = vMBOM.RequiredQty;
            oStyle.ConRequiredQty = vMBOM.ConRequiredQty;
            oStyle.UnitPrice = vMBOM.UnitPrice;
            oStyle.Tolerance = vMBOM.Tolerance;
            oStyle.TotalPrice = decimal.Multiply(vMBOM.RequiredQty, vMBOM.UnitPrice);
            oStyle.Remarks = vMBOM.Description;
            if (await _db.SaveChangesAsync() > 0)
            {
                result = oStyle.ID;
            }
            return result;
        }

        public async Task<int> GarmentsTestAdd(VMBOM vMBOM)
        {
            var result = -1;
            Merchandising_StyleSlave oStyleSlave = new Merchandising_StyleSlave
            {
                Merchandising_StyleFK = vMBOM.Merchandising_StyleFK,
                Common_RawItemFK = vMBOM.Common_RawItemFK,
                Consumption = 1,
                RequiredQty = 1,
                UnitPrice = vMBOM.UnitPrice,
                Tolerance = 0,
                TotalPrice = vMBOM.UnitPrice,
                Remarks = vMBOM.Description,
                UserID = vMBOM.UserID,
                Time = vMBOM.Time
            };
            _db.Merchandising_StyleSlave.Add(oStyleSlave);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = oStyleSlave.ID;
            }
            return result;
        }
        public async Task<int> GarmentsTestEdit(VMBOM vMBOM)
        {
            var result = -1;
            Merchandising_StyleSlave oStyle = _db.Merchandising_StyleSlave.Find(vMBOM.ID);
            oStyle.Common_RawItemFK = vMBOM.Common_RawItemFK;
            oStyle.UnitPrice = vMBOM.UnitPrice;
            oStyle.TotalPrice = vMBOM.UnitPrice;
            oStyle.Remarks = vMBOM.Description;
            if (await _db.SaveChangesAsync() > 0)
            {
                result = oStyle.ID;
            }
            return result;
        }

        public async Task<int> BOMApproval(int Id, bool IsApproved)
        {
            var result = -1;

            Merchandising_StyleSlave oStyle = _db.Merchandising_StyleSlave.Find(Id);

            if (oStyle != null)
            {
                oStyle.IsLocked = IsApproved;
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = oStyle.ID;
                }
            }
            return result;
        }

        public async Task<int> BOMAllItemApproval(int Id, bool IsApproved)
        {
            var result = -1;
            var oStyle = _db.Merchandising_StyleSlave.Where(a => a.Merchandising_StyleFK == Id && a.Active == true);
            if (IsApproved)
            {
                var approve = oStyle.Where(a => a.IsLocked == false);
                foreach (var v in approve)
                {
                    v.IsLocked = true;
                }
                var update = _db.Merchandising_Style.FirstOrDefault(a => a.ID == Id);
                update.IsComplete = true;
            }
            else
            {
                var update = _db.Merchandising_Style.FirstOrDefault(a => a.ID == Id);
                update.IsComplete = false;
            }
            if (await _db.SaveChangesAsync() > 0)
            {
                result = Id;
            }
            return result;
        }

        public VMBOM BOMGetById(int ID)
        {
            var vData = (from t1 in _db.Merchandising_StyleSlave
                         join t2 in _db.Merchandising_Style on t1.ID equals t2.ID
                         join t3 in _db.Common_RawItem on t1.Common_RawItemFK equals t3.ID
                         join t4 in _db.Common_RawSubCategory on t3.Common_RawSubCategoryFK equals t4.ID
                         join t5 in _db.Common_RawCategory on t4.Common_RawCategoryFK equals t5.ID
                         join t8 in _db.Merchandising_CBSSlave on t1.Merchandising_CBSSlaveFK equals t8.ID into CBS_Join
                         from cbs in CBS_Join.DefaultIfEmpty()
                         where t1.Active == true && t2.ID == ID
                         select new VMBOM
                         {
                             ID = t2.ID,
                             Merchandising_StyleFK = t1.ID,
                             Common_RawCategoryFK = t5.ID,
                             Common_RawSubCategoryFK = t4.ID,
                             Common_RawItemFK = t1.Common_RawItemFK.Value,
                             Merchandising_CBSSlaveFK = cbs.ID,
                             RawCategory = t5.Name,
                             RawSubCategory = t4.Name,
                             RawItem = t3.Name,
                             Style = t2.StyleName,
                             Tolerance = t1.Tolerance,
                             Currency = "$",
                             UnitPrice = t2.UnitPrice,
                             ConRequiredQty = t1.ConRequiredQty,
                             RequiredQty = t1.RequiredQty,
                             TotalPrice = decimal.Multiply(t2.UnitPrice, t1.RequiredQty),
                             Description = t1.Remarks,
                             ActionId = (int)ActionEnum.Edit
                         }).FirstOrDefault();

            return vData;
        }
        public async Task<VMBOM> GetClosedBOM()
        {
            List<VMBOM> bomItem = new List<VMBOM>();
            var DummyData = new VMBOM
            {
                Common_CurrencyFK = 1,
                Common_RawCategoryFK = 1,
                Common_RawSubCategoryFK = 1,
                Common_RawItemFK = 1,
                Merchandising_StyleFK = 1,
                RawCategory = "Yarn Processsing (Fabrics)",
                RawSubCategory = "Dyeing",
                RawItem = "Dyeing For AOP",
                Style = "KIK TEXTILIEN UND NON FOOD GMBH/19/020/P84484(4500153511)/P84484",
                UnitName = "$",
                Tolerance = (decimal)44.00,
                Currency = "USD",
                UnitPrice = (decimal)3.10000,
                RequiredQty = (decimal)1159.07,
                TotalPrice = 0

            };
            bomItem.Add(DummyData);
            VMBOM vMBOM = new VMBOM
            {
                DataList = await Task.Run(() => bomItem)
            };

            return vMBOM;
        }
        public VMBOM BOMGetByStyleID(int Id, int CatID)
        {
            var vCat = _db.Common_RawCategory.Where(a => a.ID == CatID);
            var vData = (from t1 in _db.Merchandising_StyleSlave
                         join t2 in _db.Common_RawItem on t1.Common_RawItemFK equals t2.ID
                         join t3 in _db.Common_RawSubCategory on t2.Common_RawSubCategoryFK equals t3.ID
                         join t4 in _db.Common_RawCategory on t3.Common_RawCategoryFK equals t4.ID
                         join t6 in _db.Common_Unit on t2.Common_UnitFK equals t6.ID
                         join t8 in _db.Common_Color on t1.Common_ColorFK equals t8.ID into color
                         from t9 in color.DefaultIfEmpty()
                         join t10 in _db.Common_Size on t1.Common_SizeFk equals t10.ID into Join_Size
                         from t11 in Join_Size.DefaultIfEmpty()
                         join t7 in _db.Merchandising_StyleSlave on t1.SelfReferenceFK equals t7.ID into self
                         from parent in self.DefaultIfEmpty()
                         join RI in _db.Common_RawItem on parent.Common_RawItemFK equals RI.ID into RI_Join
                         from RI in RI_Join.DefaultIfEmpty()
                             //join RSC in _db.Common_RawSubCategory on RI.Common_RawSubCategoryFK equals RSC.ID into RSC_Join
                             //from RSC in RSC_Join.DefaultIfEmpty()
                         join C in _db.Common_Color on parent.Common_ColorFK equals C.ID into C_Join
                         from C in C_Join.DefaultIfEmpty()
                         join st in _db.Merchandising_Style on t1.Merchandising_StyleFK equals st.ID
                         where t1.Active == true && t1.Merchandising_StyleFK == Id && t4.ID == CatID
                         select new VMBOM
                         {
                             ID = t1.ID,
                             Merchandising_StyleFK = t1.Merchandising_StyleFK,
                             Common_RawCategoryFK = t4.ID,
                             Common_RawSubCategoryFK = t3.ID,
                             Common_RawItemFK = (int)t1.Common_RawItemFK,
                             Merchandising_CBSSlaveFK = t1.Merchandising_CBSSlaveFK,
                             RawCategory = t4.Name,
                             RawSubCategory = t3.Name,
                             RawItem = t2.Name,
                             UnitName = t6.Name,
                             Currency = "$",
                             GSM = t1.GSM,
                             Common_ColorFK = t1.Common_ColorFK == null ? 0 : (int)t1.Common_ColorFK,
                             Common_SizeFK = t1 != null ? (int)t1.Common_SizeFk : 0,
                             Tolerance = t1.Tolerance,
                             Consumption = t1.Consumption,
                             ConRequiredQty = t1.ConRequiredQty,
                             RequiredQty = t1.RequiredQty,
                             UnitPrice = (decimal)t1.UnitPrice,
                             TotalPrice = (decimal)t1.TotalPrice,
                             Description = t1.Remarks,
                             Color = t9 != null ? t9.Name : string.Empty,
                             Size = t11 != null ? t11.Name : string.Empty,
                             SelfReferenceFK = t1.SelfReferenceFK == null ? 0 : (int)t1.SelfReferenceFK,
                             FabricName = parent != null ? (RI != null ? RI.Name + "-" + (C != null ? C.Name : string.Empty) + "-" + parent.GSM : string.Empty) : string.Empty,
                             IsLocked = t1.IsLocked,
                             IsApproved = st.IsComplete
                         }).OrderByDescending(a => a.ID).ToList();
            VMBOM vMBOM = new VMBOM
            {
                DataList = vData,
                RawCategory = vCat.FirstOrDefault().Name
            };
            if (vMBOM.DataList.Any() && vMBOM.DataList != null)
            {
                vMBOM.TotalPrice = vMBOM.DataList.Sum(a => a.TotalPrice);
                vMBOM.RequiredQty = vMBOM.DataList.Sum(a => a.RequiredQty);
            }
            return vMBOM;
        }

        public List<VMBOM> PRItemGetByStyleID(int StyleID, int TypeID = 0)
        {
            List<VMBOM> v3Data = new List<VMBOM>();
            if (TypeID == 1)
            {
                var vData = (from t1 in _db.Merchandising_StyleSlave
                             join t2 in _db.Common_RawItem on t1.Common_RawItemFK equals t2.ID
                             join t3 in _db.Common_RawSubCategory on t2.Common_RawSubCategoryFK equals t3.ID
                             join t4 in _db.Common_RawCategory on t3.Common_RawCategoryFK equals t4.ID
                             join t6 in _db.Common_Unit on t2.Common_UnitFK equals t6.ID
                             join t7 in _db.Merchandising_Style on t1.Merchandising_StyleFK equals t7.ID
                             join t8 in _db.Common_Color on t1.Common_ColorFK equals t8.ID into color
                             from t9 in color.DefaultIfEmpty()
                             join t11 in _db.Common_Size on t1.Common_SizeFk equals t11.ID into Join_Size
                             from t13 in Join_Size.DefaultIfEmpty()
                             join t10 in _db.Merchandising_StyleSlave on t1.SelfReferenceFK equals t10.ID into self
                             from parent in self.DefaultIfEmpty()
                             join RI in _db.Common_RawItem on parent.Common_RawItemFK equals RI.ID into RI_Join
                             from RI in RI_Join.DefaultIfEmpty()
                                 //join RSC in _db.Common_RawSubCategory on RI.Common_RawSubCategoryFK equals RSC.ID into RSC_Join
                                 //from RSC in RSC_Join.DefaultIfEmpty()
                             join C in _db.Common_Color on parent.Common_ColorFK equals C.ID into C_Join
                             from C in C_Join.DefaultIfEmpty()
                             where t1.Active == true && t1.Merchandising_StyleFK == StyleID && t1.IsLocked == true
                             select new VMBOM
                             {
                                 ID = t1.ID,
                                 Merchandising_StyleFK = t1.Merchandising_StyleFK,
                                 Common_RawCategoryFK = t4.ID,
                                 Common_RawSubCategoryFK = t3.ID,
                                 Common_RawItemFK = (int)t1.Common_RawItemFK,
                                 Merchandising_CBSSlaveFK = t1.Merchandising_CBSSlaveFK,
                                 Style = t7.StyleName,
                                 RawCategory = t4.Name,
                                 RawSubCategory = t3.Name,
                                 RawItem = t2.Name,
                                 Currency = "$",
                                 GSM = t1.GSM,

                                 Common_ColorFK = t1.Common_ColorFK == null ? 0 : (int)t1.Common_ColorFK,
                                 Color = t9 != null ? t9.Name : string.Empty,
                                 Common_SizeFK = t1.Common_SizeFk == null ? 0 : (int)t1.Common_SizeFk,
                                 Size = t13 != null ? t13.Name : "",
                                 Tolerance = t1.Tolerance,
                                 ConRequiredQty = t1.ConRequiredQty,
                                 Consumption = t1.Consumption,
                                 RequiredQty = t1.RequiredQty,
                                 UnitPrice = (decimal)t1.UnitPrice,
                                 TotalPrice = (decimal)t1.TotalPrice,
                                 UnitName = t6.Name,
                                 SelfReferenceFK = t1.SelfReferenceFK == null ? 0 : (int)t1.SelfReferenceFK,
                                 FabricName = parent != null ? (RI != null ? RI.Name + "-" + (C != null ? C.Name : string.Empty) + "-" + parent.GSM : string.Empty) : string.Empty,
                                 IsBOM = true,
                                 IsFabric = t1.IsFabric,
                                 Description = t1.Remarks
                             }).OrderBy(a => a.Common_RawCategoryFK).ToList();

                ///edited by mamun vai on 19/6/2019 on fahim's machine
                var v2Data = (from t1 in _db.Merchandising_BOF
                              join t2 in _db.Common_RawItem on t1.Common_RawItemFK equals t2.ID
                              join t3 in _db.Common_RawSubCategory on t2.Common_RawSubCategoryFK equals t3.ID
                              join t4 in _db.Common_RawCategory on t3.Common_RawCategoryFK equals t4.ID
                              //join t5 in _db.Common_Currency on t1.Common_CurrencyFK equals t5.ID
                              join t6 in _db.Common_Unit on t2.Common_UnitFK equals t6.ID
                              join t7 in _db.Merchandising_Style on t1.Merchandising_StyleFK equals t7.ID
                              where t1.Active == true && t1.Merchandising_StyleFK == StyleID
                              select new VMBOM
                              {
                                  ID = t1.ID,
                                  Merchandising_StyleFK = t1.Merchandising_StyleFK,
                                  Common_RawCategoryFK = t4.ID,
                                  Common_RawSubCategoryFK = t3.ID,
                                  Common_RawItemFK = (int)t1.Common_RawItemFK,
                                  Style = t7.StyleName,

                                  Common_ColorFK = 1, //t1.Common_ColorFK == null ? 0 : (int)t1.Common_ColorFK,
                                  Color = "", //t9 != null ? t9.Name : string.Empty,
                                  Description = t1.Description,
                                  RawCategory = t4.Name,
                                  RawSubCategory = t3.Name,
                                  RawItem = t2.Name,
                                  Currency = "$",
                                  Consumption = t1.Consumption,
                                  RequiredQty = t1.RequiredQuantity.Value,
                                  UnitPrice = (decimal)t1.Price,
                                  TotalPrice = ((decimal)t1.Price * t1.RequiredQuantity.Value),
                                  UnitName = t6.Name,
                                  IsBOM = false,
                                  IsFabric = t1.IsFabric
                              }).OrderBy(a => a.Common_RawCategoryFK).ToList();

                v3Data = vData.Union(v2Data).ToList();
            }
            else if (TypeID == 2)
            {
                v3Data = (from t1 in _db.Merchandising_StyleSlave
                          join t2 in _db.Common_RawItem on t1.Common_RawItemFK equals t2.ID
                          join t3 in _db.Common_RawSubCategory on t2.Common_RawSubCategoryFK equals t3.ID
                          join t4 in _db.Common_RawCategory on t3.Common_RawCategoryFK equals t4.ID
                          join t6 in _db.Common_Unit on t2.Common_UnitFK equals t6.ID
                          join t7 in _db.Merchandising_Style on t1.Merchandising_StyleFK equals t7.ID

                          join t8 in _db.Common_Color on t1.Common_ColorFK equals t8.ID into color
                          from t9 in color.DefaultIfEmpty()
                          join t11 in _db.Common_Size on t1.Common_SizeFk equals t11.ID into Join_Size
                          from t13 in Join_Size.DefaultIfEmpty()
                          join t10 in _db.Merchandising_StyleSlave on t1.SelfReferenceFK equals t10.ID into self
                          from parent in self.DefaultIfEmpty()
                          join RI in _db.Common_RawItem on parent.Common_RawItemFK equals RI.ID into RI_Join
                          from RI in RI_Join.DefaultIfEmpty()
                          join C in _db.Common_Color on parent.Common_ColorFK equals C.ID into C_Join
                          from C in C_Join.DefaultIfEmpty()
                              //join RSC in _db.Common_RawSubCategory on RI.Common_RawSubCategoryFK equals RSC.ID into RSC_Join
                              //from RSC in RSC_Join.DefaultIfEmpty()
                          where t1.Active == true && t1.Merchandising_StyleFK == StyleID && t1.IsLocked == true
                          select new VMBOM
                          {
                              ID = t1.ID,
                              Merchandising_StyleFK = t1.Merchandising_StyleFK,
                              Common_RawCategoryFK = t4.ID,
                              Common_RawSubCategoryFK = t3.ID,
                              Common_RawItemFK = (int)t1.Common_RawItemFK,
                              Merchandising_CBSSlaveFK = t1.Merchandising_CBSSlaveFK,
                              Style = t7.StyleName,
                              RawCategory = t4.Name,
                              RawSubCategory = t3.Name,
                              RawItem = t2.Name,
                              Currency = "$",
                              GSM = t1.GSM,

                              Common_ColorFK = t1.Common_ColorFK,// == null ? 0 : (int)t1.Common_ColorFK,
                              Color = t9 != null ? t9.Name : string.Empty,
                              Common_SizeFK = t1.Common_SizeFk == null ? 0 : (int)t1.Common_SizeFk,
                              Size = t13 != null ? t13.Name : "",

                              Tolerance = t1.Tolerance,
                              ConRequiredQty = t1.ConRequiredQty,
                              Consumption = t1.Consumption,
                              RequiredQty = t1.RequiredQty,
                              UnitPrice = (decimal)t1.UnitPrice,
                              TotalPrice = (decimal)t1.TotalPrice,
                              UnitName = t6.Name,
                              SelfReferenceFK = t1.SelfReferenceFK == null ? 0 : (int)t1.SelfReferenceFK,
                              FabricName = parent != null ? (RI != null ? RI.Name + "-" + (C != null ? C.Name : string.Empty) + "-" + parent.GSM : string.Empty) : string.Empty,
                              IsBOM = true,
                              IsFabric = t1.IsFabric,
                              Description = t1.Remarks
                          }).OrderBy(a => a.Common_RawCategoryFK).ToList();
            }
            else if (TypeID == 3)
            {
                v3Data = (from t1 in _db.Merchandising_BOF
                          join t2 in _db.Common_RawItem on t1.Common_RawItemFK equals t2.ID
                          join t3 in _db.Common_RawSubCategory on t2.Common_RawSubCategoryFK equals t3.ID
                          join t4 in _db.Common_RawCategory on t3.Common_RawCategoryFK equals t4.ID
                          join t6 in _db.Common_Unit on t2.Common_UnitFK equals t6.ID
                          join t7 in _db.Merchandising_Style on t1.Merchandising_StyleFK equals t7.ID

                          //join t8 in _db.Common_Color on t2.Common_ColorFK equals t8.ID into color
                          //from t9 in color.DefaultIfEmpty()
                          where t1.Active == true && t1.Merchandising_StyleFK == StyleID
                          select new VMBOM
                          {
                              ID = t1.ID,
                              Merchandising_StyleFK = t1.Merchandising_StyleFK,
                              Common_RawCategoryFK = t4.ID,
                              Common_RawSubCategoryFK = t3.ID,
                              Common_RawItemFK = (int)t1.Common_RawItemFK,
                              Style = t7.StyleName,

                              Common_ColorFK = 1, //t1.Common_ColorFK,
                              Color = t1.Description,  //t9 != null ? t9.Name : string.Empty,

                              RawCategory = t4.Name,
                              RawSubCategory = t3.Name,
                              RawItem = t2.Name,
                              Currency = "$",
                              Description = t1.Description,
                              Consumption = t1.Consumption,
                              RequiredQty = t1.RequiredQuantity.Value,
                              UnitPrice = (decimal)t1.Price,
                              TotalPrice = ((decimal)t1.Price * t1.RequiredQuantity.Value),
                              UnitName = t6.Name,
                              IsBOM = false,
                              IsFabric = t1.IsFabric
                          }).OrderBy(a => a.Common_RawCategoryFK).ToList();
            }

            return v3Data;
        }

        public VMBOM BOMItemGetByID(int ID)
        {
            var vData = (from t1 in _db.Merchandising_StyleSlave
                         join t2 in _db.Merchandising_Style on t1.Merchandising_StyleFK equals t2.ID
                         join t3 in _db.Common_RawItem on t1.Common_RawItemFK equals t3.ID
                         join t4 in _db.Common_RawSubCategory on t3.Common_RawSubCategoryFK equals t4.ID
                         join t5 in _db.Common_RawCategory on t4.Common_RawCategoryFK equals t5.ID
                         join t6 in _db.Common_Color on t1.Common_ColorFK equals t6.ID into color
                         from t8 in color.DefaultIfEmpty()
                         join t9 in _db.Common_Size on t1.Common_SizeFk equals t9.ID into Join_Size
                         from t10 in Join_Size.DefaultIfEmpty()
                         join t7 in _db.Merchandising_StyleSlave on t1.SelfReferenceFK equals t7.ID into self
                         from parent in self.DefaultIfEmpty()
                         join RI in _db.Common_RawItem on parent.Common_RawItemFK equals RI.ID into RI_Join
                         from RI in RI_Join.DefaultIfEmpty()
                             //join RSC in _db.Common_RawSubCategory on RI.Common_RawSubCategoryFK equals RSC.ID into RSC_Join
                             //from RSC in RSC_Join.DefaultIfEmpty()
                         join C in _db.Common_Color on parent.Common_ColorFK equals C.ID into C_Join
                         from C in C_Join.DefaultIfEmpty()
                         where t1.Active == true && t1.ID == ID
                         select new VMBOM
                         {
                             ID = t1.ID,
                             Merchandising_StyleFK = t1.Merchandising_StyleFK,
                             Common_RawCategoryFK = t5.ID,
                             Common_RawSubCategoryFK = t4.ID,
                             Common_RawItemFK = t1.Common_RawItemFK.Value,
                             Merchandising_CBSSlaveFK = t1.Merchandising_CBSSlaveFK.Value,
                             RawCategory = t5.Name,
                             RawSubCategory = t4.Name,
                             RawItem = t3.Name,
                             GSM = t1.GSM,
                             Common_ColorFK = t1.Common_ColorFK == null ? 0 : (int)t1.Common_ColorFK,
                             Common_SizeFK = t1.Common_SizeFk == null ? 0 : (int)t1.Common_SizeFk,
                             Color = t8 != null ? t8.Name : string.Empty,
                             Size = t10 != null ? t10.Name : string.Empty,
                             Style = t2.StyleName,
                             Consumption = t1.Consumption,
                             ConRequiredQty = t1.ConRequiredQty,
                             Tolerance = (decimal)t1.Tolerance,
                             Currency = "$",
                             UnitPrice = (decimal)t1.UnitPrice,
                             RequiredQty = (decimal)t1.RequiredQty,
                             TotalPrice = decimal.Multiply(t1.UnitPrice, t1.RequiredQty),
                             Description = t1.Remarks,
                             SelfReferenceFK = t1.SelfReferenceFK == null ? 0 : (int)t1.SelfReferenceFK,
                             FabricName = parent != null ? (RI != null ? RI.Name + "-" + (C != null ? C.Name : string.Empty) + "-" + parent.GSM : string.Empty) : string.Empty,
                             ActionId = (int)ActionEnum.Edit,
                             IsApproved = t2.IsComplete
                         }).FirstOrDefault();
            return vData;
        }

        public async Task<int> BOMGroupAdd(VMStyleView model)
        {
            var result = -1;
            List<Merchandising_StyleSlave> lstBom = new List<Merchandising_StyleSlave>();
            if (model.TypeID == 1)
            {
                var vGetCBS = _db.Merchandising_CBSSlave.Where(a => a.Active == true && a.Merchandising_CBSFK == model.ImportID);
                if (vGetCBS.Any())
                {
                    foreach (var v in vGetCBS)
                    {
                        model.UpdateRequiredQty = decimal.Zero;
                        model.UpdateConsumption = decimal.Zero;
                        if (v.Consumption > 0)
                        {
                            model.UpdateConsumption = v.Consumption;
                            //modifyConsumption = decimal.Multiply(decimal.Divide(v.Consumption, v.RequiredQty), model.vmStyle.PieceQty);
                            model.UpdateRequiredQty = decimal.Multiply(decimal.Divide(model.vmStyle.PieceQty, 12), v.Consumption);
                        }
                        else
                        {
                            model.UpdateRequiredQty = v.RequiredQty;
                            model.UpdateConsumption = decimal.Divide(v.RequiredQty, decimal.Divide(model.vmStyle.PieceQty, 12));
                        }
                        //requiredQty = decimal.Multiply(decimal.Divide(model.vmStyle.PieceQty, 12), modifyConsumption);
                        Merchandising_StyleSlave oStyleSlave = new Merchandising_StyleSlave
                        {
                            Merchandising_StyleFK = model.vmStyle.ID,
                            Merchandising_CBSSlaveFK = v.ID,
                            Common_ColorFK = v.Common_ColorFK,
                            GSM = v.GSM,
                            ConRequiredQty = model.vmStyle.PieceQty,
                            //Common_CurrencyFK = v.Common_CurrencyFK,
                            Common_RawItemFK = v.Common_RawItemFK,
                            Consumption = model.UpdateConsumption,
                            RequiredQty = model.UpdateRequiredQty,
                            UnitPrice = v.UnitPrice,
                            Tolerance = 0,
                            TotalPrice = decimal.Multiply(v.UnitPrice, model.UpdateRequiredQty)
                        };
                        lstBom.Add(oStyleSlave);
                    }
                }
            }
            else if (model.TypeID == 2)
            {
                var vGetBOM = _db.Merchandising_StyleSlave.Where(a => a.Active == true && a.Merchandising_StyleFK == model.ImportID);
                if (vGetBOM.Any())
                {
                    foreach (var v in vGetBOM)
                    {
                        model.UpdateRequiredQty = decimal.Zero;
                        model.UpdateConsumption = decimal.Zero;
                        if (v.Consumption > 0)
                        {
                            model.UpdateConsumption = v.Consumption;
                            //modifyConsumption = decimal.Multiply(decimal.Divide(v.Consumption, v.RequiredQty), model.vmStyle.PieceQty);
                            model.UpdateRequiredQty = decimal.Multiply(decimal.Divide(model.vmStyle.PieceQty, 12), v.Consumption);
                        }
                        else
                        {
                            model.UpdateRequiredQty = v.RequiredQty;
                            model.UpdateConsumption = decimal.Divide(v.RequiredQty, decimal.Divide(model.vmStyle.PieceQty, 12));
                        }
                        //requiredQty = decimal.Multiply(decimal.Divide(model.vmStyle.PieceQty, 12), modifyConsumption);
                        Merchandising_StyleSlave oStyleSlave = new Merchandising_StyleSlave
                        {
                            Merchandising_StyleFK = model.vmStyle.ID,
                            Merchandising_CBSSlaveFK = v.Merchandising_CBSSlaveFK,
                            Common_CurrencyFK = v.Common_CurrencyFK,
                            Common_RawItemFK = v.Common_RawItemFK,
                            Consumption = model.UpdateConsumption,
                            ConRequiredQty = model.vmStyle.PieceQty,
                            RequiredQty = model.UpdateRequiredQty,
                            UnitPrice = v.UnitPrice,
                            Tolerance = v.Tolerance,
                            TotalPrice = decimal.Multiply(v.UnitPrice, model.UpdateRequiredQty)
                        };
                        lstBom.Add(oStyleSlave);
                    }
                }
            }

            _db.Merchandising_StyleSlave.AddRange(lstBom);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = model.vmStyle.ID;
            }
            return result;
        }

        public string GetBOMCategoryName(int CategoryID)
        {
            return _db.Common_RawCategory.FirstOrDefault(x => x.ID == CategoryID).Name;
        }
        #endregion

        #region ShipmentSchedule

        private bool CheckShipmentQty(VMStyleView model)
        {
            bool IsExcess = false;
            int totalAssignQty = 0;
            var getShipment = _db.Merchandising_StyleShipmentSchedule.Where(a => a.Merchandising_StyleFK == model.vmStyle.ID && a.Active == true).Sum(a => a.Quantity);

            if (!model.ListColorSize.Any(a => a.Quantity <= 0))
            {
                totalAssignQty = model.ListColorSize.Sum(a => a.Quantity);
            }

            int totalQty = (int)getShipment + totalAssignQty;
            //int orderPiece = (int)decimal.Multiply(model.vmStyle.SetQuantity, model.vmStyle.PackQty);
            int orderPiece = model.vmStyle.ProductionPieceQty;
            if (totalQty > orderPiece)
            {
                IsExcess = true;
            }
            return IsExcess;
        }

        public async Task<VMShipmentSchedule> ShipmentGet()
        {
            List<VMShipmentSchedule> shipmentItem = new List<VMShipmentSchedule>();
            var DummyData = new VMShipmentSchedule
            {
                ID = 1,
                Merchandising_StyleFK = 1,
                Common_CountryFK = 1,
                Common_CountryPortFK = 1,
                Destination = "USA",
                PortNo = "PS00125",
                Style = "MS0025896",
                Date = DateTime.Now,
                Quantity = 5000,
                ColorSize = "Red-3000 Blue-200"
            };
            shipmentItem.Add(DummyData);
            VMShipmentSchedule vMShipment = new VMShipmentSchedule
            {
                DataList = await Task.Run(() => shipmentItem)
            };

            return vMShipment;
        }
        public async Task<int> ShipmentAdd(VMStyleView model)
        {
            var result = -1;

            if (CheckShipmentQty(model))
            {
                result = -1;
            }
            else
            {
                if (!model.ListColorSize.Any(a => a.Quantity <= 0))
                {
                    Merchandising_StyleShipmentSchedule sSchedule = new Merchandising_StyleShipmentSchedule();
                    var vConvertData = GetShipmentColorSizeRatioToQty(model.ListColorSize).Where(a => a.Ratio != 0);

                    var vData = _db.Merchandising_StyleShipmentSchedule.Where(a => a.Common_CountryFK == model.vmShipmentSchedule.Common_CountryFK && a.Common_CountryPortFK == model.vmShipmentSchedule.Common_CountryPortFK && a.DestNo == model.vmShipmentSchedule.DestinationNo && a.Merchandising_StyleFK == model.vmStyle.ID && a.ShipmentDate.Date == model.vmShipmentSchedule.Date.Date && a.Active == true);
                    if (vData.Any())
                    {
                        var updatePort = vData.FirstOrDefault();
                        sSchedule.ID = updatePort.ID;
                        foreach (var v in vConvertData)
                        {
                            Merchandising_StyleShipmentRatio ratio = new Merchandising_StyleShipmentRatio();
                            ratio.Merchandising_StyleShipmentScheduleFK = updatePort.ID;
                            ratio.Merchandising_StyleMeasurementFK = v.Merchandising_StyleMeasurementFK;
                            ratio.Quantity = (int)v.Quantity;
                            ratio.Ratio = v.Ratio;
                            ratio.Merchandising_StyleSetPackFK = v.SetPackFK;
                            _db.Merchandising_StyleShipmentRatio.Add(ratio);
                        }
                    }
                    else
                    {
                        sSchedule.Merchandising_StyleFK = model.vmStyle.ID;
                        sSchedule.ShipmentDate = model.vmShipmentSchedule.Date;
                        sSchedule.Common_CountryFK = model.vmShipmentSchedule.Common_CountryFK;
                        sSchedule.Common_CountryPortFK = model.vmShipmentSchedule.Common_CountryPortFK;
                        sSchedule.DestNo = model.vmShipmentSchedule.DestinationNo;
                        sSchedule.Quantity = vConvertData.Sum(a => a.Quantity);
                        _db.Merchandising_StyleShipmentSchedule.Add(sSchedule);

                        foreach (var v in vConvertData)
                        {
                            Merchandising_StyleShipmentRatio ratio = new Merchandising_StyleShipmentRatio();
                            ratio.Merchandising_StyleShipmentScheduleFK = sSchedule.ID;
                            ratio.Quantity = (int)v.Quantity;
                            ratio.Merchandising_StyleMeasurementFK = v.Merchandising_StyleMeasurementFK;
                            ratio.Ratio = v.Ratio;
                            ratio.Merchandising_StyleSetPackFK = v.SetPackFK;
                            _db.Merchandising_StyleShipmentRatio.Add(ratio);
                        }
                    }

                    if (await _db.SaveChangesAsync() > 0)
                    {
                        UpdatePort(sSchedule.ID);
                        result = sSchedule.ID;
                    }
                }
            }

            return result;
        }
        public async Task<int> ShipmentEdit(VMStyleView model)
        {
            var result = -1;
            Merchandising_StyleShipmentSchedule shipment = _db.Merchandising_StyleShipmentSchedule.Find(model.vmShipmentSchedule.ID);
            shipment.ShipmentDate = model.vmShipmentSchedule.Date;

            if (await _db.SaveChangesAsync() > 0)
            {
                result = model.vmShipmentSchedule.ID;
            }
            return result;
        }
        public async Task<int> ShipmentDelete(int id)
        {
            var result = -1;
            var oRatio = _db.Merchandising_StyleShipmentRatio.Where(a => a.Merchandising_StyleShipmentScheduleFK == id && a.Active == true);

            if (oRatio.Any())
            {
                foreach (var v in oRatio)
                {
                    v.Active = false;
                }
            }

            var oShipment = _db.Merchandising_StyleShipmentSchedule.Where(a => a.ID == id && a.Active == true);
            if (oShipment.Any())
            {
                foreach (var v in oShipment)
                {
                    v.Active = false;
                }
            }
            if (await _db.SaveChangesAsync() > 0)
            {
                result = id;
            }
            return result;
        }

        public async Task<VMShipmentSchedule> ShipmentByStyleId2(int ID)
        {
            var vData = (from t1 in _db.Merchandising_StyleShipmentSchedule
                         join t2 in _db.Common_Country on t1.Common_CountryFK equals t2.ID
                         join t3 in _db.Common_CountryPort on t1.Common_CountryPortFK equals t3.ID
                         join t4 in _db.Merchandising_Style on t1.Merchandising_StyleFK equals t4.ID
                         where t1.Active == true && t1.Merchandising_StyleFK == ID
                         select new VMShipmentSchedule
                         {
                             ID = (int)t1.ID,
                             Merchandising_StyleFK = (int)t1.Merchandising_StyleFK,
                             Common_CountryFK = (int)t1.Common_CountryFK,
                             Common_CountryPortFK = (int)t1.Common_CountryPortFK,
                             Destination = t2.Name,
                             PortNo = t3.Name,
                             DestinationNo = t1.DestNo,
                             Quantity = (int)t1.Quantity,
                             PackPieceQty = t4.PackPieceQty,
                             PiecsQuantity = (int)decimal.Multiply(decimal.Multiply(t1.Quantity, t4.PackPieceQty), t4.SetQuantity),
                             ColorSize = t1.Remarks,
                             Style = t4.CID,
                             Date = t1.ShipmentDate
                         }).AsEnumerable().OrderBy(a => a.ID);

            //var styleSubSet = _db.Merchandising_StyleSetPack.Where(a=>a.Merchandising_StyleFK==ID && a.Active==true);

            //foreach (var v in vData)
            //{

            //var SetPack = (from t1 in _db.Merchandising_StyleShipmentRatio
            //               join t2 in _db.Merchandising_StyleShipmentSchedule on t1.Merchandising_StyleShipmentScheduleFK equals t2.ID
            //               where t1.Active == true && t2.Active == true && t2.Merchandising_StyleFK == ID && t2.ID == v.ID
            //               group t1.Quantity by new { t1.Merchandising_StyleSetPackFK,t1.Merchandising_StyleShipmentScheduleFK } into all
            //               select new
            //               {
            //                   ID=all.Key.Merchandising_StyleShipmentScheduleFK,
            //                   SetPackId = all.Key.Merchandising_StyleSetPackFK
            //               }).ToList();

            //if (SetPack.Any())
            //{
            //    foreach () { }
            //    if (styleSubSet.Any(a => a.ID == v.StyleSetPackFK))
            //    {
            //        v.PiecsQuantity = (int)decimal.Multiply(decimal.Multiply(v.Quantity, v.PackPieceQty), styleSubSet.FirstOrDefault(a => a.ID == v.StyleSetPackFK).Quantity);
            //    }
            //    else
            //    {
            //        v.PiecsQuantity = (int)decimal.Multiply(decimal.Multiply(v.Quantity, v.PackPieceQty), 1);
            //    }
            //}
            //else
            //{
            //    v.PiecsQuantity = (int)decimal.Multiply(decimal.Multiply(v.Quantity, v.PackPieceQty), 1);
            //}

            //}


            VMShipmentSchedule vMShipment = new VMShipmentSchedule
            {
                DataList = await Task.Run(() => vData)
            };
            return vMShipment;
        }
        public VMShipmentColorSize ShipmentSizeRatioByStyleId(int ID)
        {
            var setPack = _db.Merchandising_StyleSetPack.Where(a => a.Merchandising_StyleFK == ID && a.Active == true);
            var vData = (from t1 in _db.Merchandising_StyleShipmentSchedule
                         join t2 in _db.Common_Country on t1.Common_CountryFK equals t2.ID
                         join t3 in _db.Common_CountryPort on t1.Common_CountryPortFK equals t3.ID
                         join t5 in _db.Merchandising_StyleShipmentRatio on t1.ID equals t5.Merchandising_StyleShipmentScheduleFK
                         join t6 in _db.Merchandising_StyleMeasurement on t5.Merchandising_StyleMeasurementFK equals t6.ID
                         join t7 in _db.Common_Color on t6.Common_ColorFK equals t7.ID
                         join t8 in _db.Common_Size on t6.Common_SizeFK equals t8.ID
                         join t9 in _db.Merchandising_Style on t1.Merchandising_StyleFK equals t9.ID
                         join t10 in _db.Merchandising_StyleSetPack on t5.Merchandising_StyleSetPackFK equals t10.ID into Set_Join
                         from t11 in Set_Join.DefaultIfEmpty()
                         where t1.Active == true && t5.Active == true && t1.Merchandising_StyleFK == ID
                         select new VMShipmentColorSize
                         {
                             ID = t5.ID,
                             Merchandising_StyleMeasurementFK = (int)t5.Merchandising_StyleMeasurementFK,
                             Common_ColorFK = t6.Common_ColorFK,
                             Common_SizeFK = t6.Common_SizeFK,
                             Destination = t2.Name,
                             PortNo = t3.Name,
                             DestinationNo = t1.DestNo,
                             Color = t7.Name,
                             Size = t8.Name,
                             Quantity = (int)t5.Quantity,
                             SetPackFK = t11.ID,
                             SetPackQty = t11.Quantity,
                             SubSet = t11.SetPackName,
                             PackPerPiece = t9.PackPieceQty,
                             //ProductionQty = (int)decimal.Multiply(decimal.Multiply(t5.Quantity, t9.PackPieceQty), t9.SetQuantity),
                             Ratio = t5.Ratio
                         }).OrderBy(a => a.ID).ToList();
            if (vData.Any())
            {
                vData.ForEach(a =>
                {
                    if (a.SetPackFK > 0)
                    {
                        a.ProductionQty = (int)decimal.Multiply(decimal.Multiply(a.SetPackQty, a.PackPerPiece), a.Quantity);
                        //a.ProductionQty = (int)decimal.Multiply(a.SetPackQty, a.Quantity);
                    }
                    else
                    {
                        a.ProductionQty = (int)decimal.Multiply(decimal.Multiply(1, a.PackPerPiece), a.Quantity);
                    }
                });
            }

            VMShipmentColorSize vMShipmentRatio = new VMShipmentColorSize
            {
                DataList = vData
            };
            return vMShipmentRatio;
        }

        public async Task<VMShipmentSchedule> ShipmentByStyleId(int ID)
        {
            List<VMShipmentSchedule> lstSchedule = new List<VMShipmentSchedule>();
            var vData = (from t1 in _db.Merchandising_StyleShipmentSchedule
                         join t2 in _db.Common_Country on t1.Common_CountryFK equals t2.ID
                         join t3 in _db.Common_CountryPort on t1.Common_CountryPortFK equals t3.ID
                         join t4 in _db.Merchandising_Style on t1.Merchandising_StyleFK equals t4.ID
                         where t1.Active == true && t1.Merchandising_StyleFK == ID
                         select new
                         {
                             ID = (int)t1.ID,
                             Merchandising_StyleFK = (int)t1.Merchandising_StyleFK,
                             Common_CountryFK = (int)t1.Common_CountryFK,
                             Common_CountryPortFK = (int)t1.Common_CountryPortFK,
                             Destination = t2.Name,
                             PortNo = t3.Name,
                             DestinationNo = t1.DestNo,
                             Quantity = (int)t1.Quantity,
                             PackPieceQty = t4.PackPieceQty,
                             ColorSize = t1.Remarks,
                             Style = t4.CID,
                             Date = t1.ShipmentDate,
                             //PiecsQuantity = (int)decimal.Multiply(decimal.Multiply(t1.Quantity, t4.PackPieceQty), t4.SetQuantity),


                         }).AsEnumerable().OrderBy(a => a.ID);
            var styleSubSet = _db.Merchandising_StyleSetPack.Where(a => a.Merchandising_StyleFK == ID && a.Active == true).ToList();

            foreach (var v in vData)
            {
                VMShipmentSchedule objData = new VMShipmentSchedule();
                objData.ID = (int)v.ID;
                objData.Merchandising_StyleFK = (int)v.Merchandising_StyleFK;
                objData.Common_CountryFK = (int)v.Common_CountryFK;
                objData.Common_CountryPortFK = (int)v.Common_CountryPortFK;
                objData.Destination = v.Destination;
                objData.PortNo = v.PortNo;
                objData.DestinationNo = v.DestinationNo;
                objData.Quantity = (int)v.Quantity;
                objData.PackQuantity = (int)decimal.Multiply(v.Quantity, v.PackPieceQty);

                objData.PackPieceQty = v.PackPieceQty;
                objData.ColorSize = v.ColorSize;
                objData.Style = v.Style;
                objData.Date = v.Date;
                var vSetPack = (from t1 in _db.Merchandising_StyleShipmentRatio
                                where t1.Merchandising_StyleShipmentScheduleFK == v.ID && t1.Active == true
                                group t1.Quantity by new { t1.Merchandising_StyleSetPackFK, } into all
                                select new { subsetId = all.Key.Merchandising_StyleSetPackFK }).ToList();
                if (vSetPack.Any())
                {
                    foreach (var v1 in vSetPack)
                    {
                        if (styleSubSet.Any(a => a.ID == v1.subsetId))
                        {
                            var qty = styleSubSet.FirstOrDefault(a => a.ID == v1.subsetId);//SubSetPieceQty
                            objData.PiecsQuantity += (int)decimal.Multiply(decimal.Multiply(objData.PackPieceQty, objData.Quantity), qty.Quantity);
                        }
                        else
                        {
                            objData.PiecsQuantity += (int)decimal.Multiply(objData.Quantity, objData.PackPieceQty);
                        }
                    }
                }
                else
                {
                    objData.PiecsQuantity = (int)decimal.Multiply(objData.Quantity, objData.PackPieceQty);
                }
                lstSchedule.Add(objData);
            }


            VMShipmentSchedule vMShipment = new VMShipmentSchedule
            {
                DataList = await Task.Run(() => lstSchedule)
            };
            return vMShipment;
        }

        #endregion

        #region Helper
        private List<VMShipmentColorSize> GetShipmentColorSizeRatioToQty(List<VMColorSize> list)
        {
            List<VMShipmentColorSize> addList = new List<VMShipmentColorSize>();
            if (list.Any())
            {
                var vAllCSize = _db.Merchandising_StyleMeasurement.Where(x => x.Merchandising_StyleFK == list.FirstOrDefault().ID && x.Active == true).ToList();
                int UnitRatio;
                list.ForEach(a =>
                {
                    UnitRatio = 0;
                    UnitRatio = a.Quantity / a.SizeList.DefaultIfEmpty().Sum(x => x.SizeRatio);

                    a.SizeList.ForEach(b =>
                    {
                        b.Quantity = b.SizeRatio * UnitRatio;
                        int m = vAllCSize.FirstOrDefault(c => c.Common_ColorFK == a.ColorId && c.Common_SizeFK == b.SizeId).ID;
                        addList.Add(new VMShipmentColorSize { Merchandising_StyleMeasurementFK = m, Quantity = b.Quantity, Ratio = b.SizeRatio, SetPackFK = b.SetPackFK });
                    });

                });
            }
            return addList;
        }

        public List<VMShipmentColorSize> GetSizeByColorID1(int ColorID, int StyleID, int ColorQty)
        {
            var getAllSize = _db.Merchandising_StyleMeasurement.Where(a => a.Merchandising_StyleFK == StyleID && a.Common_ColorFK == ColorID)
                .Join(_db.Common_Size, a => a.Common_SizeFK, b => b.ID, (a, b) => new VMShipmentColorSize
                {
                    Common_SizeFK = a.Common_SizeFK,
                    Size = b.Name,
                    Common_ColorFK = a.Common_ColorFK,
                    TotalQty = ColorQty,
                    Merchandising_StyleMeasurementFK = a.ID
                }).OrderBy(a => a.Common_SizeFK).ToList();

            return getAllSize;
        }

        public List<VMShipmentColorSize> GetSizeByColorID(List<string> ColorID, List<string> ColorQty, int StyleID)
        {
            List<VMShipmentColorSize> list = new List<VMShipmentColorSize>();
            if (ColorID.Any())
            {
                var getAllColorSize = (from t1 in _db.Merchandising_StyleMeasurement
                                       where t1.Merchandising_StyleFK == StyleID && t1.Active == true
                                       join t2 in _db.Common_Size on t1.Common_SizeFK equals t2.ID
                                       join t3 in _db.Common_Color on t1.Common_ColorFK equals t3.ID
                                       select new VMShipmentColorSize
                                       {
                                           Common_SizeFK = t1.Common_SizeFK,
                                           Size = t2.Name,
                                           Common_ColorFK = t1.Common_ColorFK,
                                           Color = t3.Name,
                                           Merchandising_StyleMeasurementFK = t1.ID
                                       }).ToList();


                List<ColorList> lstColor = new List<ColorList>();
                int countColor = ColorID.Count();
                int countQty = ColorQty.Count();
                if (countColor <= countQty)
                {
                    int ColorFK = 0;
                    int ColorTotalQty = 0;
                    for (int i = 0; i < countColor; i++)
                    {
                        ColorTotalQty = 0;
                        string colorId = ColorID[i];
                        string qty = ColorQty[i];
                        if (!string.IsNullOrEmpty(colorId))
                        {
                            int.TryParse(colorId, out ColorFK);
                            ColorList clist = new ColorList();
                            clist.ColorFK = ColorFK;
                            int.TryParse(qty, out ColorTotalQty);
                            clist.TotalQty = ColorTotalQty;
                            lstColor.Add(clist);
                        }
                    }
                }

                foreach (var v in lstColor)
                {
                    if (getAllColorSize.Any(a => a.Common_ColorFK == v.ColorFK))
                    {
                        var all = getAllColorSize.Where(a => a.Common_ColorFK == v.ColorFK).ToList();
                        all.ForEach(a => { a.TotalQty = v.TotalQty; });
                        list.AddRange(all);
                    }
                }
            }

            return list;
        }

        public void UpdatePort(int ID)
        {
            var vData = _db.Merchandising_StyleShipmentSchedule.Where(a => a.ID == ID && a.Active == true).ToList();
            if (vData.Any())
            {
                var update = vData.FirstOrDefault();
                var vPortData = (from a in _db.Merchandising_StyleShipmentRatio
                                 where a.Merchandising_StyleShipmentScheduleFK == ID && a.Active == true
                                 join b in _db.Merchandising_StyleMeasurement on a.Merchandising_StyleMeasurementFK equals b.ID
                                 join c in _db.Common_Color on b.Common_ColorFK equals c.ID
                                 where a.Active == true
                                 group a.Quantity by new { c.Name } into all
                                 select new
                                 {
                                     Color = all.Key.Name,
                                     Qty = all.Sum()
                                 }).ToList();
                int TotalQty = 0;
                string ColorQty = string.Empty;
                if (vPortData.Any())
                {
                    TotalQty = vPortData.Sum(a => a.Qty);
                    vPortData.ForEach(a =>
                    {
                        ColorQty += a.Color + "-" + a.Qty + ", ";
                    });
                }
                update.Quantity = TotalQty;
                update.Remarks = ColorQty;
                _db.SaveChangesAsync();
            }
        }

        public string GetCategory(int Id)
        {
            return _db.Common_RawCategory.Where(a => a.ID == Id && a.Active == true).FirstOrDefault().Name;
        }

        public List<VMColorSize> GetColorSizeRatioByStyleID(List<string> ColorIds, int StyleID, int? SubSetId)
        {
            string SetName = string.Empty;
            if (SubSetId != null)
            {
                SetName = _db.Merchandising_StyleSetPack.FirstOrDefault(a => a.ID == SubSetId.Value).SetPackName;
            }
            List<VMColorSize> list = new List<VMColorSize>();
            if (ColorIds.Any())
            {

                var vData = (from t1 in _db.Merchandising_StyleMeasurement
                             where t1.Merchandising_StyleFK == StyleID && t1.Active == true
                             join t2 in _db.Common_Size on t1.Common_SizeFK equals t2.ID
                             join t3 in _db.Common_Color on t1.Common_ColorFK equals t3.ID
                             where t1.Active == true
                             select new { ID = t1.ID, SizeId = t1.Common_SizeFK, ColorId = t1.Common_ColorFK, Size = t2.Name, Color = t3.Name }).ToList();
                foreach (var v in ColorIds)
                {
                    int id = 0;
                    int.TryParse(v, out id);

                    var SizeRange = vData.Where(a => a.ColorId == id);
                    if (SizeRange.Any())
                    {
                        var vColor = SizeRange.GroupBy(a => a.ColorId, (key, group) => new { ColorId = key, Items = group.ToList() }).ToList();
                        if (vColor.Any())
                        {
                            foreach (var col in vColor)
                            {
                                VMColorSize mColor = new VMColorSize();
                                mColor.ColorId = col.ColorId;
                                mColor.Color = col.Items.FirstOrDefault().Color;
                                mColor.ID = StyleID;
                                mColor.SubSetName = SetName;
                                var vSize = SizeRange.GroupBy(a => a.SizeId, (key, group) => new { SizeId = key, Items = group.ToList() }).ToList();
                                if (vSize.Any())
                                {
                                    List<VMColorSize> lstSize = new List<VMColorSize>();
                                    foreach (var si in vSize)
                                    {
                                        VMColorSize mSize = new VMColorSize();
                                        if (SubSetId != null)
                                        {
                                            mSize.SetPackFK = SubSetId.Value;
                                        }
                                        //mSize.ID = si.Items.FirstOrDefault(a=>a.ColorId== mColor.ColorId && a.SizeId==si.SizeId).ID;
                                        mSize.SizeId = si.SizeId;
                                        mSize.Size = si.Items.FirstOrDefault().Size;

                                        lstSize.Add(mSize);
                                    }
                                    mColor.SizeList = lstSize.OrderBy(a => a.SizeId).ToList();
                                }

                                list.Add(mColor);
                            }
                        }
                    }
                }
            }
            var vData1 = list.OrderBy(a => a.ColorId).ToList();
            return vData1;
        }

        public string GetSizeRangeByStyleID(int ID)
        {
            string SizeName = string.Empty;
            var vData = (from t1 in _db.Merchandising_StyleMeasurement
                         where t1.Merchandising_StyleFK == ID && t1.Active == true
                         join t2 in _db.Common_Size on t1.Common_SizeFK equals t2.ID
                         join t3 in _db.Common_Color on t1.Common_ColorFK equals t3.ID
                         where t1.Active == true
                         group t1 by new { t1.Common_SizeFK, t2.Name } into all
                         select new { Size = all.Key.Name, ID = all.Key.Common_SizeFK }).OrderBy(a => a.ID).ToList();

            if (vData.Any())
            {
                vData.ForEach(a =>
                {
                    SizeName += a.Size + " ";
                });
            }
            return SizeName;
        }

        public bool CheckBOMCreateByStyleID(int ID)
        {
            var vData = _db.Merchandising_StyleSlave.Where(a => a.Merchandising_StyleFK == ID && a.Active == true);
            bool IsCreate = (bool)vData.Any() == true ? true : false;
            return IsCreate;
        }

        private bool CheckRefInStyle(int Id)
        {
            bool isFound = _db.Merchandising_Style.Any(a => a.Merchandising_BuyerOrderFK == Id && a.Active == true);
            return isFound;
        }

        private bool CheckBomByStyleID(int ID)
        {
            bool isFound = _db.Merchandising_StyleSlave.Any(a => a.Merchandising_StyleFK == ID && a.Active == true);
            return isFound;
        }

        private bool CheckShipmentByStyleID(int ID)
        {
            bool isFound = _db.Merchandising_StyleShipmentSchedule.Any(a => a.Merchandising_StyleFK == ID && a.Active == true);
            return isFound;
        }
        private bool CheckBOFByStyleID(int ID)
        {
            bool isFound = _db.Merchandising_BOF.Any(a => a.Merchandising_StyleFK == ID && a.Active == true);
            return isFound;
        }

        private bool CheckSetPackByStyleID(int ID)
        {
            bool isFound = _db.Merchandising_StyleSetPack.Any(a => a.Merchandising_StyleFK == ID && a.Active == true);
            return isFound;
        }

        private bool CheckSMVByStyleID(int ID)
        {
            bool isFound = _db.Plan_SMVDraftLayout.Any(a => a.Merchandising_StyleFK == ID && a.Active == true);
            return isFound;
        }

        private string CheckStyleRef(int ID)
        {
            string isFound = string.Empty;
            if (CheckBomByStyleID(ID) || CheckShipmentByStyleID(ID) || CheckSetPackByStyleID(ID))
            {
                isFound = "hidden";
            }

            return isFound;
        }

        private bool CheckItemByCBSID(int ID)
        {
            bool isFound = _db.Merchandising_CBSSlave.Any(a => a.Merchandising_CBSFK == ID && a.Active == true);
            return isFound;
        }
        #endregion

        #region ShipmentRatio

        public async Task<int> ShipmentRatioDelete(int id)
        {
            var result = -1;
            Merchandising_StyleShipmentRatio oRatio = _db.Merchandising_StyleShipmentRatio.Find(id);

            if (oRatio != null)
            {
                oRatio.Active = false;
                if (await _db.SaveChangesAsync() > 0)
                {
                    UpdatePort((int)oRatio.Merchandising_StyleShipmentScheduleFK);
                    result = id;
                }
            }
            return result;
        }

        #endregion

        #region ManageCBS
        public async Task<VMCBS> CBSGet()
        {
            var vData = (from t1 in _db.Merchandising_CBS
                         join t2 in _db.Merchandising_CBSStyle on t1.Merchandising_CBSStyleFK equals t2.ID
                         join t3 in _db.Common_FinishItem on t2.Common_FinishItemFK equals t3.ID
                         join t4 in _db.Common_Buyer on t1.Common_BuyerFK equals t4.ID into all
                         from t5 in all.DefaultIfEmpty()
                         where t1.Active == true
                         select new VMCBS
                         {
                             ID = t1.ID,
                             CID = t1.CID,
                             StyleName = t3.Name,
                             Merchandising_CBSStyleFK = t1.Merchandising_CBSStyleFK,
                             Common_BuyerFK = t1.Common_BuyerFK,
                             BuyerName = t5.Name,
                             Reference = t1.Reference,
                             OrderQty = t1.OrderQty,
                             OverheadRate = t1.OverheadRate,
                             BuyingCommission = t1.BuyingCommission,
                             ProfitMargin = t1.ProfitMargin,
                             DozonQty = t1.DozonQty,
                             Remarks = t1.Remarks
                         }).OrderByDescending(a => a.ID).ToList();
            VMCBS vMBOM = new VMCBS
            {
                DataList = await Task.Run(() => vData)
            };
            return vMBOM;
        }
        public async Task<int> CBSAdd(VMCBS vMCBS)
        {
            var result = -1;
            vMCBS.DozonQty = decimal.Divide(vMCBS.OrderQty, 12);
            Merchandising_CBS cbs = new Merchandising_CBS
            {
                CID = CidServices.CBSCID(vMCBS.Common_BuyerFK, vMCBS.Merchandising_CBSStyleFK),
                Merchandising_CBSStyleFK = vMCBS.Merchandising_CBSStyleFK,
                Common_BuyerFK = vMCBS.Common_BuyerFK,
                Reference = vMCBS.Reference,
                OrderQty = vMCBS.OrderQty,
                DozonQty = vMCBS.DozonQty,
                OverheadRate = vMCBS.OverheadRate,
                BuyingCommission = vMCBS.BuyingCommission,
                ProfitMargin = vMCBS.ProfitMargin,
                Remarks = vMCBS.Remarks,
                User = vMCBS.User
            };
            _db.Merchandising_CBS.Add(cbs);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = cbs.ID;
            }
            return result;
        }
        public async Task<int> CBSEdit(VMCBS vMCBS)
        {
            var result = -1;
            vMCBS.DozonQty = decimal.Divide(vMCBS.OrderQty, 12);
            Merchandising_CBS cBS = _db.Merchandising_CBS.Find(vMCBS.ID);

            cBS.Merchandising_CBSStyleFK = vMCBS.Merchandising_CBSStyleFK;
            cBS.Common_BuyerFK = vMCBS.Common_BuyerFK;
            cBS.Reference = vMCBS.Reference;
            cBS.OrderQty = vMCBS.OrderQty;
            cBS.DozonQty = vMCBS.DozonQty;
            cBS.OverheadRate = vMCBS.OverheadRate;
            cBS.BuyingCommission = vMCBS.BuyingCommission;
            cBS.ProfitMargin = vMCBS.ProfitMargin;
            cBS.Remarks = vMCBS.Remarks;
            cBS.Active = true;
            cBS.Time = DateTime.Now;

            var vData = _db.Merchandising_CBSSlave.Where(a => a.Merchandising_CBSFK == vMCBS.ID && a.Active == true);
            if (vData.Any())
            {
                foreach (var v in vData)
                {
                    v.RequiredQty = vMCBS.DozonQty * v.Consumption;
                    v.TotalPrice = v.RequiredQty * v.UnitPrice;
                }
            }
            if (await _db.SaveChangesAsync() > 0)
            {
                result = cBS.ID;
            }
            return result;
        }
        public async Task<int> CBSUpdate(VMCBS vMCBS)
        {
            var result = -1;
            vMCBS.DozonQty = decimal.Divide(vMCBS.OrderQty, 12);
            Merchandising_CBS cBS = _db.Merchandising_CBS.Find(vMCBS.ID);
            cBS.OrderQty = vMCBS.OrderQty;
            cBS.DozonQty = vMCBS.DozonQty;
            cBS.Active = true;
            cBS.Time = DateTime.Now;

            var vData = _db.Merchandising_CBSSlave.Where(a => a.Merchandising_CBSFK == vMCBS.ID && a.Active == true);
            if (vData.Any())
            {
                foreach (var v in vData)
                {
                    v.RequiredQty = vMCBS.DozonQty * v.Consumption;
                    v.ConRequiredQty =
                    v.TotalPrice = v.RequiredQty * v.UnitPrice;
                }
            }

            if (await _db.SaveChangesAsync() > 0)
            {
                result = cBS.ID;
            }
            return result;
        }
        public async Task<int> CBSDelete(int id)
        {
            var result = -1;
            if (!_db.Merchandising_CBSSlave.Any(a => a.Merchandising_CBSFK == id))
            {
                Merchandising_CBS selectSingle = _db.Merchandising_CBS.Find(id);
                if (selectSingle != null)
                {
                    selectSingle.Active = false;
                    if (await _db.SaveChangesAsync() > 0)
                    {
                        result = selectSingle.ID;
                    }
                }
            }
            return result;
        }
        public VMCBS CBSGetByID(int Id)
        {
            var vData = (from t1 in _db.Merchandising_CBS
                         join t2 in _db.Merchandising_CBSStyle on t1.Merchandising_CBSStyleFK equals t2.ID
                         join t3 in _db.Common_FinishItem on t2.Common_FinishItemFK equals t3.ID
                         where t1.Active == true && t1.ID == Id
                         select new VMCBS
                         {
                             ID = t1.ID,
                             CID = t1.CID,
                             StyleID = t2.CID,
                             StyleName = t3.Name,
                             Merchandising_CBSStyleFK = t1.Merchandising_CBSStyleFK,
                             Common_BuyerFK = t1.Common_BuyerFK,
                             Reference = t1.Reference,
                             OrderQty = t1.OrderQty,
                             OverheadRate = t1.OverheadRate,
                             BuyingCommission = t1.BuyingCommission,
                             ProfitMargin = t1.ProfitMargin,
                             DozonQty = t1.DozonQty,
                             Remarks = t1.Remarks,
                             ActionId = (int)ActionEnum.Edit
                         }).FirstOrDefault();
            return vData;
        }
        public VMStyleView CBSGetByIDView(int ID)
        {
            var VM = new VMStyleView();
            VM.vmCBS = new VMCBS();
            VM.VMCBSSummary = new VMCBSSummary();
            VM.vmCBS = CBSGetByID(ID);
            VM.VMCBSSummary = GetSummaryByCBSID(VM.vmCBS);
            VM.IsBomCreate = CheckItemByCBSID(ID);
            return VM;
        }

        private VMCBSSummary GetSummaryByCBSID(VMCBS cbs)
        {
            var vDataFabric = GetSummary(cbs.ID, 2);//Fabric
            var vDataWash = GetSummary(cbs.ID, 3);//Wash
            var vDataPrint = GetSummary(cbs.ID, 4);//Print
            var vDataEmbroidery = GetSummary(cbs.ID, 5);//Embroidery
            var vDataTrims = GetSummary(cbs.ID, 6);//Trims_Accesses
            var vDataCharge = GetSummary(cbs.ID, 1023);//Trims_Accesses
            VMCBSSummary model = new VMCBSSummary();

            model.OverheadRate = cbs.OverheadRate;
            model.BuyingComRate = cbs.BuyingCommission;
            model.ProfitMarginRate = cbs.ProfitMargin;

            model.CostFabric = vDataFabric.TotalPrice;
            model.CostWash = vDataWash.TotalPrice;
            model.CostPrint = vDataPrint.TotalPrice;
            model.CostEmbroidiry = vDataEmbroidery.TotalPrice;
            model.CostAccessories = vDataTrims.TotalPrice;
            model.CostCharges = vDataCharge.TotalPrice;
            model.CostTotalValue = model.CostFabric + model.CostWash + model.CostPrint + model.CostEmbroidiry + model.CostAccessories + model.CostCharges;

            model.CostOverheadValue = decimal.Multiply(model.CostTotalValue, decimal.Divide(model.OverheadRate, 100));
            model.CostOHValue = model.CostTotalValue + model.CostOverheadValue;

            model.CostProfitMarginValue = decimal.Divide(decimal.Multiply(model.CostOHValue, model.ProfitMarginRate), 100);

            model.ProductValueProfitValue = model.CostOHValue + model.CostProfitMarginValue;

            model.CostOrderValue = decimal.Divide(decimal.Multiply(model.ProductValueProfitValue, 100), decimal.Subtract(100, model.BuyingComRate));
            model.CostBuyingComValue = decimal.Subtract(model.CostOrderValue, model.ProductValueProfitValue);

            //Dozon
            model.CatFabric = decimal.Multiply(decimal.Divide(model.CostFabric, cbs.OrderQty), 12);
            model.CatAccessories = decimal.Multiply(decimal.Divide(model.CostAccessories, cbs.OrderQty), 12);
            model.CatWash = decimal.Multiply(decimal.Divide(model.CostWash, cbs.OrderQty), 12);
            model.CatPrint = decimal.Multiply(decimal.Divide(model.CostPrint, cbs.OrderQty), 12);
            model.CatEmbroidiry = decimal.Multiply(decimal.Divide(model.CostEmbroidiry, cbs.OrderQty), 12);
            model.CatCharges = decimal.Multiply(decimal.Divide(model.CostCharges, cbs.OrderQty), 12);
            model.CatTotalValue = model.CatFabric + model.CatAccessories + model.CatWash + model.CatPrint + model.CatEmbroidiry + model.CatCharges;

            model.DzOverheadValue = decimal.Multiply(model.CatTotalValue, decimal.Divide(model.OverheadRate, 100));
            model.DzOHValue = model.CatTotalValue + model.DzOverheadValue;

            model.DzProfitMarginValue = decimal.Divide(decimal.Multiply(model.DzOHValue, model.ProfitMarginRate), 100);

            model.DzProductValueProfitValue = model.DzOHValue + model.DzProfitMarginValue;

            model.DzOrderValue = decimal.Divide(decimal.Multiply(model.DzProductValueProfitValue, 100), decimal.Subtract(100, model.BuyingComRate));
            model.DzBuyingComValue = decimal.Subtract(model.DzOrderValue, model.DzProductValueProfitValue);

            return model;
        }
        #endregion

        #region ManageCBSSlave

        private VMCBSSlave GetSummary(int Id, int CatID)
        {
            VMCBSSlave model = new VMCBSSlave();

            var vData = (from t1 in _db.Merchandising_CBSSlave
                         join t2 in _db.Common_RawItem on t1.Common_RawItemFK equals t2.ID
                         join t3 in _db.Common_RawSubCategory on t2.Common_RawSubCategoryFK equals t3.ID
                         join t4 in _db.Common_RawCategory on t3.Common_RawCategoryFK equals t4.ID
                         where t1.Active == true && t1.Merchandising_CBSFK == Id && t4.ID == CatID
                         select new
                         {
                             totalQty = t1.RequiredQty,
                             totalPrice = t1.TotalPrice,
                             unitPrice = t1.UnitPrice
                         }).ToList();
            if (vData.Any())
            {
                model.RequiredQty = vData.Sum(a => a.totalQty);
                model.TotalPrice = vData.Sum(a => a.totalPrice);
                vData.ForEach(a =>
                {
                    model.UnitPrice += decimal.Multiply(a.unitPrice, 12);
                });

            }
            return model;
        }

        public VMCBSSlave CBSSlaveGetByCBSID(int Id, int CatID)
        {
            var vData = (from t1 in _db.Merchandising_CBSSlave
                         join t2 in _db.Common_RawItem on t1.Common_RawItemFK equals t2.ID
                         join t3 in _db.Common_RawSubCategory on t2.Common_RawSubCategoryFK equals t3.ID
                         join t4 in _db.Common_RawCategory on t3.Common_RawCategoryFK equals t4.ID
                         //join t5 in _db.Common_Currency on t1.Common_CurrencyFK equals t5.ID
                         join t6 in _db.Common_Unit on t2.Common_UnitFK equals t6.ID
                         where t1.Active == true && t1.Merchandising_CBSFK == Id && t4.ID == CatID
                         select new VMCBSSlave
                         {
                             ID = t1.ID,
                             Common_RawCategoryFK = t4.ID,
                             Common_RawSubCategoryFK = t3.ID,
                             Common_RawItemFK = (int)t1.Common_RawItemFK,
                             Merchandising_CBSFK = t1.Merchandising_CBSFK,
                             RawCategory = t4.Name,
                             RawSubCategory = t3.Name,
                             RawItem = t2.Name,
                             GSM = t1.GSM,
                             Currency = "$",
                             Consumption = t1.Consumption,
                             ConRequiredQty = t1.ConRequiredQty,
                             Common_ColorFK = t1.Common_ColorFK == null ? 0 : (int)t1.Common_ColorFK,
                             RequiredQty = t1.RequiredQty,
                             UnitType = t6.Name,
                             UnitPrice = (decimal)t1.UnitPrice,
                             TotalPrice = (decimal)t1.TotalPrice
                         }).ToList();

            if (vData.Any())
            {
                vData.ForEach(a =>
                {
                    var vColor = _db.Common_Color.Where(x => x.ID == a.Common_ColorFK);
                    if (vColor.Any())
                    {
                        a.Color = vColor.FirstOrDefault().Name;
                    }
                });
            }

            VMCBSSlave vMCBSSlave = new VMCBSSlave
            {
                DataList = vData
            };

            if (vMCBSSlave.DataList.Any() && vMCBSSlave.DataList != null)
            {
                vMCBSSlave.TotalPrice = vMCBSSlave.DataList.Sum(a => a.TotalPrice);
                vMCBSSlave.RequiredQty = vMCBSSlave.DataList.Sum(a => a.RequiredQty);
            }
            return vMCBSSlave;
        }

        public VMCBSSlave CBSSlaveGetByStyleID(int Id)
        {
            var vData = (from t1 in _db.Merchandising_CBSSlave
                         join t2 in _db.Common_RawItem on t1.Common_RawItemFK equals t2.ID
                         join t3 in _db.Common_RawSubCategory on t2.Common_RawSubCategoryFK equals t3.ID
                         join t4 in _db.Common_RawCategory on t3.Common_RawCategoryFK equals t4.ID
                         //join t5 in _db.Common_Currency on t1.Common_CurrencyFK equals t5.ID
                         join t6 in _db.Merchandising_CBS on t1.Merchandising_CBSFK equals t6.ID
                         where t1.Active == true && t6.Merchandising_CBSStyleFK == Id && t6.Active == true
                         select new VMCBSSlave
                         {
                             ID = t1.ID,
                             Common_RawCategoryFK = t4.ID,
                             Common_RawSubCategoryFK = t3.ID,
                             Common_RawItemFK = (int)t1.Common_RawItemFK,
                             Merchandising_CBSFK = t1.Merchandising_CBSFK,
                             RawCategory = t4.Name,
                             RawSubCategory = t3.Name,
                             RawItem = t2.Name,
                             Currency = "$",
                             Consumption = t1.Consumption,
                             RequiredQty = t1.RequiredQty,
                             UnitPrice = (decimal)t1.UnitPrice,
                             TotalPrice = (decimal)t1.TotalPrice
                         }).OrderBy(a => a.ID).ToList();
            VMCBSSlave vMCBSSlave = new VMCBSSlave
            {
                DataList = vData
            };

            return vMCBSSlave;
        }
        public async Task<int> CBSSlaveAdd(VMCBSSlave vMCBSS)
        {
            var result = -1;

            vMCBSS.RequiredQty = decimal.Multiply(decimal.Divide(vMCBSS.TotalPieceQty, 12), vMCBSS.Consumption);

            Merchandising_CBSSlave cbss = new Merchandising_CBSSlave
            {
                Merchandising_CBSFK = vMCBSS.Merchandising_CBSFK,
                Common_RawItemFK = vMCBSS.Common_RawItemFK,
                //Common_CurrencyFK = vMCBSS.Common_CurrencyFK,
                Consumption = vMCBSS.Consumption,
                RequiredQty = vMCBSS.RequiredQty,
                UnitPrice = vMCBSS.UnitPrice,
                TotalPrice = decimal.Multiply(vMCBSS.RequiredQty, vMCBSS.UnitPrice),
                Active = true,
                Time = DateTime.Today,
                User = vMCBSS.User
            };
            _db.Merchandising_CBSSlave.Add(cbss);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = cbss.ID;
            }
            return result;
        }
        public async Task<int> CBSSlaveEdit(VMCBSSlave vMCBSS)
        {
            var result = -1;
            vMCBSS.RequiredQty = decimal.Multiply(decimal.Divide(vMCBSS.TotalPieceQty, 12), vMCBSS.Consumption);
            Merchandising_CBSSlave cBSS = _db.Merchandising_CBSSlave.Find(vMCBSS.ID);
            cBSS.Common_RawItemFK = vMCBSS.Common_RawItemFK;
            //cBSS.Common_CurrencyFK = vMCBSS.Common_CurrencyFK;
            cBSS.Consumption = vMCBSS.Consumption;
            cBSS.RequiredQty = vMCBSS.RequiredQty;
            cBSS.UnitPrice = vMCBSS.UnitPrice;
            cBSS.TotalPrice = decimal.Multiply(vMCBSS.RequiredQty, vMCBSS.UnitPrice);
            cBSS.Active = true;
            cBSS.Time = DateTime.Today;
            cBSS.User = vMCBSS.User;

            if (await _db.SaveChangesAsync() > 0)
            {
                result = vMCBSS.ID;
            }

            return result;
        }
        public async Task<int> CBSSlaveDelete(int Id)
        {
            var result = -1;
            Merchandising_CBSSlave selectSingle = _db.Merchandising_CBSSlave.Find(Id);
            if (selectSingle != null)
            {
                selectSingle.Active = false;
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = selectSingle.ID;
                }
            }
            return result;
        }

        public async Task<int> CBSItemFabricAdd(VMCBSSlave vMCBSS)
        {
            var result = -1;
            if (vMCBSS.ConRequiredQty > 0)
            {
                vMCBSS.RequiredQty = decimal.Multiply(decimal.Divide(vMCBSS.ConRequiredQty, 12), vMCBSS.Consumption);
            }
            else
            {
                vMCBSS.RequiredQty = decimal.Multiply(decimal.Divide(vMCBSS.TotalPieceQty, 12), vMCBSS.Consumption);
                vMCBSS.ConRequiredQty = vMCBSS.TotalPieceQty;
            }

            Merchandising_CBSSlave cbss = new Merchandising_CBSSlave
            {
                Merchandising_CBSFK = vMCBSS.Merchandising_CBSFK,
                Common_RawItemFK = vMCBSS.Common_RawItemFK,
                //Common_CurrencyFK = vMCBSS.Common_CurrencyFK,
                Common_ColorFK = vMCBSS.Common_ColorFK,
                GSM = vMCBSS.GSM,
                Consumption = vMCBSS.Consumption,
                ConRequiredQty = vMCBSS.ConRequiredQty,
                RequiredQty = vMCBSS.RequiredQty,
                UnitPrice = vMCBSS.UnitPrice,
                TotalPrice = decimal.Multiply(vMCBSS.RequiredQty, vMCBSS.UnitPrice),
                Active = true,
                Time = DateTime.Today,
                User = vMCBSS.User
            };
            _db.Merchandising_CBSSlave.Add(cbss);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = cbss.ID;
            }
            return result;
        }
        public async Task<int> CBSItemFabricEdit(VMCBSSlave vMCBSS)
        {
            var result = -1;
            if (vMCBSS.ConRequiredQty > 0)
            {
                vMCBSS.RequiredQty = decimal.Multiply(decimal.Divide(vMCBSS.ConRequiredQty, 12), vMCBSS.Consumption);
            }
            else
            {
                vMCBSS.RequiredQty = decimal.Multiply(decimal.Divide(vMCBSS.TotalPieceQty, 12), vMCBSS.Consumption);
                vMCBSS.ConRequiredQty = vMCBSS.TotalPieceQty;
            }
            Merchandising_CBSSlave cBSS = _db.Merchandising_CBSSlave.Find(vMCBSS.ID);
            cBSS.Common_RawItemFK = vMCBSS.Common_RawItemFK;
            cBSS.Common_ColorFK = vMCBSS.Common_ColorFK;
            //cBSS.Common_CurrencyFK = vMCBSS.Common_CurrencyFK;
            cBSS.GSM = vMCBSS.GSM;
            cBSS.Consumption = vMCBSS.Consumption;
            cBSS.ConRequiredQty = vMCBSS.ConRequiredQty;
            cBSS.RequiredQty = vMCBSS.RequiredQty;
            cBSS.UnitPrice = vMCBSS.UnitPrice;
            cBSS.TotalPrice = decimal.Multiply(vMCBSS.RequiredQty, vMCBSS.UnitPrice);
            cBSS.Active = true;
            cBSS.Time = DateTime.Today;
            cBSS.User = vMCBSS.User;

            if (await _db.SaveChangesAsync() > 0)
            {
                result = vMCBSS.ID;
            }

            return result;
        }

        public async Task<int> CBSItemChargeAdd(VMCBSSlave vMCBSS)
        {
            var result = -1;

            Merchandising_CBSSlave cbss = new Merchandising_CBSSlave
            {
                Merchandising_CBSFK = vMCBSS.Merchandising_CBSFK,
                Common_RawItemFK = vMCBSS.Common_RawItemFK,
                Consumption = 0,
                RequiredQty = 1,
                ConRequiredQty = 1,
                UnitPrice = vMCBSS.UnitPrice,
                TotalPrice = vMCBSS.UnitPrice,
                Active = true,
                Time = DateTime.Today,
                User = vMCBSS.User
            };
            _db.Merchandising_CBSSlave.Add(cbss);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = cbss.ID;
            }
            return result;
        }
        public async Task<int> CBSItemChargeEdit(VMCBSSlave vMCBSS)
        {
            var result = -1;
            Merchandising_CBSSlave cBSS = _db.Merchandising_CBSSlave.Find(vMCBSS.ID);
            cBSS.Common_RawItemFK = vMCBSS.Common_RawItemFK;
            cBSS.UnitPrice = vMCBSS.UnitPrice;
            cBSS.TotalPrice = vMCBSS.UnitPrice;
            cBSS.Active = true;
            cBSS.Time = DateTime.Today;
            cBSS.User = vMCBSS.User;

            if (await _db.SaveChangesAsync() > 0)
            {
                result = vMCBSS.ID;
            }

            return result;
        }

        public VMCBSSlave CBSItemGetByID(int ID)
        {
            var vData = (from t1 in _db.Merchandising_CBSSlave
                         join t2 in _db.Merchandising_CBS on t1.Merchandising_CBSFK equals t2.ID
                         join t3 in _db.Common_RawItem on t1.Common_RawItemFK equals t3.ID
                         join t4 in _db.Common_RawSubCategory on t3.Common_RawSubCategoryFK equals t4.ID
                         join t5 in _db.Common_RawCategory on t4.Common_RawCategoryFK equals t5.ID
                         //join t6 in _db.Common_Color on t1.Common_ColorFK equals t6.ID
                         //join t7 in _db.Common_Currency on t1.Common_CurrencyFK equals t7.ID
                         where t1.Active == true && t1.ID == ID
                         select new VMCBSSlave
                         {
                             ID = t1.ID,
                             Merchandising_CBSFK = t1.Merchandising_CBSFK,
                             Common_RawCategoryFK = t5.ID,
                             Common_RawSubCategoryFK = t4.ID,
                             Common_RawItemFK = t1.Common_RawItemFK.Value,
                             //Common_CurrencyFK = t7.ID,
                             RawCategory = t5.Name,
                             RawSubCategory = t4.Name,
                             RawItem = t3.Name,
                             Common_ColorFK = t1.Common_ColorFK == null ? 0 : (int)t1.Common_ColorFK,
                             GSM = t1.GSM,
                             //Color=t6.Name,
                             Currency = "$",
                             Consumption = t1.Consumption,
                             ConRequiredQty = t1.ConRequiredQty,
                             UnitPrice = (decimal)t1.UnitPrice,
                             RequiredQty = (decimal)t1.RequiredQty,
                             TotalPrice = decimal.Multiply(t1.UnitPrice, t1.RequiredQty),
                             ActionId = (int)ActionEnum.Edit
                         }).FirstOrDefault();
            var vColor = _db.Common_Color.Where(a => a.ID == vData.Common_ColorFK);
            if (vColor.Any())
            {
                vData.Color = vColor.FirstOrDefault().Name;
            }
            return vData;
        }

        public async Task<int> CBSReferenceAdd(VMStyleView model)
        {
            var result = -1;
            decimal modifyConsumption = decimal.Zero;
            List<Merchandising_CBSSlave> lstCBS = new List<Merchandising_CBSSlave>();
            if (model.TypeID == 1)
            {
                var vGetCBS = _db.Merchandising_CBSSlave.Where(a => a.Active == true && a.Merchandising_CBSFK == model.ImportID);
                if (vGetCBS.Any())
                {
                    foreach (var v in vGetCBS)
                    {
                        //modifyConsumption = decimal.Zero;
                        modifyConsumption = decimal.Zero;
                        if (v.Consumption > 0)
                        {
                            modifyConsumption = decimal.Multiply(decimal.Divide(v.Consumption, v.RequiredQty), model.vmCBS.OrderQty);
                        }

                        //v.RequiredQty = decimal.Multiply(decimal.Divide(model.vmCBS.OrderQty, 12), v.Consumption);
                        v.RequiredQty = decimal.Multiply(decimal.Divide(model.vmCBS.OrderQty, 12), modifyConsumption);
                        v.ConRequiredQty = model.vmCBS.OrderQty;
                        Merchandising_CBSSlave cbss = new Merchandising_CBSSlave();
                        cbss.Merchandising_CBSFK = model.vmCBS.ID;
                        cbss.Common_RawItemFK = v.Common_RawItemFK;
                        cbss.Common_CurrencyFK = v.Common_CurrencyFK;
                        cbss.Common_ColorFK = v.Common_ColorFK;
                        cbss.Consumption = modifyConsumption;
                        cbss.ConRequiredQty = v.ConRequiredQty;
                        cbss.RequiredQty = v.ConRequiredQty;
                        cbss.UnitPrice = v.UnitPrice;
                        cbss.TotalPrice = decimal.Multiply(v.RequiredQty, v.UnitPrice);
                        cbss.Active = true;
                        cbss.Time = DateTime.Today;
                        lstCBS.Add(cbss);
                    }
                }
            }
            else if (model.TypeID == 2)
            {
                var vGetBOM = _db.Merchandising_StyleSlave.Where(a => a.Active == true && a.Merchandising_StyleFK == model.ImportID);
                if (vGetBOM.Any())
                {
                    foreach (var v in vGetBOM)
                    {
                        modifyConsumption = decimal.Zero;
                        if (v.Consumption > 0)
                        {
                            modifyConsumption = decimal.Multiply(decimal.Divide(v.Consumption, v.RequiredQty), model.vmCBS.OrderQty);
                        }
                        //v.RequiredQty = decimal.Multiply(decimal.Divide(model.vmCBS.OrderQty, 12), v.Consumption);
                        v.RequiredQty = decimal.Multiply(decimal.Divide(model.vmCBS.OrderQty, 12), modifyConsumption);
                        v.ConRequiredQty = model.vmCBS.OrderQty;
                        Merchandising_CBSSlave cbss = new Merchandising_CBSSlave();
                        cbss.Merchandising_CBSFK = model.vmCBS.ID;
                        cbss.Common_RawItemFK = v.Common_RawItemFK;
                        cbss.Common_CurrencyFK = v.Common_CurrencyFK;
                        cbss.Consumption = modifyConsumption;
                        cbss.ConRequiredQty = v.ConRequiredQty;
                        cbss.RequiredQty = v.ConRequiredQty;
                        cbss.UnitPrice = v.UnitPrice;
                        cbss.TotalPrice = decimal.Multiply(cbss.RequiredQty, cbss.UnitPrice);
                        cbss.Active = true;
                        cbss.Time = DateTime.Today;
                        lstCBS.Add(cbss);
                    }
                }
            }

            _db.Merchandising_CBSSlave.AddRange(lstCBS);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = model.vmCBS.ID;
            }
            return result;
        }
        //public async Task<VMStyleView> CBSViewByID(int ID)
        //{
        //    var VM = new VMStyleView();
        //    VM.vmStyle = new VMStyle();
        //    VM.vmStyle = StyleById(ID);
        //    VM.vmShipmentSchedule = await ShipmentByStyleId(ID);
        //    VM.vmColorSizeRatio = ShipmentSizeRatioByStyleId(ID).DataList.ToList();
        //    return VM;
        //}

        #endregion

        #region DropDown
        public List<object> OrderDropDownList()
        {
            var OrderList = new List<object>();
            var vData = (from t1 in _db.Merchandising_BuyerOrder
                         join t2 in _db.Common_Buyer on t1.Common_BuyerFK equals t2.ID
                         where t1.Active == true
                         select new
                         {
                             ID = t1.ID,
                             Name = t2.BuyerID + "/" + t1.BuyerPO + "/" + t1.CID
                         }).ToList();
            foreach (var order in vData)
            {
                OrderList.Add(new { Text = order.Name, Value = order.ID });
            }
            return OrderList;
        }
        public List<object> StyleDropDownList()
        {
            var StyleList = new List<object>();
            var vData = (from t1 in _db.Merchandising_Style
                         join t2 in _db.Merchandising_BuyerOrder on t1.Merchandising_BuyerOrderFK equals t2.ID
                         where t1.Active == true
                         select new { ID = t1.ID, Name = t1.CID + "/" + t2.BuyerPO + "/" + t1.StyleName + "/" + t1.ReferenceNo }).OrderByDescending(x => x.ID).ToList();

            foreach (var style in vData)
            {
                StyleList.Add(new { Text = style.Name, Value = style.ID });
            }
            return StyleList;
        }

        public List<object> DestinationDropDownList()
        {
            var DestinationList = new List<object>();
            var vData = _db.Common_Country.Where(a => a.Active == true).ToList();
            foreach (var Country in vData)
            {
                DestinationList.Add(new { Text = Country.Name, Value = Country.ID });
            }
            return DestinationList;
        }
        public List<object> BuyerDropDownList()
        {
            var BuyerList = new List<object>();
            var vData = _db.Common_Buyer.Where(a => a.Active == true).ToList();
            foreach (var Buyer in vData)
            {
                BuyerList.Add(new { Text = Buyer.Name, Value = Buyer.ID });
            }
            return BuyerList;
        }
        public List<object> CurrencyDropDownList()
        {
            var CurrencyList = new List<object>();
            var vData = _db.Common_Currency.Where(a => a.Active == true).ToList();
            foreach (var Currency in vData)
            {
                CurrencyList.Add(new { Text = Currency.Name, Value = Currency.ID });
            }
            return CurrencyList;
        }
        public List<object> UnitDropDownList()
        {
            var UnitList = new List<object>();
            var vData = _db.Common_Unit.Where(a => a.Active == true).ToList();
            foreach (var Unit in vData)
            {
                UnitList.Add(new { Text = Unit.Name, Value = Unit.ID });
            }
            return UnitList;
        }

        public List<object> RawCategoryDropDownList()
        {
            var CategoryList = new List<object>();
            var vData = _db.Common_RawCategory.Where(a => a.Active == true).ToList();

            foreach (var cat in vData)
            {
                CategoryList.Add(new { Text = cat.Name, Value = cat.ID });
            }
            return CategoryList;
        }
        public List<object> RawSubCategoryDropDownList()
        {
            var SubCategoryList = new List<object>();
            var vData = (from t1 in _db.Common_RawSubCategory
                         join t2 in _db.Common_RawCategory on t1.Common_RawCategoryFK equals t2.ID
                         where t2.ID == 1020 // Raw Category "Yarn" ID
                         select new
                         {
                             Name = t1.Name,
                             ID = t1.ID
                         }).ToList();
            foreach (var sub in vData)
            {
                SubCategoryList.Add(new { Text = sub.Name, Value = sub.ID });
            }
            return SubCategoryList;
        }
        public List<object> RawItemDropDownList()
        {
            var ItemList = new List<object>();
            var vData = _db.Common_RawItem.Where(a => a.Active == true).ToList();
            foreach (var item in vData)
            {
                ItemList.Add(new { Text = item.Name, Value = item.ID });
            }
            return ItemList;
        }
        public List<object> GetStyleSlaveFabricItems(int id)
        {
            var ItemList = new List<object>();
            var vData = (from t1 in _db.Common_RawItem
                         join t2 in _db.Merchandising_StyleSlave on t1.ID equals t2.Common_RawItemFK
                         join t3 in _db.Common_RawSubCategory on t1.Common_RawSubCategoryFK equals t3.ID
                         where t2.Merchandising_StyleFK == id && t3.Common_RawCategoryFK == 2
                         && t2.Active == true
                         select new
                         {
                             ItemName = t1.Name,
                             StyleSlaveID = t2.ID
                         }).Distinct().ToList();

            //var vData = _db.Common_RawItem.Where(a => a.Active == true).ToList();
            foreach (var item in vData)
            {
                ItemList.Add(new { Text = item.ItemName, Value = item.StyleSlaveID });
            }
            return ItemList;
        }
        public List<object> RawSubCatByCategoryIDDropDownList(int Id)
        {
            var SubCategoryList = new List<object>();
            var vData = _db.Common_RawSubCategory.Where(a => a.Active == true && a.Common_RawCategoryFK == Id).ToList();
            foreach (var sub in vData)
            {
                SubCategoryList.Add(new { Text = sub.Name, Value = sub.ID });
            }
            return SubCategoryList;
        }

        public List<object> DDLRawCategoryItemByCatID(int Id)
        {
            var SubCategoryList = new List<object>();
            var vData = (from t1 in _db.Common_RawSubCategory
                         join t2 in _db.Common_RawItem on t1.ID equals t2.Common_RawSubCategoryFK
                         where t1.Active == true && t1.Common_RawCategoryFK == Id && t2.Active == true
                         select new
                         {
                             Name = t1.Name + "-" + t2.Name,
                             ID = t2.ID
                         }).ToList();
            foreach (var sub in vData)
            {
                SubCategoryList.Add(new { Text = sub.Name, Value = sub.ID });
            }
            return SubCategoryList;
        }
        public List<object> RawItemBySubCategoryIDDropDownList(int Id)
        {
            var ItemList = new List<object>();
            var vData = _db.Common_RawItem.Where(a => a.Active == true && a.Common_RawSubCategoryFK == Id).ToList();
            foreach (var item in vData)
            {
                ItemList.Add(new { Text = item.Name, Value = item.ID });
            }
            return ItemList;
        }
        public List<object> MachineDiaDownList()
        {
            var ItemList = new List<object>();

            ItemList.Add(new { Text = "40” * 24", Value = "40” * 24" });
            ItemList.Add(new { Text = "38” * 24", Value = "38” * 24" });
            ItemList.Add(new { Text = "36” * 24", Value = "36” * 24" });
            ItemList.Add(new { Text = "34” * 24", Value = "34” * 24" });
            ItemList.Add(new { Text = "32” * 24", Value = "32” * 24" });
            ItemList.Add(new { Text = "30” * 24", Value = "30” * 24" });
            ItemList.Add(new { Text = "28” * 24", Value = "28” * 24" });

            return ItemList;
        }
        public List<object> RawItemByCategoryIDDropDownList(int Id)
        {
            var ItemList = new List<object>();
            var vData = (from t1 in _db.Common_RawSubCategory
                         join t2 in _db.Common_RawItem on t1.ID equals t2.Common_RawSubCategoryFK
                         where t1.Active == true && t2.Active == true && t1.Common_RawCategoryFK == Id
                         select new { Name = t2.Name, ID = t2.ID }).ToList();
            foreach (var item in vData)
            {
                ItemList.Add(new { Text = item.Name, Value = item.ID });
            }
            return ItemList;
        }
        public List<object> CBSRawItemBySubCategoryIDDropDownList(int Id, int CBSID)
        {
            var ItemList = new List<object>();
            var vData = (from a in _db.Merchandising_CBSSlave
                         join b in _db.Common_RawItem on a.Common_RawItemFK equals b.ID
                         where a.Merchandising_CBSFK == CBSID && b.Common_RawSubCategoryFK == Id && a.Active == true
                         select new
                         {
                             Name = b.Name,
                             ID = b.ID
                         }).ToList();

            var vAllData = _db.Common_RawItem.Where(a => a.Active == true && a.Common_RawSubCategoryFK == Id).Select(x => new { Name = x.Name, ID = x.ID }).ToList();
            var vGetData = vAllData.Except(vData).ToList();
            foreach (var item in vGetData)
            {
                ItemList.Add(new { Text = item.Name, Value = item.ID });
            }
            return ItemList;
        }
        public List<object> BOMRawItemBySubCategoryIDDropDownList(int Id, int StyleID)
        {
            var ItemList = new List<object>();
            var vData = (from a in _db.Merchandising_StyleSlave
                         join b in _db.Common_RawItem on a.Common_RawItemFK equals b.ID
                         where a.Merchandising_StyleFK == StyleID && b.Common_RawSubCategoryFK == Id && a.Active == true
                         select new
                         {
                             Name = b.Name,
                             ID = b.ID
                         }).ToList();

            var vAllData = _db.Common_RawItem.Where(a => a.Active == true && a.Common_RawSubCategoryFK == Id).Select(x => new { Name = x.Name, ID = x.ID }).ToList();
            var vGetData = vAllData.Except(vData).ToList();
            foreach (var item in vGetData)
            {
                ItemList.Add(new { Text = item.Name, Value = item.ID });
            }
            return ItemList;
        }

        public List<object> OrderCategoryDropDownList()
        {
            var OrderCategoryList = new List<object>();
            var vData = _db.Common_FinishCategory.Where(a => a.Active == true).ToList();

            foreach (var Currency in vData)
            {
                OrderCategoryList.Add(new { Text = Currency.Name, Value = Currency.ID });
            }
            return OrderCategoryList;
        }
        public List<object> OrderSubCategoryDropDownList()
        {
            var OrderSubCategoryList = new List<object>();
            var vData = _db.Common_FinishSubCategory.Where(a => a.Active == true).ToList();
            foreach (var Currency in vData)
            {
                OrderSubCategoryList.Add(new { Text = Currency.Name, Value = Currency.ID });
            }
            return OrderSubCategoryList;
        }
        public List<object> OrderItemDropDownList()
        {
            var ItemList = new List<object>();
            var vData = _db.Common_FinishItem.Where(a => a.Active == true).ToList();
            foreach (var Currency in vData)
            {
                ItemList.Add(new { Text = Currency.Name, Value = Currency.ID });
            }
            return ItemList;
        }
        public List<object> OrderSubCategoryByItemIDDropDownList(int ID)
        {
            var ItemList = new List<object>();
            var GetSubCatID = _db.Common_FinishItem.FirstOrDefault(a => a.Active == true && a.ID == ID).Common_FinishSubCategoryFK;
            var GetCatID = _db.Common_FinishSubCategory.FirstOrDefault(a => a.Active == true && a.ID == GetSubCatID).Common_FinishCategoryFK;
            var vData = _db.Common_FinishSubCategory.Where(a => a.Active == true && a.Common_FinishCategoryFK == GetCatID).ToList();
            foreach (var subcat in vData)
            {
                ItemList.Add(new { Text = subcat.Name, Value = subcat.ID });
            }
            return ItemList;
        }

        public List<object> OrderItemByItemIDDropDownList(int ID)
        {
            var ItemList = new List<object>();
            var getId = _db.Common_FinishItem.FirstOrDefault(a => a.Active == true && a.ID == ID).Common_FinishSubCategoryFK;
            var vData = _db.Common_FinishItem.Where(a => a.Active == true && a.Common_FinishSubCategoryFK == getId).ToList();
            foreach (var subcat in vData)
            {
                ItemList.Add(new { Text = subcat.Name, Value = subcat.ID });
            }
            return ItemList;
        }
        public List<object> GetPortNoByCountryIdDropDownList(int Id)
        {
            var PortList = new List<object>();

            var vData = _db.Common_CountryPort.Where(t1 => t1.Active == true && t1.Common_CountryFK == Id).ToList();

            foreach (var port in vData)
            {
                PortList.Add(new { Text = port.Name, Value = port.ID });
            }
            return PortList;
        }

        public List<object> DDLPortList()
        {
            var PortList = new List<object>();

            var vData = _db.Common_CountryPort.Where(t1 => t1.Active == true).ToList();

            foreach (var port in vData)
            {
                PortList.Add(new { Text = port.Name, Value = port.ID });
            }
            return PortList;
        }

        public List<object> SizeDropDownList()
        {
            var sizeList = new List<object>();
            var vData = _db.Common_Size.Where(a => a.Active == true).ToList();
            foreach (var size in vData)
            {
                sizeList.Add(new { Text = size.Name, Value = size.ID });
            }
            return sizeList;
        }
        public List<object> ColorDropDownList()
        {
            var colorList = new List<object>();
            var vData = _db.Common_Color.Where(a => a.Active == true).ToList();
            foreach (var color in vData)
            {
                colorList.Add(new { Text = color.Name, Value = color.ID });
            }
            return colorList;
        }
        public List<object> ColorForShipmentDropDownList(int ID)
        {
            var colorList = new List<object>();
            var vData = (from t1 in _db.Merchandising_StyleMeasurement
                         join t2 in _db.Common_Color on t1.Common_ColorFK equals t2.ID
                         where t1.Active == true && t1.Merchandising_StyleFK == ID
                         group t2 by new { t1.Common_ColorFK, t2.Name } into a
                         select new
                         {
                             Name = a.Key.Name,
                             ID = a.Key.Common_ColorFK
                         }).ToList();
            var vData1 = (from t1 in _db.Merchandising_StyleShipmentRatio
                          join t2 in _db.Merchandising_StyleMeasurement on t1.Merchandising_StyleMeasurementFK equals t2.ID
                          join t3 in _db.Common_Color on t2.Common_ColorFK equals t3.ID
                          join t4 in _db.Merchandising_StyleShipmentSchedule on t1.Merchandising_StyleShipmentScheduleFK equals t4.ID
                          where t1.Active == true && t4.Active == true && t4.Merchandising_StyleFK == ID
                          group t2 by new { t2.Common_ColorFK, t3.Name } into a
                          select new
                          {
                              Name = a.Key.Name,
                              ID = a.Key.Common_ColorFK
                          }).ToList();

            var aData = vData.Except(vData1);
            //var vData = _db.Common_Color.Where(a => a.Active == true).ToList();
            foreach (var color in aData)
            {
                colorList.Add(new { Text = color.Name, Value = color.ID });
            }
            return colorList;
        }

        public List<object> ColorByStyleIDDropDownList(int ID)
        {
            var colorList = new List<object>();
            var vData = (from t1 in _db.Merchandising_StyleMeasurement
                         join t2 in _db.Common_Color on t1.Common_ColorFK equals t2.ID
                         where t1.Active == true && t1.Merchandising_StyleFK == ID
                         group t2 by new { t1.Common_ColorFK, t2.Name } into a
                         select new
                         {
                             Name = a.Key.Name,
                             ID = a.Key.Common_ColorFK
                         }).ToList();
            foreach (var color in vData)
            {
                colorList.Add(new { Text = color.Name, Value = color.ID });
            }
            return colorList;
        }

        public List<object> DDLSizeByStyleID(int ID)
        {
            var sizeList = new List<object>();
            var vData = (from t1 in _db.Merchandising_StyleMeasurement
                         join t2 in _db.Common_Size on t1.Common_SizeFK equals t2.ID
                         where t1.Active == true && t1.Merchandising_StyleFK == ID
                         group t2 by new { t1.Common_SizeFK, t2.Name } into a
                         select new
                         {
                             Name = a.Key.Name,
                             ID = a.Key.Common_SizeFK
                         }).ToList();
            foreach (var size in vData)
            {
                sizeList.Add(new { Text = size.Name, Value = size.ID });
            }
            return sizeList;
        }

        public List<object> BOMImportDropDownList()
        {
            var BOMList = new List<object>();
            List<string> lstType = new List<string>();
            lstType.Add("From CBS");
            lstType.Add("From BOM");

            int i = 1;
            foreach (var v in lstType)
            {
                BOMList.Add(new { Text = v, Value = i });
                i++;
            }
            return BOMList;
        }

        public List<object> CBSDropDownList()
        {
            var cbsList = new List<object>();
            var vData = (from o in _db.Merchandising_CBS
                         join p in _db.Merchandising_CBSStyle on o.Merchandising_CBSStyleFK equals p.ID
                         join q in _db.Common_FinishItem on p.Common_FinishItemFK equals q.ID
                         where o.Active == true
                         select new
                         {
                             Id = o.ID,
                             Name = o.CID + "-" + q.Name
                         }).OrderBy(a => a.Id).ToList();
            foreach (var v in vData)
            {
                cbsList.Add(new { Text = v.Name, Value = v.Id });
            }
            return cbsList;
        }

        public List<object> CBSStyleDownList()
        {
            var cbsList = new List<object>();
            var vData = (from o in _db.Merchandising_CBSStyle
                         join p in _db.Common_FinishItem on o.Common_FinishItemFK equals p.ID
                         where o.Active == true
                         select new
                         {
                             Id = o.ID,
                             Name = o.CID + "-" + p.Name
                         }).OrderBy(a => a.Id).ToList();
            foreach (var v in vData)
            {
                cbsList.Add(new { Text = v.Name, Value = v.Id });
            }
            return cbsList;
        }
        /// <summary>
        /// updated by fahim on 11072019 to exclude pr completed styles
        /// </summary>
        /// <returns></returns>
        public List<object> DDLBOMStyleList()
        {
            var StyleList = new List<object>();
            if (_vmLogin.UserAccessLevelId == (int)UserAccessLevel.Basic)
            {
                StyleList = (from MS in _db.Merchandising_Style.Where(x => _db.Merchandising_StyleSlave.Any(y => y.Merchandising_StyleFK == x.ID && y.IsLocked && y.Active == true) || _db.Merchandising_BOF.Any(y => y.Merchandising_StyleFK == x.ID && y.Active))
                             join MBO in _db.Merchandising_BuyerOrder on MS.Merchandising_BuyerOrderFK equals MBO.ID
                             where MS.Active == true && MBO.Active == true && MS.UserID==_vmLogin.ID
                             select new
                             {
                                 Value = MS.ID,
                                 Text = MS.CID + MS.StyleName,
                                 TotalQty = _db.Merchandising_BOF.Where(x => x.Merchandising_StyleFK == MS.ID && x.Active).Select(x => (double)x.RequiredQuantity).DefaultIfEmpty(0).Sum() + _db.Merchandising_StyleSlave.Where(x => x.Active == true && x.IsLocked && x.Merchandising_StyleFK == MS.ID).Select(x => (double)x.RequiredQty).DefaultIfEmpty(0).Sum()
                             }).ToList<object>();
            }
            else
            {
                StyleList = (from MS in _db.Merchandising_Style.Where(x => _db.Merchandising_StyleSlave.Any(y => y.Merchandising_StyleFK == x.ID && y.IsLocked && y.Active == true) || _db.Merchandising_BOF.Any(y => y.Merchandising_StyleFK == x.ID && y.Active))
                             join MBO in _db.Merchandising_BuyerOrder on MS.Merchandising_BuyerOrderFK equals MBO.ID
                             where MS.Active == true && MBO.Active == true
                             select new
                             {
                                 Value = MS.ID,
                                 Text = MS.CID + MS.StyleName,
                                 TotalQty = _db.Merchandising_BOF.Where(x => x.Merchandising_StyleFK == MS.ID && x.Active).Select(x => (double)x.RequiredQuantity).DefaultIfEmpty(0).Sum() + _db.Merchandising_StyleSlave.Where(x => x.Active == true && x.IsLocked && x.Merchandising_StyleFK == MS.ID).Select(x => (double)x.RequiredQty).DefaultIfEmpty(0).Sum()
                             }).ToList<object>();
            }
            return StyleList;
        }
        //public List<object> DDLBOMStyleList()
        //{
        //    var StyleList = new List<object>();


        //    var vData = (from t1 in _db.Merchandising_StyleSlave
        //                 join t2 in _db.Merchandising_Style on t1.Merchandising_StyleFK equals t2.ID
        //                 join t4 in _db.Merchandising_BuyerOrder on t2.Merchandising_BuyerOrderFK equals t4.ID
        //                 join t5 in _db.Common_Buyer on t4.Common_BuyerFK equals t5.ID
        //                 where t1.Active == true && t2.Active == true && t4.Active==true && t1.IsLocked==true
        //                 group t1.RequiredQty by new { t4.BuyerPO, t2.StyleName, t2.ID,t5.BuyerID,t2.CID } into all
        //                 select new
        //                 {
        //                     ID = all.Key.ID,
        //                     Name = all.Key.BuyerID + "/"+all.Key.BuyerPO + "/" + all.Key.CID+ "/"+ all.Key.StyleName,
        //                     TotalQty=all.Sum() + (from x in _db.Merchandising_BOF where x.Merchandising_StyleFK == all.Key.ID select x.RequiredQuantity).DefaultIfEmpty(0).Sum()



        //                 }).ToList();

        //    foreach (var style in vData)
        //    {
        //        StyleList.Add(new { Text = style.Name, Value = style.ID, TotalQty=style.TotalQty });
        //    }
        //    return StyleList;
        //}

        public List<object> DDLSubSetListByStyleID(int StyleID)
        {
            var StyleList = new List<object>();
            var vData = _db.Merchandising_StyleSetPack.Where(a => a.Merchandising_StyleFK == StyleID && a.Active == true).Select(a => new
            {
                ID = a.ID,
                Name = a.SetPackName
            }).ToList();
            if (vData.Any())
            {
                foreach (var style in vData)
                {
                    StyleList.Add(new { Text = style.Name, Value = style.ID });
                }
            }
            return StyleList;
        }

        public List<object> DDLAgentList()
        {
            var agentList = new List<object>();
            foreach (var agent in _db.Common_Agent.Where(a => a.Active == true).ToList())
            {
                agentList.Add(new { Text = agent.Name, Value = agent.ID });
            }

            return agentList;
        }
        public List<object> DDLAgentInspection()
        {
            var agentInspection = new List<object>();
            foreach (var agIn in _db.Common_InspectionAgent.Where(a => a.Active == true).ToList())
            {
                agentInspection.Add(new { Text = agIn.Name, Value = agIn.ID });
            }

            return agentInspection;
        }

        public List<object> FinishSubCatByCategoryIDDropDownList(int Id)
        {
            var ItemList = new List<object>();
            var vData = _db.Common_FinishSubCategory.Where(t1 => t1.Active == true && t1.Common_FinishCategoryFK == Id).ToList();
            foreach (var item in vData)
            {
                ItemList.Add(new { Text = item.Name, Value = item.ID });
            }
            return ItemList;
        }
        public List<object> FinishItemBySubCategoryIDDropDownList(int Id)
        {
            var ItemList = new List<object>();
            var vData = _db.Common_FinishItem.Where(t1 => t1.Active == true && t1.Common_FinishSubCategoryFK == Id).ToList();
            foreach (var item in vData)
            {
                ItemList.Add(new { Text = item.Name, Value = item.ID });
            }
            return ItemList;
        }

        public List<object> DDLBOMFabricByStyleID(int StyleId)
        {
            var ItemList = new List<object>();
            var vData = (from t1 in _db.Merchandising_StyleSlave
                         join t2 in _db.Common_RawItem on t1.Common_RawItemFK equals t2.ID
                         join t3 in _db.Common_RawSubCategory on t2.Common_RawSubCategoryFK equals t3.ID
                         join t4 in _db.Common_Color on t1.Common_ColorFK equals t4.ID
                         where t1.IsFabric == true && t1.Active == true && t1.Merchandising_StyleFK == StyleId
                         select new
                         {
                             ID = t1.ID,
                             Name = t3.Name + "-" + t2.Name + "-" + t4.Name + "-" + t1.GSM
                         }).ToList();
            foreach (var item in vData)
            {
                ItemList.Add(new { Text = item.Name, Value = item.ID });
            }
            return ItemList;
        }
        public List<object> DDLShipmentReportTypeList()
        {
            var ReportType = new List<object>();
            ReportType.Add(new { Text = "Details Report", Value = "1" });
            ReportType.Add(new { Text = "Summary Report", Value = "2" });
            return ReportType;
        }
        #endregion

        #region Bill Of Fabrics

        public async Task<int> YarnCalculationAdd(VMYarnCalculation vmYarnCalculation)
        {
            var result = -1;
            #region Genarate reference No
            int totalyarnCalculation = (from t1 in _db.Merchandising_YarnCalculation
                                        select t1).Count();
            if (totalyarnCalculation == 0)
            {
                totalyarnCalculation = 1;
            }
            else
            {
                totalyarnCalculation++;
            }
            var newString = totalyarnCalculation.ToString().PadLeft(5, '0');

            string referenceNo = "Y" + newString + "/" + DateTime.Now.ToString("yy") + "/" + DateTime.Today.Month;
            #endregion
            Merchandising_YarnCalculation yarnCalculation = new Merchandising_YarnCalculation()
            {
                Combo = vmYarnCalculation.Combo,
                Common_CurrencyFK = vmYarnCalculation.Common_CurrencyFK,
                Merchandising_StyleSlaveFK_Fabric = vmYarnCalculation.Merchandising_StyleSlaveFK_Fabric,
                Common_UnitFK = vmYarnCalculation.Common_UnitFK,
                Consumption = vmYarnCalculation.Consumption,
                Fabrication = vmYarnCalculation.Fabrication,
                FinishDIA = vmYarnCalculation.FinishDIA,
                // FirstCreatedBy = vmYarnCalculation.FirstCreatedBy,
                Common_ColorFK = vmYarnCalculation.Common_ColorFK,
                GSM = vmYarnCalculation.GSM,
                //LastEditeddBy = vmYarnCalculation.LastEditeddBy,
                Lycra = vmYarnCalculation.Lycra,
                Merchandising_StyleFk = vmYarnCalculation.VMStyle.ID,
                Merchandising_YarnTypeFk = vmYarnCalculation.Merchandising_YarnTypeFk,
                Price = vmYarnCalculation.Price,
                ProcessLoss = vmYarnCalculation.ProcessLoss,
                OrderQuantityPCS = vmYarnCalculation.OrderQuantityPCS,
                Raw_ItemFK = vmYarnCalculation.Raw_ItemFK,
                ReferenceNo = referenceNo,
                Remarks = vmYarnCalculation.ReferenceNo,
                Time = DateTime.Now,
                UserID = vmYarnCalculation.UserID
            };

            _db.Merchandising_YarnCalculation.Add(yarnCalculation);

            if (await _db.SaveChangesAsync() > 0)
            {
                result = yarnCalculation.ID;

                VMBOM vmBOM = new VMBOM
                {
                    ID = yarnCalculation.Merchandising_StyleSlaveFK_Fabric,
                    Consumption = yarnCalculation.Consumption,
                    ConRequiredQty = yarnCalculation.OrderQuantityPCS                    

                };
                int bomSlave = await BOMFabricEdit(vmBOM);
            }
            return result;
        }
        public void EditExistingBOF(VMYarnCalculation vmYarnCalculation)
        {
            var modifyedYarn = (from t1 in _db.Merchandising_YarnCalculation
                                join t2 in _db.Common_RawItem on t1.Raw_ItemFK equals t2.ID
                                join t3 in _db.Common_Color on t1.Common_ColorFK equals t3.ID
                                where t1.Active == true && t1.IsApprove == true
                                && t1.ID == vmYarnCalculation.ID

                                select new VMYarnCalculation
                                {
                                    ItemName = t2.Name,
                                    Color = t3.Name,
                                    Common_ColorFK = t3.ID,
                                    Combo = t1.Combo,
                                    Common_CurrencyFK = t1.Common_CurrencyFK,
                                    Common_UnitFK = t1.Common_UnitFK,
                                    Fabrication = t1.Fabrication,
                                    FinishDIA = t1.FinishDIA,
                                    GSM = t1.GSM,
                                    ID = t1.ID,
                                    Merchandising_StyleFk = t1.Merchandising_StyleFk,
                                    Merchandising_YarnTypeFk = t1.Merchandising_YarnTypeFk,
                                    Raw_ItemFK = t1.Raw_ItemFK,
                                    ReferenceNo = t1.ReferenceNo,
                                    User = t1.User,
                                    IsActive = t1.Active,
                                    IsApproved = t1.IsApprove,
                                    ProcessLoss = t1.ProcessLoss,
                                    Lycra = t1.Lycra,
                                    Consumption = t1.Consumption,
                                    OrderQuantityPCS = t1.OrderQuantityPCS,
                                    Price = t1.Price

                                }).FirstOrDefault();


            var existingBOF = _db.Merchandising_BOF.Where(x => x.Merchandising_YarnCalculationFK == vmYarnCalculation.ID).ToList();
            List<Merchandising_BOF> BOFList = new List<Merchandising_BOF>();
            if (existingBOF.Count() > 0)
            {
                foreach (var _BOF in existingBOF)
                {

                    _BOF.Consumption = modifyedYarn.Consumption;
                    _BOF.Description = modifyedYarn.Fabrication + "-GSM:" + modifyedYarn.GSM + "-Dia:" + modifyedYarn.FinishDIA + "-Color:" + modifyedYarn.Color;
                    var category = GetCategoryByItem(modifyedYarn.Raw_ItemFK);
                    if (category == 1020)
                    {
                        //Yarn
                        _BOF.RequiredQuantity = decimal.Subtract(decimal.Divide(((decimal.Multiply((decimal.Divide(modifyedYarn.OrderQuantityPCS, 12)), modifyedYarn.Consumption))), (decimal.Subtract(decimal.Divide(100, 100), decimal.Multiply((decimal.Divide(modifyedYarn.ProcessLoss, 100)), 1)))), decimal.Multiply(decimal.Divide(decimal.Divide(((decimal.Multiply((decimal.Divide(modifyedYarn.OrderQuantityPCS, 12)), modifyedYarn.Consumption))), (decimal.Subtract(decimal.Divide(100, 100), decimal.Multiply((decimal.Divide(modifyedYarn.ProcessLoss, 100)), 1)))), 100), modifyedYarn.Lycra));
                        _BOF.Price = modifyedYarn.Price;
                    }
                    else if (category == 1019)
                    {
                        //process
                        _BOF.RequiredQuantity = decimal.Divide(((decimal.Multiply((decimal.Divide(modifyedYarn.OrderQuantityPCS, 12)), modifyedYarn.Consumption))), (decimal.Subtract(decimal.Divide(100, 100), decimal.Multiply((decimal.Divide(modifyedYarn.ProcessLoss, 100)), 1))));

                    }

                    BOFList.Add(_BOF);

                }
            }
            BOFList.ForEach(a => a.Time = DateTime.Now);
            _db.SaveChangesAsync();
        }


        public void DeleteExistingBOF(VMYarnCalculation vmYarnCalculation)
        {


            var existingBOF = _db.Merchandising_BOF.Where(x => x.Merchandising_YarnCalculationFK == vmYarnCalculation.ID).ToList();
            List<Merchandising_BOF> BOFList = new List<Merchandising_BOF>();
            if (existingBOF.Count() > 0)
            {
                foreach (var _BOF in existingBOF)
                {

                    _BOF.Active = false;


                    BOFList.Add(_BOF);

                }
            }
            BOFList.ForEach(a => a.Time = DateTime.Now);
            _db.SaveChangesAsync();
        }

        private int GetCategoryByItem(int raw_ItemFK)
        {
            return (from t1 in _db.Common_RawItem
                    join t2 in _db.Common_RawSubCategory on t1.Common_RawSubCategoryFK equals t2.ID
                    where t1.Active == true && t1.ID == raw_ItemFK
                    select t1.Common_RawSubCategoryFK).FirstOrDefault();
        }

        public VMYarnCalculation GetSingleYarnCalculation(int iD)
        {
            var v = (from t1 in _db.Merchandising_YarnCalculation
                     join t2 in _db.Common_RawItem on t1.Raw_ItemFK equals t2.ID
                     join t3 in _db.Common_Color on t1.Common_ColorFK equals t3.ID

                     where t1.ID == iD
                     select new VMYarnCalculation
                     {
                         Color = t3.Name,
                         Common_ColorFK = t3.ID,
                         Combo = t1.Combo,
                         Common_CurrencyFK = t1.Common_CurrencyFK,
                         Common_UnitFK = t1.Common_UnitFK,
                         Consumption = t1.Consumption,
                         Fabrication = t1.Fabrication,
                         FinishDIA = t1.FinishDIA,
                         GSM = t1.GSM,
                         ID = t1.ID,
                         IsActive = t1.Active,
                         IsApproved = t1.IsApprove,
                         Lycra = t1.Lycra,
                         Merchandising_StyleFk = t1.Merchandising_StyleFk,
                         Merchandising_YarnTypeFk = t1.Merchandising_YarnTypeFk,
                         OrderQuantityPCS = t1.OrderQuantityPCS,
                         OrderQuantityDZ = t1.OrderQuantityPCS / 12,
                         Price = t1.Price,
                         ProcessLoss = t1.ProcessLoss,
                         Raw_ItemFK = t1.Raw_ItemFK,
                         Raw_SubCategoryFK = t2.Common_RawSubCategoryFK,
                         ReferenceNo = t1.ReferenceNo,
                         UserID = t1.UserID
                     }).FirstOrDefault();



            return v; /// _db.Merchandising_YarnCalculation.Where(x => x.ID == iD).FirstOrDefault();
        }
        public async Task<int> YarnCalculationEdit(VMYarnCalculation vmYarnCalculation)
        {
            var result = -1;
            Merchandising_YarnCalculation yarnCalculation = _db.Merchandising_YarnCalculation.Find(vmYarnCalculation.ID);
            yarnCalculation.Common_ColorFK = vmYarnCalculation.Common_ColorFK;
            yarnCalculation.Combo = vmYarnCalculation.Combo;
            yarnCalculation.Common_CurrencyFK = vmYarnCalculation.Common_CurrencyFK;
            yarnCalculation.Merchandising_StyleSlaveFK_Fabric = vmYarnCalculation.Merchandising_StyleSlaveFK_Fabric;
            yarnCalculation.Common_UnitFK = vmYarnCalculation.Common_UnitFK;
            yarnCalculation.Consumption = vmYarnCalculation.Consumption;
            yarnCalculation.Fabrication = vmYarnCalculation.Fabrication;
            yarnCalculation.FinishDIA = vmYarnCalculation.FinishDIA;
            //yarnCalculation.FirstCreatedBy = vmYarnCalculation.FirstCreatedBy;
            yarnCalculation.GSM = vmYarnCalculation.GSM;
            // yarnCalculation.LastEditeddBy = vmYarnCalculation.LastEditeddBy;
            yarnCalculation.Lycra = vmYarnCalculation.Lycra;
            yarnCalculation.Merchandising_StyleFk = vmYarnCalculation.Merchandising_StyleFk;
            yarnCalculation.Merchandising_YarnTypeFk = vmYarnCalculation.Merchandising_YarnTypeFk;
            yarnCalculation.Price = vmYarnCalculation.Price;
            yarnCalculation.ProcessLoss = vmYarnCalculation.ProcessLoss;
            yarnCalculation.OrderQuantityPCS = vmYarnCalculation.OrderQuantityPCS;
            yarnCalculation.Raw_ItemFK = vmYarnCalculation.Raw_ItemFK;
            yarnCalculation.ReferenceNo = vmYarnCalculation.ReferenceNo;
            yarnCalculation.Remarks = vmYarnCalculation.ReferenceNo;
            yarnCalculation.Time = DateTime.Now;
            yarnCalculation.User = vmYarnCalculation.User;


            if (await _db.SaveChangesAsync() > 0)
            {
                result = yarnCalculation.ID;

                // This code update fabric info of BOM
                VMBOM vmBOM = new VMBOM
                {
                    ID = yarnCalculation.Merchandising_StyleSlaveFK_Fabric,
                    Consumption = yarnCalculation.Consumption,
                    ConRequiredQty = yarnCalculation.OrderQuantityPCS

                };
                int bomSlave = await BOMFabricEdit(vmBOM);
            }
            return result;
        }

        public async Task<int> YarnCalculationDelete(int Id)
        {
            var result = -1;

            Merchandising_YarnCalculation yarnCalculation = _db.Merchandising_YarnCalculation.Find(Id);

            if (yarnCalculation != null)
            {
                yarnCalculation.Active = false;
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = yarnCalculation.ID;

                    _db.Merchandising_BOF.Where(x => x.Merchandising_YarnCalculationFK == yarnCalculation.ID).ToList().ForEach(x => x.Active = false);
                    await _db.SaveChangesAsync();

                }
            }


            return result;
        }
        public async Task<int> YarnCalculationApprove(int Id)
        {
            var result = -1;

            Merchandising_YarnCalculation yarnCalculation = _db.Merchandising_YarnCalculation.Find(Id);

            if (yarnCalculation != null)
            {
                yarnCalculation.IsApprove = true;
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = yarnCalculation.ID;
                }
            }


            return result;
        }

        public async Task<int> YarnCalculationUnApprove(int Id)
        {
            var result = -1;

            Merchandising_YarnCalculation yarnCalculation = _db.Merchandising_YarnCalculation.Find(Id);

            if (yarnCalculation != null)
            {
                yarnCalculation.IsApprove = false;
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = yarnCalculation.ID;
                }
            }


            return result;
        }
        public async Task<int> MerchandisingBOFMultiAdd(List<VMBOFView> vMBOF)
        {
            var result = -1;
            List<Merchandising_BOF> merchandisingBOFList = new List<Merchandising_BOF>();
            if (vMBOF.Any())
            {
                foreach (var item in vMBOF)
                {
                    Merchandising_BOF merchandisingBOF = new Merchandising_BOF()
                    {
                        Common_RawItemFK = item.Common_RawItemFK,
                        Description = item.Description,
                        Common_UnitFK = item.Common_UnitFK,
                        UserID = item.UserID,
                        IsFabric = item.IsFabric,

                        Common_CurrencyFK = item.Common_CurrencyFK,
                        //Common_SupplierFK = vmYarnCalculation.Common_SupplierFK,
                        Merchandising_StyleFK = item.Merchandising_StyleFK,
                        Merchandising_YarnCalculationFK = item.Merchandising_YarnCalculationFK,
                        Consumption = item.Consumption,
                        RequiredQuantity = item.RequiredQuantity,
                        Price = item.Price,
                        Time = DateTime.Now,
                        User = item.User
                    };
                    merchandisingBOFList.Add(merchandisingBOF);
                }
            }

            _db.Merchandising_BOF.AddRange(merchandisingBOFList);

            if (await _db.SaveChangesAsync() > 0)
            {
                result = 1;
            }
            return result;
        }


        public async Task<int> LoadExistingFabricsSpec(VMYarnCalculation vmYarnCalculation)
        {
            var result = -1;
            var existingYarn = _db.Merchandising_YarnCalculation.Where(x => x.Merchandising_StyleFk == vmYarnCalculation.Merchandising_StyleFk && x.Active).ToList();


            var currentStyle = _db.Merchandising_Style.Where(x => x.ID == vmYarnCalculation.VMStyle.ID).FirstOrDefault();
            List<Merchandising_YarnCalculation> merchandisingYarnCalculationList = new List<Merchandising_YarnCalculation>();
            if (existingYarn.Any())
            {
                foreach (var item in existingYarn)
                {
                    Merchandising_YarnCalculation merchandisingYarnCalculation = new Merchandising_YarnCalculation()
                    {
                        IsApprove = false,
                        Merchandising_StyleFk = currentStyle.ID,
                        Combo = item.Combo,
                        Common_ColorFK = item.Common_ColorFK,
                        Common_UnitFK = item.Common_UnitFK,
                        Fabrication = item.Fabrication,
                        FinishDIA = item.FinishDIA,
                        GSM = item.GSM,
                        Merchandising_StyleSlaveFK_Fabric = item.Merchandising_StyleSlaveFK_Fabric,
                        Lycra = item.Lycra,
                        Merchandising_YarnTypeFk = item.Merchandising_YarnTypeFk,
                        OrderQuantityPCS = currentStyle.PackQty * currentStyle.PackPieceQty,
                        ProcessLoss = item.ProcessLoss,
                        Raw_ItemFK = item.Raw_ItemFK,
                        ReferenceNo = item.ReferenceNo,
                        Remarks = item.Remarks,

                        UserID = item.UserID,
                        Common_CurrencyFK = item.Common_CurrencyFK,
                        Consumption = item.Consumption,
                        Price = item.Price,
                        Time = DateTime.Now,
                        User = item.User
                    };
                    merchandisingYarnCalculationList.Add(merchandisingYarnCalculation);
                }
            }

            _db.Merchandising_YarnCalculation.AddRange(merchandisingYarnCalculationList);

            if (await _db.SaveChangesAsync() > 0)
            {

                result = 1;
            }
            return result;
        }

        public async Task<int> MerchandisingBOFAdd(VMBOFView vMBOF)
        {
            var result = -1;
            Merchandising_BOF merchandisingBOF = new Merchandising_BOF()
            {
                Common_RawItemFK = vMBOF.Common_RawItemFK,
                Description = vMBOF.Description,
                Common_UnitFK = vMBOF.Common_UnitFK,
                UserID = vMBOF.UserID,
                IsFabric = vMBOF.IsFabrics,

                Common_CurrencyFK = vMBOF.Common_CurrencyFK,
                //Common_SupplierFK = vmYarnCalculation.Common_SupplierFK,
                Merchandising_StyleFK = vMBOF.Merchandising_StyleFK,
                Merchandising_YarnCalculationFK = vMBOF.Merchandising_YarnCalculationFK,
                Consumption = vMBOF.Consumption,
                RequiredQuantity = vMBOF.RequiredQuantity,
                Price = vMBOF.Price,
                Time = DateTime.Now,
                User = vMBOF.User
            };
            _db.Merchandising_BOF.Add(merchandisingBOF);

            if (await _db.SaveChangesAsync() > 0)
            {
                result = merchandisingBOF.ID;
            }
            return result;
        }

        public async Task<int> MerchandisingBOFEdit(VMBOFView vMBOF)
        {
            var result = -1;

            Merchandising_BOF merchandisingBOF = _db.Merchandising_BOF.Find(vMBOF.ID);
            merchandisingBOF.Merchandising_StyleFK = vMBOF.Merchandising_StyleFK;
            merchandisingBOF.Merchandising_YarnCalculationFK = vMBOF.Merchandising_YarnCalculationFK;
            merchandisingBOF.Price = vMBOF.Price;
            merchandisingBOF.RequiredQuantity = vMBOF.RequiredQuantity;
            merchandisingBOF.Common_RawItemFK = vMBOF.Common_RawItemFK;
            merchandisingBOF.Consumption = vMBOF.Consumption;
            merchandisingBOF.Description = vMBOF.Description;

            if (await _db.SaveChangesAsync() > 0)
            {
                result = merchandisingBOF.ID;
            }
            return result;
        }

        public async Task<int> MerchandisingBOFDelete(int Id)
        {
            var result = -1;

            Merchandising_BOF merchandisingBOF = _db.Merchandising_BOF.Find(Id);

            if (merchandisingBOF != null)
            {
                merchandisingBOF.Active = false;
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = merchandisingBOF.Merchandising_StyleFK;
                }
            }

            return result;
        }

        public List<object> YarnTypeDropDownList()
        {
            var List = new List<object>();
            //List.Add(new { Text = "Solid", Value = 1 });
            //List.Add(new { Text = "Yarn Deying", Value = 2 });
            //List.Add(new { Text = "All Over Print(AOP)", Value = 3 });

            _db.Merchandising_YarnType
            .Where(x => x.Active).Select(x => x).ToList()
            .ForEach(x => List.Add(new
            {
                Value = x.ID,
                Text = x.Name
            }));
            return List;
        }

        private List<int> GetInsertedYarn(int styleID)
        {
            List<int> insertedYarnOnBOFList = new List<int>();
            insertedYarnOnBOFList = (from t1 in _db.Merchandising_BOF
                                     join t2 in _db.Merchandising_YarnCalculation on t1.Merchandising_YarnCalculationFK equals t2.ID
                                     where t1.Active == true && t2.Active == true && t1.Merchandising_StyleFK == styleID
                                     select t1.Merchandising_YarnCalculationFK).Distinct().ToList();
            // insertedYarnOnBOFList = _db.Merchandising_BOF.Where(x => x.Merchandising_YarnCalculationFK == styleID && x.Active == true).Select(x => x.Merchandising_YarnCalculationFK).ToList();

            return insertedYarnOnBOFList;
        }
        internal string GetLycraValue(int id)
        {

            return (from t1 in _db.Merchandising_YarnCalculation
                    join t2 in _db.Merchandising_Style on t1.Merchandising_StyleFk equals t2.ID
                    where t1.Merchandising_StyleFk == id && t1.Active == true && t1.IsApprove == true
                    select decimal.Multiply(decimal.Divide(decimal.Divide(((decimal.Multiply((decimal.Divide(t1.OrderQuantityPCS, 12)), t1.Consumption))), (decimal.Subtract(decimal.Divide(100, 100), decimal.Multiply((decimal.Divide(t1.ProcessLoss, 100)), 1)))), 100), t1.Lycra)).DefaultIfEmpty(0).Sum().ToString("F");

        }


        internal async Task<int> AdditionYarnInBOM(int yarnId, int styleId, int userId)
        {

            var insertedYarn = GetInsertedYarn(styleId);


            decimal? lycrarequiredQuantity = (from t1 in _db.Merchandising_BOF
                                              join t2 in _db.Common_RawItem on t1.Common_RawItemFK equals t2.ID
                                              where t1.Active == true && t1.Merchandising_StyleFK == styleId && t2.Common_RawSubCategoryFK == 2044
                                              select t1.RequiredQuantity).DefaultIfEmpty(0).Sum();


            var yarns = (from t1 in _db.Merchandising_YarnCalculation
                         join t2 in _db.Common_RawItem on t1.Raw_ItemFK equals t2.ID
                         join t3 in _db.Common_Color on t1.Common_ColorFK equals t3.ID

                         where t1.ID == yarnId && t1.Active == true && t1.IsApprove == true
                         && t1.Merchandising_StyleFk == styleId
                         && !insertedYarn.Contains(t1.ID)
                         select new VMYarnCalculation
                         {
                             Color = t3.Name,
                             Common_ColorFK = t3.ID,
                             Combo = t1.Combo,
                             Common_CurrencyFK = t1.Common_CurrencyFK,
                             Common_UnitFK = t1.Common_UnitFK,
                             Fabrication = t1.Fabrication,
                             FinishDIA = t1.FinishDIA,
                             GSM = t1.GSM,
                             ID = t1.ID,
                             Merchandising_StyleFk = t1.Merchandising_StyleFk,
                             Merchandising_YarnTypeFk = t1.Merchandising_YarnTypeFk,
                             Raw_ItemFK = t1.Raw_ItemFK,
                             ReferenceNo = t1.ReferenceNo,
                             User = t1.User,
                             IsActive = t1.Active,
                             IsApproved = t1.IsApprove,
                             ProcessLoss = t1.ProcessLoss,
                             Lycra = t1.Lycra,
                             Consumption = t1.Consumption,
                             OrderQuantityPCS = t1.OrderQuantityPCS,
                             Price = t1.Price

                         }).ToList();

            var unit = yarns.Count() > 0 ? yarns.FirstOrDefault().Common_UnitFK : 0;
            //VmMkt_BOM vmMkt_BOM = new VmMkt_BOM();

            string lycraValue = GetLycraValue(styleId);


            /*This requirement is approved by Mohsin charman of ROMO
             =======================================================


                Solid: 1 Yarn 100%
                       2 Knitting 100%
                       3 Dying 100%
                       4 No
                       5 Extra Finish

           Yarn Dying: 1 Yarn 100%
                       2 Yarn Dying 100%
                       3 Knitting 100% editable
                       4 Yarn Dying Washing 100% of Knitting
                       5 Extra Finish

                  AOP: 1 Yarn 100%
                       2 Knitting 100%
                       3 Dying for AOP 100%
                       4 AOP 100% editable
                       5 Extra Finish */
            //var lycra = _db.Merchandising_BOF.Join(_db.Common_RawItem,
            //                merchandisingBOF => merchandisingBOF.Common_RawItemFK, commonRawItem => commonRawItem.ID,
            //                (merchandisingBOF, commonRawItem) => new VMBOF
            //                {
            //                    ID = merchandisingBOF.ID,
            //                    RawItemName = commonRawItem.Name
            //                }).ToList();
            var lycra = (from t1 in _db.Merchandising_BOF
                         join t2 in _db.Common_RawItem on t1.Common_RawItemFK equals t2.ID
                         where t1.Active == true && t1.Merchandising_StyleFK == styleId && t2.Common_RawSubCategoryFK == 2044
                         select new
                         {
                             BOFID = t1.ID
                         }).FirstOrDefault();
            List<Merchandising_BOF> BOFList = new List<Merchandising_BOF>();

            if (yarns.Count() > 0)
            {
                // Serial 1
                foreach (var x in yarns)
                {
                    Merchandising_BOF BOF_1 = new Merchandising_BOF();
                    BOF_1.Merchandising_StyleFK = styleId;
                    if (x.Common_CurrencyFK != 0)
                        BOF_1.Common_CurrencyFK = x.Common_CurrencyFK;
                    BOF_1.Common_UnitFK = x.Common_UnitFK;

                    //if (x.Merchandising_YarnCalculation.Common_SupplierFK != null)
                    //    x.Merchandising_StyleSlave.Common_SupplierFK = x.Merchandising_YarnCalculation.Common_SupplierFK.Value;
                    //if (x.Merchandising_YarnCalculation.Common_UnitFK != null)
                    //    merchandising_BOF.Common_UnitFK = x.Merchandising_YarnCalculation.Common_UnitFK.Value;
                    BOF_1.Merchandising_YarnCalculationFK = x.ID;
                    BOF_1.Consumption = x.Consumption;
                    BOF_1.Description = x.Fabrication + "-GSM:" + x.GSM + "-Dia:" + x.FinishDIA + "-Color:" + x.Color;
                    if (x.Raw_ItemFK != 0)
                    {
                        BOF_1.Common_RawItemFK = x.Raw_ItemFK;
                    }

                    BOF_1.RequiredQuantity = decimal.Subtract(decimal.Divide(((decimal.Multiply((decimal.Divide(x.OrderQuantityPCS, 12)), x.Consumption))), (decimal.Subtract(decimal.Divide(100, 100), decimal.Multiply((decimal.Divide(x.ProcessLoss, 100)), 1)))), decimal.Multiply(decimal.Divide(decimal.Divide(((decimal.Multiply((decimal.Divide(x.OrderQuantityPCS, 12)), x.Consumption))), (decimal.Subtract(decimal.Divide(100, 100), decimal.Multiply((decimal.Divide(x.ProcessLoss, 100)), 1)))), 100), x.Lycra));
                    BOF_1.Price = x.Price;
                    BOF_1.UserID = userId;
                    BOF_1.User = userId.ToString();

                    BOFList.Add(BOF_1);
                    //_db.Merchandising_BOF.Add(merchandising_BOF);
                    //_db.SaveChangesAsync();


                    // Serial 2
                    Merchandising_BOF BOF_2 = new Merchandising_BOF();
                    BOF_2.Merchandising_StyleFK = styleId;
                    if (x.Common_CurrencyFK != 0)
                        BOF_2.Common_CurrencyFK = x.Common_CurrencyFK;
                    //merchandising_BOF.Common_SupplierFK = 1;
                    //if (x.Merchandising_YarnCalculation.Common_UnitFK != null)
                    BOF_2.Common_UnitFK = x.Common_UnitFK;
                    BOF_2.Merchandising_YarnCalculationFK = x.ID;
                    BOF_2.Consumption = x.Consumption;
                    BOF_2.Description = x.Fabrication + "-GSM:" + x.GSM + "-Dia:" + x.FinishDIA + "-Color:" + x.Color;
                    if (x.Merchandising_YarnTypeFk == 1)
                    {
                        if (x.Raw_ItemFK != 0)
                        {
                            BOF_2.Common_RawItemFK = 5484; //Knitting  
                        }
                    }
                    else if (x.Merchandising_YarnTypeFk == 2)
                    {
                        if (x.Raw_ItemFK != 0)
                        {
                            BOF_2.Common_RawItemFK = 5488; //Yarn Dyeing
                        }
                    }
                    else if (x.Merchandising_YarnTypeFk == 3)
                    {
                        if (x.Raw_ItemFK != 0)
                        {
                            BOF_2.Common_RawItemFK = 5485; //Knitting for AOP
                        }
                    }
                    BOF_2.RequiredQuantity = decimal.Divide(((decimal.Multiply((decimal.Divide(x.OrderQuantityPCS, 12)), x.Consumption))), (decimal.Subtract(decimal.Divide(100, 100), decimal.Multiply((decimal.Divide(x.ProcessLoss, 100)), 1))));
                    BOF_2.Price = 0;
                    BOF_2.UserID = userId;
                    BOF_2.User = userId.ToString();

                    BOFList.Add(BOF_2);

                    // Serial 3
                    Merchandising_BOF BOF_3 = new Merchandising_BOF();

                    BOF_3.Merchandising_StyleFK = styleId;
                    if (x.Common_CurrencyFK != 0)
                        BOF_3.Common_CurrencyFK = x.Common_CurrencyFK;

                    //merchandising_BOF.Common_SupplierFK = 1;
                    //if (x.Merchandising_YarnCalculation.Common_UnitFK != null)
                    BOF_3.Common_UnitFK = x.Common_UnitFK;

                    BOF_3.Merchandising_YarnCalculationFK = x.ID;
                    BOF_3.Consumption = x.Consumption;
                    BOF_3.Description = x.Fabrication + "-GSM:" + x.GSM + "-Dia:" + x.FinishDIA + "-Color:" + x.Color;
                    if (x.Merchandising_YarnTypeFk == 1)
                    {
                        if (x.Raw_ItemFK != 0)
                        {
                            BOF_3.Common_RawItemFK = 5487; // Dyeing
                            BOF_3.IsFabric = true; // Dyeing

                        }
                    }
                    else if (x.Merchandising_YarnTypeFk == 2)
                    {
                        if (x.Raw_ItemFK != 0)
                        {
                            BOF_3.Common_RawItemFK = 5486; //Yarn Dyeing Knitting
                        }
                    }
                    else if (x.Merchandising_YarnTypeFk == 3)
                    {
                        if (x.Raw_ItemFK != 0)
                        {
                            BOF_3.Common_RawItemFK = 5489; //Dyeing for AOP
                        }
                    }
                    BOF_3.RequiredQuantity = decimal.Divide(((decimal.Multiply((decimal.Divide(x.OrderQuantityPCS, 12)), x.Consumption))), (decimal.Subtract(decimal.Divide(100, 100), decimal.Multiply((decimal.Divide(x.ProcessLoss, 100)), 1))));
                    BOF_3.Price = 0;
                    BOF_3.UserID = userId;
                    BOF_3.User = userId.ToString();

                    BOFList.Add(BOF_3);
                    // Serial 4
                    if (x.Merchandising_YarnTypeFk != 1)
                    {
                        Merchandising_BOF BOF_4 = new Merchandising_BOF();
                        BOF_4.Merchandising_StyleFK = styleId;
                        if (x.Common_CurrencyFK != 0)
                            BOF_4.Common_CurrencyFK = x.Common_CurrencyFK;

                        //merchandising_BOF.Common_SupplierFK = 1;
                        //if (x.Merchandising_YarnCalculation.Common_UnitFK != null)
                        BOF_4.Common_UnitFK = x.Common_UnitFK;

                        BOF_4.Merchandising_YarnCalculationFK = x.ID;
                        BOF_4.Consumption = x.Consumption;
                        BOF_4.Description = x.Fabrication + "-GSM:" + x.GSM + "-Dia:" + x.FinishDIA + "-Color:" + x.Color;

                        if (x.Merchandising_YarnTypeFk == 2)
                        {
                            if (x.Raw_ItemFK != 0)
                            {
                                BOF_4.Common_RawItemFK = 5490; //yarn Dyeing Washing
                                BOF_4.IsFabric = true; //yarn Dyeing Washing

                            }
                        }
                        else if (x.Merchandising_YarnTypeFk == 3)
                        {
                            if (x.Raw_ItemFK != 0)
                            {
                                BOF_4.Common_RawItemFK = 5491; //All Over Print (AOP)
                                BOF_4.IsFabric = true; //All Over Print (AOP)

                            }
                        }
                        BOF_4.RequiredQuantity = decimal.Divide(((decimal.Multiply((decimal.Divide(x.OrderQuantityPCS, 12)), x.Consumption))), (decimal.Subtract(decimal.Divide(100, 100), decimal.Multiply((decimal.Divide(x.ProcessLoss, 100)), 1))));
                        BOF_4.Price = 0;
                        BOF_4.UserID = userId;
                        BOF_4.User = userId.ToString();

                        BOFList.Add(BOF_4);

                        //_db.Merchandising_BOF.Add(merchandising_BOF);
                        //_db.SaveChangesAsync();
                    }
                }

            }
            if (lycra != null)
            {
                Merchandising_BOF BOF_X = new Merchandising_BOF();
                BOF_X = _db.Merchandising_BOF.Where(x => x.ID == lycra.BOFID).FirstOrDefault();
                BOF_X.RequiredQuantity = Convert.ToDecimal(lycraValue);
                await _db.SaveChangesAsync();
            }
            else
            {
                if (Convert.ToDecimal(lycraValue) > 0)
                {

                    Merchandising_BOF BOF_5 = new Merchandising_BOF();


                    BOF_5.Merchandising_StyleFK = styleId;
                    BOF_5.Common_CurrencyFK = 1;
                    BOF_5.Common_UnitFK = unit;
                    //vmBOFView.Merchandising_StyleSlave.Common_UnitFK = 13;
                    BOF_5.Merchandising_YarnCalculationFK = 0;
                    BOF_5.Consumption = 0;
                    BOF_5.Description = "Lycra Yarn 20D";

                    BOF_5.Common_RawItemFK = 5331;
                    BOF_5.RequiredQuantity = Convert.ToDecimal(lycraValue);
                    BOF_5.Price = 0;
                    BOF_5.UserID = userId;
                    BOF_5.User = userId.ToString();


                    BOFList.Add(BOF_5);


                }
            }
            int result = -1;
            if (BOFList.Count() > 0)
            {
                try
                {
                    _db.Merchandising_BOF.AddRange(BOFList);
                    result = await _db.SaveChangesAsync();

                }
                catch (Exception ex)
                {
                    var msg = ex.Message;
                }
            }
            return result;
        }

        public void BOFGetByIDView(int yarnId, int styleID, int userID)
        {

            int success = Task.Run(() => AdditionYarnInBOM(yarnId, styleID, userID)).Result;

        }

        private async Task<IEnumerable<VMBOF>> BOFDataLoad(int iD)
        {
            var v = await Task.Run(() => (from t1 in _db.Merchandising_BOF
                                          join t2 in _db.Merchandising_Style on t1.Merchandising_StyleFK equals t2.ID
                                          join t3 in _db.Common_RawItem on t1.Common_RawItemFK equals t3.ID
                                          join t4 in _db.Common_Currency on t1.Common_CurrencyFK equals t4.ID
                                          join t5 in _db.Common_RawSubCategory on t3.Common_RawSubCategoryFK equals t5.ID
                                          join t6 in _db.Common_RawCategory on t5.Common_RawCategoryFK equals t6.ID

                                          where t1.Active == true && t1.Merchandising_StyleFK == iD
                                          select new VMBOF
                                          {
                                              SubcategoryName = t5.Name,
                                              Common_CurrencyFK = t1.Common_CurrencyFK,
                                              Common_CurrencyName = t4.Name,
                                              Common_RawItemFK = t1.Common_RawItemFK,
                                              RawItemName = t3.Name,
                                              Consumption = t1.Consumption,
                                              Description = t1.Description,
                                              IsFabric = t1.IsFabric,
                                              Merchandising_StyleFK = t1.Merchandising_StyleFK,
                                              StyleNo = t2.StyleName,
                                              Merchandising_YarnCalculationFK = t1.Merchandising_YarnCalculationFK,
                                              Price = t1.Price,
                                              RequiredQuantity = t1.RequiredQuantity,
                                              ID = t1.ID,
                                              Common_UnitFK = t1.Common_UnitFK,
                                              Common_RawSubCategoryFK = t5.ID,
                                              Common_RawCategoryFK = t6.ID
                                          }).OrderByDescending(x => x.ID).AsEnumerable());
            return v;
        }

        public async Task<VMYarnCalculation> GetYarnCalculation(int ID)
        {
            var vmYarnCalculation = new VMYarnCalculation();
            vmYarnCalculation.VMStyle = new VMStyle();
            vmYarnCalculation.vmBOFView = new VMBOFView();

            vmYarnCalculation.VMStyle = StyleById(ID);
            vmYarnCalculation.OrderQuantityPCS = decimal.Multiply(vmYarnCalculation.VMStyle.PackPieceQty, vmYarnCalculation.VMStyle.PackQty);

            vmYarnCalculation.OrderQuantityDZ = (decimal.Divide((decimal.Multiply(vmYarnCalculation.VMStyle.PackPieceQty, vmYarnCalculation.VMStyle.PackQty)), 12));

            vmYarnCalculation.DataList = YarnCalculationDataload(ID);
            vmYarnCalculation.vmBOFView.BOFDataList = await BOFDataLoad(ID);

            return vmYarnCalculation;
        }

        private IEnumerable<VMYarnCalculation> YarnCalculationDataload(int id)
        {
            VMYarnCalculation vmYarnCalculation = new VMYarnCalculation();
            try
            {
                vmYarnCalculation.DataList = (from t1 in _db.Merchandising_YarnCalculation
                                              join t3 in _db.Merchandising_YarnType on t1.Merchandising_YarnTypeFk equals t3.ID
                                              join t4 in _db.Common_RawItem on t1.Raw_ItemFK equals t4.ID
                                              join t5 in _db.Common_Currency on t1.Common_CurrencyFK equals t5.ID
                                              join t6 in _db.Common_Unit on t1.Common_UnitFK equals t6.ID
                                              join t7 in _db.Common_Color on t1.Common_ColorFK equals t7.ID

                                              where t1.Active == true && t1.Merchandising_StyleFk == id
                                              select new VMYarnCalculation
                                              {
                                                  IsApproved = t1.IsApprove,
                                                  Color = t7.Name,
                                                  Common_ColorFK = t7.ID,
                                                  Combo = t1.Combo,
                                                  Fabrication = t1.Fabrication,
                                                  FinishDIA = t1.FinishDIA,
                                                  GSM = t1.GSM,
                                                  ID = t1.ID,
                                                  Merchandising_StyleFk = t1.Merchandising_StyleFk,
                                                  Common_CurrencyFK = t1.Common_CurrencyFK,
                                                  Common_UnitFK = t1.Common_UnitFK,
                                                  Consumption = t1.Consumption,
                                                  Merchandising_YarnTypeFk = t1.Merchandising_YarnTypeFk,
                                                  Raw_ItemFK = t1.Raw_ItemFK,
                                                  ReferenceNo = t1.ReferenceNo,
                                                  User = t1.User,
                                                  YarnTypeName = t3.Name,
                                                  ItemName = t4.Name,
                                                  CurrencyName = t5.Name,
                                                  UnitName = t6.Name,
                                                  OrderQuantityPCS = t1.OrderQuantityPCS,
                                                  OrderQuantityDZ = decimal.Divide(t1.OrderQuantityPCS, 12),
                                                  FinishFabric = decimal.Multiply((decimal.Divide(t1.OrderQuantityPCS, 12)), t1.Consumption),
                                                  ProcessLoss = t1.ProcessLoss,
                                                  YarnQnty = decimal.Divide(((decimal.Multiply((decimal.Divide(t1.OrderQuantityPCS, 12)), t1.Consumption))), (decimal.Subtract(decimal.Divide(100, 100), decimal.Multiply((decimal.Divide(t1.ProcessLoss, 100)), 1)))),
                                                  Lycra = t1.Lycra,
                                                  TotalLycra = decimal.Multiply(decimal.Divide(decimal.Divide(((decimal.Multiply((decimal.Divide(t1.OrderQuantityPCS, 12)), t1.Consumption))), (decimal.Subtract(decimal.Divide(100, 100), decimal.Multiply((decimal.Divide(t1.ProcessLoss, 100)), 1)))), 100), t1.Lycra),
                                                  TotalYarn = decimal.Subtract(decimal.Divide(((decimal.Multiply((decimal.Divide(t1.OrderQuantityPCS, 12)), t1.Consumption))), (decimal.Subtract(decimal.Divide(100, 100), decimal.Multiply((decimal.Divide(t1.ProcessLoss, 100)), 1)))), decimal.Multiply(decimal.Divide(decimal.Divide(((decimal.Multiply((decimal.Divide(t1.OrderQuantityPCS, 12)), t1.Consumption))), (decimal.Subtract(decimal.Divide(100, 100), decimal.Multiply((decimal.Divide(t1.ProcessLoss, 100)), 1)))), 100), t1.Lycra)),
                                                  Price = t1.Price
                                              }).OrderByDescending(x => x.ID).AsEnumerable();
            }
            catch (Exception e)
            {
                string str = e.Message;
            }

            return vmYarnCalculation.DataList;
        }

        #endregion

        public int ItemPoValueListStyleWise(int styleId, int itemId)
        {
            var value = (int)_db.Merchandising_StyleSlave.Where(x => x.Merchandising_StyleFK == styleId && x.Common_RawItemFK == itemId).Sum(s => s.TotalPrice);
            return value;
        }

        #region Report

        public VMStyleView StyleBOMReport(int ID)
        {
            var VM = new VMStyleView();
            VM.vmStyle = new VMStyle();
            VM.lstCat1 = new VMBOM();
            VM.lstCat2 = new VMBOM();
            VM.lstCat3 = new VMBOM();
            VM.lstCat4 = new VMBOM();
            VM.lstCat5 = new VMBOM();
            VM.lstCat6 = new VMBOM();
            VM.VMCBSSummary = new VMCBSSummary();

            VM.vmStyle = StyleById(ID);
            VM.lstCat1 = BOMGetByStyleID(ID, 2);
            VM.lstCat2 = BOMGetByStyleID(ID, 6);
            VM.lstCat3 = BOMGetByStyleID(ID, 3);
            VM.lstCat4 = BOMGetByStyleID(ID, 4);
            VM.lstCat5 = BOMGetByStyleID(ID, 5);
            VM.lstCat6 = BOMGetByStyleID(ID, 1023);
            VM.VMCBSSummary = GetBOMSummaryByStyleID(VM.vmStyle);
            return VM;
        }

        public async Task<VMStyleView> StyleBOFReportAsync(int ID)
        {
            var VM = new VMStyleView();
            VM.vmStyle = new VMStyle();

            VM.vmStyle = StyleById(ID);
            VM.BOFDataList = await BOFDataLoad(ID);
            return VM;
        }

        public VMStyle GetShipmentDetailsReport(VMStyle model)
        {
            VMStyle sList = new VMStyle();
            sList = model;
            if (model.ReportID == 1 && model.ID > 0)
            {
                #region BuyerPOWiseDetails

                var vData = (from t1 in _db.Merchandising_BuyerOrder
                             join t2 in _db.Merchandising_Style on t1.ID equals t2.Merchandising_BuyerOrderFK
                             join t3 in _db.Common_Buyer on t1.Common_BuyerFK equals t3.ID
                             join t4 in _db.Merchandising_StyleShipmentSchedule on t2.ID equals t4.Merchandising_StyleFK into ship
                             from t5 in ship.DefaultIfEmpty()
                             where t1.Active == true && t1.IsComplete == false && t2.Active == true && t5.Active == true
                             && t1.Common_BuyerFK == model.ID && t5.ShipmentDate >= model.FromDate && t5.ShipmentDate <= model.ToDate
                             group t5.Quantity by new { t1, t2, t3, t5.ShipmentDate } into all
                             select new VMStyle
                             {
                                 BuyerName = all.Key.t3.Name,
                                 BuyerPO = all.Key.t1.BuyerPO,
                                 Style = all.Key.t2.StyleName,
                                 ShipDate = all.Key.ShipmentDate,
                                 PackQty = (int)all.Sum(),
                                 PieceQty = (int)decimal.Multiply(all.Sum(), all.Key.t2.PackPieceQty)
                                 //ProductionPieceQty = (int)decimal.Multiply(decimal.Divide((int)all.Sum(), all.Key.t2.SetQuantity), all.Key.t2.PackPieceQty)
                             }).OrderBy(a => a.ShipDate).ToList();

                sList.DataList = vData;
                #endregion
            }
            else if (model.ReportID == 1 && model.ID == 0)
            {
                #region AllBuyerPODetails

                var vData = (from t1 in _db.Merchandising_BuyerOrder
                             join t2 in _db.Merchandising_Style on t1.ID equals t2.Merchandising_BuyerOrderFK
                             join t3 in _db.Common_Buyer on t1.Common_BuyerFK equals t3.ID
                             join t4 in _db.Merchandising_StyleShipmentSchedule on t2.ID equals t4.Merchandising_StyleFK into ship
                             from t5 in ship.DefaultIfEmpty()
                             where t1.Active == true && t1.IsComplete == false && t2.Active == true && t5.Active == true
                             && t5.ShipmentDate >= model.FromDate && t5.ShipmentDate <= model.ToDate
                             group t5.Quantity by new { t5.ShipmentDate, t1, t2, t3 } into all
                             select new VMStyle
                             {
                                 BuyerName = all.Key.t3.Name,
                                 BuyerPO = all.Key.t1.BuyerPO,
                                 Style = all.Key.t2.StyleName,
                                 ShipDate = all.Key.ShipmentDate,
                                 PackQty = (int)all.Sum(),
                                 PieceQty = (int)decimal.Multiply(all.Sum(), all.Key.t2.PackPieceQty)
                                 //PackQty = (int)decimal.Divide(decimal.Divide((int)all.Sum(), all.Key.t2.SetQuantity), all.Key.t2.PackPieceQty)
                             }).ToList();

                sList.DataList = vData;
                #endregion
            }
            else if (model.ReportID == 2 && model.ID > 0)
            {
                #region BuyerWiseSummary

                var vData = (from t1 in _db.Merchandising_BuyerOrder
                             join t2 in _db.Merchandising_Style on t1.ID equals t2.Merchandising_BuyerOrderFK
                             join t3 in _db.Common_Buyer on t1.Common_BuyerFK equals t3.ID
                             join t4 in _db.Merchandising_StyleShipmentSchedule on t2.ID equals t4.Merchandising_StyleFK into ship
                             from t5 in ship.DefaultIfEmpty()
                             where t1.Active == true && t1.IsComplete == false && t2.Active == true && t5.Active == true
                             && t1.Common_BuyerFK == model.ID && t5.ShipmentDate >= model.FromDate && t5.ShipmentDate <= model.ToDate
                             group t5.Quantity by new { t1, t2, t3 } into all
                             select new VMStyle
                             {
                                 BuyerName = all.Key.t3.Name,
                                 BuyerPO = all.Key.t1.BuyerPO,
                                 Style = all.Key.t2.StyleName,
                                 ProductionPieceQty = all.Sum(),
                                 PackQty = (int)all.Sum(),
                                 PieceQty = (int)decimal.Multiply(all.Sum(), all.Key.t2.PackPieceQty)
                             }).ToList();

                sList.DataList = vData;

                #endregion
            }
            else if (model.ReportID == 2 && model.ID == 0)
            {
                #region AllBuyerPOSummary

                var vData = (from t1 in _db.Merchandising_BuyerOrder
                             join t2 in _db.Merchandising_Style on t1.ID equals t2.Merchandising_BuyerOrderFK
                             join t3 in _db.Common_Buyer on t1.Common_BuyerFK equals t3.ID
                             join t4 in _db.Merchandising_StyleShipmentSchedule on t2.ID equals t4.Merchandising_StyleFK into ship
                             from t5 in ship.DefaultIfEmpty()
                             where t1.Active == true && t1.IsComplete == false && t2.Active == true && t5.Active == true
                             && t5.ShipmentDate >= model.FromDate && t5.ShipmentDate <= model.ToDate
                             group t5.Quantity by new { t1, t2, t3 } into all
                             select new VMStyle
                             {
                                 BuyerName = all.Key.t3.Name,
                                 BuyerPO = all.Key.t1.BuyerPO,
                                 Style = all.Key.t2.StyleName,
                                 PackQty = (int)all.Sum(),
                                 PieceQty = (int)decimal.Multiply(all.Sum(), all.Key.t2.PackPieceQty)
                             }).ToList();

                sList.DataList = vData;
                #endregion
            }

            return sList;
        }

        #endregion

        #region API
        public List<VMApiStyle> GetAllApiStyle()
        {
            DateTime today = DateTime.Today;
            var DataList = (from t1 in _db.Prod_ReferencePlan
                            join t2 in _db.Prod_Reference on t1.Prod_ReferenceFK equals t2.ID
                            join t3 in _db.Common_LineDevice on t1.ProductionLineFK equals t3.Common_ProductionLineFK
                            join t4 in _db.Merchandising_Style on t1.Merchandising_StyleFK equals t4.ID
                            join t5 in _db.Common_ProductionLine on t3.Common_ProductionLineFK equals t5.ID
                            where t2.ReferenceDate == today && t1.Active == true
                            select new VMApiStyle
                            {
                                DeviceID = t3.DeviceID,
                                ID = t1.Merchandising_StyleFK,
                                LineID = t3.Common_ProductionLineFK,
                                Name = t4.StyleName,
                                LineName=t5.Name
                            }).ToList();

            //var DataList = _db.Merchandising_Style.Where(a => a.Active == true).ToList().ConvertAll(
            //      x => new VMApiStyle { ID = x.ID, Name = x.StyleName });

            return DataList;
        }

        public List<VMApiStyleSetPack> GetAllApiSetPack()
        {
            DateTime today = DateTime.Today;
            var DataList = (from t1 in _db.Prod_ReferencePlan
                            join t2 in _db.Prod_Reference on t1.Prod_ReferenceFK equals t2.ID
                            join t3 in _db.Merchandising_Style on t1.Merchandising_StyleFK equals t3.ID
                            join t5 in _db.Merchandising_StyleSetPack on t3.ID equals t5.Merchandising_StyleFK
                            where t2.ReferenceDate == today && t1.Active == true
                            select new VMApiStyleSetPack
                            {
                                ID = t5.ID,
                                SetPackName = t5.SetPackName
                            }).ToList();

            //var DataList = _db.Merchandising_StyleSetPack.Where(a => a.Active == true).ToList().ConvertAll(
            //      x => new VMApiStyleSetPack { ID = x.ID, SetPackName = x.SetPackName });

            return DataList;
        }
        #endregion

        #region DashboardIntigration

        //private async Task<int> SetDashboard(VMNotification model)
        //{
        //    var vCheck = _db.Common_Notification.Where(a=>a.StyleID==model.ID);
        //    if (vCheck.Any())
        //    {
        //        if (model.IsBOMCo)
        //        {
        //            var update = vCheck.FirstOrDefault();
        //            update.CompleteBOM = model.CompleteBOM;
        //        }
        //        else if (model.IsPRCo)
        //        {
        //            var update = vCheck.FirstOrDefault();
        //            update.TPR = model.TPR;
        //        }

        //    }
        //    else { }
        //}

        #endregion
    }

    public class ColorList
    {
        public int ColorFK { get; set; }
        public int TotalQty { get; set; }
    }
}