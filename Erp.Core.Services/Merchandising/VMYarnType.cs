﻿using Erp.Core.Entity.Common;
using Erp.Core.Entity.Merchandising;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Erp.Core.Services.Merchandising
{
    public class VMYarnType : BaseVM
    {        
        [MaxLength(50, ErrorMessage = "Upto 50 Chracter")]
        [DisplayName("Type")]
        public string Name { get; set; }
        public IEnumerable<VMYarnType> DataList { get; set; }



    }
}