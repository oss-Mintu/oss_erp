﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Erp.Core.Services.Merchandising
{
    public class VMBOF : BaseVM
    {
        public int Merchandising_StyleFK { get; set; }
        public string StyleNo { get; set; }

        public int? Merchandising_YarnCalculationFK { get; set; }
        public int? Common_RawItemFK { get; set; }
        public string RawItemName { get; set; }
        public int? Common_CurrencyFK { get; set; }
        public string Common_CurrencyName { get; set; }
        public string SubcategoryName { get; set; }

        public string Description { get; set; }
        
        public decimal Consumption { get; set; }
        
        public decimal? RequiredQuantity { get; set; }
        
        public decimal Price { get; set; }
        public bool IsFabric { get;  set; }
        public int? Common_UnitFK { get; set; }
        public string UnitName { get; set; }
        public int Common_RawSubCategoryFK { get;  set; }
        public int Common_RawCategoryFK { get;  set; }
    }
}
