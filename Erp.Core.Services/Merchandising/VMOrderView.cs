﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Erp.Core.Services.Merchandising
{
    public class VMOrderView : BaseVM
    {
        public VMStyle vmStyle { get; set; }

        public VMOrder vmOrder { get; set; }

        public VMShipmentSchedule VMShipmentSchedule { get; set; }
    }
}


