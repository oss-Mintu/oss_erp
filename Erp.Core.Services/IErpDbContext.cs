﻿using System.Threading;
using System.Threading.Tasks;
using Erp.Core.Entity.Accounting;
using Erp.Core.Entity.Common;
using Erp.Core.Entity.HRMS;
using Erp.Core.Entity.Merchandising;
using Erp.Core.Entity.MIS;
using Erp.Core.Entity.Payroll;
using Erp.Core.Entity.Procurement;
using Erp.Core.Entity.Production;
using Erp.Core.Entity.Shipment;
using Erp.Core.Entity.Store;
using Erp.Core.Entity.User;
using Erp.Core.Entity.Inventory;
using Erp.Core.Services.Commercial;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace Erp.Core.Services
{
    public interface IErpDbContext
    {
        //Accounting

        DbSet<Accounting_Type> Accounting_Type { get; set; }
        DbSet<Accounting_Chart1> Accounting_Chart1 { get; set; }
        DbSet<Accounting_Chart2> Accounting_Chart2 { get; set; }
        DbSet<Accounting_Head> Accounting_Head { get; set; }
        DbSet<Accounting_Journal> Accounting_Journal { get; set; }
        DbSet<Accounting_JournalSlave> Accounting_JournalSlave { get; set; }
        DbSet<Accounting_CostCenter> Accounting_CostCenter { get; set; }
        DbSet<Accounting_Transaction> Accounting_Transaction { get; set; }
     

        // Inventory
        DbSet<Inventorydb>Inventorydb { get; set; }



        //Common Setting

        DbSet<Common_BuyerNotifyParty> Common_BuyerNotifyParty { get; set; }
        DbSet<Common_RawCategory> Common_RawCategory { get; set; }
        DbSet<Common_RawSubCategory> Common_RawSubCategory { get; set; }
        DbSet<Common_RawItem> Common_RawItem { get; set; }
        DbSet<Common_Brand> Common_Brand { get; set; }
        DbSet<Common_Model> Common_Model { get; set; }
        DbSet<Common_FinishCategory> Common_FinishCategory { get; set; }
        DbSet<Common_FinishSubCategory> Common_FinishSubCategory { get; set; }
        DbSet<Common_FinishItem> Common_FinishItem { get; set; }

        DbSet<Common_Buyer> Common_Buyer { get; set; }
        DbSet<Common_Agent> Common_Agent { get; set; }
        DbSet<Common_InspectionAgent> Common_InspectionAgent { get; set; }
        DbSet<Common_Supplier> Common_Supplier { get; set; }
        DbSet<Common_Unit> Common_Unit { get; set; }
        DbSet<Common_Currency> Common_Currency { get; set; }
        DbSet<Common_Country> Common_Country { get; set; }
       

        DbSet<Common_Color> Common_Color { get; set; }
        DbSet<Common_Size> Common_Size { get; set; }
        DbSet<Common_RejectType> Common_RejectType { get; set; }

        DbSet<Common_CountryPort> Common_CountryPort { get; set; }
        DbSet<Common_CountrySize> Common_CountrySize { get; set; }
        DbSet<Common_Product> Common_Product { get; set; }
        DbSet<Common_LcType> Common_LcType { get; set; }
        DbSet<Common_BuyerBank> Common_BuyerBank { get; set; }
        DbSet<Common_LienBank> Common_LienBank { get; set; }
        DbSet<Common_CompanyBank> Common_CompanyBank { get; set; }
        DbSet<Common_Bank> Common_Bank { get; set; }
        DbSet<Common_CurrencyChangesLog> Common_CurrencyChangesLog { get; set; }

        DbSet<Common_SectionLine> Common_SectionLine { get; set; }
        DbSet<Common_ProductionUnit> Common_ProductionUnit { get; set; }
        DbSet<Common_ProductionFloor> Common_ProductionFloor { get; set; }
        DbSet<Common_Notification> Common_Notification { get; set; }
        DbSet<Common_LineDevice> Common_LineDevice { get; set; }
        DbSet<Common_Notify> Common_Notify { get; set; }

        //User Credentials
        DbSet<User_Department> User_Department { get; set; }
        DbSet<User_User> User_User { get; set; }
        DbSet<User_AccessLevel> User_AccessLevel { get; set; }
        DbSet<User_Menu> User_Menu { get; set; }
        DbSet<User_MenuItem> User_MenuItem { get; set; }
        DbSet<User_Role> User_Role { get; set; }
        DbSet<User_RoleMenuItem> User_RoleMenuItem { get; set; }
        DbSet<User_SubMenu> User_SubMenu { get; set; }

        DbSet<Shipment_TermsOfShipment> Shipment_TermsOfShipment { get; set; }

        DbSet<Shipment_ShipmentInstruction> Shipment_ShipmentInstruction { get; set; }
        DbSet<Shipment_ShipmentInstructionSlave> Shipment_ShipmentInstructionSlave { get; set; }
        DbSet<Shipment_DeliveryChallan> Shipment_DeliveryChallan { get; set; }
        DbSet<Shipment_DeliveryChallanSlave> Shipment_DeliveryChallanSlave { get; set; }
        DbSet<Shipment_ShipmentInvoice> Shipment_ShipmentInvoice { get; set; }
        DbSet<Shipment_InvoicedBuyerNotifyParty> Shipment_InvoicedBuyerNotifyParty { get; set; }
        DbSet<Shipment_ShipmentInvoiceSlave> Shipment_ShipmentInvoiceSlave { get; set; }
        DbSet<Shipment_BillOfExchange> Shipment_BillOfExchange { get; set; }
        DbSet<Shipment_BillOfExchangeSlave> Shipment_BillOfExchangeSlave { get; set; }
        DbSet<Shipment_ExportRealisation> Shipment_ExportRealisation { get; set; }


        //Mis
        DbSet<Mis_CMEarned> Mis_CMEarned { get; set; }
        DbSet<Mis_CashInHand> Mis_CashInHand { get; set; }
        DbSet<Mis_CashAtBank> Mis_CashAtBank { get; set; }
        DbSet<Mis_IncomeStatement> Mis_IncomeStatement { get; set; }
        DbSet<Mis_BalanceSheet> Mis_BalanceSheet { get; set; }
        DbSet<Mis_AccountsReceivable> Mis_AccountsReceivable { get; set; }
        DbSet<Mis_AccountsPayable> Mis_AccountsPayable { get; set; }
        DbSet<Mis_OrderConfirm> Mis_OrderConfirm { get; set; }
        DbSet<Mis_RawMaterials> Mis_RawMaterials { get; set; }
        DbSet<Mis_ShipmentStatus> Mis_ShipmentStatus { get; set; }
        DbSet<Mis_DailyProduction> Mis_DailyProduction { get; set; }
        DbSet<Mis_DailyAttendance> Mis_DailyAttendance { get; set; }
        DbSet<Mis_FinancialStatus> Mis_FinancialStatus { get; set; }
        DbSet<Mis_BTBLCStatus> Mis_BTBLCStatus { get; set; }
        DbSet<Mis_Log> Mis_Log { get; set; }

        DbSet<Commercial_ECIValueAmendment> Commercial_ECIValueAmendment { get; set; }
        DbSet<Commercial_BBLCValueAmendment> Commercial_BBLCValueAmendment { get; set; }
        DbSet<Commercial_UD> Commercial_UD { get; set; }      
       
        DbSet<Commercial_ECI> Commercial_ECI { get; set; } 
        DbSet<Commercial_BBLC> Commercial_BBLC { get; set; }      
      
        DbSet<Common_TCTitle> Common_TCTitle { get; set; }
        DbSet<Commercial_BBLCPaymentInformation> Commercial_BBLCPaymentInformation { get; set; }


        

        //Procurement
        DbSet<Procurement_PurchaseRequisition> Procurement_PurchaseRequisition { get; set; }
        DbSet<Procurement_PurchaseRequisitionSlave> Procurement_PurchaseRequisitionSlave { get; set; }
        
        DbSet<Procurement_PurchaseOrder> Procurement_PurchaseOrder { get; set; }
        DbSet<Procurement_PurchaseOrderSlave> Procurement_PurchaseOrderSlave { get; set; }
        DbSet<Procurement_PurchaseInvoice> Procurement_PurchaseInvoice { get; set; }
        DbSet<Procurement_PurchaseInvoiceSlave> Procurement_PurchaseInvoiceSlave { get; set; }

        /// Merchandising
        DbSet<Merchandising_BuyerOrder> Merchandising_BuyerOrder { get; set; }
        DbSet<Merchandising_Style> Merchandising_Style { get; set; }
        DbSet<Merchandising_StyleMeasurement> Merchandising_StyleMeasurement { get; set; }
        DbSet<Merchandising_StyleSlave> Merchandising_StyleSlave { get; set; }
        DbSet<Merchandising_StyleSetPack> Merchandising_StyleSetPack { get; set; }
        DbSet<Merchandising_StyleShipmentSchedule> Merchandising_StyleShipmentSchedule { get; set; }
        DbSet<Merchandising_StyleShipmentRatio> Merchandising_StyleShipmentRatio { get; set; }
        DbSet<Merchandising_CBSStyle> Merchandising_CBSStyle { get; set; }
        DbSet<Merchandising_CBS> Merchandising_CBS { get; set; }
        DbSet<Merchandising_CBSSlave> Merchandising_CBSSlave { get; set; }
        DbSet<Merchandising_YarnCalculation> Merchandising_YarnCalculation { get; set; }
        DbSet<Merchandising_YarnType> Merchandising_YarnType { get; set; }
        DbSet<Merchandising_BOF> Merchandising_BOF { get; set; }
        DbSet<Merchandising_Packing> Merchandising_Packing { get; set; }


        //ProductionPlan
        DbSet<Common_ProductionLine> Common_ProductionLine { get; set; }
        DbSet<Plan_PlanConfig> Plan_PlanConfig { get; set; }
        DbSet<Plan_SMVDraftLayout> Plan_SMVDraftLayout { get; set; }
        DbSet<Plan_DraftOrderPlan> Plan_DraftOrderPlan { get; set; }
        DbSet<Plan_DraftLinePlan> Plan_DraftLinePlan { get; set; }
        DbSet<Plan_SMVMasterLayout> Plan_SMVMasterLayout { get; set; }
        DbSet<Plan_MasterOrderPlan> Plan_MasterOrderPlan { get; set; }
        DbSet<Plan_MasterLinePlan> Plan_MasterLinePlan { get; set; }
        DbSet<Plan_OrderAction> Plan_OrderAction { get; set; }
        DbSet<Plan_OrderActionStep> Plan_OrderActionStep { get; set; }
        DbSet<Plan_OrderProcess> Plan_OrderProcess { get; set; }

        //Production
        DbSet<Common_LineChief> Common_LineChief { get; set; }
        DbSet<Common_LineSuperVisor> Common_LineSuperVisor { get; set; }
        DbSet<Prod_Reference> Prod_Reference { get; set; }
        DbSet<Prod_ReferencePlan> Prod_ReferencePlan { get; set; }
        DbSet<Prod_ReferencePlanFollowup> Prod_ReferencePlanFollowup { get; set; }
        DbSet<Prod_ReferencePlanFollowupDetails> Prod_ReferencePlanFollowupDetails { get; set; }
        DbSet<Prod_PlanAchievment> Prod_PlanAchievment { get; set; }
        DbSet<Common_LineChiefLine> Common_LineChiefLine { get; set; }
        DbSet<Prod_Remarks> Prod_Remarks { get; set; }
        DbSet<Prod_DailyAchivement> Prod_DailyAchivement { get; set; }
        DbSet<Prod_QCType> Prod_QCType { get; set; }
        DbSet<Prod_QCOrder> Prod_QCOrder { get; set; }
        //Store
        DbSet<Store_General> Store_General { get; set; }
        DbSet<Store_Bin> Store_Bin { get; set; }
        DbSet<Store_StockIn> Store_StockIn { get; set; }
        DbSet<Store_StockRegister> Store_StockRegister { get; set; }
        DbSet<Store_StockOut> Store_StockOut { get; set; }
        DbSet<Store_Requisition> Store_Requisition { get; set; }
        DbSet<Store_RequisitionSlave> Store_RequisitionSlave { get; set; }
        DbSet<Store_Section> Store_Section { get; set; }
        DbSet<Store_StockOutPersonal> Store_StockOutPersonal { get; set; }
        DbSet<Store_StockInMaster> Store_StockInMaster { get; set; }
        DbSet<Store_StockInSlave> Store_StockInSlave { get; set; }
        DbSet<Store_StockOutMaster> Store_StockOutMaster { get; set; }
        DbSet<Store_StockOutSlave> Store_StockOutSlave { get; set; }
        DbSet<Store_QCInformation> Store_QCInformation { get; set; }
        DbSet<Store_AssetDepreciation> Store_AssetDepreciation { get; set; }
        DbSet<Store_ItemWiseConsumption> Store_ItemWiseConsumption { get; set; }
        DbSet<Store_ItemWiseConsumptionAllStyle> Store_ItemWiseConsumptionAllStyle { get; set; }  

        //HRMS
        DbSet<HRMS_BusinessUnit> HRMS_BusinessUnit { get; set; }
        DbSet<HRMS_Unit> HRMS_Unit { get; set; }
        DbSet<HRMS_Section> HRMS_Section { get; set; }
        DbSet<HRMS_Designation> HRMS_Designation { get; set; }
        DbSet<HRMS_Shift> HRMS_Shift { get; set; }
        DbSet<HRMS_ShiftAssign> HRMS_ShiftAssign { get; set; }
        DbSet<HRMS_EarnLeave> HRMS_EarnLeave { get; set; }
        DbSet<HRMS_Employee> HRMS_Employee { get; set; }
        DbSet<HRMS_EmployeeDependent> HRMS_EmployeeDependent { get; set; }
        DbSet<HRMS_EmployeeEducation> HRMS_EmployeeEducation { get; set; }
        DbSet<HRMS_EmployeeTraining> HRMS_EmployeeTraining { get; set; }
        DbSet<HRMS_EmployeeExperience> HRMS_EmployeeExperience { get; set; }
        DbSet<HRMS_EmployeeSkill> HRMS_EmployeeSkill { get; set; }
        DbSet<HRMS_EmployeeReference> HRMS_EmployeeReference { get; set; }
        DbSet<HRMS_OffDay> HRMS_OffDay { get; set; }
        DbSet<HRMS_Holiday> HRMS_Holiday { get; set; }
        DbSet<HRMS_EmployeeLeaveAssign> HRMS_EmployeeLeaveAssign { get; set; }
        DbSet<HRMS_Leave> HRMS_Leave { get; set; }
        DbSet<HRMS_LeaveApplication> HRMS_LeaveApplication { get; set; }
        DbSet<HRMS_EmployeeWeekendAssign> HRMS_EmployeeWeekendAssign { get; set; }
        DbSet<HRMS_EmployeeWeekend> HRMS_EmployeeWeekend { get; set; }
        DbSet<HRMS_District> HRMS_District { get; set; }
        DbSet<HRMS_PoliceStation> HRMS_PoliceStation { get; set; }
        DbSet<HRMS_PostOffice> HRMS_PostOffice { get; set; }
        DbSet<HRMS_JobDescription> HRMS_JobDescription { get; set; }

        DbSet<HRMS_AttendanceHistory> HRMS_AttendanceHistory { get; set; }
        DbSet<HRMS_AttendanceHistoryLog> HRMS_AttendanceHistoryLog { get; set; }
        DbSet<HRMS_AttendanceRemarks> HRMS_AttendanceRemarks { get; set; }
        DbSet<HRMS_CheckInOuts> HRMS_CheckInOuts { get; set; }
        DbSet<HRMS_EmployeeEarnLeaveCalculation> HRMS_EmployeeEarnLeaveCalculation { get; set; }
        DbSet<HRMS_EmployeeServiceBenifit> HRMS_EmployeeServiceBenifit { get; set; }
        DbSet<HRMS_CompanyHierarchyLabel> HRMS_CompanyHierarchyLabel { get; set; }
        DbSet<HRMS_EmployeeImage> HRMS_EmployeeImage { get; set; }
        DbSet<Common_CompanySetup> Common_CompanySetup { get; set; }
        DbSet<HRMS_EducationalDegree> HRMS_EducationalDegree { get; set; }
        DbSet<HRMS_EmployeeNominee> HRMS_EmployeeNominee { get; set; }

        //Payroll
        DbSet<Payroll_EODReference> Payroll_EODReference { get; set; }
        DbSet<Payroll_EODRecordMaster> Payroll_EODRecordMaster { get; set; }
        DbSet<Payroll_EODRecord> Payroll_EODRecord { get; set; }
        DbSet<Payroll_PayrollMaster> Payroll_PayrollMaster { get; set; }
        DbSet<Payroll_PayrollDetails> Payroll_PayrollDetails { get; set; }
        DbSet<Payroll_EmployeePromotionalHistory> Payroll_EmployeePromotionalHistory { get; set; }
        DbSet<Payroll_BonusSettings> Payroll_BonusSettings { get; set; }
        DbSet<Payroll_BonusMaster> Payroll_BonusMaster { get; set; }
        DbSet<Payroll_BonusDetails> Payroll_BonusDetails { get; set; }
        DbSet<Payroll_SalaryStructure> Payroll_SalaryStructure { get; set; }

        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken));
        Task<EntityEntry> AddAsync(object entity, CancellationToken cancellationToken = default(CancellationToken));
        int ExecuteSqlCommand(string command);
        Task<int> ExecuteSqlCommandAsync(RawSqlString command, params object[] parameters);

       

    }

    //public class ErpDbContext : DbContext
    //{

    //    public ErpDbContext(DbContextOptions<ErpDbContext> options) : base(options)
    //    {

    //    }


    //    public virtual DbSet<Accounting_Type> Accounting_Type { get; set; }
    //    public virtual DbSet<Accounting_Chart1> Accounting_Chart1 { get; set; }
    //    public virtual DbSet<Accounting_Chart2> Accounting_Chart2 { get; set; }
    //    public virtual DbSet<Accounting_Head> Accounting_Head { get; set; }
    //}
}
