﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Erp.Core.Services.Production
{
    public class VMOrderProcess : BaseVM
    {
        public bool IsTaken { get; set; }

        public int Plan_OrderActionFK { get; set; }

        public int Plan_OrderActionStepFK { get; set; }

        public int Merchandising_StyleFK { get; set; }

        public string ActionName { get; set; }

        public string ActionStepName { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime PlanStartDate { get; set; } //= DateTime.Now.ToString("yyyy-MM-dd");
        //public string PlanStartDateControlString { get { return this.PlanStartDate != DateTime.MinValue ? this.PlanStartDate.ToString("yyyy-MM-dd") : ""; } }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime PlanFinishDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime ActualStartDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime ActualFinishDate { get; set; }

        public DateTime ExtendDate { get; set; }

        public string AssignedTo { get; set; }
        public int ActionSequence { get; set; }
        public int StepSequence { get; set; }
    }
}
