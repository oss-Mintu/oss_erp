﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Erp.Core.Services.Production
{
    public class VMSupportPlan : BaseVM
    {
        public int LineID { get; set; }

        public string LineName { get; set; }

        public int StyleID { get; set; }

        public DateTime FromDate { get; set; }

        public DateTime ToDate { get; set; }

        public int PlanQty { get; set; }

        public int OccupiedQty { get; set; }

        public int WorkingHour { get; set; }

        public int HourOccupied { get; set; }

        public int HourTarget { get; set; }

        public int DayRange { get; set; }
    }
}
