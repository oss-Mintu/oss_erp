﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Erp.Core.Services.Production
{
    public class VMProductionReport : BaseVM
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }

        public DateTime ReferenceDate { get; set; }
        public string ProductionDate { get; set; }
        public int ProdReferenceId { get; set; }
        public int ProdReferencePlanId { get; set; }
        public int StyleID { get; set; }
        public int SectionId { get; set; }
        public int LineId { get; set; }
        public int ColorID { get; set; }
        public int SizeID { get; set; }
        public int RemarksID { get; set; }
        public string LineOutput { get; set; }
        public string BuyerName { get; set; }
        public string StyleName { get; set; }
        public string BuyerPO { get; set; }
        public string LineName { get; set; }
        public int PackQty { get; set; }
        public int QtyLineMachine { get; set; }
        public int QtyMachine { get; set; }

        public int QtyPresentOP { get; set; }

        public int QtyPresentHP { get; set; }

        public int QtyTotalWorker { get; set; }

        public int QtySizeMan { get; set; }

        public int QtyLineMan { get; set; }

        public int TotalMachineinLine { get; set; }

        public int QtyInputMan { get; set; }

        public decimal SMV { get; set; }

        public decimal GSM { get; set; }

        public int HourlyTarget { get; set; }

        public int WorkingHour { get; set; }

        public int TotalTarget { get; set; }

        public int OccupiedHour { get; set; }

        public int HourlyAchivement { get; set; }

        public int TotalAchivement { get; set; }

        public decimal TargetEffi { get; set; }

        public decimal AchiveEffi { get; set; }

        public int TargetBalance { get; set; }

        public string Remarks { get; set; }
        public int Quantity { get; set; }
        public int QtyLine { get; set; }
        public int TotalCutting { get; set; }
        public int TotalSewing { get; set; }
        public int TotalPacking { get; set; }
        public int TotalIroning { get; set; }
        public int TotalPreviousDone { get; set; }
        public int TotalCTN { get; set; }
        public decimal CuttingRate { get; set; }
        public decimal SewingRate { get; set; }
        public decimal IroningRate { get; set; }
        public decimal PackingRate { get; set; }
        public int PackPice { get; set; }
        public string ColorName { get; set; }
        public string Size { get; set; }
        public int CuttingBalance { get; set; }
        public int SewingBalance { get; set; }
        public int IronBalance { get; set; }
        public decimal PackingBalance { get; set; }
        public int DayCutting { get; set; }
        public int DaySewing { get; set; }
        public int DayIroning { get; set; }
        public int DayPacking { get; set; }
        public int Searchby { get; set; }
        public decimal DayCuttingWeight { get; set; }
        public int BalanceQty { get; set; }
        public decimal TotalWeight { get; set; }
        public decimal LayerWeight { get; set; }
        public decimal MarkerPic { get; set; }
        public decimal MarkerLength { get; set; }
        public decimal MarkerWidth { get; set; }
        public int ExtraQty { get; set; }
        public int ExtraRate { get; set; }
        public decimal Consumption { get; set; }
        public decimal TotalStoreReceived { get; set; }
        public decimal StoreBalance { get; set; }
        public decimal BookingConsump { get; set; }
        public decimal CuttingConsump { get; set; }
        public decimal TotalCuttingReceived { get; set; }
        public decimal TotalFabricUsed { get; set; }
        public decimal FabricBalance { get; set; }
        public decimal TotalBalance { get; set; }
        public int PlanQty { get; set; }
        public int TargetQty { get; set; }
        public int AchiveQty { get; set; }


        [DisplayName("Production Unit")]
        public int ProductionUnitID { get; set; }
        [DisplayName("Production Floor")]
        public int ProductionFloorID { get; set; }

        //public List<VMColorSize> vmColorSizeSummary { get; set; }
        public string Subset { get; set; }
        public int SetPackFK { set; get; }
        public List<VMProductionReport> DataList { get; set; }
    }
}
