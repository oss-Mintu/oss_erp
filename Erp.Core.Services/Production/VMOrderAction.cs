﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Erp.Core.Services.Production
{
    public class VMOrderAction : BaseVM
    {
        public string Name { get; set; }

        public int ActionRange { get; set; }

        public int Priority { get; set; }

        public bool IsStop { get; set; }

        public string BtnLavel { get; set; }

        public IEnumerable<VMOrderAction> DataList { get; set; }
    }
}
