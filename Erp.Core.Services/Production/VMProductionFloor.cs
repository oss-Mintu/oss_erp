﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Erp.Core.Services.Production
{
    public class VMProductionFloor:BaseVM
    {

        public string Name { get; set; }
        //public string Code { get; set; }
        public string Remarks { get; set; }
        public bool IsColsed { get; set; }
        public string ProductionUnit { get; set; }
        public int ProductionUnitFK { get; set; }

        public List<VMProductionFloor> DataList { get; set; }
    }
}
