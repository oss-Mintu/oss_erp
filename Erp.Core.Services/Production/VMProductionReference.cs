﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Erp.Core.Services.Production
{
    public class VMProductionReference : BaseVM
    {
        [Display(Name = "Reference No")]
        public string ReferenceNo { get; set; }

        [Display(Name = "Reference Date"), DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime ReferenceDate { get; set; }

        public string Remarks { get; set; }

        public int SectionID { get; set; }

        public IEnumerable<VMProductionReference> DataList { get; set; }

        public List<VMLinePlanHourlyChart> LinePlanList { get; set; }

        public VMDailyProduction vmDailyProduction { get; set; }

        public List<VMDailyProduction> PlanList { get; set; }
    }
}
