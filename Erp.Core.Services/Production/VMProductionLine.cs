﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Erp.Core.Services.Production
{
    public class VMProductionLine : BaseVM
    {
        //[Required]
        //[DisplayName("Line Code")]
        //[StringLength(20, ErrorMessage = "Upto 20 Chracter")]
        //public string Code { get; set; }

        [Required]
        [DisplayName("Line Name")]
        [StringLength(20, ErrorMessage = "Upto 20 Chracter")]
        public string Name { get; set; }

        [DisplayName("Line Priority")]
        public int Priority { get; set; }

        [DisplayName("Description")]
        [StringLength(200, ErrorMessage = "Upto 200 Chracter")]
        public string Description { get; set; }

        [DisplayName("Step Name")]
        public string Step { get; set; }

        public int Plan_ProductionStepFK { get; set; }
        [DisplayName("Production Unit")]
        public string ProductionUnit { get; set; }

        [DisplayName("Production Floor")]
        public string ProductionFloor { get; set; }

        [DisplayName("Unit")]
        public string Unit { get; set; }

        [DisplayName("Section Line")]
        public string SectionLine { get; set; }

        public int Common_SectionLineFK { get; set; }
        public int ProductionUnitFK { get; set; }
        public int ProductionFloorFK { get; set; }

        public List<VMProductionLine> DataList { get; set; }
    }
}
