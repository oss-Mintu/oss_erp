﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Erp.Core.Services.Production
{
    public class VMOrderActionStep : BaseVM
    {
        public int Plan_OrderActionFK { get; set; }

        public string OrderActionName { get; set; }

        public int OrderActionRange { get; set; }

        public string Name { get; set; }

        public int Priority { get; set; }

        public int DefaultRange { get; set; }

        public string SetColor { get; set; }

        public int ExtendedRange { get; set; }

        public string ExtendColor { get; set; }

        public string StepCode { get; set; }

        public string BtnLavel { get; set; }

        public List<VMOrderActionStep> DataList { get; set; }

        public List<VMOrderAction> ActionDataList { get; set; }
    }
}
