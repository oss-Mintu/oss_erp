﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Erp.Core.Services.Production
{
    public class VMProductionUnit:BaseVM
    {
        public string Name { get; set; }
       // public string Code { get; set; }
        public bool IsColsed { get; set; }
        public string Remarks { get; set; }
        public string BusinessUnit { get; set; }
        public int HRMS_UnitFK { get; set; }
       
        public List<VMProductionUnit> DataList { get; set; }

    }
}
