﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Erp.Core.Services.Production
{
    public class VMOrderPlanning : BaseVM
    {
        public string OrderNumber { get; set; }

        public int Merchandising_StyleFK { get; set; }

        public int Plan_DraftOrderPlanFK { get; set; }

        public int Merchandising_StyleShipmentScheduleFK { get; set; }

        public int Merchandising_SetPackFK { get; set; }
        public int SetPackQty { get; set; }
        public int PackPieceQty { get; set; }

        public string StyleNo { get; set; }

        public string ItemName { get; set; }

        [DisplayName("Order Plan Qty"), Required(ErrorMessage = "Order plan quantity is required.")]
        public decimal PlanQty { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Shipment Date")]
        public DateTime ShipmentDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Sample Approved Date")]
        public DateTime SampleApprovedDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Fabric Inhouse Date")]
        public DateTime FabricInhouseDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Accessories Inhouse Date")]
        public DateTime AccessoriesInhouseDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Cutting Start Date")]
        public DateTime CuttingStartDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Sewing Start Date")]
        public DateTime SewingStartDate { get; set; }
        [DisplayName("Extra Cutting")]
        public decimal ExtraRate { get; set; }
        public string Remarks { get; set; }

        public int OrderPlanQty { get; set; }

        public int OrderPlanAchiveQty { get; set; }

        public bool IsClosed { get; set; }

        public bool IsPaused { get; set; }

        public bool IsReference { get; set; }

        public List<VMOrderPlanning> PlanList { get; set; }

        public List<VMOrderPlanning> DataList { get; set; }

        public List<VMLinePlanning> LinePlanList { get; set; }
    }
}
