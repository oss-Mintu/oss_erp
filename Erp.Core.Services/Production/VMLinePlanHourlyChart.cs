﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Erp.Core.Services.Production
{
    public class VMLinePlanHourlyChart :BaseVM
    {
        public int LineID { get; set; }

        public string LineName { get; set; }

        public string Hour { get; set; }

        public string DateTitle { get; set; }

        public int LineWorkingHour { get; set; }

        public DateTime PlanDate { get; set; }

        public string DayColor { get; set; }

        public List<VMLinePlanHourlyChart> HourList { get; set; }
    }
}
