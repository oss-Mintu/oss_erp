﻿using Erp.Core.Services.Merchandising;
using System;
using System.Collections.Generic;
using System.Text;

namespace Erp.Core.Services.Production
{
    public class VMPlanView : BaseVM
    {
        public VMStyle VmStyle { get; set; }

        public List<VMOrderPlanning> ListOrderPlan { get; set; }

        public VMOrderPlanning vmOrderPlanning { get; set; }

        public VMSMVLayout vmSMVLayout { get; set; }

        public VMShipmentProductionPlan vmProductionPlan { get; set; }
    }
}
