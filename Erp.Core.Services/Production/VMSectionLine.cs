﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Erp.Core.Services.Production
{
    public class VMSectionLine:BaseVM
    {
        public string Name { get; set; }
     
        //public string Code { get; set; }
        public int BuildingFk { get; set; }
        public int FloorFk { get; set; }
        public string Building { get; set; }
        public string Floor{ get; set; }
        public int HRMS_UnitFK { get; set; }
        public string Unit { get; set; }
        public bool IsClosed { get; set; }

        public List<VMSectionLine> Datalist { get; set; }

    }
}
