﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Erp.Core.Services.Production
{
    public class VMLineSuperVisor:BaseVM
    {
        public string Name { get; set; }
        public bool IsReference { get; set; }
        public List<VMLineSuperVisor> DataList { get; set; }
    }
}
