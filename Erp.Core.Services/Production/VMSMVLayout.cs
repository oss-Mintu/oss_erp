﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Erp.Core.Services.Production
{
    public class VMSMVLayout: BaseVM
    {
        public int PLan_SMVDraftLayoutFK { get; set; }

        [DisplayName("StyleFk")]
        public int Merchandising_StyleFk { get; set; }

        public int Merchandising_StyleSetPackFK { get; set; }

        public string Style { get; set; }

        public string Subset { get; set; }

        [DisplayName("Total Operator")]
        public int NoOfOperator { get; set; }

        [DisplayName("Total Helper")]
        public int NoOfHelper { get; set; }

        [DisplayName("Operator SMV")]
        public decimal OperatorSMV { get; set; }

        [DisplayName("Helper SMV")]
        public decimal HelperSMV { get; set; }

        [DisplayName("Total Worker")]
        public int TotalWorker { get; set; }

        [DisplayName("SMV")]
        public decimal TotalSMV { get; set; }

        [DisplayName("Working Hour")]
        public int WorkingHour { get; set; }

        [DisplayName("Hour Target")]
        public decimal PerHourTarget { get; set; }

        [DisplayName("Target Efficiency")]
        public decimal Efficiency { get; set; }

        [DisplayName("Description")]
        [StringLength(500, ErrorMessage = "Upto 500 Chracter")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        public List<VMSMVLayout> DataList { get; set; }
    }
}
