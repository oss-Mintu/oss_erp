﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Erp.Core.Services.Production
{
    public class VMOrderStepReport : BaseVM
    {
        public int Merchandising_StyleFK { get; set; }

        public int Plan_OrderActionFK { get; set; }

        public int Plan_OrderActionStepFK { get; set; }

        public string Style { get; set; }

        public string StepName { get; set; }

        public string ViewName { get; set; }

        public string DayColor { get; set; }

        public string HeadDate { get; set; }

        public int ColSpan { get; set; }

        public DateTime PlanStartDate { get; set; }

        public DateTime PlanFinishDate { get; set; }

        public DateTime ActualStartDate { get; set; }

        public DateTime ActualFinishDate { get; set; }

        public List<VMOrderStepReport> DataList { get; set; }
    }
}
