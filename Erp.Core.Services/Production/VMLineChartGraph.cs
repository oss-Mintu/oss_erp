﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Erp.Core.Services.Production
{
   public class VMLineChartGraph 
   {
        public string[] labels { get; set; }
        public List<datasets> datasets { get; set; }

   }

    public class datasets
    {
        public string label { get; set; }

        public bool fill { get; set; }
        public string backgroundColor { get; set; }

        public string borderColor { get; set; }

        //public int[] data { get; set; }
        public List<int> data { get; set; }
    }
}
