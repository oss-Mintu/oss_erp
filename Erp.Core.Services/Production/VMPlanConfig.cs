﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Erp.Core.Services.Production
{
    public class VMPlanConfig : BaseVM
    {
        [DisplayName("Cutting start lead days considering shipment day")]
        public int CuttingBFShip { get; set; }

        [DisplayName("Fabric inhouse lead days considering cutting day")]
        public int FabricINBFCutting { get; set; }

        [DisplayName("Accessories inhouse lead days considering cutting day")]
        public int AccessoriesINBFCutting { get; set; }

        [DisplayName("PPSample approved lead days considering cutting day")]
        public int PPSampleBFCutting { get; set; }

        [DisplayName("Sewing start lead days considering cutting day")]
        public int SewingAFCutting { get; set; }

        [DisplayName("Reserved lead days considering shipment day")]
        public int ReservedDay { get; set; }

        [DisplayName("Allow Extra Cutting Rate")]
        public decimal ExtraRate { get; set; }

        public bool IsRunning { get; set; }
    }
}
