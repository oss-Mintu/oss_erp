﻿using Erp.Core.Entity.Common;
using Erp.Core.Entity.Production;
using Erp.Core.Services.Home;
using Erp.Core.Services.Integration;
using Erp.Core.Services.Merchandising;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Erp.Core.Services.Production
{
    public class ProductionService : BaseService
    {
        private readonly IntegrationService integrationService;
        public ProductionService(IErpDbContext db, HttpContext httpContext)
        {
            _db = db;
            CidServices = new CidServices(db);
            integrationService = new IntegrationService(db, httpContext);
        }

        #region ProductionReference
        public async Task<VMProductionReference> ProdReferenceGet()
        {
            var vData = _db.Prod_Reference.Where(a => a.Active == true).Select(a => new VMProductionReference
            {
                ID = a.ID,
                ReferenceDate = a.ReferenceDate,
                Remarks = a.Remarks,
                ReferenceNo = a.ReferenceNo
            }).OrderByDescending(a => a.ReferenceDate).AsEnumerable();

            VMProductionReference vMReference = new VMProductionReference
            {
                DataList = await Task.Run(() => vData)
            };

            return vMReference;
        }
        public async Task<int> ProdReferenceAdd(VMProductionReference vMRef)
        {
            var result = -1;
            var CheckRef = _db.Prod_Reference.Any(a => a.ReferenceDate == vMRef.ReferenceDate && a.Active == true);
            if (!CheckRef)
            {
                Prod_Reference pRef = new Prod_Reference();
                pRef.ReferenceNo = CidServices.ProductionCID();
                pRef.ReferenceDate = vMRef.ReferenceDate;
                pRef.Remarks = vMRef.Remarks;
                pRef.Active = true;

                _db.Prod_Reference.Add(pRef);

                if (await _db.SaveChangesAsync() > 0)
                {
                    result = pRef.ID;
                }
            }
            return result;
        }
        public async Task<int> ProdReferenceEdit(VMProductionReference vMRef)
        {
            var result = -1;
            Prod_Reference pObj = _db.Prod_Reference.Find(vMRef.ID);
            pObj.Remarks = vMRef.Remarks;
            pObj.Active = true;

            if (await _db.SaveChangesAsync() > 0)
            {
                result = pObj.ID;
            }
            return result;
        }
        public async Task<int> ProdReferenceDelete(int ID)
        {
            var result = -1;
            Prod_Reference pObj = _db.Prod_Reference.Find(ID);
            if (pObj != null)
            {
                pObj.Active = false;
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = pObj.ID;
                }
            }
            return result;
        }
        public async Task<VMProductionReference> ProdReferenceGetByID(int ID)
        {
            var vData = _db.Prod_Reference.Where(a => a.ID == ID).Select(a => new VMProductionReference
            {
                ID = a.ID,
                ReferenceDate = a.ReferenceDate,
                Remarks = a.Remarks,
                ReferenceNo = a.ReferenceNo
            }).FirstOrDefault();

            return await Task.Run(() => vData);
        }
        #endregion

        #region ProductionStep

        #region CuttingStep
        public async Task<VMDailyProduction> CuttingDailyPlanGetByRefID(int ID)
        {
            var vData = (from t1 in _db.Prod_ReferencePlan
                         join t2 in _db.Merchandising_Style on t1.Merchandising_StyleFK equals t2.ID
                         join t3 in _db.Merchandising_BuyerOrder on t2.Merchandising_BuyerOrderFK equals t3.ID
                         join t4 in _db.Common_FinishItem on t2.Common_FinishItemFK equals t4.ID
                         join t6 in _db.Common_Color on t1.Common_ColorFK equals t6.ID
                         where t1.Prod_ReferenceFK == ID && t1.Active == true && t1.SectionId == 1
                         select new VMDailyProduction
                         {
                             Prod_ReferencePlanFK = t1.ID,
                             Merchandising_StyleFK = t1.Merchandising_StyleFK,
                             OrderNo = t3.BuyerPO + "/" + t4.Name,
                             Prod_ReferenceFK = t1.Prod_ReferenceFK,
                             Common_ColorFK = (int)t1.Common_ColorFK,
                             Color = t6.Name,
                             TargetQty = t1.PlanQuantity,
                             CuttingExtra = t1.ExtraCutting,
                             GSM = t1.GSMORSMV,
                             Layer=t1.FabricLayer,
                             Code=t1.CID
                         }).OrderBy(x => x.Common_ColorFK).ToList();

            if (vData.Any())
            {
                foreach (var v in vData)
                {
                    
                    v.DoneQty = GetDoneQtyByColorWise(v.Prod_ReferenceFK, v.Common_ColorFK, 1,v.Merchandising_StyleFK);
                    v.OrderQty = GetTotalOrderQtyByColorID(v.Merchandising_StyleFK, v.Common_ColorFK);
                    v.ExtraQty = (int)decimal.Round(decimal.Multiply(decimal.Divide(v.CuttingExtra, (decimal)100), (decimal)v.OrderQty));
                    v.RemainQty = (v.OrderQty + v.ExtraQty) - v.DoneQty;
                }
            }

            VMDailyProduction vMReference = new VMDailyProduction
            {
                DataList = await Task.Run(() => vData)
            };

            return vMReference;
        }

        public async Task<VMDailyProduction> CuttingDailyFollowupGetByRefID(int ID)
        {
            var vData = (from t1 in _db.Prod_ReferencePlanFollowup
                         join t2 in _db.Prod_ReferencePlan on t1.Prod_ReferencePlanFK equals t2.ID
                         join t3 in _db.Merchandising_StyleShipmentSchedule on t1.Merchandising_StyleShipmentScheduleFK equals t3.ID
                         join t4 in _db.Merchandising_StyleShipmentRatio on t1.Merchandising_StyleShipmentRatioFK equals t4.ID
                         join t5 in _db.Merchandising_StyleMeasurement on t4.Merchandising_StyleMeasurementFK equals t5.ID
                         join t6 in _db.Common_Color on t5.Common_ColorFK equals t6.ID
                         join t7 in _db.Common_Size on t5.Common_SizeFK equals t7.ID
                         join t8 in _db.Merchandising_Style on t3.Merchandising_StyleFK equals t8.ID
                         join t9 in _db.Merchandising_BuyerOrder on t8.Merchandising_BuyerOrderFK equals t9.ID
                         join t10 in _db.Common_Country on t3.Common_CountryFK equals t10.ID
                         where t2.Prod_ReferenceFK == ID && t1.Active == true && t2.SectionId == 1
                         select new VMDailyProduction
                         {
                             ID = t1.ID,
                             Prod_ReferencePlanFK = t2.ID,
                             Prod_ReferenceFK = t2.Prod_ReferenceFK,
                             Merchandising_StyleFK = t3.Merchandising_StyleFK,
                             StyleShipmentRatioFK = (int)t1.Merchandising_StyleShipmentRatioFK,
                             StyleShipmentScheduleFK = (int)t1.Merchandising_StyleShipmentScheduleFK,
                             Merchandising_StyleSetPackFK=t2.Merchandising_StyleSetPackFK!=null?(int)t2.Merchandising_StyleSetPackFK:0,
                             Port = t10.Name + "-" + t3.DestNo,
                             OrderNo = t9.BuyerPO + "/" + t8.StyleName,
                             Color = t6.Name,
                             Size = t7.Name,
                             Common_SizeFK= t5.Common_SizeFK,
                             Common_ColorFK= t5.Common_ColorFK,
                             OrderQty = (int)decimal.Multiply(t4.Quantity,t8.PackPieceQty),
                             DoneQty = (int)t1.Quantity,
                             CuttingExtra = t2.ExtraCutting,
                             GSM = t2.GSMORSMV,
                             Layer=t2.FabricLayer,
                             EntryDate = t3.ShipmentDate
                         }).OrderBy(x => x.Prod_ReferenceFK).ToList();
            List<VMDailyProduction> lst = new List<VMDailyProduction>();
            if (vData.Any())
            {
                foreach (var v in vData)
                {
                    
                    v.ExtraQty = (int)decimal.Round(decimal.Multiply(decimal.Divide(v.CuttingExtra, (decimal)100), (decimal)v.OrderQty));
                    //v.OrderQty = TotalOrderQtyByColorID(v.Merchandising_StyleFK, v.Common_ColorFK);
                    v.RemainQty = (v.OrderQty + v.ExtraQty) - GetDoneQtyBySizeWise(v.Prod_ReferenceFK, v.Merchandising_StyleFK, v.Merchandising_StyleSetPackFK, v.StyleShipmentRatioFK, v.Common_ColorFK, v.Common_SizeFK, 1);
                    //if (v.RemainQty>0)
                    //{
                    //    lst.Add(v);
                    //}
                   
                }
            }

            VMDailyProduction vMReference = new VMDailyProduction
            {
                DataList = await Task.Run(() => vData)
            };

            return vMReference;
        }

        public async Task<VMProductionReference> CuttingProductionGetByRefID(int ID)
        {
            VMProductionReference vModel = new VMProductionReference();
            vModel.vmDailyProduction = new VMDailyProduction();
            vModel = await ProdReferenceGetByID(ID);
            vModel.vmDailyProduction = await CuttingDailyPlanGetByRefID(ID);
            return vModel;
        }

        public async Task<VMProductionReference> CuttingFollowupGetByRefID(int ID)
        {
            VMProductionReference vModel = new VMProductionReference();
            vModel.vmDailyProduction = new VMDailyProduction();
            vModel = await ProdReferenceGetByID(ID);
            vModel.vmDailyProduction = await CuttingDailyFollowupGetByRefID(ID);
            return vModel;
        }

        public VMDailyProduction CuttingPlanGetByID(int pid)
        {
            var getPlanData = (from t1 in _db.Prod_ReferencePlan
                               join t2 in _db.Merchandising_Style on t1.Merchandising_StyleFK equals t2.ID
                               join t3 in _db.Merchandising_BuyerOrder on t2.Merchandising_BuyerOrderFK equals t3.ID
                               join t4 in _db.Common_ProductionLine on t1.ProductionLineFK equals t4.ID into Line_Join
                               from t5 in Line_Join.DefaultIfEmpty()
                               join t6 in _db.Common_LineChief on t1.Common_LineChiefFK equals t6.ID into Chief_Join
                               from t7 in Chief_Join.DefaultIfEmpty()
                               join t8 in _db.Common_LineSuperVisor on t1.Common_SuperVisorFK equals t8.ID into SV_Join
                               from t9 in SV_Join.DefaultIfEmpty()
                               join t10 in _db.Merchandising_StyleSetPack on t1.Merchandising_StyleSetPackFK equals t10.ID into Subset_Join
                               from t11 in Subset_Join.DefaultIfEmpty()
                               join t12 in _db.Common_Color on t1.Common_ColorFK equals t12.ID into Color_Join
                               from t13 in Color_Join.DefaultIfEmpty()
                               where t1.ID == pid
                               select new VMDailyProduction
                               {
                                   Prod_ReferencePlanFK = t1.ID,
                                   Prod_ReferenceFK = t1.Prod_ReferenceFK,
                                   Plan_ProductionLineFk = t1.ProductionLineFK??0,
                                   Merchandising_StyleFK = t1.Merchandising_StyleFK,
                                   Style = t3.BuyerPO + "/" + t2.StyleName,
                                   LineName = t5 != null ? t5.Code + "-" + t5.Name : "",
                                   SubSetName = t11 != null ? t11.SetPackName : "",
                                   LineChiefSupervisor = t7 != null ? t7.Name : "",
                                   SuperVisor = t9 != null ? t9.Name : "",
                                   Color = t13 != null ? t13.Name : "",
                                   CuttingExtra = t1.ExtraCutting,
                                   Layer = t1.FabricLayer,
                                   PerLayerWeight = t1.LayerWeight,
                                   MarkerPiece = t1.MarkerPiece,
                                   MarkerLength = t1.MarkerLength,
                                   MarkerWidth = t1.MarkerWidth,
                                   GSM = t1.GSMORSMV,
                                   CuttingNumber = t1.CuttingNo,
                                   Remarks = t1.Remarks,
                                   ActionId = (int)ActionEnum.Edit,
                                   SectionID = (int)EnumProcess.Cutting,
                               }).FirstOrDefault();

            return getPlanData;
        }

        public async Task<int> CuttingStyleAdd(VMDailyProduction model)
        {
            int result = -1;
            model.ProductionDate = _db.Prod_Reference.FirstOrDefault(a => a.ID == model.Prod_ReferenceFK).ReferenceDate;

            //DeleteZeroPlanOrder(model.Prod_PlanReferenceOrder.Prod_ReferenceFK, 1);
            Prod_ReferencePlan cuttingRef = new Prod_ReferencePlan();
            cuttingRef.Prod_ReferenceFK = model.Prod_ReferenceFK;
            cuttingRef.CID = CidServices.ProductionSectionCID((int)EnumProcess.Cutting, model.ProductionDate, model.Prod_ReferenceFK);
            cuttingRef.CuttingNo = model.CuttingNumber;
            cuttingRef.ProductionDate = model.ProductionDate;
            cuttingRef.Merchandising_StyleFK = model.Merchandising_StyleFK;
            cuttingRef.Merchandising_StyleSetPackFK = model.Merchandising_StyleSetPackFK;
            cuttingRef.Common_ColorFK = model.Common_ColorFK;
            cuttingRef.ProductionLineFK = model.Plan_ProductionLineFk;
            cuttingRef.Common_LineChiefFK = model.Prod_LineChiefFK;
            cuttingRef.Common_SuperVisorFK = model.Prod_SuperVisorFK;
            cuttingRef.PlanQuantity = model.TargetQty;
            cuttingRef.ExtraCutting = model.CuttingExtra;
            cuttingRef.GSMORSMV = model.GSM;
            cuttingRef.FabricLayer = model.Layer;
            cuttingRef.LayerWeight = model.PerLayerWeight;
            cuttingRef.MarkerPiece = model.MarkerPiece;
            cuttingRef.MarkerLength = model.MarkerLength;
            cuttingRef.MarkerWidth = model.MarkerWidth;
            cuttingRef.Remarks = model.Remarks;
            cuttingRef.SectionId = (int)EnumProcess.Cutting;
            cuttingRef.Active = true;
            _db.Prod_ReferencePlan.Add(cuttingRef);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = cuttingRef.ID;
            }
            return result;
        }

        public async Task<int> CuttingStyleEdit(VMDailyProduction model)
        {
            var result = -1;
            Prod_ReferencePlan cuttingRef = _db.Prod_ReferencePlan.Find(model.Prod_ReferencePlanFK);
            cuttingRef.CuttingNo = model.CuttingNumber;
            cuttingRef.ExtraCutting = model.CuttingExtra;
            cuttingRef.GSMORSMV = model.GSM;
            cuttingRef.FabricLayer = model.Layer;
            cuttingRef.LayerWeight = model.PerLayerWeight;
            cuttingRef.MarkerPiece = model.MarkerPiece;
            cuttingRef.MarkerLength = model.MarkerLength;
            cuttingRef.MarkerWidth = model.MarkerWidth;
            cuttingRef.Remarks = model.Remarks;
            cuttingRef.Active = true;
            if (await _db.SaveChangesAsync() > 0)
            {
                result = cuttingRef.Prod_ReferenceFK;
            }
            return result;
        }

        public async Task<int> CuttingFollowUpStyleAdd(VMDailyProduction model)
        {
            int result = -1;
            //DeleteZeroPlanOrder(model.Prod_PlanReferenceOrder.Prod_ReferenceFK, 1);

            var vData = (from t1 in _db.Merchandising_StyleShipmentSchedule
                         join t4 in _db.Merchandising_StyleShipmentRatio on t1.ID equals t4.Merchandising_StyleShipmentScheduleFK
                         join t5 in _db.Merchandising_StyleMeasurement on t4.Merchandising_StyleMeasurementFK equals t5.ID
                         join t6 in _db.Merchandising_Style on t1.Merchandising_StyleFK equals t6.ID
                         where t1.Active == true && t5.Common_ColorFK == model.Common_ColorFK
                         && t1.Merchandising_StyleFK == model.Merchandising_StyleFK && t4.Active == true
                         select new { t1, t4, t6 }).AsEnumerable();

            if (vData.Any())
            {
                foreach (var v in vData)
                {
                    Prod_ReferencePlanFollowup cuttingFollow = new Prod_ReferencePlanFollowup();
                    cuttingFollow.Prod_ReferencePlanFK = model.Prod_ReferencePlanFK;
                    cuttingFollow.Merchandising_StyleShipmentScheduleFK = v.t1.ID;
                    cuttingFollow.Merchandising_StyleShipmentRatioFK = v.t4.ID;
                    cuttingFollow.Quantity = (int)decimal.Multiply(model.DoneQty, v.t6.PackPieceQty);
                    cuttingFollow.Active = true;
                    _db.Prod_ReferencePlanFollowup.Add(cuttingFollow);
                }
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = 1;
                }
            }
            return result;
        }
        #endregion

        #region SewingStep
        public async Task<VMDailyProduction> SewingDailyPlanGetByRefID(int ID)
        {
            var vData = (from t1 in _db.Prod_ReferencePlan
                         join t2 in _db.Merchandising_Style on t1.Merchandising_StyleFK equals t2.ID
                         join t3 in _db.Merchandising_BuyerOrder on t2.Merchandising_BuyerOrderFK equals t3.ID
                         join t6 in _db.Common_ProductionLine on t1.ProductionLineFK equals t6.ID
                         where t1.Prod_ReferenceFK == ID && t1.Active == true && t1.SectionId == 2
                         select new VMDailyProduction
                         {
                             Code = t1.CID,
                             Prod_ReferenceFK = t1.Prod_ReferenceFK,
                             Prod_ReferencePlanFK = t1.ID,
                             Merchandising_StyleFK = t1.Merchandising_StyleFK,
                             Plan_ProductionLineFk = (int)t1.ProductionLineFK,
                             OrderNo = t3.BuyerPO + "/" + t2.StyleName,
                             LineTable=t6.Name,
                             SMV = t1.GSMORSMV,
                             TargetQty = t1.PlanQuantity,
                             NoOfHelper = t1.PresentHelper,
                             PresentOpt = t1.PresentOperator,
                             WorkingHour = t1.WorkingHour,
                             CuttingExtra = t1.ExtraCutting,
                             GSM = t1.GSMORSMV,
                             MachineRunning = t1.MachineRunning,
                             LineInputQty = t1.LineInputQuantity,
                             Remarks = t1.Remarks,
                             Prod_LineChiefFK = t1.Common_LineChiefFK == null ? 0 : (int)t1.Common_LineChiefFK,
                             Prod_SuperVisorFK = t1.Common_SuperVisorFK == null ? 0 : (int)t1.Common_SuperVisorFK,
                         }).OrderBy(x => x.Common_ColorFK).ToList();

            if (vData.Any())
            {
                foreach (var v in vData)
                {
                    v.LineChiefSupervisor = GetLineChiefSVName((int)v.Prod_LineChiefFK, (int)v.Prod_SuperVisorFK);
                    v.DoneQty = GetDoneQtyBySectionID(v.Prod_ReferenceFK, v.Merchandising_StyleFK, 2);
                    v.OrderQty = GetTotalOrderQtyByStyleID(v.Merchandising_StyleFK);
                    v.RemainQty = v.OrderQty - v.DoneQty;
                }
            }

            VMDailyProduction vMReference = new VMDailyProduction
            {
                DataList = await Task.Run(() => vData)
            };

            return vMReference;
        }

        public async Task<VMDailyProduction> SewingDailyFollowupGetByRefID(int ID)
        {
            var vData = (from t1 in _db.Prod_ReferencePlan    
                         join t2 in _db.Prod_ReferencePlanFollowup on t1.ID equals t2.Prod_ReferencePlanFK
                         join t3 in _db.Merchandising_StyleShipmentSchedule on t2.Merchandising_StyleShipmentScheduleFK equals t3.ID
                         join t4 in _db.Merchandising_StyleShipmentRatio on t2.Merchandising_StyleShipmentRatioFK equals t4.ID
                         join t5 in _db.Merchandising_StyleMeasurement on t4.Merchandising_StyleMeasurementFK equals t5.ID
                         join t6 in _db.Common_Color on t5.Common_ColorFK equals t6.ID
                         join t7 in _db.Common_Size on t5.Common_SizeFK equals t7.ID
                         join t8 in _db.Merchandising_Style on t3.Merchandising_StyleFK equals t8.ID
                         join t9 in _db.Merchandising_BuyerOrder on t8.Merchandising_BuyerOrderFK equals t9.ID
                         join t10 in _db.Common_Country on t3.Common_CountryFK equals t10.ID
                         where t1.Prod_ReferenceFK == ID && t1.Active == true && t1.SectionId == (int)EnumProcess.Sewing && t2.Active==true
                         select new VMDailyProduction
                         {
                             ID=t2.ID,
                             Prod_ReferencePlanFK = t1.ID,
                             Prod_ReferenceFK = t1.Prod_ReferenceFK,
                             Merchandising_StyleFK = t3.Merchandising_StyleFK,
                             StyleShipmentRatioFK = (int)t2.Merchandising_StyleShipmentRatioFK,
                             StyleShipmentScheduleFK = (int)t2.Merchandising_StyleShipmentScheduleFK,
                             Plan_ProductionLineFk=(int)t1.ProductionLineFK,
                             Common_ColorFK= t5.Common_ColorFK,
                             Common_SizeFK= t5.Common_SizeFK,
                             Merchandising_StyleSetPackFK =t1.Merchandising_StyleSetPackFK!=null?(int)t1.Merchandising_StyleSetPackFK:0,
                             Port = t10.Name+"-"+ t3.DestNo,
                             OrderNo = t9.BuyerPO + "/" + t8.StyleName,
                             Color = t6.Name,
                             Size = t7.Name,
                             OrderQty = (int)decimal.Multiply(t4.Quantity, t8.PackPieceQty),
                             DoneQty = (int)t2.Quantity,
                             EntryDate = t3.ShipmentDate
                         }).OrderBy(x => x.Prod_ReferenceFK).ToList();

            if (vData.Any())
            {
                foreach (var v in vData)
                {
                    v.LineName = GetLineName(v.Plan_ProductionLineFk);
                    v.RemainQty = v.OrderQty - GetDoneQtyBySizeWise(v.Prod_ReferenceFK, v.Merchandising_StyleFK, v.Merchandising_StyleSetPackFK, v.StyleShipmentRatioFK, v.Common_ColorFK, v.Common_SizeFK, 2);
                }
            }

            VMDailyProduction vMReference = new VMDailyProduction
            {
                DataList = await Task.Run(() => vData)
            };

            return vMReference;
        }

        private string GetLineName(int LineID)
        {
            string name = string.Empty;
            var vData = ProductionLineGetByProcessID(2).Where(a=>a.ID==LineID);
            if (vData.Any())
            {
                name = vData.FirstOrDefault().Name;
            }
            return name;
        }

        public async Task<VMProductionReference> SewingFollowupGetByRefID(int ID)
        {
            VMProductionReference vModel = new VMProductionReference();
            vModel.vmDailyProduction = new VMDailyProduction();
            vModel = await ProdReferenceGetByID(ID);
            vModel.vmDailyProduction = await SewingDailyFollowupGetByRefID(ID);
            return vModel;
        }

        public async Task<VMProductionReference> SewingProductionGetByRefID(int ID)
        {
            VMProductionReference vModel = new VMProductionReference();
            vModel.vmDailyProduction = new VMDailyProduction();
            vModel = await ProdReferenceGetByID(ID);
            vModel.vmDailyProduction = await SewingDailyPlanGetByRefID(ID);
            return vModel;
        }

        public string GetLineChiefSVName(int ChiefID, int SVID)
        {
            string Name = string.Empty;
            var vChief = _db.Common_LineChief.Where(a => a.ID == ChiefID);
            if (vChief.Any())
            {
                Name = vChief.FirstOrDefault().Name + "|";
            }
            var SV = _db.Common_LineSuperVisor.Where(a => a.ID == SVID);
            if (SV.Any())
            {
                Name += SV.FirstOrDefault().Name;
            }
            return Name;
        }

        public async Task<int> SewingStyleAdd(VMDailyProduction model)
        {
            int result = -1;
            model.ProductionDate = _db.Prod_Reference.FirstOrDefault(a => a.ID == model.Prod_ReferenceFK).ReferenceDate;
            var vData = _db.Plan_SMVMasterLayout.Find(model.Plan_SMVMasterLayoutFK);
            model.SMV = vData.TotalSMV;
            model.TargetQty = (int)decimal.Multiply(model.WorkingHour, vData.PerHourTarget);

            //DeleteZeroPlanOrder(model.Prod_PlanReferenceOrder.Prod_ReferenceFK, 1);
            Prod_ReferencePlan sewingRef = new Prod_ReferencePlan();
            sewingRef.Prod_ReferenceFK = model.Prod_ReferenceFK;
            sewingRef.CID = CidServices.ProductionSectionCID((int)EnumProcess.Sewing, model.ProductionDate, model.Prod_ReferenceFK);
            sewingRef.ProductionDate = model.ProductionDate;
            sewingRef.Merchandising_StyleSetPackFK = model.Merchandising_StyleSetPackFK;
            sewingRef.Merchandising_StyleFK = model.Merchandising_StyleFK;
            sewingRef.Plan_SMVMasterLayoutFK = model.Plan_SMVMasterLayoutFK;
            sewingRef.ProductionLineFK = (int)model.Plan_ProductionLineFk;
            sewingRef.PlanQuantity = model.TargetQty;
            sewingRef.GSMORSMV = model.SMV;
            sewingRef.LineInputQuantity = model.LineInputQty;
            sewingRef.WorkingHour = model.WorkingHour;
            sewingRef.MachineRunning = model.MachineRunning;
            sewingRef.PresentOperator = model.PresentOpt;
            sewingRef.PresentHelper = model.NoOfHelper;
            sewingRef.Common_LineChiefFK = model.Prod_LineChiefFK;
            sewingRef.Common_SuperVisorFK = model.Prod_SuperVisorFK;
            sewingRef.Remarks = model.Remarks;
            sewingRef.SizeMan = 1;
            sewingRef.InputMan = 1;
            sewingRef.LineMan = 1;
            sewingRef.SectionId = (int)EnumProcess.Sewing;
            sewingRef.Active = true;
            _db.Prod_ReferencePlan.Add(sewingRef);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = sewingRef.ID;
            }
            return result;
        }

        public VMDailyProduction SewingPlanGetByID(int pid)
        {
            var getPlanData = (from t1 in _db.Prod_ReferencePlan
                               join t2 in _db.Merchandising_Style on t1.Merchandising_StyleFK equals t2.ID
                               join t3 in _db.Merchandising_BuyerOrder on t2.Merchandising_BuyerOrderFK equals t3.ID
                               join t4 in _db.Common_ProductionLine on t1.ProductionLineFK equals t4.ID
                               join t5 in _db.Plan_SMVMasterLayout on t1.Plan_SMVMasterLayoutFK equals t5.ID
                               join t6 in _db.Common_LineChief on t1.Common_LineChiefFK equals t6.ID
                               join t7 in _db.Common_LineSuperVisor on t1.Common_SuperVisorFK equals t7.ID
                               where t1.ID == pid
                               select new VMDailyProduction
                               {
                                   Prod_ReferencePlanFK = t1.ID,
                                   Prod_ReferenceFK = t1.Prod_ReferenceFK,
                                   Plan_ProductionLineFk = t1.Prod_ReferenceFK,
                                   Merchandising_StyleFK = t1.Merchandising_StyleFK,
                                   Plan_SMVMasterLayoutFK = (int)t1.Plan_SMVMasterLayoutFK,
                                   Style = t5.Remarks + "|" + "[SMV-" + t5.TotalSMV + "]" + t3.BuyerPO + "/" + t2.StyleName,
                                   LineTable = t5.Remarks + "/" + t2.StyleName,
                                   LineInputQty = t1.LineInputQuantity,
                                   PresentOpt = t1.PresentOperator,
                                   NoOfHelper = t1.PresentHelper,
                                   MachineRunning = t1.MachineRunning,
                                   PerHourTarget = t5.PerHourTarget,
                                   WorkingHour = t1.WorkingHour,
                                   LineName = t4.Name,
                                   SMV = t5.TotalSMV,
                                   LineChiefSupervisor = t6.Name,
                                   SuperVisor = t7.Name,
                                   Remarks = t1.Remarks,
                                   ActionId = (int)ActionEnum.Edit,
                                   SectionID = (int)EnumProcess.Sewing,
                               }).FirstOrDefault();

            return getPlanData;
        }

        public async Task<int> SewingStyleEdit(VMDailyProduction model)
        {
            var result = -1;
            model.TargetQty = (int)decimal.Multiply(model.WorkingHour, model.PerHourTarget);

            Prod_ReferencePlan sewingRef = _db.Prod_ReferencePlan.Find(model.Prod_ReferencePlanFK);
            sewingRef.PlanQuantity = model.TargetQty;
            sewingRef.LineInputQuantity = model.LineInputQty;
            sewingRef.WorkingHour = model.WorkingHour;
            sewingRef.MachineRunning = model.MachineRunning;
            sewingRef.PresentOperator = model.PresentOpt;
            sewingRef.PresentHelper = model.NoOfHelper;
            sewingRef.DayQty = model.LineInputQty;
            sewingRef.Remarks = model.Remarks;
            sewingRef.Active = true;
            if (await _db.SaveChangesAsync() > 0)
            {
                result = sewingRef.Prod_ReferenceFK;
            }
            return result;
        }

        public async Task<int> SewingFollowUpStyleAdd(VMDailyProduction model)
        {
            int result = -1;
            //var getPlanId = _db.Prod_ReferencePlan.Where(a=>a.Merchandising_StyleFK==model.Merchandising_StyleFK && a.ProductionLineFK==model.Plan_ProductionLineFk && a.Active==true && a.Prod_ReferenceFK);
            //if (getPlanId.Any())
            //{
            //    model.Prod_ReferencePlanFK = getPlanId.FirstOrDefault().ID;
            //}
            //DeleteZeroPlanOrder(model.Prod_PlanReferenceOrder.Prod_ReferenceFK, 1);
            var vData = (from t1 in _db.Merchandising_StyleShipmentSchedule
                         join t4 in _db.Merchandising_StyleShipmentRatio on t1.ID equals t4.Merchandising_StyleShipmentScheduleFK
                         join t5 in _db.Merchandising_StyleMeasurement on t4.Merchandising_StyleMeasurementFK equals t5.ID
                         join t6 in _db.Merchandising_Style on t1.Merchandising_StyleFK equals t6.ID
                         where t1.Active == true && t5.Common_ColorFK == model.Common_ColorFK
                         && t1.Merchandising_StyleFK == model.Merchandising_StyleFK && t4.Active == true
                         select new { t1, t4, t6 }).AsEnumerable();

            if (vData.Any())
            {
                foreach (var v in vData)
                {
                    Prod_ReferencePlanFollowup sewingFollow = new Prod_ReferencePlanFollowup();
                    sewingFollow.Prod_ReferencePlanFK = model.Prod_ReferencePlanFK;
                    sewingFollow.Merchandising_StyleShipmentScheduleFK = v.t1.ID;
                    sewingFollow.Merchandising_StyleShipmentRatioFK = v.t4.ID;
                    sewingFollow.Quantity = (int)decimal.Multiply(model.DoneQty, v.t6.PackPieceQty);
                    sewingFollow.Active = true;
                    _db.Prod_ReferencePlanFollowup.Add(sewingFollow);
                }
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = 1;
                }
            }
            return result;
        }
        #endregion

        #region IroningStep
        public async Task<VMDailyProduction> IroningDailyPlanGetByRefID(int ID)
        {
            var vData = (from t1 in _db.Prod_ReferencePlan
                         join t2 in _db.Merchandising_Style on t1.Merchandising_StyleFK equals t2.ID
                         join t3 in _db.Merchandising_BuyerOrder on t2.Merchandising_BuyerOrderFK equals t3.ID
                         join t4 in _db.Common_FinishItem on t2.Common_FinishItemFK equals t4.ID
                         where t1.Prod_ReferenceFK == ID && t1.Active == true && t1.SectionId == 3
                         select new VMDailyProduction
                         {
                             Code = t1.CID,
                             ID = t1.ID,
                             Prod_ReferenceFK = t1.Prod_ReferenceFK,
                             Prod_ReferencePlanFK = t1.ID,
                             Merchandising_StyleFK = t1.Merchandising_StyleFK,
                             OrderNo = t3.BuyerPO + "/" + t2.StyleName,
                         }).OrderByDescending(x => x.ID).ToList();

            if (vData.Any())
            {
                foreach (var v in vData)
                {
                    v.OrderQty = GetTotalOrderQtyByStyleID(v.Merchandising_StyleFK);
                    v.CuttingDoneQuantity = GetTotalDoneQtyBySectionID(v.Merchandising_StyleFK, 1);
                    v.SewingDoneQuantity = GetTotalDoneQtyBySectionID(v.Merchandising_StyleFK, 2);
                    v.DoneQty = GetDoneQtyBySectionID(v.Prod_ReferenceFK, v.Merchandising_StyleFK, 3);
                    v.RemainQty = v.OrderQty - v.DoneQty;
                }
            }

            VMDailyProduction vMReference = new VMDailyProduction
            {
                DataList = await Task.Run(() => vData)
            };

            return vMReference;
        }

        public async Task<VMProductionReference> IroningProductionGetByRefID(int ID)
        {
            VMProductionReference vModel = new VMProductionReference();
            vModel.vmDailyProduction = new VMDailyProduction();
            vModel = await ProdReferenceGetByID(ID);
            vModel.vmDailyProduction = await IroningDailyPlanGetByRefID(ID);
            return vModel;
        }

        public async Task<VMDailyProduction> IroningDailyFollowupGetByRefID(int ID)
        {
            var vData = (from t1 in _db.Prod_ReferencePlanFollowup
                         join t2 in _db.Prod_ReferencePlan on t1.Prod_ReferencePlanFK equals t2.ID
                         join t3 in _db.Merchandising_StyleShipmentSchedule on t1.Merchandising_StyleShipmentScheduleFK equals t3.ID
                         join t4 in _db.Merchandising_StyleShipmentRatio on t1.Merchandising_StyleShipmentRatioFK equals t4.ID
                         join t5 in _db.Merchandising_StyleMeasurement on t4.Merchandising_StyleMeasurementFK equals t5.ID
                         join t6 in _db.Common_Color on t5.Common_ColorFK equals t6.ID
                         join t7 in _db.Common_Size on t5.Common_SizeFK equals t7.ID
                         join t8 in _db.Merchandising_Style on t3.Merchandising_StyleFK equals t8.ID
                         join t9 in _db.Merchandising_BuyerOrder on t8.Merchandising_BuyerOrderFK equals t9.ID
                         join t10 in _db.Common_Country on t3.Common_CountryFK equals t10.ID
                         where t2.Prod_ReferenceFK == ID && t1.Active == true && t2.SectionId == 3
                         select new VMDailyProduction
                         {
                             ID = t1.ID,
                             Prod_ReferencePlanFK = t2.ID,
                             Prod_ReferenceFK = t2.Prod_ReferenceFK,
                             Merchandising_StyleFK = t3.Merchandising_StyleFK,
                             StyleShipmentRatioFK = (int)t1.Merchandising_StyleShipmentRatioFK,
                             StyleShipmentScheduleFK = (int)t1.Merchandising_StyleShipmentScheduleFK,
                             Common_ColorFK = t5.Common_ColorFK,
                             Common_SizeFK = t5.Common_SizeFK,
                             Merchandising_StyleSetPackFK = t2.Merchandising_StyleSetPackFK != null ? (int)t2.Merchandising_StyleSetPackFK : 0,
                             Port = t10.Name + "-" + t3.DestNo,
                             OrderNo = t9.BuyerPO + "/" + t8.StyleName,
                             Color = t6.Name,
                             Size = t7.Name,
                             OrderQty = (int)decimal.Multiply(t4.Quantity, t8.PackPieceQty),
                             DoneQty = (int)t1.Quantity,
                             EntryDate = t3.ShipmentDate
                         }).OrderBy(x => x.Prod_ReferenceFK).ToList();

            if (vData.Any())
            {
                foreach (var v in vData)
                {
                    v.RemainQty = v.OrderQty - GetIronPackDoneQtyBySizeWise(v.Prod_ReferenceFK, v.Merchandising_StyleFK, v.Common_ColorFK, v.Common_SizeFK, 3);
                }
            }

            VMDailyProduction vMReference = new VMDailyProduction
            {
                DataList = await Task.Run(() => vData)
            };

            return vMReference;
        }

        public async Task<VMProductionReference> IroningFollowupGetByRefID(int ID)
        {
            VMProductionReference vModel = new VMProductionReference();
            vModel.vmDailyProduction = new VMDailyProduction();
            vModel = await ProdReferenceGetByID(ID);
            vModel.vmDailyProduction = await IroningDailyFollowupGetByRefID(ID);
            return vModel;
        }

        public async Task<int> IroningStyleAdd(VMDailyProduction model)
        {
            int result = -1;

            model.ProductionDate = _db.Prod_Reference.FirstOrDefault(a => a.ID == model.Prod_ReferenceFK).ReferenceDate;
            Prod_ReferencePlan ironingRef = new Prod_ReferencePlan();
            ironingRef.Prod_ReferenceFK = model.Prod_ReferenceFK;
            ironingRef.CID = CidServices.ProductionSectionCID((int)EnumProcess.Ironning, model.ProductionDate, model.Prod_ReferenceFK);
            ironingRef.ProductionDate = model.ProductionDate;
            ironingRef.Merchandising_StyleFK = model.Merchandising_StyleFK;
            ironingRef.SectionId = (int)EnumProcess.Ironning;
            ironingRef.Active = true;
            _db.Prod_ReferencePlan.Add(ironingRef);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = ironingRef.ID;
            }

            return result;
        }

        public async Task<int> IroningFollowUpStyleAdd(VMDailyProduction model)
        {
            int result = -1;
            var vData = (from t1 in _db.Merchandising_StyleShipmentSchedule
                         join t4 in _db.Merchandising_StyleShipmentRatio on t1.ID equals t4.Merchandising_StyleShipmentScheduleFK
                         join t5 in _db.Merchandising_StyleMeasurement on t4.Merchandising_StyleMeasurementFK equals t5.ID
                         join t6 in _db.Merchandising_Style on t1.Merchandising_StyleFK equals t6.ID
                         where t1.Active == true && t5.Common_ColorFK == model.Common_ColorFK
                         && t1.Merchandising_StyleFK == model.Merchandising_StyleFK && t4.Active == true && t1.Active == true
                         select new { t1, t4, t6 }).AsEnumerable();

            if (vData.Any())
            {
                foreach (var v in vData)
                {
                    Prod_ReferencePlanFollowup sewingFollow = new Prod_ReferencePlanFollowup();
                    sewingFollow.Prod_ReferencePlanFK = model.Prod_ReferencePlanFK;
                    sewingFollow.Merchandising_StyleShipmentScheduleFK = v.t1.ID;
                    sewingFollow.Merchandising_StyleShipmentRatioFK = v.t4.ID;
                    sewingFollow.Quantity = (int)decimal.Multiply(model.DoneQty, v.t6.PackPieceQty);
                    sewingFollow.Active = true;
                    _db.Prod_ReferencePlanFollowup.Add(sewingFollow);
                }
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = 1;
                }
            }
            return result;
        }
        #endregion

        #region PackingStep

        public async Task<VMDailyProduction> PackingDailyPlanGetByRefID(int ID)
        {
            var vData = (from t1 in _db.Prod_ReferencePlan
                         join t2 in _db.Merchandising_Style on t1.Merchandising_StyleFK equals t2.ID
                         join t3 in _db.Merchandising_BuyerOrder on t2.Merchandising_BuyerOrderFK equals t3.ID
                         where t1.Prod_ReferenceFK == ID && t1.Active == true && t1.SectionId == 4
                         select new VMDailyProduction
                         {
                             Code = t1.CID,
                             ID = t1.ID,
                             Prod_ReferenceFK = t1.Prod_ReferenceFK,
                             Prod_ReferencePlanFK = t1.ID,
                             Merchandising_StyleFK = t1.Merchandising_StyleFK,
                             OrderNo = t3.BuyerPO + "/" + t2.StyleName
                         }).OrderByDescending(x => x.ID).ToList();

            if (vData.Any())
            {
                foreach (var v in vData)
                {
                    v.OrderQty = GetTotalOrderQtyByStyleID(v.Merchandising_StyleFK);
                    v.CuttingDoneQuantity = GetTotalDoneQtyBySectionID(v.Merchandising_StyleFK, 1);
                    v.SewingDoneQuantity = GetTotalDoneQtyBySectionID(v.Merchandising_StyleFK, 2);
                    v.IronDoneQuantity = GetTotalDoneQtyBySectionID(v.Merchandising_StyleFK, 3);
                    v.DoneQty = GetDoneQtyBySectionID(v.Prod_ReferenceFK, v.Merchandising_StyleFK, 4);
                    v.RemainQty = v.OrderQty - v.DoneQty;
                }
            }

            VMDailyProduction vMReference = new VMDailyProduction
            {
                DataList = await Task.Run(() => vData)
            };

            return vMReference;
        }

        public async Task<VMProductionReference> PackingProductionGetByRefID(int ID)
        {
            VMProductionReference vModel = new VMProductionReference();
            vModel.vmDailyProduction = new VMDailyProduction();
            vModel = await ProdReferenceGetByID(ID);
            vModel.vmDailyProduction = await PackingDailyPlanGetByRefID(ID);
            return vModel;
        }

        public async Task<VMDailyProduction> PackingDailyFollowupGetByRefID(int ID)
        {
            var vData = (from t1 in _db.Prod_ReferencePlanFollowup
                         join t2 in _db.Prod_ReferencePlan on t1.Prod_ReferencePlanFK equals t2.ID
                         join t3 in _db.Merchandising_StyleShipmentSchedule on t1.Merchandising_StyleShipmentScheduleFK equals t3.ID
                         join t4 in _db.Merchandising_StyleShipmentRatio on t1.Merchandising_StyleShipmentRatioFK equals t4.ID
                         join t5 in _db.Merchandising_StyleMeasurement on t4.Merchandising_StyleMeasurementFK equals t5.ID
                         join t6 in _db.Common_Color on t5.Common_ColorFK equals t6.ID
                         join t7 in _db.Common_Size on t5.Common_SizeFK equals t7.ID
                         join t8 in _db.Merchandising_Style on t3.Merchandising_StyleFK equals t8.ID
                         join t9 in _db.Merchandising_BuyerOrder on t8.Merchandising_BuyerOrderFK equals t9.ID
                         join t10 in _db.Common_Country on t3.Common_CountryFK equals t10.ID
                         where t2.Prod_ReferenceFK == ID && t1.Active == true && t2.SectionId == 4
                         select new VMDailyProduction
                         {
                             ID = t1.ID,
                             Prod_ReferencePlanFK = t2.ID,
                             Prod_ReferenceFK = t2.Prod_ReferenceFK,
                             Merchandising_StyleFK = t3.Merchandising_StyleFK,
                             StyleShipmentRatioFK = (int)t1.Merchandising_StyleShipmentRatioFK,
                             StyleShipmentScheduleFK = (int)t1.Merchandising_StyleShipmentScheduleFK,
                             Merchandising_StyleSetPackFK = t2.Merchandising_StyleSetPackFK != null ? (int)t2.Merchandising_StyleSetPackFK : 0,
                             Common_ColorFK = t5.Common_ColorFK,
                             Common_SizeFK = t5.Common_SizeFK,
                             Port = t10.Name + "-" + t3.DestNo,
                             OrderNo = t9.BuyerPO + "/" + t8.StyleName,
                             Color = t6.Name,
                             Size = t7.Name,
                             OrderQty = (int)decimal.Multiply(t4.Quantity, t8.PackPieceQty),
                             DoneQty = (int)t1.Quantity,
                             EntryDate = t3.ShipmentDate
                         }).OrderBy(x => x.Prod_ReferenceFK).ToList();

            if (vData.Any())
            {
                foreach (var v in vData)
                {
                    v.RemainQty = v.OrderQty - GetIronPackDoneQtyBySizeWise(v.Prod_ReferenceFK, v.Merchandising_StyleFK, v.Common_ColorFK, v.Common_SizeFK, 4);
                }
            }

            VMDailyProduction vMReference = new VMDailyProduction
            {
                DataList = await Task.Run(() => vData)
            };

            return vMReference;
        }

        public async Task<VMProductionReference> PackingFollowupGetByRefID(int ID)
        {
            VMProductionReference vModel = new VMProductionReference();
            vModel.vmDailyProduction = new VMDailyProduction();
            vModel = await ProdReferenceGetByID(ID);
            vModel.vmDailyProduction = await PackingDailyFollowupGetByRefID(ID);
            return vModel;
        }

        public async Task<int> PackingStyleAdd(VMDailyProduction model)
        {
            int result = -1;
            model.ProductionDate = _db.Prod_Reference.FirstOrDefault(a => a.ID == model.Prod_ReferenceFK).ReferenceDate;
            Prod_ReferencePlan packingRef = new Prod_ReferencePlan();
            packingRef.Prod_ReferenceFK = model.Prod_ReferenceFK;
            packingRef.CID = CidServices.ProductionSectionCID((int)EnumProcess.Packing, model.ProductionDate, model.Prod_ReferenceFK);
            packingRef.ProductionDate = model.ProductionDate;
            packingRef.Merchandising_StyleFK = model.Merchandising_StyleFK;
            packingRef.SectionId = (int)EnumProcess.Packing;
            packingRef.Active = true;
            _db.Prod_ReferencePlan.Add(packingRef);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = packingRef.ID;
            }

            return result;
        }

        public async Task<int> PackingFollowUpStyleAdd(VMDailyProduction model)
        {
            int result = -1;
            var vData = (from t1 in _db.Merchandising_StyleShipmentSchedule
                         join t4 in _db.Merchandising_StyleShipmentRatio on t1.ID equals t4.Merchandising_StyleShipmentScheduleFK
                         join t5 in _db.Merchandising_StyleMeasurement on t4.Merchandising_StyleMeasurementFK equals t5.ID
                         join t6 in _db.Merchandising_Style on t1.Merchandising_StyleFK equals t6.ID
                         where t1.Active == true && t5.Common_ColorFK == model.Common_ColorFK
                         && t1.Merchandising_StyleFK == model.Merchandising_StyleFK && t4.Active == true
                         select new { t1, t4, t6 }).AsEnumerable();

            if (vData.Any())
            {
                foreach (var v in vData)
                {
                    Prod_ReferencePlanFollowup sewingFollow = new Prod_ReferencePlanFollowup();
                    sewingFollow.Prod_ReferencePlanFK = model.Prod_ReferencePlanFK;
                    sewingFollow.Merchandising_StyleShipmentScheduleFK = v.t1.ID;
                    sewingFollow.Merchandising_StyleShipmentRatioFK = v.t4.ID;
                    sewingFollow.Quantity = (int)decimal.Multiply(model.DoneQty, v.t6.PackPieceQty);
                    sewingFollow.Active = true;
                    _db.Prod_ReferencePlanFollowup.Add(sewingFollow);
                }
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = 1;
                }
            }
            return result;
        }
        #endregion

        #region CommonAction

        public async Task<int> FollowUpDetailsAdd(VMDailyProduction model)
        {
            int result = -1;
            var vGetPlan = _db.Prod_ReferencePlanFollowup.Where(a => a.ID == model.Prod_ReferencePlanFollowupFK).FirstOrDefault();

            Prod_ReferencePlanFollowupDetails objFollow = new Prod_ReferencePlanFollowupDetails();
            objFollow.Prod_ReferencePlanFollowupFK = vGetPlan.ID;
            objFollow.Quantity = model.DoneQty;
            objFollow.Time = DateTime.Now;
            _db.Prod_ReferencePlanFollowupDetails.Add(objFollow);

            if (await _db.SaveChangesAsync() > 0)
            {
                result = UpdateFollowUp(vGetPlan.ID);
            }

            return result;
        }

        private int UpdateFollowUp(int ID)
        {
            var vGetPlan = _db.Prod_ReferencePlanFollowup.Where(a => a.ID == ID).FirstOrDefault();
            var TotalFollowupQty = _db.Prod_ReferencePlanFollowupDetails.Where(a => a.Prod_ReferencePlanFollowupFK == vGetPlan.ID).DefaultIfEmpty().Sum(a => a.Quantity);

            vGetPlan.Quantity = TotalFollowupQty;
            _db.SaveChangesAsync();

            return (int)vGetPlan.Quantity;
        }

        private DateTime GetPreviousReferenceDate(DateTime RefDate)
        {
            var prePlan = _db.Prod_Reference.Where(a => a.ReferenceDate == RefDate);
            if (prePlan.Any())
            {
                return prePlan.FirstOrDefault().ReferenceDate;
            }
            else
            {
                return GetPreviousReferenceDate(RefDate.AddDays(-1));
            }
        }

        public async Task<int> AddPreviousSewingPlanOrder(int Id)
        {
            int result = -1;
            var vData = _db.Prod_Reference.Where(a => a.ID == Id);
            if (vData.Any())
            {
                var take = vData.FirstOrDefault();
                DateTime dt = GetPreviousReferenceDate(take.ReferenceDate.AddDays(-1));
                var getPlan = (from t1 in _db.Prod_Reference
                               join t2 in _db.Prod_ReferencePlan on t1.ID equals t2.Prod_ReferenceFK
                               where t1.ReferenceDate == dt && t2.Active == true && t2.SectionId == 2
                               select new { t1, t2 }).ToList();
                if (getPlan.Any())
                {
                    foreach (var v in getPlan)
                    {
                        Prod_ReferencePlan sewingRef = new Prod_ReferencePlan();
                        sewingRef.Prod_ReferenceFK = Id;
                        sewingRef.Merchandising_StyleFK = v.t2.Merchandising_StyleFK;
                        sewingRef.Plan_SMVMasterLayoutFK = v.t2.Plan_SMVMasterLayoutFK;
                        sewingRef.ProductionLineFK = (int)v.t2.ProductionLineFK;
                        sewingRef.PlanQuantity = v.t2.PlanQuantity;
                        sewingRef.GSMORSMV = v.t2.GSMORSMV;
                        sewingRef.WorkingHour = v.t2.WorkingHour;
                        sewingRef.MachineRunning = v.t2.MachineRunning;
                        sewingRef.PresentOperator = v.t2.PresentOperator;
                        sewingRef.PresentHelper = v.t2.PresentHelper;
                        sewingRef.SizeMan = 1;
                        sewingRef.InputMan = 1;
                        sewingRef.LineMan = 1;
                        sewingRef.SectionId = 2;
                        sewingRef.Active = true;
                        _db.Prod_ReferencePlan.Add(sewingRef);
                        if (await _db.SaveChangesAsync() > 0)
                        {
                            result = sewingRef.ID;
                        }
                    }
                }
            }
            return result;
        }

        public async Task<int> GetDeletePlanOrderWithoutPlan(int PlanOrderId)
        {
            int result = -1;
            var vData = _db.Prod_ReferencePlan.Where(a => a.ID == PlanOrderId);
            if (vData.Any())
            {
                bool IsCanDelete = true;

                var takeFirst = vData.FirstOrDefault();
                VMDailyProduction dailyProduction = new VMDailyProduction();
                //this.ProductionDailyPlan.Prod_ReferenceFk = takeFirst.Prod_ReferenceFK;
                var checkPlanSection = _db.Prod_ReferencePlanFollowup.Where(a => a.Prod_ReferencePlanFK == takeFirst.ID).ToList();
                if (checkPlanSection.Any())
                {
                    foreach (var v in checkPlanSection)
                    {
                        var vSlave = _db.Prod_ReferencePlanFollowupDetails.Where(a => a.Prod_ReferencePlanFollowupFK == v.ID);
                        var vSlaveCount = _db.Prod_ReferencePlanFollowupDetails.Where(a => a.Prod_ReferencePlanFollowupFK == v.ID).Sum(a => a.Quantity);
                        if (vSlave.Any() && vSlaveCount>0)
                        {
                            IsCanDelete = false;
                            break;
                        }
                    }
                }
                if (IsCanDelete)
                {
                    var checkPlanSection1 = _db.Prod_ReferencePlanFollowup.Where(a => a.Prod_ReferencePlanFK == takeFirst.ID).ToList();
                    if (checkPlanSection1.Any())
                    {
                        foreach (var v in checkPlanSection1)
                        {
                            var vSlave = _db.Prod_ReferencePlanFollowupDetails.Where(a => a.Prod_ReferencePlanFollowupFK == v.ID);
                            var vSlaveCount = _db.Prod_ReferencePlanFollowupDetails.Where(a => a.Prod_ReferencePlanFollowupFK == v.ID).Sum(a => a.Quantity);
                            if (vSlave.Any() && vSlaveCount <= 0)
                            {
                                _db.Prod_ReferencePlanFollowupDetails.RemoveRange(vSlave);
                                _db.Prod_ReferencePlanFollowup.RemoveRange(checkPlanSection1);
                                _db.Prod_ReferencePlan.Remove(takeFirst);
                            }
                        }
                    }
                    else
                    {
                        _db.Prod_ReferencePlan.Remove(takeFirst);
                    }
                }
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = PlanOrderId;
                }
            }
            return result;
        }

        #endregion

        #endregion

        #region ManagePrePlanSMVLayout
        public VMSMVLayout SMVLayoutGetByStyleID(int ID)
        {
            var vData= (from a in _db.Plan_SMVDraftLayout
                        join b in _db.Merchandising_StyleSetPack on a.Merchandising_StyleSetPackFK equals b.ID into all
                        from c in all.DefaultIfEmpty()
                        where a.Merchandising_StyleFK == ID && a.Active == true
                        select new VMSMVLayout
                        {
                            ID = a.ID,
                            Description = a.Remarks,
                            HelperSMV = a.HelperSMV,
                            Subset = all != null ? c.SetPackName : "",
                            Merchandising_StyleSetPackFK = a.Merchandising_StyleSetPackFK == null ? 0 : (int)a.Merchandising_StyleSetPackFK,
                            Merchandising_StyleFk = a.Merchandising_StyleFK,
                            Efficiency = a.Efficiency,
                            NoOfHelper = a.NoOfHelper,
                            OperatorSMV = a.OperatorSMV,
                            NoOfOperator = a.NoOfOperator,
                            PerHourTarget = a.PerHourTarget,
                            TotalSMV = a.TotalSMV,
                            TotalWorker = a.TotalWorker,
                            WorkingHour = a.WorkingHour,
                            ActionId = (int)ActionEnum.Edit
                        }).OrderBy(a => a.ID).ToList();

            VMSMVLayout sMVLayout = new VMSMVLayout
            {
                DataList = vData
            };
            return sMVLayout;
        }
        public VMPlanView GetStylePreSMV(int ID)
        {
            var VM = new VMPlanView();
            VM.VmStyle = new VMStyle();
            VM.vmSMVLayout = new VMSMVLayout();
            VM.vmSMVLayout = SMVLayoutGetByStyleID(ID);
            VM.VmStyle = integrationService.StyleGetByID(ID);

            return VM;
        }
        public async Task<int> PlanSMVAdd(VMPlanView vMSMV)
        {
            var result = -1;
            decimal percent = decimal.Divide(vMSMV.vmSMVLayout.Efficiency, 100);
            
            vMSMV.vmSMVLayout.TotalWorker = vMSMV.vmSMVLayout.NoOfOperator + vMSMV.vmSMVLayout.NoOfHelper;
            vMSMV.vmSMVLayout.TotalSMV = vMSMV.vmSMVLayout.OperatorSMV + vMSMV.vmSMVLayout.HelperSMV;
            //decimal a = vMSMV.vmSMVLayout.TotalWorker * vMSMV.vmSMVLayout.WorkingHour * percent * 60;
            //decimal b = (vMSMV.vmSMVLayout.TotalWorker * vMSMV.vmSMVLayout.WorkingHour * percent * 60) / vMSMV.vmSMVLayout.TotalSMV;
            vMSMV.vmSMVLayout.PerHourTarget = ((vMSMV.vmSMVLayout.TotalWorker * vMSMV.vmSMVLayout.WorkingHour * percent * 60) / vMSMV.vmSMVLayout.TotalSMV) / vMSMV.vmSMVLayout.WorkingHour;

            Plan_SMVDraftLayout smvLayout = new Plan_SMVDraftLayout();
            smvLayout.Merchandising_StyleFK = vMSMV.vmSMVLayout.Merchandising_StyleFk;
            smvLayout.Merchandising_StyleSetPackFK = vMSMV.vmSMVLayout.Merchandising_StyleSetPackFK;
            smvLayout.NoOfOperator = vMSMV.vmSMVLayout.NoOfOperator;
            smvLayout.NoOfHelper = vMSMV.vmSMVLayout.NoOfHelper;
            smvLayout.OperatorSMV = vMSMV.vmSMVLayout.OperatorSMV;
            smvLayout.HelperSMV = vMSMV.vmSMVLayout.HelperSMV;
            smvLayout.TotalWorker = vMSMV.vmSMVLayout.TotalWorker;
            smvLayout.TotalSMV = vMSMV.vmSMVLayout.TotalSMV;
            smvLayout.WorkingHour = vMSMV.vmSMVLayout.WorkingHour;
            smvLayout.PerHourTarget = (int)Math.Round(vMSMV.vmSMVLayout.PerHourTarget);
            smvLayout.Efficiency = vMSMV.vmSMVLayout.Efficiency;
            smvLayout.Remarks = vMSMV.vmSMVLayout.Description;
            smvLayout.Active = true;

            _db.Plan_SMVDraftLayout.Add(smvLayout);

            if (await _db.SaveChangesAsync() > 0)
            {
                result = vMSMV.vmSMVLayout.Merchandising_StyleFk;
            }
            return result;
        }
        public async Task<int> PlanSMVEdit(VMPlanView vMSMV)
        {
            var result = -1;
            decimal percent = decimal.Divide(vMSMV.vmSMVLayout.Efficiency, 100);
            vMSMV.vmSMVLayout.TotalWorker = (int)decimal.Add(vMSMV.vmSMVLayout.NoOfOperator, vMSMV.vmSMVLayout.NoOfHelper);
            vMSMV.vmSMVLayout.TotalSMV = decimal.Add(vMSMV.vmSMVLayout.OperatorSMV, vMSMV.vmSMVLayout.HelperSMV);
            //decimal a = vMSMV.vmSMVLayout.TotalWorker * vMSMV.vmSMVLayout.WorkingHour * percent * 60;
            //decimal b = (vMSMV.vmSMVLayout.TotalWorker * vMSMV.vmSMVLayout.WorkingHour * percent * 60) / vMSMV.vmSMVLayout.TotalSMV;
            vMSMV.vmSMVLayout.PerHourTarget = ((vMSMV.vmSMVLayout.TotalWorker * vMSMV.vmSMVLayout.WorkingHour * percent * 60) / vMSMV.vmSMVLayout.TotalSMV) / vMSMV.vmSMVLayout.WorkingHour;

            Plan_SMVDraftLayout smvLayout = _db.Plan_SMVDraftLayout.Find(vMSMV.vmSMVLayout.ID);
            smvLayout.Merchandising_StyleSetPackFK = vMSMV.vmSMVLayout.Merchandising_StyleSetPackFK;
            smvLayout.NoOfOperator = vMSMV.vmSMVLayout.NoOfOperator;
            smvLayout.NoOfHelper = vMSMV.vmSMVLayout.NoOfHelper;
            smvLayout.OperatorSMV = vMSMV.vmSMVLayout.OperatorSMV;
            smvLayout.HelperSMV = vMSMV.vmSMVLayout.HelperSMV;
            smvLayout.TotalWorker = vMSMV.vmSMVLayout.TotalWorker;
            smvLayout.TotalSMV = vMSMV.vmSMVLayout.TotalSMV;
            smvLayout.WorkingHour = vMSMV.vmSMVLayout.WorkingHour;
            smvLayout.PerHourTarget = (int)Math.Round(vMSMV.vmSMVLayout.PerHourTarget);
            smvLayout.Efficiency = vMSMV.vmSMVLayout.Efficiency;
            smvLayout.Remarks = vMSMV.vmSMVLayout.Description;
            smvLayout.Active = true;

            if (await _db.SaveChangesAsync() > 0)
            {
                result = smvLayout.Merchandising_StyleFK;
            }
            return result;
        }
        public int PlanSMVDelete(int ID)
        {
            var result = -1;

            return result;
        }

        public VMSMVLayout GetPreSMVByID(int ID)
        {
            VMSMVLayout vmSMVLayout = new VMSMVLayout();

            vmSMVLayout = (from a in _db.Plan_SMVDraftLayout
                           join b in _db.Merchandising_StyleSetPack on a.Merchandising_StyleSetPackFK equals b.ID into all
                           from c in all.DefaultIfEmpty()
                           where a.ID == ID && a.Active == true
                           select new VMSMVLayout
                           {
                               ID = a.ID,
                               Description = a.Remarks,
                               HelperSMV = a.HelperSMV,
                               Subset = all != null ? c.SetPackName : "",
                               Merchandising_StyleSetPackFK = a.Merchandising_StyleSetPackFK == null ? 0 : (int)a.Merchandising_StyleSetPackFK,
                               Merchandising_StyleFk = a.Merchandising_StyleFK,
                               Efficiency = a.Efficiency,
                               NoOfHelper = a.NoOfHelper,
                               OperatorSMV = a.OperatorSMV,
                               NoOfOperator = a.NoOfOperator,
                               PerHourTarget = a.PerHourTarget,
                               TotalSMV = a.TotalSMV,
                               TotalWorker = a.TotalWorker,
                               WorkingHour = a.WorkingHour,
                               ActionId = (int)ActionEnum.Edit
                           }).FirstOrDefault();

            return vmSMVLayout;
        }

        public VMSMVLayout GetMasterSMVByID(int ID)
        {
            VMSMVLayout vmSMVLayout = new VMSMVLayout();

            vmSMVLayout = _db.Plan_SMVMasterLayout.Where(a => a.ID == ID && a.Active == true).Select(a => new VMSMVLayout
            {
                ID = a.ID,
                Merchandising_StyleFk = a.Merchandising_StyleFK,
                PLan_SMVDraftLayoutFK =a.PLan_SMVDraftLayoutFK==null?0:(int)a.PLan_SMVDraftLayoutFK,
                Merchandising_StyleSetPackFK =a.Merchandising_StyleSetPackFK==null?0:(int)a.Merchandising_StyleSetPackFK,
                Description = a.Remarks,
                HelperSMV = a.HelperSMV,
                Efficiency = a.Efficiency,
                NoOfHelper = a.NoOfHelper,
                OperatorSMV = a.OperatorSMV,
                NoOfOperator = a.NoOfOperator,
                PerHourTarget = a.PerHourTarget,
                TotalSMV = a.TotalSMV,
                TotalWorker = a.TotalWorker,
                WorkingHour = a.WorkingHour,
                ActionId = (int)ActionEnum.Edit
            }).FirstOrDefault();

            return vmSMVLayout;
        }

        #endregion

        #region ManageMasterPlanSMVLayout

        public VMPlanView IndexMasterSMV()
        {
            var VM = new VMPlanView();
            VM.VmStyle = new VMStyle();
            VM.vmSMVLayout = new VMSMVLayout();
            VM.vmSMVLayout = SMVMasterLayoutGet();
            return VM;
        }
        public VMSMVLayout SMVMasterLayoutGet()
        {
            var vdata = (from t1 in _db.Plan_SMVMasterLayout
                         join t2 in _db.Merchandising_Style on t1.Merchandising_StyleFK equals t2.ID
                         join t4 in _db.Merchandising_BuyerOrder on t2.Merchandising_BuyerOrderFK equals t4.ID
                         join t5 in _db.Common_Buyer on t4.Common_BuyerFK equals t5.ID
                         join t6 in _db.Merchandising_StyleSetPack on t1.Merchandising_StyleSetPackFK equals t6.ID into Set_Join
                         from t7 in Set_Join.DefaultIfEmpty()
                         where t1.Active == true
                         select new VMSMVLayout
                         {
                             ID = t1.ID,
                             Style = t5.BuyerID + "/" + t4.BuyerPO + "/" + t2.StyleName,
                             Merchandising_StyleFk = t1.Merchandising_StyleFK,
                             Merchandising_StyleSetPackFK=t1.Merchandising_StyleSetPackFK!=null?(int)t1.Merchandising_StyleSetPackFK:0,
                             PLan_SMVDraftLayoutFK =t1.PLan_SMVDraftLayoutFK!=null?(int)t1.PLan_SMVDraftLayoutFK:0,
                             Subset=t7.SetPackName,
                             NoOfOperator = t1.NoOfOperator,
                             NoOfHelper = t1.NoOfHelper,
                             OperatorSMV = t1.OperatorSMV,
                             HelperSMV = t1.HelperSMV,
                             TotalWorker = t1.TotalWorker,
                             TotalSMV = t1.TotalSMV,
                             WorkingHour = t1.WorkingHour,
                             PerHourTarget = t1.PerHourTarget,
                             Efficiency = t1.Efficiency,
                             Description = t1.Remarks
                         }).OrderBy(a => a.Merchandising_StyleFk).ToList();

            VMSMVLayout sMVLayout = new VMSMVLayout
            {
                DataList = vdata
            };
            return sMVLayout;
        }
        public async Task<int> SMVMasterLayoutEdit(VMSMVLayout vMSMVLayout)
        {
            var result = -1;
            decimal percent = decimal.Divide(vMSMVLayout.Efficiency, 100);
            vMSMVLayout.TotalWorker = vMSMVLayout.NoOfOperator + vMSMVLayout.NoOfHelper;
            vMSMVLayout.TotalSMV = vMSMVLayout.OperatorSMV + vMSMVLayout.HelperSMV;
            vMSMVLayout.PerHourTarget = ((vMSMVLayout.TotalWorker * vMSMVLayout.WorkingHour * percent * 60) / vMSMVLayout.TotalSMV) / vMSMVLayout.WorkingHour;

            Plan_SMVMasterLayout pObj = _db.Plan_SMVMasterLayout.Find(vMSMVLayout.ID);
            pObj.Merchandising_StyleFK = vMSMVLayout.Merchandising_StyleFk;
            pObj.Merchandising_StyleSetPackFK = vMSMVLayout.Merchandising_StyleSetPackFK;
            pObj.NoOfOperator = vMSMVLayout.NoOfOperator;
            pObj.NoOfHelper = vMSMVLayout.NoOfHelper;
            pObj.OperatorSMV = vMSMVLayout.OperatorSMV;
            pObj.HelperSMV = vMSMVLayout.HelperSMV;
            pObj.TotalWorker = vMSMVLayout.TotalWorker;
            pObj.TotalSMV = vMSMVLayout.TotalSMV;
            pObj.WorkingHour = vMSMVLayout.WorkingHour;
            pObj.PerHourTarget = (int)Math.Round(vMSMVLayout.PerHourTarget);
            pObj.Remarks = vMSMVLayout.Description;
            pObj.Efficiency = vMSMVLayout.Efficiency;
            pObj.Active = true;

            if (await _db.SaveChangesAsync() > 0)
            {
                result = pObj.ID;
            }
            return result;
        }
        public async Task<int> SMVMasterLayoutDelete(int id)
        {
            var result = -1;
            Plan_SMVMasterLayout pObj = _db.Plan_SMVMasterLayout.Find(id);
            if (pObj != null)
            {
                pObj.Active = false;
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = pObj.ID;
                }
            }
            return result;
        }

        #endregion

        #region ExecutionPlan

        public List<VMOrderPlanning> GetAllPrePlan()
        {
            List<VMOrderPlanning> lst = new List<VMOrderPlanning>();
            var vMasterPlanData = (from t1 in _db.Plan_MasterOrderPlan
                                   join t2 in _db.Merchandising_Style on t1.Merchandising_StyleFK equals t2.ID
                                   join t3 in _db.Merchandising_BuyerOrder on t2.Merchandising_BuyerOrderFK equals t3.ID
                                   join t5 in _db.Plan_DraftOrderPlan on t1.Plan_DraftOrderPlanFK equals t5.ID
                                   join t6 in _db.Common_Buyer on t3.Common_BuyerFK equals t6.ID
                                   //join t7 in _db.Merchandising_StyleSetPack on t1.Merchandising_StyleSetPackFK equals t7.ID into allTable
                                   //from t8 in allTable.DefaultIfEmpty()
                                   where t1.Active == true
                                   select new VMOrderPlanning
                                   {
                                       ID = t1.ID,
                                       Plan_DraftOrderPlanFK = (int)t1.Plan_DraftOrderPlanFK,
                                       Merchandising_StyleFK = t1.Merchandising_StyleFK,
                                       Merchandising_SetPackFK = t1.Merchandising_StyleSetPackFK == null ? 0 : (int)t1.Merchandising_StyleSetPackFK,
                                       //ItemName=t8.SetPackName,
                                       OrderNumber = t3.BuyerPO,
                                       StyleNo = t6.BuyerID + "/" + t3.BuyerPO + "/" + t2.StyleName,
                                       SampleApprovedDate = t1.SampleApprovedDate,
                                       AccessoriesInhouseDate = t1.AccessoriesInDate,
                                       CuttingStartDate = t1.CuttingDate,
                                       FabricInhouseDate = t1.FabricInDate,
                                       OrderPlanQty = (int)t1.PlanQty,
                                       ShipmentDate = t1.ShipmentDate,
                                       SewingStartDate = t1.SewingDate,
                                       ExtraRate = t1.ExtraRate,
                                       Remarks = t1.Remarks
                                   }).ToList().OrderByDescending(a => a.ID);

            var vData = (from t1 in _db.Plan_DraftOrderPlan
                         join t2 in _db.Merchandising_Style on t1.Merchandising_StyleFK equals t2.ID
                         join t3 in _db.Merchandising_BuyerOrder on t2.Merchandising_BuyerOrderFK equals t3.ID
                         join t6 in _db.Common_Buyer on t3.Common_BuyerFK equals t6.ID
                         //join t7 in _db.Merchandising_StyleSetPack on t1.Merchandising_StyleSetPackFK equals t7.ID into allTable
                         //from t8 in allTable.DefaultIfEmpty()
                         where t1.Active == true
                         select new VMOrderPlanning
                         {
                             ID = t1.ID,
                             Merchandising_StyleFK = t1.Merchandising_StyleFK,
                             Merchandising_SetPackFK = t1.Merchandising_StyleSetPackFK == null ? 0 : (int)t1.Merchandising_StyleSetPackFK,
                             //ItemName = t8.SetPackName,
                             OrderNumber = t3.BuyerPO,
                             StyleNo = t6.BuyerID + "/" + t3.BuyerPO + "/" + t2.StyleName,
                             OrderPlanQty = (int)t1.PlanQty,
                             ShipmentDate = t1.ShipmentDate,
                             SampleApprovedDate = t1.SampleApprovedDate,
                             AccessoriesInhouseDate = t1.AccessoriesInDate,
                             FabricInhouseDate = t1.FabricInDate,
                             CuttingStartDate = t1.CuttingDate,
                             SewingStartDate = t1.SewingDate,
                             ExtraRate = t1.ExtraRate,
                             Remarks = t1.Remarks
                         }).ToList().OrderBy(a => a.ShipmentDate);

            if (vData.Any())
            {
                foreach (var v in vData)
                {
                    if (!vMasterPlanData.Any(a => a.Merchandising_StyleFK == v.Merchandising_StyleFK && a.ShipmentDate == v.ShipmentDate && a.Plan_DraftOrderPlanFK == v.ID))
                    {
                        if (v.Merchandising_SetPackFK > 0)
                        {
                            var vSubSet = _db.Merchandising_StyleSetPack.FirstOrDefault(a => a.ID == v.Merchandising_SetPackFK);
                            v.ItemName = vSubSet.SetPackName;
                        }

                        lst.Add(v);
                    }
                }
            }
            return lst;
        }

        public VMOrderPlanning GetAllMasterPlan()
        {
            var vData = (from t1 in _db.Plan_MasterOrderPlan
                         join t2 in _db.Merchandising_Style on t1.Merchandising_StyleFK equals t2.ID
                         join t3 in _db.Merchandising_BuyerOrder on t2.Merchandising_BuyerOrderFK equals t3.ID
                         join t6 in _db.Common_Buyer on t3.Common_BuyerFK equals t6.ID
                         join t7 in _db.Plan_DraftOrderPlan on t1.Plan_DraftOrderPlanFK equals t7.ID
                         //from t8 in allTable.DefaultIfEmpty()
                         where t1.Active == true
                         select new VMOrderPlanning
                         {
                             ID = t1.ID,
                             Merchandising_StyleFK = t1.Merchandising_StyleFK,
                             OrderNumber = t3.BuyerPO,
                             StyleNo = t6.BuyerID + "/" + t3.BuyerPO + "/" + t2.StyleName,
                             //ItemName=t8.SetPackName,
                             Merchandising_SetPackFK = t7.Merchandising_StyleSetPackFK == null ? 0 : (int)t7.Merchandising_StyleSetPackFK,
                             SampleApprovedDate = t1.SampleApprovedDate,
                             AccessoriesInhouseDate = t1.AccessoriesInDate,
                             CuttingStartDate = t1.CuttingDate,
                             FabricInhouseDate = t1.FabricInDate,
                             OrderPlanQty = (int)t1.PlanQty,
                             ShipmentDate = t1.ShipmentDate,
                             SewingStartDate = t1.SewingDate,
                             ExtraRate = t1.ExtraRate,
                             Remarks = t1.Remarks
                         }).OrderByDescending(a => a.ID).ToList();
            if (vData.Any())
            {
                vData.ForEach(a => {
                    a.IsReference = CheckLineExePlan(a.ID);
                    if (a.Merchandising_SetPackFK > 0)
                    {
                        var vSubSet = _db.Merchandising_StyleSetPack.FirstOrDefault(x => x.ID == a.Merchandising_SetPackFK);
                        a.ItemName = vSubSet.SetPackName;
                    }
                });
            }
            VMOrderPlanning vMReference = new VMOrderPlanning
            {
                DataList = vData
            };

            return vMReference;
        }

        public VMPlanView GetMasterPlan()
        {
            var VM = new VMPlanView();
            VM.vmOrderPlanning = new VMOrderPlanning();
            VM.ListOrderPlan = new List<VMOrderPlanning>();
            VM.ListOrderPlan = GetAllPrePlan();
            VM.vmOrderPlanning = GetAllMasterPlan();
            return VM;
        }

        public VMShipmentProductionPlan StylePlanByID(int ID)
        {
            var vConfig = GetPlanConfig().Result;
            var vData = (from t1 in _db.Plan_MasterOrderPlan
                         join t2 in _db.Merchandising_Style on t1.Merchandising_StyleFK equals t2.ID
                         join t3 in _db.Merchandising_BuyerOrder on t2.Merchandising_BuyerOrderFK equals t3.ID
                         join t4 in _db.Common_Buyer on t3.Common_BuyerFK equals t4.ID
                         where t1.ID == ID && t1.Active == true
                         select new VMShipmentProductionPlan
                         {
                             ID = t1.ID,
                             Style = t4.BuyerID + "/" + t3.BuyerPO + "/" + t2.StyleName,
                             Merchandising_StyleFK = t1.Merchandising_StyleFK,
                             SewingDate = t1.SewingDate,
                             DeliveryDate = t1.ShipmentDate,
                             PlanQty = t1.PlanQty
                         }).FirstOrDefault();

            //vData.DeliveryDate = vData.DeliveryDate.AddDays(-vConfig.ReservedDay);

            var vPlan = (from t1 in _db.Plan_MasterLinePlan
                         join t2 in _db.Plan_MasterOrderPlan on t1.PLan_MasterOrderPlanFK equals t2.ID
                         join t3 in _db.Common_ProductionLine on t1.Common_ProductionLineFK equals t3.ID
                         where t1.PLan_MasterOrderPlanFK == vData.ID && t1.Active == true
                         select new VMLinePlanning
                         {
                             ID = t1.ID,
                             ProdOrderPlanningFK = t1.PLan_MasterOrderPlanFK,
                             Merchandising_StyleFK = t2.Merchandising_StyleFK,
                             Plan_SMVLayoutFK = (int)t1.Plan_SMVMasterLayoutFK,
                             Capacity = t1.HourTargetPlan,
                             DayOutputQty = t1.DayOutputQty,
                             Description = t1.Remarks,
                             FinishDate = t1.FinishDate,
                             StartDate = t1.StartDate,
                             MachineQuantity = t1.NoOfOperator,
                             ProductionLineFK = t1.Common_ProductionLineFK,
                             RemainQty = t1.RemainQty,
                             TargetDays = t1.TargetDays,
                             TargetOutputQty = t1.TargetOutputQty,
                             WorkingHour = t1.WorkingHour,
                             OccupiedHour = (int)t1.LineHour,
                             LineName = t3.Name,
                             IsPaused = t1.IsPaused,
                             IsClosed = t1.IsClosed,
                             Status = t1.IsPaused == true ? "Paused" : "Running"
                         }).ToList();

            if (vPlan.Any())
            {
                vData.PlanList = vPlan.ToList();
                vData.SMVLayoutFK = vPlan.FirstOrDefault().Plan_SMVLayoutFK;

                var vPlanLine = (from t1 in vPlan
                                 group t1 by new { t1.LineName, t1.ProductionLineFK } into all
                                 select new { ID = all.Key.ProductionLineFK, Name = all.Key.LineName }).ToList();

                string[] LineIds = new string[vPlanLine.Count];
                for (int i = 0; i < vPlanLine.Count; i++)
                {
                    LineIds[i] = vPlanLine[i].ID.ToString();
                }
                MultiSelectList lstLine = new MultiSelectList(DDLSewingLine(), "Value", "Text", LineIds);
                vData.LineList = lstLine;
                vData.ActionId = 2;
            }
            else
            {
                MultiSelectList lstLine = new MultiSelectList(DDLSewingLine(), "Value", "Text");
                vData.LineList = lstLine;
            }
            vData.LinePlanChart = new List<VMLineSchedule>();
            vData.LinePlanChart = GetLinePlanChart(vData.SewingDate, vData.DeliveryDate, false);

            return vData;
        }

        public async Task<int> ExecutionPlanAdd(List<VMOrderPlanning> lstItem)
        {
            var result = -1;
            if (lstItem.Any(a => a.IsReference == true))
            {
                List<Plan_MasterOrderPlan> lstSaveItem = new List<Plan_MasterOrderPlan>();
                foreach (var v in lstItem.Where(a => a.IsReference == true))
                {
                    Plan_MasterOrderPlan dPlan = new Plan_MasterOrderPlan();
                    dPlan.Plan_DraftOrderPlanFK = v.ID;
                    dPlan.Merchandising_StyleFK = v.Merchandising_StyleFK;
                    dPlan.Merchandising_StyleSetPackFK = v.Merchandising_SetPackFK;
                    dPlan.SampleApprovedDate = v.SampleApprovedDate;
                    dPlan.AccessoriesInDate = v.AccessoriesInhouseDate;
                    dPlan.CuttingDate = v.CuttingStartDate;
                    dPlan.FabricInDate = v.FabricInhouseDate;
                    dPlan.PlanQty = v.OrderPlanQty;
                    dPlan.ShipmentDate = v.ShipmentDate;
                    dPlan.SewingDate = v.SewingStartDate;
                    dPlan.ExtraRate = v.ExtraRate;
                    dPlan.Remarks = v.Remarks;
                    dPlan.Active = true;
                    _db.Plan_MasterOrderPlan.Add(dPlan);

                    var vLine = _db.Plan_DraftLinePlan.Where(a => a.Plan_DraftOrderPlanFK == v.ID && a.Active == true);
                    if (vLine.Any())
                    {
                        int SMVID = (int)vLine.FirstOrDefault().Plan_SMVDraftLayoutFK;
                        var vLayout = _db.Plan_SMVDraftLayout.Where(a => a.Merchandising_StyleFK == v.Merchandising_StyleFK && a.ID == SMVID && a.Active == true);

                        if (vLayout.Any())
                        {
                            var tlayout = vLayout.FirstOrDefault();

                            var CheckMarsterLayoutSMV = _db.Plan_SMVMasterLayout.Where(a => a.Merchandising_StyleFK == tlayout.Merchandising_StyleFK && a.PLan_SMVDraftLayoutFK == tlayout.ID && a.Active == true);
                            if (!CheckMarsterLayoutSMV.Any())
                            {
                                Plan_SMVMasterLayout smvLayout = new Plan_SMVMasterLayout();
                                smvLayout.PLan_SMVDraftLayoutFK = tlayout.ID;
                                smvLayout.Merchandising_StyleFK = tlayout.Merchandising_StyleFK;
                                smvLayout.Merchandising_StyleSetPackFK = tlayout.Merchandising_StyleSetPackFK;
                                smvLayout.NoOfOperator = tlayout.NoOfOperator;
                                smvLayout.NoOfHelper = tlayout.NoOfHelper;
                                smvLayout.OperatorSMV = tlayout.OperatorSMV;
                                smvLayout.HelperSMV = tlayout.HelperSMV;
                                smvLayout.TotalWorker = tlayout.TotalWorker;
                                smvLayout.TotalSMV = tlayout.TotalSMV;
                                smvLayout.WorkingHour = tlayout.WorkingHour;
                                smvLayout.PerHourTarget = tlayout.PerHourTarget;
                                smvLayout.Efficiency = tlayout.Efficiency;
                                smvLayout.Remarks = tlayout.Remarks;
                                smvLayout.Active = true;
                                _db.Plan_SMVMasterLayout.Add(smvLayout);

                                foreach (var line in vLine)
                                {
                                    Plan_MasterLinePlan mLine = new Plan_MasterLinePlan();
                                    mLine.PLan_MasterOrderPlanFK = dPlan.ID;
                                    mLine.Plan_SMVMasterLayoutFK = smvLayout.ID;
                                    mLine.Common_ProductionLineFK = line.Common_ProductionLineFK;
                                    mLine.StartDate = line.StartDate;
                                    mLine.FinishDate = line.FinishDate;
                                    mLine.DayOutputQty = line.DayOutputQty;
                                    mLine.HourTargetPlan = line.HourTargetPlan;
                                    mLine.NoOfOperator = line.NoOfOperator;
                                    mLine.TargetDays = line.TargetDays;
                                    mLine.TargetOutputQty = line.TargetOutputQty;
                                    mLine.LineHour = line.LineHour;
                                    mLine.WorkingHour = line.WorkingHour;
                                    mLine.RemainQty = line.RemainQty;
                                    mLine.Remarks = line.Remarks;
                                    mLine.Active = true;
                                    _db.Plan_MasterLinePlan.Add(mLine);
                                }
                            }
                            else
                            {
                                foreach (var line in vLine)
                                {
                                    Plan_MasterLinePlan mLine = new Plan_MasterLinePlan();
                                    mLine.PLan_MasterOrderPlanFK = dPlan.ID;
                                    mLine.Plan_SMVMasterLayoutFK = CheckMarsterLayoutSMV.FirstOrDefault().ID;
                                    mLine.Common_ProductionLineFK = line.Common_ProductionLineFK;
                                    mLine.StartDate = line.StartDate;
                                    mLine.FinishDate = line.FinishDate;
                                    mLine.DayOutputQty = line.DayOutputQty;
                                    mLine.HourTargetPlan = line.HourTargetPlan;
                                    mLine.NoOfOperator = line.NoOfOperator;
                                    mLine.TargetDays = line.TargetDays;
                                    mLine.TargetOutputQty = line.TargetOutputQty;
                                    mLine.LineHour = line.LineHour;
                                    mLine.WorkingHour = line.WorkingHour;
                                    mLine.RemainQty = line.RemainQty;
                                    mLine.Remarks = line.Remarks;
                                    mLine.Active = true;
                                    _db.Plan_MasterLinePlan.Add(mLine);
                                }
                            }
                        }
                    }
                }
                //_db.Plan_MasterOrderPlan.AddRange(lstSaveItem);

                if (await _db.SaveChangesAsync() > 0)
                {
                    result = lstItem.FirstOrDefault().Merchandising_StyleFK;
                }
            }
            return result;
        }

        public async Task<int> ExecutionPlanDelete(int ID)
        {
            var result = -1;
            Plan_MasterOrderPlan pObj = _db.Plan_MasterOrderPlan.Find(ID);
            if (pObj != null)
            {
                pObj.Active = false;

                var mLinePlan = _db.Plan_MasterLinePlan.Where(a => a.PLan_MasterOrderPlanFK == pObj.ID);
                if (mLinePlan.Any())
                {
                    foreach (var v in mLinePlan)
                    {
                        v.Active = false;
                    }
                }

                if (await _db.SaveChangesAsync() > 0)
                {
                    result = pObj.ID;
                }
            }
            return result;
        }

        public async Task<string> ChangeLineStatus(int Id, bool State)
        {
            string result = string.Empty;
            var update = _db.Plan_MasterLinePlan.Find(Id);

            if (update != null)
            {
                if (State)
                {
                    update.IsPaused = false;
                }
                else
                {
                    update.IsPaused = true;
                }
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = update.IsPaused == true ? "Paused" : "Running";
                }
            }
            return result;
        }

        //public List<VMLinePlanning> GetAutoLinePlan(int SMVID, int ShipmentID, List<int> LineIDs)
        //{
        //    List<VMLinePlanning> lst = new List<VMLinePlanning>();
        //    //var vShipment = _db.Plan_MasterOrderPlan.FirstOrDefault(a => a.ID == ShipmentID);
        //    var vSMV = _db.Plan_SMVMasterLayout.FirstOrDefault(a => a.ID == SMVID);
        //    var vConfig = _db.Plan_PlanConfig.FirstOrDefault();
        //    var vLine = _db.Common_ProductionLine.Where(a => a.ProductionStepFK == 2 && a.Active == true).ToList();

        //    var vAllLinePlan = (from t1 in _db.Plan_MasterLinePlan
        //                        join t2 in _db.Plan_MasterOrderPlan on t1.PLan_MasterOrderPlanFK equals t2.ID
        //                        where t1.Active == true && t1.StartDate >= vShipment.SewingDate && t1.StartDate <= vShipment.ShipmentDate.AddDays(-vConfig.ReservedDate)
        //                        select new
        //                        {
        //                            LineID = t1.Common_ProductionLineFK,
        //                            FromDate = t1.StartDate,
        //                            ToDate = t1.FinishDate,
        //                            styleId = t2.Merchandising_StyleFK,
        //                            lineHour = t1.LineHour
        //                        }).ToList();
        //    int Totaldays = 0;
        //    decimal countDay = decimal.Divide(vShipment.PlanQty, decimal.Multiply(vSMV.WorkingHour, vSMV.PerHourTarget));
        //    int tday = (int)countDay;
        //    if (tday < countDay)
        //    {
        //        Totaldays = tday + 1;
        //    }

        //    decimal countHour = decimal.Divide(vShipment.PlanQty, vSMV.PerHourTarget);
        //    int tHour = (int)countHour;
        //    int TotalHour = 0;
        //    TotalHour = tHour < countHour == true ? ++tHour : 0;

        //    if (LineIDs.Any())
        //    {
        //        decimal LinePlanQty = vShipment.PlanQty;
        //        int NoOFLine = LineIDs.Count();
        //        if (NoOFLine == 1)
        //        {
        //            int ProductionHour = TotalHour;
        //            int lineID = LineIDs[0];
        //            string LineName = vLine.FirstOrDefault(a => a.ID == lineID).Name;
        //            var vLinePlan = vAllLinePlan.Where(a => a.LineID == lineID).ToList();
        //            int CountDays = Totaldays;

        //            if (vLinePlan.Any())
        //            {
        //                DateTime StartSewingDate = new DateTime();
        //                DateTime FinishSewingDate = new DateTime();
        //                DateTime startDate = vShipment.SewingDate;
        //                DateTime finishDate = vShipment.ShipmentDate.AddDays(vConfig.ReservedDate);

        //                while (startDate <= finishDate)
        //                {
        //                    if (CountDays == Totaldays)
        //                    {
        //                        StartSewingDate = startDate;
        //                    }
        //                    if (CountDays == 1)
        //                    {
        //                        FinishSewingDate = startDate;
        //                        startDate = finishDate;
        //                    }

        //                    --CountDays;
        //                    if (vLinePlan.Any(a => a.FromDate <= startDate && a.ToDate >= finishDate))
        //                    {
        //                        CountDays = Totaldays;
        //                    }
        //                    startDate = startDate.AddDays(1);
        //                }

        //                VMLinePlanning m = new VMLinePlanning();
        //                m.OccupiedHour = ProductionHour;
        //                m.StartDate = StartSewingDate;
        //                m.FinishDate = FinishSewingDate;
        //                m.Merchandising_StyleFK = vSMV.Merchandising_StyleFK;
        //                m.Plan_SMVLayoutFK = vSMV.ID;
        //                m.ProductionLineFK = lineID;
        //                m.LineName = LineName;
        //                m.TargetDays = Totaldays;
        //                m.Capacity = vSMV.PerHourTarget;
        //                //m.DayOutputQty = vSMV.WorkingHour * vSMV.PerHourTarget;
        //                m.DayOutputQty = Decimal.Multiply(m.OccupiedHour, vSMV.PerHourTarget);
        //                //m.TargetOutputQty = Totaldays * m.DayOutputQty;
        //                m.TargetOutputQty = m.DayOutputQty;
        //                m.WorkingHour = vSMV.WorkingHour;
        //                m.MachineQuantity = vSMV.NoOfOperator;
        //                m.ProdOrderPlanningFK = vShipment.ID;
        //                m.RemainQty = vShipment.PlanQty - m.TargetOutputQty;
        //                lst.Add(m);
        //            }
        //            else
        //            {
        //                VMLinePlanning m = new VMLinePlanning();
        //                m.OccupiedHour = ProductionHour;
        //                m.StartDate = vShipment.SewingDate;
        //                m.FinishDate = vShipment.SewingDate.AddDays(Totaldays - 1);
        //                m.Merchandising_StyleFK = vSMV.Merchandising_StyleFK;
        //                m.Plan_SMVLayoutFK = vSMV.ID;
        //                m.ProductionLineFK = lineID;
        //                m.LineName = LineName;
        //                m.TargetDays = Totaldays;
        //                m.Capacity = vSMV.PerHourTarget;
        //                m.DayOutputQty = Decimal.Multiply(m.OccupiedHour, vSMV.PerHourTarget);
        //                m.TargetOutputQty = m.DayOutputQty;
        //                m.WorkingHour = vSMV.WorkingHour;
        //                m.MachineQuantity = vSMV.NoOfOperator;
        //                m.ProdOrderPlanningFK = vShipment.ID;
        //                m.RemainQty = vShipment.PlanQty - m.TargetOutputQty;
        //                lst.Add(m);
        //            }
        //        }
        //        else
        //        {
        //            DateTime StartSewingDate = new DateTime();
        //            DateTime FinishSewingDate = new DateTime();
        //            DateTime startDate = vShipment.SewingDate;
        //            DateTime finishDate = vShipment.ShipmentDate.AddDays(vConfig.ReservedDate);

        //            decimal countLineDay = Decimal.Round(decimal.Divide(Totaldays, NoOFLine), 2);
        //            int LineTotalDay = (int)countLineDay;
        //            int LineDays = LineTotalDay >= countLineDay ? LineTotalDay : ++LineTotalDay;

        //            decimal TotalPHour = Decimal.Round(decimal.Divide(TotalHour, NoOFLine), 2);
        //            int ProdHour = (int)TotalPHour;
        //            int ProductionHour = ProdHour >= TotalPHour ? ProdHour : ++ProdHour;

        //            foreach (var v in LineIDs)
        //            {
        //                string LineName = vLine.FirstOrDefault(a => a.ID == v).Name;
        //                var vLinePlan = vAllLinePlan.Where(a => a.LineID == v).ToList();
        //                int count = LineDays;

        //                if (vLinePlan.Any())
        //                {
        //                    //DateTime StartSewingDate = new DateTime();
        //                    //DateTime FinishSewingDate = new DateTime();
        //                    //DateTime startDate = vShipment.SewingDate;
        //                    //DateTime finishDate = vShipment.ShipmentDate.AddDays(vConfig.ReservedDate);

        //                    while (startDate <= finishDate)
        //                    {
        //                        if (count == LineDays)
        //                        {
        //                            StartSewingDate = startDate;
        //                        }
        //                        if (count == 1)
        //                        {
        //                            FinishSewingDate = startDate;
        //                            startDate = finishDate;
        //                        }
        //                        --count;
        //                        if (vLinePlan.Any(a => a.FromDate <= startDate && a.ToDate >= finishDate))
        //                        {
        //                            count = LineDays;
        //                        }
        //                        startDate = startDate.AddDays(1);
        //                    }
        //                    VMLinePlanning m = new VMLinePlanning();
        //                    m.StartDate = StartSewingDate;
        //                    m.FinishDate = FinishSewingDate;
        //                    m.Merchandising_StyleFK = vSMV.Merchandising_StyleFK;
        //                    m.Plan_SMVLayoutFK = vSMV.ID;
        //                    m.ProductionLineFK = v;
        //                    m.LineName = LineName;
        //                    m.TargetDays = LineDays;
        //                    m.Capacity = vSMV.PerHourTarget;
        //                    m.OccupiedHour = (int)ProductionHour;
        //                    //m.DayOutputQty = vSMV.WorkingHour * vSMV.PerHourTarget;
        //                    m.DayOutputQty = decimal.Multiply(m.OccupiedHour, vSMV.PerHourTarget);
        //                    //m.TargetOutputQty = LineDays * m.DayOutputQty;
        //                    m.TargetOutputQty = m.DayOutputQty;
        //                    m.WorkingHour = vSMV.WorkingHour;
        //                    m.MachineQuantity = vSMV.NoOfOperator;
        //                    m.ProdOrderPlanningFK = vShipment.ID;
        //                    m.RemainQty = LinePlanQty - m.TargetOutputQty;
        //                    LinePlanQty = m.RemainQty;
        //                    lst.Add(m);
        //                }
        //                else
        //                {
        //                    //DateTime startDate = vShipment.SewingDate;
        //                    //DateTime finishDate = vShipment.ShipmentDate.AddDays(vConfig.ReservedDate);
        //                    while (startDate <= finishDate)
        //                    {
        //                        if (count == LineDays)
        //                        {
        //                            StartSewingDate = startDate;
        //                        }
        //                        if (count == 1)
        //                        {
        //                            FinishSewingDate = startDate;
        //                            startDate = finishDate;
        //                        }
        //                        --count;
        //                        if (vLinePlan.Any(a => a.FromDate <= startDate && a.ToDate >= finishDate))
        //                        {
        //                            count = LineDays;
        //                        }
        //                        startDate = startDate.AddDays(1);
        //                    }
        //                    VMLinePlanning m = new VMLinePlanning();
        //                    m.OccupiedHour = ProductionHour;
        //                    m.StartDate = StartSewingDate;
        //                    m.FinishDate = FinishSewingDate;
        //                    m.Merchandising_StyleFK = vSMV.Merchandising_StyleFK;
        //                    m.Plan_SMVLayoutFK = vSMV.ID;
        //                    m.ProductionLineFK = v;
        //                    m.LineName = LineName;
        //                    m.TargetDays = LineDays;
        //                    m.Capacity = vSMV.PerHourTarget;
        //                    //m.DayOutputQty = vSMV.WorkingHour * vSMV.PerHourTarget;
        //                    m.DayOutputQty = decimal.Multiply(m.OccupiedHour, vSMV.PerHourTarget);
        //                    //m.TargetOutputQty = LineDays * m.DayOutputQty;
        //                    m.TargetOutputQty = m.DayOutputQty;
        //                    m.WorkingHour = vSMV.WorkingHour;
        //                    m.MachineQuantity = vSMV.NoOfOperator;
        //                    m.ProdOrderPlanningFK = vShipment.ID;
        //                    m.RemainQty = LinePlanQty - m.TargetOutputQty;

        //                    LinePlanQty = m.RemainQty;
        //                    lst.Add(m);
        //                }
        //            }
        //        }
        //    }

        //    return lst;
        //}

        public async Task<int> LineMasterPlanEdit(VMShipmentProductionPlan model)
        {
            int result = -1;
            var vConfig = _db.Plan_PlanConfig.FirstOrDefault();
            decimal tempRemainQty = model.PlanQty;
            List<VMLinePlanning> lst = new List<VMLinePlanning>();
            var vAllLinePlan = (from t1 in _db.Plan_MasterLinePlan
                                join t2 in _db.Plan_MasterOrderPlan on t1.PLan_MasterOrderPlanFK equals t2.ID
                                where t1.Active == true && t1.StartDate >= model.SewingDate && t1.StartDate <= model.DeliveryDate.AddDays(-vConfig.ReservedDate)
                                select new VMSupportPlan
                                {
                                    LineID = t1.Common_ProductionLineFK,
                                    FromDate = t1.StartDate,
                                    ToDate = t1.FinishDate,
                                    StyleID = t2.Merchandising_StyleFK,
                                    WorkingHour = (int)t1.WorkingHour,
                                    HourOccupied = (int)t1.LineHour
                                }).ToList();
            VMSupportPlan modelobj = new VMSupportPlan();
            modelobj.StyleID = model.Merchandising_StyleFK;
            modelobj.FromDate = model.SewingDate;
            modelobj.ToDate = model.DeliveryDate.AddDays(-vConfig.ReservedDate);
            modelobj.WorkingHour = model.WorkingHour;
            modelobj.HourTarget = (int)model.PerHourTarget;

            var smvLayout = _db.Plan_SMVMasterLayout.FirstOrDefault(a => a.ID == model.SMVLayoutFK);


            if (model.PlanList != null)
            {
                foreach (var v1 in model.LineIDs)
                {
                    int id = 0;
                    int.TryParse(v1, out id);
                    if (model.PlanList.Any(a => a.ProductionLineFK == id))
                    {
                        var updateInfo = model.PlanList.FirstOrDefault(a => a.ProductionLineFK == id);
                        lst.Add(new VMLinePlanning { ProductionLineFK = id, IsPaused = true, ID = updateInfo.ID, WorkingHour = updateInfo.WorkingHour });
                    }
                    else
                    {
                        lst.Add(new VMLinePlanning { ProductionLineFK = id, IsPaused = false });
                    }

                }
                //modelobj.PlanQty = (int)model.PlanList.OrderByDescending(a => a.ID).FirstOrDefault().RemainQty;
                modelobj.PlanQty = (int)model.PlanQty;
                lst.ForEach(v =>
                {
                    if (v.IsPaused)
                    {
                        modelobj.WorkingHour = (int)v.WorkingHour;
                        var vLinePlan = vAllLinePlan.Where(a => a.LineID == v.ProductionLineFK).ToList();
                        VMSupportPlan data = GetWorkRange(modelobj, vLinePlan);
                        var updatePlan = _db.Plan_MasterLinePlan.FirstOrDefault(a => a.ID == v.ID);
                        updatePlan.StartDate = data.FromDate;
                        updatePlan.FinishDate = data.ToDate;
                        updatePlan.DayOutputQty = Decimal.Multiply(v.WorkingHour, smvLayout.PerHourTarget);
                        updatePlan.HourTargetPlan = smvLayout.PerHourTarget;
                        updatePlan.NoOfOperator = smvLayout.NoOfOperator;
                        updatePlan.TargetDays = data.DayRange;
                        updatePlan.TargetOutputQty = Decimal.Multiply(data.HourOccupied, smvLayout.PerHourTarget);
                        updatePlan.LineHour = data.HourOccupied;
                        updatePlan.WorkingHour = v.WorkingHour;
                        updatePlan.RemainQty = data.PlanQty - updatePlan.TargetOutputQty;
                        updatePlan.Remarks = "Auto Update";
                        updatePlan.Active = true;
                        modelobj.PlanQty = (int)updatePlan.RemainQty;
                    }
                    else//(!v.IsPaused)
                    {
                        var vLinePlan = vAllLinePlan.Where(a => a.LineID == v.ProductionLineFK).ToList();
                        VMSupportPlan data = GetWorkRange(modelobj, vLinePlan);
                        Plan_MasterLinePlan mLine = new Plan_MasterLinePlan();
                        mLine.PLan_MasterOrderPlanFK = model.ID;
                        mLine.Plan_SMVMasterLayoutFK = model.SMVLayoutFK;
                        mLine.Common_ProductionLineFK = v.ProductionLineFK;
                        mLine.StartDate = data.FromDate;
                        mLine.FinishDate = data.ToDate;
                        mLine.DayOutputQty = Decimal.Multiply(smvLayout.WorkingHour, smvLayout.PerHourTarget);
                        mLine.HourTargetPlan = smvLayout.PerHourTarget;
                        mLine.NoOfOperator = smvLayout.NoOfOperator;
                        mLine.TargetDays = data.DayRange;
                        mLine.TargetOutputQty = Decimal.Multiply(data.HourOccupied, smvLayout.PerHourTarget);
                        mLine.LineHour = data.HourOccupied;
                        mLine.WorkingHour = smvLayout.WorkingHour;
                        mLine.RemainQty = data.PlanQty - mLine.TargetOutputQty;
                        mLine.Remarks = "Auto";
                        mLine.Active = true;
                        //model.PlanQty = (int)mLine.RemainQty;
                        modelobj.PlanQty = (int)mLine.RemainQty;
                        _db.Plan_MasterLinePlan.Add(mLine);
                    }
                });
            }
            else
            {
                modelobj.PlanQty = (int)model.PlanQty;
                foreach (var v1 in model.LineIDs)
                {
                    int id = 0;
                    int.TryParse(v1, out id);
                    lst.Add(new VMLinePlanning { ProductionLineFK = id, IsPaused = false });
                }
                lst.ForEach(v =>
                {
                    if (!v.IsPaused)
                    {
                        var vLinePlan = vAllLinePlan.Where(a => a.LineID == v.ProductionLineFK).ToList();
                        VMSupportPlan data = GetWorkRange(modelobj, vLinePlan);
                        Plan_MasterLinePlan mLine = new Plan_MasterLinePlan();
                        mLine.PLan_MasterOrderPlanFK = model.ID;
                        mLine.Plan_SMVMasterLayoutFK = model.SMVLayoutFK;
                        mLine.Common_ProductionLineFK = v.ProductionLineFK;
                        mLine.StartDate = data.FromDate;
                        mLine.FinishDate = data.ToDate;
                        mLine.DayOutputQty = Decimal.Multiply(smvLayout.WorkingHour, smvLayout.PerHourTarget);
                        mLine.HourTargetPlan = smvLayout.PerHourTarget;
                        mLine.NoOfOperator = smvLayout.NoOfOperator;
                        mLine.TargetDays = data.DayRange;
                        mLine.TargetOutputQty = Decimal.Multiply(data.HourOccupied, smvLayout.PerHourTarget);
                        mLine.LineHour = data.HourOccupied;
                        mLine.WorkingHour = smvLayout.WorkingHour;
                        mLine.RemainQty = data.PlanQty - mLine.TargetOutputQty;
                        mLine.Remarks = "Auto";
                        mLine.Active = true;
                        model.PlanQty = (int)mLine.RemainQty;
                        _db.Plan_MasterLinePlan.Add(mLine);
                    }
                });
            }

            if (await _db.SaveChangesAsync() > 0)
            {
                result = model.Merchandising_StyleFK;
            }

            return result;
        }

        #endregion

        #region PrePlan

        public VMOrderPlanning GetAllPrePlanStyle()
        {
            VMAutoPlan vConfig = GetPlanConfig().Result;
            var vData = (from t1 in _db.Plan_DraftOrderPlan
                         join t2 in _db.Merchandising_Style on t1.Merchandising_StyleFK equals t2.ID
                         join t3 in _db.Merchandising_BuyerOrder on t2.Merchandising_BuyerOrderFK equals t3.ID
                         join t4 in _db.Merchandising_StyleSetPack on t1.Merchandising_StyleSetPackFK equals t4.ID into allTable
                         from t6 in allTable.DefaultIfEmpty()
                         where t1.Active == true && t2.Active == true
                         select new VMOrderPlanning
                         {
                             ID = t1.ID,
                             Merchandising_StyleFK = t1.Merchandising_StyleFK,
                             Merchandising_SetPackFK = t1.Merchandising_StyleSetPackFK == null ? 0 : (int)t1.Merchandising_StyleSetPackFK,
                             OrderNumber = t3.BuyerPO,
                             ItemName = t6.SetPackName,
                             StyleNo = t2.CID + "/" + t3.BuyerPO + "/" + t2.StyleName,
                             SampleApprovedDate = t1.SampleApprovedDate,
                             AccessoriesInhouseDate = t1.AccessoriesInDate,
                             CuttingStartDate = t1.CuttingDate,
                             FabricInhouseDate = t1.FabricInDate,
                             OrderPlanQty = (int)t1.PlanQty,
                             ShipmentDate = t1.ShipmentDate,
                             SewingStartDate = t1.SewingDate,
                             Remarks = t1.Remarks,
                             ExtraRate = t1.ExtraRate
                         }).OrderByDescending(a => a.ID).ToList();

            if (vData.Any())
            {
                vData.ForEach(a =>
                {
                    a.IsReference = CheckLinePrePlan(a.ID);
                });
            }
            VMOrderPlanning vMReference = new VMOrderPlanning
            {
                DataList = vData
            };

            return vMReference;
        }

        public List<VMOrderPlanning> GetAllPendingPrePlanStyle()
        {
            List<VMOrderPlanning> lst = new List<VMOrderPlanning>();
            VMAutoPlan vConfig = GetPlanConfig().Result;
            var vPlanData = (from t1 in _db.Plan_DraftOrderPlan
                             join t2 in _db.Merchandising_Style on t1.Merchandising_StyleFK equals t2.ID
                             join t3 in _db.Merchandising_BuyerOrder on t2.Merchandising_BuyerOrderFK equals t3.ID
                             where t1.Active == true && t2.Active == true
                             select new VMOrderPlanning
                             {
                                 ID = t1.ID,
                                 Merchandising_StyleFK = t1.Merchandising_StyleFK,
                                 Merchandising_SetPackFK = t1.Merchandising_StyleSetPackFK == null ? 0 : (int)t1.Merchandising_StyleSetPackFK,
                                 OrderNumber = t3.BuyerPO,
                                 StyleNo = t2.CID + "/" + t3.BuyerPO + "/" + t2.StyleName,
                                 OrderPlanQty = (int)t1.PlanQty,
                                 ShipmentDate = t1.ShipmentDate
                             }).OrderByDescending(a => a.ShipmentDate).ToList();

            var vData = (from t1 in _db.Merchandising_StyleShipmentSchedule
                         join t2 in _db.Merchandising_Style on t1.Merchandising_StyleFK equals t2.ID
                         join t3 in _db.Merchandising_BuyerOrder on t2.Merchandising_BuyerOrderFK equals t3.ID
                         join t5 in _db.Merchandising_StyleShipmentRatio on t1.ID equals t5.Merchandising_StyleShipmentScheduleFK
                         where t1.Active == true && t2.Active == true && t5.Active == true
                         group t5.Quantity by new { t5.Merchandising_StyleSetPackFK, t1.ShipmentDate, t1.Merchandising_StyleFK, t2.StyleName, t3.BuyerPO, t2.CID, t2.PackPieceQty } into all
                         select new VMOrderPlanning
                         {
                             Merchandising_SetPackFK = all.Key.Merchandising_StyleSetPackFK == null ? 0 : (int)all.Key.Merchandising_StyleSetPackFK,
                             Merchandising_StyleFK = all.Key.Merchandising_StyleFK,
                             OrderNumber = all.Key.BuyerPO,
                             StyleNo = all.Key.CID + "/" + all.Key.BuyerPO + "/" + all.Key.StyleName,
                             PackPieceQty = all.Key.PackPieceQty,
                             PlanQty = all.Sum(),
                             ShipmentDate = all.Key.ShipmentDate,
                         }).OrderByDescending(a => a.ShipmentDate).ToList();

            if (vData.Any())
            {
                foreach (var v in vData)
                {
                    if (v.Merchandising_SetPackFK > 0)
                    {
                        var vSet = _db.Merchandising_StyleSetPack.Where(a => a.Merchandising_StyleFK == v.Merchandising_StyleFK && a.ID == v.Merchandising_SetPackFK);
                        if (vSet.Any())
                        {
                            var take = vSet.FirstOrDefault();
                            v.ItemName = take.SetPackName;
                            v.SetPackQty = take.Quantity;
                            v.OrderPlanQty = (int)decimal.Multiply(decimal.Multiply(v.PackPieceQty, v.SetPackQty), v.PlanQty);
                        }
                    }
                    else
                    {
                        v.OrderPlanQty = (int)decimal.Multiply(v.PackPieceQty, v.PlanQty);
                    }
                    bool isFound = vPlanData.Any(a => a.Merchandising_StyleFK == v.Merchandising_StyleFK && a.ShipmentDate == v.ShipmentDate && a.OrderPlanQty == v.OrderPlanQty && a.Merchandising_SetPackFK == v.Merchandising_SetPackFK);
                    if (!isFound)
                    {
                        v.ExtraRate = vConfig.ExtraRate;
                        v.CuttingStartDate = v.ShipmentDate.AddDays(-vConfig.CuttingBFShip);
                        v.SewingStartDate = v.CuttingStartDate.AddDays(vConfig.SewingAFCutting);
                        v.AccessoriesInhouseDate = v.CuttingStartDate.AddDays(-vConfig.AccessoriesINBFCutting);
                        v.FabricInhouseDate = v.CuttingStartDate.AddDays(-vConfig.FabricINBFCutting);
                        v.SampleApprovedDate = v.CuttingStartDate.AddDays(-vConfig.PPSampleBFCutting);
                        lst.Add(v);
                    }
                }
            }

            return lst.OrderBy(a => a.ShipmentDate).ToList();
        }

        private bool CheckLinePrePlan(int ID)
        {
            var vCheck = _db.Plan_DraftLinePlan.Any(a => a.Plan_DraftOrderPlanFK == ID && a.Active == true);
            if (vCheck)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool CheckLineExePlan(int ID)
        {
            var vCheck = _db.Plan_MasterLinePlan.Any(a => a.PLan_MasterOrderPlanFK == ID && a.Active == true);
            if (vCheck)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public VMPlanView GetPrePlan()
        {
            var VM = new VMPlanView();
            VM.vmOrderPlanning = new VMOrderPlanning();
            VM.ListOrderPlan = new List<VMOrderPlanning>();
            VM.vmOrderPlanning = GetAllPrePlanStyle();
            VM.ListOrderPlan = GetAllPendingPrePlanStyle();
            return VM;
        }

        public async Task<VMPlanView> PlanGetByStyleID(int ID)
        {
            var VM = new VMPlanView();
            VM.vmOrderPlanning = new VMOrderPlanning();
            VM.vmSMVLayout = new VMSMVLayout();
            VM.ListOrderPlan = new List<VMOrderPlanning>();
            VM.vmSMVLayout = SMVLayoutGetByStyleID(ID);
            VM.vmOrderPlanning = await GetPrePlanGetByStyleID(ID);
            VM.ListOrderPlan = await GetStyleAllPendingPrePlanByStyleID(ID);
            return VM;
        }

        public async Task<List<VMOrderPlanning>> GetStyleAllPendingPrePlanByStyleID(int ID)
        {
            List<VMOrderPlanning> lst = new List<VMOrderPlanning>();
            VMAutoPlan vConfig = GetPlanConfig().Result;
            var vPlanData = await Task.Run(() => (from t1 in _db.Plan_DraftOrderPlan
                                                  join t2 in _db.Merchandising_Style on t1.Merchandising_StyleFK equals t2.ID
                                                  join t3 in _db.Merchandising_BuyerOrder on t2.Merchandising_BuyerOrderFK equals t3.ID
                                                  join t4 in _db.Merchandising_StyleShipmentSchedule on t1.Merchandising_StyleShipmentScheduleFK equals t4.ID
                                                  where t1.Merchandising_StyleFK == ID && t1.Active == true
                                                  select new VMOrderPlanning
                                                  {
                                                      Merchandising_StyleShipmentScheduleFK = (int)t1.Merchandising_StyleShipmentScheduleFK,
                                                      Merchandising_StyleFK = t1.Merchandising_StyleFK,
                                                      StyleNo = t2.CID + "/" + t3.BuyerPO + "/" + t2.StyleName,
                                                      OrderPlanQty = (int)t1.PlanQty,
                                                      ShipmentDate = t4.ShipmentDate
                                                  }).OrderByDescending(a => a.ID).AsEnumerable());

            var vData = await Task.Run(() => (from t1 in _db.Merchandising_StyleShipmentSchedule
                                              join t2 in _db.Merchandising_Style on t1.Merchandising_StyleFK equals t2.ID
                                              join t3 in _db.Merchandising_BuyerOrder on t2.Merchandising_BuyerOrderFK equals t3.ID
                                              where t1.Merchandising_StyleFK == ID && t1.Active == true
                                              select new VMOrderPlanning
                                              {
                                                  Merchandising_StyleShipmentScheduleFK = t1.ID,
                                                  Merchandising_StyleFK = t1.Merchandising_StyleFK,
                                                  StyleNo = t2.CID + "/" + t3.BuyerPO + "/" + t2.StyleName,
                                                  OrderPlanQty = (int)t1.Quantity,
                                                  ShipmentDate = t1.ShipmentDate
                                              }).OrderByDescending(a => a.ID).AsEnumerable());

            if (vData.Any())
            {
                foreach (var v in vData)
                {
                    if (!vPlanData.Any(a => a.Merchandising_StyleShipmentScheduleFK == v.Merchandising_StyleShipmentScheduleFK))
                    {
                        v.CuttingStartDate = v.ShipmentDate.AddDays(-vConfig.CuttingBFShip);
                        v.SewingStartDate = v.CuttingStartDate.AddDays(vConfig.SewingAFCutting);
                        v.AccessoriesInhouseDate = v.CuttingStartDate.AddDays(-vConfig.AccessoriesINBFCutting);
                        v.FabricInhouseDate = v.CuttingStartDate.AddDays(-vConfig.FabricINBFCutting);
                        v.SampleApprovedDate = v.CuttingStartDate.AddDays(-vConfig.PPSampleBFCutting);
                        lst.Add(v);
                    }
                }
            }

            return lst;
        }

        public async Task<VMOrderPlanning> GetPrePlanGetByStyleID(int StyleID)
        {
            var vData = (from t1 in _db.Plan_DraftOrderPlan
                         join t2 in _db.Merchandising_Style on t1.Merchandising_StyleFK equals t2.ID
                         join t3 in _db.Merchandising_BuyerOrder on t2.Merchandising_BuyerOrderFK equals t3.ID
                         where t1.Merchandising_StyleFK == StyleID && t1.Active == true
                         select new VMOrderPlanning
                         {
                             ID = t1.ID,
                             Merchandising_StyleFK = t1.Merchandising_StyleFK,
                             OrderNumber = t3.BuyerPO,
                             StyleNo = t2.CID + "/" + t3.BuyerPO + "/" + t2.StyleName,
                             SampleApprovedDate = t1.SampleApprovedDate,
                             AccessoriesInhouseDate = t1.AccessoriesInDate,
                             CuttingStartDate = t1.CuttingDate,
                             FabricInhouseDate = t1.FabricInDate,
                             OrderPlanQty = (int)t1.PlanQty,
                             ShipmentDate = t1.ShipmentDate,
                             SewingStartDate = t1.SewingDate,
                             Remarks = t1.Remarks
                         }).OrderByDescending(a => a.ID).ToList();

            VMOrderPlanning vMReference = new VMOrderPlanning
            {
                DataList = await Task.Run(() => vData)
            };

            return vMReference;
        }

        public VMShipmentProductionPlan StylePrePlanByID(int ID)
        {
            var vConfig = GetPlanConfig().Result;
            var vData = (from t1 in _db.Plan_DraftOrderPlan
                         join t2 in _db.Merchandising_Style on t1.Merchandising_StyleFK equals t2.ID
                         join t3 in _db.Merchandising_BuyerOrder on t2.Merchandising_BuyerOrderFK equals t3.ID
                         where t1.ID == ID && t1.Active == true
                         select new VMShipmentProductionPlan
                         {
                             ID = t1.ID,
                             Style = t2.CID + "/" + t3.BuyerPO + "/" + t2.StyleName,
                             Merchandising_StyleFK = t1.Merchandising_StyleFK,
                             SewingDate = t1.SewingDate,
                             DeliveryDate = t1.ShipmentDate,
                             PlanQty = t1.PlanQty
                         }).FirstOrDefault();

            vData.DeliveryDate = vData.DeliveryDate.AddDays(-vConfig.ReservedDay);

            var vPlan = (from t1 in _db.Plan_DraftLinePlan
                         join t2 in _db.Plan_SMVDraftLayout on t1.Plan_SMVDraftLayoutFK equals t2.ID
                         join t3 in _db.Common_ProductionLine on t1.Common_ProductionLineFK equals t3.ID
                         where t1.Plan_DraftOrderPlanFK == vData.ID && t1.Active == true
                         select new VMLinePlanning
                         {
                             ID = t1.ID,
                             ProdOrderPlanningFK = t1.Plan_DraftOrderPlanFK,
                             Plan_SMVLayoutFK = (int)t1.Plan_SMVDraftLayoutFK,
                             Merchandising_StyleFK = t2.Merchandising_StyleFK,
                             Capacity = t1.HourTargetPlan,
                             DayOutputQty = t1.DayOutputQty,
                             Description = t1.Remarks,
                             FinishDate = t1.FinishDate,
                             StartDate = t1.StartDate,
                             MachineQuantity = t1.NoOfOperator,
                             ProductionLineFK = t1.Common_ProductionLineFK,
                             RemainQty = t1.RemainQty,
                             TargetDays = t1.TargetDays,
                             TargetOutputQty = t1.TargetOutputQty,
                             WorkingHour = t1.WorkingHour,
                             OccupiedHour = (int)t1.LineHour,
                             LineName = t3.Name
                         }).ToList();

            if (vPlan.Any())
            {
                vData.PlanList = vPlan.ToList();
                vData.SMVLayoutFK = vPlan.FirstOrDefault().Plan_SMVLayoutFK;

                var vPlanLine = (from t1 in vPlan
                                 group t1 by new { t1.LineName, t1.ProductionLineFK } into all
                                 select new { ID = all.Key.ProductionLineFK, Name = all.Key.LineName }).ToList();

                string[] LineIds = new string[vPlanLine.Count];
                for (int i = 0; i < vPlanLine.Count; i++)
                {
                    LineIds[i] = vPlanLine[i].ID.ToString();
                }
                MultiSelectList lstLine = new MultiSelectList(DDLSewingLine(), "Value", "Text", LineIds);
                vData.LineList = lstLine;
                vData.ActionId = 2;
            }
            else
            {
                MultiSelectList lstLine = new MultiSelectList(DDLSewingLine(), "Value", "Text");
                vData.LineList = lstLine;
            }

            vData.LinePlanChart = new List<VMLineSchedule>();
            vData.LinePlanChart = GetLinePlanChart(vData.SewingDate, vData.DeliveryDate, true);

            return vData;
        }

        #region StylePrePlan

        public async Task<int> StylePrePlanAdd(List<VMOrderPlanning> lstItem)
        {
            var result = -1;
            if (lstItem.Any(a => a.IsReference == true))
            {
                List<Plan_DraftOrderPlan> lstSaveItem = new List<Plan_DraftOrderPlan>();
                foreach (var v in lstItem.Where(a => a.IsReference == true))
                {
                    Plan_DraftOrderPlan dPlan = new Plan_DraftOrderPlan();
                    dPlan.Merchandising_StyleFK = v.Merchandising_StyleFK;
                    //dPlan.Merchandising_StyleShipmentScheduleFK = v.Merchandising_StyleShipmentScheduleFK;
                    dPlan.Merchandising_StyleSetPackFK = v.Merchandising_SetPackFK;
                    dPlan.SampleApprovedDate = v.SampleApprovedDate;
                    dPlan.AccessoriesInDate = v.AccessoriesInhouseDate;
                    dPlan.CuttingDate = v.CuttingStartDate;
                    dPlan.FabricInDate = v.FabricInhouseDate;
                    dPlan.PlanQty = v.OrderPlanQty;
                    dPlan.ShipmentDate = v.ShipmentDate;
                    dPlan.SewingDate = v.SewingStartDate;
                    dPlan.ExtraRate = v.ExtraRate;
                    dPlan.Remarks = v.Remarks;
                    dPlan.Active = true;
                    lstSaveItem.Add(dPlan);
                }
                _db.Plan_DraftOrderPlan.AddRange(lstSaveItem);

                if (await _db.SaveChangesAsync() > 0)
                {
                    result = lstItem.FirstOrDefault().Merchandising_StyleFK;
                }
            }
            return result;
        }

        public int StylePrePlanEdit(VMOrderPlanning vmOPlan)
        {
            var result = -1;

            return result;
        }

        public async Task<int> StylePrePlanDelete(int ID)
        {
            var result = -1;
            Plan_DraftOrderPlan pObj = _db.Plan_DraftOrderPlan.Find(ID);
            if (pObj != null)
            {
                pObj.Active = false;
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = pObj.ID;
                }
            }
            return result;
        }

        #endregion

        #region LinePrePlan

        public async Task<int> PreLinePlanAdd(List<VMLinePlanning> lstLinePlan)
        {
            var result = -1;
            if (lstLinePlan.Any())
            {
                foreach (var v in lstLinePlan)
                {
                    Plan_DraftLinePlan dPlan = new Plan_DraftLinePlan();
                    dPlan.Plan_DraftOrderPlanFK = v.ProdOrderPlanningFK;
                    dPlan.Common_ProductionLineFK = v.ProductionLineFK;
                    dPlan.Plan_SMVDraftLayoutFK = v.Plan_SMVLayoutFK;
                    dPlan.StartDate = v.StartDate;
                    dPlan.FinishDate = v.FinishDate;
                    dPlan.DayOutputQty = v.DayOutputQty;
                    dPlan.HourTargetPlan = v.Capacity;
                    dPlan.NoOfOperator = v.MachineQuantity;
                    dPlan.TargetDays = v.TargetDays;
                    dPlan.TargetOutputQty = v.TargetOutputQty;
                    dPlan.LineHour = v.OccupiedHour;
                    dPlan.WorkingHour = v.WorkingHour;
                    dPlan.RemainQty = v.RemainQty;
                    dPlan.Remarks = v.Description;
                    dPlan.Active = true;
                    _db.Plan_DraftLinePlan.Add(dPlan);
                }
            }

            if (await _db.SaveChangesAsync() > 0)
            {
                result = lstLinePlan.FirstOrDefault().Merchandising_StyleFK;
            }
            return result;
        }

        public async Task<int> LinePrePlanEdit(VMShipmentProductionPlan model)
        {
            var result = -1;
            if (model.ListLinePlan.Any())
            {
                foreach (var v in model.ListLinePlan)
                {
                    if (!model.PlanList.Any(a => a.ProductionLineFK == v.ProductionLineFK && a.ProdOrderPlanningFK == v.ProdOrderPlanningFK && a.Merchandising_StyleFK == v.Merchandising_StyleFK))
                    {
                        //Add Line Plan
                        Plan_DraftLinePlan dPlan = new Plan_DraftLinePlan();
                        dPlan.Plan_DraftOrderPlanFK = v.ProdOrderPlanningFK;
                        dPlan.Common_ProductionLineFK = v.ProductionLineFK;
                        dPlan.Plan_SMVDraftLayoutFK = v.Plan_SMVLayoutFK;
                        dPlan.StartDate = v.StartDate;
                        dPlan.FinishDate = v.FinishDate;
                        dPlan.DayOutputQty = v.DayOutputQty;
                        dPlan.HourTargetPlan = v.Capacity;
                        dPlan.NoOfOperator = v.MachineQuantity;
                        dPlan.TargetDays = v.TargetDays;
                        dPlan.TargetOutputQty = v.TargetOutputQty;
                        dPlan.WorkingHour = v.WorkingHour;
                        dPlan.LineHour = v.OccupiedHour;
                        dPlan.RemainQty = v.RemainQty;
                        dPlan.Remarks = v.Description;
                        dPlan.Active = true;
                        _db.Plan_DraftLinePlan.Add(dPlan);
                    }
                }
            }

            if (model.PlanList.Any())
            {
                foreach (var v in model.PlanList)
                {
                    if (model.ListLinePlan.Any(a => a.ProductionLineFK == v.ProductionLineFK && a.ProdOrderPlanningFK == v.ProdOrderPlanningFK && a.Merchandising_StyleFK == v.Merchandising_StyleFK))
                    {
                        //update Line Plan
                        VMLinePlanning update = model.ListLinePlan.FirstOrDefault(a => a.ProductionLineFK == v.ProductionLineFK && a.ProdOrderPlanningFK == v.ProdOrderPlanningFK && a.Merchandising_StyleFK == v.Merchandising_StyleFK);
                        Plan_DraftLinePlan dPlan = _db.Plan_DraftLinePlan.Find(v.ID);
                        dPlan.StartDate = update.StartDate;
                        dPlan.FinishDate = update.FinishDate;
                        dPlan.DayOutputQty = update.DayOutputQty;
                        dPlan.HourTargetPlan = update.Capacity;
                        dPlan.NoOfOperator = update.MachineQuantity;
                        dPlan.TargetDays = update.TargetDays;
                        dPlan.TargetOutputQty = update.TargetOutputQty;
                        dPlan.WorkingHour = update.WorkingHour;
                        dPlan.RemainQty = update.RemainQty;
                        dPlan.LineHour = update.OccupiedHour;
                        dPlan.Remarks = update.Description;
                        dPlan.Active = true;
                    }
                    else
                    {
                        //delete Line Plan From DB
                        Plan_DraftLinePlan pObj = _db.Plan_DraftLinePlan.Find(v.ID);
                        pObj.Remarks = "Automatic Delete";
                        pObj.Active = false;
                    }
                }
            }

            if (await _db.SaveChangesAsync() > 0)
            {
                result = model.Merchandising_StyleFK;
            }
            return result;
        }

        #endregion

        #endregion

        #region ProductionLine
        private string GetStepName(int ID)
        {
            string name = string.Empty;
            switch (ID)
            {
                case 1:
                    name = "Cutting";
                    break;
                case 2:
                    name = "Sewing";
                    break;
                case 3:
                    name = "Ironing";
                    break;
                case 4:
                    name = "Packing";
                    break;
                default:
                    name = string.Empty;
                    break;
            }
            return name;
        }
        public VMProductionLine ProductionLineGet()
        {
            var vData = (from t1 in _db.Common_ProductionLine
                         join t2 in _db.Common_SectionLine
                         on t1.Common_SectionLineFK equals t2.ID
                         where t1.Active == true
                         select new VMProductionLine
                         {
                             ID = t1.ID,
                             Plan_ProductionStepFK = (int)t1.ProductionStepFK,
                             Name = t1.Name,
                             Code = t1.Code,
                             Priority = t1.Priority,
                             Description = t1.Remarks,
                             Common_SectionLineFK = t2.ID,
                             SectionLine = t2.Name

                         }).OrderBy(a => a.Plan_ProductionStepFK).ThenByDescending(a => a.Priority).ToList();



            vData.ForEach(a =>
            {
                a.Step = GetStepName(a.Plan_ProductionStepFK);
            });

            VMProductionLine productionLine = new VMProductionLine
            {
                DataList = vData
            };
            return productionLine;
        }
        public async Task<int> ProductionLineAdd(VMProductionLine vMLine)
        {
            var result = -1;
            Common_ProductionLine pObj = new Common_ProductionLine();
            pObj.ProductionStepFK = vMLine.Plan_ProductionStepFK;
            pObj.Name = vMLine.Name;
            pObj.Code = CidServices.ProLineCode(vMLine.Plan_ProductionStepFK,vMLine.Common_SectionLineFK);
            pObj.Priority = vMLine.Priority;
            pObj.Remarks = vMLine.Description;
            pObj.Common_SectionLineFK = vMLine.Common_SectionLineFK;
            pObj.Active = true;
            _db.Common_ProductionLine.Add(pObj);

            if (await _db.SaveChangesAsync() > 0)
            {
                result = pObj.ID;
            }
            return result;
        }
        public async Task<int> ProductionLineEdit(VMProductionLine vMLine)
        {
            var result = -1;
            Common_ProductionLine pObj = _db.Common_ProductionLine.Find(vMLine.ID);

            pObj.ProductionStepFK = vMLine.Plan_ProductionStepFK;
            pObj.Name = vMLine.Name;
            pObj.Priority = vMLine.Priority;
            pObj.Remarks = vMLine.Description;
            pObj.Common_SectionLineFK = vMLine.Common_SectionLineFK;
            pObj.Active = true;

            if (await _db.SaveChangesAsync() > 0)
            {
                result = pObj.ID;
            }
            return result;
        }
        public async Task<int> ProductionLineDelete(int ID)
        {
            var result = -1;
            Common_ProductionLine pObj = _db.Common_ProductionLine.Find(ID);
            if (pObj != null)
            {
                pObj.Active = false;
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = pObj.ID;
                }
            }
            return result;
        }
        #endregion

        #region ProductionLineDevice
        public VMLineDevice LineDeviceGetByID(int ID)
        {
            var vData = (from t1 in _db.Common_LineDevice
                         join t2 in _db.Common_ProductionLine on t1.Common_ProductionLineFK equals t2.ID
                         where t1.Active == true && t1.ID==ID
                         select new VMLineDevice
                         {
                             ID = t1.ID,
                             Name = t1.Name,
                             DeviceID = t1.DeviceID,
                             Remarks = t1.Remarks,
                             Common_ProductionLineFK = t1.Common_ProductionLineFK,
                             ActionId=(int)ActionEnum.Edit
                         }).FirstOrDefault();

            return vData;
        }

        public VMLineDevice LineDeviceGet()
        {
            var vData = (from t1 in _db.Common_LineDevice
                         join t2 in _db.Common_ProductionLine on t1.Common_ProductionLineFK equals t2.ID
                         where t1.Active == true
                         select new VMLineDevice
                         {
                             ID = t1.ID,
                             Name = t1.Name,
                             DeviceID = t1.DeviceID,
                             Remarks = t1.Remarks,
                             Common_ProductionLineFK = t1.Common_ProductionLineFK,
                             ProductionLine = t2.Name +"-"+t2.Code
                         }).OrderBy(a => a.ID).ToList();

            VMLineDevice LineDevice = new VMLineDevice
            {
                DataList = vData
            };
            return LineDevice;
        }

        public async Task<int> LineDeviceAdd(VMLineDevice vMLine)
        {
            var result = -1;
            Common_LineDevice pObj = new Common_LineDevice();
            pObj.Common_ProductionLineFK = vMLine.Common_ProductionLineFK;
            pObj.Name = vMLine.Name;
            pObj.DeviceID = vMLine.DeviceID;
            pObj.Remarks = vMLine.Remarks;
            pObj.Active = true;
            _db.Common_LineDevice.Add(pObj);

            if (await _db.SaveChangesAsync() > 0)
            {
                result = pObj.ID;
            }
            return result;
        }

        public async Task<int> LineDeviceEdit(VMLineDevice vMLine)
        {
            var result = -1;
            Common_LineDevice pObj = _db.Common_LineDevice.Find(vMLine.ID);
            pObj.Common_ProductionLineFK = vMLine.Common_ProductionLineFK;
            pObj.Name = vMLine.Name;
            pObj.DeviceID = vMLine.DeviceID;
            pObj.Remarks = vMLine.Remarks;
            pObj.Active = true;

            if (await _db.SaveChangesAsync() > 0)
            {
                result = pObj.ID;
            }
            return result;
        }

        public async Task<int> LineDeviceDelete(int ID)
        {
            var result = -1;
            Common_LineDevice pObj = _db.Common_LineDevice.Find(ID);
            if (pObj != null)
            {
                pObj.Active = false;
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = pObj.ID;
                }
            }
            return result;
        }
        #endregion

        #region LineChiefLine

        public List<VMLineChiefLine> GetAllLineByLineChiefID(int ID)
        {
            var lineChief = (from t1 in _db.Common_LineChiefLine
                             join t2 in _db.Common_LineChief on t1.Common_LineChiefFk equals t2.ID
                             join t4 in _db.Common_ProductionLine on t1.Common_ProductionLineFk equals t4.ID
                             where t1.Active == true && t1.IsLineChief == true && t1.Common_LineChiefFk == ID
                             select new VMLineChiefLine
                             {
                                 ID = t1.ID,
                                 LineChief = t2.Name,
                                 Common_LineChiefFk = (int)t1.Common_LineChiefFk,
                                 ProductionLine = t4.Name,
                                 Common_ProductionLineFk = t1.Common_ProductionLineFk,
                                 IsLineChief = t1.IsLineChief
                             }).ToList();
            return lineChief;
        }

        public VMLineChiefLine GetLineChiefLine(int ID)
        {
            VMLineChiefLine lineChiefLine = new VMLineChiefLine();
            var vData = _db.Common_LineChief.Where(a => a.ID == ID).Select(a => new VMLineChiefLine
            {
                Common_LineChiefFk = a.ID,
                LineChief = a.Name

            }).FirstOrDefault();
            VMLineChiefLine vMLineChief = new VMLineChiefLine();
            vMLineChief.LineChief = vData.LineChief;
            vMLineChief.Common_LineChiefFk = vData.Common_LineChiefFk;
            vMLineChief.DataList = new List<VMLineChiefLine>();
            vMLineChief.DataList = GetAllLineByLineChiefID(ID);
            return vMLineChief;
        }
        public async Task<int> LineChiefLineAdd(VMLineChiefLine vMLineChiefLine)
        {
            var result = -1;

            Common_LineChiefLine ChiefLine = new Common_LineChiefLine();
            ChiefLine.Common_ProductionLineFk = vMLineChiefLine.Common_ProductionLineFk;
            ChiefLine.Common_LineChiefFk = vMLineChiefLine.ID;
            ChiefLine.IsLineChief = true;
            ChiefLine.User = "UserOne";

            _db.Common_LineChiefLine.Add(ChiefLine);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = ChiefLine.ID;
            }
            return result;
        }
        public async Task<int> LineChiefLineEdit(VMLineChiefLine vMLineChiefLine)
        {
            var result = -1;

            Common_LineChiefLine ChiefLine = _db.Common_LineChiefLine.Find(vMLineChiefLine.ID);

            ChiefLine.Common_ProductionLineFk = vMLineChiefLine.Common_ProductionLineFk;
            ChiefLine.IsLineChief = true;
            if (await _db.SaveChangesAsync() > 0)
            {
                result = ChiefLine.ID;
            }
            return result;
        }
        public async Task<int> LineChiefLineDelete(int id)
        {
            var result = -1;
            Common_LineChiefLine ChiefLine = _db.Common_LineChiefLine.Find(id);
            if (ChiefLine != null)
            {
                ChiefLine.Active = false;

                if (await _db.SaveChangesAsync() > 0)
                {
                    result = ChiefLine.ID;
                }
            }
            return result;
        }

        #endregion

        #region SupervisorLine
        public List<VMLineChiefLine> GetSupervisorLine(int id)
        {
            var lineChief = (from t1 in _db.Common_LineChiefLine
                             join t2 in _db.Common_LineSuperVisor on t1.Common_LineSuperVisorFk equals t2.ID
                             join t4 in _db.Common_ProductionLine on t1.Common_ProductionLineFk equals t4.ID
                             where t1.Active == true && t1.IsLineChief == false && t1.Common_LineSuperVisorFk == id
                             select new VMLineChiefLine
                             {
                                 ID = t1.ID,
                                 LineSuperVisor = t2.Name,
                                 Common_LineSuperVisorFk = t2.ID,
                                 ProductionLine = t4.Name,
                                 Common_ProductionLineFk = t4.ID,
                                 IsLineChief = t1.IsLineChief

                             }).ToList();

            return lineChief;
        }
        public VMLineChiefLine GetLineSuperVisorForProLine(int ID)
        {
            var vData = _db.Common_LineSuperVisor.Where(a => a.ID == ID).Select(a => new VMLineChiefLine
            {
                Common_LineSuperVisorFk = a.ID,
                LineSuperVisor = a.Name

            }).FirstOrDefault();
            VMLineChiefLine vMLineChief = new VMLineChiefLine();
            vMLineChief.LineSuperVisor = vData.LineSuperVisor;
            vMLineChief.Common_LineSuperVisorFk = vData.Common_LineSuperVisorFk;
            vMLineChief.DataList = new List<VMLineChiefLine>();
            vMLineChief.DataList = GetSupervisorLine(ID);
            return vMLineChief;
        }
        public async Task<int> SupervisorLineAdd(VMLineChiefLine vMLineChiefLine)
        {
            var result = -1;

            Common_LineChiefLine ChiefLine = new Common_LineChiefLine();
            ChiefLine.Common_ProductionLineFk = vMLineChiefLine.Common_ProductionLineFk;
            ChiefLine.Common_LineSuperVisorFk = vMLineChiefLine.ID;
            ChiefLine.IsLineChief = false;
            ChiefLine.User = "UserOne";

            _db.Common_LineChiefLine.Add(ChiefLine);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = ChiefLine.ID;
            }
            return result;
        }
        public async Task<int> SupervisorLineEdit(VMLineChiefLine vMLineChiefLine)
        {
            var result = -1;
            Common_LineChiefLine ChiefLine = new Common_LineChiefLine();
            ChiefLine = _db.Common_LineChiefLine.Find(vMLineChiefLine.ID);
            ChiefLine.Common_ProductionLineFk = vMLineChiefLine.Common_ProductionLineFk;

            ChiefLine.IsLineChief = false;
            if (await _db.SaveChangesAsync() > 0)
            {
                result = ChiefLine.ID;
            }
            return result;
        }
        public async Task<int> SupervisorLineDelete(int id)
        {
            var result = -1;
            Common_LineChiefLine ChiefLine = _db.Common_LineChiefLine.Find(id);
            if (ChiefLine != null)
            {
                ChiefLine.Active = false;

                if (await _db.SaveChangesAsync() > 0)
                {
                    result = ChiefLine.ID;
                }
            }
            return result;
        }

        #endregion

        #region LineChief
        private bool Checklinechief(int id)
        {
            var a = _db.Common_LineChiefLine.Any(c => c.Common_LineChiefFk == id && c.Active == true);

            if (a)
            {
                return true;
            }
            else
            {
                return false;
            }


        }
        public async Task<VMLineChief> GetLineChief()
        {

            var line = _db.Common_LineChief.Where(x => x.Active == true).ToList().ConvertAll(
                    x => new VMLineChief { ID = x.ID, Name = x.Name }).OrderByDescending(x => x.ID).ToList();



            line.ForEach(a =>
            {
                a.IsReference = Checklinechief(a.ID);
            });
            VMLineChief vMLineChief = new VMLineChief
            {
                DataList = await Task.Run(() => line)
            };

            return vMLineChief;
        }
        public async Task<int> LineChiefAdd(VMLineChief _LineChief)
        {
            var result = -1;

            Common_LineChief LineChief = new Common_LineChief
            {
                Name = _LineChief.Name,
                User = "UserOne",
            };
            _db.Common_LineChief.Add(LineChief);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = LineChief.ID;
            }
            return result;
        }
        public async Task<int> LineChiefEdit(VMLineChief _LineChief)
        {
            var result = -1;
            Common_LineChief LineChief = _db.Common_LineChief.Find(_LineChief.ID);
            LineChief.Name = _LineChief.Name;
            if (await _db.SaveChangesAsync() > 0)
            {
                result = LineChief.ID;
            }
            return result;
        }
        public async Task<int> LineChiefDelete(int id)
        {
            var result = -1;
            Common_LineChief _LineChief = _db.Common_LineChief.Find(id);
            if (_LineChief != null)
            {
                _LineChief.Active = false;
                //_db.Common_Unit.Remove(_unit);
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = _LineChief.ID;
                }
            }
            return result;
        }

        #region Error
        //public async Task<VMLineChiefLine> GetLineChiefForProLine(int ID)
        //{
        //    var vData = _db.Common_ProductionLine.Where(a => a.ID == ID).Select(a => new VMLineChiefLine
        //    {
        //        ID = a.ID,
        //        ProductionLine = a.Name,
        //        Priority = a.Priority,
        //        //Description = a.Remarks

        //    }).FirstOrDefault();

        //    return await Task.Run(() => vData);

        //}
        #endregion






        public async Task<VMLineChiefLine> GetLineChiefForProLine(int ID)
        {
            var vData = _db.Common_LineSuperVisor.Where(a => a.ID == ID).Select(a => new VMLineChiefLine
            {
                ID = a.ID,
                LineSuperVisor = a.Name,
            }).FirstOrDefault();

            //vData.DataList = GetSupervisorLine(ID);
            return await Task.Run(() => vData);


        }



        #endregion

        #region LineSuperVisor
        public async Task<VMLineSuperVisor> GetLineSuperVisor()
        {

            var supervisor = _db.Common_LineSuperVisor.Where(x => x.Active == true).ToList().ConvertAll(
                    x => new VMLineSuperVisor { ID = x.ID, Name = x.Name }).OrderByDescending(x => x.ID).ToList();

            supervisor.ForEach(a =>
            {
                a.IsReference = Checklinechief(a.ID);
            });
            VMLineSuperVisor vMLineSuperVisor = new VMLineSuperVisor
            {
                DataList = await Task.Run(() => supervisor)
            };

            return vMLineSuperVisor;

        }
        public async Task<int> LineSuperVisorAdd(VMLineSuperVisor _unit)
        {
            var result = -1;

            Common_LineSuperVisor LineSuperVisor = new Common_LineSuperVisor
            {
                Name = _unit.Name,
                User = "UserOne",
            };
            _db.Common_LineSuperVisor.Add(LineSuperVisor);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = LineSuperVisor.ID;
            }
            return result;
        }
        public async Task<int> LineSuperVisorEdit(VMLineSuperVisor _LineSuperVisor)
        {
            var result = -1;

            Common_LineSuperVisor LineSuperVisor = _db.Common_LineSuperVisor.Find(_LineSuperVisor.ID);

            LineSuperVisor.Name = _LineSuperVisor.Name;


            if (await _db.SaveChangesAsync() > 0)
            {
                result = LineSuperVisor.ID;
            }
            return result;
        }
        public async Task<int> LineSuperVisorDelete(int id)
        {
            var result = -1;
            Common_LineSuperVisor _LineSuperVisor = _db.Common_LineSuperVisor.Find(id);
            if (_LineSuperVisor != null)
            {
                _db.Common_LineSuperVisor.Remove(_LineSuperVisor);
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = _LineSuperVisor.ID;
                }
            }
            return result;
        }
        #endregion

        #region PlanConfig
        public VMPlanConfig GetPlanConfiguration()
        {
            var vData = _db.Plan_PlanConfig.Select(a => new VMPlanConfig
            {
                ID = a.ID,
                SewingAFCutting = a.SewingAFCutting,
                AccessoriesINBFCutting = a.AccessoriesINBFCutting,
                CuttingBFShip = a.CuttingBFShip,
                FabricINBFCutting = a.FabricINBFCutting,
                PPSampleBFCutting = a.PPSampleBFCutting,
                ReservedDay = a.ReservedDate,
                ExtraRate = a.ExtraRate,
                ActionId = (int)ActionEnum.Edit
            });
            if (vData.Any())
            {
                return vData.FirstOrDefault();
            }
            else
            {
                VMPlanConfig m = new VMPlanConfig();
                return m;
            }
        }

        public async Task<int> PlanConfigUpdate(VMPlanConfig pConfig)
        {
            var result = -1;
            if (pConfig.ActionId == (int)ActionEnum.Add)
            {
                Plan_PlanConfig pcon = new Plan_PlanConfig
                {
                    SewingAFCutting = pConfig.SewingAFCutting,
                    AccessoriesINBFCutting = pConfig.AccessoriesINBFCutting,
                    CuttingBFShip = pConfig.CuttingBFShip,
                    FabricINBFCutting = pConfig.FabricINBFCutting,
                    PPSampleBFCutting = pConfig.PPSampleBFCutting,
                    ReservedDate = pConfig.ReservedDay,
                    ExtraRate = pConfig.ExtraRate
                };
                _db.Plan_PlanConfig.Add(pcon);
            }
            else if (pConfig.ActionId == (int)ActionEnum.Edit)
            {
                Plan_PlanConfig pupdate = _db.Plan_PlanConfig.Find(pConfig.ID);
                pupdate.SewingAFCutting = pConfig.SewingAFCutting;
                pupdate.AccessoriesINBFCutting = pConfig.AccessoriesINBFCutting;
                pupdate.CuttingBFShip = pConfig.CuttingBFShip;
                pupdate.FabricINBFCutting = pConfig.FabricINBFCutting;
                pupdate.PPSampleBFCutting = pConfig.PPSampleBFCutting;
                pupdate.ReservedDate = pConfig.ReservedDay;
                pupdate.ExtraRate = pConfig.ExtraRate;
            }

            if (await _db.SaveChangesAsync() > 0)
            {
                result = pConfig.ID;
            }
            return result;
        }
        #endregion

        #region Helper

        public async Task<int> DeleteZeroFollowUp(int RefID, int SectionID)
        {
            int result = -1;
            var vData = _db.Prod_ReferencePlan.Where(t1 => t1.Active == true && t1.SectionId == SectionID).ToList();
            if (vData.Any())
            {
                foreach (var v in vData)
                {
                    var vDataDelete = _db.Prod_ReferencePlanFollowup.Where(t2 => t2.Quantity == 0 && t2.Active == true && t2.Prod_ReferencePlanFK == v.ID);
                    if (vDataDelete.Any())
                    {
                        foreach (var v1 in vDataDelete)
                        {
                            var vSlaveDelete = _db.Prod_ReferencePlanFollowupDetails.Where(a => a.Prod_ReferencePlanFollowupFK == v1.ID && a.Active == true);
                            if (vSlaveDelete.Any())
                            {
                                _db.Prod_ReferencePlanFollowupDetails.RemoveRange(vSlaveDelete);
                            }
                        }
                        _db.Prod_ReferencePlanFollowup.RemoveRange(vDataDelete);

                        if (await _db.SaveChangesAsync() > 0)
                        {
                            result = RefID;
                        }
                    }
                }
            }
            return result;
        }

        public async Task<VMAutoPlan> GetPlanConfig()
        {
            return await Task.Run(() => _db.Plan_PlanConfig.Select(a => new VMAutoPlan
            {
                SewingAFCutting = a.SewingAFCutting,
                AccessoriesINBFCutting = a.AccessoriesINBFCutting,
                CuttingBFShip = a.CuttingBFShip,
                FabricINBFCutting = a.FabricINBFCutting,
                PPSampleBFCutting = a.PPSampleBFCutting,
                ReservedDay = a.ReservedDate,
                ExtraRate = a.ExtraRate
            }).FirstOrDefault());
        }

        public List<VMLinePlanning> GetAutoLinePrePlan(int SMVID, int ShipmentID, List<int> LineIDs)
        {
            List<VMLinePlanning> lst = new List<VMLinePlanning>();

            var vShipment = _db.Plan_DraftOrderPlan.FirstOrDefault(a => a.ID == ShipmentID);
            var vSMV = _db.Plan_SMVDraftLayout.FirstOrDefault(a => a.ID == SMVID);
            var vConfig = _db.Plan_PlanConfig.FirstOrDefault();
            var vLine = _db.Common_ProductionLine.Where(a => a.ProductionStepFK == 2 && a.Active == true).ToList();

            var vAllLinePlan = (from t1 in _db.Plan_DraftLinePlan
                                join t2 in _db.Plan_DraftOrderPlan on t1.Plan_DraftOrderPlanFK equals t2.ID
                                where t1.Active == true && t1.StartDate >= vShipment.SewingDate && t1.StartDate <= vShipment.ShipmentDate.AddDays(-vConfig.ReservedDate)
                                select new
                                {
                                    LineID = t1.Common_ProductionLineFK,
                                    FromDate = t1.StartDate,
                                    ToDate = t1.FinishDate,
                                    styleId = t2.Merchandising_StyleFK,
                                    lineHour = t1.LineHour
                                }).ToList();
            int Totaldays = 0;
            decimal countDay = decimal.Divide(vShipment.PlanQty, decimal.Multiply(vSMV.WorkingHour, vSMV.PerHourTarget));
            int tday = (int)countDay;
            if (tday < countDay)
            {
                Totaldays = tday + 1;
            }

            decimal countHour = decimal.Divide(vShipment.PlanQty, vSMV.PerHourTarget);
            int tHour = (int)countHour;
            int TotalHour = 0;
            TotalHour = tHour < countHour == true ? ++tHour : 0;

            if (LineIDs.Any())
            {
                decimal LinePlanQty = vShipment.PlanQty;
                int NoOFLine = LineIDs.Count();
                if (NoOFLine == 1)
                {
                    int ProductionHour = TotalHour;
                    int lineID = LineIDs[0];
                    string LineName = vLine.FirstOrDefault(a => a.ID == lineID).Name;
                    var vLinePlan = vAllLinePlan.Where(a => a.LineID == lineID).ToList();
                    int CountDays = Totaldays;

                    if (vLinePlan.Any())
                    {
                        DateTime StartSewingDate = new DateTime();
                        DateTime FinishSewingDate = new DateTime();
                        DateTime startDate = vShipment.SewingDate;
                        DateTime finishDate = vShipment.ShipmentDate.AddDays(vConfig.ReservedDate);

                        while (startDate <= finishDate)
                        {
                            if (CountDays == Totaldays)
                            {
                                StartSewingDate = startDate;
                            }
                            if (CountDays == 1)
                            {
                                FinishSewingDate = startDate;
                                startDate = finishDate;
                            }

                            --CountDays;
                            if (vLinePlan.Any(a => a.FromDate <= startDate && a.ToDate >= finishDate))
                            {
                                CountDays = Totaldays;
                            }
                            startDate = startDate.AddDays(1);
                        }

                        VMLinePlanning m = new VMLinePlanning();
                        m.OccupiedHour = ProductionHour;
                        m.StartDate = StartSewingDate;
                        m.FinishDate = FinishSewingDate;
                        m.Merchandising_StyleFK = vSMV.Merchandising_StyleFK;
                        m.Plan_SMVLayoutFK = vSMV.ID;
                        m.ProductionLineFK = lineID;
                        m.LineName = LineName;
                        m.TargetDays = Totaldays;
                        m.Capacity = vSMV.PerHourTarget;
                        //m.DayOutputQty = vSMV.WorkingHour * vSMV.PerHourTarget;
                        m.DayOutputQty = Decimal.Multiply(m.OccupiedHour, vSMV.PerHourTarget);
                        //m.TargetOutputQty = Totaldays * m.DayOutputQty;
                        m.TargetOutputQty = m.DayOutputQty;
                        m.WorkingHour = vSMV.WorkingHour;
                        m.MachineQuantity = vSMV.NoOfOperator;
                        m.ProdOrderPlanningFK = vShipment.ID;
                        m.RemainQty = vShipment.PlanQty - m.TargetOutputQty;
                        lst.Add(m);
                    }
                    else
                    {
                        VMLinePlanning m = new VMLinePlanning();
                        m.OccupiedHour = ProductionHour;
                        m.StartDate = vShipment.SewingDate;
                        m.FinishDate = vShipment.SewingDate.AddDays(Totaldays - 1);
                        m.Merchandising_StyleFK = vSMV.Merchandising_StyleFK;
                        m.Plan_SMVLayoutFK = vSMV.ID;
                        m.ProductionLineFK = lineID;
                        m.LineName = LineName;
                        m.TargetDays = Totaldays;
                        m.Capacity = vSMV.PerHourTarget;
                        //m.DayOutputQty = vSMV.WorkingHour * vSMV.PerHourTarget;
                        m.DayOutputQty = Decimal.Multiply(m.OccupiedHour, vSMV.PerHourTarget);
                        //m.TargetOutputQty = Totaldays * m.DayOutputQty;
                        m.TargetOutputQty = m.DayOutputQty;
                        m.WorkingHour = vSMV.WorkingHour;
                        m.MachineQuantity = vSMV.NoOfOperator;
                        m.ProdOrderPlanningFK = vShipment.ID;
                        m.RemainQty = vShipment.PlanQty - m.TargetOutputQty;
                        lst.Add(m);
                    }
                }
                else
                {
                    DateTime StartSewingDate = new DateTime();
                    DateTime FinishSewingDate = new DateTime();
                    DateTime startDate = vShipment.SewingDate;
                    DateTime finishDate = vShipment.ShipmentDate.AddDays(vConfig.ReservedDate);

                    decimal countLineDay = Decimal.Round(decimal.Divide(Totaldays, NoOFLine), 2);
                    int LineTotalDay = (int)countLineDay;
                    int LineDays = LineTotalDay >= countLineDay ? LineTotalDay : ++LineTotalDay;

                    decimal TotalPHour = Decimal.Round(decimal.Divide(TotalHour, NoOFLine), 2);
                    int ProdHour = (int)TotalPHour;
                    int ProductionHour = ProdHour >= TotalPHour ? ProdHour : ++ProdHour;

                    foreach (var v in LineIDs)
                    {
                        string LineName = vLine.FirstOrDefault(a => a.ID == v).Name;
                        var vLinePlan = vAllLinePlan.Where(a => a.LineID == v).ToList();
                        int count = LineDays;

                        if (vLinePlan.Any())
                        {
                            //DateTime StartSewingDate = new DateTime();
                            //DateTime FinishSewingDate = new DateTime();
                            //DateTime startDate = vShipment.SewingDate;
                            //DateTime finishDate = vShipment.ShipmentDate.AddDays(vConfig.ReservedDate);

                            while (startDate <= finishDate)
                            {
                                if (count == LineDays)
                                {
                                    StartSewingDate = startDate;
                                }
                                if (count == 1)
                                {
                                    FinishSewingDate = startDate;
                                    startDate = finishDate;
                                }
                                --count;
                                if (vLinePlan.Any(a => a.FromDate <= startDate && a.ToDate >= finishDate))
                                {
                                    count = LineDays;
                                }
                                startDate = startDate.AddDays(1);
                            }
                            VMLinePlanning m = new VMLinePlanning();
                            m.StartDate = StartSewingDate;
                            m.FinishDate = FinishSewingDate;
                            m.Merchandising_StyleFK = vSMV.Merchandising_StyleFK;
                            m.Plan_SMVLayoutFK = vSMV.ID;
                            m.ProductionLineFK = v;
                            m.LineName = LineName;
                            m.TargetDays = LineDays;
                            m.Capacity = vSMV.PerHourTarget;
                            m.OccupiedHour = (int)ProductionHour;
                            //m.DayOutputQty = vSMV.WorkingHour * vSMV.PerHourTarget;
                            m.DayOutputQty = decimal.Multiply(m.OccupiedHour, vSMV.PerHourTarget);
                            //m.TargetOutputQty = LineDays * m.DayOutputQty;
                            m.TargetOutputQty = m.DayOutputQty;
                            m.WorkingHour = vSMV.WorkingHour;
                            m.MachineQuantity = vSMV.NoOfOperator;
                            m.ProdOrderPlanningFK = vShipment.ID;
                            m.RemainQty = LinePlanQty - m.TargetOutputQty;
                            LinePlanQty = m.RemainQty;
                            lst.Add(m);
                        }
                        else
                        {
                            //DateTime startDate = vShipment.SewingDate;
                            //DateTime finishDate = vShipment.ShipmentDate.AddDays(vConfig.ReservedDate);
                            while (startDate <= finishDate)
                            {
                                if (count == LineDays)
                                {
                                    StartSewingDate = startDate;
                                }
                                if (count == 1)
                                {
                                    FinishSewingDate = startDate;
                                    startDate = finishDate;
                                }
                                --count;
                                if (vLinePlan.Any(a => a.FromDate <= startDate && a.ToDate >= finishDate))
                                {
                                    count = LineDays;
                                }
                                startDate = startDate.AddDays(1);
                            }
                            VMLinePlanning m = new VMLinePlanning();
                            m.OccupiedHour = ProductionHour;
                            m.StartDate = StartSewingDate;
                            m.FinishDate = FinishSewingDate;
                            m.Merchandising_StyleFK = vSMV.Merchandising_StyleFK;
                            m.Plan_SMVLayoutFK = vSMV.ID;
                            m.ProductionLineFK = v;
                            m.LineName = LineName;
                            m.TargetDays = LineDays;
                            m.Capacity = vSMV.PerHourTarget;
                            //m.DayOutputQty = vSMV.WorkingHour * vSMV.PerHourTarget;
                            m.DayOutputQty = decimal.Multiply(m.OccupiedHour, vSMV.PerHourTarget);
                            //m.TargetOutputQty = LineDays * m.DayOutputQty;
                            m.TargetOutputQty = m.DayOutputQty;
                            m.WorkingHour = vSMV.WorkingHour;
                            m.MachineQuantity = vSMV.NoOfOperator;
                            m.ProdOrderPlanningFK = vShipment.ID;
                            m.RemainQty = LinePlanQty - m.TargetOutputQty;

                            LinePlanQty = m.RemainQty;
                            lst.Add(m);
                        }
                    }
                }
            }

            return lst;
        }

        public List<VMLinePlanning> GetAutoLinePrePlan1(int SMVID, int ShipmentID, List<int> LineIDs)
        {
            List<VMLinePlanning> lst = new List<VMLinePlanning>();
            var vShipment = _db.Plan_DraftOrderPlan.FirstOrDefault(a => a.ID == ShipmentID);
            var vSMV = _db.Plan_SMVDraftLayout.FirstOrDefault(a => a.ID == SMVID);
            var vConfig = _db.Plan_PlanConfig.FirstOrDefault();
            var vLine = _db.Common_ProductionLine.Where(a => a.ProductionStepFK == 2 && a.Active == true).ToList();

            var vAllLinePlan = (from t1 in _db.Plan_DraftLinePlan
                                join t2 in _db.Plan_DraftOrderPlan on t1.Plan_DraftOrderPlanFK equals t2.ID
                                where t1.Active == true && t1.StartDate >= vShipment.SewingDate && t1.StartDate <= vShipment.ShipmentDate.AddDays(-vConfig.ReservedDate)
                                select new
                                {
                                    LineID = t1.Common_ProductionLineFK,
                                    FromDate = t1.StartDate,
                                    ToDate = t1.FinishDate,
                                    styleId = t2.Merchandising_StyleFK
                                }).ToList();

            decimal countDay = decimal.Divide(vShipment.PlanQty, decimal.Multiply(vSMV.WorkingHour, vSMV.PerHourTarget));
            int tday = (int)countDay;
            //int Totaldays = 0;
            //if (tday < countDay)
            //{
            //    Totaldays = tday + 1;
            //}
            int Totaldays = tday < countDay ? ++tday : 0;


            if (LineIDs.Any())
            {
                decimal LinePlanQty = vShipment.PlanQty;
                int NoOFLine = LineIDs.Count();
                if (NoOFLine == 1)
                {
                    int lineID = LineIDs[0];
                    string LineName = vLine.FirstOrDefault(a => a.ID == lineID).Name;
                    var vLinePlan = vAllLinePlan.Where(a => a.LineID == lineID).ToList();
                    int CountDays = Totaldays;

                    if (vLinePlan.Any())
                    {
                        DateTime StartSewingDate = new DateTime();
                        DateTime FinishSewingDate = new DateTime();
                        DateTime startDate = vShipment.SewingDate;
                        DateTime finishDate = vShipment.ShipmentDate.AddDays(vConfig.ReservedDate);

                        while (startDate <= finishDate)
                        {
                            if (CountDays == Totaldays)
                            {
                                StartSewingDate = startDate;
                            }
                            if (CountDays == 1)
                            {
                                FinishSewingDate = startDate;
                                startDate = finishDate;
                            }
                            --CountDays;
                            if (vLinePlan.Any(a => a.FromDate <= startDate && a.ToDate >= finishDate))
                            {
                                CountDays = Totaldays;
                            }
                            startDate = startDate.AddDays(1);
                        }
                        VMLinePlanning m = new VMLinePlanning();
                        m.StartDate = StartSewingDate;
                        m.FinishDate = FinishSewingDate;
                        m.Merchandising_StyleFK = vSMV.Merchandising_StyleFK;
                        m.Plan_SMVLayoutFK = vSMV.ID;
                        m.ProductionLineFK = lineID;
                        m.LineName = LineName;
                        m.TargetDays = Totaldays;
                        m.Capacity = vSMV.PerHourTarget;
                        m.DayOutputQty = vSMV.WorkingHour * vSMV.PerHourTarget;
                        m.TargetOutputQty = Totaldays * m.DayOutputQty;
                        m.WorkingHour = vSMV.WorkingHour;
                        m.MachineQuantity = vSMV.NoOfOperator;
                        m.ProdOrderPlanningFK = vShipment.ID;
                        m.RemainQty = vShipment.PlanQty - m.TargetOutputQty;
                        lst.Add(m);
                    }
                    else
                    {
                        VMLinePlanning m = new VMLinePlanning();
                        m.StartDate = vShipment.SewingDate;
                        m.FinishDate = vShipment.SewingDate.AddDays(Totaldays);
                        m.Merchandising_StyleFK = vSMV.Merchandising_StyleFK;
                        m.Plan_SMVLayoutFK = vSMV.ID;
                        m.ProductionLineFK = lineID;
                        m.LineName = LineName;
                        m.TargetDays = Totaldays;
                        m.Capacity = vSMV.PerHourTarget;
                        m.DayOutputQty = vSMV.WorkingHour * vSMV.PerHourTarget;
                        m.TargetOutputQty = Totaldays * m.DayOutputQty;
                        m.WorkingHour = vSMV.WorkingHour;
                        m.MachineQuantity = vSMV.NoOfOperator;
                        m.ProdOrderPlanningFK = vShipment.ID;
                        m.RemainQty = vShipment.PlanQty - m.TargetOutputQty;
                        lst.Add(m);
                    }
                }
                else
                {
                    decimal countLineDay = Decimal.Round(decimal.Divide(Totaldays, NoOFLine), 2);
                    int LineTotalDay = (int)countLineDay;
                    int LineDays = LineTotalDay < countLineDay ? LineTotalDay + 1 : 0;

                    foreach (var v in LineIDs)
                    {
                        string LineName = vLine.FirstOrDefault(a => a.ID == v).Name;
                        var vLinePlan = vAllLinePlan.Where(a => a.LineID == v).ToList();
                        int count = LineDays;

                        if (vLinePlan.Any())
                        {
                            DateTime StartSewingDate = new DateTime();
                            DateTime FinishSewingDate = new DateTime();
                            DateTime startDate = vShipment.SewingDate;
                            DateTime finishDate = vShipment.ShipmentDate.AddDays(vConfig.ReservedDate);

                            while (startDate <= finishDate)
                            {
                                if (count == LineDays)
                                {
                                    StartSewingDate = startDate;
                                }
                                if (count == 1)
                                {
                                    FinishSewingDate = startDate;
                                    startDate = finishDate;
                                }
                                --count;
                                if (vLinePlan.Any(a => a.FromDate <= startDate && a.ToDate >= finishDate))
                                {
                                    count = LineDays;
                                }
                                startDate = startDate.AddDays(1);
                            }
                            VMLinePlanning m = new VMLinePlanning();
                            m.StartDate = StartSewingDate;
                            m.FinishDate = FinishSewingDate;
                            m.Merchandising_StyleFK = vSMV.Merchandising_StyleFK;
                            m.Plan_SMVLayoutFK = vSMV.ID;
                            m.ProductionLineFK = v;
                            m.LineName = LineName;
                            m.TargetDays = LineDays;
                            m.Capacity = vSMV.PerHourTarget;
                            m.DayOutputQty = vSMV.WorkingHour * vSMV.PerHourTarget;
                            m.TargetOutputQty = LineDays * m.DayOutputQty;
                            m.WorkingHour = vSMV.WorkingHour;
                            m.MachineQuantity = vSMV.NoOfOperator;
                            m.ProdOrderPlanningFK = vShipment.ID;
                            m.RemainQty = LinePlanQty - m.TargetOutputQty;
                            LinePlanQty = m.RemainQty;
                            lst.Add(m);
                        }
                        else
                        {
                            VMLinePlanning m = new VMLinePlanning();
                            m.StartDate = vShipment.SewingDate;
                            m.FinishDate = vShipment.SewingDate.AddDays(Totaldays);
                            m.Merchandising_StyleFK = vSMV.Merchandising_StyleFK;
                            m.Plan_SMVLayoutFK = vSMV.ID;
                            m.ProductionLineFK = v;
                            m.LineName = LineName;
                            m.TargetDays = LineDays;
                            m.Capacity = vSMV.PerHourTarget;
                            m.DayOutputQty = vSMV.WorkingHour * vSMV.PerHourTarget;
                            m.TargetOutputQty = LineDays * m.DayOutputQty;
                            m.WorkingHour = vSMV.WorkingHour;
                            m.MachineQuantity = vSMV.NoOfOperator;
                            m.ProdOrderPlanningFK = vShipment.ID;
                            m.RemainQty = LinePlanQty - m.TargetOutputQty;
                            LinePlanQty = m.RemainQty;
                            lst.Add(m);
                        }
                    }
                }
            }


            return lst;
        }

        public List<VMLinePlanHourlyChart> GetDailyLineHourlyOccupied(int RefID)
        {
            List<VMLinePlanHourlyChart> lstData = new List<VMLinePlanHourlyChart>();
            var vPlanOrder = (from t1 in _db.Prod_ReferencePlan
                              where t1.Prod_ReferenceFK==RefID && t1.Active==true
                              group t1 by new { t1.ProductionLineFK } into all
                              select new
                              {
                                  LineId = all.Key.ProductionLineFK,
                                  Work = (from o in all
                                          select new
                                          {
                                              WorkingHour = o.WorkingHour,
                                              StyleId = o.Merchandising_StyleFK
                                          }).ToList()

                              }).ToList();
            int maxWork = 0;
            if (vPlanOrder.Any())
            {
                maxWork = vPlanOrder.Max(a => a.Work.Sum(x => x.WorkingHour)) + 1;
            }
            else
            {
                maxWork = 9;
            }

            var vAllLine = _db.Common_ProductionLine.Where(a => a.ProductionStepFK == 2 && a.Active == true).OrderBy(a => a.Priority).ToList();
            if (vAllLine.Any())
            {
                foreach (var v in vAllLine)
                {
                    VMLinePlanHourlyChart vLine = new VMLinePlanHourlyChart();
                    vLine.LineName = v.Name;
                    vLine.LineID = v.ID;
                    vLine.DayColor = "background-color:blue; color:white;";
                    vLine.HourList = new List<VMLinePlanHourlyChart>();
                    if (vPlanOrder.Any(a => a.LineId == v.ID))
                    {
                        var vPWork = vPlanOrder.FirstOrDefault(a => a.LineId == v.ID).Work;
                        if (vPWork.Any())
                        {
                            int maxW = vPlanOrder.FirstOrDefault(a => a.LineId == v.ID).Work.Sum(a => a.WorkingHour) + 1;

                            vLine.LineWorkingHour = maxW - 1;
                            if (maxW < 5)
                            {
                                maxW = maxW - 1;
                            }
                            for (int i = 0; i < maxWork; i++)
                            {
                                if (i == 5)
                                {
                                    TimeSpan s1Time = new TimeSpan(8 + i, 00, 00);
                                    VMLinePlanHourlyChart vWorkHour = new VMLinePlanHourlyChart();
                                    vWorkHour.Hour = s1Time.ToString(@"hh\:mm");
                                    vWorkHour.DayColor = "background-color:orange;";
                                    vLine.HourList.Add(vWorkHour);
                                }
                                else
                                {
                                    if (i < maxW)
                                    {
                                        TimeSpan s1Time = new TimeSpan(8 + i, 00, 00);
                                        VMLinePlanHourlyChart vWorkHour = new VMLinePlanHourlyChart();
                                        vWorkHour.Hour = s1Time.ToString(@"hh\:mm");
                                        vWorkHour.DayColor = "background-color:darkred;";
                                        vLine.HourList.Add(vWorkHour);
                                    }
                                    else
                                    {
                                        TimeSpan s1Time = new TimeSpan(8 + i, 00, 00);
                                        VMLinePlanHourlyChart vWorkHour = new VMLinePlanHourlyChart();
                                        vWorkHour.Hour = s1Time.ToString(@"hh\:mm");
                                        vWorkHour.DayColor = "background-color:green;";
                                        vLine.HourList.Add(vWorkHour);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < maxWork; i++)
                        {
                            if (i == 5)
                            {
                                TimeSpan s1Time = new TimeSpan(8 + i, 00, 00);
                                VMLinePlanHourlyChart vWorkHour = new VMLinePlanHourlyChart();
                                vWorkHour.Hour = s1Time.ToString(@"hh\:mm");
                                vWorkHour.DayColor = "background-color:orange;";
                                vLine.HourList.Add(vWorkHour);
                            }
                            else
                            {
                                TimeSpan sTime = new TimeSpan(8 + i, 00, 00);

                                VMLinePlanHourlyChart vWorkHour = new VMLinePlanHourlyChart();
                                vWorkHour.Hour = sTime.ToString(@"hh\:mm");
                                vWorkHour.DayColor = "background-color:green;";
                                vLine.HourList.Add(vWorkHour);

                            }

                        }

                    }
                    lstData.Add(vLine);
                }
            }
            return lstData;
        }

        public DataTable GetLinePrePlanChart(DateTime SDate, DateTime TDate)
        {
            DataTable ds = new DataTable();
            ds.Columns.Add("Line/Date");

            DateTime fDate = SDate;
            DateTime tDate = TDate;
            int totalDay = 0;
            while (fDate <= tDate)
            {
                ++totalDay;
                int m = fDate.Month;
                int d = fDate.Day;
                string sDate = d + "/" + m;

                ds.Columns.Add(sDate);
                fDate = fDate.AddDays(1);
            }

            int count = 0;
            var getAllLines = _db.Common_ProductionLine.Where(a => a.Active == true && a.ProductionStepFK == 2).OrderBy(a => a.Priority).ToList();
            if (getAllLines.Any())
            {
                foreach (var v in getAllLines)
                {
                    var vPlan = (from t1 in _db.Plan_DraftLinePlan
                                 join t2 in _db.Plan_DraftOrderPlan on t1.Plan_DraftOrderPlanFK equals t2.ID
                                 join t4 in _db.Merchandising_Style on t2.Merchandising_StyleFK equals t4.ID
                                 join t5 in _db.Merchandising_BuyerOrder on t4.Merchandising_BuyerOrderFK equals t5.ID
                                 where t1.Active == true && t2.Active == true && t4.Active == true && t5.Active == true
                                 && t1.StartDate >= SDate && t1.StartDate <= TDate && t1.Common_ProductionLineFK == v.ID
                                 select new
                                 {
                                     BuyerPo = t5.BuyerPO,
                                     FromDate = t1.StartDate,
                                     ToDate = t1.FinishDate
                                 }).ToList();

                    DateTime fDate1 = SDate;
                    DateTime tDate1 = TDate;

                    ds.Rows.Add(v.Name);
                    int c = 0;
                    while (fDate1 <= tDate1)
                    {
                        c++;
                        if (vPlan.Any(a => a.FromDate <= fDate1 && a.ToDate >= fDate1))
                        {
                            var data = vPlan.FirstOrDefault(a => a.FromDate <= fDate1 && a.ToDate >= fDate1);
                            ds.Rows[count][c] = data.BuyerPo;
                        }
                        else
                        {
                            ds.Rows[count][c] = "Undefine";
                        }
                        fDate1 = fDate1.AddDays(1);
                    }
                    ++count;
                }
            }

            return ds;
        }

        public DataTable GetLinePlanChart(DateTime SDate, DateTime TDate)
        {
            DataTable ds = new DataTable();
            ds.Columns.Add("Line/Date");

            DateTime fDate = SDate;
            DateTime tDate = TDate;
            int totalDay = 0;
            while (fDate <= tDate)
            {
                ++totalDay;
                int m = fDate.Month;
                int d = fDate.Day;
                string sDate = d + "/" + m;

                ds.Columns.Add(sDate);
                fDate = fDate.AddDays(1);
            }

            int count = 0;
            var getAllLines = _db.Common_ProductionLine.Where(a => a.Active == true && a.ProductionStepFK == 2).OrderBy(a => a.Priority).ToList();
            if (getAllLines.Any())
            {
                foreach (var v in getAllLines)
                {
                    var vPlan = (from t1 in _db.Plan_MasterLinePlan
                                 join t2 in _db.Plan_MasterOrderPlan on t1.PLan_MasterOrderPlanFK equals t2.ID
                                 join t4 in _db.Merchandising_Style on t2.Merchandising_StyleFK equals t4.ID
                                 join t5 in _db.Merchandising_BuyerOrder on t4.Merchandising_BuyerOrderFK equals t5.ID
                                 where t1.Active == true && t2.Active == true && t4.Active == true && t5.Active == true
                                 && t1.StartDate >= SDate && t1.StartDate <= TDate && t1.Common_ProductionLineFK == v.ID
                                 select new
                                 {
                                     BuyerPo = t5.BuyerPO,
                                     FromDate = t1.StartDate,
                                     ToDate = t1.FinishDate
                                 }).ToList();

                    DateTime fDate1 = SDate;
                    DateTime tDate1 = TDate;

                    ds.Rows.Add(v.Name);
                    int c = 0;
                    while (fDate1 <= tDate1)
                    {
                        c++;
                        if (vPlan.Any(a => a.FromDate <= fDate1 && a.ToDate >= fDate1))
                        {
                            var data = vPlan.FirstOrDefault(a => a.FromDate <= fDate1 && a.ToDate >= fDate1);
                            ds.Rows[count][c] = data.BuyerPo;
                        }
                        else
                        {
                            ds.Rows[count][c] = "Undefine";
                        }
                        fDate1 = fDate1.AddDays(1);
                    }
                    ++count;
                }
            }

            return ds;
        }

        private int GetTotalOrderQtyByColorID(int StyleID, int ColorID)
        {
            var vData = (from t1 in _db.Merchandising_StyleShipmentSchedule
                         join t2 in _db.Merchandising_StyleShipmentRatio on t1.ID equals t2.Merchandising_StyleShipmentScheduleFK
                         join t3 in _db.Merchandising_StyleMeasurement on t2.Merchandising_StyleMeasurementFK equals t3.ID
                         join t4 in _db.Merchandising_Style on t1.Merchandising_StyleFK equals t4.ID
                         where t1.Active == true && t2.Active == true && t3.Common_ColorFK == ColorID && t1.Merchandising_StyleFK == StyleID
                         group t2.Quantity by new { t3.Common_ColorFK,t4.PackPieceQty  } into all
                         select new {
                             qty = all.Sum(),
                             packPiece=all.Key.PackPieceQty
                         }).AsEnumerable().Sum(a => a.qty* a.packPiece);

            return vData;
        }

        private int GetDoneQtyByColorWise(int RefID, int ColorID, int SectionId, int StyleID)
        {
            int TotalDoneQty = 0;

            var takedate = _db.Prod_Reference.Where(a => a.ID == RefID).FirstOrDefault();
            DateTime FDate = takedate.ReferenceDate;
            var vOrderDoneData = (from o in _db.Prod_Reference
                                  join p in _db.Prod_ReferencePlan on o.ID equals p.Prod_ReferenceFK
                                  join q in _db.Prod_ReferencePlanFollowup on p.ID equals q.Prod_ReferencePlanFK
                                  join r in _db.Merchandising_StyleShipmentRatio on q.Merchandising_StyleShipmentRatioFK equals r.ID
                                  join s in _db.Merchandising_StyleMeasurement on r.Merchandising_StyleMeasurementFK equals s.ID
                                  where o.ReferenceDate <= FDate && p.SectionId == SectionId && s.Common_ColorFK == ColorID && p.Merchandising_StyleFK==StyleID
                                  select new { q }).ToList();

            if (vOrderDoneData.Any())
            {
                TotalDoneQty = (int)vOrderDoneData.Sum(a => a.q.Quantity);
            }

            return TotalDoneQty;
        }

        private int GetDoneQty(int RefID, int ColorID, int SizeID, int SectionId)
        {
            int TotalDoneQty = 0;

            var takedate = _db.Prod_Reference.Where(a => a.ID == RefID).FirstOrDefault();
            DateTime FDate = takedate.ReferenceDate;
            var vOrderDoneData = (from o in _db.Prod_Reference
                                  join p in _db.Prod_ReferencePlan on o.ID equals p.Prod_ReferenceFK
                                  join q in _db.Prod_ReferencePlanFollowup on p.ID equals q.Prod_ReferencePlanFK
                                  join r in _db.Merchandising_StyleShipmentRatio on q.Merchandising_StyleShipmentRatioFK equals r.ID
                                  join s in _db.Merchandising_StyleMeasurement on r.Merchandising_StyleMeasurementFK equals s.ID
                                  where o.ReferenceDate <= FDate && p.SectionId == SectionId && s.Common_ColorFK == ColorID && s.Common_SizeFK == SizeID
                                  select new { q }).ToList();

            if (vOrderDoneData.Any())
            {
                TotalDoneQty = (int)vOrderDoneData.Sum(a => a.q.Quantity);
            }

            return TotalDoneQty;
        }

        private int GetIronPackDoneQtyBySizeWise(int RefID, int StyleID, int ColorID, int SizeID, int SectionId)
        {
            int TotalDoneQty = 0;

            var takedate = _db.Prod_Reference.Where(a => a.ID == RefID).FirstOrDefault();
            DateTime FDate = takedate.ReferenceDate;
            var vOrderDoneData = (from o in _db.Prod_Reference
                                  join p in _db.Prod_ReferencePlan on o.ID equals p.Prod_ReferenceFK
                                  join q in _db.Prod_ReferencePlanFollowup on p.ID equals q.Prod_ReferencePlanFK
                                  join r in _db.Merchandising_StyleShipmentRatio on q.Merchandising_StyleShipmentRatioFK equals r.ID
                                  join s in _db.Merchandising_StyleMeasurement on r.Merchandising_StyleMeasurementFK equals s.ID
                                  where o.ReferenceDate <= FDate && p.Merchandising_StyleFK == StyleID && p.SectionId == SectionId && s.Common_ColorFK == ColorID && s.Common_SizeFK == SizeID
                                  select new { q }).ToList();

            if (vOrderDoneData.Any())
            {
                TotalDoneQty = (int)vOrderDoneData.Sum(a => a.q.Quantity);
            }

            return TotalDoneQty;
        }

        private int GetDoneQtyBySizeWise(int RefID,int StyleID,int SubSetID,int RatioID, int ColorID, int SizeID, int SectionId)
        {
            int TotalDoneQty = 0;

            var takedate = _db.Prod_Reference.Where(a => a.ID == RefID).FirstOrDefault();
            DateTime FDate = takedate.ReferenceDate;
            var vOrderDoneData = (from o in _db.Prod_Reference
                                  join p in _db.Prod_ReferencePlan on o.ID equals p.Prod_ReferenceFK
                                  join q in _db.Prod_ReferencePlanFollowup on p.ID equals q.Prod_ReferencePlanFK
                                  join r in _db.Merchandising_StyleShipmentRatio on q.Merchandising_StyleShipmentRatioFK equals r.ID
                                  join s in _db.Merchandising_StyleMeasurement on r.Merchandising_StyleMeasurementFK equals s.ID
                                  where o.ReferenceDate <= FDate && p.Merchandising_StyleFK== StyleID && p.Merchandising_StyleSetPackFK==SubSetID && q.Merchandising_StyleShipmentRatioFK==RatioID && p.SectionId == SectionId && s.Common_ColorFK == ColorID && s.Common_SizeFK == SizeID
                                  select new { q }).ToList();

            if (vOrderDoneData.Any())
            {
                TotalDoneQty = (int)vOrderDoneData.Sum(a => a.q.Quantity);
            }

            return TotalDoneQty;
        }

        private int GetDoneQtyBySectionID(int RefID, int StyleID, int SectionId)
        {
            int TotalDoneQty = 0;

            var takedate = _db.Prod_Reference.Where(a => a.ID == RefID).FirstOrDefault();
            DateTime FDate = takedate.ReferenceDate;
            var vOrderDoneData = (from o in _db.Prod_Reference
                                  join p in _db.Prod_ReferencePlan on o.ID equals p.Prod_ReferenceFK
                                  join q in _db.Prod_ReferencePlanFollowup on p.ID equals q.Prod_ReferencePlanFK
                                  join r in _db.Merchandising_StyleShipmentRatio on q.Merchandising_StyleShipmentRatioFK equals r.ID
                                  join s in _db.Merchandising_StyleMeasurement on r.Merchandising_StyleMeasurementFK equals s.ID
                                  where o.ReferenceDate <= FDate && p.SectionId == SectionId && p.Merchandising_StyleFK == StyleID
                                  select new { q }).ToList();

            if (vOrderDoneData.Any())
            {
                TotalDoneQty = (int)vOrderDoneData.Sum(a => a.q.Quantity);
            }

            return TotalDoneQty;
        }

        private int GetTotalOrderQtyByStyleID(int StyleID)
        {
            var vData = (from t1 in _db.Merchandising_StyleShipmentSchedule
                         join t2 in _db.Merchandising_StyleShipmentRatio on t1.ID equals t2.Merchandising_StyleShipmentScheduleFK
                         join t3 in _db.Merchandising_StyleMeasurement on t2.Merchandising_StyleMeasurementFK equals t3.ID
                         join t4 in _db.Merchandising_Style on t1.Merchandising_StyleFK equals t4.ID
                         where t1.Active == true && t2.Active == true && t1.Merchandising_StyleFK == StyleID
                         group t2.Quantity by new { t1.Merchandising_StyleFK,t4.PackPieceQty } into all
                         select new { qty = all.Sum(),packPiece=all.Key.PackPieceQty }).Sum(a => a.qty* a.packPiece);

            return vData;
        }

        private int GetTotalDoneQtyBySectionID(int StyleID, int SectionID)
        {
            int TotalDoneQty = 0;
            var vOrderDoneData = (from o in _db.Prod_Reference
                                  join p in _db.Prod_ReferencePlan on o.ID equals p.Prod_ReferenceFK
                                  join q in _db.Prod_ReferencePlanFollowup on p.ID equals q.Prod_ReferencePlanFK
                                  join r in _db.Merchandising_StyleShipmentRatio on q.Merchandising_StyleShipmentRatioFK equals r.ID
                                  join s in _db.Merchandising_StyleMeasurement on r.Merchandising_StyleMeasurementFK equals s.ID
                                  where p.SectionId == SectionID && p.Merchandising_StyleFK == StyleID && p.Active == true && o.Active == true
                                  select new { q }).ToList();

            if (vOrderDoneData.Any())
            {
                TotalDoneQty = (int)vOrderDoneData.Sum(a => a.q.Quantity);
            }

            return TotalDoneQty;
        }

        /// <summary>
        /// Style production quantity 
        /// Color and size wise
        /// </summary>
        /// <param name="StyleID"></param>
        /// <param name="SectionId"></param>
        /// <returns></returns>
        public List<VMDailyProduction> GetTotalDoneQtyBySectionColorSizeWise(int StyleID, int SectionId)
        {
            List<VMDailyProduction> lstColor = new List<VMDailyProduction>();
            var vOrderDoneData = (from o in _db.Prod_Reference
                                  join p in _db.Prod_ReferencePlan on o.ID equals p.Prod_ReferenceFK
                                  join q in _db.Prod_ReferencePlanFollowup on p.ID equals q.Prod_ReferencePlanFK
                                  join r in _db.Merchandising_StyleShipmentRatio on q.Merchandising_StyleShipmentRatioFK equals r.ID
                                  join s in _db.Merchandising_StyleMeasurement on r.Merchandising_StyleMeasurementFK equals s.ID
                                  join t in _db.Common_Color on s.Common_ColorFK equals t.ID
                                  join u in _db.Common_Size on s.Common_SizeFK equals u.ID
                                  where p.SectionId == SectionId && p.Merchandising_StyleFK==StyleID
                                  group q.Quantity by new {p.Merchandising_StyleFK, color=t.Name,cId=t.ID, size=u.Name,sId=u.ID} into all
                                  select new VMDailyProduction
                                  {
                                      Merchandising_StyleFK=all.Key.Merchandising_StyleFK,
                                      Color=all.Key.color,
                                      Common_ColorFK=all.Key.cId,
                                      Size=all.Key.size,
                                      Common_SizeFK=all.Key.sId,
                                      TargetQty=(int)all.Sum()
                                  }).ToList();

            return lstColor;
        }
        #endregion

        #region LineChart

        public List<DateTime> GetDays(DateTime FromDate, DateTime ToDate, DayOfWeek day)
        {
            List<DateTime> lstDay = new List<DateTime>();
            for (DateTime date = FromDate; date <= ToDate; date = date.AddDays(1))
            {
                if (date.DayOfWeek == day)
                {
                    lstDay.Add(date);
                }
            }

            return lstDay;
        }

        public List<DateTime> GetAllWeekendDays(DateTime StartDate, DateTime EndDate)
        {
            List<DateTime> lstWeekend = new List<DateTime>();
            var AllWeekendDay = integrationService.GetCompanyWeekend();
            #region FindDay

            if (AllWeekendDay.Any())
            {
                foreach (var v in AllWeekendDay)
                {
                    string WeekendDay = v.HoildayName;
                    if (WeekendDay.Equals("Saturday"))
                    {
                        var lstWeek = GetDays(StartDate, EndDate, DayOfWeek.Saturday);
                        lstWeekend.AddRange(lstWeek);
                    }
                    else if (WeekendDay.Equals("Sunday"))
                    {
                        var lstWeek = GetDays(StartDate, EndDate, DayOfWeek.Sunday);
                        lstWeekend.AddRange(lstWeek);
                    }
                    else if (WeekendDay.Equals("Monday"))
                    {
                        var lstWeek = GetDays(StartDate, EndDate, DayOfWeek.Monday);
                        lstWeekend.AddRange(lstWeek);
                    }
                    else if (WeekendDay.Equals("Tuesday"))
                    {
                        var lstWeek = GetDays(StartDate, EndDate, DayOfWeek.Tuesday);
                        lstWeekend.AddRange(lstWeek);
                    }
                    else if (WeekendDay.Equals("Wednesday"))
                    {
                        var lstWeek = GetDays(StartDate, EndDate, DayOfWeek.Wednesday);
                        lstWeekend.AddRange(lstWeek);
                    }
                    else if (WeekendDay.Equals("Thursday"))
                    {
                        var lstWeek = GetDays(StartDate, EndDate, DayOfWeek.Thursday);
                        lstWeekend.AddRange(lstWeek);
                    }
                    else if (WeekendDay.Equals("Friday"))
                    {
                        var lstWeek = GetDays(StartDate, EndDate, DayOfWeek.Friday);
                        lstWeekend.AddRange(lstWeek);
                    }
                }
            }

            return lstWeekend;
            #endregion
        }

        public List<VMLineSchedule> GetLinePlanChart(DateTime SDate, DateTime TDate, bool IsPrePlan)
        {
            var OffDay = integrationService.GetAllHoliday(SDate, TDate);
            List<VMLineSchedule> lstPlan = new List<VMLineSchedule>();
            List<DateTime> lstWeekEnd = new List<DateTime>();
            lstWeekEnd = GetAllWeekendDays(SDate, TDate);

            //var getAllLines = _db.Common_ProductionLine.Where(a => a.Active == true && a.ProductionStepFK == 2).OrderBy(a => a.Priority).ToList();
            var getAllLines = ProductionLineGetByProcessID((int)EnumProcess.Sewing);
            if (getAllLines.Any())
            {
                foreach (var v in getAllLines)
                {
                    VMLineSchedule pline = new VMLineSchedule();
                    pline.LineID = v.ID;
                    pline.LineName = v.Name;
                    pline.LineColor = "background-color:blue; color:white;";
                    pline.ListLinePlan = new List<VMLineSchedule>();

                    int totalDay = 0;
                    DateTime fDate = SDate;
                    DateTime tDate = TDate;

                    if (IsPrePlan)
                    {
                        var vPlan = (from t1 in _db.Plan_DraftLinePlan
                                     join t2 in _db.Plan_DraftOrderPlan on t1.Plan_DraftOrderPlanFK equals t2.ID
                                     join t4 in _db.Merchandising_Style on t2.Merchandising_StyleFK equals t4.ID
                                     join t5 in _db.Merchandising_BuyerOrder on t4.Merchandising_BuyerOrderFK equals t5.ID
                                     where t1.Active == true && t2.Active == true && t4.Active == true && t5.Active == true
                                     && t1.StartDate >= SDate && t1.StartDate <= TDate && t1.Common_ProductionLineFK == v.ID
                                     select new
                                     {
                                         BuyerPo = t5.BuyerPO,
                                         FromDate = t1.StartDate,
                                         ToDate = t1.FinishDate,
                                     }).ToList();

                        while (fDate <= tDate)
                        {
                            ++totalDay;
                            VMLineSchedule dPlan = new VMLineSchedule();
                            if (lstWeekEnd.Any(a => a.Date == fDate) && OffDay.Any(a => a.FromDate == fDate))
                            {
                                dPlan.HeadDate = fDate.ToString("MMMM dd");
                                dPlan.DayColor = "background-color:yellow; color:white;";
                                dPlan.ViewName = "WD/HD";
                            }
                            else if (lstWeekEnd.Any(a => a.Date == fDate) && !OffDay.Any(a => a.FromDate == fDate))
                            {
                                dPlan.HeadDate = fDate.ToString("MMMM dd");
                                dPlan.DayColor = "background-color:orange; color:white;";
                                dPlan.ViewName = "WD";
                            }
                            else if (!lstWeekEnd.Any(a => a.Date == fDate) && OffDay.Any(a => a.FromDate == fDate))
                            {
                                dPlan.HeadDate = fDate.ToString("MMMM dd");
                                dPlan.DayColor = "background-color:blue; color:white;";
                                dPlan.ViewName = "HD";
                            }
                            else if (!lstWeekEnd.Any(a => a.Date == fDate) && !OffDay.Any(a => a.FromDate == fDate))
                            {
                                if (vPlan.Any(a => a.FromDate <= fDate && a.ToDate >= fDate))
                                {
                                    var data = vPlan.FirstOrDefault(a => a.FromDate <= fDate && a.ToDate >= fDate);
                                    dPlan.HeadDate = fDate.ToString("MMMM dd");
                                    dPlan.DayColor = "background-color:firebrick; color:white;";
                                    dPlan.ViewName = data.BuyerPo;
                                }
                                else
                                {
                                    dPlan.HeadDate = fDate.ToString("MMMM dd");
                                    dPlan.DayColor = "background-color:green; color:white;";
                                    dPlan.ViewName = string.Empty;
                                }
                            }
                            pline.ListLinePlan.Add(dPlan);
                            fDate = fDate.AddDays(1);
                        }
                    }
                    else
                    {
                        var vPlan = (from t1 in _db.Plan_MasterLinePlan
                                     join t2 in _db.Plan_MasterOrderPlan on t1.PLan_MasterOrderPlanFK equals t2.ID
                                     join t4 in _db.Merchandising_Style on t2.Merchandising_StyleFK equals t4.ID
                                     join t5 in _db.Merchandising_BuyerOrder on t4.Merchandising_BuyerOrderFK equals t5.ID
                                     where t1.Active == true && t2.Active == true && t4.Active == true && t5.Active == true
                                     && t1.StartDate >= SDate && t1.StartDate <= TDate && t1.Common_ProductionLineFK == v.ID
                                     select new
                                     {
                                         BuyerPo = t5.BuyerPO,
                                         FromDate = t1.StartDate,
                                         ToDate = t1.FinishDate
                                     }).ToList();

                        while (fDate <= tDate)
                        {
                            ++totalDay;
                            VMLineSchedule dPlan = new VMLineSchedule();
                            if (lstWeekEnd.Any(a => a.Date == fDate) && OffDay.Any(a => a.FromDate == fDate))
                            {
                                dPlan.HeadDate = fDate.ToString("MMMM dd");
                                dPlan.DayColor = "background-color:yellow; color:white;";
                                dPlan.ViewName = "WD/HD";
                            }
                            else if (lstWeekEnd.Any(a => a.Date == fDate) && !OffDay.Any(a => a.FromDate == fDate))
                            {
                                dPlan.HeadDate = fDate.ToString("MMMM dd");
                                dPlan.DayColor = "background-color:orange; color:white;";
                                dPlan.ViewName = "WD";
                            }
                            else if (!lstWeekEnd.Any(a => a.Date == fDate) && OffDay.Any(a => a.FromDate == fDate))
                            {
                                dPlan.HeadDate = fDate.ToString("MMMM dd");
                                dPlan.DayColor = "background-color:blue; color:white;";
                                dPlan.ViewName = "HD";
                            }
                            else if (!lstWeekEnd.Any(a => a.Date == fDate) && !OffDay.Any(a => a.FromDate == fDate))
                            {
                                if (vPlan.Any(a => a.FromDate <= fDate && a.ToDate >= fDate))
                                {
                                    var data = vPlan.FirstOrDefault(a => a.FromDate <= fDate && a.ToDate >= fDate);
                                    dPlan.HeadDate = fDate.ToString("MMMM dd");
                                    dPlan.DayColor = "background-color:firebrick; color:white;";
                                    dPlan.ViewName = data.BuyerPo;
                                }
                                else
                                {
                                    dPlan.HeadDate = fDate.ToString("MMMM dd");
                                    dPlan.DayColor = "background-color:green; color:white;";
                                    dPlan.ViewName = string.Empty;
                                }
                            }
                            pline.ListLinePlan.Add(dPlan);
                            fDate = fDate.AddDays(1);
                        }
                    }

                    lstPlan.Add(pline);
                }
            }

            return lstPlan;
        }

        private VMSupportPlan GetWorkRange(VMSupportPlan model, List<VMSupportPlan> LinePlan)
        {
            int scheduleRange = (model.ToDate - model.FromDate).Days + 1;
            int ProductionRange = 0;
            decimal countDay = decimal.Divide(model.PlanQty, decimal.Multiply(model.WorkingHour, model.HourTarget));
            int tday = (int)countDay;
            if (tday < countDay)
            {
                ProductionRange = tday + 1;
            }
            else
            {
                ProductionRange = tday + 1;
            }
            int LoopCounter = 0;
            if (scheduleRange >= ProductionRange)
            {
                LoopCounter = ProductionRange;
            }
            else
            {
                LoopCounter = scheduleRange;
            }

            var OffDay = integrationService.GetAllHoliday(model.FromDate, model.ToDate);
            List<DateTime> lstWeekEnd = new List<DateTime>();
            lstWeekEnd = GetAllWeekendDays(model.FromDate, model.ToDate);

            DateTime sDate = new DateTime();
            DateTime fDate = new DateTime();

            sDate = model.FromDate;
            fDate = model.ToDate;

            VMSupportPlan m = new VMSupportPlan();
            while (sDate <= fDate)
            {
                if (LoopCounter != 0)
                {
                    if (!lstWeekEnd.Any(a => a.Date == sDate))
                    {
                        if (!OffDay.Any(a => a.FromDate == sDate))
                        {
                            if (LinePlan != null)
                            {
                                if (!LinePlan.Any(a => a.FromDate <= sDate && a.ToDate >= sDate && a.StyleID != model.StyleID && a.LineID == model.LineID))
                                {
                                    ++m.DayRange;

                                    if (m.DayRange == 1)
                                    {
                                        m.FromDate = sDate;
                                        m.WorkingHour += model.WorkingHour;
                                        m.ToDate = sDate;
                                    }
                                    else
                                    {
                                        m.ToDate = sDate;
                                        m.WorkingHour += model.WorkingHour;
                                    }
                                    --LoopCounter;
                                }
                            }
                            else
                            {
                                ++m.DayRange;

                                if (m.DayRange == 1)
                                {
                                    m.FromDate = sDate;
                                    m.WorkingHour += model.WorkingHour;
                                }
                                else
                                {
                                    m.ToDate = sDate;
                                    m.WorkingHour += model.WorkingHour;
                                }
                                --LoopCounter;
                            }
                        }

                    }
                }
                else
                {
                    break;
                }

                sDate = sDate.AddDays(1);
            }

            if (scheduleRange >= ProductionRange)
            {
                if (model.PlanQty <= 0)
                {
                    m.HourOccupied = 1;
                }
                else
                {
                    decimal countHour = decimal.Divide(model.PlanQty, model.HourTarget);
                    int tHour = (int)countHour;
                    m.HourOccupied = tHour < countHour == true ? ++tHour : (int)countHour;
                }
            }
            else
            {
                m.HourOccupied = m.WorkingHour;
            }
            m.PlanQty = model.PlanQty;
            return m;
        }

        public List<VMLinePlanning> GetAutoLinePrePlan2(int PlanID, int SMVID, List<int> LineIDs)
        {
            List<VMLinePlanning> lst = new List<VMLinePlanning>();
            List<VMLineSchedule> lstPlan = new List<VMLineSchedule>();

            var vShipment = _db.Plan_DraftOrderPlan.FirstOrDefault(t1 => t1.ID == PlanID && t1.Active == true);
            var vSMV = _db.Plan_SMVDraftLayout.FirstOrDefault(a => a.ID == SMVID);
            var vConfig = _db.Plan_PlanConfig.FirstOrDefault();
            //var vLine = _db.Common_ProductionLine.Where(a => a.ProductionStepFK == 2 && a.Active == true).ToList();
            var vLine = ProductionLineGetByProcessID((int)EnumProcess.Sewing);
            var vAllLinePlan = (from t1 in _db.Plan_DraftLinePlan
                                join t2 in _db.Plan_DraftOrderPlan on t1.Plan_DraftOrderPlanFK equals t2.ID
                                where t1.Active == true && t1.StartDate >= vShipment.SewingDate && t1.StartDate <= vShipment.ShipmentDate.AddDays(-vConfig.ReservedDate)
                                select new VMSupportPlan
                                {
                                    LineID = t1.Common_ProductionLineFK,
                                    FromDate = t1.StartDate,
                                    ToDate = t1.FinishDate,
                                    StyleID = t2.Merchandising_StyleFK,
                                    WorkingHour = (int)t1.WorkingHour,
                                    HourOccupied = (int)t1.LineHour
                                }).ToList();

            if (LineIDs.Any())
            {
                VMSupportPlan model = new VMSupportPlan();
                model.StyleID = vShipment.Merchandising_StyleFK;
                model.FromDate = vShipment.SewingDate;
                model.ToDate = vShipment.ShipmentDate.AddDays(-vConfig.ReservedDate);
                model.WorkingHour = vSMV.WorkingHour;
                model.HourTarget = (int)vSMV.PerHourTarget;
                model.PlanQty = (int)vShipment.PlanQty;
                foreach (var v in LineIDs)
                {
                    //string LineName = vLine.FirstOrDefault(a => a.ID == v).Name;
                    var vLinePlan = vAllLinePlan.Where(a => a.LineID == v).ToList();
                    model.LineID = v;
                    VMSupportPlan data = GetWorkRange(model, vLinePlan);

                    VMLinePlanning m = new VMLinePlanning();
                    m.OccupiedHour = data.HourOccupied;
                    m.StartDate = data.FromDate;
                    m.FinishDate = data.ToDate;
                    m.Merchandising_StyleFK = vSMV.Merchandising_StyleFK;
                    m.Plan_SMVLayoutFK = vSMV.ID;
                    m.ProductionLineFK = v;
                    m.LineName = vLine.FirstOrDefault(a => a.ID == v).Name;
                    m.TargetDays = data.DayRange;
                    m.Capacity = vSMV.PerHourTarget;
                    m.DayOutputQty = Decimal.Multiply(m.OccupiedHour, vSMV.PerHourTarget);
                    m.TargetOutputQty = m.DayOutputQty;
                    m.WorkingHour = vSMV.WorkingHour;
                    m.MachineQuantity = vSMV.NoOfOperator;
                    m.ProdOrderPlanningFK = vShipment.ID;
                    m.RemainQty = data.PlanQty - m.TargetOutputQty;
                    lst.Add(m);
                    model.PlanQty = (int)m.RemainQty;
                }
            }

            return lst;
        }

        private List<VMLineSchedule> GetLinePlan(DateTime fDate, DateTime tDate, List<HRMS.VMHoliday> OffDay, List<DateTime> lstWeekEnd, List<VMLineSchedule> vPlan)
        {
            int totalDay = 0;
            List<VMLineSchedule> lstPlan = new List<VMLineSchedule>();
            while (fDate <= tDate)
            {
                ++totalDay;
                VMLineSchedule dPlan = new VMLineSchedule();
                if (lstWeekEnd.Any(a => a.Date == fDate) && OffDay.Any(a => a.FromDate == fDate))
                {
                    dPlan.HeadDate = fDate.ToString("MM dd");
                    dPlan.DayColor = "background-color:yellow; color:white;";
                    dPlan.ViewName = "WD/HD";
                }
                else if (lstWeekEnd.Any(a => a.Date == fDate) && !OffDay.Any(a => a.FromDate == fDate))
                {
                    dPlan.HeadDate = fDate.ToString("MMMM dd");
                    dPlan.DayColor = "background-color:orange; color:white;";
                    dPlan.ViewName = "WD";
                }
                else if (!lstWeekEnd.Any(a => a.Date == fDate) && OffDay.Any(a => a.FromDate == fDate))
                {
                    dPlan.HeadDate = fDate.ToString("MMMM dd");
                    dPlan.DayColor = "background-color:blue; color:white;";
                    dPlan.ViewName = "HD";
                }
                else if (!lstWeekEnd.Any(a => a.Date == fDate) && !OffDay.Any(a => a.FromDate == fDate))
                {
                    if (vPlan.Any(a => a.FromDate <= fDate && a.ToDate >= fDate))
                    {
                        var data = vPlan.FirstOrDefault(a => a.FromDate <= fDate && a.ToDate >= fDate);
                        dPlan.HeadDate = fDate.ToString("MMMM dd");
                        dPlan.DayColor = "background-color:firebrick; color:white;";
                        dPlan.ViewName = data.ViewName;
                    }
                    else
                    {
                        dPlan.HeadDate = fDate.ToString("MMMM dd");
                        dPlan.DayColor = "background-color:green; color:white;";
                        dPlan.ViewName = string.Empty;
                    }
                }
                lstPlan.Add(dPlan);
                fDate = fDate.AddDays(1);
            }
            return lstPlan;
        }

        public List<VMLineSchedule> GetGraphProductionPlan(VMShipmentProductionPlan model)
        {
            bool IsPrePlan = model.ID == 1 ? true : false;
            var OffDay = integrationService.GetAllHoliday(model.FromDate, model.ToDate);
            List<VMLineSchedule> lstPlan = new List<VMLineSchedule>();
            List<DateTime> lstWeekEnd = new List<DateTime>();
            lstWeekEnd = GetAllWeekendDays(model.FromDate, model.ToDate);

            //var getAllLines = (from t1 in _db.Common_ProductionLine
            //                   join t2 in _db.Common_SectionLine on t1.Common_SectionLineFK equals t2.ID
            //                   where t1.Active == true && t1.IsClosed == false && t1.ProductionStepFK == 2
            //                   select new { t1, t2 }).OrderBy(a => a.t1.Priority).ToList();
            var getAllLines = ProductionLineGetByProcessID((int)EnumProcess.Sewing);
            if (IsPrePlan)
            {
                var vPlanedOrder = (from t1 in _db.Plan_DraftLinePlan
                                    join t2 in _db.Plan_DraftOrderPlan on t1.Plan_DraftOrderPlanFK equals t2.ID
                                    join t4 in _db.Merchandising_Style on t2.Merchandising_StyleFK equals t4.ID
                                    join t5 in _db.Merchandising_BuyerOrder on t4.Merchandising_BuyerOrderFK equals t5.ID
                                    where t1.Active == true && t2.Active == true && t4.Active == true && t5.Active == true
                                    && t1.StartDate >= model.FromDate && t1.StartDate <= model.ToDate
                                    select new VMLineSchedule { ViewName = t5.BuyerPO, FromDate = t1.StartDate, ToDate = t1.FinishDate, LineID = t1.Common_ProductionLineFK }).ToList();

                if (model.ProductionUnitID != 0 && model.ProductionFloorID != 0)
                {
                    var vLine = getAllLines.Any() != false ? getAllLines.Where(a => a.ProductionUnitFK == model.ProductionUnitID && a.ProductionFloorFK == model.ProductionFloorID).ToList() : null;
                    if (vLine.Any())
                    {
                        foreach (var v in vLine)
                        {
                            VMLineSchedule pline = new VMLineSchedule();
                            pline.LineID = v.ID;
                            pline.LineName = v.Name;
                            pline.LineColor = "background-color:blue; color:white;";
                            pline.ListLinePlan = new List<VMLineSchedule>();
                            var pOrder = vPlanedOrder.Where(a => a.LineID == v.ID).ToList();
                            var vData = GetLinePlan(model.FromDate, model.ToDate, OffDay, lstWeekEnd, pOrder);
                            if (vData.Any())
                            {
                                pline.ListLinePlan.AddRange(vData);
                            }
                            lstPlan.Add(pline);
                        }
                    }
                }
                else if (model.ProductionUnitID != 0 && model.ProductionFloorID == 0)
                {
                    var vLine = getAllLines.Any() != false ? getAllLines.Where(a => a.ProductionUnitFK == model.ProductionUnitID).ToList() : null;
                    if (vLine.Any())
                    {
                        foreach (var v in vLine)
                        {
                            VMLineSchedule pline = new VMLineSchedule();
                            pline.LineID = v.ID;
                            pline.LineName = v.Name;
                            pline.LineColor = "background-color:blue; color:white;";
                            pline.ListLinePlan = new List<VMLineSchedule>();
                            var pOrder = vPlanedOrder.Where(a => a.LineID == v.ID).ToList();
                            var vData = GetLinePlan(model.FromDate, model.ToDate, OffDay, lstWeekEnd, pOrder);
                            if (vData.Any())
                            {
                                pline.ListLinePlan.AddRange(vData);
                            }
                            lstPlan.Add(pline);
                        }
                    }
                }
                else if (model.ProductionUnitID == 0 && model.ProductionFloorID != 0)
                {
                    var vLine = getAllLines.Any() != false ? getAllLines.Where(a => a.ProductionFloorFK == model.ProductionFloorID).ToList() : null;
                    if (vLine.Any())
                    {
                        foreach (var v in vLine)
                        {
                            VMLineSchedule pline = new VMLineSchedule();
                            pline.LineID = v.ID;
                            pline.LineName = v.Name;
                            pline.LineColor = "background-color:blue; color:white;";
                            pline.ListLinePlan = new List<VMLineSchedule>();
                            var pOrder = vPlanedOrder.Where(a => a.LineID == v.ID).ToList();
                            var vData = GetLinePlan(model.FromDate, model.ToDate, OffDay, lstWeekEnd, pOrder);
                            if (vData.Any())
                            {
                                pline.ListLinePlan.AddRange(vData);
                            }
                            lstPlan.Add(pline);
                        }
                    }
                }
                else if (model.ProductionUnitID == 0 && model.ProductionFloorID == 0)
                {
                    if (getAllLines.Any())
                    {
                        foreach (var v in getAllLines)
                        {
                            VMLineSchedule pline = new VMLineSchedule();
                            pline.LineID = v.ID;
                            pline.LineName = v.Name;
                            pline.LineColor = "background-color:blue; color:white;";
                            pline.ListLinePlan = new List<VMLineSchedule>();
                            var pOrder = vPlanedOrder.Where(a => a.LineID == v.ID).ToList();
                            var vData = GetLinePlan(model.FromDate, model.ToDate, OffDay, lstWeekEnd, pOrder);
                            if (vData.Any())
                            {
                                pline.ListLinePlan.AddRange(vData);
                            }
                            lstPlan.Add(pline);
                        }
                    }
                }
            }
            else
            {
                var vPlanedOrder = (from t1 in _db.Plan_MasterLinePlan
                                    join t2 in _db.Plan_MasterOrderPlan on t1.PLan_MasterOrderPlanFK equals t2.ID
                                    join t4 in _db.Merchandising_Style on t2.Merchandising_StyleFK equals t4.ID
                                    join t5 in _db.Merchandising_BuyerOrder on t4.Merchandising_BuyerOrderFK equals t5.ID
                                    where t1.Active == true && t2.Active == true && t4.Active == true && t5.Active == true
                                    && t1.StartDate >= model.FromDate && t1.StartDate <= model.ToDate
                                    select new VMLineSchedule { ViewName = t5.BuyerPO, FromDate = t1.StartDate, ToDate = t1.FinishDate }).ToList();

                if (model.ProductionUnitID != 0 && model.ProductionFloorID != 0)
                {
                    var vLine = getAllLines.Any() != false ? getAllLines.Where(a => a.ProductionUnitFK == model.ProductionUnitID && a.ProductionFloorFK == model.ProductionFloorID).ToList() : null;
                    if (vLine.Any())
                    {
                        foreach (var v in vLine)
                        {
                            VMLineSchedule pline = new VMLineSchedule();
                            pline.LineID = v.ID;
                            pline.LineName = v.Name;
                            pline.LineColor = "background-color:blue; color:white;";
                            pline.ListLinePlan = new List<VMLineSchedule>();
                            var pOrder = vPlanedOrder.Where(a => a.LineID == v.ID).ToList();
                            var vData = GetLinePlan(model.FromDate, model.ToDate, OffDay, lstWeekEnd, pOrder);
                            if (vData.Any())
                            {
                                pline.ListLinePlan.AddRange(vData);
                            }
                            lstPlan.Add(pline);
                        }
                    }
                }
                else if (model.ProductionUnitID != 0 && model.ProductionFloorID == 0)
                {
                    var vLine = getAllLines.Any() != false ? getAllLines.Where(a => a.ProductionUnitFK == model.ProductionUnitID).ToList() : null;
                    if (vLine.Any())
                    {
                        foreach (var v in vLine)
                        {
                            VMLineSchedule pline = new VMLineSchedule();
                            pline.LineID = v.ID;
                            pline.LineName = v.Name;
                            pline.LineColor = "background-color:blue; color:white;";
                            pline.ListLinePlan = new List<VMLineSchedule>();
                            var pOrder = vPlanedOrder.Where(a => a.LineID == v.ID).ToList();
                            var vData = GetLinePlan(model.FromDate, model.ToDate, OffDay, lstWeekEnd, pOrder);
                            if (vData.Any())
                            {
                                pline.ListLinePlan.AddRange(vData);
                            }
                            lstPlan.Add(pline);
                        }
                    }
                }
                else if (model.ProductionUnitID == 0 && model.ProductionFloorID != 0)
                {
                    var vLine = getAllLines.Any() != false ? getAllLines.Where(a => a.ProductionFloorFK == model.ProductionFloorID).ToList() : null;
                    if (vLine.Any())
                    {
                        foreach (var v in vLine)
                        {
                            VMLineSchedule pline = new VMLineSchedule();
                            pline.LineID = v.ID;
                            pline.LineName = v.Name;
                            pline.LineColor = "background-color:blue; color:white;";
                            pline.ListLinePlan = new List<VMLineSchedule>();
                            var pOrder = vPlanedOrder.Where(a => a.LineID == v.ID).ToList();
                            var vData = GetLinePlan(model.FromDate, model.ToDate, OffDay, lstWeekEnd, pOrder);
                            if (vData.Any())
                            {
                                pline.ListLinePlan.AddRange(vData);
                            }
                            lstPlan.Add(pline);
                        }
                    }
                }
                else if (model.ProductionUnitID == 0 && model.ProductionFloorID == 0)
                {
                    if (getAllLines.Any())
                    {
                        foreach (var v in getAllLines)
                        {
                            VMLineSchedule pline = new VMLineSchedule();
                            pline.LineID = v.ID;
                            pline.LineName = v.Name;
                            pline.LineColor = "background-color:blue; color:white;";
                            pline.ListLinePlan = new List<VMLineSchedule>();
                            var pOrder = vPlanedOrder.Where(a => a.LineID == v.ID).ToList();
                            var vData = GetLinePlan(model.FromDate, model.ToDate, OffDay, lstWeekEnd, pOrder);
                            if (vData.Any())
                            {
                                pline.ListLinePlan.AddRange(vData);
                            }
                            lstPlan.Add(pline);
                        }
                    }
                }
            }

            return lstPlan;
        }

        

        #endregion

        #region SectionLine
        public List<VMSectionLine> SectionLineGet()
        {
            var sectionline = (from t1 in _db.Common_SectionLine
                               join t2 in _db.Common_ProductionFloor on t1.Common_ProductionFloorFK equals t2.ID
                               join t3 in _db.Common_ProductionUnit on t2.Common_ProductionUnitFK equals t3.ID
                               join t4 in _db.HRMS_Unit on t3.HRMS_UnitFK equals t4.ID into all
                               from t5 in all.DefaultIfEmpty()
                               where t1.Active == true
                               select new VMSectionLine
                               {
                                   ID = t1.ID,
                                   Name = t1.Name,
                                   Code = t1.Code,
                                   BuildingFk = t3.ID,
                                   Building=t3.Name,
                                   FloorFk = t2.ID,
                                   Floor=t2.Name,
                                   HRMS_UnitFK = t5.ID,
                                   Unit = t5.Name
                               }).OrderByDescending(a => a.ID).ToList();
            return sectionline;
        }
        public VMSectionLine SectionLineGetByID(int id)
        {
            var sectionline = (from t1 in _db.Common_SectionLine
                     
                               where t1.Active == true && t1.ID == id
                               select new VMSectionLine
                               {
                                   Name = t1.Name,
                                   Code = t1.Code,
                                   FloorFk =(int)t1.Common_ProductionFloorFK,
                                   ActionId = (int)ActionEnum.Edit
                               }).FirstOrDefault();
            return sectionline;
        }
        public async Task<int> SectionLineAdd(VMSectionLine sectionLine)
        {
            var result = -1;

            Common_SectionLine sl = new Common_SectionLine
            {
                Name = sectionLine.Name,
                Code = sectionLine.Code,
               Common_ProductionFloorFK = sectionLine.FloorFk

            };
            _db.Common_SectionLine.Add(sl);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = sl.ID;
            }
            return result;
        }
        public async Task<int> SectionLineEdit(VMSectionLine sectionLine)
        {
            var result = -1;
            Common_SectionLine sl = _db.Common_SectionLine.Find(sectionLine.ID);
            sl.Name = sectionLine.Name;
            sl.Code = sectionLine.Code;
            sl.Common_ProductionFloorFK = sectionLine.FloorFk;


            if (await _db.SaveChangesAsync() > 0)
            {
                result = sl.ID;
            }
            return result;
        }
        public async Task<int> SectionLineDelete(int id)
        {
            var result = -1;
            Common_SectionLine sectionLine = _db.Common_SectionLine.Find(id);
            if (sectionLine != null)
            {

                sectionLine.Active = false;
                if (await _db.SaveChangesAsync() > 0)
                {

                    result = sectionLine.ID;
                }
            }
            return result;
        }
        #endregion

        #region TimeAndAction
        public async Task<VMOrderAction> OrderActionGet()
        {
            var vData = _db.Plan_OrderAction.Where(a => a.Active == true).Select(a => new VMOrderAction
            {
                ID=a.ID,
                Name=a.Name,
                Priority=a.Priority,
                IsStop=a.IsStop,
                ActionRange=a.ActionRange
            }).OrderByDescending(a=>a.ID).AsEnumerable();

            VMOrderAction oAction = new VMOrderAction
            {
                DataList = await Task.Run(() => vData)
            };
            return oAction;
        }
        public VMOrderAction OrderActionGetByID(int id)
        {
            var vData = _db.Plan_OrderAction.Where(a => a.ID == id).Select(a => new VMOrderAction
            {
                ID = a.ID,
                Name = a.Name,
                Priority = a.Priority,
                ActionRange = a.ActionRange,
                IsStop = a.IsStop,
                IsActive = a.Active,
                ActionId = (int)ActionEnum.Edit
            }).FirstOrDefault();
            return vData;
        }
        public async Task<int> OrderActionAdd(VMOrderAction model)
        {
            var result = -1;
            Plan_OrderAction sl = new Plan_OrderAction
            {
                Name = model.Name,
                Priority = model.Priority,
                ActionRange = model.ActionRange,
                UserID=model.UserID,
                Time=DateTime.Today
            };
            _db.Plan_OrderAction.Add(sl);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = sl.ID;
            }
            return result;
        }
        public async Task<int> OrderActionEdit(VMOrderAction model)
        {
            var result = -1;
            Plan_OrderAction sl = _db.Plan_OrderAction.Find(model.ID);
            sl.Name = model.Name;
            sl.Priority = model.Priority;
            sl.ActionRange = model.ActionRange;
            sl.Time = DateTime.Today;
            if (await _db.SaveChangesAsync() > 0)
            {
                result = sl.ID;
            }
            return result;
        }
        public async Task<int> OrderActionDelete(int id)
        {
            var result = -1;
            Plan_OrderAction oAction = _db.Plan_OrderAction.Find(id);
            if (oAction != null)
            {
                oAction.Active = false;
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = oAction.ID;
                }
            }
            return result;
        }

        public List<VMOrderActionStep> OrderActionStepGet(int orderactionid)
        {
            var vData = (from a in _db.Plan_OrderActionStep
                         join b in _db.Plan_OrderAction on a.Plan_OrderActionFK equals b.ID
                         where a.Active == true && a.Plan_OrderActionFK == orderactionid
                         select new VMOrderActionStep
                         {
                             ID = a.ID,
                             Name = a.Name,
                             Priority = a.Priority,
                             Plan_OrderActionFK = a.Plan_OrderActionFK,
                             DefaultRange = a.DefaultRange,
                             ExtendedRange = a.ExtendedRange,
                             OrderActionName = b.Name,
                             StepCode=a.Remarks
                         }).OrderByDescending(a => a.ID).ToList();

            return vData;
        }
        public VMOrderActionStep OrderActionStepGetByActionID(int orderactionid)
        {
            var vData= OrderActionGetByID(orderactionid);
            VMOrderActionStep model = new VMOrderActionStep();
            model.Plan_OrderActionFK = vData.ID;
            model.OrderActionName = vData.Name;
            model.DataList = new List<VMOrderActionStep>();
            model.DataList = OrderActionStepGet(orderactionid);
            return model;
        }
        public VMOrderActionStep OrderActionStepGetByID(int id)
        {
            var vData = _db.Plan_OrderActionStep.Where(a => a.ID == id).Select(a => new VMOrderActionStep
            {
                ID=a.ID,
                Name = a.Name,
                Plan_OrderActionFK=a.Plan_OrderActionFK,
                Priority = a.Priority,
                DefaultRange = a.DefaultRange,
                ExtendedRange = a.ExtendedRange,
                IsActive = a.Active,
                ExtendColor=a.ExtendColor,
                SetColor=a.SetColor,
                StepCode=a.Remarks,
                ActionId = (int)ActionEnum.Edit
            }).FirstOrDefault();
            return vData;
        }
        public async Task<int> OrderActionStepAdd(VMOrderActionStep model)
        {
            var result = -1;
            Plan_OrderActionStep sl = new Plan_OrderActionStep
            {
                Plan_OrderActionFK=model.Plan_OrderActionFK,
                Name = model.Name,
                Priority = model.Priority,
                DefaultRange = model.DefaultRange,
                ExtendedRange=model.ExtendedRange,
                Remarks=model.StepCode,
                UserID = model.UserID,
                Time = DateTime.Today
            };
            _db.Plan_OrderActionStep.Add(sl);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = sl.ID;
            }
            return result;
        }
        public async Task<int> OrderActionStepEdit(VMOrderActionStep model)
        {
            var result = -1;
            Plan_OrderActionStep sl = _db.Plan_OrderActionStep.Find(model.ID);
            sl.Name = model.Name;
            sl.Priority = model.Priority;
            sl.DefaultRange = model.DefaultRange;
            sl.ExtendedRange = model.ExtendedRange;
            sl.Remarks = model.StepCode;
            sl.Time = DateTime.Today;
            if (await _db.SaveChangesAsync() > 0)
            {
                result = sl.ID;
            }
            return result;
        }
        public async Task<int> OrderActionStepDelete(int id)
        {
            var result = -1;
            Plan_OrderActionStep oAction = _db.Plan_OrderActionStep.Find(id);
            if (oAction != null)
            {
                oAction.Active = false;
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = oAction.ID;
                }
            }
            return result;
        }

        public VMOrderStep GetActionStep(int sid)
        {
            List<VMOrderProcess> lstModel = new List<VMOrderProcess>();
            DateTime orderDate = (from s in _db.Merchandising_Style
                         join o in _db.Merchandising_BuyerOrder on s.Merchandising_BuyerOrderFK equals o.ID
                         select new { orderdate=o.OrderDate }).FirstOrDefault().orderdate;
            var assignData = GetAllStepByStyleID(sid);
            DateTime StartDate = orderDate.AddDays(4);
            var vData = (from t1 in _db.Plan_OrderAction
                        join t2 in _db.Plan_OrderActionStep on t1.ID equals t2.Plan_OrderActionFK
                        where t1.Active==true && t2.Active==true && t1.IsStop==false
                        select new{ t1, t2 }).OrderBy(a=>a.t1.Priority).ThenBy(a=>a.t2.Priority).ToList();
            if (vData.Any())
            {
                foreach (var v in vData)
                {
                    if (!assignData.Any(a => a.Plan_OrderActionStepFK == v.t2.ID))
                    {
                        VMOrderProcess mod = new VMOrderProcess();
                        mod.ActionName = v.t1.Name;
                        mod.ActionStepName = v.t2.Name;

                        mod.PlanStartDate = StartDate;
                        mod.PlanFinishDate = mod.PlanStartDate.AddDays(v.t2.DefaultRange - 1);
                        mod.ActualStartDate = StartDate;
                        mod.ActualFinishDate = StartDate;
                        mod.Merchandising_StyleFK = sid;
                        mod.Plan_OrderActionFK = v.t1.ID;
                        mod.Plan_OrderActionStepFK = v.t2.ID;
                        lstModel.Add(mod);
                        StartDate = mod.PlanFinishDate.AddDays(1);
                    }
                }
            }

            VMOrderStep step = new VMOrderStep()
            {
                StepList = lstModel
            };

            return step;
        }
        public async Task<int> OrderProcessAdd(VMOrderStep model)
        {
            var result = -1;
            if (model.StepList.Any(a=>a.IsTaken==true))
            {
                List<Plan_OrderProcess> lstAdd = new List<Plan_OrderProcess>();
                foreach (var v in model.StepList.Where(a=>a.IsTaken==true))
                {
                    Plan_OrderProcess mod = new Plan_OrderProcess();
                    mod.Merchandising_StyleFK = v.Merchandising_StyleFK;
                    mod.PlanStartDate = v.PlanStartDate;
                    mod.PlanFinishDate = v.PlanFinishDate;
                    mod.ActualStartDate = v.ActualStartDate;
                    mod.ActualFinishDate = v.ActualFinishDate;
                    mod.Plan_OrderActionStepFK = v.Plan_OrderActionStepFK;
                    lstAdd.Add(mod);
                }
                _db.Plan_OrderProcess.AddRange(lstAdd);
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = model.Merchandising_StyleFK;
                }
            }
            return result;
        }

        public async Task<int> OrderProcessEdit(VMOrderStep model)
        {
            var result = -1;
            if (model.SaveDataList.Any())
            {
                foreach (var v in model.SaveDataList)
                {
                    var take = _db.Plan_OrderProcess.FirstOrDefault(a => a.ID == v.ID);
                    if (v.IsTaken)
                    {
                        take.Active = false;
                    }
                    else
                    {
                        take.PlanStartDate = v.PlanStartDate;
                        take.PlanFinishDate = v.PlanFinishDate;
                        take.ActualStartDate = v.ActualStartDate;
                        take.ActualFinishDate = v.ActualFinishDate;
                    }
                }
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = model.ID;
                }
            }
            return result;
        }

        public List<VMOrderProcess> GetAllStepByStyleID(int sid)
        {
            var vPlan = (from t1 in _db.Plan_OrderProcess
                        join t2 in _db.Plan_OrderActionStep on t1.Plan_OrderActionStepFK equals t2.ID
                        join t3 in _db.Plan_OrderAction on t2.Plan_OrderActionFK equals t3.ID
                        where t1.Merchandising_StyleFK==sid && t1.Active==true
                        select new VMOrderProcess
                        {
                            ActionName = t3.Name,
                            ActionStepName = t2.Name,
                            PlanStartDate = t1.PlanStartDate,
                            PlanFinishDate = t1.PlanFinishDate,
                            ActualStartDate = t1.ActualFinishDate,
                            ActualFinishDate = t1.ActualFinishDate,
                            Merchandising_StyleFK = t1.Merchandising_StyleFK,
                            Plan_OrderActionFK = t3.ID,
                            Plan_OrderActionStepFK = t1.Plan_OrderActionStepFK,
                            ActionSequence=t3.Priority,
                            StepSequence=t2.Priority

                        }).OrderBy(a => a.ActionSequence).ThenBy(a => a.StepSequence).ToList();

            return vPlan;
        }

        #region Time&ActionReport
        private List<VMOrderStepReport> GetOrderStepPlan(DateTime FromDate, DateTime ToDate, VMOrderStepReport ListStepPlan)
        {
            int totalDay = (int)(ListStepPlan.PlanFinishDate - ListStepPlan.PlanStartDate).TotalDays + 1;
            bool isExtened = ListStepPlan.ActualFinishDate > ListStepPlan.PlanFinishDate == true ? true : false;
            bool isStart = ListStepPlan.ActualStartDate != DateTime.MinValue == true ? true : false;

            List<VMOrderStepReport> lstPlan = new List<VMOrderStepReport>();
            while (FromDate <= ToDate)
            {
                VMOrderStepReport dPlan = new VMOrderStepReport();
                dPlan.HeadDate = FromDate.ToString("MMM-dd");
                dPlan.ViewName = ListStepPlan.ViewName;
                if (isExtened && isStart)
                {
                    if (ListStepPlan.ActualStartDate <= FromDate && ListStepPlan.ActualFinishDate >= FromDate)
                    {
                        dPlan.DayColor = "background-color:red; color:white;";
                    }
                    else
                    {
                        dPlan.DayColor = "background-color:#AAB7B8; color:white;";
                        dPlan.ViewName = string.Empty;
                    }
                }
                else if (!isStart && !isExtened)
                {
                    if (ListStepPlan.PlanStartDate <= FromDate && ListStepPlan.PlanFinishDate >= FromDate)
                    {
                        dPlan.DayColor = "background-color:blue; color:white;";
                    }
                    else
                    {
                        dPlan.DayColor = "background-color:#AAB7B8; color:white;";
                        dPlan.ViewName = string.Empty;
                    }
                }
                else if (!isExtened && isStart)
                {
                    if (ListStepPlan.PlanStartDate <= FromDate && ListStepPlan.PlanFinishDate >= FromDate)
                    {
                        dPlan.DayColor = "background-color:green; color:white;";
                        dPlan.ViewName = ListStepPlan.ViewName;
                    }
                    else
                    {
                        dPlan.DayColor = "background-color:#AAB7B8; color:white;";
                        dPlan.ViewName = string.Empty;
                    }
                }
                dPlan.ColSpan = totalDay;
                lstPlan.Add(dPlan);
                FromDate = FromDate.AddDays(1);
            }
            return lstPlan;
        }

        private List<VMOrderStepReport> GetOrderStepPlanFollowup(DateTime FromDate, DateTime ToDate, VMOrderStepReport ListStepPlan)
        {
            int totalDay = (int)(ListStepPlan.PlanFinishDate - ListStepPlan.PlanStartDate).TotalDays + 1;

            List<VMOrderStepReport> lstPlan = new List<VMOrderStepReport>();
            while (FromDate <= ToDate)
            {
                #region Plan
                VMOrderStepReport dPlan = new VMOrderStepReport();
                dPlan.HeadDate = FromDate.ToString("MMM-dd");
                dPlan.ViewName = ListStepPlan.ViewName;
                if (ListStepPlan.PlanStartDate <= FromDate && ListStepPlan.PlanFinishDate >= FromDate)
                {
                    
                    dPlan.DayColor = "background-color:blue; color:white;";
                }
                else
                {
                    dPlan.DayColor = "background-color:#AAB7B8; color:white;";
                    dPlan.ViewName = string.Empty;
                }
                dPlan.ColSpan = totalDay;
                lstPlan.Add(dPlan);
                #endregion
                
                FromDate = FromDate.AddDays(1);
            }
            return lstPlan;
        }

        private List<VMOrderStepReport> GetOrderActualPlan(DateTime FromDate, DateTime ToDate, List<VMOrderStepReport> ListStepPlan)
        {
            List<VMOrderStepReport> lstPlan = new List<VMOrderStepReport>();

            while (FromDate <= ToDate)
            {
                if (ListStepPlan.Any(a => a.PlanStartDate <= FromDate && a.PlanFinishDate >= FromDate))
                {
                    var data = ListStepPlan.FirstOrDefault(a => a.PlanStartDate <= FromDate && a.PlanFinishDate >= FromDate);
                    var getAllStep = ListStepPlan.FirstOrDefault(a => a.Plan_OrderActionStepFK == data.Plan_OrderActionStepFK);
                    DateTime StartDate = getAllStep.PlanStartDate;
                    DateTime EndDate = getAllStep.PlanFinishDate;

                    int totalDay = (int)(getAllStep.PlanFinishDate - getAllStep.PlanStartDate).TotalDays + 1;
                    bool isExtened = getAllStep.ActualFinishDate > getAllStep.PlanFinishDate == true ? true : false;
                    bool isStart = getAllStep.ActualStartDate != DateTime.MinValue == true ? true : false;

                    while (StartDate <= EndDate)
                    {
                        VMOrderStepReport dPlan = new VMOrderStepReport();
                        dPlan.HeadDate = StartDate.ToString("MMM dd");
                        if (getAllStep.PlanStartDate <= StartDate && getAllStep.PlanFinishDate >= StartDate)
                        {
                            dPlan.ViewName = getAllStep.ViewName;
                            if (isExtened && isStart)
                            {
                                dPlan.DayColor = "background-color:red; color:white;";
                            }
                            else if (!isStart && !isExtened)
                            {
                                dPlan.DayColor = "background-color:blue; color:white;";
                            }
                            else if (!isExtened && isStart)
                            {
                                dPlan.DayColor = "background-color:green; color:white;";
                            }
                            else
                            {
                                dPlan.DayColor = "background-color:#AAB7B8; color:white;";
                                dPlan.ViewName = string.Empty;
                            }

                            dPlan.ColSpan = totalDay;
                            lstPlan.Add(dPlan);
                            StartDate = StartDate.AddDays(1);
                        }
                    }

                    FromDate = StartDate;
                }
                else
                {
                    VMOrderStepReport dPlan = new VMOrderStepReport();
                    dPlan.HeadDate = FromDate.ToString("MMM dd");
                    dPlan.DayColor = "background-color:#AAB7B8; color:white;";
                    dPlan.ViewName = string.Empty;
                    lstPlan.Add(dPlan);
                }

                FromDate = FromDate.AddDays(1);
            }
            return lstPlan;
        }

        public List<VMOrderStepReport> GetGraphOrderPlan(int StyleID = 2)
        {
            List<VMOrderStepReport> lstPlan = new List<VMOrderStepReport>();
            var vPlanedOrder = (from t1 in _db.Plan_OrderProcess
                                join t2 in _db.Plan_OrderActionStep on t1.Plan_OrderActionStepFK equals t2.ID
                                join t4 in _db.Plan_OrderAction on t2.Plan_OrderActionFK equals t4.ID
                                join t5 in _db.Merchandising_Style on t1.Merchandising_StyleFK equals t5.ID
                                join t6 in _db.Merchandising_BuyerOrder on t5.Merchandising_BuyerOrderFK equals t6.ID
                                where t1.Active == true && t1.Merchandising_StyleFK == StyleID
                                select new VMOrderStepReport
                                {
                                    Style = t5.CID,
                                    StepName = t2.Name,
                                    ViewName = t2.Remarks,
                                    PlanStartDate = t1.PlanStartDate,
                                    PlanFinishDate = t1.PlanFinishDate,
                                    ActualFinishDate = t1.ActualFinishDate,
                                    ActualStartDate = t1.ActualStartDate,
                                    Merchandising_StyleFK = t1.Merchandising_StyleFK,
                                    Plan_OrderActionStepFK = t1.Plan_OrderActionStepFK

                                }).ToList();

            if (vPlanedOrder.Any())
            {
                var takeStep = (from t1 in vPlanedOrder
                                group t1 by new { t1.Plan_OrderActionStepFK, t1.StepName } into a
                                select new VMOrderStep { ID = a.Key.Plan_OrderActionStepFK, ViewName = a.Key.StepName, }).ToList();
                DateTime tempDate= vPlanedOrder.Max(a => a.PlanFinishDate);
                DateTime fDate = vPlanedOrder.Min(a => a.PlanStartDate);
                DateTime tDate = vPlanedOrder.Max(a => a.ActualFinishDate);
                if (tDate<=tempDate)
                {
                    tDate = tempDate;
                }
                //var vStyle = vPlanedOrder.FirstOrDefault();
                //var vStylePlanData = GetOrderActualPlan(fDate, tDate, vPlanedOrder);

                //VMOrderStepReport pStyle = new VMOrderStepReport();
                //pStyle.DataList = new List<VMOrderStepReport>();
                //pStyle.StepName = vStyle.Style;
                //pStyle.DataList.AddRange(vStylePlanData);
                //lstPlan.Add(pStyle);

                foreach (var v in takeStep)
                {
                    var stepData = vPlanedOrder.Where(a => a.Plan_OrderActionStepFK == v.ID).ToList();

                    var vDataPlan = GetOrderStepPlanFollowup(fDate, tDate, stepData.Any() != false ? stepData.FirstOrDefault() : null);
                    VMOrderStepReport planStep = new VMOrderStepReport();
                    planStep.DataList = new List<VMOrderStepReport>();
                    planStep.StepName = v.ViewName;
                    planStep.DataList.AddRange(vDataPlan);
                    lstPlan.Add(planStep);

                    var vData = GetOrderStepPlan(fDate, tDate, stepData.Any() != false ? stepData.FirstOrDefault() : null);
                    VMOrderStepReport pline = new VMOrderStepReport();
                    pline.DataList = new List<VMOrderStepReport>();
                    pline.StepName = v.ViewName;
                    pline.DataList.AddRange(vData);
                    lstPlan.Add(pline);
                }
            }

            return lstPlan;
        }

        public List<VMOrderProcess> GetOrderPlan(int StyleID)
        {
            var vPlanedOrder = (from t1 in _db.Plan_OrderProcess
                                join t2 in _db.Plan_OrderActionStep on t1.Plan_OrderActionStepFK equals t2.ID
                                join t4 in _db.Plan_OrderAction on t2.Plan_OrderActionFK equals t4.ID
                                join t5 in _db.Merchandising_Style on t1.Merchandising_StyleFK equals t5.ID
                                join t6 in _db.Merchandising_BuyerOrder on t5.Merchandising_BuyerOrderFK equals t6.ID
                                where t1.Active == true && t1.Merchandising_StyleFK == StyleID
                                select new VMOrderProcess
                                {
                                    ID=t1.ID,
                                    ActionName = t4.Name,
                                    ActionStepName = t2.Name,
                                    PlanStartDate = t1.PlanStartDate,
                                    PlanFinishDate = t1.PlanFinishDate,
                                    ActualFinishDate = t1.ActualFinishDate,
                                    ActualStartDate = t1.ActualStartDate,
                                    Merchandising_StyleFK = t1.Merchandising_StyleFK,
                                    Plan_OrderActionStepFK = t1.Plan_OrderActionStepFK
                                }).OrderBy(a=>a.ID).ToList();

            return vPlanedOrder;
        }

        public List<VMOrderStepReport> GetGraphOrderActionPlan(int StyleID = 2)
        {
            List<VMOrderStepReport> lstPlan = new List<VMOrderStepReport>();
            var vPlanedOrder = (from t1 in _db.Plan_OrderProcess
                                join t2 in _db.Plan_OrderActionStep on t1.Plan_OrderActionStepFK equals t2.ID
                                join t4 in _db.Plan_OrderAction on t2.Plan_OrderActionFK equals t4.ID
                                join t5 in _db.Merchandising_Style on t1.Merchandising_StyleFK equals t5.ID
                                join t6 in _db.Merchandising_BuyerOrder on t5.Merchandising_BuyerOrderFK equals t6.ID
                                where t1.Active == true && t1.Merchandising_StyleFK == StyleID
                                select new VMOrderStepReport
                                {
                                    Style = t5.CID,
                                    StepName = t4.Name,
                                    PlanStartDate = t1.PlanStartDate,
                                    PlanFinishDate = t1.PlanFinishDate,
                                    ActualFinishDate = t1.ActualFinishDate,
                                    ActualStartDate = t1.ActualStartDate,
                                    Merchandising_StyleFK = t1.Merchandising_StyleFK,
                                    Plan_OrderActionStepFK = t1.Plan_OrderActionStepFK

                                }).ToList();

            if (vPlanedOrder.Any())
            {
                var takeStep = (from t1 in vPlanedOrder
                                group t1 by new { t1.Plan_OrderActionStepFK, t1.StepName } into a
                                select new VMOrderStep { ID = a.Key.Plan_OrderActionStepFK, ViewName = a.Key.StepName, }).ToList();
                DateTime tempDate = vPlanedOrder.Max(a => a.PlanFinishDate);
                DateTime fDate = vPlanedOrder.Min(a => a.PlanStartDate);
                DateTime tDate = vPlanedOrder.Max(a => a.ActualFinishDate);
                if (tDate <= tempDate)
                {
                    tDate = tempDate;
                }
                //var vStyle = vPlanedOrder.FirstOrDefault();
                //var vStylePlanData = GetOrderActualPlan(fDate, tDate, vPlanedOrder);

                //VMOrderStepReport pStyle = new VMOrderStepReport();
                //pStyle.DataList = new List<VMOrderStepReport>();
                //pStyle.StepName = vStyle.Style;
                //pStyle.DataList.AddRange(vStylePlanData);
                //lstPlan.Add(pStyle);

                foreach (var v in takeStep)
                {
                    var stepData = vPlanedOrder.Where(a => a.Plan_OrderActionStepFK == v.ID).ToList();

                    var vDataPlan = GetOrderStepPlanFollowup(fDate, tDate, stepData.Any() != false ? stepData.FirstOrDefault() : null);
                    VMOrderStepReport planStep = new VMOrderStepReport();
                    planStep.DataList = new List<VMOrderStepReport>();
                    planStep.StepName = v.ViewName;
                    planStep.DataList.AddRange(vDataPlan);
                    lstPlan.Add(planStep);

                    var vData = GetOrderStepPlan(fDate, tDate, stepData.Any() != false ? stepData.FirstOrDefault() : null);
                    VMOrderStepReport pline = new VMOrderStepReport();
                    pline.DataList = new List<VMOrderStepReport>();
                    pline.StepName = v.ViewName;
                    pline.DataList.AddRange(vData);
                    lstPlan.Add(pline);
                }
            }

            return lstPlan;
        }
        #endregion
        #endregion

        #region QCType
        public VMQCType QCTypeGet()
        {
            var vData = _db.Prod_QCType.Where(x=> x.Active == true).Select(t1=>new VMQCType {
                ID = t1.ID,
                Name = t1.Name,
                IsDeprecate = t1.IsDeprecate,
                Status=t1.IsDeprecate==true? "Deprecated":"Activated"
            }).OrderBy(a => a.ID).ToList();

            VMQCType LineDevice = new VMQCType
            {
                DataList = vData
            };
            return LineDevice;
        }

        public VMQCType QCTypeGetByID(int ID)
        {
            var vData = _db.Prod_QCType.Where(x => x.ID == ID).Select(t1 => new VMQCType
            {
                ID = t1.ID,
                Name = t1.Name,
                IsDeprecate = t1.IsDeprecate,
                ActionId = (int)ActionEnum.Edit
            }).FirstOrDefault();
            
            return vData;
        }

        public async Task<int> QCTypeAdd(VMQCType vMQC)
        {
            var result = -1;
            Prod_QCType qcType = new Prod_QCType();
            qcType.Name = vMQC.Name;
            qcType.IsDeprecate = vMQC.IsDeprecate;
            qcType.Remarks = vMQC.Remarks;
            qcType.Active = true;
            qcType.UserID = vMQC.UserID;
            _db.Prod_QCType.Add(qcType);

            if (await _db.SaveChangesAsync() > 0)
            {
                result = qcType.ID;
            }
            return result;
        }

        public async Task<int> QCTypeEdit(VMQCType vMQCType)
        {
            var result = -1;
            Prod_QCType qcType = _db.Prod_QCType.Find(vMQCType.ID);
            qcType.Name = vMQCType.Name;
            qcType.IsDeprecate = vMQCType.IsDeprecate;
            qcType.Remarks = vMQCType.Remarks;
            qcType.Active = true;
            qcType.UserID = vMQCType.UserID;
            if (await _db.SaveChangesAsync() > 0)
            {
                result = qcType.ID;
            }
            return result;
        }

        public async Task<int> QCTypeDelete(int ID)
        {
            var result = -1;
            Prod_QCType qcType = _db.Prod_QCType.Find(ID);
            if (qcType != null)
            {
                qcType.Active = false;
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = qcType.ID;
                }
            }
            return result;
        }

        #endregion

        #region QCOrder
        public VMStyle GetStyle()
        {
            VMStyle vm = new VMStyle();
            vm = integrationService.GetAllStyle();
            vm.DataList.ForEach(a =>
            {
                a.IsCount = _db.Prod_QCOrder.Any(x => x.Merchandising_StyleFK == a.ID);
                a.RefBOM= a.IsCount ? "label-success" : "label-danger";
            });
            return vm;
        }

        public VMOrderQC StyleQCGetByStyleID(int StyleID)
        {
            var vData = (from t1 in _db.Prod_QCOrder
                         join t2 in _db.Prod_QCType on t1.Prod_QCTypeFK equals t2.ID
                         where t1.Active == true && t1.Merchandising_StyleFK==StyleID
                         select new VMOrderQC
                         {
                             ID = t1.ID,
                             Merchandising_StyleFK = t1.Merchandising_StyleFK,
                             QCType = t2.Name,
                             Prod_QCTypeFK = t1.Prod_QCTypeFK,
                             Priority = t1.Priority,
                             Remarks=t1.Remarks
                         }).OrderBy(a => a.Priority).ToList();

            VMOrderQC orderQC = new VMOrderQC
            {
                Merchandising_StyleFK=StyleID,
                VMStyle= integrationService.StyleGetByID(StyleID),
                DataList = vData
            };
            return orderQC;
        }

        public VMOrderQC OrderQCGetByID(int ID)
        {
            var vData = _db.Prod_QCOrder.Where(x => x.ID == ID).Select(t1 => new VMOrderQC
            {
                ID = t1.ID,
                Merchandising_StyleFK = t1.Merchandising_StyleFK,
                Prod_QCTypeFK = t1.Prod_QCTypeFK,
                Priority = t1.Priority,
                Remarks=t1.Remarks,
                ActionId = (int)ActionEnum.Edit
            }).FirstOrDefault();

            return vData;
        }

        public async Task<int> OrderQCAdd(VMOrderQC vMQCOrder)
        {
            var result = -1;
            Prod_QCOrder qcOrder = new Prod_QCOrder();
            qcOrder.Prod_QCTypeFK = vMQCOrder.Prod_QCTypeFK;
            qcOrder.Merchandising_StyleFK = vMQCOrder.Merchandising_StyleFK;
            qcOrder.Priority = vMQCOrder.Priority;
            qcOrder.Remarks = vMQCOrder.Remarks;
            qcOrder.Active = true;
            qcOrder.UserID = vMQCOrder.UserID;

            _db.Prod_QCOrder.Add(qcOrder);

            if (await _db.SaveChangesAsync() > 0)
            {
                result = qcOrder.ID;
            }
            return result;
        }

        public async Task<int> OrderQCEdit(VMOrderQC vMQCOrder)
        {
            var result = -1;
            Prod_QCOrder qcType = _db.Prod_QCOrder.Find(vMQCOrder.ID);
            qcType.Prod_QCTypeFK = vMQCOrder.Prod_QCTypeFK;
            qcType.Priority = vMQCOrder.Priority;
            qcType.Remarks = vMQCOrder.Remarks;
            qcType.Active = true;
            qcType.UserID = vMQCOrder.UserID;
            if (await _db.SaveChangesAsync() > 0)
            {
                result = qcType.ID;
            }
            return result;
        }

        public async Task<int> OrderQCDelete(int ID)
        {
            var result = -1;
            Prod_QCOrder qcType = _db.Prod_QCOrder.Find(ID);
            if (qcType != null)
            {
                qcType.Active = false;
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = qcType.ID;
                }
            }
            return result;
        }
        #endregion

        #region DropdownControl

        public List<DropList> DDLShipmentByBPOStyleID(int StyleID)
        {
            //var vPlaned = (from t1 in db.Prod_OrderPlanning
            //               where t1.Mkt_BOMFK == BomId && t1.Active == true
            //               select new DropList
            //               {
            //                   ID = t1.ShipmentDate,
            //                   Value = 0
            //               }).ToList();

            //var vShipment = (from t1 in db.Mkt_OrderDeliverySchedule
            //                 join t2 in db.Common_TheOrder on t1.Common_TheOrderFk equals t2.ID
            //                 join t3 in db.Mkt_BOM on t1.Mkt_BOMFk equals t3.ID
            //                 where t1.Active == true && t3.Active == true && t2.Active == true && t1.Mkt_BOMFk == BomId
            //                 group t1.Quantity by new { t1.Date } into a
            //                 select new DropList
            //                 {
            //                     ID = (DateTime)a.Key.Date,
            //                     Value = (int)a.Sum()
            //                 }).ToList();
            List<DropList> lst = new List<DropList>();
            //if (vShipment.Any())
            //{
            //    foreach (var v in vShipment)
            //    {
            //        if (!vPlaned.Any(a => a.ID == v.ID))
            //        {
            //            v.Name = v.ID;
            //            lst.Add(v);
            //        }
            //        v.Name = v.ID;
            //        //v.Name = string.Format("{0:MM/dd/yyyy}", v.ID) + " " + v.Value;
            //    }
            //}
            return lst;
        }

        public List<object> DDLCuttingTable()
        {
            var ItemList = new List<object>();
            var vData = ProductionLineGetByProcessID((int)EnumProcess.Cutting);
            foreach (var v in vData)
            {
                ItemList.Add(new { Text = v.Name, Value = v.ID });
            }
            return ItemList;
        }
        public List<object> StyleDropDownList()
        {
            var StyleList = new List<object>();
            var vData = (from t1 in _db.Merchandising_Style
                         join t2 in _db.Merchandising_BuyerOrder on t1.Merchandising_BuyerOrderFK equals t2.ID
                         where t1.Active == true
                         select new { ID = t1.ID, Name = t1.CID + "/" + t2.BuyerPO + "/" + t1.StyleName }).ToList();

            foreach (var style in vData)
            {
                StyleList.Add(new { Text = style.Name, Value = style.ID });
            }
            return StyleList;
        }
        public List<object> DDLStyle()
        {
            var StyleList = new List<object>();
            var vData = (from t1 in _db.Merchandising_Style
                         join t2 in _db.Merchandising_BuyerOrder on t1.Merchandising_BuyerOrderFK equals t2.ID
                         where t1.Active == true
                         select new { ID = t1.ID, Name = t1.CID + "/" + t2.BuyerPO + "/" + t1.StyleName}).ToList();

            foreach (var style in vData)
            {
                StyleList.Add(new { Text = style.Name, Value = style.ID });
            }
            return StyleList;
        }

        public List<object> DDLSewingLine()
        {
            var ItemList = new List<object>();

            var vData = ProductionLineGetByProcessID((int)EnumProcess.Sewing);
            foreach (var v in vData)
            {
                ItemList.Add(new { Text = v.Name, Value = v.ID });
            }
            return ItemList;
        }

        public List<object> DDLProductionStep()
        {
            var ItemList = new List<object>();
            ItemList.Add(new { Value = 1, Text = "Cutting", });
            ItemList.Add(new { Value = 2, Text = "Sewing" });
            ItemList.Add(new { Value = 3, Text = "Iron" });
            ItemList.Add(new { Value = 4, Text = "Packing" });

            return ItemList;
        }

        /// <summary>
        /// Style wise SMV
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="IsMaster"></param>
        /// <returns></returns>
        public List<object> DDLSMVByStyleID(int ID, bool IsMaster)
        {
            var ItemList = new List<object>();
            if (IsMaster)
            {
                var vData = from t1 in _db.Plan_SMVMasterLayout
                            join t2 in _db.Merchandising_Style on t1.Merchandising_StyleFK equals t2.ID
                            join t3 in _db.Merchandising_BuyerOrder on t2.Merchandising_BuyerOrderFK equals t3.ID
                            where t1.Merchandising_StyleFK == ID && t1.Active == true
                            select new
                            {
                                ID = t1.ID,
                                Name = t1.Remarks+ " [" + t1.TotalSMV + "] " + t3.BuyerPO + "/" + t2.StyleName
                            };
                foreach (var v in vData)
                {
                    ItemList.Add(new { Text = v.Name, Value = v.ID });
                }
            }
            else
            {
                var vData = from t1 in _db.Plan_SMVDraftLayout
                            join t2 in _db.Merchandising_Style on t1.Merchandising_StyleFK equals t2.ID
                            join t3 in _db.Merchandising_BuyerOrder on t2.Merchandising_BuyerOrderFK equals t3.ID
                            where t1.Merchandising_StyleFK == ID && t1.Active == true
                            select new
                            {
                                ID = t1.ID,
                                Name = t1.Remarks + " [" + t1.TotalSMV + "] " + t3.BuyerPO + "/" + t2.StyleName
                            };
                foreach (var v in vData)
                {
                    ItemList.Add(new { Text = v.Name, Value = v.ID });
                }
            }
            
            return ItemList;
        }

        public VMSMVLayout GetPrePlanSMVByID(int ID)
        {
            var vData = _db.Plan_SMVDraftLayout.Where(a => a.ID == ID && a.Active == true).Select(a => new VMSMVLayout
            {
                ID = a.ID,
                Merchandising_StyleFk = a.Merchandising_StyleFK,
                NoOfOperator = a.NoOfOperator,
                NoOfHelper = a.NoOfHelper,
                OperatorSMV = a.OperatorSMV,
                HelperSMV = a.HelperSMV,
                TotalWorker = a.TotalWorker,
                TotalSMV = a.TotalSMV,
                WorkingHour = a.WorkingHour,
                PerHourTarget = a.PerHourTarget,
                Efficiency = a.Efficiency,
                Description = a.Remarks
            });
            if (vData.Any())
            {
                return vData.FirstOrDefault();
            }

            return null;
        }

        //public List<object> DDLPlanSMVByStyleID(int ID)
        //{
        //    var ItemList = new List<object>();
        //    var vData = from t1 in _db.Plan_SMVMasterLayout
        //                join t2 in _db.Merchandising_Style on t1.Merchandising_StyleFK equals t2.ID
        //                join t3 in _db.Merchandising_BuyerOrder on t2.Merchandising_BuyerOrderFK equals t3.ID
        //                where t1.Merchandising_StyleFK == ID && t1.Active == true
        //                select new
        //                {
        //                    ID = t1.ID,
        //                    Name = "[" + t1.TotalSMV + "] " + t3.BuyerPO + "/" + t2.StyleName
        //                };
        //    foreach (var v in vData)
        //    {
        //        ItemList.Add(new { Text = v.Name, Value = v.ID });
        //    }
        //    return ItemList;
        //}

        public VMSMVLayout GetPlanSMVByID(int ID)
        {
            var vData = _db.Plan_SMVMasterLayout.Where(a => a.ID == ID && a.Active == true).Select(a => new VMSMVLayout
            {
                ID = a.ID,
                Merchandising_StyleFk = a.Merchandising_StyleFK,
                NoOfOperator = a.NoOfOperator,
                NoOfHelper = a.NoOfHelper,
                OperatorSMV = a.OperatorSMV,
                HelperSMV = a.HelperSMV,
                TotalWorker = a.TotalWorker,
                TotalSMV = a.TotalSMV,
                WorkingHour = a.WorkingHour,
                PerHourTarget = a.PerHourTarget,
                Efficiency = a.Efficiency,
                Description = a.Remarks
            });
            if (vData.Any())
            {
                return vData.FirstOrDefault();
            }

            return null;
        }

        public List<object> DDLSubSetListByStyleID(int StyleID)
        {
            var StyleList = new List<object>();
            var vData = _db.Merchandising_StyleSetPack.Where(a => a.Merchandising_StyleFK == StyleID && a.Active == true).Select(a => new
            {
                ID = a.ID,
                Name = a.SetPackName
            }).ToList();
            if (vData.Any())
            {
                foreach (var style in vData)
                {
                    StyleList.Add(new { Text = style.Name, Value = style.ID });
                }
            }
            return StyleList;
        }

        public List<object> GetAllColorByStyleID(int ID)
        {
            var ItemList = new List<object>();
            var vData = from t1 in _db.Merchandising_StyleMeasurement
                        join t2 in _db.Common_Color on t1.Common_ColorFK equals t2.ID
                        where t1.Merchandising_StyleFK == ID && t1.Active == true
                        group t1 by new { t1.Common_ColorFK, t2.Name } into all
                        select new
                        {
                            ID = all.Key.Common_ColorFK,
                            Name = all.Key.Name
                        };
            foreach (var v in vData)
            {
                ItemList.Add(new { Text = v.Name, Value = v.ID });
            }
            return ItemList;
        }

        public List<object> DDLStyleByRefID(int ID)
        {
            var ItemList = new List<object>();
            var vData = from t1 in _db.Prod_ReferencePlan
                        join t2 in _db.Merchandising_Style on t1.Merchandising_StyleFK equals t2.ID
                        join t4 in _db.Merchandising_BuyerOrder on t2.Merchandising_BuyerOrderFK equals t4.ID
                        where t1.Prod_ReferenceFK == ID && t1.Active == true
                        select new
                        {
                            ID = t1.Merchandising_StyleFK,
                            Name = t2.CID + "/" + t4.BuyerPO + "/" + t2.StyleName
                        };
            foreach (var v in vData)
            {
                ItemList.Add(new { Text = v.Name, Value = v.ID });
            }
            return ItemList;
        }

        public List<object> DDLCuttingStyleColor(int StyleID, int RefID)
        {
            var ItemList = new List<object>();
            var vData = from t1 in _db.Prod_ReferencePlan
                        join t2 in _db.Common_Color on t1.Common_ColorFK equals t2.ID
                        where t1.Merchandising_StyleFK == StyleID && t1.Active == true && t1.SectionId == 1 && t1.Prod_ReferenceFK == RefID
                        select new
                        {
                            ID = t1.Common_ColorFK,
                            Name = t2.Name
                        };
            foreach (var v in vData)
            {
                ItemList.Add(new { Text = v.Name, Value = v.ID });
            }
            return ItemList;
        }

        public List<object> DDLCuttingStyleColorLayer(int StyleID, int RefID)
        {
            var ItemList = new List<object>();
            var vData = from t1 in _db.Prod_ReferencePlan
                        join t2 in _db.Common_Color on t1.Common_ColorFK equals t2.ID
                        where t1.Merchandising_StyleFK == StyleID && t1.Active == true && t1.SectionId == 1 && t1.Prod_ReferenceFK == RefID
                        select new
                        {
                            ID = t1.Common_ColorFK,
                            Name = t1.CID+"-"+t2.Name+"-"+t1.FabricLayer
                        };
            foreach (var v in vData)
            {
                ItemList.Add(new { Text = v.Name, Value = v.ID });
            }
            return ItemList;
        }

        public VMDailyProduction GetCuttingPlanInfo(int RefID, int StyleID, int ColorID)
        {
            var vData = (from t1 in _db.Prod_ReferencePlan
                         where t1.Merchandising_StyleFK == StyleID && t1.Active == true
                         && t1.SectionId == 1 && t1.Prod_ReferenceFK == RefID && t1.Common_ColorFK == ColorID
                         select new VMDailyProduction
                         {
                             Prod_ReferencePlanFK = t1.ID,
                             GSM = t1.GSMORSMV,
                             TargetQty = t1.PlanQuantity
                         }).FirstOrDefault();

            return vData;
        }

        public VMDailyProduction GetSMVPaperByID(int Id)
        {
            var vData = (from o in _db.Plan_SMVMasterLayout
                         join p in _db.Merchandising_Style on o.Merchandising_StyleFK equals p.ID
                         join q in _db.Merchandising_StyleSetPack on o.Merchandising_StyleSetPackFK equals q.ID into Set_Join
                         from r in Set_Join.DefaultIfEmpty()
                         where o.ID == Id && o.Active == true
                         select new VMDailyProduction
                         {
                             Merchandising_StyleFK = p.ID,
                             Merchandising_StyleSetPackFK=o.Merchandising_StyleSetPackFK==null?0:(int)o.Merchandising_StyleSetPackFK,
                             SubSetName=r.SetPackName,
                             PresentOpt = o.NoOfOperator,
                             NoOfHelper = o.NoOfHelper,
                             SMV = o.TotalSMV,
                             PerHourTarget = o.PerHourTarget,
                             WorkingHour = o.WorkingHour,
                             Style = r.SetPackName+"-"+o.Remarks,
                         }).AsEnumerable();
            if (vData.Any())
            {
                var getData = vData.FirstOrDefault();
                return getData;
            }
            else
            {
                return null;
            }
        }

        public List<object> DDLSewingPlanOrder(int RefID)
        {
            var OrderList = new List<object>();
            var vData = (from t1 in _db.Prod_ReferencePlan
                         join t2 in _db.Merchandising_Style on t1.Merchandising_StyleFK equals t2.ID
                         join t3 in _db.Merchandising_BuyerOrder on t2.Merchandising_BuyerOrderFK equals t3.ID
                         join t5 in _db.Plan_SMVMasterLayout on t1.Plan_SMVMasterLayoutFK equals t5.ID
                         join t6 in _db.Common_ProductionLine on t1.ProductionLineFK equals t6.ID into all
                         from t7 in all.DefaultIfEmpty()
                         where t1.Active == true && t1.Prod_ReferenceFK == RefID && t1.SectionId == 2
                         group t1.PlanQuantity by new {t1.ID, t1.Merchandising_StyleFK,t1.Prod_ReferenceFK, t5.Remarks,t5.TotalSMV, t3.BuyerPO,t2.StyleName, t7.Code,t7.Name } into all
                         select new
                         {
                             //ID = t1.Merchandising_StyleFK + "/" + t1.ID,
                             //Name = t5.Remarks + "[SMV-" + t5.TotalSMV + "]" + t3.BuyerPO + "/" + t2.StyleName
                             ID = all.Key.Merchandising_StyleFK + "/" + all.Key.ID,
                             Name =all.Key.Code+"-"+all.Key.Name+"-"+ all.Key.Remarks + "[SMV-" + all.Key.TotalSMV + "]" + all.Key.BuyerPO + "/" + all.Key.StyleName 
                         }).OrderBy(a => a.ID).ToList();

            foreach (var order in vData)
            {
                OrderList.Add(new { Text = order.Name, Value = order.ID });
            }
            return OrderList;
        }

        public List<object> DDLStyleForCuttingFollowup(int RefID)
        {
            var OrderList = new List<object>();

            var vData = (from t1 in _db.Prod_ReferencePlan
                         join t2 in _db.Merchandising_Style on t1.Merchandising_StyleFK equals t2.ID
                         join t3 in _db.Merchandising_BuyerOrder on t2.Merchandising_BuyerOrderFK equals t3.ID
                         where t1.Active == true && t1.Prod_ReferenceFK == RefID && t1.SectionId == 1
                         group t1.PlanQuantity by new {t1.ID, t1.Merchandising_StyleFK,t1.FabricLayer, t1.Prod_ReferenceFK, t3.BuyerPO, t2.StyleName } into all
                         select new
                         {
                             ID = all.Key.Merchandising_StyleFK + "/" + all.Key.ID,
                             Name = all.Key.BuyerPO + "/" + all.Key.StyleName+"-"+all.Key.FabricLayer
                             
                         }).OrderBy(a => a.ID).ToList();

            foreach (var order in vData)
            {
                OrderList.Add(new { Text = order.Name, Value = order.ID });
            }
            return OrderList;
        }

        public List<object> DDLStyleForCutting()
        {
            var OrderList = new List<object>();
            var vData = (from t1 in _db.Merchandising_StyleShipmentSchedule
                         join t2 in _db.Merchandising_Style on t1.Merchandising_StyleFK equals t2.ID
                         join t3 in _db.Merchandising_BuyerOrder on t2.Merchandising_BuyerOrderFK equals t3.ID
                         join t6 in _db.Merchandising_StyleShipmentRatio on t1.ID equals t6.Merchandising_StyleShipmentScheduleFK
                         where t1.Active == true && t6.Active
                         group t2 by new { t1.Merchandising_StyleFK, t3.BuyerPO, t2.StyleName } into all
                         select new
                         {
                             ID = all.Key.Merchandising_StyleFK,
                             Name = all.Key.BuyerPO + "/" + all.Key.StyleName
                         }).OrderBy(a => a.ID).ToList();

            foreach (var order in vData)
            {
                OrderList.Add(new { Text = order.Name, Value = order.ID });
            }
            return OrderList;
        }

        public List<object> DDLStyleForSewing()
        {
            var OrderList = new List<object>();

            var vData = (from t1 in _db.Plan_SMVMasterLayout
                         join t2 in _db.Merchandising_Style on t1.Merchandising_StyleFK equals t2.ID
                         join t3 in _db.Merchandising_BuyerOrder on t2.Merchandising_BuyerOrderFK equals t3.ID
                         //join t4 in _db.Merchandising_StyleSetPack on t1.Merchandising_StyleSetPackFK equals t4.ID into Set_Join
                         //from t5 in Set_Join.DefaultIfEmpty()
                         where t3.Active == true && t3.IsComplete == false && t1.Active == true
                         select new
                         {
                             ID = t1.ID,
                             Name = t1.Remarks + "[ SMV-" + t1.TotalSMV + " ]" + t3.BuyerPO + "/" + t2.StyleName,
                         }).OrderByDescending(a => a.ID).ToList();

            foreach (var order in vData)
            {
                OrderList.Add(new { Text = order.Name, Value = order.ID });
            }
            return OrderList;
        }

        public List<object> DDLStyleForPlanIroning()
        {
            var OrderList = new List<object>();

            var vData = (from t1 in _db.Prod_ReferencePlan
                         join t2 in _db.Merchandising_Style on t1.Merchandising_StyleFK equals t2.ID
                         join t3 in _db.Merchandising_BuyerOrder on t2.Merchandising_BuyerOrderFK equals t3.ID
                         where t3.Active == true && t1.Active == true && t1.SectionId == 2
                         group t2 by new { t2.ID, t3.BuyerPO, t2.StyleName } into all
                         select new
                         {
                             ID = all.Key.ID,
                             Name = all.Key.BuyerPO + "/" + all.Key.StyleName,
                         }).OrderByDescending(a => a.ID).ToList();

            foreach (var order in vData)
            {
                OrderList.Add(new { Text = order.Name, Value = order.ID });
            }
            return OrderList;
        }

        public List<object> DDLStyleForFollowupIroning(int Ref)
        {
            var OrderList = new List<object>();

            var vData = (from t1 in _db.Prod_ReferencePlan
                         join t2 in _db.Merchandising_Style on t1.Merchandising_StyleFK equals t2.ID
                         join t3 in _db.Merchandising_BuyerOrder on t2.Merchandising_BuyerOrderFK equals t3.ID
                         where t3.Active == true && t1.Active == true && t1.SectionId == 3 && t1.Prod_ReferenceFK == Ref
                         group t2 by new { t1.ID, t1.Merchandising_StyleFK, t3.BuyerPO, t2.StyleName } into all
                         select new
                         {
                             ID = all.Key.Merchandising_StyleFK + "/" + all.Key.ID,
                             Name = all.Key.BuyerPO + "/" + all.Key.StyleName,
                         }).OrderByDescending(a => a.ID).ToList();

            foreach (var order in vData)
            {
                OrderList.Add(new { Text = order.Name, Value = order.ID });
            }
            return OrderList;
        }

        public List<object> DDLStyleForPacking()
        {
            var OrderList = new List<object>();

            var vData = (from t1 in _db.Prod_ReferencePlan
                         join t2 in _db.Merchandising_Style on t1.Merchandising_StyleFK equals t2.ID
                         join t3 in _db.Merchandising_BuyerOrder on t2.Merchandising_BuyerOrderFK equals t3.ID
                         where t3.Active == true && t3.IsComplete == false && t1.Active == true && t1.SectionId == 2
                         group t2 by new { t2.ID, t3.BuyerPO, t2.StyleName } into all
                         select new
                         {
                             ID = all.Key.ID,
                             Name = all.Key.BuyerPO + "/" + all.Key.StyleName,
                         }).OrderByDescending(a => a.ID).ToList();

            foreach (var order in vData)
            {
                OrderList.Add(new { Text = order.Name, Value = order.ID });
            }
            return OrderList;
        }

        public List<object> DDLStyleForFollowupPacking(int Ref)
        {
            var OrderList = new List<object>();

            var vData = (from t1 in _db.Prod_ReferencePlan
                         join t2 in _db.Merchandising_Style on t1.Merchandising_StyleFK equals t2.ID
                         join t3 in _db.Merchandising_BuyerOrder on t2.Merchandising_BuyerOrderFK equals t3.ID
                         where t3.Active == true && t1.Active == true && t1.SectionId == 4 && t1.Prod_ReferenceFK == Ref
                         group t2 by new { t1.ID, t1.Merchandising_StyleFK, t3.BuyerPO, t2.StyleName } into all
                         select new
                         {
                             ID = all.Key.Merchandising_StyleFK + "/" + all.Key.ID,
                             Name = all.Key.BuyerPO + "/" + all.Key.StyleName,
                         }).OrderByDescending(a => a.ID).ToList();

            foreach (var order in vData)
            {
                OrderList.Add(new { Text = order.Name, Value = order.ID });
            }
            return OrderList;
        }

        public List<object> DDLStyleFollowUp(int RefID, int SectionID)
        {
            var OrderList = new List<object>();
            if (SectionID == 1)
            {
                var vData = (from t1 in _db.Prod_ReferencePlan
                             join t2 in _db.Merchandising_Style on t1.Merchandising_StyleFK equals t2.ID
                             join t3 in _db.Merchandising_BuyerOrder on t2.Merchandising_BuyerOrderFK equals t3.ID
                             where t1.Active == true && t1.Prod_ReferenceFK == RefID && t1.SectionId == 1
                             group t1 by new { t2.StyleName, t1.Merchandising_StyleFK, t3.BuyerPO } into all
                             select new
                             {
                                 ID = all.Key.Merchandising_StyleFK,
                                 Name = all.Key.BuyerPO + "/" + all.Key.StyleName
                             }).OrderBy(a => a.ID).ToList();
                foreach (var order in vData)
                {
                    OrderList.Add(new { Text = order.Name, Value = order.ID });
                }
            }
            else if (SectionID == 2)
            {
                var vData = (from t1 in _db.Prod_ReferencePlan
                             join t2 in _db.Merchandising_Style on t1.Merchandising_StyleFK equals t2.ID
                             join t3 in _db.Merchandising_BuyerOrder on t2.Merchandising_BuyerOrderFK equals t3.ID
                             join t5 in _db.Plan_SMVMasterLayout on t1.Plan_SMVMasterLayoutFK equals t5.ID
                             where t1.Active == true && t1.Prod_ReferenceFK == RefID && t1.SectionId == 2
                             select new
                             {
                                 ID = t1.Merchandising_StyleFK + "/" + t1.ID,
                                 Name = t5.Remarks + "|" + "[SMV-" + t5.TotalSMV + "]" + t3.BuyerPO + "/" + t2.StyleName
                             }).OrderBy(a => a.ID).ToList();

                foreach (var order in vData)
                {
                    OrderList.Add(new { Text = order.Name, Value = order.ID });
                }
            }
            else if (SectionID == 3)
            {
                var vData = (from t1 in _db.Prod_ReferencePlan
                             join t2 in _db.Merchandising_Style on t1.Merchandising_StyleFK equals t2.ID
                             join t3 in _db.Merchandising_BuyerOrder on t2.Merchandising_BuyerOrderFK equals t3.ID
                             where t1.Active == true && t1.Prod_ReferenceFK == RefID && t1.SectionId == 3
                             select new
                             {
                                 ID = t1.Merchandising_StyleFK + "/" + t1.ID,
                                 Name = t3.BuyerPO + "/" + t2.StyleName
                             }).OrderBy(a => a.ID).ToList();

                foreach (var order in vData)
                {
                    OrderList.Add(new { Text = order.Name, Value = order.ID });
                }
            }
            else if (SectionID == 4)
            {

            }

            return OrderList;
        }

        //public List<object> DDLSewingLineByStyleID(int RefID, int StyleID)
        //{
        //    var OrderList = new List<object>();
        //    var vData = (from t1 in _db.Prod_ReferencePlan
        //                 join t2 in _db.Common_ProductionLine on t1.ProductionLineFK equals t2.ID
        //                 where t1.Active == true && t1.SectionId == 2 && t1.Prod_ReferenceFK == RefID && t1.Merchandising_StyleFK == StyleID
        //                 select new
        //                 {
        //                     ID = t1.ProductionLineFK,
        //                     Name = t2.Name
        //                 }).OrderBy(a => a.ID).ToList();

        //    foreach (var order in vData)
        //    {
        //        OrderList.Add(new { Text = order.Name, Value = order.ID });
        //    }
        //    return OrderList;
        //}

        public List<object> DDLLineChiefDropDownList()
        {
            var LineChiefList = new List<object>();
            foreach (var LineChief in _db.Common_LineChief.Where(a => a.Active == true).ToList())
            {
                LineChiefList.Add(new { Text = LineChief.Name, Value = LineChief.ID });
            }
            return LineChiefList;
        }

        public List<object> DDLLineSuperVisorDropDownList()
        {
            var LineSuperVisorList = new List<object>();
            foreach (var LineSuperVisor in _db.Common_LineSuperVisor.Where(a => a.Active == true).ToList())
            {
                LineSuperVisorList.Add(new { Text = LineSuperVisor.Name, Value = LineSuperVisor.ID });
            }
            return LineSuperVisorList;
        }

        public List<object> DDLSewingLineByStyleID(int RefID, int StyleID)
        {
            var OrderList = new List<object>();

            var vData = (from t1 in _db.Prod_ReferencePlan
                          join t2 in _db.Common_ProductionLine on t1.ProductionLineFK equals t2.ID
                          where t1.Active == true && t1.SectionId == (int)EnumProcess.Sewing && t1.Prod_ReferenceFK == RefID && t1.Merchandising_StyleFK == StyleID
                          select new
                          {
                              LID = t1.ProductionLineFK,
                              LCode=t2.Code,
                              Name = t2.Name
                          }).OrderBy(a => a.LID).ToList();

            foreach (var v in vData)
            {
                OrderList.Add(new { Text = v.LCode, Value =v.LID });
            }
            return OrderList;
        }

        public List<object> DDLProductionLineDropDownList()
        {
            var ProductionLineList = new List<object>();
            var vData = ProductionLineGetByProcessID(0);
            foreach (var ProductionLine in vData)
            {
                ProductionLineList.Add(new { Text = ProductionLine.Name, Value = ProductionLine.ID });
            }
            return ProductionLineList;
        }

        public List<VMProductionLine> ProductionLineGetByProcessID(int ProductionProcessID)
        {
            if (ProductionProcessID > 0)
            {
                var vLineData = (from t1 in _db.Common_ProductionLine
                             join t2 in _db.Common_SectionLine on t1.Common_SectionLineFK equals t2.ID
                             where t1.Active == true && t1.ProductionStepFK == ProductionProcessID && t1.IsClosed == false
                             select new VMProductionLine
                             {
                                 Name=t1.Code+"-"+t1.Name,
                                 ID = t1.ID,
                                 Priority = t1.Priority
                             }).ToList();
                return vLineData;
            }
            else
            {
                var vLineData = (from t1 in _db.Common_ProductionLine
                                 join t2 in _db.Common_SectionLine on t1.Common_SectionLineFK equals t2.ID
                                 where t1.Active == true && t1.IsClosed == false
                                 select new VMProductionLine
                                 {
                                     Name = t1.Code + "-" + t1.Name,
                                     ID = t1.ID,
                                     Priority = t1.Priority
                                 }).ToList();
                
                return vLineData;
            }
        }

        public List<object> DDLDeviceLine()
        {
            var DeviceLine = new List<object>();
            var vData = ProductionLineGetByProcessID((int)EnumProcess.Sewing);
            var vAssignData = _db.Common_LineDevice.Where(a => a.IsClossed == false).ToList();
            if (vData.Any())
            {
                foreach (var v in vData)
                {
                    if (!vAssignData.Any(a=>a.Common_ProductionLineFK==v.ID))
                    {
                        DeviceLine.Add(new { Text = v.Name, Value = v.ID });
                    }
                }
            }
            return DeviceLine;
        }

        public List<object> DDLQCType()
        {
            var qcType = new List<object>();
            var vAssignData = _db.Prod_QCType.Where(a =>a.Active==true && a.IsDeprecate == false).ToList();
            if (vAssignData.Any())
            {
                foreach (var v in vAssignData)
                {
                    qcType.Add(new { Text = v.Name, Value = v.ID });
                }
            }
            return qcType;
        }

        public List<object> DDLLineMan()
        {
            var ItemList = new List<object>();
            ItemList.Add(new { Value = 1, Text = "Line Chief", });
            ItemList.Add(new { Value = 2, Text = "Line Supervisor" });

            return ItemList;
        }
        public List<object> DDLPlanType()
        {
            var ItemList = new List<object>();
            ItemList.Add(new { Value = 1, Text = "Pre-Plan" });
            ItemList.Add(new { Value = 2, Text = "Execution Plan" });
            return ItemList;
        }

        #region ChangeViewBag
        public List<object> DDLBuilding()
        {
            var ItemList = new List<object>();
            ItemList.Add(new { Value = 1, Text = "Production Unit-1" });
            ItemList.Add(new { Value = 2, Text = "Production Unit-2" });
            ItemList.Add(new { Value = 3, Text = "Production Unit-3" });
            ItemList.Add(new { Value = 4, Text = "Production Unit-4" });
            ItemList.Add(new { Value = 5, Text = "Production Unit-5" });
            return ItemList;
        }
        public List<object> DDLFloor()
        {
            var ItemList = new List<object>();
            ItemList.Add(new { Value = 1, Text = "Ground Floor" });
            ItemList.Add(new { Value = 2, Text = "1st Floor" });
            ItemList.Add(new { Value = 3, Text = "2nd Floor" });
            ItemList.Add(new { Value = 4, Text = "3rd Floor" });
            ItemList.Add(new { Value = 5, Text = "4th Floor" });
            ItemList.Add(new { Value = 6, Text = "5th Floor" });
            ItemList.Add(new { Value = 7, Text = "6th Floor" });
            return ItemList;
        }

        #endregion
        public List<object> DDLUnit()
        {
            var unitList = new List<object>();
            var unit = (from t1 in _db.HRMS_Unit
                        join t2 in _db.HRMS_BusinessUnit on t1.HRMS_BusinessUnitFK equals t2.ID
                        where t1.Active==true
                        select new
                        {
                            ID = t1.ID,
                            Name = t2.Name + "| " + t1.Name

                        }).ToList();
            foreach (var item in unit)
            {
                unitList.Add(new { Text = item.Name, Value = item.ID });
            }
            return unitList;

           
        }

        public List<object> DDProductionUnit()
        {
            var unitList = new List<object>();

            var unit = (from t1 in _db.Common_ProductionUnit
                        join t2 in _db.HRMS_Unit on t1.HRMS_UnitFK equals t2.ID
                        where t1.Active==true
                        select new
                        {
                            ID=t1.ID,
                            Name= t2.Name+" | "+t1.Name
                        }).ToList();
            foreach (var item in unit)
            {
                unitList.Add(new { Text = item.Name, Value = item.ID });
            }
            return unitList;
        }
        public List<object> DDLSectionLine()
        {
            var sectionlineList = new List<object>();

            var sectionline = (from t1 in _db.Common_SectionLine
                               join t2 in _db.Common_ProductionFloor on t1.Common_ProductionFloorFK equals t2.ID
                               join t3 in _db.Common_ProductionUnit on t2.Common_ProductionUnitFK equals t3.ID
                               where t1.Active==true
                               select new
                               {
                                   ID=t1.ID,
                                   Name=t3.Name+"| "+t2.Name +"| "+t1.Name

                               }).ToList();
            foreach (var item in sectionline)
            {
                sectionlineList.Add(new { Text = item.Name, Value = item.ID });
            }
            return sectionlineList;
        }
       
        public List<object> DDLProductionFloor()
        {
            var unitList = new List<object>();
            var FlorList = (from t1 in _db.Common_ProductionFloor
                            join t2 in _db.Common_ProductionUnit on t1.Common_ProductionUnitFK equals t2.ID
                            where t1.Active==true && t2.Active==true
                            select new
                            {
                                Name = t2.Name + "|" + t1.Name,
                                ID = t1.ID
                            }).ToList();
            foreach (var x in FlorList)
            {
                unitList.Add(new { Text = x.Name, Value = x.ID });
            }
           
            return unitList;
        }

        public List<object> GetProductionReport()
        {
            var ReportType = new List<object>();
            ReportType.Add(new { Text = "Monthly Report-Buyer", Value = "1" });
            ReportType.Add(new { Text = "Monthly Report-Order", Value = "2" });
            ReportType.Add(new { Text = "Monthly Line-Order", Value = "3" });
            ReportType.Add(new { Text = "Monthly Achievement", Value = "4" });
            return ReportType;
        }

        public List<object> DDLRemarks()
        {
            var remarksList = new List<object>();
            var vData = _db.Prod_Remarks.Where(t1 => t1.Active == true && t1.IsClose == false).Select(a => new {
                Name = a.Name,
                ID = a.ID
            }).ToList();
            foreach (var x in vData)
            {
                remarksList.Add(new { Text = x.Name, Value = x.ID });
            }
            return remarksList;
        }
        #endregion

        #region ModelObject
        public VMDailyProduction GetLineChiefAndSuperVisorByID(int LineId)
        {
            var vChief = (from o in _db.Common_LineChiefLine
                          join p in _db.Common_LineChief on o.Common_LineChiefFk equals p.ID
                          where o.Common_ProductionLineFk == LineId && o.Active == true && o.IsLineChief == true
                          select new
                          {
                              CheifId = p.ID,
                              CheifName = p.Name
                          }).ToList();

            var vSuperVisor = (from o in _db.Common_LineChiefLine
                               join p in _db.Common_LineSuperVisor on o.Common_LineSuperVisorFk equals p.ID
                               where o.Common_ProductionLineFk == LineId && o.Active == true && o.IsLineChief == false
                               select new
                               {
                                   SVID = p.ID,
                                   SuperVisor = p.Name
                               }).ToList();

            if (vChief.Any() && vSuperVisor.Any())
            {
                var getChief = vChief.FirstOrDefault();
                var getSV = vSuperVisor.FirstOrDefault();
                VMDailyProduction model = new VMDailyProduction();
                model.Prod_LineChiefFK = getChief.CheifId;
                model.LineChiefSupervisor = getChief.CheifName;
                model.Prod_SuperVisorFK = getSV.SVID;
                model.SuperVisor = getSV.SuperVisor;
                return model;
            }
            else if (vChief.Any() && !vSuperVisor.Any())
            {
                var getChief = vChief.FirstOrDefault();
                VMDailyProduction model = new VMDailyProduction();
                model.Prod_LineChiefFK = getChief.CheifId;
                model.LineChiefSupervisor = getChief.CheifName;
                model.SuperVisor = string.Empty;
                return model;
            }
            else if (!vChief.Any() && vSuperVisor.Any())
            {
                var getSV = vSuperVisor.FirstOrDefault();
                VMDailyProduction model = new VMDailyProduction();
                model.LineChiefSupervisor = string.Empty;
                model.Prod_SuperVisorFK = getSV.SVID;
                model.SuperVisor = getSV.SuperVisor;
                return model;
            }
            else
            {
                VMDailyProduction model = new VMDailyProduction();
                return model;
            }
        }

        #endregion

        #region Report
        public List<VMOrderPlanning> GetPlanReport(VMOrderPlanning model)
        {
            bool PrePlan = model.ID == 1 ? true : false;
            if (PrePlan)
            {
                var vOrderData = (from t1 in _db.Plan_DraftOrderPlan
                                  join t2 in _db.Merchandising_Style on t1.Merchandising_StyleFK equals t2.ID
                                  join t3 in _db.Merchandising_BuyerOrder on t2.Merchandising_BuyerOrderFK equals t3.ID
                                  join t4 in _db.Common_Buyer on t3.Common_BuyerFK equals t4.ID
                                  where t1.ShipmentDate >= model.SewingStartDate
                                  && t1.ShipmentDate <= model.ShipmentDate
                                  && t1.IsClosed == false && t1.Active == true
                                  select new VMOrderPlanning
                                  {
                                      Plan_DraftOrderPlanFK = t1.ID,
                                      OrderNumber = t4.Name,
                                      StyleNo = t4.BuyerID + "/" + t3.BuyerPO + "/" + t2.StyleName,
                                      PlanQty = (int)t1.PlanQty,
                                      ShipmentDate = t1.ShipmentDate,
                                      CuttingStartDate = t1.CuttingDate,
                                      SewingStartDate = t1.SewingDate,
                                      SampleApprovedDate = t1.SampleApprovedDate,
                                      FabricInhouseDate = t1.FabricInDate,
                                      AccessoriesInhouseDate = t1.AccessoriesInDate,
                                      OrderPlanQty = (int)decimal.Multiply(decimal.Multiply(t2.PackQty, t2.PackPieceQty), t2.SetQuantity),
                                      LinePlanList = (from a in _db.Plan_DraftLinePlan
                                                      join b in _db.Common_ProductionLine on a.Common_ProductionLineFK equals b.ID
                                                      where a.Plan_DraftOrderPlanFK == t1.ID && a.Active == true
                                                      select new VMLinePlanning
                                                      {
                                                          ID = a.ID,
                                                          LineName = b.Name,
                                                          MachineQuantity = a.NoOfOperator,
                                                          StartDate = a.StartDate,
                                                          FinishDate = a.FinishDate,
                                                          TargetDays = a.TargetDays,
                                                          OccupiedHour = (int)a.LineHour,
                                                          WorkingHour = a.WorkingHour,
                                                          Capacity = a.HourTargetPlan,
                                                          DayOutputQty = a.DayOutputQty,
                                                          TargetOutputQty = a.TargetOutputQty,
                                                          RemainQty = a.RemainQty,
                                                          Status = a.IsClosed ? "Closed" : a.IsPaused ? "Paused" : "Running",
                                                      }).OrderBy(a => a.ID).ToList()

                                  }).ToList();
                return vOrderData;
            }
            else
            {
                var vOrderData = (from t1 in _db.Plan_MasterOrderPlan
                                  join t2 in _db.Merchandising_Style on t1.Merchandising_StyleFK equals t2.ID
                                  join t3 in _db.Merchandising_BuyerOrder on t2.Merchandising_BuyerOrderFK equals t3.ID
                                  join t4 in _db.Common_Buyer on t3.Common_BuyerFK equals t4.ID
                                  where t1.ShipmentDate >= model.SewingStartDate
                                  && t1.ShipmentDate <= model.ShipmentDate
                                  && t1.IsClosed == false && t1.Active == true
                                  select new VMOrderPlanning
                                  {
                                      Plan_DraftOrderPlanFK = t1.ID,
                                      OrderNumber = t4.Name,
                                      StyleNo = t4.BuyerID + "/" + t3.BuyerPO + "/" + t2.StyleName,
                                      PlanQty = (int)t1.PlanQty,
                                      ShipmentDate = t1.ShipmentDate,
                                      CuttingStartDate = t1.CuttingDate,
                                      SewingStartDate = t1.SewingDate,
                                      SampleApprovedDate = t1.SampleApprovedDate,
                                      FabricInhouseDate = t1.FabricInDate,
                                      AccessoriesInhouseDate = t1.AccessoriesInDate,
                                      OrderPlanQty = (int)decimal.Multiply(decimal.Multiply(t2.PackQty, t2.PackPieceQty), t2.SetQuantity),
                                      LinePlanList = (from a in _db.Plan_MasterLinePlan
                                                      join b in _db.Common_ProductionLine on a.Common_ProductionLineFK equals b.ID
                                                      where a.PLan_MasterOrderPlanFK == t1.ID && a.Active == true
                                                      select new VMLinePlanning
                                                      {
                                                          ID = a.ID,
                                                          LineName = b.Name,
                                                          MachineQuantity = a.NoOfOperator,
                                                          StartDate = a.StartDate,
                                                          FinishDate = a.FinishDate,
                                                          TargetDays = a.TargetDays,
                                                          OccupiedHour = (int)a.LineHour,
                                                          WorkingHour = a.WorkingHour,
                                                          Capacity = a.HourTargetPlan,
                                                          DayOutputQty = a.DayOutputQty,
                                                          TargetOutputQty = a.TargetOutputQty,
                                                          RemainQty = a.RemainQty,
                                                          Status = a.IsClosed ? "Closed" : a.IsPaused ? "Paused" : "Running",
                                                      }).OrderBy(a => a.ID).ToList()

                                  }).ToList();
                return vOrderData;
            }
        }

        public VMProductionReport GetTargetAchivementReport(VMProductionReport model)
        {
            List<VMProductionReport> lstLine = new List<VMProductionReport>();
            var getAllLines = (from t1 in _db.Common_ProductionLine
                               join t2 in _db.Common_SectionLine on t1.Common_SectionLineFK equals t2.ID
                               join t3 in _db.Common_ProductionFloor on t2.Common_ProductionFloorFK equals t3.ID
                               join t4 in _db.Common_ProductionUnit on t3.Common_ProductionUnitFK equals t4.ID
                               where t1.Active == true && t1.IsClosed == false && t1.ProductionStepFK == 2
                               select new VMProductionLine
                               {
                                   ID = t1.ID,
                                   Priority = t1.Priority,
                                   Name = t1.Name,
                                   ProductionUnitFK = t4.ID,
                                   ProductionFloorFK = t3.ID,
                                   Common_SectionLineFK = t2.ID
                               }).OrderBy(a => a.Priority).ToList();
            List<VMProductionLine> lstFilterLine = new List<VMProductionLine>();

            var allData = (from t1 in _db.Prod_ReferencePlan
                           join t2 in _db.Common_ProductionLine on t1.ProductionLineFK equals t2.ID
                           join t3 in _db.Merchandising_Style on t1.Merchandising_StyleFK equals t3.ID
                           join t7 in _db.Merchandising_BuyerOrder on t3.Merchandising_BuyerOrderFK equals t7.ID
                           join t9 in _db.Common_Buyer on t7.Common_BuyerFK equals t9.ID
                           join t10 in _db.Plan_SMVMasterLayout on t1.Plan_SMVMasterLayoutFK equals t10.ID
                           join t11 in _db.Prod_Reference on t1.Prod_ReferenceFK equals t11.ID
                           where t1.Prod_ReferenceFK == model.ProdReferenceId && t1.SectionId == 2 && t1.Active==true
                           select new VMProductionReport
                           {
                               ProdReferencePlanId = t1.ID,
                               ProdReferenceId=t1.Prod_ReferenceFK,
                               StyleID = t1.Merchandising_StyleFK,
                               LineId = (int)t1.ProductionLineFK,
                               LineName = t2.Name,
                               BuyerPO = t9.Name + "/" + t7.BuyerPO,
                               StyleName = t9.BuyerID + "/" + t7.BuyerPO + "/" + t3.StyleName,

                               QtyLineMachine = t10.NoOfOperator,
                               QtyMachine = t1.MachineRunning,
                               QtyPresentOP = t1.PresentOperator,
                               QtyPresentHP = t1.PresentHelper,
                               WorkingHour = t1.WorkingHour,
                               QtyTotalWorker = t1.PresentOperator + t1.PresentHelper,
                               SMV = t1.GSMORSMV,
                               HourlyTarget = t10.PerHourTarget,

                               TotalTarget = t1.PlanQuantity,
                               FromDate = t11.ReferenceDate,
                               QtyInputMan = t1.InputMan,
                               QtySizeMan = t1.SizeMan,
                               QtyLineMan = t1.LineMan
                               //TotalAchivement = (int)(from t1 in _db.Prod_ReferencePlanFollowup
                               //                        where t1.Prod_ReferencePlanFK == t1.ID
                               //                        //group t1.Quantity by new { t1.Prod_ReferencePlanFK } into all
                               //                        //from t2 in all.DefaultIfEmpty(0)
                               //                        select new
                               //                        {
                               //                            total = t1.Quantity
                               //                        }).Sum(a => a.total)
                           }).ToList();


            if (model.ProductionUnitID != 0 && model.ProductionFloorID != 0)
            {
                lstFilterLine = getAllLines.Any() != false ? getAllLines.Where(a => a.ProductionUnitFK == model.ProductionUnitID && a.ProductionFloorFK == model.ProductionFloorID).ToList() : null;
            }
            else if (model.ProductionUnitID != 0 && model.ProductionFloorID == 0)
            {
                lstFilterLine = getAllLines.Any() != false ? getAllLines.Where(a => a.ProductionUnitFK == model.ProductionUnitID).ToList() : null;
            }
            else if (model.ProductionUnitID == 0 && model.ProductionFloorID != 0)
            {
                lstFilterLine = getAllLines.Any() != false ? getAllLines.Where(a => a.ProductionFloorFK == model.ProductionFloorID).ToList() : null;
            }
            else if (model.ProductionUnitID == 0 && model.ProductionFloorID == 0)
            {
                lstFilterLine = getAllLines.Any() != false ? getAllLines : null;
            }
            int LineTotalMachineQty = 0;
            if (lstFilterLine.Any())
            {
                foreach (var line in lstFilterLine)
                {
                    if (allData.Any(a => a.LineId == line.ID))
                    {
                        var GetLineInfo = allData.Where(a => a.LineId == line.ID).ToList();

                        LineTotalMachineQty += GetLineInfo.FirstOrDefault().QtyLineMachine;
                        foreach (var v in GetLineInfo)
                        {
                            var getRemarks = (from t1 in _db.Prod_DailyAchivement
                                             join t2 in _db.Prod_Remarks on t1.Prod_RemarksFK equals t2.ID into all
                                             from t3 in all.DefaultIfEmpty()
                                             where t1.Prod_ReferenceFK == v.ProdReferenceId && t1.Merchandising_StyleFK == v.StyleID && t1.ProductionLineFK == v.LineId && t1.Active == true
                                             select new { ID=t1.ID,RemarksID=t1.Prod_RemarksFK, Remarks=t3.Name }).ToList();

                            var vTotal = (from t1 in _db.Prod_ReferencePlanFollowup
                                          where t1.Prod_ReferencePlanFK == v.ProdReferencePlanId
                                          select new
                                          {
                                              total = t1.Quantity
                                          }).ToList();
                            if (vTotal.Any())
                            {
                                v.AchiveQty = (int)vTotal.Sum(a => a.total);
                            }
                            v.ID = getRemarks.Any() == true ? getRemarks.FirstOrDefault().ID : 0;
                            v.HourlyAchivement = v.AchiveQty / v.WorkingHour;
                            v.TargetEffi = Math.Round(((v.SMV * v.TotalTarget) / (v.WorkingHour * 60 * v.QtyTotalWorker)) * 100, 2);
                            v.AchiveEffi = Math.Round(((v.SMV * v.AchiveQty) / (v.WorkingHour * 60 * v.QtyTotalWorker)) * 100, 2);
                            v.TargetBalance = v.AchiveQty - v.TotalTarget;
                            v.RemarksID= getRemarks.Any() == true ? getRemarks.FirstOrDefault().RemarksID : 0;
                            v.Remarks = getRemarks.Any() == true ? getRemarks.FirstOrDefault().Remarks : string.Empty;
                            v.TotalMachineinLine = LineTotalMachineQty;
                            lstLine.Add(v);
                        }
                    }
                }
            }
            model.DataList = lstLine;
            return model;
        }

        public async Task<int> AchivementAdd(VMProductionReport model)
        {
            var result = -1;
            if (model.DataList.Any())
            {
                var vData = _db.Prod_DailyAchivement.Where(a => a.Prod_ReferenceFK ==model.ProdReferenceId);
                
                foreach (var v in model.DataList)
                {
                    if (vData.Any(x => x.ID == v.ID))
                    {
                        var update = _db.Prod_DailyAchivement.FirstOrDefault(a => a.ID == v.ID);
                        update.TargetBalance = v.TargetBalance;
                        update.TargetEfficiency = v.TargetEffi;
                        update.TotalAchievQty = v.AchiveQty;
                        update.AchiveEfficiency = v.AchiveEffi;
                        update.Prod_RemarksFK = v.RemarksID;
                        update.Time = DateTime.Now;
                        update.UserID = v.UserID;
                        update.User = v.User;
                        update.Active = true;
                    }
                    else
                    {
                        Prod_DailyAchivement achiev = new Prod_DailyAchivement();
                        achiev.Prod_ReferenceFK = v.ProdReferenceId;
                        achiev.Prod_ReferencePlanFk = v.ProdReferencePlanId;
                        achiev.Merchandising_StyleFK = v.StyleID;
                        achiev.ProductionLineFK = v.LineId;
                        achiev.TargetBalance = v.TargetBalance;
                        achiev.TargetEfficiency = v.TargetEffi;
                        achiev.TotalAchievQty = v.AchiveQty;
                        achiev.AchiveEfficiency = v.AchiveEffi;
                        achiev.Prod_RemarksFK = v.RemarksID;
                        achiev.Remarks = v.Remarks;
                        achiev.Time = DateTime.Now;
                        achiev.UserID = v.UserID;
                        achiev.User = v.User;
                        achiev.Active = true;
                        _db.Prod_DailyAchivement.Add(achiev);
                    }
                }

                if (await _db.SaveChangesAsync() > 0)
                {
                    result = model.ProdReferenceId;
                }
            }
            return result;
        }

        public List<VMProductionReport> GetPreodicOrderProduction(VMProductionReport model)
        {
            List<VMProductionReport> lst = new List<VMProductionReport>();

            var vPreData = (from t1 in _db.Prod_Reference
                            join t2 in _db.Prod_ReferencePlan on t1.ID equals t2.Prod_ReferenceFK
                            join t3 in _db.Merchandising_Style on t2.Merchandising_StyleFK equals t3.ID
                            join t4 in _db.Merchandising_BuyerOrder on t3.Merchandising_BuyerOrderFK equals t4.ID
                            join t5 in _db.Prod_ReferencePlanFollowup on t2.ID equals t5.Prod_ReferencePlanFK
                            join t6 in _db.Prod_ReferencePlanFollowupDetails  on t5.ID equals t6.Prod_ReferencePlanFollowupFK
                            where t1.ReferenceDate < model.FromDate && t2.SectionId == 2 && t3.Active == true && t4.Active == true
                            group t6.Quantity by new { t2.Merchandising_StyleFK } into all
                            select new VMProductionReport
                            {
                                StyleID = all.Key.Merchandising_StyleFK,
                                Quantity = (int)all.Sum()
                            }).ToList().OrderBy(a => a.StyleID);

            var vData = (from t1 in _db.Prod_Reference
                         join t2 in _db.Prod_ReferencePlan on t1.ID equals t2.Prod_ReferenceFK
                         join t3 in _db.Merchandising_Style on t2.Merchandising_StyleFK equals t3.ID
                         join t4 in _db.Merchandising_BuyerOrder on t3.Merchandising_BuyerOrderFK equals t4.ID
                         join t5 in _db.Prod_ReferencePlanFollowup on t2.ID equals t5.Prod_ReferencePlanFK
                         join t6 in _db.Prod_ReferencePlanFollowupDetails on t5.ID equals t6.Prod_ReferencePlanFollowupFK
                         join t7 in _db.Common_Buyer on t4.Common_BuyerFK equals t7.ID
                         where t1.ReferenceDate >= model.FromDate && t1.ReferenceDate <= model.ToDate && t2.SectionId == 2 && t1.Active == true && t2.Active == true && t5.Active == true && t6.Active==true
                         group t6.Quantity by new { t2.Merchandising_StyleFK, t4.BuyerPO, t3.StyleName,t7.Name,t3.PackQty } into all
                         select new VMProductionReport
                         {
                             StyleID = all.Key.Merchandising_StyleFK,
                             BuyerPO = all.Key.BuyerPO,
                             StyleName = all.Key.StyleName,
                             BuyerName = all.Key.Name,
                             PackQty=all.Key.PackQty,
                             //PackPice = all.Key.Quantity * all.Key.QPack,
                             Quantity = (int)all.Sum()
                         }).ToList().OrderBy(a => a.StyleID);

            if (vData.Any())
            {
                foreach (var v in vData)
                {
                    v.TargetBalance = vPreData.Any(a => a.StyleID == v.StyleID) == true ? vPreData.Where(a => a.StyleID == v.StyleID).Sum(a => a.Quantity) : 0;
                    v.TotalSewing = v.Quantity + v.TargetBalance;
                    v.SewingBalance = v.TotalSewing - v.PackPice;
                }
            }

            return vData.ToList();
        }

        private string ShortDateString(DateTime Date)
        {
            string datestring = string.Empty;

            string day = Date.Day < 10 ? "0" + Date.Day : Date.Day.ToString();
            string month = Date.Month < 10 ? "0" + Date.Month : Date.Month.ToString();
            int year = Date.Year;

            datestring = day + "-" + month + "-" + year;

            return datestring;
        }

        public DataTable GetPreodicProductionBuyerWise(VMProductionReport model)
        {
            DataTable ds = new DataTable();
            ds.Columns.Add("Date/Buyer");

            var vDetails = (from t1 in _db.Prod_Reference
                            join t2 in _db.Prod_ReferencePlan on t1.ID equals t2.Prod_ReferenceFK
                            join t3 in _db.Merchandising_Style on t2.Merchandising_StyleFK equals t3.ID
                            join t4 in _db.Merchandising_BuyerOrder on t3.Merchandising_BuyerOrderFK equals t4.ID
                            join t5 in _db.Common_Buyer on t4.Common_BuyerFK equals t5.ID
                            join t6 in _db.Prod_ReferencePlanFollowup on t2.ID equals t6.Prod_ReferencePlanFK
                            join t7 in _db.Prod_ReferencePlanFollowupDetails on t6.ID equals t7.Prod_ReferencePlanFollowupFK
                            where t1.ReferenceDate >= model.FromDate && t1.ReferenceDate <= model.ToDate && t2.SectionId == 2 && t3.Active == true && t4.Active == true && t5.Active == true
                            group t7.Quantity by new { t1.ReferenceDate, t2.Merchandising_StyleFK,t3.SetQuantity, t4.BuyerPO,t5.BuyerID } into all
                            select new
                            {
                                refDate = all.Key.ReferenceDate,
                                styleId = all.Key.Merchandising_StyleFK,
                                buyerPo = all.Key.BuyerID + "/"+all.Key.BuyerPO,
                                qty = all.Sum()* all.Key.SetQuantity

                            }).ToList().OrderBy(a => a.styleId);

            var LineOrder = (from o in vDetails
                             group o.qty by new { o.styleId, o.buyerPo } into g
                             select new { buyerPo = g.Key.buyerPo, styleId = g.Key.styleId }).ToList().OrderBy(a => a.buyerPo);

            int totalBuyer = LineOrder.Count();
            if (totalBuyer > 0)
            {
                foreach (var v in LineOrder)
                {
                    ds.Columns.Add(v.buyerPo);
                }
            }
            ds.Columns.Add("Total");

            DateTime fDate = model.FromDate;
            DateTime tDate = model.ToDate;
            int rowCount = 0;
            while (fDate <= tDate)
            {
                string date = ShortDateString(fDate);
                ds.Rows.Add(date);
                int colCount = 1;
                int RowTotal = 0;
                foreach (var v in LineOrder)
                {
                    var getqty = vDetails.Where(a => a.styleId == v.styleId && a.refDate == fDate);
                    RowTotal += getqty.Any() == true ? (int)getqty.Sum(a => a.qty) : 0;
                    ds.Rows[rowCount][colCount] = getqty.Any() == true ? getqty.Sum(a => a.qty) : 0;
                    ++colCount;
                }
                ds.Rows[rowCount][colCount] = RowTotal;
                ++rowCount;
                fDate = fDate.AddDays(1);
            }
            return ds;
        }

        public List<VMProductionReport> GetPreodicProductionLineWise(VMProductionReport model)
        {
            List<VMProductionReport> lst = new List<VMProductionReport>();
            var allData = (from t0 in _db.Prod_Reference
                           join t1 in _db.Prod_ReferencePlan on t0.ID equals t1.Prod_ReferenceFK
                           join t4 in _db.Common_ProductionLine on t1.ProductionLineFK equals t4.ID
                           join t5 in _db.Merchandising_Style on t1.Merchandising_StyleFK equals t5.ID
                           join t6 in _db.Merchandising_BuyerOrder on t5.Merchandising_BuyerOrderFK equals t6.ID
                           join t7 in _db.Merchandising_StyleSetPack on t1.Merchandising_StyleSetPackFK equals t7.ID into Set
                           from t8 in Set.DefaultIfEmpty()
                           where t0.ReferenceDate >= model.FromDate && t0.ReferenceDate <= model.ToDate && t1.SectionId == 2
                           group t1.PlanQuantity by new { t0.ReferenceDate, t1, t4,t5.CID,t5.ID, t5.StyleName, t6.BuyerPO,t1.GSMORSMV,t8 } into all
                           select new
                           {
                               StyleID= all.Key.ID,
                               FromDate = all.Key.ReferenceDate,
                               LineId = all.Key.t4.ID,
                               LineName = all.Key.t4.Name,
                               OrderNo = all.Key.BuyerPO,
                               Style = all.Key.CID+all.Key.BuyerPO + "/" + all.Key.StyleName,
                               LineHour = all.Key.t1.WorkingHour,
                               SMV = all.Key.t1.GSMORSMV,
                               TotalTarget = all.Key.t1.PlanQuantity,
                               SetName=all.Key.t8.SetPackName,
                               TotalAchivement = (from a in _db.Prod_ReferencePlanFollowup
                                                  join c in _db.Merchandising_StyleShipmentRatio on a.Merchandising_StyleShipmentRatioFK equals c.ID
                                                  join d in _db.Merchandising_StyleMeasurement on c.Merchandising_StyleMeasurementFK equals d.ID
                                                  join e in _db.Common_Color on d.Common_ColorFK equals e.ID
                                                  where a.Prod_ReferencePlanFK == all.Key.t1.ID && a.Active==true
                                                  group a.Quantity by new { e.Name,e.ID } into a
                                                  select new
                                                  {
                                                      Color = a.Key.Name,
                                                      ColorId=a.Key.ID,
                                                      total = a.Sum()
                                                  }).ToList()
                           }).ToList().OrderBy(a=>a.FromDate);


            if (allData.Any())
            {
                foreach (var v in allData)
                {
                    if (v.TotalAchivement.Any())
                    {
                        foreach (var v1 in v.TotalAchivement)
                        {
                            VMProductionReport m1 = new VMProductionReport();
                            m1.LineName = v.LineName;
                            m1.BuyerPO = v.OrderNo;
                            m1.StyleName = v.Style;
                            m1.ColorName = v1.Color;
                            m1.ColorID = v1.ColorId;
                            m1.WorkingHour = v.LineHour;
                            m1.Subset = v.SetName;
                            m1.SMV = v.SMV;
                            m1.TotalTarget = v.TotalTarget;
                            m1.TotalAchivement = (int)v1.total;
                            m1.TotalSewing = GetLineWiseColorTotal(v.StyleID, v.LineId, v1.ColorId, v.FromDate);
                            m1.ProductionDate = ShortDateString(v.FromDate);
                            lst.Add(m1);
                        }
                    }
                    else
                    {
                        VMProductionReport m = new VMProductionReport();
                        m.LineName = v.LineName;
                        m.BuyerPO = v.OrderNo;
                        m.StyleName = v.Style;
                        m.ColorName = string.Empty;
                        m.WorkingHour = v.LineHour;
                        m.SMV = v.SMV;
                        m.TotalTarget = v.TotalTarget;
                        m.TotalAchivement = 0;
                        m.ProductionDate = ShortDateString(v.FromDate);
                        lst.Add(m);
                    }

                }
            }

            return lst;
        }

        private int GetLineWiseColorTotal(int StyleID, int LineId, int ColorID, DateTime date)
        {
            int total = 0;

            var vData = (from t0 in _db.Prod_Reference
                         join t1 in _db.Prod_ReferencePlan on t0.ID equals t1.Prod_ReferenceFK
                         join t2 in _db.Prod_ReferencePlanFollowup on t1.ID equals t2.Prod_ReferencePlanFK
                         join t3 in _db.Merchandising_StyleShipmentRatio on t2.Merchandising_StyleShipmentRatioFK equals t3.ID
                         join t4 in _db.Merchandising_StyleMeasurement on t3.Merchandising_StyleMeasurementFK equals t4.ID
                         join t5 in _db.Common_Color on t4.Common_ColorFK equals t5.ID
                         where t0.ReferenceDate <= date && t1.SectionId == 2 && t1.ProductionLineFK == LineId && t4.Common_ColorFK==ColorID 
                         && t0.Active==true && t1.Active == true && t2.Active == true && t1.Merchandising_StyleFK== StyleID
                         group t2.Quantity by new { t5.Name,t4.ID } into a
                         select new { Color = a.Key.Name, total = a.Sum() }).ToList();
            if (vData.Any())
            {
                return total = (int)vData.Sum(a => a.total);
            }
            else { return total; }
        }

        public void GetOrderCloseReport(int BomId)
        {
            //var vOrder = (from t1 in db.Mkt_BOM
            //              join t2 in db.Common_TheOrder on t1.Common_TheOrderFk equals t2.ID
            //              join t3 in db.Mkt_Buyer on t2.Mkt_BuyerFK equals t3.ID
            //              join t4 in db.Mkt_OrderDeliverySchedule on t1.ID equals t4.Mkt_BOMFk
            //              join t5 in db.Mkt_OrderColorAndSizeRatio on t4.ID equals t5.OrderDeliveryScheduleFk
            //              where t1.ID == BomId && t2.Active == true && t1.Active == true && t4.Active == true && t5.Active == true
            //              group t5.Quantity by new { t1.ID, t2.CID, t1.Style, t3.Name, t2.BuyerPO, t5.Color } into a
            //              select new VMProductionReport
            //              {
            //                  BOMID = a.Key.ID,
            //                  OrderNo = a.Key.CID,
            //                  Style = a.Key.Style,
            //                  BuyerName = a.Key.Name,
            //                  BuyerPO = a.Key.BuyerPO,
            //                  ColorName = a.Key.Color,
            //                  Quantity = (int)a.Sum(),
            //                  Extra = 2 + " %"
            //              }).ToList();

            //if (vOrder.Any())
            //{
            //    decimal extraCutRate = (decimal)0.02;
            //    foreach (var v in vOrder)
            //    {
            //        var vData = GetTotalCuttingProduction(v.BOMID, v.ColorName);
            //        v.ExtraQty = (int)(v.Quantity * extraCutRate);
            //        v.TotalTarget = v.Quantity + v.ExtraQty;
            //        v.TotalStoreReceived = GetTotalFabricReceivedQty(v.BOMID, v.ColorName);
            //        v.BookingConsump = this.BookingConsump;
            //        v.TotalCuttingReceived = GetTotalRequisition(v.BOMID, v.ColorName);
            //        v.StoreBalance = v.TotalStoreReceived - v.TotalCuttingReceived;
            //        v.TotalCutting = vData.TotalCutting;
            //        v.TotalFabricUsed = vData.TotalWeight;
            //        v.CuttingConsumption = vData.CuttingConsumption;
            //        v.FabricBalance = v.TotalCuttingReceived - v.TotalFabricUsed;
            //        v.TotalBalance = v.StoreBalance + v.FabricBalance;
            //    }
            //}
            //this.DataList = vOrder;
        }

        public List<VMProductionReport> GetDailyAchievment(int RefId)
        {
            var allData = (from t1 in _db.Prod_DailyAchivement
                           join t2 in _db.Common_ProductionLine on t1.ProductionLineFK equals t2.ID
                           join t3 in _db.Merchandising_Style on t1.Merchandising_StyleFK equals t3.ID
                           join t7 in _db.Merchandising_BuyerOrder on t3.Merchandising_BuyerOrderFK equals t7.ID
                           join t9 in _db.Prod_Reference on t1.Prod_ReferenceFK equals t9.ID
                           join t10 in _db.Prod_ReferencePlan on t1.Prod_ReferencePlanFk equals t10.ID
                           join t11 in _db.Prod_Remarks on t1.Prod_RemarksFK equals t11.ID into Join_Remarks
                           from t12 in Join_Remarks.DefaultIfEmpty()
                           join t13 in _db.Plan_SMVMasterLayout on t10.Plan_SMVMasterLayoutFK equals t13.ID
                           where t1.Prod_ReferenceFK == RefId
                           select new VMProductionReport
                           {
                               StyleID = t1.Merchandising_StyleFK,
                               LineId = t1.ProductionLineFK,
                               FromDate=t9.ReferenceDate,
                               BuyerPO = t7.BuyerPO,
                               StyleName = t3.StyleName,
                               LineName = t2.Name,
                               TotalMachineinLine = t13.NoOfOperator,
                               QtyLineMachine = t10.MachineRunning,
                               QtyPresentOP = t10.PresentOperator,
                               QtyPresentHP = t10.PresentHelper,
                               QtyTotalWorker = t10.PresentOperator + t10.PresentHelper,
                               WorkingHour = t10.WorkingHour,
                               SMV = t10.GSMORSMV,
                               HourlyTarget = (int)decimal.Divide(t10.PlanQuantity, t10.WorkingHour),
                               TotalTarget = t10.PlanQuantity,
                               HourlyAchivement = t1.TotalAchievQty > 0 ? (int)decimal.Divide(t1.TotalAchievQty, t10.WorkingHour) : 0,
                               AchiveQty = (int)t1.TotalAchievQty,
                               TargetEffi = t1.TargetEfficiency,
                               AchiveEffi = t1.AchiveEfficiency,
                               TargetBalance = t1.TargetBalance,
                               Remarks=t12!=null?t12.Name:""
                           }).OrderBy(a=>a.FromDate).ThenBy(a=>a.LineId).ToList();

            return allData;
        }

        public List<VMProductionReport> GetMonthlyAcheivment(VMProductionReport model)
        {
            DateTime fDate = model.FromDate;
            DateTime tDate = model.ToDate;
            var vData = _db.Prod_Reference.Where(a => a.ReferenceDate >= fDate && a.ReferenceDate <= tDate).ToList();
            List<VMProductionReport> lst = new List<VMProductionReport>();
            while (fDate <= tDate)
            {
                if (vData.Any(a => a.ReferenceDate == fDate))
                {
                    var getData = vData.FirstOrDefault(a => a.ReferenceDate == fDate);

                    var vA = GetDailyAchievment(getData.ID);

                    if (vA.Any())
                    {
                        foreach (var v in vA)
                        {
                            v.ProductionDate = ShortDateString(getData.ReferenceDate);
                            lst.Add(v);
                        }
                    }
                }

                fDate = fDate.AddDays(1);
            }

            return lst;
        }

        #endregion

        #region FutureMethode

        public async Task<VMOrderPlanning> GetPlanByStyleID(int ID)
        {
            var vData = (from t1 in _db.Plan_MasterOrderPlan
                         join t2 in _db.Merchandising_Style on t1.Merchandising_StyleFK equals t2.ID
                         join t3 in _db.Merchandising_BuyerOrder on t2.Merchandising_BuyerOrderFK equals t3.ID
                         join t4 in _db.Common_FinishItem on t2.Common_FinishItemFK equals t4.ID
                         where t1.Merchandising_StyleFK == ID && t1.Active == true
                         select new VMOrderPlanning
                         {
                             ID = t1.ID,
                             Merchandising_StyleFK = t1.Merchandising_StyleFK,
                             //Merchandising_StyleShipmentScheduleFK = (int)t1.Merchandising_StyleShipmentScheduleFK,
                             OrderNumber = t3.BuyerPO,
                             StyleNo = t2.StyleName,
                             ItemName = t4.Name,
                             SampleApprovedDate = t1.SampleApprovedDate,
                             AccessoriesInhouseDate = t1.AccessoriesInDate,
                             CuttingStartDate = t1.CuttingDate,
                             FabricInhouseDate = t1.FabricInDate,
                             OrderPlanQty = (int)t1.PlanQty,
                             ShipmentDate = t1.ShipmentDate,
                             SewingStartDate = t1.SewingDate,
                             ExtraRate = t1.ExtraRate,
                             Remarks = t1.Remarks
                         }).OrderByDescending(a => a.ID).ToList();

            VMOrderPlanning vMReference = new VMOrderPlanning
            {
                DataList = await Task.Run(() => vData)
            };

            return vMReference;
        }

        public async Task<IEnumerable<VMOrderPlanning>> GetAllPrePlanByStyleID(int StyleID)
        {
            var vData = (from t1 in _db.Plan_DraftOrderPlan
                         join t2 in _db.Merchandising_Style on t1.Merchandising_StyleFK equals t2.ID
                         join t3 in _db.Merchandising_BuyerOrder on t2.Merchandising_BuyerOrderFK equals t3.ID
                         join t4 in _db.Common_FinishItem on t2.Common_FinishItemFK equals t4.ID
                         where t1.Merchandising_StyleFK == StyleID && t1.Active == true
                         select new VMOrderPlanning
                         {
                             ID = t1.ID,
                             //Merchandising_StyleShipmentScheduleFK=(int)t1.Merchandising_StyleShipmentScheduleFK,
                             OrderNumber = t3.BuyerPO,
                             StyleNo = t2.StyleName,
                             ItemName = t4.Name,
                             SampleApprovedDate = t1.SampleApprovedDate,
                             AccessoriesInhouseDate = t1.AccessoriesInDate,
                             CuttingStartDate = t1.CuttingDate,
                             FabricInhouseDate = t1.FabricInDate,
                             Merchandising_StyleFK = t1.Merchandising_StyleFK,
                             OrderPlanQty = (int)t1.PlanQty,
                             ShipmentDate = t1.ShipmentDate,
                             Remarks = t1.Remarks
                         }).OrderByDescending(a => a.ShipmentDate).AsEnumerable();

            return await Task.Run(() => vData);
        }

        public async Task<IEnumerable<VMOrderPlanning>> GetAllShipmentByStyleID(int StyleID)
        {
            var vData = (from t1 in _db.Merchandising_StyleShipmentSchedule
                         join t2 in _db.Merchandising_Style on t1.Merchandising_StyleFK equals t2.ID
                         join t3 in _db.Merchandising_BuyerOrder on t2.Merchandising_BuyerOrderFK equals t3.ID
                         join t4 in _db.Common_FinishItem on t2.Common_FinishItemFK equals t4.ID
                         where t1.Merchandising_StyleFK == StyleID && t1.Active == true
                         select new VMOrderPlanning
                         {
                             ID = t1.ID,
                             Merchandising_StyleShipmentScheduleFK = t1.ID,
                             Merchandising_StyleFK = t1.Merchandising_StyleFK,
                             OrderNumber = t3.BuyerPO,
                             StyleNo = t2.StyleName,
                             ItemName = t4.Name,
                             OrderPlanQty = (int)t1.Quantity,
                             ShipmentDate = t1.ShipmentDate,
                             Remarks = t1.Remarks
                         }).OrderByDescending(a => a.ShipmentDate).AsEnumerable();
            return await Task.Run(() => vData);
        }

        public async Task<VMSMVLayout> SMVLayoutGet()
        {
            var vdata = (from t1 in _db.Plan_SMVMasterLayout
                         join t2 in _db.Merchandising_Style on t1.Merchandising_StyleFK equals t2.ID
                         join t4 in _db.Merchandising_BuyerOrder on t2.Merchandising_BuyerOrderFK equals t4.ID
                         join t5 in _db.Common_Buyer on t4.Common_BuyerFK equals t5.ID
                         where t1.Active == true
                         select new VMSMVLayout
                         {
                             ID = t1.ID,
                             Style = t5.BuyerID + "/" + t4.BuyerPO + "/" + t2.StyleName,
                             Merchandising_StyleFk = t1.Merchandising_StyleFK,
                             NoOfOperator = t1.NoOfOperator,
                             NoOfHelper = t1.NoOfHelper,
                             OperatorSMV = t1.OperatorSMV,
                             HelperSMV = t1.HelperSMV,
                             TotalWorker = t1.TotalWorker,
                             TotalSMV = t1.TotalSMV,
                             WorkingHour = t1.WorkingHour,
                             PerHourTarget = t1.PerHourTarget,
                             Efficiency = t1.Efficiency,
                             Description = t1.Remarks
                         }).OrderBy(a => a.Merchandising_StyleFk).ToList();

            VMSMVLayout sMVLayout = new VMSMVLayout
            {
                DataList = await Task.Run(() => vdata)
            };
            return sMVLayout;
        }
        #endregion

        #region Remarks
        public List<VMRemarks> GetRemarks()
        {

            var remark = _db.Prod_Remarks.Where(x => x.Active == true).ToList().ConvertAll(
                    x => new VMRemarks { ID = x.ID, Name = x.Name, IsClose = x.IsClose }).OrderByDescending(x => x.ID).ToList();

            return remark;
        }

        public VMRemarks GetRemarks(int id)
        {
            var remark = (from t1 in _db.Prod_Remarks
                          where t1.Active == true && t1.ID == id
                          select new VMRemarks
                          {
                              Name = t1.Name,
                              IsClose = t1.IsClose,
                              ActionId = (int)ActionEnum.Edit

                          }).FirstOrDefault();
            return remark;

        }
        public async Task<int> RemarksAdd(VMRemarks _Remarks)
        {
            var result = -1;

            Prod_Remarks remarks = new Prod_Remarks
            {
                Name = _Remarks.Name,
                IsClose = _Remarks.IsClose,
                User = "UserOne",
            };
            _db.Prod_Remarks.Add(remarks);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = remarks.ID;
            }
            return result;
        }
        public async Task<int> RemarksEdit(VMRemarks vMRemarks)
        {
            var result = -1;
            Prod_Remarks remarks = _db.Prod_Remarks.Find(vMRemarks.ID);
            remarks.Name = vMRemarks.Name;
            remarks.IsClose = vMRemarks.IsClose;

            if (await _db.SaveChangesAsync() > 0)
            {
                result = remarks.ID;
            }
            return result;
        }
        public async Task<int> RemarksDelete(int id)
        {
            var result = -1;
            Prod_Remarks remarks = _db.Prod_Remarks.Find(id);
            if (remarks != null)
            {
                remarks.Active = false;
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = remarks.ID;
                }
            }
            return result;
        }
        public async Task<int> Acknowledge(VMRemarks vMRemarks)
        {
            var result = -1;
            Prod_Remarks remarks = _db.Prod_Remarks.Find(vMRemarks.ID);
            remarks.IsClose = vMRemarks.IsClose;

            if (await _db.SaveChangesAsync() > 0)
            {
                result = remarks.ID;
            }

            //if (remarks != null)
            //{
            //    remarks.IsClose = false;
            //    if (await _db.SaveChangesAsync() > 0)
            //    {
            //        result = remarks.ID;
            //    }
            //}
            return result;
        }


        #endregion

        #region ProductionUnit
        public async Task<VMProductionUnit> GetProductionUnit()
        {
            VMProductionUnit vMProductionUnit = new VMProductionUnit();
            var productionUnit = await Task.Run(() => (from t1 in _db.Common_ProductionUnit
                                                join t2 in _db.HRMS_Unit on t1.HRMS_UnitFK equals t2.ID
                                                where t1.Active == true
                                                select new VMProductionUnit
                                                {
                                                    ID = t1.ID,
                                                    Name = t1.Name,
                                                    Code = t1.Code,
                                                    Remarks = t1.Remarks,
                                                    HRMS_UnitFK = t2.ID,
                                                    BusinessUnit = t2.Name

                                                }).ToList());
            vMProductionUnit.DataList = productionUnit;
            return vMProductionUnit;
        }
        public async Task<int> ProductionUnitAdd(VMProductionUnit mProductionUnit)
        {
            var result = -1;

            Common_ProductionUnit productionUnit = new Common_ProductionUnit();
            productionUnit.Name = mProductionUnit.Name;
            productionUnit.Code = CidServices.ProductionUnitCode(mProductionUnit.HRMS_UnitFK);
            productionUnit.Remarks = mProductionUnit.Remarks;
            productionUnit.HRMS_UnitFK = mProductionUnit.HRMS_UnitFK;
            productionUnit.User = "UserOne";

            _db.Common_ProductionUnit.Add(productionUnit);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = productionUnit.ID;
            }
            return result;
        }
        public async Task<int> ProductionUnitEdit(VMProductionUnit vMProduction)
        {
            var result = -1;
            Common_ProductionUnit productionUnit = new Common_ProductionUnit();
            productionUnit = _db.Common_ProductionUnit.Find(vMProduction.ID);
            productionUnit.Name = vMProduction.Name;
            productionUnit.Code = vMProduction.Code;
            productionUnit.Remarks = vMProduction.Remarks;
            productionUnit.HRMS_UnitFK = vMProduction.HRMS_UnitFK;
            if (await _db.SaveChangesAsync() > 0)
            {
                result = productionUnit.ID;
            }
            return result;
        }
        public async Task<int> ProductionUnitDelete(int id)
        {
            var result = -1;
            Common_ProductionUnit productionUnit = _db.Common_ProductionUnit.Find(id);
            if (productionUnit != null)
            {
                productionUnit.Active = false;

                if (await _db.SaveChangesAsync() > 0)
                {
                    result = productionUnit.ID;
                }
            }
            return result;
        }
        #endregion

        #region Production Floor
        public async Task<VMProductionFloor> GetProductionFloor()
        {
            VMProductionFloor vMProductionFloor = new VMProductionFloor();
            var productionFloor = await Task.Run(() => (from t1 in _db.Common_ProductionFloor
                                                       join t2 in _db.Common_ProductionUnit on t1.Common_ProductionUnitFK equals t2.ID
                                                       where t1.Active == true
                                                       select new VMProductionFloor
                                                       {
                                                           ID = t1.ID,
                                                           Name = t1.Name,
                                                           Code = t1.Code,
                                                           Remarks = t1.Remarks,
                                                          ProductionUnitFK= t2.ID,
                                                          ProductionUnit = t2.Name

                                                       }).ToList());
            vMProductionFloor.DataList = productionFloor;
            return vMProductionFloor;
        }
        public async Task<int> ProductionFloorAdd(VMProductionFloor vmProductionFloor)
        {
            var result = -1;

            Common_ProductionFloor productionFloor = new Common_ProductionFloor();
            productionFloor.Name = vmProductionFloor.Name;
            productionFloor.Code = vmProductionFloor.Code;
            productionFloor.Remarks = vmProductionFloor.Remarks;
            productionFloor.Common_ProductionUnitFK = vmProductionFloor.ProductionUnitFK;
            productionFloor.User = "UserOne";

            _db.Common_ProductionFloor.Add(productionFloor);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = productionFloor.ID;
            }
            return result;
        }
        public async Task<int> ProductionFloorEdit(VMProductionFloor vmProductionFloor)
        {
            var result = -1;
            Common_ProductionFloor productionFloor = new Common_ProductionFloor();
            productionFloor = _db.Common_ProductionFloor.Find(vmProductionFloor.ID);
            productionFloor.Name = vmProductionFloor.Name;
            productionFloor.Code = vmProductionFloor.Code;
            productionFloor.Remarks = vmProductionFloor.Remarks;
            productionFloor.Common_ProductionUnitFK = vmProductionFloor.ProductionUnitFK;
            if (await _db.SaveChangesAsync() > 0)
            {
                result = productionFloor.ID;
            }
            return result;
        }
        public async Task<int> ProductionFloorDelete(int id)
        {
            var result = -1;
            Common_ProductionFloor productionFloor = _db.Common_ProductionFloor.Find(id);
            if (productionFloor != null)
            {
                productionFloor.Active = false;

                if (await _db.SaveChangesAsync() > 0)
                {
                    result = productionFloor.ID;
                }
            }
            return result;
        }
        #endregion

        public DataTable GetHourlyLineProduction(int RefId)
        {
            DataTable ds = new DataTable();
            VMProductionReport vMDailyProduction = new VMProductionReport();

            var vLine = _db.Common_ProductionLine.Where(d => d.ProductionStepFK == (int)EnumProcess.Sewing ).OrderBy(a => a.ID).ToList();
            ds.Columns.Add("Hour");
            foreach (var v in vLine)
            {
                ds.Columns.Add(v.Code+"-"+v.Name);
            }
            ds.Columns.Add("Total");

            //Fined Line Time
            vMDailyProduction.FromDate = _db.Prod_Reference.Where(a => a.ID == RefId).FirstOrDefault().ReferenceDate;
            vMDailyProduction.ProductionDate = vMDailyProduction.FromDate.ToLongDateString();
            var allData = (from t1 in _db.Prod_ReferencePlan
                           join t4 in _db.Common_ProductionLine on t1.ProductionLineFK equals t4.ID
                           join t5 in _db.Merchandising_Style on t1.Merchandising_StyleFK equals t5.ID
                           join t6 in _db.Merchandising_BuyerOrder on t5.Merchandising_BuyerOrderFK equals t6.ID
                           join t7 in _db.Common_Buyer on t6.Common_BuyerFK equals t7.ID
                           join t8 in _db.Prod_Reference on t1.Prod_ReferenceFK equals t8.ID
                           where t1.Prod_ReferenceFK == RefId && t1.SectionId == 2
                           group t1.PlanQuantity
                           by new { t1, t5.StyleName, t6.BuyerPO, t7.Name } into all
                           select new
                           {
                               LineId = all.Key.t1.ProductionLineFK,
                               OrderNo = all.Key.BuyerPO,
                               Style = all.Key.Name + "-" + all.Key.StyleName,
                               WorkHour = all.Key.t1.WorkingHour,
                               SMV = all.Key.t1.GSMORSMV,
                               TotalTarget = all.Key.t1.PlanQuantity
                           }).ToList();
            if (allData.Any())
            {
                var LineOrder = allData.GroupBy(a => a.LineId).Select(x => new { t = x.ToList() });

                var allDoneData = (from t1 in _db.Prod_ReferencePlan
                                   join t2 in _db.Prod_ReferencePlanFollowup on t1.ID equals t2.Prod_ReferencePlanFK
                                   join t3 in _db.Prod_ReferencePlanFollowupDetails on t2.ID equals t3.Prod_ReferencePlanFollowupFK
                                   where t1.Prod_ReferenceFK == RefId && t1.SectionId == 2 && t2.Prod_ReferencePlanFK == 2
                                   select new
                                   {
                                       LineId = t1.ProductionLineFK,
                                       EntryDate = t3.Time,
                                       TotalDoneQty = t3.Quantity
                                   }).ToList();

                var TotalHour = allData.GroupBy(a => a.LineId).Select(x => new { t = x.Sum(p => p.WorkHour) }).Max(a => a.t);
                List<VMProductionReport> lstList = new List<VMProductionReport>();
                if (LineOrder.Any())
                {
                    foreach (var v in LineOrder)
                    {
                        if (v.t.Any())
                        {
                            foreach (var v1 in v.t)
                            {
                                VMProductionReport m = new VMProductionReport();
                                m.LineName = vLine.FirstOrDefault(a => a.ID == v1.LineId).Name;
                                m.BuyerPO = v1.OrderNo;
                                m.StyleName = v1.Style;
                                m.SMV = v1.SMV;
                                m.TargetQty = v1.TotalTarget;
                                lstList.Add(m);
                            }
                        }
                    }
                }
                vMDailyProduction.DataList = lstList;

                if (TotalHour > 0)
                {
                    int count = 0;
                    for (int i = 1; i <= TotalHour + 2; i++)
                    {
                        int TotalOutput = 0;
                        int LineOutput = 0;
                        DateTime fDate = vMDailyProduction.FromDate + new TimeSpan(8 + i, 00, 00);
                        DateTime tDate = vMDailyProduction.FromDate + new TimeSpan(8 + i, 59, 00);
                        ds.Rows.Add(fDate.TimeOfDay);
                        int c = 0;
                        foreach (var v1 in vLine)
                        {
                            c++;
                            LineOutput = 0;
                            var getProduction = allDoneData.Where(a => a.EntryDate.Hour <= fDate.Hour && a.EntryDate.Hour >= tDate.Hour && a.LineId == v1.ID);
                            LineOutput = getProduction.Any() == true ? (int)getProduction.Sum(a => a.TotalDoneQty) : 0;
                            TotalOutput += LineOutput;
                            ds.Rows[count][c] = LineOutput;
                        }
                        ++c;
                        ds.Rows[count][c] = TotalOutput;
                        ++count;
                    }
                }
            }
            else
            {
                vMDailyProduction.DataList = new List<VMProductionReport>();
            }
            return ds;
        }
        #region DailyTotalProductionSectionWise
        public List<VMProductionReport> GetDailyTotalProductionSectionWise(int RefId,int SectionId)
        {
            var VData = (from t in _db.Prod_ReferencePlan 
                         join pd in _db.Prod_ReferencePlanFollowup on t.ID equals pd.Prod_ReferencePlanFK
                         join p in _db.Merchandising_StyleShipmentSchedule on pd.Merchandising_StyleShipmentScheduleFK equals p.ID
                         join r in _db.Merchandising_Style on t.Merchandising_StyleFK equals r.ID
                         join s in _db.Merchandising_BuyerOrder on r.Merchandising_BuyerOrderFK equals s.ID
                         where t.Prod_ReferenceFK == RefId && t.Active == true && pd.Active == true && t.SectionId == SectionId
                         group pd.Quantity by new { s.CID, r.StyleName, t.Merchandising_StyleFK,t.PlanQuantity } into all
                         select new VMProductionReport
                         {
                             StyleID = (int)all.Key.Merchandising_StyleFK,
                             BuyerPO = all.Key.CID,
                             StyleName = all.Key.StyleName,
                             TargetQty=all.Key.PlanQuantity,
                             Quantity = (int)all.Sum()
                         }).ToList();

            return VData;
        }

        public List<VMProductionReport> GetDailyProductionBySizeWise(int RefId, int SectionId)
        {
            var VData = (from t in _db.Prod_ReferencePlan
                         join pd in _db.Prod_ReferencePlanFollowup on t.ID equals pd.Prod_ReferencePlanFK
                         join ra in _db.Merchandising_StyleShipmentRatio on pd.Merchandising_StyleShipmentRatioFK equals ra.ID
                         join me in _db.Merchandising_StyleMeasurement on ra.Merchandising_StyleMeasurementFK equals me.ID
                         join si in _db.Common_Size on me.Common_SizeFK equals si.ID
                         join co in _db.Common_Color on me.Common_ColorFK equals co.ID
                         where t.Prod_ReferenceFK == RefId && t.Active == true && pd.Active == true && t.SectionId == SectionId
                         group pd.Quantity by new { co,si.Name,si.ID,t.Merchandising_StyleFK } into all
                         select new VMProductionReport
                         {
                             StyleID = (int)all.Key.Merchandising_StyleFK,
                             ColorName = all.Key.co.Name,
                             Size = all.Key.Name,
                             Quantity = (int)all.Sum()
                         }).ToList();

            return VData;
        }
        #endregion
        #region Section Wise Daily Production 
        public List<VMProductionReport> GetDailyProduction(VMProductionReport model)
        {
            var getRef = _db.Prod_Reference.Where(a=>a.ReferenceDate==model.FromDate && a.Active==true).ToList();
            var SectionId = _db.Prod_ReferencePlan.Where(c => c.SectionId == model.SectionId && c.Active==true).FirstOrDefault().SectionId;
            if (getRef.Any())
            {
                model.ProdReferenceId = getRef.FirstOrDefault().ID;

                var VData1 = (from t0 in _db.Prod_ReferencePlan
                              join t1 in _db.Merchandising_Style on t0.Merchandising_StyleFK equals t1.ID
                              join t2 in _db.Merchandising_StyleShipmentSchedule on t1.ID equals t2.Merchandising_StyleFK
                              join t3 in _db.Merchandising_StyleShipmentRatio on t2.ID equals t3.Merchandising_StyleShipmentScheduleFK
                              join t4 in _db.Merchandising_BuyerOrder on t1.Merchandising_BuyerOrderFK equals t4.ID
                              join t5 in _db.Merchandising_StyleMeasurement on t3.Merchandising_StyleMeasurementFK equals t5.ID
                              join t6 in _db.Common_Color on t5.Common_ColorFK equals t6.ID
                              join t7 in _db.Common_Size on t5.Common_SizeFK equals t7.ID
                              where t0.Prod_ReferenceFK == model.ProdReferenceId && t0.Active == true && t2.Active == true && t3.Active == true && t0.SectionId == SectionId
                              group t3.Quantity by new { t0.Merchandising_StyleFK, t1.StyleName, t4.BuyerPO, t5.Common_ColorFK, t5.Common_SizeFK, t6.Name, size = t7.Name ,t0.ProductionDate} into all
                              select new VMProductionReport
                              {
                                  StyleID = all.Key.Merchandising_StyleFK,
                                  BuyerPO = all.Key.BuyerPO,
                                  StyleName = all.Key.StyleName,
                                  ColorID = all.Key.Common_ColorFK,
                                  SizeID = all.Key.Common_SizeFK,
                                  ColorName = all.Key.Name,
                                  Size = all.Key.size,
                                  Quantity = all.Sum(),
                                  ReferenceDate=all.Key.ProductionDate,

                              }).ToList();

                //var VData1 = (from t0 in _db.Prod_ReferencePlan
                //              join t1 in _db.Merchandising_Style on t0.Merchandising_StyleFK equals t1.ID
                //              join t2 in _db.Merchandising_StyleShipmentSchedule on t1.ID equals t2.Merchandising_StyleFK
                //              join t3 in _db.Merchandising_StyleShipmentRatio on t2.ID equals t3.Merchandising_StyleShipmentScheduleFK
                //              join t4 in _db.Merchandising_BuyerOrder on t1.Merchandising_BuyerOrderFK equals t4.ID
                //              join t5 in _db.Merchandising_StyleMeasurement on t3.Merchandising_StyleMeasurementFK equals t5.ID
                //              join t6 in _db.Common_Color on t5.Common_ColorFK equals t6.ID
                //              join t7 in _db.Common_Size on t5.Common_SizeFK equals t7.ID
                //              join t8 in _db.Prod_ReferencePlanFollowup on t0.ID equals t8.Prod_ReferencePlanFK
                //              where t0.Prod_ReferenceFK == model.ProdReferenceId && t0.Active == true && t2.Active == true && t3.Active == true && t0.SectionId == SectionId
                //              group t8.Quantity by new { t0.Merchandising_StyleFK, t1.StyleName, t4.BuyerPO, t5.Common_ColorFK, t5.Common_SizeFK, t6.Name, size = t7.Name, t0,t3.Quantity } into all
                //              select new VMProductionReport
                //              {
                //                  StyleID = all.Key.Merchandising_StyleFK,
                //                  BuyerPO = all.Key.BuyerPO,
                //                  StyleName = all.Key.StyleName,
                //                  ColorID = all.Key.Common_ColorFK,
                //                  SizeID = all.Key.Common_SizeFK,
                //                  ColorName = all.Key.Name,
                //                  Size = all.Key.size,
                //                  ExtraRate = all.Key.t0.ExtraCutting,
                //                  GSM = all.Key.t0.GSMORSMV,
                //                  LayerWeight = all.Key.t0.LayerWeight,
                //                  MarkerPic = all.Key.t0.MarkerPiece,
                //                  MarkerLength = all.Key.t0.MarkerLength,
                //                  MarkerWidth = all.Key.t0.MarkerWidth,
                //                  Quantity= all.Key.Quantity,
                //                  AchiveQty = (int)all.Sum()

                //              }).ToList();

                

                #region SweingSection

                if (VData1.Any())
                {

                    #region CuttingSection
                    if (model.SectionId == (int)EnumProcess.Cutting)
                    {
                        foreach (var v in VData1)
                        {
                            v.AchiveQty = GetOrderSizeDoneQty(model.ProdReferenceId, v.StyleID, v.ColorID, v.SizeID, model.SectionId);
                            v.TotalCutting = GetPreviousDoneQtyBySizeWise(model.ProdReferenceId, v.StyleID, v.ColorID, v.SizeID, model.SectionId);

                            v.ExtraQty = (int)Math.Round(decimal.Divide(Decimal.Multiply(v.Quantity, 2), (decimal)100));
                            v.TotalTarget = v.ExtraQty + v.Quantity;

                            v.BalanceQty = v.TotalTarget - v.TotalCutting;
                        }
                    }
                    #endregion

                    else if (model.SectionId == (int)EnumProcess.Sewing)
                    {
                        foreach (var v in VData1)
                        {
                            v.AchiveQty = GetOrderSizeDoneQty(model.ProdReferenceId, v.StyleID, v.ColorID, v.SizeID, model.SectionId);
                            v.TotalSewing = GetPreviousDoneQtyBySizeWise(model.ProdReferenceId, v.StyleID, v.ColorID, v.SizeID, model.SectionId);
                            v.BalanceQty = v.Quantity - v.TotalSewing;

                        }
                    }
                    else if(model.SectionId == (int)EnumProcess.Ironning)
                    {
                        #region IroningSection
                        foreach (var v in VData1)
                        {
                            v.AchiveQty = GetOrderSizeDoneQty(model.ProdReferenceId, v.StyleID, v.ColorID, v.SizeID, model.SectionId);
                            v.TotalIroning = GetPreviousDoneQtyBySizeWise(model.ProdReferenceId, v.StyleID, v.ColorID, v.SizeID, model.SectionId);
                            v.BalanceQty = v.Quantity - v.TotalIroning;
                        }
                        #endregion
                    }
                    else if (model.SectionId == (int)EnumProcess.Packing)
                    {
                        #region PackingSection

                        foreach (var v in VData1)
                        {
                            v.AchiveQty = GetOrderSizeDoneQty(model.ProdReferenceId, v.StyleID, v.ColorID, v.SizeID, model.SectionId);
                            v.TotalPacking = GetPreviousDoneQtyBySizeWise(model.ProdReferenceId, v.StyleID, v.ColorID, v.SizeID, model.SectionId);
                            v.BalanceQty = v.Quantity - v.TotalPacking;
                        }
                        #endregion
                    }

                }
                #endregion
            return VData1;
            }
            else
            {
                List<VMProductionReport> emptyList = new List<VMProductionReport>();
                return emptyList;
            }
        }
        private int GetOrderSizeDoneQty(int RefId, int StyleID, int ColorID, int SizeID, int SectionId)
        {
            //Query For Total Achievement Qty
            int AcvQty = 0;
             var vdata = (from t3 in _db.Prod_ReferencePlan
                          join t0 in _db.Prod_ReferencePlanFollowup on t3.ID equals t0.Prod_ReferencePlanFK
                          join t2 in _db.Merchandising_StyleShipmentRatio on t0.Merchandising_StyleShipmentRatioFK equals t2.ID
                          join t4 in _db.Merchandising_StyleMeasurement on t2.Merchandising_StyleMeasurementFK equals t4.ID
                          where t3.SectionId == SectionId && t3.Merchandising_StyleFK == StyleID && t4.Common_ColorFK == ColorID && t4.Common_SizeFK == SizeID
                          && t3.Active == true && t0.Active == true && t3.Prod_ReferenceFK==RefId
                          group t0.Quantity by new { t4.Common_ColorFK, t4.Common_SizeFK } into all
                          select new
                          {
                              qty = all.Sum()
                          }).ToList();
           
            if (vdata.Any())
            {

                AcvQty= (int)vdata.Sum(a => a.qty);
            }
            return AcvQty;
        }

        private int GetPreviousDoneQtyBySizeWise(int RefID, int StyleID, int ColorID, int SizeID, int SectionId)
        {
            int TotalDoneQty = 0;

            var takedate = _db.Prod_Reference.Where(a => a.ID == RefID).FirstOrDefault();
            DateTime FDate = takedate.ReferenceDate;
            var vOrderDoneData = (from o in _db.Prod_Reference
                                  join p in _db.Prod_ReferencePlan on o.ID equals p.Prod_ReferenceFK
                                  join q in _db.Prod_ReferencePlanFollowup on p.ID equals q.Prod_ReferencePlanFK
                                  join r in _db.Merchandising_StyleShipmentRatio on q.Merchandising_StyleShipmentRatioFK equals r.ID
                                  join s in _db.Merchandising_StyleMeasurement on r.Merchandising_StyleMeasurementFK equals s.ID
                                  where o.ReferenceDate <= FDate && p.Merchandising_StyleFK == StyleID &&  p.SectionId == SectionId && s.Common_ColorFK == ColorID && s.Common_SizeFK == SizeID
                                  select new { q }).ToList();

            if (vOrderDoneData.Any())
            {
                TotalDoneQty = (int)vOrderDoneData.Sum(a => a.q.Quantity);
            }

            return TotalDoneQty;
        }
        #endregion
        #region Style Wise Production Reports
        public List<VMProductionReport> GetColorSizeRatioByStyleID( int StyleID, List<string> ColorIds, int? SubSetId)
        {
            string SetName = string.Empty;
            if (SubSetId != null)
            {
                SetName = _db.Merchandising_StyleSetPack.FirstOrDefault(a => a.ID == SubSetId.Value).SetPackName;
            }
            List<VMProductionReport> list = new List<VMProductionReport>();
            if (ColorIds.Any())
            {
                var vData = (from t0 in _db.Prod_ReferencePlan
                             join t1 in _db.Merchandising_Style on t0.Merchandising_StyleFK equals t1.ID
                             join t2 in _db.Merchandising_StyleShipmentSchedule on t1.ID equals t2.Merchandising_StyleFK
                             join t3 in _db.Merchandising_StyleShipmentRatio on t2.ID equals t3.Merchandising_StyleShipmentScheduleFK
                             join t4 in _db.Merchandising_BuyerOrder on t1.Merchandising_BuyerOrderFK equals t4.ID
                             join t5 in _db.Merchandising_StyleMeasurement on t3.Merchandising_StyleMeasurementFK equals t5.ID
                             join t6 in _db.Common_Color on t5.Common_ColorFK equals t6.ID
                             join t7 in _db.Common_Size on t5.Common_SizeFK equals t7.ID
                             where t0.Merchandising_StyleFK== StyleID && t5.Active==true
                             select new
                             {
                                 ID = t1.ID,
                                 SizeId = t5.Common_SizeFK,
                                 ColorId = t5.Common_ColorFK,
                                 Size = t7.Name,
                                 Color = t6.Name

                             }).ToList();


                //var vData = (from t1 in _db.Merchandising_StyleMeasurement
                //             where t1.Merchandising_StyleFK == StyleID && t1.Active == true
                //             join t2 in _db.Common_Size on t1.Common_SizeFK equals t2.ID
                //             join t3 in _db.Common_Color on t1.Common_ColorFK equals t3.ID
                //             where t1.Active == true
                //             select new { ID = t1.ID, SizeId = t1.Common_SizeFK, ColorId = t1.Common_ColorFK, Size = t2.Name, Color = t3.Name }).ToList();
                foreach (var v in ColorIds)
                {
                    int id = 0;
                    int.TryParse(v, out id);

                    var SizeRange = vData.Where(a => a.ColorId == id);
                    if (SizeRange.Any())
                    {
                        var vColor = SizeRange.GroupBy(a => a.ColorId, (key, group) => new { ColorId = key, Items = group.ToList() }).ToList();
                        if (vColor.Any())
                        {
                            foreach (var col in vColor)
                            {
                                VMProductionReport mColor = new VMProductionReport();
                                mColor.ColorID= col.ColorId;
                                mColor.ColorName = col.Items.FirstOrDefault().Color;
                                mColor.ID = StyleID;
                                mColor.Subset = SetName;
                                var vSize = SizeRange.GroupBy(a => a.SizeId, (key, group) => new { SizeId = key, Items = group.ToList() }).ToList();
                                if (vSize.Any())
                                {
                                    List<VMProductionReport> lstSize = new List<VMProductionReport>();
                                    foreach (var si in vSize)
                                    {
                                        VMProductionReport mSize = new VMProductionReport();
                                        if (SubSetId != null)
                                        {
                                            mSize.SetPackFK = SubSetId.Value;
                                        }
                                       
                                        mSize.SizeID = si.SizeId;
                                        mSize.Size = si.Items.FirstOrDefault().Size;

                                        lstSize.Add(mSize);
                                    }
                                    mColor.DataList = lstSize.OrderBy(a => a.SizeID).ToList();
                                }

                                list.Add(mColor);
                            }
                        }
                    }
                }
            }
            var vData1 = list.OrderBy(a => a.ColorID).ToList();
            return vData1;
        }

        public List<VMProductionReport> GetStyleWiseProduction(VMProductionReport model)
        {
            var styleId = _db.Merchandising_Style.Where(a => a.ID == model.StyleID && a.Active == true).ToList();

            if (styleId.Any())
            {
                model.StyleID = styleId.FirstOrDefault().ID;

                var data = (from t in _db.Prod_Reference
                            join t0 in _db.Prod_ReferencePlan on t.ID equals t0.Prod_ReferenceFK
                            join t1 in _db.Merchandising_Style on t0.Merchandising_StyleFK equals t1.ID
                            join t2 in _db.Merchandising_StyleShipmentSchedule on t1.ID equals t2.Merchandising_StyleFK
                            join t3 in _db.Merchandising_StyleShipmentRatio on t2.ID equals t3.Merchandising_StyleShipmentScheduleFK
                            join t4 in _db.Merchandising_BuyerOrder on t1.Merchandising_BuyerOrderFK equals t4.ID
                            join t5 in _db.Merchandising_StyleSetPack on t3.Merchandising_StyleSetPackFK equals t5.ID
                            join t6 in _db.Merchandising_StyleMeasurement on t3.Merchandising_StyleMeasurementFK equals t6.ID
                            join t7 in _db.Common_Color on t6.Common_ColorFK equals t7.ID
                            join t8 in _db.Common_Size on t6.Common_SizeFK equals t8.ID
                            where t0.Merchandising_StyleFK == model.StyleID && t1.Active==true && t0.Active == true && t2.Active == true && t3.Active == true
                            group t3.Quantity by new { t4.BuyerPO,t.ReferenceDate, t5.SetPackName, t6.Common_ColorFK, t6.Common_SizeFK,t7.Name, size = t8.Name } into all
                            select new VMProductionReport
                            {
                                BuyerPO = all.Key.BuyerPO,
                                ReferenceDate = all.Key.ReferenceDate,
                                Subset = all.Key.SetPackName,
                                ColorID = all.Key.Common_ColorFK,
                                SizeID = all.Key.Common_SizeFK,
                                ColorName = all.Key.Name,
                                Size = all.Key.size,
                               
                            }).ToList();


            return data;
            }
            else
            {
                List<VMProductionReport> emptyList = new List<VMProductionReport>();
                return emptyList;
            }

        } 

        #endregion

    }
}
public class DropList
{
    public DateTime ID { get; set; }

    public int Value { get; set; }

    public DateTime Name { get; set; }
}

public class SelectListItems
{
    public int Value { get; set; }
    public string Text { get; set; }
}
