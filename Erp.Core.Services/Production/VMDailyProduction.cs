﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Erp.Core.Services.Production
{
    public class VMDailyProduction : BaseVM
    {
        public DateTime ProductionDate { get; set; }

        public string MultiFK { get; set; }

        public int Prod_ReferenceFK { get; set; }

        public int Prod_ReferencePlanFK { get; set; }

        public int Prod_ReferencePlanFollowupFK { get; set; }

        public int Merchandising_StyleFK { get; set; }

        public int StyleShipmentScheduleFK { get; set; }

        public int Plan_SMVMasterLayoutFK { get; set; }

        public int Merchandising_StyleSetPackFK { get; set; }

        public int StyleShipmentRatioFK { get; set; }

        public int Common_ColorFK { get; set; }

        public int Common_SizeFK { get; set; }

        public int Plan_ProductionLineFk { get; set; }

        public string OrderNo { get; set; }

        public string Style { get; set; }

        public string Destination { get; set; }

        public string Port { get; set; }

        public string Color { get; set; }

        public string Size { get; set; }

        public int OrderQty { get; set; }

        public int TargetQty { get; set; }

        public int DoneQty { get; set; }

        public int LineInputQty { get; set; }

        public int RemainQty { get; set; }

        public int Prod_LineChiefFK { get; set; }

        public int Prod_SuperVisorFK { get; set; }

        [Display(Name = "Line Chief")]
        public string LineChiefSupervisor { get; set; }

        [Display(Name = "Supervisor")]
        public string SuperVisor { get; set; }

        public string LineName { get; set; }

        public string SubSetName { get; set; }

        [Display(Name = "CuttingPlanQuantity")]
        public int CuttingPlanQuantity { get; set; }

        [DisplayName("CuttingDoneQuantity")]
        public int CuttingDoneQuantity { get; set; }

        [DisplayName("Extra Cutting")]
        public int CuttingExtra { get; set; }

        [DisplayName("GSM")]
        public decimal GSM { get; set; }

        [DisplayName("SewingPlanQuantity")]
        public int SewingPlanQuantity { get; set; }

        [DisplayName("SewingDoneQuantity")]
        public int SewingDoneQuantity { get; set; }

        [DisplayName("Machine Running")]
        public int MachineRunning { get; set; }

        [DisplayName("Working Hour")]
        public int WorkingHour { get; set; }

        public decimal SMV { get; set; }

        public string CuttingNumber { get; set; }

        public string LineTable { get; set; }

        [DisplayName("IronPlanQuantity")]
        public int IronPlanQuantity { get; set; }

        [DisplayName("IronDoneQuantity")]
        public int IronDoneQuantity { get; set; }

        [DisplayName("PackingPlanQuantity")]
        public int PackingPlanQuantity { get; set; }

        [DisplayName("PackingDoneQuantity")]
        public int PackingDoneQuantity { get; set; }

        public string FollowupTime { get; set; }

        public string FollowupDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}")]
        public DateTime EntryDate { get; set; }

        public string CreateBy { get; set; }

        public decimal CuttingDoneQuantityWeight { get; set; }

        public int Layer { get; set; }

        public decimal PerLayerWeight { get; set; }

        public int MarkerPiece { get; set; }

        public decimal MarkerLength { get; set; }

        public decimal MarkerWidth { get; set; }

        [Display(Name = "Present Opt")]
        public int PresentOpt { get; set; }

        [Display(Name = "No of helper")]
        public int NoOfHelper { get; set; }

        public int ExtraQty { get; set; }

        [Display(Name = "Hour Terget")]
        public int PerHourTarget { get; set; }

        public string Remarks { get; set; }

        public int InputMan { get; set; }

        public int SizeMan { get; set; }

        public int LineMan { get; set; }

        public int SectionID { get; set; }
        public int Common_FinishItemFK { get; set; }

        public string FinishItemName { get; set; }


        public List<VMLinePlanHourlyChart> LinePlanList { get; set; }

        public List<VMDailyProduction> DataList { get; set; }
    }
}
