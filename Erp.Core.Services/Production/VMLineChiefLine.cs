﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Erp.Core.Services.Production
{
    public class VMLineChiefLine: BaseVM
    {
        [Display(Name = "Line Chief")]
        public string LineChief { get; set; }
        [Display(Name = "Line SuperVisor")]
        public string LineSuperVisor { get; set; }
        [Display(Name = "Production Line")]
        public string ProductionLine { get; set; }
        public int Priority { get; set; }
       
        public int Common_ProductionLineFk { get; set; }

        public int LineManTypeFK { get; set; }
        public int LineChiefOrSVFK { get; set; }
       
        public bool IsLineChief { get; set; }
        public int Common_LineChiefFk { get; set; }
        public int Common_LineSuperVisorFk { get; set; }
        public List<VMLineChiefLine> DataList { get; set; }
    }
}
