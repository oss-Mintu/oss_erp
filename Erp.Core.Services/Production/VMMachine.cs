﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Erp.Core.Services.Production
{
   public class VMMachine:BaseVM
    {
        [DefaultValue(0)]
        public int? FirstCreatedBy { get; set; }
        [DefaultValue(0)]
        public int? LastEditeddBy { get; set; }

        [DisplayName("Machine")]
        public string Name { get; set; }

        [MaxLength(100, ErrorMessage = "Upto 100 Chracter")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        [DisplayName("Machine Type")]
        public int Prod_MachineTypeFK { get; set; }

        public List<VMMachine> DataList { get; set; }

    }
}
