﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Erp.Core.Services.Production
{
    public class VMAutoPlan : BaseVM
    {
        [Display(Name = "Cutting Date Before Delivery Date")]
        public int CuttingBFShip { get; set; }
        [Display(Name = "Fabric Inhouse Date Before Cutting Date")]
        public int FabricINBFCutting { get; set; }
        [Display(Name = "Accessories Inhouse Date Before Cutting Date")]
        public int AccessoriesINBFCutting { get; set; }
        [Display(Name = "PPSample Date Before Cutting Date")]
        public int PPSampleBFCutting { get; set; }
        [Display(Name = "Sewing Date After Cutting Date")]
        public int SewingAFCutting { get; set; }
        public int ReservedDay { get; set; }
        public decimal ExtraRate { get; set; }
        public string Remarks { get; set; }
    }
}
