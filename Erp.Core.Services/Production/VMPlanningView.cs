﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Erp.Core.Services.Production
{
    public class VMPlanningView : BaseVM
    {
        public VMOrderPlanning vmOrderPlanning { get; set; }

        public VMLinePlanning vmLinePlanning { get; set; }
    }
}
