﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Erp.Core.Services.Production
{
    public class VMShipmentProductionPlan : BaseVM
    {
        public int SMVLayoutFK { get; set; }

        public int Merchandising_StyleFK { get; set; }

        [DisplayName("Total Operator")]
        public int NoOfOperator { get; set; }

        [DisplayName("Total Worker")]
        public int TotalWorker { get; set; }

        [DisplayName("Working Hour")]
        public int WorkingHour { get; set; }

        [DisplayName("Hour Target")]
        public decimal PerHourTarget { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true), DisplayName("Sewing Date")]
        public DateTime SewingDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true), DisplayName("Delivery Date")]
        public DateTime DeliveryDate { get; set; }

        public int WorkingDay { get; set; }

        public decimal PlanQty { get; set; }

        public List<string> LineIDs { get; set; }

        public string Style { get; set; }

        [DisplayName("Production Unit")]
        public int ProductionUnitID { get; set; }
        [DisplayName("Production Floor")]
        public int ProductionFloorID { get; set; }
        [DisplayName("From Date")]
        public DateTime FromDate { get; set; }
        [DisplayName("To Date")]
        public DateTime ToDate { get; set; }

        public List<VMLinePlanning> ListLinePlan { get; set; }

        public List<VMLinePlanning> PlanList { get; set; }

        public List<VMLineSchedule> LinePlanChart { get; set; }

        public MultiSelectList LineList { get; set; }
    }
}
