﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Erp.Core.Services.Production
{
    public class VMRemarks:BaseVM
    {
        public string Name { get; set; }
        public bool IsClose { get; set; }

        public List<VMRemarks> DataList { get; set; }
    }
}
