﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Erp.Core.Services.Merchandising;

namespace Erp.Core.Services.Production
{
    public class VMLinePlanning : BaseVM
    {
        public int ProdOrderPlanningFK { get; set; }

        public int Plan_SMVLayoutFK { get; set; }

        public string LineName { get; set; }

        public int Merchandising_StyleFK { get; set; }

        [DisplayName("Line"), Required(ErrorMessage = "Production line is required")]
        public int ProductionLineFK { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Start Date")]
        public DateTime StartDate { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Finish Date")]
        public DateTime FinishDate { get; set; }

        [DisplayName("Per Hour Capacity ")]
        public decimal Capacity { get; set; }

        [DisplayName("Machine Qty")]
        public int MachineQuantity { get; set; }

        [DisplayName("Working Hour")]
        public decimal WorkingHour { get; set; }

        [DisplayName("Target Days")]
        public decimal TargetDays { get; set; }

        [DisplayName("Daily Target")]
        public decimal DayOutputQty { get; set; }

        [DisplayName("Total Qty")]
        public decimal TargetOutputQty { get; set; }

        [DisplayName("Remain Qty")]
        public decimal RemainQty { get; set; }

        public int OccupiedHour { get; set; }

        [StringLength(100, ErrorMessage = "Upto 100 Chracter")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }

        public bool IsClosed { get; set; }

        public bool IsPaused { get; set; }

        public string Status { get; set; }

        public List<VMLinePlanning> DataList { get; set; }
    }
}
