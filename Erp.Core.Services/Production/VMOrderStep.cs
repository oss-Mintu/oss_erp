﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Erp.Core.Services.Production
{
    public class VMOrderStep : BaseVM
    {
        public int Merchandising_StyleFK { get; set; }

        public int Plan_OrderActionFK { get; set; }

        public string ViewName { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Plan Start Date")]
        public DateTime PlanStartDate { get; set; }

        public VMOrderProcess vmOrderProcess { get; set; }

        public List<VMOrderProcess> StepList { get; set; }

        public List<VMOrderProcess> SaveDataList { get; set; }

        public List<VMOrderStepReport> DataList { get; set; }
    }
}
