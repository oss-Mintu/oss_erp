﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Erp.Core.Services.Production
{
    public class VMQCType : BaseVM
    {
        public string Name { get; set; }

        public bool IsDeprecate { get; set; }

        public string Remarks { get; set; }

        public string Status { get; set; }

        public List<VMQCType> DataList { get; set; }
    }
}
