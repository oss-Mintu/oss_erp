﻿using Erp.Core.Services.Merchandising;
using System;
using System.Collections.Generic;
using System.Text;

namespace Erp.Core.Services.Production
{
    public class VMOrderQC : BaseVM
    {
        public int Prod_QCTypeFK { get; set; }

        public int Merchandising_StyleFK { get; set; }

        public string QCType { get; set; }

        public string StyleName { get; set; }

        public int Priority { get; set; }

        public string Remarks { get; set; }

        public VMStyle VMStyle { get; set; }

        public List<VMOrderQC> DataList { get; set; }
    }
}
