﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Erp.Core.Services.Production
{
    public class VMLineSchedule
    {
        public int LineID { get; set; }

        public string LineName { get; set; }

        public string LineColor { get; set; }

        public string HeadDate { get; set; }

        public DateTime FromDate { get; set; }

        public DateTime ToDate { get; set; }

        public string DayColor { get; set; }

        public string ViewName { get; set; }

        public List<VMLineSchedule> ListLinePlan { get; set; }
    }
}
