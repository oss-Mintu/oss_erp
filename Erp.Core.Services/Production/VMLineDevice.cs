﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Erp.Core.Services.Production
{
    public class VMLineDevice : BaseVM
    {
        public string Name { get; set; }
        public string DeviceID { get; set; }
        public string ProductionLine { get; set; }
        public bool IsClossed { get; set; }
        public string Remarks { get; set; }
        public int Common_ProductionLineFK { get; set; }
        public List<VMLineDevice> DataList { get; set; }
    }
}
