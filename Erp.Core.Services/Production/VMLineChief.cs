﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Erp.Core.Services.Production
{
    public  class VMLineChief:BaseVM
    {
        public string Name { get; set; }
        public bool IsReference { get; set; }
        public List<VMLineChief> DataList { get; set; }

    }
}
