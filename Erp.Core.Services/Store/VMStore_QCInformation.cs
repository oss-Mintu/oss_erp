﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Erp.Core.Services.Store
{
    public class VMStore_QCInformation:BaseVM
    {
        public string CID { get; set; }
        public string DeviceId { get; set; }
        public string Remark { get; set; }
        public int Merchandising_StyleIdFK { get; set; }
        //public string Style { get; set; }

        public int Common_ColorFK { get; set; }
    
        //public string Color { get; set; }

        public int Common_SizeFK { get; set; }
       
        //public string Size { get; set; }

        public int Common_ProductionLineFK { get; set; }
       
        //public string Line { get; set; }

        public int Merchandising_StyleSetPackFK { get; set; }
      
        //public string SetPack { get; set; }

        public decimal QCQuantity { get; set; }

        public int RejectQuantityOne { get; set; }
        public int RejectQuantityTwo { get; set; }
        public int RejectQuantityThree { get; set; }
        public int RejectQuantityFour { get; set; }
        public int RejectQuantityFive { get; set; }
        public int RejectQuantitySix { get; set; }
        public int RejectQuantityTotal { get; set; }
        public DateTime EntryDate { get; set; } = DateTime.Now;
        public string Message { get; set; }
        public bool IsSuccess { get; set; }

    }
}
