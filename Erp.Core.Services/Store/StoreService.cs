﻿using Erp.Core.Entity.Store;
using Erp.Core.Services.Home;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using PostgreSQLBulkInsertWebApp.Service;
using Microsoft.AspNetCore.Mvc.Rendering;

using Erp.Core.Entity.Common;
using Erp.Core.Entity.Procurement;
using Erp.Core.Services.HRMS;
using Erp.Core.Services.Integration;
using Erp.Core.Services.User;
using Microsoft.AspNetCore.Mvc;
using Erp.Core.Services.Inventory;
using Erp.Core.Services.Production;
using Microsoft.AspNetCore.Http;
using Erp.Core.Entity.Merchandising;

namespace Erp.Core.Services.Store
{
    public class StoreService : BaseService
    {
        private BulkInsertService _iconfiguration;
        private IntegrationService integrationservice;

        //private MessageService _messageService;
        public StoreService(IErpDbContext db, HttpContext httpContext, IConfiguration iconfiguration)
        {
            _db = db;
            CidServices = new CidServices(db);
            integrationservice = new IntegrationService(db, httpContext);
            _iconfiguration = new BulkInsertService(iconfiguration);
            //_messageService = new MessageService();
        }

        public StoreService(IErpDbContext db, HttpContext httpContext)
        {
            _db = db;
            CidServices = new CidServices(db);
            integrationservice = new IntegrationService(db, httpContext);
            // _iconfiguration = new BulkInsertService(iconfiguration);
            //_messageService = new MessageService();
        }

        // =================== Store In ====================
        public async Task<VmStoreBin> GetStoreBin()
        {
            var storeBin = new VmStoreBin();
            var aStoreBin = await Task.Run(() => (from store in _db.Store_Bin
                                                  join general in _db.Store_General on store.Store_GeneralFK equals general.ID
                                                  //join commonUnit in _db.Common_Unit on store.Common_UnitFK equals commonUnit.ID
                                                  where store.Active
                                                  select new VmStoreBin
                                                  {
                                                      ID = store.ID,
                                                      CID = store.CID,
                                                      Section = store.Section,
                                                      Row = store.Row,
                                                      Shelf = store.Shelf,
                                                      Rack = store.Rack,
                                                      TotalRow = store.TotalRow,
                                                      TotalShelf = store.TotalShelf,
                                                      Capacity = store.Capacity,
                                                      Remarks = store.Remarks,
                                                      Dimension = store.Dimension,
                                                      User = store.User,
                                                      IsActive = store.Active,

                                                      StoreGeneralFK = store.Store_GeneralFK,
                                                      StoreGeneralName = general.StoreName,
                                                      //CommonUnitFK = store.Common_UnitFK,
                                                      //CommonUnitName = commonUnit.Name
                                                  }).OrderByDescending(x => x.ID).AsEnumerable());
            storeBin.DataList = aStoreBin;
            return storeBin;
        }
        public async Task<int> SeveAll(List<VmStoreBin> storeBins)
        {
            var result = -1;
            foreach (var storeBin in storeBins)
            {
                var bin = new Store_Bin()
                {
                    //ID = CidServices.GetBinMaxId(), //storeBin.ID,
                    CID = storeBin.CID,
                    Section = storeBin.Section,
                    Row = storeBin.Row,
                    Shelf = storeBin.Shelf,
                    Rack = storeBin.Rack,
                    TotalRow = storeBin.TotalRow,
                    TotalShelf = storeBin.TotalShelf,
                    Capacity = storeBin.Capacity,
                    Remarks = storeBin.Remarks,
                    Dimension = storeBin.Dimension,
                    User = "UserOne",
                    Active = true,
                    Store_GeneralFK = storeBin.StoreGeneralFK,
                    //Common_UnitFK = storeBin.CommonUnitFK
                };
                _db.Store_Bin.Add(bin);
                result = await _db.SaveChangesAsync();
            }
            return result;
        }
        public async Task<int> StoreAdd(VmStoreGeneral vmStoreGeneral)
        {
            var result = -1;
            var store = new Store_General()
            {
                StoreName = vmStoreGeneral.StoreName,
                Location = vmStoreGeneral.Location,
                Description = vmStoreGeneral.Description,
                User_DepartmentFK = vmStoreGeneral.User_DepartmentFK,
                HRMS_BusinessUnitFK = vmStoreGeneral.HRMS_BusinessUnitFK,
                HRMS_UnitFK = vmStoreGeneral.HRMS_UnitFK,
                Common_StoreTypeFK = vmStoreGeneral.Common_StoreTypeFK,
                User = vmStoreGeneral.User
            };

            _db.Store_General.Add(store);

            if (await _db.SaveChangesAsync() > 0)
            {
                result = store.ID;
            }
            return result;
        }
        public async Task<int> StoreEdit(VmStoreGeneral vmStoreGeneral)
        {
            var result = -1;
            var store = _db.Store_General.Find(vmStoreGeneral.ID);
            store.StoreName = vmStoreGeneral.StoreName;
            store.Location = vmStoreGeneral.Location;
            store.Description = vmStoreGeneral.Description;

            //store.User_DepartmentFK = vmStoreGeneral.User_DepartmentFK;
            //store.HRMS_BusinessUnitFK = vmStoreGeneral.HRMS_BusinessUnitFK;
            //store.HRMS_UnitFK = vmStoreGeneral.HRMS_UnitFK;
            //store.Common_StoreTypeFK = vmStoreGeneral.Common_StoreTypeFK;

            if (await _db.SaveChangesAsync() > 0)
            {
                result = store.ID;
            }
            return result;
        }

        public async Task<int> SectionAdd(VmStoreSection vmStoreSection)
        {
            var result = -1;
            var section = new Store_Section()
            {
                SectionName = vmStoreSection.SectionName,
                Description = vmStoreSection.Description,
                Store_GeneralFK = (int)vmStoreSection.AStore_GeneralFK,
                User = "UserOne"
            };

            _db.Store_Section.Add(section);

            if (await _db.SaveChangesAsync() > 0)
            {
                result = section.ID;
            }
            return result;
        }
        public async Task<int> SectionEdit(VmStoreSection vmStoreSection)
        {
            var result = -1;
            var section = _db.Store_Section.Find(vmStoreSection.ID);
            section.SectionName = vmStoreSection.SectionName;
            section.Description = vmStoreSection.Description;
            section.Store_GeneralFK = (int)vmStoreSection.AStore_GeneralFK;
            if (await _db.SaveChangesAsync() > 0)
            {
                result = section.ID;
            }
            return result;
        }
        public async Task<int> SectionDelete(VmStoreSection vmStoreSection)
        {
            var result = -1;
            var store = _db.Store_Section.Find(vmStoreSection.ID);
            store.Active = false;
            if (await _db.SaveChangesAsync() > 0)
            {
                result = store.ID;
            }
            return result;
        }
        public async Task<VmStoreGeneral> GetStoreAll()
        {
            var vmStoreGeneral = new VmStoreGeneral();
            var stores = await Task.Run(() => (
                from general in _db.Store_General
                join user in _db.User_Department on general.User_DepartmentFK equals user.ID

                where general.Active
                select new VmStoreGeneral
                {
                    ID = general.ID,
                    StoreName = general.StoreName,
                    Location = general.Location,
                    Description = general.Description,
                    Department = user.Name,
                    User_DepartmentFK = user.ID
                }).OrderByDescending(x => x.ID).ToList());

            vmStoreGeneral.DataList = stores;
            vmStoreGeneral.BusinessUnitList = await integrationservice.BusinessUnitDropDownListAsync();
            vmStoreGeneral.UnitList = await integrationservice.UnitDropDownListAsync(); //new SelectList(HrmsUnitDropDownList(), "Value", "Text"); 
            vmStoreGeneral.DepartmentList = new SelectList(DepartmentDropDownList(), "Value", "Text");
            vmStoreGeneral.StoreTypeList = new SelectList(StoreTypeDropDownList(), "Value", "Text");
            return vmStoreGeneral;
        }
        public async Task<VmStoreIn> GetStoreIn(int id)
        {

            var dataListsMaster = (from s in _db.Store_StockInMaster
                                   where s.Active
                                   select new VmStoreInMaster
                                   {
                                       ID = s.ID,
                                       CID = s.CID,
                                       ChallanNo = s.ChallanNo,
                                       ChallanDate = s.ChallanDate,
                                       ReceivedDate = s.ReceivedDate,
                                       Description = s.Description,
                                       StockType = s.StockType,
                                       StoreInType = s.StoreInType,
                                       User = s.User
                                   }).ToList();


            var vmmodel = new VmStoreIn
            {
                DataLists = new List<VmStoreIn>(),
                DataListsMaster = new List<VmStoreInMaster>()
            };
            var aStoreBin = await Task.Run(() => (
                from s in _db.Store_StockIn
                join u in _db.Common_Unit on s.CommonUnitFK equals u.ID
                join ri in _db.Common_RawItem on s.ItemIDFK equals ri.ID
                where s.Active
                select new VmStoreIn()
                {
                    ID = s.ID,
                    CID = s.CID,
                    ChallanNo = s.ChallanNo,
                    ChallanDate = s.ChallanDate,
                    ReceivedDate = s.ReceivedDate,
                    WarrentyDate = s.WarrentyDate,
                    Name = ri.Name, //s.Name,
                    Description = s.Description,
                    StockType = s.StockType,
                    Remarks = s.Remarks,

                    RemainingQty = s.RemainingQty,
                    DamagedQty = s.DamagedQty,
                    ReceivedQty = s.ReceivedQty,
                    UnitName = u.Name,

                    IPOFK = s.IPOFK,
                    ItemIDFK = s.ItemIDFK,
                    StoreBinFK = s.StoreBinFK,
                    CommonUnitFK = s.CommonUnitFK,
                    CommonSupplierFK = s.CommonSupplierFK,
                    CommonCategoryFK = s.CommonCategoryFK,
                    CommonSubCategoryFK = s.CommonSubCategoryFK,
                    //Merchandising_StyleFK = p.ID,

                    //Common_RawItemFK = p.Common_RawItemFK,
                    Common_ColorFK = s.Common_ColorFK, // == 0 ? default : s.Common_ColorFK,
                    //Common_SizeFK = p.ID,
                    //UnitPrice = p.PurchasingPrice,
                    //TotalPrice = p.PurchasingPrice * (remainingQty == 0 ? p.PurchaseQuantity : remainingQty),

                    //Merchandising_StyleSlaveFK = p.ID,
                    //Procurement_PurchaseOrderSlaveFK = ss.ID,
                    IsLocked = true,

                    FixAsset = s.FixAsset,
                    BinDefine = s.BinDefine,
                    IsActive = s.Active,

                }).OrderByDescending(x => x.ID).ToListAsync());
            if (aStoreBin.Any())
            {
                aStoreBin.ForEach(a =>
                {
                    // model = _db.Store_StockInMaster.First(x => x.ID == a.StoreInMasterFK);
                    var color = _db.Common_Color.Where(x => x.ID == a.Common_ColorFK);
                    if (color.Any())
                    {
                        a.Name += " (Color:" + color.FirstOrDefault().Name + " Size:" + a.Common_Size + " mm)";
                    }
                });
                vmmodel.DataLists = aStoreBin;
                vmmodel.DataListsMaster = dataListsMaster;
            }

            vmmodel.SupplierList = await SupplierDropDownListAsync();
            vmmodel.SupplierPoList = await SupplierPoDropDownListAsync();
            vmmodel.UnitList = await UnitDropDownListAsync();
            vmmodel.PoList = await PoDropDownListAsync();
            vmmodel.ICList = await ICAllDropDownList(0);
            vmmodel.StoreList = await StoreDropDownListAsync();
            vmmodel.StoreInTypeList = await StoreInTypeDropDownList();
            return vmmodel;
        }
        public async Task<VmStoreIn> GetStoreInCutting(int id)
        {
            //var data = (_db.Store_Requisition.FirstOrDefault(sr => sr.ID == 27).StoreInFK);

            var vmmodel = new VmStoreIn();
            var aStoreBin = await Task.Run(() => (
                from s in _db.Store_StockIn
                join u in _db.Common_Unit on s.CommonUnitFK equals u.ID
                join ri in _db.Common_RawItem on s.ItemIDFK equals ri.ID
                where s.Active && s.CommonStoreFK == id
                select new VmStoreIn()
                {
                    ID = s.ID,
                    CID = s.CID,
                    ChallanNo = s.ChallanNo,
                    ChallanDate = s.ChallanDate,
                    ReceivedDate = s.ReceivedDate,
                    WarrentyDate = s.WarrentyDate,
                    Name = ri.Name, //s.Name,
                    Description = s.Description,
                    StockType = s.StockType,
                    Remarks = s.Remarks,

                    RemainingQty = s.RemainingQty,
                    DamagedQty = s.DamagedQty,
                    ReceivedQty = s.ReceivedQty,
                    UnitName = u.Name,

                    IPOFK = s.IPOFK,
                    ItemIDFK = s.ItemIDFK,
                    StoreBinFK = s.StoreBinFK,
                    CommonUnitFK = s.CommonUnitFK,
                    CommonSupplierFK = s.CommonSupplierFK,
                    CommonCategoryFK = s.CommonCategoryFK,
                    CommonSubCategoryFK = s.CommonSubCategoryFK,
                    //Merchandising_StyleFK = p.ID,

                    //Common_RawItemFK = p.Common_RawItemFK,
                    Common_ColorFK = s.Common_ColorFK, // == 0 ? default(int) : s.Common_ColorFK,
                    //Common_SizeFK = p.ID,
                    //UnitPrice = p.PurchasingPrice,
                    //TotalPrice = p.PurchasingPrice * (remainingQty == 0 ? p.PurchaseQuantity : remainingQty),

                    //Merchandising_StyleSlaveFK = p.ID,
                    //Procurement_PurchaseOrderSlaveFK = ss.ID,
                    CommonStoreFK = s.CommonStoreFK, //(_db.Store_Requisition.FirstOrDefault(sr => sr.ID == 3).StoreInFK),
                    IsLocked = true,

                    FixAsset = s.FixAsset,
                    BinDefine = s.BinDefine,
                    IsActive = s.Active
                }).OrderByDescending(x => x.ID).ToListAsync());

            if (aStoreBin.Any())
            {
                aStoreBin.ForEach(a =>
                {
                    var color = _db.Common_Color.Where(x => x.ID == a.Common_ColorFK);
                    if (color.Any())
                    {
                        a.Name += " (Color:" + color.FirstOrDefault().Name + " Size:" + a.Common_Size + " mm)";
                    }
                });
            }

            //aStoreBin.Select(x =>{x.CommonStoreFK = 1; return x;}).ToList();
            vmmodel.DataLists = aStoreBin;
            vmmodel.CommonStoreFK = id;
            vmmodel.SupplierList = await SupplierDropDownListAsync();
            vmmodel.SupplierPoList = await SupplierPoDropDownListAsync();
            vmmodel.UnitList = await UnitDropDownListAsync();
            vmmodel.PoList = await PoDropDownListAsync();
            vmmodel.ICList = await InternalChallanDropDownList(id);
            return vmmodel;
        }
        public async Task<VmStoreIn> GetStoreInSewing(int id)
        {
            var vmmodel = new VmStoreIn();
            var aStoreBin = await Task.Run(() => (
                from s in _db.Store_StockIn
                join u in _db.Common_Unit on s.CommonUnitFK equals u.ID
                join ri in _db.Common_RawItem on s.ItemIDFK equals ri.ID
                where s.Active && s.CommonStoreFK == id
                select new VmStoreIn()
                {
                    ID = s.ID,
                    CID = s.CID,
                    ChallanNo = s.ChallanNo,
                    ChallanDate = s.ChallanDate,
                    ReceivedDate = s.ReceivedDate,
                    WarrentyDate = s.WarrentyDate,
                    Name = ri.Name, //s.Name,
                    Description = s.Description,
                    StockType = s.StockType,
                    Remarks = s.Remarks,

                    RemainingQty = s.RemainingQty,
                    DamagedQty = s.DamagedQty,
                    ReceivedQty = s.ReceivedQty,
                    UnitName = u.Name,

                    IPOFK = s.IPOFK,
                    ItemIDFK = s.ItemIDFK,
                    StoreBinFK = s.StoreBinFK,
                    CommonUnitFK = s.CommonUnitFK,
                    CommonSupplierFK = s.CommonSupplierFK,
                    CommonCategoryFK = s.CommonCategoryFK,
                    CommonSubCategoryFK = s.CommonSubCategoryFK,
                    //Merchandising_StyleFK = p.ID,

                    //Common_RawItemFK = p.Common_RawItemFK,
                    Common_ColorFK = s.Common_ColorFK, // == 0 ? default(int) : s.Common_ColorFK),
                    //Common_SizeFK = p.ID,
                    //UnitPrice = p.PurchasingPrice,
                    //TotalPrice = p.PurchasingPrice * (remainingQty == 0 ? p.PurchaseQuantity : remainingQty),

                    //Merchandising_StyleSlaveFK = p.ID,
                    //Procurement_PurchaseOrderSlaveFK = ss.ID,
                    CommonStoreFK = s.CommonStoreFK,
                    IsLocked = true,

                    FixAsset = s.FixAsset,
                    BinDefine = s.BinDefine,
                    IsActive = s.Active
                }).OrderByDescending(x => x.ID).ToListAsync());
            if (aStoreBin.Any())
            {
                aStoreBin.ForEach(a =>
                {
                    var color = _db.Common_Color.Where(x => x.ID == a.Common_ColorFK);
                    if (color.Any())
                    {
                        a.Name += " (Color:" + color.FirstOrDefault().Name + " Size:" + a.Common_Size + " mm)";
                    }
                });
            }
            vmmodel.DataLists = aStoreBin;
            vmmodel.CommonStoreFK = id;
            vmmodel.SupplierList = await SupplierDropDownListAsync();
            vmmodel.SupplierPoList = await SupplierPoDropDownListAsync();
            vmmodel.UnitList = await UnitDropDownListAsync();
            vmmodel.PoList = await PoDropDownListAsync();

            return vmmodel;
        }

        public async Task<VmStoreIn> GetItemByPoId(VmStoreIn vmStoreIn, int id)
        {
            ////====================================/////
            var vmmodel = new VmStoreIn();
            var remainingQty = 0.0;
            var orginType = 0;
            // for 1 new Pur 
            if (_db.Store_StockIn.Any(s => s.IPOFK == vmStoreIn.IPOFK))
            {
                remainingQty = _db.Store_StockIn.Where(s => s.IPOFK == vmStoreIn.IPOFK).Min(s => s.RemainingQty);
            }
            // for 2 internal 
            if (_db.Store_StockIn.Any(s => s.IPOFK == vmStoreIn.IPOFK && vmStoreIn.StoreInType == "2"))
            {
                remainingQty = _db.Store_StockIn.Where(s => s.IPOFK == vmStoreIn.IPOFK).Min(s => s.RemainingQty);
            }
            if (_db.Procurement_PurchaseOrder.Any(x => x.ID == vmStoreIn.IPOFK))
            {
                orginType = _db.Procurement_PurchaseOrder.FirstOrDefault(x => x.ID == vmStoreIn.IPOFK).OriginType;
            }

            var aStoreWithStyle = new List<VmStoreIn>();
            if (orginType != 0)
            {
                aStoreWithStyle = await Task.Run(() => (
                    from p in _db.Procurement_PurchaseOrderSlave
                    join po in _db.Procurement_PurchaseOrder on p.Procurement_PurchaseOrderFK equals po.ID //into ptemp
                    join i in _db.Common_RawItem on p.Common_RawItemFK equals i.ID //into rtemp
                    join ms in _db.Merchandising_StyleSlave on p.Merchandising_StyleSlaveFK equals ms.ID //into mstemp

                    //from il in rtemp.DefaultIfEmpty()
                    //from ss in mstemp.DefaultIfEmpty()
                    where p.Active && ms.Active && ms.IsLocked
                    && p.Procurement_PurchaseOrderFK == (vmStoreIn.IPOFK == 0 ? 1 : vmStoreIn.IPOFK)
                    && p.PurchaseQuantity >
                    (_db.Store_StockIn.Any(x => x.Active && x.Procurement_PurchaseOrderSlaveFK == p.ID)
                        ? _db.Store_StockIn.Where(x => x.Active && x.Procurement_PurchaseOrderSlaveFK == p.ID)
                            .Select(y => new { ReceivedQty = y.ReceivedQty }).Sum(c => c.ReceivedQty)
                        : 0)
                    select new VmStoreIn
                    {
                        ID = vmStoreIn.ID,
                        CID = vmStoreIn.CID,
                        ChallanNo = vmStoreIn.ChallanNo,
                        ChallanDate = vmStoreIn.ChallanDate,
                        ReceivedDate = vmStoreIn.ReceivedDate,
                        WarrentyDate = DateTime.Now,
                        Name = i.Name,
                        Description = "No Description",
                        StockType = "Production",
                        Remarks = vmStoreIn.Remarks,

                        PurchaseQuantity = p.PurchaseQuantity,
                        RemainingQty = p.PurchaseQuantity - (_db.Store_StockIn.Any(x => x.Active && x.Procurement_PurchaseOrderSlaveFK == p.ID)
                                           ? _db.Store_StockIn.Where(x => x.Active && x.Procurement_PurchaseOrderSlaveFK == p.ID)
                                               .Select(y => new { ReceivedQty = y.ReceivedQty }).Sum(c => c.ReceivedQty)
                                           : 0),
                        DamagedQty = 0,
                        ReceivedQty = p.PurchaseQuantity - (_db.Store_StockIn.Any(x => x.Active && x.Procurement_PurchaseOrderSlaveFK == p.ID)
                                          ? _db.Store_StockIn.Where(x => x.Active && x.Procurement_PurchaseOrderSlaveFK == p.ID)
                                              .Select(y => new { ReceivedQty = y.ReceivedQty }).Sum(c => c.ReceivedQty)
                                          : 0),

                        IPOFK = vmStoreIn.IPOFK,
                        ItemIDFK = p.Common_RawItemFK,
                        StoreBinFK = vmStoreIn.StoreBinFK,
                        CommonUnitFK = i.Common_UnitFK,
                        CommonStoreFK = vmStoreIn.CommonStoreFK,
                        CommonSupplierFK = 1,
                        CommonCategoryFK = 1,
                        CommonSubCategoryFK = 1,

                        Common_RawItemFK = p.Common_RawItemFK,
                        Common_ColorFK = ms.Common_ColorFK, //(int)(ms.Common_ColorFK == null ? default(int) : ms.Common_ColorFK),
                        Common_Size = ms.GSM,
                        UnitPrice = p.PurchasingPrice,
                        TotalPrice = p.PurchasingPrice * (remainingQty == 0 ? p.PurchaseQuantity : remainingQty),
                        Merchandising_StyleFK =
                            (int)(p.Merchandising_StyleID == null ? default(int) : p.Merchandising_StyleID),
                        Merchandising_StyleSlaveFK = (int)(p.Merchandising_StyleSlaveFK == null
                            ? default(int)
                            : p.Merchandising_StyleSlaveFK),
                        Procurement_PurchaseOrderSlaveFK = p.ID,
                        IsLocked = true,

                        FixAsset = false,
                        BinDefine = false,
                        IsActive = true

                    }).ToListAsync());

            }
            else
            {

                //var aStoreWithOutStyle = new List<VmStoreIn>();
                aStoreWithStyle = await Task.Run(() => (
                from p in _db.Procurement_PurchaseOrderSlave
                join po in _db.Procurement_PurchaseOrder on p.Procurement_PurchaseOrderFK equals po.ID //into ptemp
                join I in _db.Common_RawItem on p.Common_RawItemFK equals I.ID into rtemp
                //join ms in _db.Merchandising_StyleSlave on p.Merchandising_StyleSlaveFK equals ms.ID into mstemp

                from il in rtemp.DefaultIfEmpty()
                    //from ss in mstemp.DefaultIfEmpty()
                where p.Active //&& ss.Active && ss.IsLocked
                      && p.Procurement_PurchaseOrderFK == (vmStoreIn.IPOFK == 0 ? 1 : vmStoreIn.IPOFK)
                      && p.PurchaseQuantity >
                      (_db.Store_StockIn.Any(x => x.Active && x.Procurement_PurchaseOrderSlaveFK == p.ID)
                          ? _db.Store_StockIn.Where(x => x.Active && x.Procurement_PurchaseOrderSlaveFK == p.ID)
                              .Select(y => new { ReceivedQty = y.ReceivedQty }).Sum(c => c.ReceivedQty)
                          : 0)
                select new VmStoreIn
                {
                    ID = vmStoreIn.ID,
                    CID = vmStoreIn.CID,
                    ChallanNo = vmStoreIn.ChallanNo,
                    ChallanDate = vmStoreIn.ChallanDate,
                    ReceivedDate = vmStoreIn.ReceivedDate,
                    WarrentyDate = DateTime.Now,
                    Name = il.Name,
                    Description = "No Description",
                    StockType = "Production",
                    Remarks = vmStoreIn.Remarks,

                    PurchaseQuantity = p.PurchaseQuantity,
                    RemainingQty = p.PurchaseQuantity - (_db.Store_StockIn.Any(x => x.Active && x.Procurement_PurchaseOrderSlaveFK == p.ID)
                                       ? _db.Store_StockIn.Where(x => x.Active && x.Procurement_PurchaseOrderSlaveFK == p.ID)
                                           .Select(y => new { ReceivedQty = y.ReceivedQty }).Sum(c => c.ReceivedQty)
                                       : 0),
                    DamagedQty = 0,
                    ReceivedQty = p.PurchaseQuantity - (_db.Store_StockIn.Any(x => x.Active && x.Procurement_PurchaseOrderSlaveFK == p.ID)
                                      ? _db.Store_StockIn.Where(x => x.Active && x.Procurement_PurchaseOrderSlaveFK == p.ID)
                                          .Select(y => new { ReceivedQty = y.ReceivedQty }).Sum(c => c.ReceivedQty)
                                      : 0),

                    IPOFK = vmStoreIn.IPOFK,
                    ItemIDFK = p.Common_RawItemFK,
                    StoreBinFK = vmStoreIn.StoreBinFK,
                    CommonUnitFK = il.Common_UnitFK,
                    CommonStoreFK = vmStoreIn.CommonStoreFK,
                    CommonSupplierFK = 1,
                    CommonCategoryFK = 1,
                    CommonSubCategoryFK = 1,

                    Common_RawItemFK = p.Common_RawItemFK,
                    Common_ColorFK = 1, //(int)(ss.Common_ColorFK == null ? default(int) : ss.Common_ColorFK),
                    Common_Size = 1, //ss.GSM,
                    UnitPrice = p.PurchasingPrice,
                    TotalPrice = p.PurchasingPrice * (remainingQty == 0 ? p.PurchaseQuantity : remainingQty),
                    Merchandising_StyleFK =
                        (int)(p.Merchandising_StyleID == null ? default(int) : p.Merchandising_StyleID),
                    Merchandising_StyleSlaveFK = (int)(p.Merchandising_StyleSlaveFK == null
                        ? default(int)
                        : p.Merchandising_StyleSlaveFK),
                    Procurement_PurchaseOrderSlaveFK = p.ID,
                    IsLocked = true,

                    FixAsset = false,
                    BinDefine = false,
                    IsActive = true

                }).ToListAsync());
                //var list = TheObject1.ListOfLongs.Union(TheObject2.ListOfLongs).ToList();
                //var aStoreBin = aStoreWithStyle.Union(aStoreWithOutStyle).ToList();

            }

            if (aStoreWithStyle.Any())
            {
                aStoreWithStyle.ForEach(a =>
                {
                    var color = _db.Common_Color.Where(x => x.ID == a.Common_ColorFK);
                    if (color.Any())
                    {
                        a.Name += " (Color:" + color.FirstOrDefault().Name + " Size:" + a.Common_Size + " mm)";
                    }
                });
                vmmodel.DataLists = aStoreWithStyle.ToList();
            }

            vmmodel.SupplierList = await SupplierDropDownListAsync();
            vmmodel.SupplierPoList = await SupplierPoDropDownListAsync();
            vmmodel.UnitList = await UnitDropDownListAsync();
            vmmodel.PoList = await PoDropDownListAsync();
            vmmodel.StoreList = await StoreDropDownListAsync();
            vmmodel.CommonStoreFK = id == 0 ? 2 : id;
            return vmmodel;
        }

        public int GetStoreIdsByDeptId(string ndeptId)
        {
            int sId = 0;
            int deptId = 0;
            if (ndeptId != null)
            {
                deptId = Convert.ToInt32(ndeptId);
            }
            var storeId = _db.Store_General.FirstOrDefault(x => x.User_DepartmentFK == deptId);
            if (storeId != null)
            {
                sId = storeId.ID;
            }
            return sId;
        }

        public async Task<VmStoreIn> GetItemByInternalChallanId(VmStoreIn vmStoreIn, int id)
        {
            var vmmodel = new VmStoreIn
            {
                DataLists = new List<VmStoreIn>()
            };

            if (vmStoreIn.StoreInType == "1")
            {
                var aStoreBin = await Task.Run(() => (
                     from s in _db.Store_StockOut
                     where s.Active && s.RemainingQty > 0 && s.ChallanNo == (vmStoreIn.ChallanNo == "0" ? "1" : vmStoreIn.ChallanNo)
                     select new VmStoreIn
                     {
                         ID = vmStoreIn.ID,
                         CID = s.CID,
                         ChallanNo = s.ChallanNo,
                         ChallanDate = DateTime.Now, //s.RequiredDate,
                         ReceivedDate = DateTime.Now, //s.CreationDate,
                         WarrentyDate = DateTime.Now,
                         Name = s.Name,
                         Description = "No Description",
                         StockType = "Production",
                         Remarks = vmStoreIn.Remarks,

                         RemainingQty = s.RemainingQty, //s.RemainingQty,
                         DamagedQty = s.DamagedQty,
                         ReceivedQty = s.RemainingQty,

                         IPOFK = vmStoreIn.IPOFK,
                         ItemIDFK = s.ItemIDFK,
                         StoreBinFK = vmStoreIn.StoreBinFK,
                         CommonUnitFK = s.CommonUnitFK,
                         CommonSupplierFK = vmStoreIn.CommonSupplierFK,
                         CommonStoreFK = vmStoreIn.CommonStoreFK,
                         CommonCategoryFK = s.CommonCategoryFK,
                         CommonSubCategoryFK = s.CommonSubCategoryFK,

                         FixAsset = false,
                         BinDefine = false,
                         IsActive = true,

                         Merchandising_StyleFK = s.Merchandising_StyleFK,
                         Merchandising_StyleSlaveFK = s.Merchandising_StyleSlaveFK,
                         Common_ColorFK = s.Common_ColorFK,
                         Common_Size = s.Common_Size,
                         StoreInType = s.StoreInType
                     }).ToListAsync());
                vmmodel.DataLists = aStoreBin;
            }

            //IEnumerable<VmStoreIn> vmStoreIns = new List<VmStoreIn>();
            // Store In Type Internal == 2
            if (vmStoreIn.StoreInType == "2")
            {
                var remQty = 0.0;
                //var storeFk = 0;
                if (_db.Store_StockIn.Any(x => x.ChallanNo == vmStoreIn.ChallanNo))
                {
                    remQty = _db.Store_StockIn.Where(x => x.ChallanNo == vmStoreIn.ChallanNo).Min(s => s.RemainingQty);
                    //storeFk = (_db.Store_Requisition.FirstOrDefault(x => x.ID == s.IPOFK).StoreInFK);
                }

                var vmStoreIns = await Task.Run(() => (
                    from s in _db.Store_StockOut
                    where s.Active && s.ChallanNo == (vmStoreIn.ChallanNo == "0" ? "1" : vmStoreIn.ChallanNo)
                    select new VmStoreIn
                    {
                        ID = vmStoreIn.ID,
                        CID = s.CID,
                        ChallanNo = s.ChallanNo,
                        ChallanDate = DateTime.Now, //s.RequiredDate,
                        ReceivedDate = DateTime.Now, //s.CreationDate,
                        WarrentyDate = DateTime.Now,
                        Name = s.Name,
                        Description = "No Description",
                        StockType = "Production",
                        Remarks = vmStoreIn.Remarks,

                        RemainingQty = remQty == 0 ? s.ReceivedQty : remQty, //s.RemainingQty,
                        DamagedQty = s.DamagedQty,
                        ReceivedQty = remQty == 0 ? s.ReceivedQty : remQty,

                        IPOFK = s.IPOFK,
                        ItemIDFK = s.ItemIDFK,
                        StoreBinFK = vmStoreIn.StoreBinFK,
                        CommonUnitFK = s.CommonUnitFK,
                        CommonSupplierFK = vmStoreIn.CommonSupplierFK,
                        CommonStoreFK = (int)(_db.Store_Requisition.FirstOrDefault(x => x.ID == s.IPOFK).Store_GeneralInFK == 0 ? 2 : _db.Store_Requisition.FirstOrDefault(x => x.ID == s.IPOFK).Store_GeneralInFK),
                        CommonCategoryFK = s.CommonCategoryFK,
                        CommonSubCategoryFK = s.CommonSubCategoryFK,

                        FixAsset = false,
                        BinDefine = false,
                        IsActive = true,

                        Merchandising_StyleFK = s.Merchandising_StyleFK,
                        Merchandising_StyleSlaveFK = s.Merchandising_StyleSlaveFK,
                        Common_ColorFK = s.Common_ColorFK,
                        Common_Size = s.Common_Size,
                        StoreInType = s.StoreInType


                    }).ToListAsync());
                vmmodel.DataLists = vmStoreIns;
            }

            vmmodel.SupplierList = await SupplierDropDownListAsync();
            vmmodel.SupplierPoList = await SupplierPoDropDownListAsync();
            vmmodel.UnitList = await UnitDropDownListAsync();
            vmmodel.PoList = await PoDropDownListAsync();
            vmmodel.CommonStoreFK = id;
            vmmodel.StoreInType = vmStoreIn.StoreInType;
            return vmmodel;
        }

        public int GetItemByChallanId(int id)
        {
            const int data = 0;
            return id <= 0 ? data : _db.Procurement_PurchaseOrder.Find(id).Common_SupplierFK;
        }
        public int GetSupplierIdBySupplierChallanID(int id)
        {
            const int data = 0;
            return id <= 0 ? data : _db.Store_StockIn.Find(id).CommonSupplierFK;
        }


        public int GetBrandModelByItemId(int id)
        {
            //var data = _db.Store_StockRegister.Where(x => x.ID == id && x.Active).Select(r => new VmStoreAssetRegister
            //{
            //    ID = r.ID,
            //    UnitPrice = r.UnitPrice,
            //    ItemSLNO = r.ItemSLNO,
            //    CommonBrandFK = r.CommonBrandFK,
            //    CommonModelFK = r.CommonModelFK
            //}).ToList();
            const int data = 0;
            return id <= 0 ? data : _db.Common_FinishItem.Find(id).ID;
        }
        public string GetRegisterItemId(int id)
        {
            var Id = "";
            var vmodel = new VmStoreAssetRegister();
            var data = _db.Store_StockRegister.Where(x => x.ID == id && x.Active).Select(r => new VmStoreAssetRegister
            {
                ID = r.ID,
                UnitPrice = r.UnitPrice,
                ItemSLNO = r.ItemSLNO,
                CommonBrandFK = r.CommonBrandFK,
                CommonModelFK = r.CommonModelFK
            }).ToList();

            if (data.Any())
            {
                data.ForEach(r =>
                {
                    Id = r.CommonBrandFK + "~" + r.CommonModelFK + "~" + r.UnitPrice + "~" + r.ItemSLNO;
                });
            }
            //vmodel.DataLists = data;
            return Id;
        }
        public string GetStoreIdsByChallanId(int id)
        {
            var data = "0_0";
            return id <= 0 ? data : _db.Store_Requisition.Find(id).Store_GeneralOutFK + "_" + _db.Store_Requisition.Find(id).Store_GeneralInFK;
        }
        public async Task<SelectList> IsrDropDownListAsync(int id2 = 0)
        {
            var list = new List<object>();

            await Task.Run(() => (from pos in _db.Store_RequisitionSlave
                                  join po in _db.Store_Requisition.Where(x => x.Active && !string.IsNullOrEmpty(x.CID) && (x.Status == (int)PRStatusEnum.ProcurementApproved) && x.Store_GeneralOutFK == id2)
                        on pos.Store_RequisitionFK equals po.ID
                                  where pos.RequisitionQuantity >
                                      (_db.Store_StockOut.Any(x => x.Active && x.Store_RequisitionSlaveFK == pos.ID)
                                          ? _db.Store_StockOut.Where(x => x.Active && x.Store_RequisitionSlaveFK == pos.ID)
                                              .Select(y => new { ReceivedQty = y.ReceivedQty }).Sum(c => c.ReceivedQty)
                                          : 0)
                                  group po by new { po.ID, po.CID } //, po.TotalPOValue }
                    into g
                                  select new { g.Key.ID, g.Key.CID }) ///, g.Key.TotalPOValue })
                .Select(x => x).ForEachAsync(x => list.Add(new
                {
                    Value = x.ID,
                    Text = x.CID ///+ "(PO Value: " + x.TotalPOValue + ")"
                })));
            var data = new SelectList(list, "Value", "Text");
            return data;
            //var storeList = new List<object>();

            //var stockOut = _db.Store_StockOut.GroupBy(x => x.IPOFK).Select(xt => new Store_StockOut
            //{
            //    ID = xt.Max(i => i.ID),
            //    IPOFK = xt.Key,
            //    RemainingQty = xt.Min(r => r.RemainingQty)
            //}).ToList();

            //var filterData = _db.Store_Requisition.Where(x => !stockOut.Any(c => c.IPOFK == x.ID && c.Active && c.RemainingQty == 0)
            //&& x.Status == (int)SRStatusEnum.ProcurementApproved).AsEnumerable();

            //foreach (var store in filterData.ToList())
            //{
            //    storeList.Add(new { Text = store.CID, Value = store.ID, Date = store.CreationDate.ToShortDateString() });
            //}
            //var data = new SelectList(storeList, "Value", "Text");
            //return data;
        }
        public List<object> ChallanDropDownList()
        {
            var storeList = new List<object>();
            var dataList = _db.Store_StockOut.Where(x => x.Active && x.CID != null
            && !_db.Store_StockIn.Any(s => s.ChallanNo == x.CID)).GroupBy(x => x.ChallanNo).Select(g => new
            {
                ChallanNo = g.Key,
                ID = g.Max(i => i.ID),
                ChallanDate = g.Max(v => v.ChallanDate)
            }).ToList();

            foreach (var store in dataList)
            {
                storeList.Add(new { Text = store.ChallanNo, Value = store.ID, Date = store.ChallanDate.ToShortDateString() });
            }
            return storeList;
        }
        public async Task<SelectList> InternalChallanDropDownList(int id)
        {
            var storeList = new List<object>();
            var dataList = await Task.Run(() => _db.Store_StockOut.Where(x => x.Active && x.CID != null && x.CommonStoreFK == id
                                                         && !_db.Store_StockIn.Any(s =>
                                                             s.ChallanNo == x.ChallanNo &&
                                                             s.ReceivedQty >= x.ReceivedQty)).GroupBy(x => x.ChallanNo)
                .Select(g => new
                {
                    ChallanNo = g.Key,
                    ID = g.Max(i => i.ID),
                    ChallanDate = g.Max(v => v.ChallanDate)
                }).ToList());

            foreach (var store in dataList)
            {
                storeList.Add(new { Text = store.ChallanNo, Value = store.ID, Date = store.ChallanDate.ToShortDateString() });
            }
            var data = new SelectList(storeList, "Value", "Text");
            return data;
        }
        public async Task<SelectList> ICAllDropDownList(int id = 0)
        {
            var storeList = new List<object>();
            var dataList = _db.Store_StockOut.Where(x => x.Active
            && x.CID != null
            //&& !_db.Store_StockIn.Any(s => s.ChallanNo == x.ChallanNo && s.ReceivedQty >= x.ReceivedQty)
            ).GroupBy(x => x.ChallanNo)
                .Select(g => new
                {
                    ChallanNo = g.Key,
                    ID = g.Max(i => i.ID),
                    ChallanDate = g.Max(v => v.ChallanDate)
                }).ToList();

            foreach (var store in dataList)
            {
                storeList.Add(new { Text = store.ChallanNo, Value = store.ID, Date = store.ChallanDate.ToShortDateString() });
            }
            var data = new SelectList(storeList, "Value", "Text");
            return data;
        }

        public async Task<SelectList> SupplierChallanDropDownListAsync()
        {
            var storeList = new List<object>();
            var dataList = await Task.Run(() => _db.Store_StockInMaster
                 .Where(x => x.Active && x.CID != null)  //&& x.CommonSupplierFK != 0
                 .GroupBy(x => x.ChallanNo).Select(g => new
                 {
                     ChallanNo = g.Key,
                     ID = g.Max(i => i.ID),
                     ChallanDate = g.Max(v => v.ChallanDate)
                 }).ToList());

            foreach (var store in dataList)
            {
                storeList.Add(new
                { Text = store.ChallanNo, Value = store.ID, Date = store.ChallanDate.ToShortDateString() });
            }

            var data = new SelectList(storeList, "Value", "Text");
            return data;
        }


        public async Task<SelectList> StoreOutTypeDropDownList()
        {
            var storeInTypeList = new List<object>
            {
                new {Text = "Sale Return", Value = 1},
                new {Text = "Store Requisition", Value = 2}
                //new {Text = "Sale & Disposal", Value = 3},
            };
            var data = new SelectList(storeInTypeList, "Value", "Text");
            return data;
        }
        public async Task<SelectList> StoreInTypeDropDownList()
        {
            var storeInTypeList = new List<object>
            {
                new {Text = "New Purchase", Value = 1},
                new {Text = "Internal Challan", Value = 2}
            };

            var data = new SelectList(storeInTypeList, "Value", "Text");
            return data;
        }

        public List<object> StoreTypeDropDownList()
        {
            var storeInTypeList = new List<object>
            {
                new {Text = "Raw", Value = 1},
                new {Text = "WIP", Value = 2},
                new {Text = "Finish", Value = 3}
            };

            return storeInTypeList;
            //foreach (var store in _db.Store_General.ToList())
            //{
            //    storeList.Add(new { Text = store.StoreName, Value = store.ID });
            //}
            //return storeList;
        }

        public List<object> StoreToDropDownList(int id)
        {
            var storeList = new List<object>();
            foreach (var store in _db.Store_General.Where(x => x.Active && x.ID != id).ToList())
            {
                storeList.Add(new { Text = store.StoreName, Value = store.ID });
            }
            return storeList;
        }
        public List<object> SectionDropDownList()
        {
            var storeList = new List<object>();
            foreach (var store in _db.Store_Section.Where(x => x.Active).ToList())
            {
                storeList.Add(new { Text = store.SectionName, Value = store.ID });
            }
            return storeList;
        }
        public async Task<SelectList> UnitDropDownListAsync()
        {
            return new SelectList(await _db.Common_Unit.Where(x => x.Active).Select(x => new { Value = x.ID, Text = x.Name }).ToListAsync(), "Value", "Text");
        }

        public async Task<SelectList> SupplierDropDownListAsync()
        {
            return new SelectList(
                await _db.Common_Supplier.Where(x => x.Active).Select(x => new { Value = x.ID, Text = x.Name })
                    .ToListAsync(), "Value", "Text");
        }
        public async Task<SelectList> SupplierPoDropDownListAsync()
        {
            return new SelectList(new List<object>
                {
                    new {Text = "IPO-0001", Value = 1}, new {Text = "IPO-0002", Value = 2}
                }
            );
        }

        public async Task<SelectList> PoDropDownListAsync()
        {
            var list = new List<object>();

            await Task.Run(() => (from pos in _db.Procurement_PurchaseOrderSlave
                                  join po in _db.Procurement_PurchaseOrder
                                          .Where(x => x.Active && !string.IsNullOrEmpty(x.CID) &&
                                                      (x.Status == (int)PRStatusEnum.ProcurementApproved))
                                      on pos.Procurement_PurchaseOrderFK equals po.ID
                                  where
                                      pos.PurchaseQuantity >
                                      (_db.Store_StockIn.Any(x => x.Active && x.Procurement_PurchaseOrderSlaveFK == pos.ID)
                                          ? _db.Store_StockIn.Where(x => x.Active && x.Procurement_PurchaseOrderSlaveFK == pos.ID)
                                              .Select(y => new { ReceivedQty = y.ReceivedQty }).Sum(c => c.ReceivedQty)
                                          : 0)
                                  group po by new { po.ID, po.CID, po.TotalPOValue }
                    into g
                                  select new { g.Key.ID, g.Key.CID, g.Key.TotalPOValue })
                .Select(x => x).ForEachAsync(x => list.Add(new
                {
                    Value = x.ID,
                    Text = x.CID + "(PO Value: " + x.TotalPOValue + ")"
                })));
            var data = new SelectList(list, "Value", "Text");
            return data;
            //return new SelectList(list.ToList(), "Value", "Text");
        }

        public List<object> PoDropDownList(int poID = 0)
        {
            var eciList = new List<object>();

            //var store = (from pos in _db.Procurement_PurchaseOrderSlave
            //             join po in _db.Procurement_PurchaseOrder on pos.Procurement_PurchaseOrderFK equals po.ID
            //             join s in _db.Store_StockIn on pos.ID equals s.Procurement_PurchaseOrderSlaveFK into stem
            //             from st in stem.DefaultIfEmpty()

            //             where pos.Active && st.Active
            //             //        select new
            //             //        {
            //             //            pos.ID,
            //             //            po.CID,
            //             //            pos.Common_RawItemFK,
            //             //            pos.Procurement_PurchaseOrderFK,
            //             //            pos.PurchaseQuantity,
            //             //            st.ReceivedQty
            //             //        }
            //             //into potable
            //             //group potable by potable.ID
            //             group new { pos, po, st } by new { posID = pos.ID, poCID = po.CID }
            //    into g
            //             select new Store_StockIn
            //             {
            //                 CID = g.Key.poCID,
            //                 TotalPrice = g.FirstOrDefault().po.TotalPOValue,
            //                 Common_RawItemFK = g.FirstOrDefault().pos.Common_RawItemFK,
            //                 Procurement_PurchaseOrderSlaveFK = g.Key.posID,
            //                 PurchaseQuantity = g.FirstOrDefault().pos.PurchaseQuantity,
            //                 ReceivedQty = (g.FirstOrDefault().pos.PurchaseQuantity - g.Sum(x => x.st != null ? x.st.ReceivedQty : 0)),
            //                 RemainingQty = (g.FirstOrDefault().pos.PurchaseQuantity - g.Sum(x => x.st != null ? x.st.ReceivedQty : 0))
            //             }).ToList();

            //var filterData1 = store.Where(x => x.RemainingQty > 0).GroupBy(y => y.CID).Select(z => z).ToList();


            //var stockIn = _db.Store_StockIn.GroupBy(x => x.IPOFK).Select(xt => new Store_StockIn
            //{
            //    ID = xt.Max(i => i.ID),
            //    IPOFK = xt.Key,
            //    RemainingQty = xt.Min(r => r.RemainingQty)
            //}).ToList();

            //var filterData = _db.Procurement_PurchaseOrder.Where(x => !stockIn.Any(c => c.IPOFK == x.ID && c.Active && c.RemainingQty == 0)
            //&& x.Status == (int)POStatusEnum.FinalApproved).AsEnumerable();

            //if (filterData.Any())
            //{
            //    foreach (var currency in filterData.ToList())
            //    {
            //        eciList.Add(new { Text = currency.CID + " (PO Value = " + currency.TotalPOValue + ")", Value = currency.ID });
            //    }
            //}
            // return eciList;
            //=====================//
            var list = new List<object>();

            var data = (from pos in _db.Procurement_PurchaseOrderSlave
                        join po in _db.Procurement_PurchaseOrder
                                .Where(x => x.Active && !string.IsNullOrEmpty(x.CID) &&
                                            (x.Status == (int)PRStatusEnum.ProcurementApproved))
                            on pos.Procurement_PurchaseOrderFK equals po.ID
                        where
                            pos.PurchaseQuantity >
                            (_db.Store_StockIn.Any(x => x.Active && x.Procurement_PurchaseOrderSlaveFK == pos.ID)
                                ? _db.Store_StockIn.Where(x => x.Active && x.Procurement_PurchaseOrderSlaveFK == pos.ID)
                                    .Select(y => new { ReceivedQty = y.ReceivedQty }).Sum(c => c.ReceivedQty)
                                : 0)
                        group po by new { po.ID, po.CID, po.TotalPOValue }
                into g
                        select new { g.Key.ID, g.Key.CID, g.Key.TotalPOValue })
                .Select(x => x).ForEachAsync(x => list.Add(new
                {
                    Value = x.ID,
                    Text = x.CID + "(PO Value: " + x.TotalPOValue + ")"
                }));
            return list;

        }
        public List<object> ProItemDropDownList()
        {
            var itemList = new List<object>();
            if (!_db.Common_RawItem.Any(x => x.Active)) return itemList;
            {
                foreach (var Currency in _db.Common_RawItem.Where(x => x.Active).ToList())
                {
                    itemList.Add(new { Text = Currency.Name, Value = Currency.ID });
                }
                return itemList;
            }

            //return new SelectList(_db.Common_RawItem.Where(x => x.Active)
            //    .Select(x => new
            //    {
            //        Value = x.ID,
            //        Text = x.Name
            //    }).ToList(), "Value", "Text");
        }
        public List<object> ApprovedPOListAsync(int styleId = 0)
        {
            //var itemList = new List<object>();
            //itemList =
            //   from p in _db.Procurement_PurchaseOrderSlave
            //   join po in _db.Procurement_PurchaseOrder on p.Procurement_PurchaseOrderFK equals po.ID
            //   where po.Status == 5 && po.Active && p.Active && p.Merchandising_StyleID == styleId
            //   group po by po.CID into pp
            //   select new 
            //   {
            //       Text = pp.Key,
            //       Value = pp.Max(x => x.ID)
            //   };
            //return poList;

            var id = 0;
            if (!_db.Procurement_PurchaseOrder.Any(x => x.Active))
            {
                id = _db.Procurement_PurchaseOrderSlave.FirstOrDefault(x => x.Active && x.Merchandising_StyleID == styleId).ID;
            }
            var itemList = new List<object>();
            if (!_db.Procurement_PurchaseOrder.Any(x => x.Active && x.ID == id && x.Status == 5)) return itemList;
            {
                foreach (var Currency in _db.Procurement_PurchaseOrder.Where(x => x.Active && x.ID == id && x.Status == 5).ToList())
                {
                    itemList.Add(new { Text = Currency.CID, Value = Currency.ID });
                }
                return itemList;
            }

            //return new SelectList(
            //    await _db.Procurement_PurchaseOrder.Where(x => x.Active).Select(x => new { Value = x.ID, Text = x.CID })
            //        .ToListAsync(), "Value", "Text");
        }
        public List<object> ItemTypeListAsync(int id)
        {
            if (id == 1)
            {
                var storeInTypeList = new List<object>
                {
                    new {Text = "Fabric", Value = 2}
                    };
                return storeInTypeList;
            }
            else if (id == 3)
            {
                var storeInTypeList = new List<object>
            {
                    new {Text = "Sewing Trims", Value = 6},
                    new {Text = "Cut Pieces All Size", Value = 2}
                };
                return storeInTypeList;
            }
            else if (id == 4) //Ironing
            {
                var storeInTypeList = new List<object>
            {
                    new {Text = "Ironing Trims", Value = 6},
                     new {Text = "QC Item All Size", Value = 1}
                };
                return storeInTypeList;
            }
            else if (id == 5) // Packing
            {
                var storeInTypeList = new List<object>
            {
                    new {Text = "Packing Trims", Value = 6},
                    new {Text = "Packing Item All Size", Value = 5}
                };
                return storeInTypeList;
            }
            else if (id == 6)
            {
                var storeInTypeList = new List<object>
            {
                    new {Text = "Finish Product", Value = 6},
                    //new {Text = "Fabric", Value = 2}
                };
                return storeInTypeList;
            }
            else if (id == 8)
            {
                var datalist = new List<object>();
                var data = _db.Common_RawCategory.Where(x => x.Name == "Yarn").ToList();
                data.ForEach(a =>
                 {
                     datalist = new List<object> { new { Text = a.Name, Value = a.ID } };
                 });
                return datalist;
            }
            else
            {
                var storeInTypeList = new List<object> {
                    new {Text = "Trim & Accessosries", Value = 6},
                    new {Text = "Fabric", Value = 2}
                };
                return storeInTypeList;
            }
        }
        public List<object> DDLSubSetListByStyleID(int StyleID)
        {
            var StyleList = new List<object>();
            var vData = _db.Merchandising_StyleSetPack.Where(a => a.Merchandising_StyleFK == StyleID && a.Active == true).Select(a => new
            {
                ID = a.ID,
                Name = a.SetPackName
            }).ToList();

            //var vData = (from ssp in _db.Merchandising_StyleSetPack
            //            join pr in _db.Prod_ReferencePlan on ssp.ID equals pr.Merchandising_StyleSetPackFK
            //            where ssp.Active && pr.Active && ssp.Merchandising_StyleFK == StyleID && pr.Common_ColorFK != null
            //            group new { pr, ssp } by ssp into color
            //            select new
            //            {
            //                Name = color.Key.SetPackName,
            //                ID = color.Max(x=>x.pr.Common_ColorFK) 
            //            }).ToList();

            if (vData.Any())
            {
                foreach (var style in vData)
                {
                    StyleList.Add(new { Text = style.Name, Value = style.ID });
                }
            }

            return StyleList;
        }
        public async Task<SelectList> StoreDropDownListAsync(int id)
        {
            return new SelectList(
                await _db.Store_General.Where(x => x.Active && x.ID == 2).Select(x => new { Value = x.ID, Text = x.StoreName })
                    .ToListAsync(), "Value", "Text");
        }
        public async Task<SelectList> RawItemDropDownList()
        {
            return new SelectList(await _db.Common_RawItem.Where(x => x.Active)
                .Select(x => new
                {
                    Value = x.ID,
                    Text = x.Name
                }).ToListAsync(), "Value", "Text");
        }
        public async Task<SelectList> RawItemDropDownList(int id)
        {
            return new SelectList(
                await _db.Common_RawItem.Where(x => x.Active && _db.Store_StockIn.Any(s => s.FixAsset && s.ItemIDFK == x.ID)).Select(x => new { Value = x.ID, Text = x.Name }).ToListAsync()
                , "Value", "Text");
        }
        public async Task<SelectList> BrandDropDownList()
        {
            return new SelectList(await _db.Common_Brand.Where(x => x.Active).Select(x => new { Value = x.ID, Text = x.Name }).ToListAsync(), "Value", "Text");
        }
        public async Task<SelectList> ModelDropDownList()
        {
            return new SelectList(await _db.Common_Model.Where(x => x.Active).Select(x => new { Value = x.ID, Text = x.Name }).ToListAsync(), "Value", "Text");
        }
        public async Task<SelectList> AssetDropDownList()
        {
            return new SelectList(
                await _db.Store_StockRegister.Where(x => x.Active && !_db.Store_StockOutPersonal.Any(a => a.CommonAssetFK == x.ID))
                    .Select(x => new { Value = x.ID, Text = x.ItemSLNO })
                    .ToListAsync(), "Value", "Text");
        }
        public async Task<SelectList> DeptDropDownList()
        {
            return new SelectList(await _db.User_Department.Where(x => x.Active).Select(x => new { Value = x.ID, Text = x.Name }).ToListAsync(), "Value", "Text");
        }
        public async Task<SelectList> EmpDropDownList()
        {
            return new SelectList(await _db.HRMS_Employee.Where(x => x.Active).Select(x => new { Value = x.ID, Text = x.Name }).ToListAsync(), "Value", "Text");
        }
        public async Task<SelectList> CommonUnitDropDownList()
        {
            return new SelectList(await _db.Common_Unit.Where(x => x.Active).Select(x => new { Value = x.ID, Text = x.Name }).ToListAsync(), "Value", "Text");
        }

        public async Task<SelectList> StoreReturnDropDownListAsync()
        {
            return new SelectList(await _db.Store_General.Where(x => x.Active && _db.Store_StockOutPersonal.Any(a => a.StoreGeneralFK == x.ID))
                .Select(x => new { Value = x.ID, Text = x.StoreName }).ToListAsync(), "Value", "Text");
        }
        public async Task<SelectList> AssetReturnDropDownList()
        {
            return new SelectList(
                await _db.Store_StockRegister.Where(x => x.Active && _db.Store_StockOutPersonal.Any(a => a.CommonAssetFK == x.ID))
                    .Select(x => new { Value = x.ID, Text = x.ItemSLNO })
                    .ToListAsync(), "Value", "Text");
        }
        public async Task<SelectList> RawItemReturnDropDownList()
        {
            return new SelectList(await _db.Common_RawItem.Where(x => x.Active && _db.Store_StockOutPersonal.Any(a => a.RawItemFK == x.ID))
                .Select(x => new { Value = x.ID, Text = x.Name }).ToListAsync(), "Value", "Text");
        }
        public async Task<SelectList> BrandReturnDropDownList()
        {
            return new SelectList(await _db.Common_Brand.Where(x => x.Active && _db.Store_StockOutPersonal.Any(a => a.CommonBrandFK == x.ID))
                                                                        .Select(b => new { Value = b.ID, Text = b.Name })
                    .ToListAsync(), "Value", "Text");
        }
        public async Task<SelectList> ModelReturnDropDownList()
        {
            return new SelectList(await _db.Common_Model.Where(x => x.Active && _db.Store_StockOutPersonal.Any(a => a.CommonModelFK == x.ID))
                .Select(x => new { Value = x.ID, Text = x.Name }).ToListAsync(), "Value", "Text");
        }

        public async Task<SelectList> DeptReturnDropDownList()
        {
            return new SelectList(
                await _db.User_Department.Where(x => x.Active && _db.Store_StockOutPersonal.Any(a => a.CommonDepartmentFK == x.ID))
                    .Select(x => new { Value = x.ID, Text = x.Name }).ToListAsync(), "Value", "Text");
        }
        public async Task<SelectList> EmpReturnDropDownList()
        {
            return new SelectList(await _db.HRMS_Employee.Where(x => x.Active && _db.Store_StockOutPersonal.Any(a => a.EmployeeFK == x.ID))
                    .Select(x => new { Value = x.ID, Text = x.Name }).ToListAsync(), "Value", "Text");
        }
        public async Task<SelectList> CommonUnitReturnDropDownList()
        {
            return new SelectList(await _db.Common_Unit.Where(x => x.Active && _db.Store_StockOutPersonal.Any(a => a.CommonUnitFK == x.ID))
                    .Select(x => new { Value = x.ID, Text = x.Name }).ToListAsync(),
                "Value", "Text");
        }

        // =================== Store In ====================
        #region

        public IEnumerable RequisitionDropDownList()
        {
            var requisitonlist = new List<object>();
            requisitonlist.Add(new { Text = "ITR-0001", Value = 1 });
            requisitonlist.Add(new { Text = "ITR-0002", Value = 2 });
            return requisitonlist;
        }


        public List<object> DepartmentDropDownList()
        {
            var departmentList = new List<object>();
            foreach (var department in _db.User_Department.Where(x => x.Active).ToList())
            {
                departmentList.Add(new { Text = department.Name, Value = department.ID });
            }
            return departmentList;
        }

        public IEnumerable BinDropDownList()
        {
            var departmentList = new List<object>();
            var filterdata = _db.Store_Bin.Where(x => x.Common_RawItemFK == 0 && x.Active == true).AsEnumerable();
            foreach (var department in filterdata.ToList())
            {
                departmentList.Add(new { Text = department.CID + "(" + department.Capacity + ")-(" + department.Dimension + ")", Value = department.ID });
            }
            return departmentList;
        }

        public async Task<VmStoreSection> GetStoreSection()
        {
            var vmStoreGeneral = new VmStoreSection();
            var stores = await Task.Run(() => (
                from section in _db.Store_Section
                join store in _db.Store_General on section.Store_GeneralFK equals store.ID into temp
                from storeGeneral in temp.DefaultIfEmpty()
                where section.Active
                select new VmStoreSection
                {
                    ID = section.ID,
                    SectionName = section.SectionName,
                    Description = section.Description,
                    AStore_GeneralFK = storeGeneral.ID,
                    StoreName = storeGeneral.StoreName,
                }).OrderByDescending(x => x.ID).ToList());

            vmStoreGeneral.SectionList = stores;
            return vmStoreGeneral;
        }


        #endregion

        public VmStoreIn GetStoreInByPoId(VmStoreIn vmStoreIn)
        {
            var vmStorein = new VmStoreIn();
            var aStoreBin =
                from p in _db.Procurement_PurchaseOrderSlave
                join po in _db.Procurement_PurchaseOrder on p.Procurement_PurchaseOrderFK equals po.ID into ptemp
                join I in _db.Common_RawItem on p.Common_RawItemFK equals I.ID into rtemp

                from pl in ptemp.DefaultIfEmpty()
                from il in rtemp.DefaultIfEmpty()

                where p.Active && p.Procurement_PurchaseOrderFK == (vmStoreIn.IPOFK == 0 ? 1 : vmStoreIn.IPOFK)
                select new VmStoreIn
                {
                    ID = vmStoreIn.ID,
                    CID = vmStoreIn.CID,
                    ChallanNo = vmStoreIn.ChallanNo,
                    ChallanDate = vmStoreIn.ChallanDate,
                    ReceivedDate = vmStoreIn.ReceivedDate,
                    WarrentyDate = DateTime.Now,
                    Name = il.Name,
                    Description = il.Remarks,
                    StockType = "Production",
                    Remarks = vmStoreIn.Remarks,

                    RemainingQty = 0, //il.RemainingQty,
                    DamagedQty = 0, //s.DamagedQty,
                    ReceivedQty = 0, //s.ReceivedQty,

                    IPOFK = vmStoreIn.IPOFK,
                    ItemIDFK = p.Common_RawItemFK,
                    StoreBinFK = vmStoreIn.StoreBinFK,
                    CommonUnitFK = 1,//il.Common_UnitFK,
                    CommonSupplierFK = 1,//pl.Common_SupplierFK,
                    CommonCategoryFK = 1, //po.CommonCategoryFK,
                    CommonSubCategoryFK = 1, //s.CommonSubCategoryFK,

                    FixAsset = false,
                    BinDefine = false
                };
            vmStorein.DataLists = aStoreBin.ToList();
            var data = aStoreBin.ToList();
            return vmStorein;
        }

        public IEnumerable StyleDropDownList()
        {
            var StyleList = new List<object>();
            var data = (from s in _db.Merchandising_Style
                        join o in _db.Merchandising_BuyerOrder on s.Merchandising_BuyerOrderFK equals o.ID
                        where s.Active
                        select new
                        {
                            ID = s.ID,
                            StyleName = s.StyleName,
                            BuyerPO = o.BuyerPO
                        }).OrderByDescending(x => x.ID).ToList();
            data.ForEach(st => StyleList.Add(new { Value = st.ID, Text = st.StyleName + " ( Buyer Order : " + st.BuyerPO + " )" }));

            return StyleList;

            //var List = new List<object>();
            //_db.Merchandising_Style.Where(x => x.Active).Select(x => x).ToList()
            //    .ForEach(x => List.Add(new { Value = x.ID, Text = x.StyleName }));
            //return List;
        }


        public async Task<object> SaveAllItem(VmStoreIn vmmodel)
        {
            var result = -1;
            var remainingQty = 0.0;
            var count = true;
            int id = 0;
            var cid = CidServices.GetRefStoreIn(vmmodel.DepartmentId);

            foreach (var s in vmmodel.DataLists)
            {
                remainingQty = s.RemainingQty - s.ReceivedQty;
                var storeIn = new Store_StockIn()
                {
                    ID = s.ID,
                    CID = cid,
                    ChallanNo = s.ChallanNo,
                    ChallanDate = s.ChallanDate,
                    ReceivedDate = s.ReceivedDate,
                    WarrentyDate = s.WarrentyDate,
                    Name = s.Name,
                    Description = s.Description,
                    StockType = s.StockType,
                    StoreInType = s.StoreInType == "1" ? "Purchase" : "Internal",
                    Remarks = s.Remarks,

                    RemainingQty = remainingQty,
                    DamagedQty = s.DamagedQty,
                    ReceivedQty = s.ReceivedQty,

                    IPOFK = s.IPOFK == 0 ? s.ChallanNoFK : s.IPOFK,
                    ItemIDFK = s.ItemIDFK,
                    StoreBinFK = s.StoreBinFK,
                    CommonUnitFK = s.CommonUnitFK,
                    CommonSupplierFK = s.CommonSupplierFK,
                    CommonStoreFK = vmmodel.CommonStoreFK,
                    CommonCategoryFK = s.CommonCategoryFK,
                    CommonSubCategoryFK = s.CommonSubCategoryFK,

                    Procurement_PurchaseOrderSlaveFK = s.Procurement_PurchaseOrderSlaveFK,
                    UnitPrice = s.UnitPrice,
                    TotalPrice = s.UnitPrice * s.ReceivedQty,
                    Merchandising_StyleFK = s.Merchandising_StyleFK,
                    Merchandising_StyleSlaveFK = s.Merchandising_StyleSlaveFK,
                    Common_ColorFK = s.Common_ColorFK,
                    Common_Size = s.Common_Size,

                    IsLocked = true,

                    FixAsset = s.FixAsset,
                    BinDefine = s.BinDefine,
                    Active = s.IsActive,
                    User = "UserOne",
                    Common_RawItemFK = s.ItemIDFK, //s.Common_RawItemFK,
                    Store_GeneralFK = vmmodel.CommonStoreFK,
                    Common_SupplierFK = s.CommonSupplierFK
                };

                if (count)
                {
                    var storeInMaster = new Store_StockInMaster
                    {
                        ID = s.ID,
                        CID = cid,
                        ChallanNo = s.ChallanNo,
                        ChallanDate = s.ChallanDate,
                        ReceivedDate = s.ReceivedDate,
                        StockType = s.StockType,
                        StoreInType = s.StoreInType,
                        Description = s.Description
                    };
                    _db.Store_StockInMaster.Add(storeInMaster);
                    await _db.SaveChangesAsync();
                    id = storeInMaster.ID;
                    count = false;
                }

                if (storeIn.ReceivedQty > 0)
                {
                    storeIn.Store_StockInMasterFK = id;  //_db.Store_StockInMaster.FirstOrDefault(x => x.CID == cid).ID;
                    _db.Store_StockIn.Add(storeIn);
                    result = await _db.SaveChangesAsync();
                }
            }

            result = await integrationservice.POGoodsReceivedStatusUpdateFromSIn(vmmodel.IPOFK, remainingQty == 0 ? 7 : 11);
            return result;
        }


        public async Task<object> AddBinByItem(int id, int binId, string remarks, double qty)
        {
            var result = -1;
            if (binId == 0) return result;
            var storBin = _db.Store_Bin.Find(binId);
            storBin.Common_RawItemFK = id;
            storBin.ItemTotalQty = qty;
            if (await _db.SaveChangesAsync() > 0)
            {
                result = binId;
            }
            return result;
        }
        //// =================== Store Out ====================
        public async Task<VmStoreOut> GetStoreOut(int id)
        {
            var vmmodel = new VmStoreOut
            {
                DataLists = new List<VmStoreOut>()
            };
            var aStoreOut = await Task.Run(() => (
                from s in _db.Store_StockOut
                join unit in _db.Common_Unit on s.CommonUnitFK equals unit.ID into temp
                join ri in _db.Common_RawItem on s.ItemIDFK equals ri.ID into rtemp
                //join ms in _db.Merchandising_StyleSlave on s.Merchandising_StyleSlaveFK equals ms.ID into tempest
                // Code off for Color and size
                //from mss in tempest.DefaultIfEmpty()
                from rt in rtemp.DefaultIfEmpty()
                from u in temp.DefaultIfEmpty()
                where s.Active
                select new VmStoreOut()
                {
                    ID = s.ID,
                    CID = s.CID,
                    ChallanNo = s.ChallanNo,
                    ChallanDate = s.ChallanDate,
                    ReceivedDate = s.ReceivedDate,
                    WarrentyDate = s.WarrentyDate,
                    Name = rt.Name, //s.Name,

                    Description = s.Description,
                    StockType = s.StockType,
                    Remarks = s.Remarks,

                    RemainingQty = s.RemainingQty,
                    DamagedQty = s.DamagedQty,
                    ReceivedQty = s.ReceivedQty,
                    UnitName = u.Name,
                    //Common_ColorFK = (int)(mss.Common_ColorFK == null ? default(int) : mss.Common_ColorFK),
                    // Common_Size = mss.GSM,

                    IPOFK = s.IPOFK,
                    ItemIDFK = s.ItemIDFK,
                    StoreBinFK = s.StoreBinFK,
                    CommonUnitFK = s.CommonUnitFK,
                    CommonSupplierFK = s.CommonSupplierFK,
                    CommonCategoryFK = s.CommonCategoryFK,
                    CommonSubCategoryFK = s.CommonSubCategoryFK,

                    FixAsset = s.FixAsset,
                    BinDefine = s.BinDefine,
                    IsActive = s.Active
                }).ToListAsync());
            if (aStoreOut.Any())
            {
                aStoreOut.ForEach(a =>
                {
                    var color = _db.Common_Color.Where(x => x.ID == a.Common_ColorFK);
                    if (color.Any())
                    {
                        a.Name += " - " + color.FirstOrDefault().Name + " - " + a.Common_Size;
                    }
                });
                vmmodel.DataLists = aStoreOut.OrderByDescending(s => s.ID).ToList();
            }

            vmmodel.SupplierList = await SupplierDropDownListAsync();
            vmmodel.SupplierPoList = await SupplierPoDropDownListAsync();
            vmmodel.UnitList = await UnitDropDownListAsync();
            vmmodel.SupplierChallanList = await SupplierChallanDropDownListAsync();
            vmmodel.StoreList = await StoreDropDownListAsync();
            vmmodel.IsrList = await IsrDropDownListAsync();
            vmmodel.StoreOutTypeList = await StoreOutTypeDropDownList();
            vmmodel.CommonStoreFK = id;
            return vmmodel;
        }
        public async Task<VmStoreOut> GetStoreOutByStoreId(int id2)
        {
           var data = _db.Store_General.Select(x => new VmStoreGeneral {
                ID = x.ID,
                StoreName = x.StoreName,
            }).ToList();

            var vmmodel = new VmStoreOut();
            vmmodel.DataLists = new List<VmStoreOut>();
            var aStoreOut = await Task.Run(() => (
                from s in _db.Store_StockOut
                join unit in _db.Common_Unit on s.CommonUnitFK equals unit.ID into temp
                join ri in _db.Common_RawItem on s.Common_RawItemFK equals ri.ID
                //join ms in _db.Merchandising_StyleSlave on s.Merchandising_StyleSlaveFK equals ms.ID into tempest

                // from ms in tempest.DefaultIfEmpty()
                from u in temp.DefaultIfEmpty()
                where s.Active && s.Store_GeneralOutFK == id2
                select new VmStoreOut()
                {
                    ID = s.ID,
                    CID = s.CID,
                    ChallanNo = s.ChallanNo,
                    ChallanDate = s.ChallanDate,
                    ReceivedDate = s.ReceivedDate,
                    WarrentyDate = s.WarrentyDate,
                    Name = ri.Name, //s.Name,

                    Description = s.Description,
                    StockType = s.StockType,
                    Remarks = s.Remarks,

                    RemainingQty = s.RemainingQty,
                    DamagedQty = s.DamagedQty,
                    ReceivedQty = s.ReceivedQty,
                    UnitName = u.Name,
                    //Common_ColorFK = (int)(ms.Common_ColorFK == null ? default(int) : ms.Common_ColorFK),
                    //Common_Size = ms.GSM,

                    IPOFK = s.IPOFK,
                    ItemIDFK = s.ItemIDFK,
                    StoreBinFK = s.StoreBinFK,
                    CommonUnitFK = s.CommonUnitFK,
                    CommonSupplierFK = s.CommonSupplierFK,
                    CommonCategoryFK = s.CommonCategoryFK,
                    CommonSubCategoryFK = s.CommonSubCategoryFK,

                    FixAsset = s.FixAsset,
                    BinDefine = s.BinDefine,
                    IsActive = s.Active
                }).OrderByDescending(s => s.ID).ToListAsync());
            if (aStoreOut.Any())
            {
                aStoreOut.ForEach(a =>
                {
                    var color = _db.Common_Color.Where(x => x.ID == a.Common_ColorFK);
                    if (color.Any())
                    {
                        a.Name += " - " + color.FirstOrDefault().Name + " - " + a.Common_Size;
                    }
                });
            }
            vmmodel.DataLists = aStoreOut;

            vmmodel.SupplierList = await SupplierDropDownListAsync();
            vmmodel.SupplierPoList = await SupplierPoDropDownListAsync();
            vmmodel.UnitList = await UnitDropDownListAsync();
            vmmodel.SupplierChallanList = await SupplierChallanDropDownListAsync();
            vmmodel.StoreList = await StoreDropDownListAsync();
            vmmodel.IsrList = await IsrDropDownListAsync(id2);
            vmmodel.StoreOutTypeList = await StoreOutTypeDropDownList();
            vmmodel.CommonStoreFK = id2;
            return vmmodel;
        }
        public async Task<VmStoreOut> GetItemBySrId(VmStoreOut vmModel)
        {
            ///========= Store Out Master =========///
            var cid = CidServices.GetRefStoreOutMaster(vmModel.DepartmentId);

            var storeOutMaster = new Store_StockOutMaster
            {
                ID = 0, //vmModel.ID,
                Active = true,
                CID = cid,
                ChallanNo = vmModel.ChallanNo,
                ChallanDate = vmModel.ChallanDate,
                ReceivedDate = vmModel.ReceivedDate,
                StockType = vmModel.StockType,
                StoreOutType = vmModel.StoreInType,
                Description = vmModel.Description,
                User = vmModel.User
            };

            _db.Store_StockOutMaster.Add(storeOutMaster);
            int result = await _db.SaveChangesAsync();
            var sotoreOutMasterId = storeOutMaster.ID;

            ///========= Store Out Master =========///
            var vmmodel = new VmStoreOut
            {
                DataList = new List<VmStoreOut>()
            };

            if (vmModel.StoreInType == "2" && vmModel.IPOFK != 0)
            {
                int merchandising_StyleFK = _db.Store_RequisitionSlave.FirstOrDefault(x => x.Store_RequisitionFK == vmModel.IPOFK).Merchandising_StyleFK;
                var merData = (from ms in _db.Merchandising_StyleSlave
                               where ms.Merchandising_StyleFK == merchandising_StyleFK && ms.IsFabric
                               select new Merchandising_StyleSlave
                               {
                                   Merchandising_StyleFK = ms.Merchandising_StyleFK,
                                   Common_RawItemFK = ms.Common_RawItemFK,
                                   IsFabric = ms.IsFabric
                               }).ToList();

                vmModel.Store_GeneralOutFK = vmModel.Store_GeneralOutFK == 0 ? 2 : vmModel.Store_GeneralOutFK;
                vmModel.CommonStoreFK = vmModel.CommonStoreFK == 0 ? 2 : vmModel.CommonStoreFK;
                var store_StockIn = new List<Store_StockIn>();
                if (vmModel.CommonStoreFK == 2)
                {
                    store_StockIn = (from so in _db.Store_StockIn
                                     join som in _db.Store_StockInMaster on so.Store_StockInMasterFK equals som.ID
                                     join ms in _db.Merchandising_Style on so.Merchandising_StyleFK equals ms.ID
                                     where som.Active && so.Active && so.Merchandising_StyleFK == merchandising_StyleFK
                                     && so.Store_GeneralFK == vmModel.CommonStoreFK
                                     group new { so, som } by new { so.Merchandising_StyleFK, so.Common_RawItemFK } into itemgroup
                                     select new Store_StockIn
                                     {
                                         Merchandising_StyleFK = itemgroup.Key.Merchandising_StyleFK,
                                         Common_RawItemFK = itemgroup.Key.Common_RawItemFK,
                                         Name = itemgroup.Max(n => n.so.Name),

                                         ReceivedQty = _db.Merchandising_StyleSlave.Where(x => x.Merchandising_StyleFK == merchandising_StyleFK && x.Common_RawItemFK == itemgroup.Key.Common_RawItemFK && x.IsFabric).Any() ?
                                         (itemgroup.Sum(i => i.so.ReceivedQty) / (double)_db.Merchandising_StyleSlave.FirstOrDefault(x => x.Merchandising_StyleFK == merchandising_StyleFK && x.Common_RawItemFK == itemgroup.Key.Common_RawItemFK && x.IsFabric).Consumption) * 12 :
                                         itemgroup.Sum(i => i.so.ReceivedQty)

                                     }).ToList();
                }
                else
                {
                    store_StockIn = (from so in _db.Store_StockIn
                                     join som in _db.Store_StockInMaster on so.Store_StockInMasterFK equals som.ID
                                     join ms in _db.Merchandising_Style on so.Merchandising_StyleFK equals ms.ID
                                     where som.Active && so.Active && so.Merchandising_StyleFK == merchandising_StyleFK
                                     && so.Store_GeneralFK == vmModel.CommonStoreFK
                                     group new { so, som } by new { so.Merchandising_StyleFK, so.Common_RawItemFK} into itemgroup
                                     select new Store_StockIn
                                     {
                                         Merchandising_StyleFK = itemgroup.Key.Merchandising_StyleFK,
                                         Common_RawItemFK = itemgroup.Key.Common_RawItemFK,
                                         Name = itemgroup.Max(n => n.so.Name),
                                         ReceivedQty = itemgroup.Sum(i => i.so.ReceivedQty)

                                     }).ToList();

                }

                var store_StockOut = (from so in _db.Store_StockOut
                                      join som in _db.Store_StockOutMaster on so.Store_StockOutMasterFK equals som.ID
                                      join srs in _db.Store_RequisitionSlave on so.Store_RequisitionSlaveFK equals srs.ID
                                      join sr in _db.Store_Requisition on srs.Store_RequisitionFK equals sr.ID
                                      where som.Active && so.Active && so.Merchandising_StyleFK == merchandising_StyleFK
                                      && so.IPOFK == vmModel.IPOFK
                                      group new { so, som } by new { so.Common_RawItemFK, srs.Common_ColorFK, srs.Common_SizeFK } into itemgroup
                                      select new Store_StockOut
                                      {
                                          Merchandising_StyleFK = itemgroup.Max(s => s.so.Merchandising_StyleFK),
                                          Common_RawItemFK = itemgroup.Key.Common_RawItemFK,
                                          //Name = itemgroup.Max(n=>n.so.Name),
                                          ReceivedQty = itemgroup.Sum(i => i.so.ReceivedQty)
                                      }).ToList();

                var store_RequisitionSlave = (
                                  from srs in _db.Store_RequisitionSlave
                                  join sr in _db.Store_Requisition on srs.Store_RequisitionFK equals sr.ID
                                  where sr.Status == 5 && srs.Active && sr.Active && srs.Merchandising_StyleFK == merchandising_StyleFK
                                  && srs.Store_RequisitionFK == vmModel.IPOFK //&& sr.Store_GeneralOutFK == vmModel.Store_GeneralFK
                                  group new { srs, sr } by new { srs.Common_RawItemFK, srs.Common_ColorFK, srs.Common_SizeFK } into itemgroup
                                  select new Store_RequisitionSlave
                                  {
                                      Merchandising_StyleFK = itemgroup.Max(m => m.srs.Merchandising_StyleFK),
                                      Common_RawItemFK = itemgroup.Key.Common_RawItemFK,
                                      RequisitionQuantity = itemgroup.Sum(i => i.srs.RequisitionQuantity),
                                      RemainingQty = itemgroup.Sum(i => i.srs.RequisitionQuantity),
                                      RequiredDate = itemgroup.Max(m => m.srs.RequiredDate),
                                      Common_ColorFK = itemgroup.Max(m => m.srs.Common_ColorFK),
                                      Common_SizeFK = itemgroup.Max(m => m.srs.Common_SizeFK),
                                      Common_Color = itemgroup.Max(m => m.srs.Common_Color),
                                      Common_Size = itemgroup.Max(m => m.srs.Common_Size),
                                      Merchandising_StyleSlaveFK = itemgroup.Max(m => m.srs.Merchandising_StyleSlaveFK),
                                      Store_RequisitionFK = itemgroup.Max(m => m.srs.Store_RequisitionFK),

                                  }).ToList();

                if (store_RequisitionSlave.Any())
                {
                    store_RequisitionSlave.ForEach(r =>
                    {
                        var itemOut = store_StockOut.Where(so => so.Common_RawItemFK == r.Common_RawItemFK);

                        if (itemOut.Any())
                        {
                            r.RemainingQty = r.RequisitionQuantity - itemOut.FirstOrDefault().ReceivedQty;
                        }
                    });
                }
                vmmodel.DataLists = new List<VmStoreOut>();
                var dataLists = (store_RequisitionSlave.Where(x => x.RequisitionQuantity > 0).Select(r => new VmStoreOut
                {
                    StyleName = _db.Merchandising_Style.FirstOrDefault(x => x.ID == r.Merchandising_StyleFK).StyleName,
                    Merchandising_StyleFK = r.Merchandising_StyleFK,
                    Common_RawItemFK = r.Common_RawItemFK,

                    StoreAvailableQty = 0, //store_StockIn.FirstOrDefault(si => si.Common_RawItemFK == r.Common_RawItemFK).ReceivedQty > 0 ? store_StockIn.FirstOrDefault(si => si.Common_RawItemFK == r.Common_RawItemFK).ReceivedQty - (r.RequisitionQuantity - r.RemainingQty) : 0,
                    RequisitionQuantity = r.RequisitionQuantity,
                    DamagedQty = 0,
                    RemainingQty = r.RemainingQty,
                    ReceivedQty = 0, //r.RequisitionQuantity - r.RemainingQty,

                    ChallanNo = cid,
                    ChallanDate = DateTime.Now,
                    ReceivedDate = r.RequiredDate,
                    WarrentyDate = DateTime.Now,
                    Name = (r.Common_RawItemFK == 0 ? "Item Name Not Found" : _db.Common_RawItem.FirstOrDefault(ri => ri.ID == r.Common_RawItemFK).Name) +
                    "-" + (r.Common_ColorFK == 0 ? "No Color Name" : _db.Common_Color.FirstOrDefault(x => x.ID == r.Common_ColorFK).Name) +
                    "-" + _db.Common_Size.FirstOrDefault(x => x.ID == r.Common_SizeFK).Name,

                    Description = "No Description",
                    StockType = "Production",

                    Common_ColorFK = r.Common_ColorFK,
                    Common_SizeFK = r.Common_SizeFK,
                    Common_Color = r.Common_Color,
                    Common_Size = r.Common_Size,

                    UnitPrice = 0,
                    TotalPrice = 0.00,
                    Merchandising_StyleSlaveFK = r.Merchandising_StyleSlaveFK,
                    Procurement_PurchaseOrderSlaveFK = _db.Procurement_PurchaseOrderSlave.FirstOrDefault(x => x.Merchandising_StyleID == merchandising_StyleFK && x.Common_RawItemFK == r.Common_RawItemFK).ID,
                    Store_RequisitionSlaveFK = r.Store_RequisitionFK,

                    ItemIDFK = r.Common_RawItemFK,
                    CommonUnitFK = (r.Common_RawItemFK == 0 ? 1 : _db.Common_RawItem.FirstOrDefault(ri => ri.ID == r.Common_RawItemFK).Common_UnitFK),
                    UnitName = (r.Common_RawItemFK == 0 ? "Unit Name Not Found" :
                    _db.Common_Unit.FirstOrDefault(ri => ri.ID == _db.Common_RawItem.FirstOrDefault(x => x.ID == r.Common_RawItemFK).Common_UnitFK).Name),

                    CommonSupplierFK = 1,
                    CommonCategoryFK = 1,
                    CommonSubCategoryFK = 1,

                    FixAsset = false,
                    BinDefine = false,
                    IsActive = true,
                    Store_StockOutMasterFK = sotoreOutMasterId,

                    ID = 0,
                    IPOFK = vmModel.IPOFK,
                    StoreBinFK = vmModel.StoreBinFK,
                    CommonStoreFK = vmModel.Store_GeneralOutFK,
                    Remarks = vmModel.Remarks,
                    Store_GeneralInFK = vmModel.Store_GeneralInFK,
                    Store_GeneralOutFK = vmModel.Store_GeneralOutFK

                })).ToList();

                if (dataLists.Any())
                {
                    dataLists.ForEach(r =>
                    {
                        var stockIn = store_StockIn.FirstOrDefault(si => si.Common_RawItemFK == r.Common_RawItemFK).ReceivedQty;
                        if (stockIn > 0)
                        {
                            r.StoreAvailableQty = stockIn - (r.RequisitionQuantity - r.RemainingQty);
                        }
                    });
                }
                vmmodel.DataLists = dataLists;
            }

            if (vmModel.StoreInType == "2" && vmModel.IPOFK == 0)
            {
                int merchandising_StyleFK = _db.Store_RequisitionSlave.FirstOrDefault(x => x.Store_RequisitionFK == vmModel.IPOFK).Merchandising_StyleFK;
                vmModel.Store_GeneralOutFK = vmModel.Store_GeneralOutFK == 0 ? 2 : vmModel.Store_GeneralOutFK;
                vmModel.CommonStoreFK = vmModel.CommonStoreFK == 0 ? 2 : vmModel.CommonStoreFK;

                var store_StockIn = (from so in _db.Store_StockIn
                                     join som in _db.Store_StockInMaster on so.Store_StockInMasterFK equals som.ID
                                     where som.Active && so.Active && so.Merchandising_StyleFK == merchandising_StyleFK && so.Store_GeneralFK == vmModel.CommonStoreFK
                                     group new { so, som } by so.Common_RawItemFK into itemgroup
                                     select new Store_StockIn
                                     {
                                         Merchandising_StyleFK = itemgroup.Max(s => s.so.Merchandising_StyleFK),
                                         Common_RawItemFK = itemgroup.Key,
                                         Name = itemgroup.Max(n => n.so.Name),
                                         ReceivedQty = itemgroup.Sum(i => i.so.ReceivedQty)
                                     }).ToList();

                var store_StockOut = (from so in _db.Store_StockOut
                                      join som in _db.Store_StockOutMaster on so.Store_StockOutMasterFK equals som.ID
                                      join srs in _db.Store_RequisitionSlave on so.Store_RequisitionSlaveFK equals srs.ID
                                      join sr in _db.Store_Requisition on srs.Store_RequisitionFK equals sr.ID
                                      where som.Active && so.Active && so.Merchandising_StyleFK == merchandising_StyleFK
                                      && so.IPOFK == vmModel.IPOFK
                                      group new { so, som } by new { so.Common_RawItemFK, srs.Common_ColorFK, srs.Common_SizeFK } into itemgroup
                                      select new Store_StockOut
                                      {
                                          Merchandising_StyleFK = itemgroup.Max(s => s.so.Merchandising_StyleFK),
                                          Common_RawItemFK = itemgroup.Key.Common_RawItemFK,
                                          //Name = itemgroup.Max(n=>n.so.Name),
                                          ReceivedQty = itemgroup.Sum(i => i.so.ReceivedQty)
                                      }).ToList();

                var store_RequisitionSlave = (
                                  from srs in _db.Store_RequisitionSlave
                                  join sr in _db.Store_Requisition on srs.Store_RequisitionFK equals sr.ID
                                  where sr.Status == 5 && srs.Active && sr.Active && srs.Merchandising_StyleFK == merchandising_StyleFK
                                  && sr.Store_GeneralOutFK == vmModel.Store_GeneralOutFK //&& srs.Store_RequisitionFK == vmmodel.IPOFK
                                  group new { srs, sr } by new { srs.Common_RawItemFK, srs.Common_ColorFK, srs.Common_SizeFK } into itemgroup
                                  select new Store_RequisitionSlave
                                  {
                                      Merchandising_StyleFK = itemgroup.Max(m => m.srs.Merchandising_StyleFK),
                                      Common_RawItemFK = itemgroup.Key.Common_RawItemFK,
                                      RequisitionQuantity = itemgroup.Sum(i => i.srs.RequisitionQuantity),
                                      RemainingQty = itemgroup.Sum(i => i.srs.RequisitionQuantity),
                                      RequiredDate = itemgroup.Max(m => m.srs.RequiredDate),
                                      Common_ColorFK = itemgroup.Max(m => m.srs.Common_ColorFK),
                                      Common_SizeFK = itemgroup.Max(m=>m.srs.Common_SizeFK),
                                      Common_Color = itemgroup.Max(m=>m.srs.Common_Color),
                                      Common_Size = itemgroup.Max(m => m.srs.Common_Size),
                                      Merchandising_StyleSlaveFK = itemgroup.Max(m => m.srs.Merchandising_StyleSlaveFK),
                                      Store_RequisitionFK = itemgroup.Max(m => m.srs.Store_RequisitionFK),

                                  }).ToList();

                if (store_RequisitionSlave.Any())
                {
                    store_RequisitionSlave.ForEach(r =>
                    {
                        var itemOut = store_StockOut.Where(so => so.Common_RawItemFK == r.Common_RawItemFK);

                        if (itemOut.Any())
                        {
                            r.RemainingQty = r.RequisitionQuantity - itemOut.FirstOrDefault().ReceivedQty;
                        }
                    });
                }
                vmmodel.DataLists = new List<VmStoreOut>();
                vmmodel.DataLists = (store_RequisitionSlave.Where(x => x.RequisitionQuantity > 0).Select(r => new VmStoreOut
                {
                    StyleName = _db.Merchandising_Style.FirstOrDefault(x => x.ID == r.Merchandising_StyleFK).StyleName,
                    Merchandising_StyleFK = r.Merchandising_StyleFK,
                    Common_RawItemFK = r.Common_RawItemFK,

                    StoreAvailableQty = store_StockIn.Count > 0 ? store_StockIn.FirstOrDefault(si => si.Common_RawItemFK == r.Common_RawItemFK).ReceivedQty - (r.RequisitionQuantity - r.RemainingQty) : 0,
                    RequisitionQuantity = r.RequisitionQuantity,
                    DamagedQty = 0,
                    RemainingQty = r.RemainingQty,
                    ReceivedQty = 0, //r.RequisitionQuantity - r.RemainingQty,

                    ChallanNo = cid,
                    ChallanDate = DateTime.Now,
                    ReceivedDate = r.RequiredDate,
                    WarrentyDate = DateTime.Now,
                    Name = (r.Common_RawItemFK == 0 ? "Item Name Not Found" : _db.Common_RawItem.FirstOrDefault(ri => ri.ID == r.Common_RawItemFK).Name) +
                    "-" + _db.Common_Color.FirstOrDefault(x => x.ID == r.Common_ColorFK).Name + "-" + _db.Common_Size.FirstOrDefault(x => x.ID == r.Common_SizeFK).Name,

                    Description = "No Description",
                    StockType = "Production",

                    Common_ColorFK = r.Common_ColorFK,
                    Common_SizeFK = r.Common_SizeFK,
                    Common_Color = r.Common_Color,
                    Common_Size = r.Common_Size,

                    UnitPrice = 0,
                    TotalPrice = 0.00,
                    Merchandising_StyleSlaveFK = r.Merchandising_StyleSlaveFK,
                    Procurement_PurchaseOrderSlaveFK = 1,
                    Store_RequisitionSlaveFK = r.Store_RequisitionFK,

                    ItemIDFK = r.Common_RawItemFK,
                    CommonUnitFK = (r.Common_RawItemFK == 0 ? 1 : _db.Common_RawItem.FirstOrDefault(ri => ri.ID == r.Common_RawItemFK).Common_UnitFK),
                    UnitName = (r.Common_RawItemFK == 0 ? "Unit Name Not Found" :
                    _db.Common_Unit.FirstOrDefault(ri => ri.ID == _db.Common_RawItem.FirstOrDefault(x => x.ID == r.Common_RawItemFK).Common_UnitFK).Name
                    ),
                    CommonSupplierFK = 1,
                    CommonCategoryFK = 1,
                    CommonSubCategoryFK = 1,

                    FixAsset = false,
                    BinDefine = false,
                    IsActive = true,
                    Store_StockOutMasterFK = sotoreOutMasterId,

                    ID = 0,
                    IPOFK = vmModel.IPOFK,
                    StoreBinFK = vmModel.StoreBinFK,
                    CommonStoreFK = vmModel.Store_GeneralInFK,
                    Remarks = vmModel.Remarks,
                    Store_GeneralOutFK = vmModel.Store_GeneralOutFK,
                    Store_GeneralInFK = vmModel.Store_GeneralInFK

                })).ToList();
            }
            return vmmodel;
        }

        public async Task<VmStoreOut> StoreOutSaleReturn(VmStoreOut vmStoreIn, int id)
        {
            var vmmodel = new VmStoreOut();
            var callahanNum = CidServices.GetRefStoreOutSaleReturn(vmStoreIn.DepartmentId);

            var aStoreBin = await Task.Run(() => (
                from r in _db.Store_StockIn
                where r.Active && r.IsLocked == false
                && r.ChallanNo == (string.IsNullOrEmpty(vmStoreIn.IpoNumber) ? "" : vmStoreIn.IpoNumber)
                select new VmStoreOut
                {
                    ID = vmStoreIn.ID,
                    CID = vmStoreIn.CID,
                    ChallanNo = callahanNum,
                    ChallanNumberIn = r.ChallanNo,
                    ChallanDate = DateTime.Now,
                    ReceivedDate = r.ChallanDate, //pr.Time,
                    WarrentyDate = DateTime.Now,
                    Name = r.Name,
                    Description = "No Description",
                    StockType = "Production",
                    Remarks = vmStoreIn.Remarks,

                    RemainingQty = r.ReceivedQty,
                    DamagedQty = r.DamagedQty,
                    ReceivedQty = r.ReceivedQty,

                    IPOFK = r.IPOFK,
                    ItemIDFK = r.ItemIDFK,
                    StoreBinFK = vmStoreIn.StoreBinFK,
                    CommonUnitFK = r.CommonUnitFK,
                    CommonSupplierFK = 1, //il.Common_SupplierFK,
                    CommonCategoryFK = 1, //po.CommonCategoryFK,
                    CommonSubCategoryFK = 1, //s.CommonSubCategoryFK,
                    CommonStoreFK = vmStoreIn.Store_GeneralInFK,

                    FixAsset = false,
                    BinDefine = false,
                    IsActive = true

                }).ToListAsync());
            vmmodel.DataLists = aStoreBin.ToList();
            vmmodel.SupplierList = await SupplierDropDownListAsync();
            vmmodel.SupplierPoList = await SupplierPoDropDownListAsync();
            vmmodel.UnitList = await UnitDropDownListAsync();
            vmmodel.StoreOutTypeList = await StoreOutTypeDropDownList();
            vmmodel.CommonStoreFK = id;
            return vmmodel;
        }
        public async Task<VmStoreOut> StoreOutSaleReturnByStoreId(VmStoreOut vmStoreIn, int id)
        {
            var vmmodel = new VmStoreOut();
            var callahanNum = CidServices.GetRefStoreOutSaleReturn(vmStoreIn.DepartmentId);

            var aStoreBin = await Task.Run(() => (
                from r in _db.Store_StockIn
                where r.Active && r.RemainingQty > 0 && r.CommonStoreFK == id
                && r.ChallanNo == (string.IsNullOrEmpty(vmStoreIn.IpoNumber) ? "" : vmStoreIn.IpoNumber)
                select new VmStoreOut
                {
                    ID = vmStoreIn.ID,
                    CID = vmStoreIn.CID,
                    ChallanNo = callahanNum,
                    ChallanNumberIn = r.ChallanNo,
                    ChallanDate = DateTime.Now,
                    ReceivedDate = r.ChallanDate, //pr.Time,
                    WarrentyDate = DateTime.Now,
                    Name = r.Name,
                    Description = "No Description",
                    StockType = "Production",
                    Remarks = vmStoreIn.Remarks,

                    RemainingQty = r.RemainingQty,
                    DamagedQty = r.DamagedQty,
                    ReceivedQty = r.RemainingQty,

                    IPOFK = vmStoreIn.IPOFK,
                    ItemIDFK = r.ItemIDFK,
                    StoreBinFK = vmStoreIn.StoreBinFK,
                    CommonUnitFK = r.CommonUnitFK,
                    CommonSupplierFK = 1, //il.Common_SupplierFK,
                    CommonCategoryFK = 1, //po.CommonCategoryFK,
                    CommonSubCategoryFK = 1, //s.CommonSubCategoryFK,
                    CommonStoreFK = vmStoreIn.Store_GeneralInFK,

                    FixAsset = false,
                    BinDefine = false,
                    IsActive = true

                }).ToListAsync());
            vmmodel.DataLists = aStoreBin.ToList();
            vmmodel.SupplierList = await SupplierDropDownListAsync();
            vmmodel.SupplierPoList = await SupplierPoDropDownListAsync();
            vmmodel.UnitList = await UnitDropDownListAsync();
            vmmodel.StoreOutTypeList = await StoreOutTypeDropDownList();
            vmmodel.CommonStoreFK = id;
            return vmmodel;
        }
        public async Task<object> SaveAllItemStoreOut(VmStoreOut vmModel)
        {
            var result = -1;
            var callahanNum = CidServices.GetRefStoreOut(vmModel.DepartmentId);
            var stockIn = vmModel.DataLists.FirstOrDefault();

            VmStoreOut vmStoreOut = new VmStoreOut();
            vmStoreOut.DataLists = vmModel.DataLists.Where(x => x.ReceivedQty > 0).ToList();
            Store_StockOut store_StockOut = new Store_StockOut();
            store_StockOut.StoreStokOutSlave = new List<Store_StockOut>();
            store_StockOut.StoreStokOutSlave = vmStoreOut.DataLists.Select(s => new Store_StockOut
            {
                ID = s.ID,
                CID = callahanNum,
                ChallanNo = s.ChallanNo,
                ChallanDate = s.ChallanDate,
                ReceivedDate = s.ReceivedDate,
                WarrentyDate = s.WarrentyDate,
                Name = s.Name,
                Description = s.Description,
                StockType = s.StockType,
                Remarks = s.Remarks,

                RemainingQty = s.RemainingQty - s.ReceivedQty,
                DamagedQty = s.DamagedQty,
                ReceivedQty = s.ReceivedQty,

                Procurement_PurchaseOrderSlaveFK = s.Procurement_PurchaseOrderSlaveFK,
                UnitPrice = s.UnitPrice,
                TotalPrice = s.UnitPrice * s.ReceivedQty,
                Merchandising_StyleFK = s.Merchandising_StyleFK,
                Merchandising_StyleSlaveFK = s.Merchandising_StyleSlaveFK,
                Store_RequisitionSlaveFK = s.Store_RequisitionSlaveFK,
                Common_ColorFK = s.Common_ColorFK,
                Common_Size = s.Common_Size,

                IsLocked = true,

                IPOFK = s.IPOFK == 0 ? stockIn.ID : s.IPOFK, // Update by mi
                ItemIDFK = s.Common_RawItemFK,

                StoreBinFK = s.StoreBinFK,
                CommonUnitFK = s.CommonUnitFK,
                CommonSupplierFK = s.CommonSupplierFK,
                CommonCategoryFK = s.CommonCategoryFK,
                CommonSubCategoryFK = s.CommonSubCategoryFK,
                CommonStoreFK = s.CommonStoreFK,
                FixAsset = s.FixAsset,
                BinDefine = s.BinDefine,
                Active = s.IsActive,
                User = vmModel.User,
                Common_RawItemFK = s.Common_RawItemFK,
                Common_SupplierFK = s.CommonSupplierFK,
                Store_GeneralFK = s.CommonStoreFK,
                Store_StockOutMasterFK = s.Store_StockOutMasterFK,
                Store_GeneralOutFK = vmModel.Store_GeneralOutFK,
                Store_GeneralInFK = s.Store_GeneralInFK,
            }).ToList();

            await _db.Store_StockOut.AddRangeAsync(store_StockOut.StoreStokOutSlave);
            result = await _db.SaveChangesAsync();


            //    if (storeOut.ReceivedQty > 0)
            //    {
            //        _db.Store_StockOut.Add(storeOut);
            //        result = await _db.SaveChangesAsync();

            //        if (result == 1 && s.FixAsset)
            //        {
            //            AddAssetRegister(s, controller, action);
            //        }

            //        try
            //        {
            //            var totalPrice = await integrationservice.GetRawItemUnitPriceBy(storeOut.ItemIDFK, storeOut.ReceivedQty, storeOut.ItemIDFK);
            //            //result = await integrationservice.AccountingJournalPush( (int)DebitEnum.Wip, (int)CreditEnum.Rawinventory, totalPrice, IntegratedJournalTypeEnum.GetName(typeof(IntegratedJournalTypeEnum), IntegratedJournalTypeEnum.StoreOutInternal), (int)CostCenterEnum.General);
            //            // result = await integrationservice.InventroyPush( (int)DebitEnum.Wip, (int)CreditEnum.Rawinventory, totalPrice, IntegratedJournalTypeEnum.GetName(typeof(IntegratedJournalTypeEnum), IntegratedJournalTypeEnum.StoreOutInternal), (int)CostCenterEnum.General);

            //        }
            //        catch (Exception ex)
            //        {
            //            throw new Exception();
            //        }


            //        //Task<int> status;
            //        //if (vmStoreOut.StoreInType == "Internal")
            //        //{
            //        //    status = integrationservice.AccountingJournalPush((int)DebitEnum.Wip, (int)CreditEnum.Rawinventory, totalPrice, "StoreOut", (int)CostCenterEnum.General);
            //        //}
            //        //else if (vmStoreOut.StoreInType == "SaleReturn")
            //        //{
            //        //    status = integrationservice.AccountingJournalPush((int)vmStoreOut.CommonSupplierFK, (int)CreditEnum.Rawinventory, totalPrice, "SaleReturn", (int)CostCenterEnum.General);
            //        //}
            //        //else
            //        //{
            //        //    status = integrationservice.AccountingJournalPush((int)DebitEnum.Unknown, (int)CreditEnum.Rawinventory, totalPrice, "Unknown", (int)CostCenterEnum.General);
            //        //}
            //    }
            //}

            return result;
        }
        private void AddAssetRegister(VmStoreOut s, string controller, string action)
        {
            for (var i = 0; i < s.ReceivedQty; i++)
            {
                //context.Students.AddRange(newStudents);
                //context.SaveChanges();
                var regItem = _db.Store_StockRegister.Find(s.CID);
                regItem.Active = false;
                using (_db.SaveChangesAsync())
                {
                }
            }
        }

        private void AddAssetRegister(VmStoreIn s)
        {
            for (var i = 0; i < s.ReceivedQty; i++)
            {
                //context.Students.AddRange(newStudents);
                //context.SaveChanges();
                var regItem = _db.Store_StockRegister.Find(s.CID);
                regItem.Active = false;
                using (_db.SaveChangesAsync())
                {
                }
            }
        }
        //// ================== Store Out =====================
        public async Task<int> StoreDelete(VmStoreGeneral vmStoreGeneral)
        {
            var result = -1;
            var store = _db.Store_General.Find(vmStoreGeneral.ID);
            store.Active = false;
            if (await _db.SaveChangesAsync() > 0)
            {
                result = store.ID;
            }
            return result;
        }


        #region fahim
        #region Common
        public List<object> RawCategoryDropDownList()
        {
            var List = new List<object>();
            _db.Common_RawCategory.Where(x => x.Active).Select(x => x).ToList()
            .ForEach(x => List.Add(new { Value = x.ID, Text = x.Name }));
            return List;

        }

        public List<object> RawSubCategoryDropDownList()
        {
            var List = new List<object>();
            _db.Common_RawCategory.Where(x => x.Active).Select(x => x).ToList()
            .ForEach(x => List.Add(new { Value = x.ID, Text = x.Name }));
            return List;

        }

        public List<object> EmployeeDropDownList()
        {
            var List = new List<object>();
            _db.HRMS_Employee.Where(x => x.Active).Select(x => x).ToList().ForEach(x => List.Add(new { Value = x.ID, Text = x.Name }));
            return List;
        }

        #endregion

        #region StoreRequisition Create,Update, Delete Portion Services
        public async Task<VMStoreRequisition> StoreRequisitionGet(VMStoreRequisition model)
        {
            VMStoreRequisition vMStoreRequisition = new VMStoreRequisition();

            vMStoreRequisition.DataList = new List<VMStoreRequisition>();

            vMStoreRequisition.Status = model.Status;
            model.CommonStoreFK = (model.CommonStoreFK == 0 ? 2 : model.CommonStoreFK);
            var myInClause = new string[] { "null", "2" };

            int t = (int)model.Status;
            if (model.Status != SRStatusEnum.Closed)
            {
                if (model.CommonStoreFK != 2)
                {
                    vMStoreRequisition.DraftStatusCount = await Task.Run(() => (_db.Store_Requisition.Where(x => x.Status == (int)SRStatusEnum.Draft && !string.IsNullOrEmpty(x.CID) && x.Active && x.Store_GeneralInFK == model.CommonStoreFK).CountAsync().Result.ToString()));
                    vMStoreRequisition.SubmittedStatusCount = await Task.Run(() => (_db.Store_Requisition.Where(x => x.Status == (int)SRStatusEnum.Submitted && x.Active && x.Store_GeneralInFK == model.CommonStoreFK).CountAsync().Result.ToString()));
                    vMStoreRequisition.PrimaryStatusCount = await Task.Run(() => (_db.Store_Requisition.Where(x => x.Status == (int)SRStatusEnum.PrimaryApproved && x.Active && x.Store_GeneralInFK == model.CommonStoreFK).CountAsync().Result.ToString()));
                    vMStoreRequisition.FinalStatusCount = await Task.Run(() => (_db.Store_Requisition.Where(x => x.Status == (int)SRStatusEnum.FinalApproved && x.Active && x.Store_GeneralInFK == model.CommonStoreFK).CountAsync().Result.ToString()));
                    vMStoreRequisition.ProcurementStatusCount = await Task.Run(() => (_db.Store_Requisition.Where(x => x.Status == (int)SRStatusEnum.ProcurementApproved && x.Active && x.Store_GeneralInFK == model.CommonStoreFK).CountAsync().Result.ToString()));
                    vMStoreRequisition.PartialPOStatusCount = await Task.Run(() => (_db.Store_Requisition.Where(x => x.Status == (int)SRStatusEnum.PartialPOCreated && x.Active && x.Store_GeneralInFK == model.CommonStoreFK).CountAsync().Result.ToString()));
                    vMStoreRequisition.FullPOStatusCount = await Task.Run(() => (_db.Store_Requisition.Where(x => x.Status == (int)SRStatusEnum.FullPOCreated && x.Active && x.Store_GeneralInFK == model.CommonStoreFK).CountAsync().Result.ToString()));
                    vMStoreRequisition.InspectionStatusCount = await Task.Run(() => (_db.Store_Requisition.Where(x => x.Status == (int)SRStatusEnum.InspectionDone && x.Active && x.Store_GeneralInFK == model.CommonStoreFK).CountAsync().Result.ToString()));
                    vMStoreRequisition.GoodsReceivedStatusCount = await Task.Run(() => (_db.Store_Requisition.Where(x => x.Status == (int)SRStatusEnum.GoodsReceived && x.Active && x.Store_GeneralInFK == model.CommonStoreFK).CountAsync().Result.ToString()));
                    vMStoreRequisition.HoldStatusCount = await Task.Run(() => (_db.Store_Requisition.Where(x => x.Status == (int)SRStatusEnum.Hold && x.Active && x.Store_GeneralInFK == model.CommonStoreFK).CountAsync().Result.ToString()));
                }
                else
                {
                    vMStoreRequisition.DraftStatusCount = await Task.Run(() => (_db.Store_Requisition.Where(x => x.Status == (int)SRStatusEnum.Draft && !string.IsNullOrEmpty(x.CID) && x.Active).CountAsync().Result.ToString()));
                    vMStoreRequisition.SubmittedStatusCount = await Task.Run(() => (_db.Store_Requisition.Where(x => x.Status == (int)SRStatusEnum.Submitted && x.Active).CountAsync().Result.ToString()));
                    vMStoreRequisition.PrimaryStatusCount = await Task.Run(() => (_db.Store_Requisition.Where(x => x.Status == (int)SRStatusEnum.PrimaryApproved && x.Active).CountAsync().Result.ToString()));
                    vMStoreRequisition.FinalStatusCount = await Task.Run(() => (_db.Store_Requisition.Where(x => x.Status == (int)SRStatusEnum.FinalApproved && x.Active).CountAsync().Result.ToString()));
                    vMStoreRequisition.ProcurementStatusCount = await Task.Run(() => (_db.Store_Requisition.Where(x => x.Status == (int)SRStatusEnum.ProcurementApproved && x.Active).CountAsync().Result.ToString()));
                    vMStoreRequisition.PartialPOStatusCount = await Task.Run(() => (_db.Store_Requisition.Where(x => x.Status == (int)SRStatusEnum.PartialPOCreated && x.Active).CountAsync().Result.ToString()));
                    vMStoreRequisition.FullPOStatusCount = await Task.Run(() => (_db.Store_Requisition.Where(x => x.Status == (int)SRStatusEnum.FullPOCreated && x.Active).CountAsync().Result.ToString()));
                    vMStoreRequisition.InspectionStatusCount = await Task.Run(() => (_db.Store_Requisition.Where(x => x.Status == (int)SRStatusEnum.InspectionDone && x.Active).CountAsync().Result.ToString()));
                    vMStoreRequisition.GoodsReceivedStatusCount = await Task.Run(() => (_db.Store_Requisition.Where(x => x.Status == (int)SRStatusEnum.GoodsReceived && x.Active).CountAsync().Result.ToString()));
                    vMStoreRequisition.HoldStatusCount = await Task.Run(() => (_db.Store_Requisition.Where(x => x.Status == (int)SRStatusEnum.Hold && x.Active).CountAsync().Result.ToString()));


                }
            }


            //get statuswise data string.IsNullOrEmpty(x.Store_GeneralOutFK.Value.ToString()) &&
            if (model.Status == SRStatusEnum.Closed)
            {
                vMStoreRequisition.DataList = await Task.Run(() => (_db.Store_Requisition
                    .Where(x => x.Active && !string.IsNullOrEmpty(x.CID))
                    .Where(x => x.Status == (int)SRStatusEnum.Closed || x.Status == (int)SRStatusEnum.Cancel && x.Store_GeneralOutFK == model.CommonStoreFK)
                    .Select(x =>
                        new VMStoreRequisition
                        {
                            Status = (SRStatusEnum)x.Status,
                            ID = x.ID,
                            CID = x.CID,

                            Description = x.Description,
                            VMClosedByUserFK = x.ClosedByUserFK.HasValue ? (int)x.ClosedByUserFK : 0,
                            VMClosedByUserName = x.ClosedByUserFK.HasValue ? x.User_User.Name : "",
                            CancellationRemaks = x.CancellationRemaks,
                            CreationDate = x.CreationDate,
                            CreatedBy = x.User,
                            VMStoreFromFK = (int)(x.Store_GeneralOutFK == null ? 0 : x.Store_GeneralOutFK),
                            VMStoreToFK = (int)(x.Store_GeneralInFK == null ? 0 : x.Store_GeneralInFK),
                            VMStoreFromName = string.Join(", ", _db.Store_General.Include(y => y.StoreName).Where(y => y.ID == (x.Store_GeneralOutFK == null ? 0 : x.Store_GeneralOutFK)).Select(y => y.StoreName).AsEnumerable()),
                            VMStoreToName = string.Join(", ", _db.Store_General.Include(y => y.StoreName).Where(y => y.ID == (x.Store_GeneralInFK == null ? 0 : x.Store_GeneralInFK)).Select(y => y.StoreName).AsEnumerable()),
                            ItemNames = string.Join(", ", _db.Store_RequisitionSlave.Include(y => y.Common_RawItem).Where(y => y.Store_RequisitionFK == x.ID).Select(y => y.Common_RawItem.Name).AsEnumerable())


                        }).OrderByDescending(x => x.CID).ToListAsync()));
            }
            else
            {
                if (model.CommonStoreFK != 2)
                {
                    vMStoreRequisition.DataList = await Task.Run(() => (_db.Store_Requisition.Include(x => x.ClosedByUserFK)
                    .Where(x => x.Active && !string.IsNullOrEmpty(x.CID) && x.Status != (int)SRStatusEnum.Closed && x.Status != (int)SRStatusEnum.Cancel)
                    .Where(x => x.Store_GeneralInFK == model.CommonStoreFK && x.Status == (int)model.Status || (int)model.Status <= 0) //
                    .Select(x =>
                        new VMStoreRequisition
                        {
                            Status = (SRStatusEnum)x.Status,
                            ID = x.ID,
                            CID = x.CID,

                            Description = x.Description,
                            VMClosedByUserFK = x.ClosedByUserFK.HasValue ? (int)x.ClosedByUserFK : 0,
                            VMClosedByUserName = x.ClosedByUserFK.HasValue ? x.User_User.Name : "",
                            CancellationRemaks = x.CancellationRemaks,
                            CreationDate = x.CreationDate,
                            CreatedBy = x.User,
                            VMStoreFromFK = (int)(x.Store_GeneralOutFK == null ? 0 : x.Store_GeneralOutFK),
                            VMStoreToFK = (int)(x.Store_GeneralInFK == null ? 0 : x.Store_GeneralInFK),
                            VMStoreFromName = string.Join(", ", _db.Store_General.Include(y => y.StoreName)
                                .Where(y => y.ID == (x.Store_GeneralInFK == null ? 0 : x.Store_GeneralInFK)).Select(y => y.StoreName)
                                .AsEnumerable()),
                            VMStoreToName = string.Join(", ", _db.Store_General.Include(y => y.StoreName)
                                .Where(y => y.ID == (x.Store_GeneralOutFK == null ? 0 : x.Store_GeneralOutFK)).Select(y => y.StoreName)
                                .AsEnumerable()),
                            ItemNames = string.Join(", ",
                                _db.Store_RequisitionSlave.Include(y => y.Common_RawItem)
                                    .Where(y => y.Store_RequisitionFK == x.ID).Select(y => y.Common_RawItem.Name)
                                    .AsEnumerable())

                        }).OrderByDescending(x => x.CID).ToListAsync()));
                }
                else
                {
                    vMStoreRequisition.DataList = await Task.Run(() => (_db.Store_Requisition.Include(x => x.ClosedByUserFK)
                   .Where(x => x.Active && !string.IsNullOrEmpty(x.CID) && x.Status != (int)SRStatusEnum.Closed && x.Status != (int)SRStatusEnum.Cancel)
                   .Where(x => x.Status == (int)model.Status || (int)model.Status <= 0) //
                   .Select(x =>
                       new VMStoreRequisition
                       {
                           Status = (SRStatusEnum)x.Status,
                           ID = x.ID,
                           CID = x.CID,

                           Description = x.Description,
                           VMClosedByUserFK = x.ClosedByUserFK.HasValue ? (int)x.ClosedByUserFK : 0,
                           VMClosedByUserName = x.ClosedByUserFK.HasValue ? x.User_User.Name : "",
                           CancellationRemaks = x.CancellationRemaks,
                           CreationDate = x.CreationDate,
                           CreatedBy = x.User,
                           VMStoreFromFK = (int)(x.Store_GeneralOutFK == null ? 0 : x.Store_GeneralOutFK),
                           VMStoreToFK = (int)(x.Store_GeneralInFK == null ? 0 : x.Store_GeneralInFK),
                           VMStoreFromName = string.Join(", ", _db.Store_General.Include(y => y.StoreName)
                               .Where(y => y.ID == (x.Store_GeneralInFK == null ? 0 : x.Store_GeneralInFK)).Select(y => y.StoreName)
                               .AsEnumerable()),
                           VMStoreToName = string.Join(", ", _db.Store_General.Include(y => y.StoreName)
                               .Where(y => y.ID == (x.Store_GeneralOutFK == null ? 0 : x.Store_GeneralOutFK)).Select(y => y.StoreName)
                               .AsEnumerable()),
                           ItemNames = string.Join(", ",
                               _db.Store_RequisitionSlave.Include(y => y.Common_RawItem)
                                   .Where(y => y.Store_RequisitionFK == x.ID).Select(y => y.Common_RawItem.Name)
                                   .AsEnumerable())

                       }).OrderByDescending(x => x.CID).ToListAsync()));
                }
            }
            return vMStoreRequisition;
        }

        public async Task<VMStoreRequisitionSlave> SRWithSlaveListGet(VMStoreRequisitionSlave model)
        {
            VMStoreRequisitionSlave vM = new VMStoreRequisitionSlave();
            vM.DataListSlave = new List<VMStoreRequisitionSlave>();

            vM.DataListSlave = await Task.Run(() => (from SR in _db.Store_Requisition.Where(x => x.Active).Where(x => x.ID == model.Store_RequisitionFK)
                                                     join SIn in _db.Store_General on SR.Store_GeneralInFK equals SIn.ID into SIN_join
                                                     from SIn in SIN_join.DefaultIfEmpty()
                                                     join SOut in _db.Store_General on SR.Store_GeneralOutFK equals SOut.ID into SOut_join
                                                     from SOut in SOut_join.DefaultIfEmpty()
                                                     join PRS in _db.Store_RequisitionSlave.Where(x => x.Active) on SR.ID equals PRS.Store_RequisitionFK into PRS_Join
                                                     from PRS in PRS_Join.DefaultIfEmpty()
                                                     join RI in _db.Common_RawItem on PRS.Common_RawItemFK equals RI.ID into RI_Join
                                                     from RI in RI_Join.DefaultIfEmpty()
                                                     join U in _db.Common_Unit on RI.Common_UnitFK equals U.ID into U_Join
                                                     from U in U_Join.DefaultIfEmpty()
                                                     join RSC in _db.Common_RawSubCategory on RI.Common_RawSubCategoryFK equals RSC.ID into RSC_Join
                                                     from RSC in RSC_Join.DefaultIfEmpty()
                                                     join RC in _db.Common_RawCategory on RSC.Common_RawCategoryFK equals RC.ID into RC_Join
                                                     from RC in RC_Join.DefaultIfEmpty()
                                                     join E in _db.HRMS_Employee on PRS.HRMS_EmployeeFK equals E.ID into E_Join
                                                     from E in E_Join.DefaultIfEmpty()
                                                     join C_U in _db.User_User on SR.ClosedByUserFK equals C_U.ID into C_U_Join
                                                     from C_U in C_U_Join.DefaultIfEmpty()
                                                     select new VMStoreRequisitionSlave
                                                     {
                                                         CID = SR.CID,
                                                         VMStoreFromFK = (int)(SR.Store_GeneralOutFK != null ? SR.Store_GeneralOutFK : 0),
                                                         VMStoreFromName = SOut != null ? SOut.StoreName : "",
                                                         VMStoreToFK = (int)(SR.Store_GeneralInFK != null ? SR.Store_GeneralInFK : 0),
                                                         VMStoreToName = SIn != null ? SIn.StoreName : "",
                                                         CreationDate = SR.CreationDate,
                                                         Description = SR.Description,
                                                         Status = (SRStatusEnum)SR.Status,
                                                         VMClosedByUserFK = SR.ClosedByUserFK.HasValue ? (int)SR.ClosedByUserFK : 0,
                                                         VMClosedByUserName = C_U != null ? C_U.Name : "",
                                                         CancellationRemaks = SR.CancellationRemaks,

                                                         VMEmployeeFK = E != null ? E.ID : 0,
                                                         VMEmployeeName = E != null ? E.Name : "",
                                                         Store_RequisitionFK = PRS != null ? PRS.Store_RequisitionFK : SR.ID,
                                                         ID = PRS != null ? PRS.ID : 0,
                                                         DescriptionSlave = PRS != null ? PRS.Description : "",
                                                         RequisitionQuantity = PRS != null ? PRS.RequisitionQuantity : 0,
                                                         InspectionRequired = PRS != null ? PRS.InspectionRequired : false,
                                                         RequiredDate = PRS != null ? PRS.RequiredDate : DateTime.MinValue,
                                                         VMRawItemFK = RI != null ? RI.ID : 0,
                                                         VMRawItemName = RI != null ? RI.Name + " Color : " + _db.Common_Color.First(x => x.ID == PRS.Common_ColorFK).Name + "; Size : " + _db.Common_Size.First(x=>x.ID == PRS.Common_SizeFK).Name : " ",
                                                         VMRawCategoryFK = RC != null ? RC.ID : 0,
                                                         VMRawCategoryName = RC != null ? RC.Name : "",
                                                         VMRawSubCategoryFK = RSC != null ? RSC.ID : 0,
                                                         VMRawSubCategoryName = RSC != null ? RSC.Name : "",
                                                         VMUnitName = U != null ? U.Name : "",
                                                         AvailableQuantity = RI != null ? RI.Quantity : 0
                                                     }).OrderByDescending(x => x.ID).ToListAsync());

            VMStoreRequisitionSlave vMSlave = new VMStoreRequisitionSlave();

            vMSlave.Store_RequisitionFK = vM.DataListSlave[0].Store_RequisitionFK;
            vMSlave.CID = vM.DataListSlave[0].CID;
            vMSlave.CreatedBy = vM.DataListSlave[0].CreatedBy;
            vMSlave.CreationDate = vM.DataListSlave[0].CreationDate;
            vMSlave.Description = vM.DataListSlave[0].Description;
            vMSlave.Status = (SRStatusEnum)vM.DataListSlave[0].Status;
            vMSlave.VMDepartmentName = vM.DataListSlave[0].VMDepartmentName;
            vMSlave.VMClosedByUserFK = vM.DataListSlave[0].VMClosedByUserFK;
            vMSlave.VMClosedByUserName = vM.DataListSlave[0].VMClosedByUserName;
            vMSlave.CancellationRemaks = vM.DataListSlave[0].CancellationRemaks;
            vMSlave.VMStoreFromFK = vM.DataListSlave[0].VMStoreFromFK;
            vMSlave.VMStoreFromName = vM.DataListSlave[0].VMStoreFromName;
            vMSlave.VMStoreToFK = vM.DataListSlave[0].VMStoreToFK;
            vMSlave.VMStoreToName = vM.DataListSlave[0].VMStoreToName;

            vMSlave.DataListSlave = new List<VMStoreRequisitionSlave>();

            if (vM.DataListSlave.Count > 0 && vM.DataListSlave[0].ID > 0)
            {
                vMSlave.DataListSlave = vM.DataListSlave;
            }

            vMSlave.RawCategoryList = new SelectList(RawCategoryDropDownList(), "Value", "Text");
            vMSlave.EmployeeList = new SelectList(EmployeeDropDownList(), "Value", "Text");
            vMSlave.StoreFromList = await StoreDropDownListAsync();
            vMSlave.StoreToList = await StoreDropDownListAsync();
            return vMSlave;
        }

        public async Task<int> StoreRequisitionAdd(VMStoreRequisition model)
        {
            var result = -1;
            var enStore_Requisition = new Store_Requisition
            {
                CID = CidServices.GetRequisitionRefNo(model.DepartmentId),
                Status = (int)model.Status,
                CreationDate = model.CreationDate,
                Description = model.Description,
                RequisitionType = 1,
                User = model.User
            };

            try
            {
                _db.Store_Requisition.Add(enStore_Requisition);
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = enStore_Requisition.ID;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
        public async Task<int> StoreRequisitionEdit(VMStoreRequisition model)
        {
            var result = -1;
            Store_Requisition enSR = await Task.Run(() => (_db.Store_Requisition.FindAsync(model.ID)));




            if (!string.IsNullOrEmpty(model.Description))
            {
                enSR.Description = model.Description;
            }
            enSR.Store_GeneralOutFK = model.VMStoreFromFK;
            enSR.Store_GeneralInFK = model.VMStoreToFK;

            if (await _db.SaveChangesAsync() > 0)
            {
                result = enSR.ID;
            }
            return result;
        }
        public async Task<int> StoreRequisitionStatusUpdate(VMStoreRequisition model)
        {
            var result = -1;
            Store_Requisition ensr = await _db.Store_Requisition.FindAsync(model.ID);
            if (model.ActionId > 3)
            {
                if (model.SRActionEnum == SRActionEnum.Submit)
                {
                    ensr.Status = (int)SRStatusEnum.Submitted;
                }
                if (model.SRActionEnum == SRActionEnum.UndoSubmit)
                {
                    ensr.Status = (int)SRStatusEnum.Draft;
                }

                if (model.SRActionEnum == SRActionEnum.PrimaryApprove)
                {
                    ensr.Status = (int)SRStatusEnum.PrimaryApproved;
                }
                if (model.SRActionEnum == SRActionEnum.UndoPrimaryApprove)
                {
                    ensr.Status = (int)SRStatusEnum.Submitted;
                }

                if (model.SRActionEnum == SRActionEnum.FinalApprove)
                {
                    ensr.Status = (int)SRStatusEnum.FinalApproved;
                }
                if (model.SRActionEnum == SRActionEnum.UndoFinalApprove)
                {
                    ensr.Status = (int)SRStatusEnum.PrimaryApproved;
                }

                if (model.SRActionEnum == SRActionEnum.ProcurementApprove)
                {
                    ensr.Status = (int)SRStatusEnum.ProcurementApproved;
                }
                if (model.SRActionEnum == SRActionEnum.UndoProcurementApprove)
                {
                    ensr.Status = (int)SRStatusEnum.FinalApproved;
                }

                if (model.SRActionEnum == SRActionEnum.Cancel)
                {
                    ensr.Status = (int)SRStatusEnum.Cancel;
                    //assign closed by User
                    ensr.ClosedByUserFK = 1;
                }

                if (model.SRActionEnum == SRActionEnum.Hold)
                {
                    ensr.Status = (int)SRStatusEnum.Hold;
                }

                if (model.SRActionEnum == SRActionEnum.Close)
                {
                    ensr.Status = (int)SRStatusEnum.Closed;
                    //assign closed by User
                    ensr.ClosedByUserFK = 1;
                }
                if (model.SRActionEnum == SRActionEnum.ReOpen)
                {
                    ensr.Status = (int)SRStatusEnum.Draft;
                    ensr.ClosedByUserFK = null;
                }

                if (model.SRActionEnum == SRActionEnum.Finalize)
                {
                    if (string.IsNullOrEmpty(ensr.CID))
                    {
                        ensr.CID = "SR/" + DateTime.Now.ToString("yyyy") + "/" + DateTime.Now.ToString("MMMM") + "/" + model.User + "/";
                        ensr.CID = ensr.CID + await Task.Run(() => (((_db.Store_Requisition.AnyAsync().Result ? _db.Store_Requisition.MaxAsync(x => Int32.Parse(!string.IsNullOrEmpty(x.CID.Replace(ensr.CID, "")) ? x.CID.Replace(ensr.CID, "") : "0")).Result : 0) + 1).ToString().PadLeft(6, '0')));
                        //        .CID = enpr.CID + await Task.Run(() => (((_db.Procurement_PurchaseRequisition.AnyAsync().Result ? _db.Procurement_PurchaseRequisition.MaxAsync(x => Int32.Parse(!string.IsNullOrEmpty(x.CID.Replace(enpr.CID, "")) ? x.CID.Replace(enpr.CID, "") : "0")).Result : 0) + 1).ToString().PadLeft(6, '0')));
                    }

                    ensr.Store_GeneralOutFK = model.VMStoreFromFK;
                    ensr.Store_GeneralInFK = model.VMStoreToFK;
                    if (!string.IsNullOrEmpty(model.Description))
                    {
                        ensr.Description = model.Description;
                    }
                }
                if (model.SRActionEnum == SRActionEnum.UndoFinalize)
                {
                    ensr.CID = "";
                }
            }

            //else if (model.ActionEum == SRActionEnum.Close)
            //{
            //    enpr.Status = (int)SRStatusEnum.Closed;
            //}
            //else if (model.ActionEum == SRActionEnum.UnApprove)
            //{
            //    enpr.Status = (int)SRStatusEnum.Draft;
            //}
            //else if (model.ActionEum == SRActionEnum.ReOpen)
            //{
            //    enpr.Status = (int)SRStatusEnum.Draft;
            //}


            if (await _db.SaveChangesAsync() > 0)
            {
                result = ensr.ID;
            }
            return result;
        }
        public async Task<int> StoreRequisitionDelete(int id)
        {
            var result = -1;
            Store_Requisition selectSingle = await _db.Store_Requisition.FindAsync(id);
            if (selectSingle != null)
            {
                selectSingle.Active = false;
                if (await _db.SaveChangesAsync() > 0)
                {
                    await _db.Store_RequisitionSlave.Where(y => y.Store_RequisitionFK == selectSingle.ID).ForEachAsync(x => x.Active = false);

                    await _db.SaveChangesAsync();

                    result = selectSingle.ID;
                }
            }
            return result;
        }

        public async Task<int> SRSlaveAdd(VMStoreRequisitionSlave vM)
        {
            var result = -1;

            Store_RequisitionSlave pRSlave = await Task.Run(() => (_db.Store_RequisitionSlave.Where(x => x.Store_RequisitionFK == vM.Store_RequisitionFK && x.Common_RawItemFK == vM.VMRawItemFK && x.Active).FirstOrDefaultAsync()));
            if (pRSlave == null)
            {
                pRSlave = new Store_RequisitionSlave
                {
                    Store_RequisitionFK = vM.Store_RequisitionFK,
                    RequisitionQuantity = vM.RequisitionQuantity,
                    Description = vM.DescriptionSlave,
                    Common_RawItemFK = vM.VMRawItemFK,
                    HRMS_EmployeeFK = (int)(vM.VMEmployeeFK),
                    RequiredDate = vM.RequiredDate,
                    InspectionRequired = vM.InspectionRequired,
                    Common_ColorFK = vM.Common_ColorFK == 0 || vM.Common_ColorFK == null ? 43 : vM.Common_ColorFK, // 43 for  Empty Color
                };
                _db.Store_RequisitionSlave.Add(pRSlave);
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = pRSlave.ID;
                }
            }

            return result;
        }
        public async Task<int> SRSlaveEdit(VMStoreRequisitionSlave vM)
        {
            var result = -1;
            Store_RequisitionSlave pRSlave = await _db.Store_RequisitionSlave.FindAsync(vM.ID);
            if (vM.RequisitionQuantity > 0)
            {
                pRSlave.RequisitionQuantity = vM.RequisitionQuantity;
            }
            if (!string.IsNullOrEmpty(vM.DescriptionSlave))
            {
                pRSlave.Description = vM.DescriptionSlave;
            }

            if (vM.VMRawItemFK > 0)
            {
                pRSlave.Common_RawItemFK = vM.VMRawItemFK;
            }
            if (vM.VMEmployeeFK > 0)
            {
                pRSlave.HRMS_EmployeeFK = (int)vM.VMEmployeeFK;
            }

            if (vM.RequiredDate != DateTime.MinValue)
            {
                pRSlave.RequiredDate = vM.RequiredDate;
            }
            pRSlave.InspectionRequired = vM.InspectionRequired;

            if (await _db.SaveChangesAsync() > 0)
            {
                result = pRSlave.ID;
            }
            return result;
        }
        public async Task<int> SRSlaveDelete(int id)
        {
            var result = -1;
            Store_RequisitionSlave selectSingle = await _db.Store_RequisitionSlave.FindAsync(id);
            if (selectSingle != null)
            {
                selectSingle.Active = false;
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = selectSingle.ID;
                }
            }
            return result;
        }
        public async Task<VMRawItem> RawItemInfoGet(int id)
        {
            VMRawItem vM = new VMRawItem();

            Common_RawItem enRI = await Task.Run(() => (from ri in _db.Common_RawItem.Include(x => x.Common_Unit)
                                                        join rsc in _db.Common_RawSubCategory.Include(x => x.Common_RawCategory) on ri.Common_RawSubCategoryFK equals rsc.ID

                                                        select new Common_RawItem
                                                        {
                                                            ID = ri.ID,
                                                            Name = ri.Name,
                                                            Common_Unit = ri.Common_Unit,
                                                            Common_RawSubCategory = rsc,
                                                            Quantity = ri.Quantity
                                                        }
                                                        ).FirstOrDefaultAsync(x => x.ID == id));

            if (enRI != null)
            {
                vM.ID = enRI.ID;
                vM.Name = enRI.Name;
                vM.Unit = enRI.Common_Unit.Name;
                vM.Quantity = enRI.Quantity;
                vM.RawCategoryFK = enRI.Common_RawSubCategory.Common_RawCategory.ID;
                vM.RawSubCategoryFK = enRI.Common_RawSubCategory.ID;
            }
            return vM;
        }

        //public async Task<List<VMRawItem>> RawItemGet(int id)
        //{

        //    List<VMRawItem> vMRI = await Task.Run(() => (_db.Common_RawItem.Where(x => id <= 0 || x.Common_RawSubCategoryFK == id).Select(x => new VMRawItem() { ID = x.ID, Name = x.Name })).ToListAsync());


        //    return vMRI;
        //}
        #endregion
        #endregion

        public bool HasThisItem(VmStoreBin storeBin)
        {
            var data = _db.Store_Bin.Any(x =>
                x.Active && x.Row == storeBin.Row && x.Rack == storeBin.Rack && x.Section == storeBin.Section &&
                x.Store_GeneralFK == storeBin.StoreGeneralFK);
            return data;
        }

        public async Task<VmStoreAssetRegister> GetAssetRegisterByItem(VmStoreAssetRegister s)
        {
            var brandName = _db.Common_Brand.Find(s.CommonBrandFK).Name;
            var modelName = _db.Common_Model.Find(s.CommonModelFK).Name;
            var ids = (from r in _db.Store_StockRegister select r.StockInFK).Contains(28);

            var dataItems = (from sin in _db.Store_StockIn
                             join pr in _db.Procurement_PurchaseOrderSlave on sin.ItemIDFK equals pr.Common_RawItemFK
                             join i in _db.Common_RawItem on sin.ItemIDFK equals i.ID into tempItem
                             join u in _db.Common_Unit on sin.CommonUnitFK equals u.ID into tempUnit
                             from item in tempItem.DefaultIfEmpty()
                             from unit in tempUnit.DefaultIfEmpty()
                                 //where sin.Active && sin.FixAsset && !(from r in _db.Store_StockRegister select r.StockInFk).Contains(sin.ID)
                             where sin.Active && sin.FixAsset && sin.ItemIDFK == s.RawItemFK && !_db.Store_StockRegister.Any(x => x.StockInFK == sin.ID)
                             select new VmStoreAssetRegister
                             {
                                 ID = s.ID,
                                 CID = "",
                                 ItemSLNO = item.Name.ToUpper().Substring(0, 3),
                                 ItemName = item.Name + " " + brandName + " " + modelName,
                                 StockInFk = sin.ID,
                                 LotQty = sin.ReceivedQty,
                                 UnitPrice = pr.PurchasingPrice,
                                 MinPrice = pr.PurchasingPrice,
                                 User = s.User,
                                 LifeTime = 1,
                                 Remarks = s.Remarks,
                                 Status = 1,
                                 CommonDepartmentFK = 1,
                                 EmployeeFK = 1,
                                 ReceivedDate = sin.ReceivedDate,
                                 RawItemFK = s.RawItemFK,
                                 CommonBrandFK = s.CommonBrandFK,
                                 CommonModelFK = s.CommonModelFK,
                                 CommonUnitFK = unit.ID,
                                 CommonBrandName = brandName,
                                 CommonModelName = modelName,
                                 CommonUnitName = unit.Name,
                                 CommonDepreciationFK = 1
                             }).FirstOrDefault();

            var list = new List<VmStoreAssetRegister>();
            var vmStoreAsset = new VmStoreAssetRegister
            {
                DataLists = new List<VmStoreAssetRegister>()
            };
            if (dataItems == null) return vmStoreAsset;
            for (var i = 0; i < dataItems.LotQty; i++)
            {
                list.Add(dataItems);
            }
            var vmStoreAssets = new VmStoreAssetRegister
            {
                DataLists = list
            };
            return vmStoreAssets;
        }

        public async Task<VmStoreAssetRegister> GetAssetRegisterAll(int id)
        {
            var assetRegister = new VmStoreAssetRegister();
            var data = await Task.Run(() =>
            (from r in _db.Store_StockRegister
             join i in _db.Common_RawItem on r.RawItemFK equals i.ID
             join b in _db.Common_Brand on r.CommonBrandFK equals b.ID
             join m in _db.Common_Model on r.CommonModelFK equals m.ID
             join u in _db.Common_Unit on r.CommonUnitFK equals u.ID
             //join e in _db.HRMS_Employee on r.HREmployeeFK equals e.ID
             //join d in _db.User_Department on r.HREmployeeDepartmentFK equals d.ID
             where r.Active //&&  _db.Store_StockIn.Where(x => x.FixAsset)
             select new VmStoreAssetRegister
             {
                 ID = r.ID,
                 CID = r.CID,
                 ItemSLNO = r.ItemSLNO,
                 ItemName = i.Name,
                 UnitPrice = r.UnitPrice,
                 LotQty = r.LotQty,
                 LifeTime = r.LifeTime,
                 CommonUnitName = u.Name,
                 CommonBrandName = b.Name,
                 CommonModelName = m.Name,
                 DepartmentName = "Admin",//d.Name,
                 EmployeeName = "Rafiq",//e.Name,
                 Remarks = r.Remarks,
             }).ToListAsync());
            assetRegister.DataLists = data;
            return assetRegister;
        }

        public async Task<int> SaveAllRegisterItem(VmStoreAssetRegister obj, string action, string controller, int id)
        {
            var result = 0;
            var assetList = new List<Store_StockRegister>();
            foreach (var assetRegister in obj.DataLists)
            {
                var reg = new Store_StockRegister
                {
                    ID = assetRegister.ID,
                    CID = assetRegister.CID,
                    ItemSLNO = assetRegister.ItemSLNO,
                    StockInFK = assetRegister.StockInFk,
                    UnitPrice = assetRegister.UnitPrice,
                    LotQty = assetRegister.LotQty,
                    MinPrice = assetRegister.MinPrice,
                    User = assetRegister.User,
                    UserID = assetRegister.UserID,
                    LifeTime = assetRegister.LifeTime,
                    Remarks = assetRegister.Remarks,
                    Status = 1,
                    CommonStoreFK = id == 0 ? 2 : 0,
                    RawItemFK = assetRegister.RawItemFK,
                    CommonBrandFK = assetRegister.CommonBrandFK,
                    CommonModelFK = assetRegister.CommonModelFK,
                    CommonUnitFK = assetRegister.CommonUnitFK,
                    HREmployeeFK = assetRegister.EmployeeFK,
                    HREmployeeDepartmentFK = assetRegister.CommonDepartmentFK,
                    CommonDepreciationFK = assetRegister.CommonDepreciationFK,
                    HREmployeeReceivedDate = assetRegister.ReceivedDate
                };
                assetList.Add(reg);
            }
            _db.Store_StockRegister.AddRange(assetList);
            result = await _db.SaveChangesAsync();
            return result;
        }

        public async Task<int> StoreOutPersonalSave(VmStoreOutPersonal objPersonal, string register, string store)
        {
            var reg = new Store_StockOutPersonal()
            {
                ID = objPersonal.ID,
                CID = objPersonal.CID,
                User = "User One",
                ReceivedDate = objPersonal.ReceivedDate,
                Remarks = objPersonal.Remarks,
                RawItemFK = objPersonal.RawItemFK,
                CommonBrandFK = objPersonal.CommonBrandFK,
                CommonModelFK = objPersonal.CommonModelFK,
                StoreGeneralFK = objPersonal.StoreGeneralFK,
                CommonDepartmentFK = objPersonal.CommonDepartmentFK,
                EmployeeFK = objPersonal.EmployeeFK,
                ReceivedQty = objPersonal.ReceivedQty,
                CommonUnitFK = objPersonal.CommonUnitFK,
                CommonAssetFK = objPersonal.CommonAssetFK,
                IssueReturnStatus = (int)ItemStatusEnum.Working
            };
            _db.Store_StockOutPersonal.Add(reg);
            var result = await _db.SaveChangesAsync();
            return result;
        }

        public async Task<VmStoreOutPersonal> GetStoreOutPersonal()
        {
            var vmStoreOutPersonal = new VmStoreOutPersonal();
            var data = await Task.Run(() =>
                (from r in _db.Store_StockOutPersonal
                 join i in _db.Common_RawItem on r.RawItemFK equals i.ID
                 join b in _db.Common_Brand on r.CommonBrandFK equals b.ID
                 join m in _db.Common_Model on r.CommonModelFK equals m.ID
                 join u in _db.Common_Unit on r.CommonUnitFK equals u.ID
                 join e in _db.HRMS_Employee on r.EmployeeFK equals e.ID
                 join d in _db.User_Department on r.CommonDepartmentFK equals d.ID
                 join s in _db.Store_General on r.StoreGeneralFK equals s.ID
                 where r.Active && r.IssueReturnStatus == (int)ItemStatusEnum.Working
                 select new VmStoreOutPersonal()
                 {
                     ID = r.ID,
                     CID = r.CID,
                     User = r.User,
                     Remarks = r.Remarks,
                     ReceivedQty = r.ReceivedQty,
                     ReceivedDate = r.ReceivedDate,
                     Name = e.Name,
                     ItemName = i.Name,
                     BrandName = b.Name,
                     ModelName = m.Name,
                     UnitName = u.Name,
                     EmployeeName = e.Name,
                     StoreName = s.StoreName,
                     DepartmentName = d.Name,

                     RawItemFK = i.ID,
                     CommonBrandFK = b.ID,
                     CommonModelFK = m.ID,
                     CommonUnitFK = u.ID,
                     StoreGeneralFK = s.ID,
                     CommonDepartmentFK = d.ID,
                     EmployeeFK = e.ID

                 }).OrderByDescending(x => x.ID).ToListAsync());
            vmStoreOutPersonal.DataLists = data;
            return vmStoreOutPersonal;
        }

        public async Task<int> StoreInPersonalSave(VmStoreOutPersonal objPersonal, string register, string store)
        {
            var reg = new Store_StockOutPersonal()
            {
                ID = objPersonal.ID,
                CID = objPersonal.CID,
                User = "User One",
                ReceivedDate = objPersonal.ReceivedDate,
                Remarks = objPersonal.Remarks,
                RawItemFK = objPersonal.RawItemFK,
                CommonBrandFK = objPersonal.CommonBrandFK,
                CommonModelFK = objPersonal.CommonModelFK,
                StoreGeneralFK = objPersonal.StoreGeneralFK,
                CommonDepartmentFK = objPersonal.CommonDepartmentFK,
                EmployeeFK = objPersonal.EmployeeFK,
                ReceivedQty = objPersonal.ReceivedQty,
                CommonUnitFK = objPersonal.CommonUnitFK,
                CommonAssetFK = objPersonal.CommonAssetFK,
                IssueReturnStatus = (int)ItemStatusEnum.Return
            };
            _db.Store_StockOutPersonal.Add(reg);
            var result = await _db.SaveChangesAsync();
            return result;
        }

        public async Task<VmStoreOutPersonal> GetStoreInPersonal()
        {
            var vmStoreOutPersonal = new VmStoreOutPersonal();
            var data = await Task.Run(() =>
                (from r in _db.Store_StockOutPersonal
                 join i in _db.Common_RawItem on r.RawItemFK equals i.ID
                 join b in _db.Common_Brand on r.CommonBrandFK equals b.ID
                 join m in _db.Common_Model on r.CommonModelFK equals m.ID
                 join u in _db.Common_Unit on r.CommonUnitFK equals u.ID
                 join e in _db.HRMS_Employee on r.EmployeeFK equals e.ID
                 join d in _db.User_Department on r.CommonDepartmentFK equals d.ID
                 join s in _db.Store_General on r.StoreGeneralFK equals s.ID
                 where r.Active && r.IssueReturnStatus == (int)ItemStatusEnum.Return
                 select new VmStoreOutPersonal
                 {
                     ID = r.ID,
                     CID = r.CID,
                     User = r.User,
                     Remarks = r.Remarks,
                     ReceivedQty = r.ReceivedQty,
                     ReceivedDate = r.ReceivedDate,
                     Name = e.Name,
                     ItemName = i.Name,
                     BrandName = b.Name,
                     ModelName = m.Name,
                     UnitName = u.Name,
                     EmployeeName = e.Name,
                     StoreName = s.StoreName,
                     DepartmentName = d.Name,

                     RawItemFK = i.ID,
                     CommonBrandFK = b.ID,
                     CommonModelFK = m.ID,
                     CommonUnitFK = u.ID,
                     StoreGeneralFK = s.ID,
                     CommonDepartmentFK = d.ID,
                     EmployeeFK = e.ID

                 }).OrderByDescending(x => x.ID).ToListAsync());
            vmStoreOutPersonal.DataLists = data;
            return vmStoreOutPersonal;
        }

        public async Task<int> SRProductionSlaveAdd(VMStoreRequisitionSlave vM)
        {
            var result = -1;

            var pRSlave = await Task.Run(() => (
                _db.Store_RequisitionSlave.Where(x => x.Store_RequisitionFK == vM.Store_RequisitionFK
                                    && x.Common_RawItemFK == vM.VMRawItemFK && x.Active).FirstOrDefaultAsync()));
            if (pRSlave != null) return result;
            pRSlave = new Store_RequisitionSlave
            {
                Store_RequisitionFK = vM.Store_RequisitionFK,
                RequisitionQuantity = vM.RequisitionQuantity,
                Description = vM.DescriptionSlave,
                Common_RawItemFK = vM.VMRawItemFK,
                HRMS_EmployeeFK = (int)vM.VMEmployeeFK,
                RequiredDate = vM.RequiredDate,
                InspectionRequired = vM.InspectionRequired

            };
            _db.Store_RequisitionSlave.Add(pRSlave);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = pRSlave.ID;
            }
            return result;
        }
        public async Task<int> SRProductionSlaveEdit(VMStoreRequisitionSlave vM)
        {
            var result = -1;
            Store_RequisitionSlave pRSlave = await _db.Store_RequisitionSlave.FindAsync(vM.ID);
            if (vM.RequisitionQuantity > 0)
            {
                pRSlave.RequisitionQuantity = vM.RequisitionQuantity;
            }
            if (!string.IsNullOrEmpty(vM.DescriptionSlave))
            {
                pRSlave.Description = vM.DescriptionSlave;
            }

            if (vM.VMRawItemFK > 0)
            {
                pRSlave.Common_RawItemFK = vM.VMRawItemFK;
            }
            if (vM.VMEmployeeFK > 0)
            {
                pRSlave.HRMS_EmployeeFK = (int)vM.VMEmployeeFK;
            }

            if (vM.RequiredDate != DateTime.MinValue)
            {
                pRSlave.RequiredDate = vM.RequiredDate;
            }
            pRSlave.InspectionRequired = vM.InspectionRequired;

            if (await _db.SaveChangesAsync() > 0)
            {
                result = pRSlave.ID;
            }
            return result;
        }
        public async Task<int> SRProductionSlaveDelete(int id)
        {
            var result = -1;
            Store_RequisitionSlave selectSingle = await _db.Store_RequisitionSlave.FindAsync(id);
            if (selectSingle != null)
            {
                selectSingle.Active = false;
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = selectSingle.ID;
                }
            }
            return result;
        }

        public async Task<VMStoreRequisitionSlave> SRProductionWithSlaveListGet(VMStoreRequisitionSlave model)
        {

            VMStoreRequisitionSlave vM = new VMStoreRequisitionSlave();
            vM.DataListSlave = new List<VMStoreRequisitionSlave>();

            vM.DataListSlave = await Task.Run(() => (from SR in _db.Store_Requisition
                                                     .Where(x => x.Active)
                                                    .Where(x => x.ID == (int)model.Store_RequisitionFK)
                                                     join SIn in _db.Store_General on SR.Store_GeneralInFK equals SIn.ID into SIN_join
                                                     from SIn in SIN_join.DefaultIfEmpty()
                                                     join SOut in _db.Store_General on SR.Store_GeneralOutFK equals SOut.ID into SOut_join
                                                     from SOut in SOut_join.DefaultIfEmpty()
                                                     join PRS in _db.Store_RequisitionSlave.Where(x => x.Active) on SR.ID equals PRS.Store_RequisitionFK into PRS_Join
                                                     from PRS in PRS_Join.DefaultIfEmpty()
                                                     join RI in _db.Common_RawItem on PRS.Common_RawItemFK equals RI.ID into RI_Join
                                                     from RI in RI_Join.DefaultIfEmpty()
                                                     join U in _db.Common_Unit on RI.Common_UnitFK equals U.ID into U_Join
                                                     from U in U_Join.DefaultIfEmpty()
                                                     join RSC in _db.Common_RawSubCategory on RI.Common_RawSubCategoryFK equals RSC.ID into RSC_Join
                                                     from RSC in RSC_Join.DefaultIfEmpty()
                                                     join RC in _db.Common_RawCategory on RSC.Common_RawCategoryFK equals RC.ID into RC_Join
                                                     from RC in RC_Join.DefaultIfEmpty()
                                                     join E in _db.HRMS_Employee on PRS.HRMS_EmployeeFK equals E.ID into E_Join
                                                     from E in E_Join.DefaultIfEmpty()
                                                     join C_U in _db.User_User on SR.ClosedByUserFK equals C_U.ID into C_U_Join
                                                     from C_U in C_U_Join.DefaultIfEmpty()
                                                     select new VMStoreRequisitionSlave
                                                     {
                                                         CID = SR.CID,


                                                         VMStoreFromFK = (int)(SR.Store_GeneralOutFK != 0 ? SR.Store_GeneralOutFK : 0),
                                                         VMStoreFromName = SOut != null ? SOut.StoreName : "",
                                                         VMStoreToFK = (int)(SR.Store_GeneralInFK != 0 ? SR.Store_GeneralInFK : 0),
                                                         VMStoreToName = SIn != null ? SIn.StoreName : "",
                                                         CreationDate = SR.CreationDate,
                                                         Description = SR.Description,
                                                         Status = (SRStatusEnum)SR.Status,
                                                         VMClosedByUserFK = SR.ClosedByUserFK.HasValue ? (int)SR.ClosedByUserFK : 0,
                                                         VMClosedByUserName = C_U != null ? C_U.Name : "",
                                                         CancellationRemaks = SR.CancellationRemaks,

                                                         VMEmployeeFK = E != null ? E.ID : 0,
                                                         VMEmployeeName = E != null ? E.Name : "",
                                                         Store_RequisitionFK = PRS != null ? PRS.Store_RequisitionFK : SR.ID,
                                                         ID = PRS != null ? PRS.ID : 0,
                                                         DescriptionSlave = PRS != null ? PRS.Description : "",
                                                         RequisitionQuantity = PRS != null ? PRS.RequisitionQuantity : 0,
                                                         InspectionRequired = PRS != null ? PRS.InspectionRequired : false,
                                                         RequiredDate = PRS != null ? PRS.RequiredDate : DateTime.MinValue,
                                                         VMRawItemFK = RI != null ? RI.ID : 0,
                                                         VMRawItemName = RI != null ? RI.Name + " ( Color :" + _db.Common_Color.First(x => x.ID == PRS.Common_ColorFK).Name + "; Size :" + PRS.Common_Size + " mm)" : "",
                                                         VMRawCategoryFK = RC != null ? RC.ID : 0,
                                                         VMRawCategoryName = RC != null ? RC.Name : "",
                                                         VMRawSubCategoryFK = RSC != null ? RSC.ID : 0,
                                                         VMRawSubCategoryName = RSC != null ? RSC.Name : "",
                                                         VMUnitName = U != null ? U.Name : "",
                                                         AvailableQuantity = RI != null ? RI.Quantity : 0
                                                     }).OrderByDescending(x => x.ID).ToListAsync());

            VMStoreRequisitionSlave vMSlave = new VMStoreRequisitionSlave();

            vMSlave.Store_RequisitionFK = vM.DataListSlave[0].Store_RequisitionFK;
            vMSlave.CID = vM.DataListSlave[0].CID;
            vMSlave.CreatedBy = vM.DataListSlave[0].CreatedBy;
            vMSlave.CreationDate = vM.DataListSlave[0].CreationDate;
            vMSlave.Description = vM.DataListSlave[0].Description;
            vMSlave.Status = (SRStatusEnum)vM.DataListSlave[0].Status;
            vMSlave.VMDepartmentName = vM.DataListSlave[0].VMDepartmentName;
            vMSlave.VMClosedByUserFK = vM.DataListSlave[0].VMClosedByUserFK;
            vMSlave.VMClosedByUserName = vM.DataListSlave[0].VMClosedByUserName;
            vMSlave.CancellationRemaks = vM.DataListSlave[0].CancellationRemaks;
            vMSlave.VMStoreFromFK = vM.DataListSlave[0].VMStoreFromFK;
            vMSlave.VMStoreFromName = vM.DataListSlave[0].VMStoreFromName;
            vMSlave.VMStoreToFK = vM.DataListSlave[0].VMStoreToFK;
            vMSlave.VMStoreToName = vM.DataListSlave[0].VMStoreToName;

            vMSlave.DataListSlave = new List<VMStoreRequisitionSlave>();

            if (vM.DataListSlave.Count > 0 && vM.DataListSlave[0].ID > 0)
            {
                vMSlave.DataListSlave = vM.DataListSlave;
            }

            vMSlave.RawCategoryList = new Microsoft.AspNetCore.Mvc.Rendering.SelectList(RawCategoryDropDownList(), "Value", "Text");
            vMSlave.EmployeeList = new Microsoft.AspNetCore.Mvc.Rendering.SelectList(EmployeeDropDownList(), "Value", "Text");
            vMSlave.StoreFromList = await StoreDropDownListAsync();
            vMSlave.StoreToList = await StoreDropDownListAsync();
            return vMSlave;
        }

        /// integration        
        public List<object> OrderCategoryDropDownList()
        {
            return integrationservice.OrderCategoryDropDownList();
        }
        public List<object> OrderRawSubCategoryGet(int id)
        {
            return integrationservice.OrderSubCategoryDropDownList(id);
        }
        public List<object> OrderRawItemGet(int id)
        {
            return integrationservice.OrderRawItemInfoGet(id);
        }

        public async Task<List<VMRawSubCategory>> RawSubCategoryGet(int id)
        {
            var vMRSC = await Task.Run(() => (_db.Common_RawSubCategory.Where(x => id <= 0 || x.Common_RawCategoryFK == id)).Select(x => new VMRawSubCategory() { ID = x.ID, Name = x.Name }).ToListAsync());
            return vMRSC;
        }
        public async Task<List<VMRawItem>> RawItemGet(int id)
        {
            var vMRI = await Task.Run(() => (_db.Common_RawItem.Where(x => id <= 0 || x.Common_RawSubCategoryFK == id)
            .Select(x => new VMRawItem() { ID = x.ID, Name = x.Name })).ToListAsync());
            return vMRI;
        }

        public bool GetChalllanNoByChallanID(string id)
        {
            return _db.Store_StockIn.Any(x => x.ChallanNo == id);
        }
        public async Task<int> ProductionStoreRequisitionAdd(VMStoreRequisition model)
        {
            var result = -1;
            Store_Requisition enStore_Requisition = new Store_Requisition
            {
                CID = CidServices.GetRequisitionRefNo("C"), //await Task.Run(() => (((_db.Store_Requisition.AnyAsync().Result ? _db.Store_Requisition.MaxAsync(x => x.ID).Result : 0) + 1).ToString().PadLeft(5, '0'))),
                Status = (int)model.Status,
                CreationDate = model.CreationDate,
                Description = model.Description,
                RequisitionType = 1,
                //Merchandising_StyleFK = model.StyleId,
                User = model.User,
                Store_GeneralInFK = model.VMStoreToFK,
                Store_GeneralOutFK = model.VMStoreFromFK
            };

            try
            {
                _db.Store_Requisition.Add(enStore_Requisition);
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = enStore_Requisition.ID;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
        public async Task<int> ProductionStoreRequisitionEdit(VMStoreRequisition model)
        {
            var result = -1;
            Store_Requisition enSR = await Task.Run(() => (_db.Store_Requisition.FindAsync(model.ID)));
            if (!string.IsNullOrEmpty(model.Description))
            {
                enSR.Description = model.Description;
            }
            enSR.Store_GeneralOutFK = model.VMStoreFromFK;
            enSR.Store_GeneralInFK = model.VMStoreToFK;

            if (await _db.SaveChangesAsync() > 0)
            {
                result = enSR.ID;
            }
            return result;
        }
        public async Task<int> ProductionStoreRequisitionStatusUpdate(VMStoreRequisition model)
        {
            var result = -1;
            Store_Requisition ensr = await _db.Store_Requisition.FindAsync(model.ID);
            if (model.ActionId > 3)
            {
                if (model.SRActionEnum == SRActionEnum.Submit)
                {
                    ensr.Status = (int)SRStatusEnum.Submitted;
                }
                if (model.SRActionEnum == SRActionEnum.UndoSubmit)
                {
                    ensr.Status = (int)SRStatusEnum.Draft;
                }

                if (model.SRActionEnum == SRActionEnum.PrimaryApprove)
                {
                    ensr.Status = (int)SRStatusEnum.PrimaryApproved;
                }
                if (model.SRActionEnum == SRActionEnum.UndoPrimaryApprove)
                {
                    ensr.Status = (int)SRStatusEnum.Submitted;
                }

                if (model.SRActionEnum == SRActionEnum.FinalApprove)
                {
                    ensr.Status = (int)SRStatusEnum.FinalApproved;
                }
                if (model.SRActionEnum == SRActionEnum.UndoFinalApprove)
                {
                    ensr.Status = (int)SRStatusEnum.PrimaryApproved;
                }

                if (model.SRActionEnum == SRActionEnum.ProcurementApprove)
                {
                    ensr.Status = (int)SRStatusEnum.ProcurementApproved;
                }
                if (model.SRActionEnum == SRActionEnum.UndoProcurementApprove)
                {
                    ensr.Status = (int)SRStatusEnum.FinalApproved;
                }

                if (model.SRActionEnum == SRActionEnum.Cancel)
                {
                    ensr.Status = (int)SRStatusEnum.Cancel;
                    //assign closed by User
                    ensr.ClosedByUserFK = 1;
                }

                if (model.SRActionEnum == SRActionEnum.Hold)
                {
                    ensr.Status = (int)SRStatusEnum.Hold;
                }

                if (model.SRActionEnum == SRActionEnum.Close)
                {
                    ensr.Status = (int)SRStatusEnum.Closed;
                    //assign closed by User
                    ensr.ClosedByUserFK = 1;
                }
                if (model.SRActionEnum == SRActionEnum.ReOpen)
                {
                    ensr.Status = (int)SRStatusEnum.Draft;
                    ensr.ClosedByUserFK = null;
                }

                if (model.SRActionEnum == SRActionEnum.Finalize)
                {
                    if (string.IsNullOrEmpty(ensr.CID))
                    {
                        ensr.CID = "SR/" + DateTime.Now.ToString("yyyy") + "/" + DateTime.Now.ToString("MMMM") + "/" + model.User + "/";
                        ensr.CID = ensr.CID + await Task.Run(() => (((_db.Store_Requisition.AnyAsync().Result ? _db.Store_Requisition.MaxAsync(x => Int32.Parse(!string.IsNullOrEmpty(x.CID.Replace(ensr.CID, "")) ? x.CID.Replace(ensr.CID, "") : "0")).Result : 0) + 1).ToString().PadLeft(6, '0')));
                        //        .CID = enpr.CID + await Task.Run(() => (((_db.Procurement_PurchaseRequisition.AnyAsync().Result ? _db.Procurement_PurchaseRequisition.MaxAsync(x => Int32.Parse(!string.IsNullOrEmpty(x.CID.Replace(enpr.CID, "")) ? x.CID.Replace(enpr.CID, "") : "0")).Result : 0) + 1).ToString().PadLeft(6, '0')));
                    }

                    ensr.Store_GeneralOutFK = model.VMStoreFromFK;
                    ensr.Store_GeneralInFK = model.VMStoreToFK;
                    if (!string.IsNullOrEmpty(model.Description))
                    {
                        ensr.Description = model.Description;
                    }
                }
                if (model.SRActionEnum == SRActionEnum.UndoFinalize)
                {
                    ensr.CID = "";
                }
            }

            //else if (model.ActionEum == SRActionEnum.Close)
            //{
            //    enpr.Status = (int)SRStatusEnum.Closed;
            //}
            //else if (model.ActionEum == SRActionEnum.UnApprove)
            //{
            //    enpr.Status = (int)SRStatusEnum.Draft;
            //}
            //else if (model.ActionEum == SRActionEnum.ReOpen)
            //{
            //    enpr.Status = (int)SRStatusEnum.Draft;
            //}


            if (await _db.SaveChangesAsync() > 0)
            {
                result = ensr.ID;
            }
            return result;
        }
        public async Task<int> ProductionStoreRequisitionDelete(int id)
        {
            var result = -1;
            Store_Requisition selectSingle = await _db.Store_Requisition.FindAsync(id);
            if (selectSingle != null)
            {
                selectSingle.Active = false;
                if (await _db.SaveChangesAsync() > 0)
                {
                    await _db.Store_RequisitionSlave.Where(y => y.Store_RequisitionFK == selectSingle.ID).ForEachAsync(x => x.Active = false);

                    await _db.SaveChangesAsync();

                    result = selectSingle.ID;
                }
            }
            return result;
        }

        public List<object> SRStyleDropDownList()
        {
            var list = new List<object>();
            var data = (from r in _db.Store_Requisition
                        join sr in _db.Store_RequisitionSlave on r.ID equals sr.Store_RequisitionFK
                        join sin in _db.Store_StockIn on sr.Common_RawItemFK equals sin.ItemIDFK
                        join ss in _db.Merchandising_StyleSlave on sr.Common_RawItemFK equals ss.Common_RawItemFK
                        join s in _db.Merchandising_Style on ss.Merchandising_StyleFK equals s.ID
                        where r.RequisitionType == 1 && sr.RequisitionQuantity > 0 && r.Active
                        group s by s
                into g
                        select new
                        {
                            StyleId = g.Key.ID,
                            StyleName = g.Key.StyleName
                        }).ToList();
            data.ForEach(x => list.Add(new { Value = x.StyleId, Text = x.StyleName }));
            return list;
        }

        public List<object> ApprovedStyleList()
        {
            return integrationservice.DDLBOMStyleList();
        }
        public async Task<SelectList> StoreDropDownListAsync()
        {
            return new SelectList(
                await _db.Store_General.Where(x => x.Active).Select(x => new { Value = x.ID, Text = x.StoreName })
                    .ToListAsync(), "Value", "Text");
        }
        public VMStoreRequisitionSlave PRSlaveFromStyleSlaveGet(int id, int PRID)
        {
            var vM = new VMStoreRequisitionSlave
            {
                DataListSlave = new List<VMStoreRequisitionSlave>()
            };
            vM.DataListSlave = integrationservice.BOMGetByStyleID(id).Where(x => x.RequiredQty > 0).Select(x => new VMStoreRequisitionSlave()
            {
                //Procurement_PurchaseRequisitionFK = PRID,
                //VMMerchandising_StyleSlaveID = x.ID,
                VMRawItemFK = x.Common_RawItemFK,
                VMRawItemName = x.RawItem,
                EmployeeList = new SelectList(EmployeeDropDownList(), "Value", "Text"),
                AvailableQuantity = (double)x.RequiredQty,
                RequisitionQuantity = 0,
                VMUnitName = x.UnitName,
                VMRawCategoryName = x.RawCategory
            }).ToList();

            //vM.DataListSlave = vM.DataListSlave.Where(x => x.RequisitionQuantity > 0).ToList();
            return vM;
        }

        public VMStoreRequisitionSlave GetSRSlaveFromStyleSlave(int id, int prid, int id2)
        {
            var vM = new VMStoreRequisitionSlave
            {
                DataListSlave = new List<VMStoreRequisitionSlave>()
            };

            int pofk = _db.Procurement_PurchaseOrderSlave.FirstOrDefault(x => x.Merchandising_StyleID == id).Procurement_PurchaseOrderFK;
            prid = _db.Procurement_PurchaseOrder.FirstOrDefault(x => x.ID == pofk).POType;
            vM.DataListSlave = integrationservice.BOMGetByStyleID(id, prid).Where(x => x.RequiredQty > 0).Select(x =>
                new VMStoreRequisitionSlave()
                {
                    Procurement_PurchaseRequisitionFK = prid,
                    Merchandising_StyleSlaveFK = x.ID,
                    Merchandising_StyleFK = id,
                    Common_ColorFK = x.Common_ColorFK == null || x.Common_ColorFK == 0 ? 43 : x.Common_ColorFK, // 43 Empty Color
                    Common_Size = x.GSM,

                    VMRawItemFK = x.Common_RawItemFK,
                    VMRawItemName = x.RawItem + " ( Color :" + x.Color + "; Size :" + x.GSM + " )",
                    DescriptionSlave = x.Description,
                    EmployeeList = new SelectList(EmployeeDropDownList(), "Value", "Text"),
                    AvailableQuantity = (double)x.RequiredQty - (double)_db.Store_RequisitionSlave.Where(m => m.Merchandising_StyleSlaveFK == x.ID).Sum(s => s.RequisitionQuantity),

                    RequisitionQuantity = (double)x.RequiredQty,
                    VMUnitName = x.UnitName,
                    VMRawCategoryName = x.RawCategory

                }).ToList();

            vM.DataListSlave = vM.DataListSlave.Where(x => x.AvailableQuantity > 0).ToList();
            return vM;
        }
        public VMStoreRequisitionSlave GetSRSlaveFromFabricStyleSlave(int id, int prid, int id2, int itemTypeId)
        {
            var vM = new VMStoreRequisitionSlave
            {
                DataListSlave = new List<VMStoreRequisitionSlave>()
            };

            int pofk = _db.Procurement_PurchaseOrderSlave.FirstOrDefault(x => x.Merchandising_StyleID == id).Procurement_PurchaseOrderFK;
            var TypeID = _db.Procurement_PurchaseOrder.FirstOrDefault(x => x.ID == pofk).POType; //&& x.IsFabric

            vM.DataListSlave = integrationservice.BOMGetByStyleID(id, TypeID).Where(x => x.RequiredQty > 0
            && x.RawCategory == "Fabric").Select(x =>
               new VMStoreRequisitionSlave()
               {
                   Procurement_PurchaseRequisitionFK = prid,
                   Merchandising_StyleSlaveFK = x.ID,
                   Merchandising_StyleFK = id,
                   Common_ColorFK = x.Common_ColorFK == null || x.Common_ColorFK == 0 ? 43 : x.Common_ColorFK, // 43 Empty Color
                   Common_Size = x.GSM,

                   VMRawItemFK = x.Common_RawItemFK,
                   VMRawItemName = x.RawItem + " ( Color :" + x.Color + "; GSM :" + Math.Round(x.GSM) + " )",
                   DescriptionSlave = x.Description,
                   EmployeeList = new SelectList(EmployeeDropDownList(), "Value", "Text"),
                   AvailableQuantity = (double)x.RequiredQty, //- (double)_db.Store_RequisitionSlave.Where(m => m.Merchandising_StyleSlaveFK == x.ID).Sum(s => s.RequisitionQuantity),
                   RequisitionQuantity = (double)x.RequiredQty,
                   VMUnitName = x.UnitName,
                   VMRawCategoryName = x.RawCategory
               }).ToList();

            vM.DataListSlave = vM.DataListSlave.Where(x => x.AvailableQuantity > 0).ToList();
            return vM;
        }
        public VMStoreRequisitionSlave GetSRSlaveFromFabricStyleSlave(int id, int prid, int id2, int itemTypeId, int subSetId)
        {
            var vM = new VMStoreRequisitionSlave
            {
                DataListSlave = new List<VMStoreRequisitionSlave>()
            };

            int pofk = _db.Procurement_PurchaseOrderSlave.FirstOrDefault(x => x.Merchandising_StyleID == id).Procurement_PurchaseOrderFK;
            var TypeID = _db.Procurement_PurchaseOrder.FirstOrDefault(x => x.ID == pofk).POType;

            vM.DataListSlave = integrationservice.BOMGetByStyleID(id, TypeID).Where(x => x.RequiredQty > 0
            && x.IsFabric && x.RawCategory == "Fabric" && x.Common_ColorFK == subSetId).Select(x =>
               new VMStoreRequisitionSlave()
               {
                   Procurement_PurchaseRequisitionFK = prid,
                   Merchandising_StyleSlaveFK = x.ID,
                   Merchandising_StyleFK = id,
                   Common_ColorFK = x.Common_ColorFK == null || x.Common_ColorFK == 0 ? 43 : x.Common_ColorFK, // 43 Empty Color
                   Common_Size = x.GSM,

                   VMRawItemFK = x.Common_RawItemFK,
                   VMRawItemName = x.RawItem + " ( Color :" + x.Color + "; GSM :" + Math.Round(x.GSM) + " )",
                   DescriptionSlave = x.Description,
                   EmployeeList = new SelectList(EmployeeDropDownList(), "Value", "Text"),
                   AvailableQuantity = (double)x.RequiredQty, //- (double)_db.Store_RequisitionSlave.Where(m => m.Merchandising_StyleSlaveFK == x.ID).Sum(s => s.RequisitionQuantity),
                   RequisitionQuantity = (double)x.RequiredQty,
                   VMUnitName = x.UnitName,
                   VMRawCategoryName = x.RawCategory,
                   //Merchandising_StyleSetPackFK = x.Merchandising_StyleSetPackFK
               }).ToList();

            vM.DataListSlave = vM.DataListSlave.Where(x => x.AvailableQuantity > 0).ToList();
            return vM;
        }
        public VMStoreRequisitionSlave GetSRSlaveFromTrimsStyleSlave(int id, int prid, int id2, int rawCategoryId)
        {
            var vM = new VMStoreRequisitionSlave
            {
                DataListSlave = new List<VMStoreRequisitionSlave>()
            };

            int pofk = _db.Procurement_PurchaseOrderSlave.FirstOrDefault(x => x.Merchandising_StyleID == id).Procurement_PurchaseOrderFK; //&& x.Common_RawSubCategoryFK == 25
            prid = _db.Procurement_PurchaseOrder.FirstOrDefault(x => x.ID == pofk).POType;
            vM.DataListSlave = integrationservice.BOMGetByStyleID(id, prid).Where(x => x.RequiredQty > 0 && x.Common_RawCategoryFK == rawCategoryId).Select(x =>
                     new VMStoreRequisitionSlave()
                     {
                         Procurement_PurchaseRequisitionFK = prid,
                         Merchandising_StyleSlaveFK = x.ID,
                         Merchandising_StyleFK = id,
                         Common_ColorFK = x.Common_ColorFK == null || x.Common_ColorFK == 0 ? 43 : x.Common_ColorFK, // 43 Empty Color
                         Common_Size = x.GSM,

                         VMRawItemFK = x.Common_RawItemFK,
                         VMRawItemName = x.RawItem + " Color-" + x.Color + "; Size-" + Math.Round(x.GSM),
                         DescriptionSlave = x.Description,
                         EmployeeList = new SelectList(EmployeeDropDownList(), "Value", "Text"),

                         AvailableQuantity = (double)x.RequiredQty - (double)_db.Store_RequisitionSlave.Where(m => m.Merchandising_StyleSlaveFK == x.ID).Sum(s => s.RequisitionQuantity),
                         BOMQuantity = (double)x.RequiredQty,
                         RequisitionQuantity = (double)x.RequiredQty - (double)_db.Store_RequisitionSlave.Where(m => m.Merchandising_StyleSlaveFK == x.ID).Sum(s => s.RequisitionQuantity),

                         VMUnitName = x.UnitName,
                         VMRawCategoryName = x.RawCategory

                     }).ToList();

            if (vM.DataListSlave.Any())
            {
                vM.DataListSlave.ForEach(x =>
                {
                    if (x.AvailableQuantity < 0)
                    {
                        x.AvailableQuantity = 0;
                        x.RequisitionQuantity = 0;
                    };
                });
            }
            return vM;
        }
        public void GetPurchaseOrderQtyByStyleId()
        {
            var data = (from pos in _db.Procurement_PurchaseOrderSlave
                        join po in _db.Procurement_PurchaseOrder on pos.Procurement_PurchaseOrderFK equals po.ID
                        join t2 in _db.Common_RawItem on pos.Common_RawItemFK equals t2.ID
                        join t3 in _db.Common_RawSubCategory on t2.Common_RawSubCategoryFK equals t3.ID
                        join t4 in _db.Common_RawCategory on t3.Common_RawCategoryFK equals t4.ID
                        join t6 in _db.Common_Unit on t2.Common_UnitFK equals t6.ID
                        where po.Status == 5 && po.Active && pos.Active && pos.Merchandising_StyleID == 25
                        select new VMStoreRequisitionSlave
                        {
                            ID = pos.ID,
                            Common_RawItemFK = pos.Common_RawItemFK,
                        }).ToList();

        }

        public VMStoreRequisitionSlave GetSRSlaveFromTrimsPackingStyleSlave(int id, int prid, int id2, int itemTypeId)
        {
            var vM = new VMStoreRequisitionSlave
            {
                DataListSlave = new List<VMStoreRequisitionSlave>()
            };

            int pofk = _db.Procurement_PurchaseOrderSlave.FirstOrDefault(x => x.Merchandising_StyleID == id).Procurement_PurchaseOrderFK; //&& x.Common_RawSubCategoryFK != 25
            prid = _db.Procurement_PurchaseOrder.FirstOrDefault(x => x.ID == pofk).POType;
            vM.DataListSlave = integrationservice.BOMGetByStyleID(id, prid).Where(x => x.RequiredQty > 0 && x.Common_RawCategoryFK == itemTypeId).Select(x =>
                     new VMStoreRequisitionSlave()
                     {
                         Procurement_PurchaseRequisitionFK = prid,
                         Merchandising_StyleSlaveFK = x.ID,
                         Merchandising_StyleFK = id,
                         Common_ColorFK = x.Common_ColorFK == null || x.Common_ColorFK == 0 ? 43 : x.Common_ColorFK, // 43 Empty Color
                         Common_Size = x.GSM,

                         VMRawItemFK = x.Common_RawItemFK,
                         VMRawItemName = x.RawItem + " ( Color :" + x.Color + "; Size :" + Math.Round(x.GSM) + " )",
                         DescriptionSlave = x.Description,
                         EmployeeList = new SelectList(EmployeeDropDownList(), "Value", "Text"),
                         AvailableQuantity = (double)x.RequiredQty - (double)_db.Store_RequisitionSlave.Where(m => m.Merchandising_StyleSlaveFK == x.ID).Sum(s => s.RequisitionQuantity),

                         RequisitionQuantity = 0,
                         VMUnitName = x.UnitName,
                         VMRawCategoryName = x.RawCategory

                     }).ToList();

            vM.DataListSlave = vM.DataListSlave.Where(x => x.AvailableQuantity > 0).ToList();
            return vM;
        }

        //GetSRSlaveFromFinishingStyleSlave
        public VMStoreRequisitionSlave GetSRSlaveFromFinishingStyleSlave(int id, int prid, int id2)
        {
            var vM = new VMStoreRequisitionSlave
            {
                DataListSlave = new List<VMStoreRequisitionSlave>()
            };

            // vM.DataListSlave = PackingDailyFollowupGetByRefID(0).DataList.Where(x => x.DoneQty > 0 && x.Merchandising_StyleFK == id);

            vM.DataListSlave = FinishingDailyFollowupGetByRefID(0).DataList.Where(x => x.DoneQty > 0 && x.Merchandising_StyleFK == id).Select(c =>
             new VMStoreRequisitionSlave()
             {
                 Procurement_PurchaseRequisitionFK = prid,
                 Merchandising_StyleSlaveFK = c.ID,
                 Merchandising_StyleFK = id,
                 Common_ColorFK = c.Common_ColorFK == 0 ? 43 : c.Common_ColorFK,  // 43 Empty Color
                 Common_Size = c.GSM,
                 Common_FinishItemFK = c.Common_FinishItemFK,
                 VMRawItemFK = (int)_db.Merchandising_StyleSlave.Where(x => x.Merchandising_StyleFK == id && x.IsFabric == true).FirstOrDefault().Common_RawItemFK,
                 VMRawItemName = c.OrderNo + "/" + c.FinishItemName + ". ( Color: " + c.Color + ". Size: " + c.Size + "; Port: " + c.Port + ")",
                 DescriptionSlave = "",
                 EmployeeList = new SelectList(EmployeeDropDownList(), "Value", "Text"),
                 AvailableQuantity = Convert.ToDouble(c.DoneQty) - (double)_db.Store_RequisitionSlave.Where(m => m.Merchandising_StyleSlaveFK == c.ID).Sum(s => s.RequisitionQuantity),

                 RequisitionQuantity = Convert.ToDouble(c.DoneQty) - (double)_db.Store_RequisitionSlave.Where(m => m.Merchandising_StyleSlaveFK == c.ID).Sum(s => s.RequisitionQuantity),

                 VMUnitName = "Pic"
             }).ToList();

            vM.DataListSlave = vM.DataListSlave.Where(x => x.AvailableQuantity > 0).ToList();
            return vM;
        }
        public VMStoreRequisitionSlave GetSRSlaveFromFinishingStyleSlave(int id, int prid, int id2, int subSetId)
        {
            var vM = new VMStoreRequisitionSlave
            {
                DataListSlave = new List<VMStoreRequisitionSlave>()
            };
            vM.DataListSlave = FinishingDailyFollowupGetByRefID(0).DataList.Where(x => x.DoneQty > 0 && x.Merchandising_StyleFK == id && x.Common_ColorFK == subSetId).Select(c =>
             new VMStoreRequisitionSlave()
             {
                 Procurement_PurchaseRequisitionFK = prid,
                 Merchandising_StyleSlaveFK = c.ID,
                 Merchandising_StyleFK = id,
                 Common_ColorFK = c.Common_ColorFK == 0 ? 43 : c.Common_ColorFK,  // 43 Empty Color
                 Common_Size = c.GSM,
                 Common_FinishItemFK = c.Common_FinishItemFK,
                 VMRawItemFK = (int)_db.Merchandising_StyleSlave.Where(x => x.Merchandising_StyleFK == id && x.IsFabric == true).FirstOrDefault().Common_RawItemFK,
                 VMRawItemName = c.OrderNo + "/" + c.FinishItemName + ". ( Color: " + c.Color + ". Size: " + c.Size + "; Port: " + c.Port + ")",
                 DescriptionSlave = "",
                 EmployeeList = new SelectList(EmployeeDropDownList(), "Value", "Text"),
                 AvailableQuantity = Convert.ToDouble(c.DoneQty) - (double)_db.Store_RequisitionSlave.Where(m => m.Merchandising_StyleSlaveFK == c.ID).Sum(s => s.RequisitionQuantity),

                 RequisitionQuantity = Convert.ToDouble(c.DoneQty) - (double)_db.Store_RequisitionSlave.Where(m => m.Merchandising_StyleSlaveFK == c.ID).Sum(s => s.RequisitionQuantity),

                 VMUnitName = "Pic",
                 Merchandising_StyleSetPackFK = c.Merchandising_StyleSetPackFK
             }).ToList();

            vM.DataListSlave = vM.DataListSlave.Where(x => x.AvailableQuantity > 0).ToList();
            return vM;
        }
        public VMDailyProduction FinishingDailyFollowupGetByRefID(int ID)
        {
            var vData = (from t1 in _db.Prod_ReferencePlanFollowup
                         join t2 in _db.Prod_ReferencePlan on t1.Prod_ReferencePlanFK equals t2.ID
                         join t3 in _db.Merchandising_StyleShipmentSchedule on t1.Merchandising_StyleShipmentScheduleFK equals t3.ID
                         join t4 in _db.Merchandising_StyleShipmentRatio on t1.Merchandising_StyleShipmentRatioFK equals t4.ID
                         join t5 in _db.Merchandising_StyleMeasurement on t4.Merchandising_StyleMeasurementFK equals t5.ID
                         join t6 in _db.Common_Color on t5.Common_ColorFK equals t6.ID
                         join t7 in _db.Common_Size on t5.Common_SizeFK equals t7.ID
                         join t8 in _db.Merchandising_Style on t3.Merchandising_StyleFK equals t8.ID
                         join t9 in _db.Merchandising_BuyerOrder on t8.Merchandising_BuyerOrderFK equals t9.ID
                         join t10 in _db.Common_Country on t3.Common_CountryFK equals t10.ID
                         join t11 in _db.Common_FinishItem on t8.Common_FinishItemFK equals t11.ID

                         where t1.Active == true && t2.SectionId == (int)EnumProcess.Packing //t.2.Prod_ReferenceFK == ID &&
                         select new VMDailyProduction
                         {
                             ID = t1.ID,
                             Prod_ReferencePlanFK = t2.ID,
                             Prod_ReferenceFK = t2.Prod_ReferenceFK,
                             Merchandising_StyleFK = t3.Merchandising_StyleFK,
                             StyleShipmentRatioFK = (int)t1.Merchandising_StyleShipmentRatioFK,
                             StyleShipmentScheduleFK = (int)t1.Merchandising_StyleShipmentScheduleFK,
                             Port = t10.Name + "-" + t3.DestNo,
                             OrderNo = t9.BuyerPO + "/" + t8.StyleName,
                             Color = t6.Name,
                             Common_ColorFK = t6.ID,
                             Size = t7.Name,
                             OrderQty = t4.Quantity,
                             DoneQty = (int)t1.Quantity,
                             EntryDate = t3.ShipmentDate,
                             FinishItemName = t11.Name,
                             Common_FinishItemFK = t8.Common_FinishItemFK,
                             RemainQty = t4.Quantity - Convert.ToInt32(t1.Quantity),
                         }).OrderBy(x => x.Prod_ReferenceFK).ToList();

            //if (vData.Any())
            //{
            //    foreach (var v in vData)
            //    {
            //        v.RemainQty = v.OrderQty - v.DoneQty;
            //    }
            //}

            VMDailyProduction vMReference = new VMDailyProduction
            {
                DataList = vData
            };

            return vMReference;
        }

        public VMStoreRequisitionSlave GetSRSlaveFromPackingStyleSlave(int id, int prid, int id2)
        {
            var vM = new VMStoreRequisitionSlave
            {
                DataListSlave = new List<VMStoreRequisitionSlave>()
            };

            // vM.DataListSlave = PackingDailyFollowupGetByRefID(0).DataList.Where(x => x.DoneQty > 0 && x.Merchandising_StyleFK == id);

            vM.DataListSlave = PackingDailyFollowupGetByRefID(0).DataList.Where(x => x.DoneQty > 0 && x.Merchandising_StyleFK == id).Select(c =>
             new VMStoreRequisitionSlave()
             {
                 Procurement_PurchaseRequisitionFK = prid,
                 Merchandising_StyleSlaveFK = c.ID,
                 Merchandising_StyleFK = id,
                 Common_ColorFK = c.Common_ColorFK == 0 ? 43 : c.Common_ColorFK,  // 43 Empty Color
                 Common_Size = c.GSM,
                 Common_FinishItemFK = c.Common_FinishItemFK,
                 VMRawItemFK = (int)_db.Merchandising_StyleSlave.Where(x => x.Merchandising_StyleFK == id && x.IsFabric == true).FirstOrDefault().Common_RawItemFK,
                 VMRawItemName = c.FinishItemName + ". ( Color: " + c.Color + ". Size: " + c.Size + "; Port: " + c.Port + " )",
                 DescriptionSlave = "",
                 EmployeeList = new SelectList(EmployeeDropDownList(), "Value", "Text"),
                 AvailableQuantity = Convert.ToDouble(c.DoneQty) - (double)_db.Store_RequisitionSlave.Where(m => m.Merchandising_StyleSlaveFK == c.ID).Sum(s => s.RequisitionQuantity),

                 RequisitionQuantity = Convert.ToDouble(c.DoneQty) - (double)_db.Store_RequisitionSlave.Where(m => m.Merchandising_StyleSlaveFK == c.ID).Sum(s => s.RequisitionQuantity),

                 VMUnitName = "Pic"
             }).ToList();

            vM.DataListSlave = vM.DataListSlave.Where(x => x.AvailableQuantity > 0).ToList();
            return vM;
        }
        public VMStoreRequisitionSlave GetSRSlaveFromPackingStyleSlave(int id, int prid, int id2, int subSetId)
        {
            var vM = new VMStoreRequisitionSlave
            {
                DataListSlave = new List<VMStoreRequisitionSlave>()
            };
            vM.DataListSlave = PackingDailyFollowupGetByRefID(0).DataList.Where(x => x.DoneQty > 0 && x.Merchandising_StyleFK == id && x.Common_ColorFK == subSetId).Select(c =>
             new VMStoreRequisitionSlave()
             {
                 Procurement_PurchaseRequisitionFK = prid,
                 Merchandising_StyleSlaveFK = c.ID,
                 Merchandising_StyleFK = id,
                 Common_ColorFK = c.Common_ColorFK == 0 ? 43 : c.Common_ColorFK,  // 43 Empty Color
                 Common_Size = c.GSM,
                 Common_FinishItemFK = c.Common_FinishItemFK,
                 VMRawItemFK = (int)_db.Merchandising_StyleSlave.Where(x => x.Merchandising_StyleFK == id && x.IsFabric == true).FirstOrDefault().Common_RawItemFK,
                 VMRawItemName = c.FinishItemName + ". ( Color: " + c.Color + ". Size: " + c.Size + "; Port: " + c.Port + " )",
                 DescriptionSlave = "",
                 EmployeeList = new SelectList(EmployeeDropDownList(), "Value", "Text"),
                 AvailableQuantity = Convert.ToDouble(c.DoneQty) - (double)_db.Store_RequisitionSlave.Where(m => m.Merchandising_StyleSlaveFK == c.ID).Sum(s => s.RequisitionQuantity),

                 RequisitionQuantity = Convert.ToDouble(c.DoneQty) - (double)_db.Store_RequisitionSlave.Where(m => m.Merchandising_StyleSlaveFK == c.ID).Sum(s => s.RequisitionQuantity),

                 VMUnitName = "Pic",
                 Merchandising_StyleSetPackFK = c.Merchandising_StyleSetPackFK
             }).ToList();

            vM.DataListSlave = vM.DataListSlave.Where(x => x.AvailableQuantity > 0).ToList();
            return vM;
        }
        public VMDailyProduction PackingDailyFollowupGetByRefID(int ID)
        {
            var vData = (from t1 in _db.Prod_ReferencePlanFollowup
                         join t2 in _db.Prod_ReferencePlan on t1.Prod_ReferencePlanFK equals t2.ID
                         join t3 in _db.Merchandising_StyleShipmentSchedule on t1.Merchandising_StyleShipmentScheduleFK equals t3.ID
                         join t4 in _db.Merchandising_StyleShipmentRatio on t1.Merchandising_StyleShipmentRatioFK equals t4.ID
                         join t5 in _db.Merchandising_StyleMeasurement on t4.Merchandising_StyleMeasurementFK equals t5.ID
                         join t6 in _db.Common_Color on t5.Common_ColorFK equals t6.ID
                         join t7 in _db.Common_Size on t5.Common_SizeFK equals t7.ID
                         join t8 in _db.Merchandising_Style on t3.Merchandising_StyleFK equals t8.ID
                         join t9 in _db.Merchandising_BuyerOrder on t8.Merchandising_BuyerOrderFK equals t9.ID
                         join t10 in _db.Common_Country on t3.Common_CountryFK equals t10.ID
                         join t11 in _db.Common_FinishItem on t8.Common_FinishItemFK equals t11.ID


                         where t1.Active == true && t2.SectionId == (int)EnumProcess.Ironning //t.2.Prod_ReferenceFK == ID && for Ironing
                         select new VMDailyProduction
                         {
                             ID = t1.ID,
                             Prod_ReferencePlanFK = t2.ID,
                             Prod_ReferenceFK = t2.Prod_ReferenceFK,
                             Merchandising_StyleFK = t3.Merchandising_StyleFK,
                             StyleShipmentRatioFK = (int)t1.Merchandising_StyleShipmentRatioFK,
                             StyleShipmentScheduleFK = (int)t1.Merchandising_StyleShipmentScheduleFK,
                             Port = t10.Name + "-" + t3.DestNo,
                             OrderNo = t9.BuyerPO + "/" + t8.StyleName,
                             Color = t6.Name,
                             Common_ColorFK = t6.ID,
                             Size = t7.Name,
                             OrderQty = t4.Quantity,
                             DoneQty = (int)t1.Quantity,
                             EntryDate = t3.ShipmentDate,
                             FinishItemName = t11.Name,
                             Common_FinishItemFK = t8.Common_FinishItemFK,
                             RemainQty = t4.Quantity - Convert.ToInt32(t1.Quantity),
                         }).OrderBy(x => x.Prod_ReferenceFK).ToList();

            //if (vData.Any())
            //{
            //    foreach (var v in vData)
            //    {
            //        v.RemainQty = v.OrderQty - v.DoneQty;
            //    }
            //}

            VMDailyProduction vMReference = new VMDailyProduction
            {
                DataList = vData
            };

            return vMReference;
        }
        public VMStoreRequisitionSlave GetSRSlaveFromIroningStyleSlave(int id, int prid, int id2)
        {
            var vM = new VMStoreRequisitionSlave
            {
                DataListSlave = new List<VMStoreRequisitionSlave>()
            };

            vM.DataListSlave = IroningDailyFollowupGetByRefID(0).DataList.Where(x => x.DoneQty > 0 && x.Merchandising_StyleFK == id).Select(c =>
             new VMStoreRequisitionSlave()
             {
                 Procurement_PurchaseRequisitionFK = prid,
                 Merchandising_StyleSlaveFK = c.ID,
                 Merchandising_StyleFK = id,
                 Common_ColorFK = c.Common_ColorFK == 0 || c.Common_ColorFK == 0 ? 43 : c.Common_ColorFK,  // 43 Empty Color
                 Common_Size = c.GSM,

                 VMRawItemFK = (int)_db.Merchandising_StyleSlave.Where(x => x.Merchandising_StyleFK == id && x.IsFabric == true).FirstOrDefault().Common_RawItemFK,
                 VMRawItemName = "QC Item: ( Color: " + c.Color + "; Size: " + c.Size + "; Port: " + c.Port + ")",
                 DescriptionSlave = "",
                 EmployeeList = new SelectList(EmployeeDropDownList(), "Value", "Text"),
                 AvailableQuantity = Convert.ToDouble(c.DoneQty) - (double)_db.Store_RequisitionSlave.Where(m => m.Merchandising_StyleSlaveFK == c.ID).Sum(s => s.RequisitionQuantity),

                 RequisitionQuantity = Convert.ToDouble(c.DoneQty) - (double)_db.Store_RequisitionSlave.Where(m => m.Merchandising_StyleSlaveFK == c.ID).Sum(s => s.RequisitionQuantity),

                 VMUnitName = "Pic"
             }).ToList();

            vM.DataListSlave = vM.DataListSlave.Where(x => x.AvailableQuantity > 0).ToList();
            return vM;
        }
        public VMStoreRequisitionSlave GetSRSlaveFromIroningStyleSlave(int id, int prid, int id2, int subSetId)
        {
            var vM = new VMStoreRequisitionSlave
            {
                DataListSlave = new List<VMStoreRequisitionSlave>()
            };

            vM.DataListSlave = IroningDailyFollowupGetByRefID(0).DataList.Where(x => x.DoneQty > 0 && x.Merchandising_StyleFK == id && x.Common_ColorFK == subSetId).Select(c =>
             new VMStoreRequisitionSlave()
             {
                 Procurement_PurchaseRequisitionFK = prid,
                 Merchandising_StyleSlaveFK = c.ID,
                 Merchandising_StyleFK = id,
                 Common_ColorFK = c.Common_ColorFK == 0 || c.Common_ColorFK == 0 ? 43 : c.Common_ColorFK,  // 43 Empty Color
                 Common_Size = c.GSM,

                 VMRawItemFK = (int)_db.Merchandising_StyleSlave.Where(x => x.Merchandising_StyleFK == id && x.IsFabric == true).FirstOrDefault().Common_RawItemFK,
                 VMRawItemName = "QC Item: ( Color: " + c.Color + "; Size: " + c.Size + "; Port: " + c.Port + ")",
                 DescriptionSlave = "",
                 EmployeeList = new SelectList(EmployeeDropDownList(), "Value", "Text"),
                 AvailableQuantity = Convert.ToDouble(c.DoneQty) - (double)_db.Store_RequisitionSlave.Where(m => m.Merchandising_StyleSlaveFK == c.ID).Sum(s => s.RequisitionQuantity),

                 RequisitionQuantity = Convert.ToDouble(c.DoneQty) - (double)_db.Store_RequisitionSlave.Where(m => m.Merchandising_StyleSlaveFK == c.ID).Sum(s => s.RequisitionQuantity),

                 VMUnitName = "Pic",
                 Merchandising_StyleSetPackFK = c.Merchandising_StyleSetPackFK
             }).ToList();

            vM.DataListSlave = vM.DataListSlave.Where(x => x.AvailableQuantity > 0).ToList();
            return vM;
        }
        public VMDailyProduction IroningDailyFollowupGetByRefID(int ID)
        {
            var vData = (from t1 in _db.Prod_ReferencePlanFollowup
                         join t2 in _db.Prod_ReferencePlan on t1.Prod_ReferencePlanFK equals t2.ID
                         join t3 in _db.Merchandising_StyleShipmentSchedule on t1.Merchandising_StyleShipmentScheduleFK equals t3.ID
                         join t4 in _db.Merchandising_StyleShipmentRatio on t1.Merchandising_StyleShipmentRatioFK equals t4.ID
                         join t5 in _db.Merchandising_StyleMeasurement on t4.Merchandising_StyleMeasurementFK equals t5.ID
                         join t6 in _db.Common_Color on t5.Common_ColorFK equals t6.ID
                         join t7 in _db.Common_Size on t5.Common_SizeFK equals t7.ID
                         join t8 in _db.Merchandising_Style on t3.Merchandising_StyleFK equals t8.ID
                         join t9 in _db.Merchandising_BuyerOrder on t8.Merchandising_BuyerOrderFK equals t9.ID
                         join t10 in _db.Common_Country on t3.Common_CountryFK equals t10.ID
                         where t1.Active == true && t2.SectionId == (int)EnumProcess.Ironning  //t2.Prod_ReferenceFK == ID && 
                         select new VMDailyProduction
                         {
                             ID = t1.ID,
                             Prod_ReferencePlanFK = t2.ID,
                             Prod_ReferenceFK = t2.Prod_ReferenceFK,
                             Merchandising_StyleFK = t3.Merchandising_StyleFK,
                             StyleShipmentRatioFK = (int)t1.Merchandising_StyleShipmentRatioFK,
                             StyleShipmentScheduleFK = (int)t1.Merchandising_StyleShipmentScheduleFK,
                             Port = t10.Name + "-" + t3.DestNo,
                             OrderNo = t9.BuyerPO + "/" + t8.StyleName,
                             Color = t6.Name,
                             Common_ColorFK = t6.ID,
                             Size = t7.Name,
                             OrderQty = t4.Quantity,
                             DoneQty = (int)t1.Quantity,
                             EntryDate = t3.ShipmentDate
                         }).OrderBy(x => x.Prod_ReferenceFK).ToList();

            if (vData.Any())
            {
                foreach (var v in vData)
                {
                    v.RemainQty = v.OrderQty - v.DoneQty;
                }
            }

            VMDailyProduction vMReference = new VMDailyProduction
            {
                DataList = vData
            };
            return vMReference;
        }

        public VMStoreRequisitionSlave GetSRSlaveFromSewingStyleSlave(int id, int prid, int id2)
        {
            var vM = new VMStoreRequisitionSlave
            {
                DataListSlave = new List<VMStoreRequisitionSlave>()
            };

            vM.DataListSlave = SewingDailyFollowupGetByRefID(0).DataList.Where(x => x.DoneQty > 0 && x.Merchandising_StyleFK == id).Select(c =>
             new VMStoreRequisitionSlave()
             {
                 Procurement_PurchaseRequisitionFK = prid,
                 Merchandising_StyleSlaveFK = c.ID,
                 Merchandising_StyleFK = id,
                 Common_ColorFK = c.Common_ColorFK == 0 ? 43 : c.Common_ColorFK,  // 43 Empty Color
                 Common_Size = c.GSM,

                 VMRawItemFK = (int)_db.Merchandising_StyleSlave.Where(x => x.Merchandising_StyleFK == id && x.IsFabric == true).FirstOrDefault().Common_RawItemFK,
                 VMRawItemName = "QC Item: ( Color: " + c.Color + "; Size: " + c.Size + "; Port: " + c.Port + ")",
                 DescriptionSlave = "",
                 EmployeeList = new SelectList(EmployeeDropDownList(), "Value", "Text"),
                 AvailableQuantity = Convert.ToDouble(c.DoneQty) - (double)_db.Store_RequisitionSlave.Where(m => m.Merchandising_StyleSlaveFK == c.ID).Sum(s => s.RequisitionQuantity),

                 RequisitionQuantity = Convert.ToDouble(c.DoneQty) - (double)_db.Store_RequisitionSlave.Where(m => m.Merchandising_StyleSlaveFK == c.ID).Sum(s => s.RequisitionQuantity),
                 VMUnitName = "Pic"
             }).ToList();

            vM.DataListSlave = vM.DataListSlave.Where(x => x.AvailableQuantity > 0).ToList();
            return vM;
        }
        public VMStoreRequisitionSlave GetSRSlaveFromSewingStyleSlave(int id, int prid, int id2, int subSetId)
        {
            var vM = new VMStoreRequisitionSlave
            {
                DataListSlave = new List<VMStoreRequisitionSlave>()
            };

            vM.DataListSlave = SewingDailyFollowupGetByRefID(0).DataList.Where(x => x.DoneQty > 0 && x.Merchandising_StyleFK == id && x.Common_ColorFK == subSetId).Select(c =>
             new VMStoreRequisitionSlave()
             {
                 Procurement_PurchaseRequisitionFK = prid,
                 Merchandising_StyleSlaveFK = c.ID,
                 Merchandising_StyleFK = id,
                 Common_ColorFK = c.Common_ColorFK == 0 ? 43 : c.Common_ColorFK,  // 43 Empty Color
                 Common_Size = c.GSM,

                 VMRawItemFK = (int)_db.Merchandising_StyleSlave.Where(x => x.Merchandising_StyleFK == id && x.IsFabric == true).FirstOrDefault().Common_RawItemFK,
                 VMRawItemName = "QC Item: ( Color: " + c.Color + "; Size: " + c.Size + "; Port: " + c.Port + ")",
                 DescriptionSlave = "",
                 EmployeeList = new SelectList(EmployeeDropDownList(), "Value", "Text"),
                 AvailableQuantity = Convert.ToDouble(c.DoneQty) - (double)_db.Store_RequisitionSlave.Where(m => m.Merchandising_StyleSlaveFK == c.ID).Sum(s => s.RequisitionQuantity),

                 RequisitionQuantity = Convert.ToDouble(c.DoneQty) - (double)_db.Store_RequisitionSlave.Where(m => m.Merchandising_StyleSlaveFK == c.ID).Sum(s => s.RequisitionQuantity),

                 VMUnitName = "Pic",
                 Merchandising_StyleSetPackFK = c.Merchandising_StyleSetPackFK
             }).ToList();

            vM.DataListSlave = vM.DataListSlave.Where(x => x.AvailableQuantity > 0).ToList();
            return vM;
        }

        public VMDailyProduction SewingDailyFollowupGetByRefID(int ID)
        {
            var vData = (from t1 in _db.Prod_ReferencePlan
                         join t2 in _db.Prod_ReferencePlanFollowup on t1.ID equals t2.Prod_ReferencePlanFK
                         join t3 in _db.Merchandising_StyleShipmentSchedule on t2.Merchandising_StyleShipmentScheduleFK equals t3.ID
                         join t4 in _db.Merchandising_StyleShipmentRatio on t2.Merchandising_StyleShipmentRatioFK equals t4.ID
                         join t5 in _db.Merchandising_StyleMeasurement on t4.Merchandising_StyleMeasurementFK equals t5.ID
                         join t6 in _db.Common_Color on t5.Common_ColorFK equals t6.ID
                         join t7 in _db.Common_Size on t5.Common_SizeFK equals t7.ID
                         join t8 in _db.Merchandising_Style on t3.Merchandising_StyleFK equals t8.ID
                         join t9 in _db.Merchandising_BuyerOrder on t8.Merchandising_BuyerOrderFK equals t9.ID
                         join t10 in _db.Common_Country on t3.Common_CountryFK equals t10.ID
                         where t1.Active == true && t1.SectionId == (int)EnumProcess.Sewing && t2.Active == true //t1.Prod_ReferenceFK == ID &&
                         select new VMDailyProduction
                         {
                             ID = t2.ID,
                             Prod_ReferencePlanFK = t1.ID,
                             Prod_ReferenceFK = t1.Prod_ReferenceFK,
                             Merchandising_StyleFK = t3.Merchandising_StyleFK,
                             StyleShipmentRatioFK = (int)t2.Merchandising_StyleShipmentRatioFK,
                             StyleShipmentScheduleFK = (int)t2.Merchandising_StyleShipmentScheduleFK,
                             Plan_ProductionLineFk = (int)t1.ProductionLineFK,
                             Port = t10.Name + "-" + t3.DestNo,
                             OrderNo = t9.BuyerPO + "/" + t8.StyleName,
                             Color = t6.Name,
                             Common_ColorFK = t6.ID,
                             Size = t7.Name,
                             OrderQty = t4.Quantity,
                             DoneQty = (int)t2.Quantity,
                             EntryDate = t3.ShipmentDate
                         }).OrderBy(x => x.Prod_ReferenceFK).ToList();

            if (vData.Any())
            {
                foreach (var v in vData)
                {
                    v.LineName = GetLineName(v.Plan_ProductionLineFk);
                    v.RemainQty = v.OrderQty - v.DoneQty;
                }
            }

            VMDailyProduction vMReference = new VMDailyProduction
            {
                DataList = vData
            };

            return vMReference;
        }
        private string GetLineName(int LineID)
        {
            string name = string.Empty;
            var vData = ProductionLineGetByProcessID(2).Where(a => a.ID == LineID);
            if (vData.Any())
            {
                name = vData.FirstOrDefault().Name;
            }
            return name;
        }
        public List<VMProductionLine> ProductionLineGetByProcessID(int ProductionProcessID)
        {
            if (ProductionProcessID > 0)
            {
                var vLineData = (from t1 in _db.Common_ProductionLine
                                 join t2 in _db.Common_SectionLine on t1.Common_SectionLineFK equals t2.ID
                                 where t1.Active == true && t1.ProductionStepFK == ProductionProcessID && t1.IsClosed == false
                                 select new VMProductionLine
                                 {
                                     Name = t1.Code + "-" + t1.Name,
                                     ID = t1.ID,
                                     Priority = t1.Priority
                                 }).ToList();
                return vLineData;
            }
            else
            {
                var vLineData = (from t1 in _db.Common_ProductionLine
                                 join t2 in _db.Common_SectionLine on t1.Common_SectionLineFK equals t2.ID
                                 where t1.Active == true && t1.IsClosed == false
                                 select new VMProductionLine
                                 {
                                     Name = t1.Code + "-" + t1.Name,
                                     ID = t1.ID,
                                     Priority = t1.Priority
                                 }).ToList();

                return vLineData;
            }
        }

        /// <summary>
        ///  For Cutting Store
        /// </summary>
        /// <param name="id"></param>
        /// <param name="prid"></param>
        /// <param name="id2"></param>
        /// <returns></returns>

        public VMStoreRequisitionSlave GetSRSlaveFromCuttingStyleSlave(int id, int prid, int id2)
        {
            var vM = new VMStoreRequisitionSlave
            {
                DataListSlave = new List<VMStoreRequisitionSlave>()
            };


            vM.DataListSlave = CuttingDailyFollowupGetByRefID(0).DataList.Where(x => x.DoneQty > 0 && x.Merchandising_StyleFK == id).Select(c =>
             new VMStoreRequisitionSlave()
             {
                 Procurement_PurchaseRequisitionFK = prid,
                 Merchandising_StyleSlaveFK = c.ID,
                 Merchandising_StyleFK = id,

                 Common_Color = c.Color,
                 Common_Size = c.GSM,
                 Common_ColorFK = c.Common_ColorFK == 0 || c.Common_ColorFK == 0 ? 43 : c.Common_ColorFK,  // 43 Empty Color                 
                 Common_SizeFK = c.Common_SizeFK == 0 || c.Common_SizeFK == 0 ? 20 : c.Common_SizeFK,

                 VMRawItemFK = (int)_db.Merchandising_StyleSlave.Where(x => x.Merchandising_StyleFK == id && x.IsFabric == true).FirstOrDefault().Common_RawItemFK,
                 VMRawItemName = " Cut Pieces: ( Color: " + c.Color + "; Size: " + c.Size + "; Port: " + c.Port + " )",

                 DescriptionSlave = "",
                 EmployeeList = new SelectList(EmployeeDropDownList(), "Value", "Text"),
                 AvailableQuantity = (double)c.DoneQty, // - (double)_db.Store_RequisitionSlave.Where(m => m.Merchandising_StyleSlaveFK == c.ID).Sum(s => s.RequisitionQuantity),

                 RequisitionQuantity = (double)c.DoneQty,
                 VMUnitName = "Pic"
             }).ToList();

            vM.DataListSlave = vM.DataListSlave.Where(x => x.AvailableQuantity > 0).ToList();
            return vM;
        }
        public VMStoreRequisitionSlave GetSRSlaveFromCuttingStyleSlave(int id, int prid, int id2, int subSetId)
        {
            var vM = new VMStoreRequisitionSlave
            {
                DataListSlave = new List<VMStoreRequisitionSlave>()
            };

            vM.DataListSlave = CuttingDailyFollowupGetByRefID(0).DataList.Where(x => x.DoneQty > 0 && x.Merchandising_StyleFK == id && x.Common_ColorFK == subSetId).Select(c =>
             new VMStoreRequisitionSlave()
             {
                 Procurement_PurchaseRequisitionFK = prid,
                 Merchandising_StyleSlaveFK = c.ID,
                 Merchandising_StyleFK = id,
                 Common_ColorFK = c.Common_ColorFK == 0 || c.Common_ColorFK == 0 ? 43 : c.Common_ColorFK,  // 43 Empty Color
                 Common_Size = c.GSM,

                 VMRawItemFK = (int)_db.Merchandising_StyleSlave.Where(x => x.Merchandising_StyleFK == id && x.IsFabric == true).FirstOrDefault().Common_RawItemFK,
                 VMRawItemName = " Cut Pieces: ( Color: " + c.Color + "; Size: " + c.Size + "; Port: " + c.Port + " )",
                 DescriptionSlave = "",
                 EmployeeList = new SelectList(EmployeeDropDownList(), "Value", "Text"),
                 AvailableQuantity = Convert.ToDouble(c.DoneQty), //- (double)_db.Store_RequisitionSlave.Where(m => m.Merchandising_StyleSlaveFK == c.ID).Sum(s => s.RequisitionQuantity),

                 RequisitionQuantity = Convert.ToDouble(c.DoneQty),
                 VMUnitName = "Pic",
                 Merchandising_StyleSetPackFK = c.Merchandising_StyleSetPackFK
             }).ToList();

            vM.DataListSlave = vM.DataListSlave.Where(x => x.AvailableQuantity > 0).ToList();
            return vM;
        }
        public async Task<VMProductionReference> ProdReferenceGetByID(int ID)
        {
            var vData = _db.Prod_Reference.Where(a => a.ID == ID).Select(a => new VMProductionReference
            {
                ID = a.ID,
                ReferenceDate = a.ReferenceDate,
                Remarks = a.Remarks,
                ReferenceNo = a.ReferenceNo
            }).FirstOrDefault();

            return await Task.Run(() => vData);
        }

        public VMDailyProduction CuttingDailyFollowupGetByRefID(int ID)
        {
            var vData = (from t1 in _db.Prod_ReferencePlanFollowup
                         join t2 in _db.Prod_ReferencePlan on t1.Prod_ReferencePlanFK equals t2.ID
                         join t3 in _db.Merchandising_StyleShipmentSchedule on t1.Merchandising_StyleShipmentScheduleFK equals t3.ID
                         join t4 in _db.Merchandising_StyleShipmentRatio on t1.Merchandising_StyleShipmentRatioFK equals t4.ID
                         join t5 in _db.Merchandising_StyleMeasurement on t4.Merchandising_StyleMeasurementFK equals t5.ID
                         join t6 in _db.Common_Color on t5.Common_ColorFK equals t6.ID
                         join t7 in _db.Common_Size on t5.Common_SizeFK equals t7.ID
                         join t8 in _db.Merchandising_Style on t3.Merchandising_StyleFK equals t8.ID
                         join t9 in _db.Merchandising_BuyerOrder on t8.Merchandising_BuyerOrderFK equals t9.ID
                         join t10 in _db.Common_Country on t3.Common_CountryFK equals t10.ID
                         where t1.Active == true && t2.SectionId == (int)EnumProcess.Cutting && t2.Active //t2.Prod_ReferenceFK == ID && 
                         select new VMDailyProduction
                         {
                             ID = t1.ID,
                             Prod_ReferencePlanFK = t2.ID,
                             Prod_ReferenceFK = t2.Prod_ReferenceFK,
                             Merchandising_StyleFK = t3.Merchandising_StyleFK,
                             StyleShipmentRatioFK = (int)t1.Merchandising_StyleShipmentRatioFK,
                             StyleShipmentScheduleFK = (int)t1.Merchandising_StyleShipmentScheduleFK,
                             Merchandising_StyleSetPackFK = t2.Merchandising_StyleSetPackFK != null ? (int)t2.Merchandising_StyleSetPackFK : 0,
                             Port = t10.Name + "-" + t3.DestNo,
                             OrderNo = t9.BuyerPO + "/" + t8.StyleName,
                             Color = t6.Name,
                             Size = t7.Name,
                             Common_SizeFK = t5.Common_SizeFK,
                             Common_ColorFK = t5.Common_ColorFK,
                             OrderQty = (int)decimal.Multiply(t4.Quantity, t8.PackPieceQty),
                             DoneQty = (int)t1.Quantity,
                             CuttingExtra = t2.ExtraCutting,
                             GSM = t2.GSMORSMV,
                             Layer = t2.FabricLayer,
                             EntryDate = t3.ShipmentDate
                         }).OrderBy(x => x.Prod_ReferenceFK).ToList();
            List<VMDailyProduction> lst = new List<VMDailyProduction>();
            if (vData.Any())
            {
                foreach (var v in vData)
                {

                    v.ExtraQty = (int)decimal.Round(decimal.Multiply(decimal.Divide(v.CuttingExtra, (decimal)100), (decimal)v.OrderQty));
                    //v.OrderQty = TotalOrderQtyByColorID(v.Merchandising_StyleFK, v.Common_ColorFK);
                    v.RemainQty = (v.OrderQty + v.ExtraQty) - GetDoneQtyBySizeWise(v.Prod_ReferenceFK, v.Merchandising_StyleFK, v.Merchandising_StyleSetPackFK, v.StyleShipmentRatioFK, v.Common_ColorFK, v.Common_SizeFK, 1);
                    //if (v.RemainQty>0)
                    //{
                    //    lst.Add(v);
                    //}

                }
            }

            VMDailyProduction vMReference = new VMDailyProduction
            {
                DataList = vData //await Task.Run(() => vData)
            };

            return vMReference;
        }
        private int GetDoneQtyBySizeWise(int RefID, int StyleID, int SubSetID, int RatioID, int ColorID, int SizeID, int SectionId)
        {
            int TotalDoneQty = 0;

            var takedate = _db.Prod_Reference.Where(a => a.ID == RefID).FirstOrDefault();
            DateTime FDate = takedate.ReferenceDate;
            var vOrderDoneData = (from o in _db.Prod_Reference
                                  join p in _db.Prod_ReferencePlan on o.ID equals p.Prod_ReferenceFK
                                  join q in _db.Prod_ReferencePlanFollowup on p.ID equals q.Prod_ReferencePlanFK
                                  join r in _db.Merchandising_StyleShipmentRatio on q.Merchandising_StyleShipmentRatioFK equals r.ID
                                  join s in _db.Merchandising_StyleMeasurement on r.Merchandising_StyleMeasurementFK equals s.ID
                                  where o.ReferenceDate <= FDate && p.Merchandising_StyleFK == StyleID && p.Merchandising_StyleSetPackFK == SubSetID && q.Merchandising_StyleShipmentRatioFK == RatioID && p.SectionId == SectionId && s.Common_ColorFK == ColorID && s.Common_SizeFK == SizeID
                                  select new { q }).ToList();

            if (vOrderDoneData.Any())
            {
                TotalDoneQty = (int)vOrderDoneData.Sum(a => a.q.Quantity);
            }

            return TotalDoneQty;
        }

        //public VMDailyProduction CuttingDailyFollowupGetByRefID(int ID)
        //{
        //    var vData = (from t1 in _db.Prod_ReferencePlanFollowup
        //                 join t2 in _db.Prod_ReferencePlan on t1.Prod_ReferencePlanFK equals t2.ID
        //                 join t3 in _db.Merchandising_StyleShipmentSchedule on t1.Merchandising_StyleShipmentScheduleFK equals t3.ID
        //                 join t4 in _db.Merchandising_StyleShipmentRatio on t1.Merchandising_StyleShipmentRatioFK equals t4.ID
        //                 join t5 in _db.Merchandising_StyleMeasurement on t4.Merchandising_StyleMeasurementFK equals t5.ID
        //                 join t6 in _db.Common_Color on t5.Common_ColorFK equals t6.ID
        //                 join t7 in _db.Common_Size on t5.Common_SizeFK equals t7.ID
        //                 join t8 in _db.Merchandising_Style on t3.Merchandising_StyleFK equals t8.ID
        //                 join t9 in _db.Merchandising_BuyerOrder on t8.Merchandising_BuyerOrderFK equals t9.ID
        //                 join t10 in _db.Common_Country on t3.Common_CountryFK equals t10.ID
        //                 where t1.Active == true && t2.SectionId == (int)EnumProcess.Cutting && t2.Active == true //&& t2.Prod_ReferenceFK == ID &&
        //                 select new VMDailyProduction
        //                 {
        //                     ID = t1.ID,
        //                     Prod_ReferencePlanFK = t2.ID,
        //                     Prod_ReferenceFK = t2.Prod_ReferenceFK,
        //                     Merchandising_StyleFK = t3.Merchandising_StyleFK,
        //                     StyleShipmentRatioFK = (int)t1.Merchandising_StyleShipmentRatioFK,
        //                     StyleShipmentScheduleFK = (int)t1.Merchandising_StyleShipmentScheduleFK,
        //                     Port = t10.Name + "-" + t3.DestNo,
        //                     OrderNo = t9.BuyerPO + "/" + t8.StyleName,
        //                     Color = t6.Name,
        //                     Common_ColorFK = t2.Common_ColorFK ?? 43,
        //                     Size = t7.Name,
        //                     OrderQty = t4.Quantity,
        //                     DoneQty = (int)t1.Quantity,
        //                     CuttingExtra = t2.ExtraCutting,
        //                     GSM = t2.GSMORSMV,
        //                     EntryDate = t3.ShipmentDate,
        //                     Merchandising_StyleSetPackFK = t2.Merchandising_StyleSetPackFK ?? 1
        //                 }).OrderBy(x => x.Port).ToList();

        //    if (vData.Any())
        //    {
        //        foreach (var v in vData)
        //        {
        //            v.ExtraQty = (int)decimal.Round(decimal.Multiply(decimal.Divide(v.CuttingExtra, (decimal)100), (decimal)v.OrderQty));
        //            //v.DoneQty = GetDoneQty(v.Prod_ReferenceFK, v.Common_ColorFK, 1);
        //            //v.OrderQty = TotalOrderQtyByColorID(v.Merchandising_StyleFK, v.Common_ColorFK);
        //            v.RemainQty = (v.OrderQty + v.ExtraQty) - v.DoneQty;
        //        }
        //    }

        //    VMDailyProduction vMReference = new VMDailyProduction
        //    {
        //        DataList = vData
        //    };

        //    return vMReference;
        //}
        public async Task<int> PRSlaveListAdd(List<VMStoreRequisitionSlave> vM, int modelStyleId, string deptId)
        {
            var result = -1;
            if (vM == null || vM.Count <= 0) { return result; }

            var nPrid = vM.Select(x => x.Procurement_PurchaseRequisitionFK).FirstOrDefault();
            int vMStoreFromFK = vM.Select(x => x.StoreFromFK).FirstOrDefault();
            int vMStoreToFK = vM.Select(x => x.StoreToFK).FirstOrDefault();

            var enPr = new Store_Requisition
            {
                CID = CidServices.GetRequisitionRefNo(deptId),  //await Task.Run(() => (((_db.Store_Requisition.AnyAsync().Result ? _db.Store_Requisition.MaxAsync(x => x.ID).Result : 0) + 1).ToString().PadLeft(5, '0'))),

                Status = (int)PRStatusEnum.Draft,
                RequisitionType = (int)EnumRequisitionType.Production,
                User = vM.Select(x => x.User).FirstOrDefault()

                //Store_GeneralOutFK = vMStoreFromFK,
                //Store_GeneralInFK = vMStoreToFK,
                //Merchandising_StyleFK = modelStyleId,
            };
            if (nPrid > 0)
            {
                enPr = _db.Store_Requisition.Find(nPrid);
                if (enPr.Active)
                {
                    //vM = vM.Where(x => !_db.Store_RequisitionSlave.Any(y => y.Active && y.Procurement_PurchaseRequisitionFK == enPr.ID && y.Common_RawItemFK == x.VMRawItemFK && x.VMMerchandising_StyleSlaveID == y.Merchandising_StyleSlaveFK)).ToList();
                    vM = vM.Where(x => !_db.Store_RequisitionSlave.Any(y => y.Active && y.Store_RequisitionFK == enPr.ID && y.Common_RawItemFK == x.VMRawItemFK)).ToList();
                }
                else
                {
                    enPr = new Store_Requisition
                    {
                        Status = (int)PRStatusEnum.Draft
                    };
                }
            }

            enPr.Store_RequisitionSlave = new List<Store_RequisitionSlave>();
            enPr.Store_RequisitionSlave = vM.Select(x => new Store_RequisitionSlave
            {
                Merchandising_StyleFK = x.Merchandising_StyleFK,
                Merchandising_StyleSlaveFK = x.Merchandising_StyleSlaveFK,                
                Common_ColorFK = x.Common_ColorFK == null || x.Common_ColorFK == 0 ? 43 : x.Common_ColorFK,
                Common_SizeFK = x.Common_SizeFK == null || x.Common_SizeFK == 0 ? 20 : x.Common_SizeFK,

                RequisitionQuantity = x.RequisitionQuantity,
                Description = x.DescriptionSlave,
                Common_RawItemFK = x.VMRawItemFK,

                HRMS_EmployeeFK = (int)(x.VMEmployeeFK == null || x.VMEmployeeFK == 0 ? x.UserID : x.VMEmployeeFK),
                RequiredDate = x.RequiredDate,
                InspectionRequired = x.InspectionRequired,
                User = x.User,
                Merchandising_StyleSetPackFK = x.Merchandising_StyleSetPackFK ?? 1

            }).ToList();
            if (enPr.ID <= 0)
            {
                await _db.Store_Requisition.AddRangeAsync(enPr);
            }

            if (await _db.SaveChangesAsync() > 0)
            {
                result = enPr.ID;
            }
            return result;
        }

        public List<ReportRawItem> GetAllawItemListByStyleId(int styleId)
        {
            var list = new List<ReportRawItem>();

            var data = (from r in _db.Procurement_PurchaseOrderSlave
                        join ri in _db.Common_RawItem on r.Common_RawItemFK equals ri.ID
                        join ms in _db.Merchandising_Style on r.Merchandising_StyleID equals ms.ID
                        join ss in _db.Merchandising_BuyerOrder on ms.Merchandising_BuyerOrderFK equals ss.ID
                        join s in _db.Common_FinishItem on ms.Common_FinishItemFK equals s.ID

                        join si in _db.Store_StockIn on ri.ID equals si.ItemIDFK
                        join sg in _db.Store_General on si.CommonStoreFK equals sg.ID
                        join sup in _db.Common_Supplier on si.CommonSupplierFK equals sup.ID
                        join so in _db.Store_StockOut on ri.ID equals so.ItemIDFK
                        join sgg in _db.Store_General on so.CommonStoreFK equals sgg.ID


                        where r.Merchandising_StyleID == styleId && r.Active
                        //group s by s into g
                        select new ReportRawItem
                        {
                            SupplierName = sup.Name + " - " + sup.Code,
                            StyleName = ss.BuyerPO + " " + ms.CID + " " + s.Name,
                            BuyerName = ri.Name,
                            ItemCode = ri.GSM,
                            ItemName = ri.Name,
                            PurchaseQuantity = r.PurchaseQuantity,
                            StoreReceivedQty = si.ReceivedQty,
                            PurchasingValue = (r.PurchaseQuantity * si.ReceivedQty),
                            StoreOutQty = so.ReceivedQty,
                            StoreInName = si.Name,
                            StoreOutName = so.Name,
                            Remarks = si.Remarks
                        }).ToList();
            return data;
        }

        public VMStoreSRequisitionSlave PRSlaveFromStyleSlaveGet(int id, int PRID, int typeID, int orgID)
        {

            //////send orgID to BOMGetByStyleID for DataListSlave (1=Combined,2=BOM,3=BOF)
            VMStoreSRequisitionSlave vM = new VMStoreSRequisitionSlave();
            vM.DataListSlave = new List<VMStoreSRequisitionSlave>();
            vM.DataListSlave = integrationservice.BOMGetByStyleID(id).Select(x => new VMStoreSRequisitionSlave()
            {
                PRType = (PRTypeEnum)typeID,
                OriginType = (ProcurementOriginTypeEnum)orgID,
                Procurement_PurchaseRequisitionFK = PRID,
                VMMerchandising_StyleID = x.Merchandising_StyleFK,
                VMMerchandising_StyleSlaveID = x.IsBOM ? x.ID : (int?)null,
                VMMerchandising_BOFID = !x.IsBOM ? x.ID : (int?)null,
                VMRawItemFK = x.Common_RawItemFK,
                VMRawItemName = x.RawItem,
                EmployeeList = new Microsoft.AspNetCore.Mvc.Rendering.SelectList(EmployeeDropDownList(), "Value", "Text"),
                StyleQuantity = (double)x.RequiredQty,
                PRRaisedQuantity = (from PRS in _db.Procurement_PurchaseRequisitionSlave.Include(y => y.Procurement_PurchaseRequisition)
                                                          .Where(y => y.Procurement_PurchaseRequisition.Active
                                                          && (!String.IsNullOrEmpty(y.Procurement_PurchaseRequisition.CID) && y.Procurement_PurchaseRequisition.ID != PRID)
                                                          && y.Active
                                                          && (y.Common_RawItemFK == x.Common_RawItemFK)
                                                          && (y.Merchandising_StyleID == x.Merchandising_StyleFK)
                                                          && ((y.Merchandising_StyleSlaveFK == x.ID && x.IsBOM) || (!y.Merchandising_StyleSlaveFK.HasValue))
                                                          && ((y.Merchandising_BOFFK == x.ID && !x.IsBOM) || (!y.Merchandising_BOFFK.HasValue)))
                                    select new { Quantity = PRS.RequisitionQuantity })
                                                          .Sum(y => y.Quantity),
                VMUnitName = x.UnitName,
                //BONoStyleName=
            }).ToList();

            vM.DataListSlave = vM.DataListSlave.Where(x => x.RestQuantity > 0).ToList();

            return vM;
        }


        public async Task<List<HRMSUnitVM>> UnitListGetByBUIdAsync(int id)
        {
            return await integrationservice.UnitListGetByBUIdAsync(id);
        }
        //public async Task<List<VMUserDepartment>> DepartmentListGetByUIdAsync(int id = 0)
        //{
        //    return await integrationservice.DepartmentListGetByUIdAsync(id);
        //}
        public async Task<List<VMUserDepartment>> DepartmentListGetByUIdAsync(int id = 0)
        {
            return await _db.User_Department.Where(x => x.Active)  //&& x.HRMS_UnitFK == id
                .Select(x => new VMUserDepartment()
                {
                    ID = x.ID,
                    Name = x.Name
                }).ToListAsync();
        }

        ////////////// StoreIn Master //////////////
        public async Task<VmStoreInMaster> GetStoreInMaster(int id)
        {
            var dataListsMasterNew = (from s in _db.Store_StockInMaster
                                          //join si in _db.Store_StockIn on s.ID equals si.Store_StockInMasterFK
                                      join cs in _db.Common_Supplier on s.Common_SupplierFK equals cs.ID
                                      join po in _db.Procurement_PurchaseOrder on s.PurchaseOrderFK equals po.ID
                                      //join st in _db.Merchandising_Style on si.Merchandising_StyleFK equals st.ID
                                      where s.Active && s.Store_GeneralFK == (id == 0 ? 2 : id) && s.StoreInType == "New"
                                      select new VmStoreInMaster
                                      {
                                          ID = s.ID,
                                          CID = s.CID,
                                          ChallanNo = s.ChallanNo,
                                          ChallanDate = s.ChallanDate,
                                          ReceivedDate = s.ReceivedDate,
                                          Description = s.Description,
                                          StockType = s.StockType,
                                          StoreInType = s.StoreInType,
                                          User = s.User,
                                          SupplierName = cs.Name,
                                          PurchaseOrder = po.CID,
                                          PurchaseOrderFK = po.ID,
                                          // StyleName = _db.Merchandising_Style.FirstOrDefault(x => x.ID == (int)_db.Store_StockIn.FirstOrDefault(i => i.Store_StockInMasterFK == s.ID).Merchandising_StyleFK).CID + "/"+_db.Merchandising_Style.FirstOrDefault(x=>x.ID == (int)_db.Store_StockIn.FirstOrDefault(i=>i.Store_StockInMasterFK == s.ID).Merchandising_StyleFK ).StyleName //st.CID +"/"+ st.StyleName 
                                      }).OrderByDescending(x => x.ID).ToList();

            if (dataListsMasterNew.Any())
            {
                dataListsMasterNew.ForEach(d =>
                {
                    var style = _db.Merchandising_Style.FirstOrDefault(x => x.ID == _db.Store_StockIn.FirstOrDefault(i => i.Store_StockInMasterFK == d.ID).Merchandising_StyleFK);
                    if (style != null)
                    {
                        d.StyleName = style.CID + "/" + style.StyleName;
                    }
                });
            }

            var dataListsMasterInternal = (from s in _db.Store_StockInMaster
                                           join sg in _db.Store_General on s.Store_GeneralFK equals sg.ID
                                           //join po in _db.Procurement_PurchaseOrder on s.PurchaseOrderFK equals po.ID
                                           where s.Active && s.Store_GeneralFK == (id == 0 ? 2 : id) && s.StoreInType == "Internal"
                                           select new VmStoreInMaster
                                           {
                                               ID = s.ID,
                                               CID = s.CID,
                                               ChallanNo = s.ChallanNo,
                                               ChallanDate = s.ChallanDate,
                                               ReceivedDate = s.ReceivedDate,
                                               Description = s.Description,
                                               StockType = s.StockType,
                                               StoreInType = s.StoreInType,
                                               StoreName = sg.StoreName,
                                               User = s.User,
                                               //SupplierName = cs.Name,
                                               // PurchaseOrder = po.CID
                                           }).OrderByDescending(x => x.ID).ToList();

            var vmmodel = new VmStoreInMaster
            {
                DataListsMasterNew = new List<VmStoreInMaster>(),
                DataListsMasterInternal = new List<VmStoreInMaster>()
            };

            vmmodel.DataListsMasterNew = dataListsMasterNew;
            vmmodel.DataListsMasterInternal = dataListsMasterInternal;

            vmmodel.SupplierList = await SupplierDropDownListAsync();
            vmmodel.PoList = await PoDropDownListAsync();
            vmmodel.StoreList = await StoreDropDownListAsync();
            vmmodel.ICList = await ICAllDropDownList(id);
            vmmodel.CommonStoreFK = id;
            return vmmodel;
        }
        public async Task<VmStoreIn> GetStoreInSlave(VmStoreInMaster vmModel, int id, int id2)
        {
            ////====================================/////
            var itemNameex = "";
            if (id2 == 3)
            {
                itemNameex = "-Cut Pieces";
            }

            var vmmodel = new VmStoreIn
            {
                DataLists = new List<VmStoreIn>()
            };
            var remainingQty = 0.0;
            var orginType = 0;
            var aStoreWithOutStyle = new List<VmStoreIn>();
            var aStoreWithOutStyleReceived = new List<VmStoreInReceived>();

            if (_db.Store_StockInMaster.FirstOrDefault(x => x.ID == id).StoreInType == "New")
            {
                // for 1 new Pur 
                vmModel.PurchaseOrderFK = (int)_db.Store_StockInMaster.FirstOrDefault(x => x.ID == id).PurchaseOrderFK;
                if (_db.Store_StockIn.Any(s => s.IPOFK == vmModel.PurchaseOrderFK))
                {
                    remainingQty = _db.Store_StockIn.Where(s => s.IPOFK == vmModel.PurchaseOrderFK).Min(s => s.RemainingQty);
                }

                if (orginType == 0)
                {
                    aStoreWithOutStyle = await Task.Run(() => (
                        from p in _db.Procurement_PurchaseOrderSlave
                        join po in _db.Procurement_PurchaseOrder on p.Procurement_PurchaseOrderFK equals po.ID //into ptemp
                        join i in _db.Common_RawItem on p.Common_RawItemFK equals i.ID
                        join sc in _db.Common_RawSubCategory on i.Common_RawSubCategoryFK equals sc.ID
                        join c in _db.Common_RawCategory on sc.Common_RawCategoryFK equals c.ID

                        where p.Active && po.Active && sc.Active && c.Active
                        && p.Procurement_PurchaseOrderFK == (vmModel.PurchaseOrderFK == 0 ? 1 : vmModel.PurchaseOrderFK)
                        && p.PurchaseQuantity >
                        (_db.Store_StockIn.Any(x => x.Active && x.Procurement_PurchaseOrderSlaveFK == p.ID)
                            ? _db.Store_StockIn.Where(x => x.Active && x.Procurement_PurchaseOrderSlaveFK == p.ID)
                                .Select(y => new { ReceivedQty = y.ReceivedQty }).Sum(c => c.ReceivedQty)
                            : 0)
                        select new VmStoreIn
                        {
                            ID = vmModel.ID,
                            CID = vmModel.CID,
                            ChallanNo = vmModel.ChallanNo,
                            ChallanDate = vmModel.ChallanDate,
                            ReceivedDate = vmModel.ReceivedDate,
                            WarrentyDate = DateTime.Now,
                            Name = i.Name,
                            Description = po.Description,
                            StockType = "Production",
                            StoreInType = "New",
                            Remarks = vmModel.Remarks,

                            PurchaseQuantity =  p.PurchaseQuantity,
                            RemainingQty = p.PurchaseQuantity - (_db.Store_StockIn.Any(x => x.Active && x.Procurement_PurchaseOrderSlaveFK == p.ID)
                                               ? _db.Store_StockIn.Where(x => x.Active && x.Procurement_PurchaseOrderSlaveFK == p.ID)
                                                   .Select(y => new { ReceivedQty = y.ReceivedQty }).Sum(c => c.ReceivedQty)
                                               : 0),
                            DamagedQty = 0,
                            ReceivedQty = 0,
                            //p.PurchaseQuantity - (_db.Store_StockIn.Any(x => x.Active && x.Procurement_PurchaseOrderSlaveFK == p.ID)
                            //                  ? _db.Store_StockIn.Where(x => x.Active && x.Procurement_PurchaseOrderSlaveFK == p.ID)
                            //                      .Select(y => new { ReceivedQty = y.ReceivedQty }).Sum(c => c.ReceivedQty)
                            //                  : 0),

                            IPOFK = vmModel.PurchaseOrderFK,
                            ItemIDFK = p.Common_RawItemFK,
                            //StoreBinFK = vmModel.StoreBinFK,
                            CommonUnitFK = i.Common_UnitFK,
                            UnitName = i.Common_UnitFK == 0 ? "Not Set Name" : (_db.Common_Unit.FirstOrDefault(x => x.ID == i.Common_UnitFK).Name),
                            CommonStoreFK = vmModel.CommonStoreFK,
                            CommonSupplierFK = 1,
                            CommonCategoryFK = c.ID,
                            CommonSubCategoryFK = sc.ID,

                            Common_RawItemFK = p.Common_RawItemFK,
                            //Common_ColorFK = p.Common_ColorFK, // == null ? default(int) : ms.Common_ColorFK),
                            //Common_Size = ms.GSM,
                            UnitPrice = p.PurchasingPrice,
                            //TotalPrice = p.PurchasingPrice * (remainingQty == 0 ? p.PurchaseQuantity : remainingQty),
                            Merchandising_StyleFK =
                                (int)(p.Merchandising_StyleID == null ? default(int) : p.Merchandising_StyleID),
                            Merchandising_StyleSlaveFK = (int)(p.Merchandising_StyleSlaveFK == null
                                ? default(int)
                                : p.Merchandising_StyleSlaveFK),
                            Procurement_PurchaseOrderSlaveFK = p.ID,
                            IsLocked = true,

                            FixAsset = false,
                            BinDefine = false,
                            IsActive = true

                        }).ToListAsync());

                    ////// ============= Received Item =============////
                    aStoreWithOutStyleReceived = await Task.Run(() => (
                        from s in _db.Store_StockIn
                        join sm in _db.Store_StockInMaster on s.Store_StockInMasterFK equals sm.ID //into ptemp
                        join i in _db.Common_RawItem on s.Common_RawItemFK equals i.ID

                        where s.Active && sm.Active && sm.PurchaseOrderFK == vmModel.PurchaseOrderFK && s.Store_StockInMasterFK == id

                        select new VmStoreInReceived
                        {
                            ID = vmModel.ID,
                            CID = vmModel.CID,
                            ChallanNo = vmModel.ChallanNo,
                            ChallanDate = vmModel.ChallanDate,
                            ReceivedDate = vmModel.ReceivedDate,
                            WarrentyDate = DateTime.Now,
                            Name = i.Name,
                            Description = s.Description,
                            StockType = "Production",
                            Remarks = vmModel.Remarks,

                            PurchaseQuantity = s.PurchaseQuantity,
                            RemainingQty = s.RemainingQty,
                            DamagedQty = s.DamagedQty,
                            ReceivedQty = s.ReceivedQty,

                            IPOFK = vmModel.PurchaseOrderFK,
                            ItemIDFK = s.Common_RawItemFK,
                            //StoreBinFK = vmModel.StoreBinFK,
                            CommonUnitFK = i.Common_UnitFK,
                            UnitName = i.Common_UnitFK == 0 ? "Not Set Name" : (_db.Common_Unit.FirstOrDefault(x => x.ID == i.Common_UnitFK).Name),
                            CommonStoreFK = vmModel.CommonStoreFK,
                            CommonSupplierFK = 1,
                            CommonCategoryFK = 1,
                            CommonSubCategoryFK = 1,

                            Common_RawItemFK = s.Common_RawItemFK,
                            //Common_ColorFK = (int)(ms.Common_ColorFK == null ? default(int) : ms.Common_ColorFK),
                            //Common_Size = ms.GSM,

                            IsLocked = true,

                            FixAsset = s.FixAsset,
                            BinDefine = s.BinDefine,
                            IsActive = true

                        }).ToListAsync());
                }
            }
            else if (_db.Store_StockInMaster.FirstOrDefault(x => x.ID == id).StoreInType == "Internal")
            {
                // for 2 internal 
                vmModel.ChallanNo = _db.Store_StockInMaster.FirstOrDefault(x => x.ID == id).ChallanNo;
                if (_db.Store_StockIn.Any(s => s.ChallanNo == vmModel.ChallanNo))
                {
                    remainingQty = _db.Store_StockIn.Where(s => s.ChallanNo == vmModel.ChallanNo).Min(s => s.RemainingQty);
                }
                {
                    //var aStoreWithOutStyle = new List<VmStoreIn>();
                aStoreWithOutStyle = await Task.Run(() => (
                    from s in _db.Store_StockOut
                    join sm in _db.Procurement_PurchaseOrderSlave on s.Procurement_PurchaseOrderSlaveFK equals sm.ID
                    join I in _db.Common_RawItem on s.Common_RawItemFK equals I.ID
                    join sc in _db.Common_RawSubCategory on I.Common_RawSubCategoryFK equals sc.ID
                    join c in _db.Common_RawCategory on sc.Common_RawCategoryFK equals c.ID

                    where s.Active && s.ChallanNo == vmModel.ChallanNo && s.ReceivedQty >
                        (_db.Store_StockIn.Any(x => x.Active && x.ChallanNo == s.ChallanNo)
                         ? _db.Store_StockIn.Where(x => x.Active && x.ChallanNo == s.ChallanNo && x.Common_RawItemFK == s.Common_RawItemFK)
                        .Select(y => new { ReceivedQty = y.ReceivedQty }).Sum(c => c.ReceivedQty)
                        : 0)
                    select new VmStoreIn
                    {
                        ID = vmModel.ID,
                        CID = vmModel.CID,
                        ChallanNo = vmModel.ChallanNo,
                        ChallanDate = vmModel.ChallanDate,
                        ReceivedDate = vmModel.ReceivedDate,
                        WarrentyDate = DateTime.Now,
                        Name = I.Name,
                        Description = s.Description,
                        StockType = "Production",
                        Remarks = vmModel.Remarks,
                        Store_RequisitionSlaveFK = s.Store_RequisitionSlaveFK,

                        PurchaseQuantity = sm.PurchaseQuantity, //s.ReceivedQty, //
                        RemainingQty = s.ReceivedQty - (_db.Store_StockIn.Any(x => x.Active && x.ChallanNo == s.ChallanNo)
                              ? _db.Store_StockIn.Where(x => x.Active && x.ChallanNo == s.ChallanNo && x.Common_RawItemFK == s.Common_RawItemFK)
                                  .Select(y => new { ReceivedQty = y.ReceivedQty }).Sum(c => c.ReceivedQty)
                              : 0),

                        DamagedQty = 0,
                        ReceivedQty = 0,                      

                        vmReceivedQty = (_db.Store_StockIn.Any(x => x.Active && x.ChallanNo == s.ChallanNo)
                              ? _db.Store_StockIn.Where(x => x.Active && x.ChallanNo == s.ChallanNo && x.Common_RawItemFK == s.Common_RawItemFK)
                                  .Select(y => new { ReceivedQty = y.ReceivedQty }).Sum(c => c.ReceivedQty)
                              : 0),

                        IPOFK = vmModel.PurchaseOrderFK,
                        ItemIDFK = s.Common_RawItemFK,
                        CommonStoreFK = vmModel.CommonStoreFK,
                        CommonSupplierFK = 1,
                        CommonCategoryFK = c.ID,
                        CommonSubCategoryFK = sc.ID,

                        Common_RawItemFK = s.Common_RawItemFK,
                        Common_ColorFK = s.Common_ColorFK,
                        Common_Size = 0, //s.GSM,
                        UnitPrice = sm.PurchasingPrice,
                        CommonUnitFK = s.CommonUnitFK,
                        UnitName = s.CommonUnitFK == 0 ? "Not Set Name" : (_db.Common_Unit.FirstOrDefault(x => x.ID == s.CommonUnitFK).Name),
                        Merchandising_StyleFK = s.Merchandising_StyleFK,
                        Merchandising_StyleSlaveFK = s.Merchandising_StyleSlaveFK,
                        Procurement_PurchaseOrderSlaveFK = s.Procurement_PurchaseOrderSlaveFK,
                        IsLocked = true,

                        FixAsset = s.FixAsset,
                        BinDefine = s.BinDefine,
                        IsActive = true

                    }).ToListAsync());

                    ////// ============= Received Item =============////
                    
                    aStoreWithOutStyleReceived = await Task.Run(() => (
                        from s in _db.Store_StockIn
                        join i in _db.Common_RawItem on s.Common_RawItemFK equals i.ID
                        where s.Active && s.ChallanNo == vmModel.ChallanNo 
                        group s by s.Common_RawItemFK into itemgroup
                        select new VmStoreInReceived
                        {
                            ID = vmModel.ID,
                            CID = vmModel.CID,
                            ChallanNo = vmModel.ChallanNo,
                            ChallanDate = vmModel.ChallanDate,
                            ReceivedDate = vmModel.ReceivedDate,
                            WarrentyDate = DateTime.Now,
                            Name = itemgroup.Max(x => x.Name),
                            Description = itemgroup.Max(x => x.Description),
                            StockType = "Production",
                            Remarks = vmModel.Remarks,

                            PurchaseQuantity = itemgroup.Max(x => x.PurchaseQuantity),
                            RemainingQty = itemgroup.Min(x => x.RemainingQty),
                            DamagedQty = itemgroup.Min(x => x.DamagedQty),
                            ReceivedQty = itemgroup.Sum(x => x.ReceivedQty),

                            IPOFK = vmModel.PurchaseOrderFK,
                            ItemIDFK = itemgroup.Key,
                            CommonStoreFK = vmModel.CommonStoreFK,
                            CommonSupplierFK = 1,
                            CommonCategoryFK = 1,
                            CommonSubCategoryFK = 1,

                            Common_RawItemFK = itemgroup.Key,
                            CommonUnitFK = itemgroup.Max(x => x.CommonUnitFK),
                           
                            IsLocked = true,
                            //FixAsset = itemgroup.Any(x => !x.FixAsset),
                            //BinDefine = itemgroup.Any(x => !x.BinDefine),
                            IsActive = true

                        }).ToListAsync());
                }
            }

            if (aStoreWithOutStyle.Any())
            {
                aStoreWithOutStyle.ForEach(a =>
                {
                    var color = _db.Common_Color.Where(x => x.ID == a.Common_ColorFK);
                    if (color.Any())
                    {
                        a.Name += "-" + color.FirstOrDefault().Name + "-" + a.Common_Size;
                    }
                    var catagory = _db.Common_RawCategory.Where(c => c.ID == a.CommonCategoryFK);
                    var fabric = "Fabric";
                    if (catagory.Any())
                    {
                        a.Name += catagory.FirstOrDefault().Name.ToLower() == fabric.ToLower() ? itemNameex : "";
                    }                 

                });
                vmmodel.DataLists = aStoreWithOutStyle.ToList();
            }

            if (aStoreWithOutStyleReceived.Any())
            {
                aStoreWithOutStyleReceived.ForEach(r =>
                {
                    var unit = _db.Common_Unit.Where(x => x.ID == r.CommonUnitFK);
                    if (unit.Any())
                    {
                        r.UnitName = unit.FirstOrDefault().Name;
                    }
                });
            }

            vmmodel.DataListsReceived = aStoreWithOutStyleReceived.ToList();
            vmmodel.SupplierList = await SupplierDropDownListAsync();
            vmmodel.SupplierPoList = await SupplierPoDropDownListAsync();
            vmmodel.UnitList = await UnitDropDownListAsync();
            vmmodel.PoList = await PoDropDownListAsync();
            vmmodel.StoreList = await StoreDropDownListAsync();
            vmmodel.CommonStoreFK = id == 0 ? 2 : id;
            return vmmodel;
        }

        public async Task<object> SaveMasterItem(VmStoreInMaster vmmodel)
        {
            var cid = CidServices.GetRefStoreInMaster(vmmodel.DepartmentId);
            Store_StockInMaster storeInMaster;
            int result = -1;
            if (vmmodel.StoreInType == "New")
            {
                storeInMaster = new Store_StockInMaster
                {
                    //ID = vmmodel.ID,
                    Active = true,
                    CID = cid,
                    ChallanNo = vmmodel.ChallanNo,
                    ChallanDate = vmmodel.ChallanDate,
                    ReceivedDate = vmmodel.ReceivedDate,
                    StockType = vmmodel.StockType,
                    StoreInType = vmmodel.StoreInType,
                    Description = vmmodel.Description,
                    PurchaseOrderFK = vmmodel.PurchaseOrderFK,
                    Common_SupplierFK = vmmodel.Common_SupplierFK,
                    //CommonStoreFK = vmmodel.CommonStoreFK,
                    Store_GeneralFK = vmmodel.Store_GeneralFK,
                    User = vmmodel.User
                };
            }
            else
            {
                storeInMaster = new Store_StockInMaster
                {
                    //ID = vmmodel.ID,
                    Active = true,
                    CID = cid,
                    ChallanNo = vmmodel.ChallanNo,
                    ChallanDate = vmmodel.ChallanDate,
                    ReceivedDate = vmmodel.ReceivedDate,
                    StockType = vmmodel.StockType,
                    StoreInType = vmmodel.StoreInType,
                    Description = vmmodel.Description,
                    //PurchaseOrderFK = vmmodel.PurchaseOrderFK,
                    //Common_SupplierFK = vmmodel.Common_SupplierFK,
                    //CommonStoreFK = vmmodel.CommonStoreFK,
                    Store_GeneralFK = vmmodel.Store_GeneralFK,
                    User = vmmodel.User
                };
            }
            try
            {
                _db.Store_StockInMaster.Add(storeInMaster);
                result = await _db.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
            }
            return result;
        }

        public async Task<object> SaveAllItemSlave(VmStoreIn vmStoreIn, int id)
        {
            double stockQtys = 0;
            double stockValues = 0;
            int common_SupplierFK = 0;
            int merchandising_StyleFK = 0;
            int procurement_PurchaseOrderSlaveFK = 0;
            var result = -1;
            var stockInType = "";
            var cid = CidServices.GetRefStoreInSlave(vmStoreIn.DepartmentId);
            string storeRef = "";
            decimal convertedAmount = 0;
            decimal DollarAmount = 0;

            if (id != 0)
            {
                var storeStockInMaster = _db.Store_StockInMaster.FirstOrDefault(x => x.ID == id);
                var storeGeneral = _db.Store_General.FirstOrDefault(x => x.ID == storeStockInMaster.Store_GeneralFK);

                foreach (var s in vmStoreIn.DataLists)
                {
                    double remainingQty = s.RemainingQty - s.ReceivedQty;
                    var storeStockIn = new Store_StockIn()
                    {
                        ID = s.ID,
                        CID = cid,
                        ChallanNo = s.ChallanNo,
                        ChallanDate = s.ChallanDate,
                        ReceivedDate = s.ReceivedDate,
                        WarrentyDate = s.WarrentyDate,
                        Name = s.Name,
                        Description = s.Description,
                        StockType = s.StockType,
                        StoreInType = s.StoreInType == "New" ? "Purchase" : "Internal",
                        Remarks = s.Remarks,

                        PurchaseQuantity = s.PurchaseQuantity,
                        RemainingQty = remainingQty,
                        DamagedQty = s.DamagedQty,
                        ReceivedQty = s.ReceivedQty,

                        IPOFK = s.IPOFK == 0 ? s.ChallanNoFK : s.IPOFK,
                        ItemIDFK = s.ItemIDFK,
                        StoreBinFK = s.StoreBinFK,
                        CommonUnitFK = s.CommonUnitFK,
                        CommonSupplierFK = s.CommonSupplierFK,
                        CommonStoreFK = vmStoreIn.CommonStoreFK,
                        CommonCategoryFK = s.CommonCategoryFK,
                        CommonSubCategoryFK = s.CommonSubCategoryFK,

                        Procurement_PurchaseOrderSlaveFK = s.Procurement_PurchaseOrderSlaveFK,
                        UnitPrice = s.UnitPrice,
                        TotalPrice = s.UnitPrice * s.ReceivedQty,
                        Merchandising_StyleFK = s.Merchandising_StyleFK,
                        Merchandising_StyleSlaveFK = s.Merchandising_StyleSlaveFK,
                        Common_ColorFK = s.Common_ColorFK == 0 ? 1 : s.Common_ColorFK,
                        Common_Size = s.Common_Size,

                        IsLocked = true,
                        FixAsset = s.FixAsset,
                        BinDefine = s.BinDefine,
                        Active = s.IsActive,
                        User = vmStoreIn.User,
                        Common_RawItemFK = s.ItemIDFK,
                        Store_GeneralFK = storeStockInMaster.Store_GeneralFK.Value,
                        Common_SupplierFK = s.CommonSupplierFK,
                        Store_StockInMasterFK = id
                    };

                    if (storeStockIn.ReceivedQty > 0)
                    {
                        _db.Store_StockIn.Add(storeStockIn);
                        result = await _db.SaveChangesAsync();

                        stockQtys += storeStockIn.ReceivedQty;
                        stockValues += storeStockIn.TotalPrice;
                        stockInType = storeStockIn.StoreInType;
                        common_SupplierFK = storeStockIn.Common_SupplierFK;
                        merchandising_StyleFK = storeStockIn.Merchandising_StyleFK;
                        procurement_PurchaseOrderSlaveFK = storeStockIn.Procurement_PurchaseOrderSlaveFK;
                    }
                }
                if (stockInType == "Internal")
                {
                    int supplierAccthead = _db.Common_Supplier.Find(common_SupplierFK).AccHeadID;
                    vmStoreIn.Procurement_PurchaseOrderSlaveFK = vmStoreIn.DataLists.Select(s => s.Procurement_PurchaseOrderSlaveFK).FirstOrDefault();
                    var po = _db.Procurement_PurchaseOrderSlave.Where(p => p.ID == vmStoreIn.Procurement_PurchaseOrderSlaveFK).FirstOrDefault();
                    var ConversionRate = (from ppo in _db.Procurement_PurchaseOrder
                                          join c in _db.Common_Currency on ppo.POType equals c.ID
                                          where ppo.ID == po.Procurement_PurchaseOrderFK
                                          select new
                                          {
                                              currencyValue = c.ConversionRateToBDT
                                          }).FirstOrDefault();

                    convertedAmount = Convert.ToDecimal(stockValues) * ConversionRate.currencyValue;
                    DollarAmount = Convert.ToDecimal(stockValues);

                    var store_StockOut = _db.Store_StockOut.FirstOrDefault(x => x.ChallanNo == vmStoreIn.DataLists.FirstOrDefault().ChallanNo);
                    var storeOutTypeFk = _db.Store_General.FirstOrDefault(x => x.ID == store_StockOut.Store_GeneralOutFK).Common_StoreTypeFK;
                    var StoreInTypeFk = _db.Store_General.FirstOrDefault(x => x.ID == store_StockOut.Store_GeneralInFK).Common_StoreTypeFK;

                    if (StoreInTypeFk == (int)InventoryTypeEnum.Finished && merchandising_StyleFK != 0)
                    {
                        convertedAmount = 0;
                        decimal unitPrice = _db.Merchandising_Style.FirstOrDefault(x => x.ID == merchandising_StyleFK).UnitPrice;
                        convertedAmount = (Convert.ToDecimal(stockQtys) * unitPrice) * Convert.ToDecimal(ConversionRate.currencyValue);
                    }

                    VmInventory vmInventory = new VmInventory();
                    vmInventory.DataList = vmStoreIn.DataLists.Select(s => new VmInventory
                    {
                        StoreTypeFk = storeGeneral.Common_StoreTypeFK.Value,
                        TransactionType = (int)StoreTransactionTypeEnum.In,
                        Store_StockInFK = storeStockInMaster.ID,
                        Common_RawItemFK = s.Common_RawItemFK,
                        Amount = (decimal)s.ReceivedQty,
                        Price = (decimal)s.UnitPrice,
                        Merchandising_StyleID = s.Merchandising_StyleFK,
                        Merchandising_StyleSlaveID = s.Merchandising_StyleSlaveFK,
                        Procurement_PurchaseInvoiceFK = s.Procurement_PurchaseOrderSlaveFK,
                        Procurement_PurchaseOrderSlaveID = s.Procurement_PurchaseOrderSlaveFK,
                        Merchandising_BOFID = 0,
                        Time = DateTime.Now,
                        User = s.User,
                        UserID = s.UserID,
                        Active = true,
                    }).ToList();

                    if (storeStockInMaster.ChallanNo != null)
                    {
                        storeRef = "Store - " + storeStockInMaster.ChallanNo;
                    }

                    if (storeOutTypeFk != StoreInTypeFk)
                    {
                        if (storeOutTypeFk == (int)InventoryTypeEnum.Raw && StoreInTypeFk == (int)InventoryTypeEnum.Wip)
                        {
                            await integrationservice.InventroyPush(vmInventory, InventoryTransactionTriggerTypeEnum.StoreStockIn, (int)DebitEnum.Wip, (int)CreditEnum.Rawinventory, convertedAmount, DollarAmount, ConversionRate.currencyValue, storeRef, (int)CostCenterEnum.General);
                        }
                        else if (storeOutTypeFk == (int)InventoryTypeEnum.Raw && StoreInTypeFk == (int)InventoryTypeEnum.Finished)
                        {
                            await integrationservice.InventroyPush(vmInventory, InventoryTransactionTriggerTypeEnum.StoreStockIn, (int)CreditEnum.Rawinventory, (int)DebitEnum.Finish, convertedAmount, DollarAmount, ConversionRate.currencyValue, storeRef, (int)CostCenterEnum.General);
                        }
                        else if (storeOutTypeFk == (int)InventoryTypeEnum.Wip && StoreInTypeFk == (int)InventoryTypeEnum.Raw)
                        {
                            await integrationservice.InventroyPush(vmInventory, InventoryTransactionTriggerTypeEnum.StoreStockIn, (int)CreditEnum.Wip, (int)DebitEnum.Rawinventory, convertedAmount, DollarAmount, ConversionRate.currencyValue, storeRef, (int)CostCenterEnum.General);
                        }
                        else if (storeOutTypeFk == (int)InventoryTypeEnum.Wip && StoreInTypeFk == (int)InventoryTypeEnum.Finished)
                        {
                            await integrationservice.InventroyPush(vmInventory, InventoryTransactionTriggerTypeEnum.StoreStockIn, (int)DebitEnum.Finish, (int)CreditEnum.Wip, convertedAmount, DollarAmount, ConversionRate.currencyValue, storeRef, (int)CostCenterEnum.General);
                        }
                        else if (storeOutTypeFk == (int)InventoryTypeEnum.Finished && StoreInTypeFk == (int)InventoryTypeEnum.Raw)
                        {
                            await integrationservice.InventroyPush(vmInventory, InventoryTransactionTriggerTypeEnum.StoreStockIn, (int)CreditEnum.Finish, (int)DebitEnum.Rawinventory, convertedAmount, DollarAmount, ConversionRate.currencyValue, storeRef, (int)CostCenterEnum.General);
                        }
                        else if (storeOutTypeFk == (int)InventoryTypeEnum.Finished && StoreInTypeFk == (int)InventoryTypeEnum.Wip)
                        {
                            await integrationservice.InventroyPush(vmInventory, InventoryTransactionTriggerTypeEnum.StoreStockIn, (int)CreditEnum.Finish, (int)DebitEnum.Wip, convertedAmount, DollarAmount, ConversionRate.currencyValue, storeRef, (int)CostCenterEnum.General);
                        }
                    }
                }
            }
            return result;
        }

        public List<object> GetChallanByStoreID(int id = 0)
        {
            var storeList = new List<object>();
            var dataList = _db.Store_StockOut.Where(x => x.Active && x.CID != null && x.Store_GeneralInFK == (id == 0 ? 2 : id)
             && x.ReceivedQty >
             (_db.Store_StockIn.Any(s => s.Active && s.ChallanNo == x.ChallanNo)
                            ? _db.Store_StockIn.Where(s => s.Active && s.ChallanNo == x.ChallanNo)
                                .Select(y => new { ReceivedQty = y.ReceivedQty }).Sum(c => c.ReceivedQty)
                            : 0)
            //&& !_db.Store_StockIn.Any(s => s.ChallanNo == x.ChallanNo && s.ReceivedQty >= x.ReceivedQty)
            ).GroupBy(x => x.ChallanNo)
                .Select(g => new
                {
                    ChallanNo = g.Key,
                    ID = g.Max(i => i.ID),
                    ChallanDate = g.Max(v => v.ChallanDate)
                }).ToList();

            foreach (var store in dataList)
            {
                storeList.Add(new { Text = store.ChallanNo, Value = store.ID, Date = store.ChallanDate.ToShortDateString() });
            }
            //var data = new SelectList(storeList, "Value", "Text");
            return storeList;
        }

        ////////////// StoreIn Master //////////////

        #region app
        //public async Task<int> Store_QCInformationAdd(VMStore_QCInformation model)
        //{
        //    var result = -1;

        //    Store_QCInformation QCInformation = new Store_QCInformation();

        //    QCInformation.DeviceId = model.DeviceId;
        //    QCInformation.Remarks = model.Remark;
        //    QCInformation.Merchandising_StyleIdFK = model.Merchandising_StyleIdFK;
        //    QCInformation.Merchandising_StyleSetPackFK = model.Merchandising_StyleSetPackFK;
        //    QCInformation.Common_ColorFK = model.Common_ColorFK;
        //    QCInformation.Common_SizeFK = model.Common_SizeFK;
        //    QCInformation.Common_ProductionLineFK = model.Common_ProductionLineFK;
        //    QCInformation.QCQuantity = model.QCQuantity;
        //    QCInformation.RejectQuantityOne = model.RejectQuantityOne;
        //    QCInformation.RejectQuantityTwo = model.RejectQuantityTwo;
        //    QCInformation.RejectQuantityThree = model.RejectQuantityThree;
        //    QCInformation.RejectQuantityFour = model.RejectQuantityFour;
        //    QCInformation.RejectQuantityFive = model.RejectQuantityFive;
        //    QCInformation.RejectQuantitySix = model.RejectQuantitySix;
        //    QCInformation.RejectQuantityTotal = model.RejectQuantityTotal;
        //    _db.Store_QCInformation.Add(QCInformation);
        //    await _db.SaveChangesAsync();

        //    return result;
        //}

        #endregion
        #region app
        public async Task<int> Store_QCInformationAdd(VMStore_QCInformation model)
        {
            var result = -1;

            Store_QCInformation QCInformation = new Store_QCInformation
            {
                DeviceId = model.DeviceId,
                Remarks = model.Remark,
                Merchandising_StyleIdFK = model.Merchandising_StyleIdFK,
                Merchandising_StyleSetPackFK = model.Merchandising_StyleSetPackFK,
                Common_ColorFK = model.Common_ColorFK,
                Common_SizeFK = model.Common_SizeFK,
                Common_ProductionLineFK = model.Common_ProductionLineFK,

                QCQuantity = model.QCQuantity,
                RejectQuantityOne = model.RejectQuantityOne,
                RejectQuantityTwo = model.RejectQuantityTwo,
                RejectQuantityThree = model.RejectQuantityThree,
                RejectQuantityFour = model.RejectQuantityFour,
                RejectQuantityFive = model.RejectQuantityFive,
                RejectQuantitySix = model.RejectQuantitySix,
                RejectQuantityTotal = model.RejectQuantityTotal,
                Time = DateTime.Now,
                UserID = 1,
                Active = true,
                EntryDate = model.EntryDate
            };
            try
            {
                _db.Store_QCInformation.Add(QCInformation);
                await _db.SaveChangesAsync();
            }
            catch (Exception ex)
            {

            }

            return result;
        }

        #endregion
        public VmStoreReport ReportStore(int id = 0)
        {
            var vmmodel = new VmStoreReport();

            var data = (from ri in _db.Common_RawItem
                        join si in _db.Store_StockIn on ri.ID equals si.ItemIDFK
                        where ri.Active
                        //group s by s into g
                        select new VmStoreReport
                        {
                            SupplierName = "Test Supplier",
                            //StyleName = ms.CID + " " + s.Name,
                            BuyerName = ri.Name,
                            ItemCode = ri.ID.ToString(),
                            ItemName = ri.Name,
                            OpenningQty = 10000,
                            StoreReceivedQty = si.ReceivedQty,
                            TotalQty = (si.ReceivedQty),

                            //StoreOutQty = o.ReceivedQty,
                            //StoreInQty = o.ReceivedQty,
                            //MainStoreQty = o.ReceivedQty,
                            Remarks = ri.Remarks

                        }).ToList();
            vmmodel.Datalist = data;
            return vmmodel;
        }
        public VmStoreReport StoreInItemWiseReport(VmStoreReport vmodel)
        {
            var vmmodel = new VmStoreReport
            {
                Datalist = new List<VmStoreReport>()
            };

            List<Store_StockIn> storeIn = new List<Store_StockIn>();
            List<Store_StockIn> storeInMain = new List<Store_StockIn>();
            List<Store_StockOut> storeOut = new List<Store_StockOut>();

            if (_db.Store_StockIn.Any())
            {
                storeIn = (from ri in _db.Common_RawItem
                           join si in _db.Store_StockIn on ri.ID equals si.ItemIDFK
                           join po in _db.Procurement_PurchaseOrder on si.IPOFK equals po.ID
                           join pos in _db.Procurement_PurchaseOrderSlave on po.ID equals pos.Procurement_PurchaseOrderFK
                           join u in _db.Common_Unit on ri.Common_UnitFK equals u.ID

                           where ri.Active && si.Active && po.Active && pos.Active && po.Status == 5
                           && po.OriginType == vmodel.ReportID
                           group new { ri, si, po, pos, u } by ri.ID into item

                           select new Store_StockIn
                           {
                               ItemIDFK = item.First().ri.ID,
                               ReceivedQty = item.First().si.ReceivedQty
                           }).ToList();

            }

            if (_db.Store_StockOut.Any())
            {
                storeOut = _db.Store_StockOut.Where(x => x.Active && x.Store_GeneralFK == 3
                ).GroupBy(i => i.ItemIDFK).Select(sin => new Store_StockOut
                {
                    ItemIDFK = sin.First().ItemIDFK,
                    ChallanNo = sin.First().ChallanNo,
                    ReceivedQty = sin.First().ReceivedQty
                }).ToList<Store_StockOut>();
            }
            var ReportTypeName = "Regular";
            if (vmodel.ReportID == 1)
            {
                ReportTypeName = "BOM/BOF";
            }
            else if (vmodel.ReportID == 2)
            {
                ReportTypeName = "BOM";
            }
            else if (vmodel.ReportID == 3)
            {
                ReportTypeName = "BOF";
            }


            var data = (from ri in _db.Common_RawItem
                        join si in _db.Store_StockIn on ri.ID equals si.ItemIDFK
                        join po in _db.Procurement_PurchaseOrder on si.IPOFK equals po.ID
                        join pos in _db.Procurement_PurchaseOrderSlave on po.ID equals pos.Procurement_PurchaseOrderFK
                        join u in _db.Common_Unit on ri.Common_UnitFK equals u.ID

                        where ri.Active && si.Active && po.Active && pos.Active && po.Status == 5
                        && po.OriginType == vmodel.ReportID
                        group new { ri, si, po, pos, u } by ri.ID into item

                        select new VmStoreReport
                        {
                            //ReportID = item.First().ri.ID,
                            ItemName = item.First().ri.Name + "-" + item.First().ri.ID,
                            Unit = item.First().u.Name,
                            OpenningQty = item.First().pos.PurchaseQuantity,
                            StoreInQty = item.Sum(x => x.si.ReceivedQty),

                            //StoreInQty = storeIn.FirstOrDefault(x => x.ItemIDFK == item.First().si.ID).ReceivedQty == null ? 0  : storeIn.FirstOrDefault(x => x.ItemIDFK == item.First().si.ID).ReceivedQty,
                            StoreOutQty = storeOut.FirstOrDefault(x => x.ItemIDFK == item.First().si.ID).ReceivedQty == 0 ? 0 : storeOut.FirstOrDefault(x => x.ItemIDFK == item.First().si.ID).ReceivedQty,

                            ConsumeQty = 0,
                            BalanceQty = storeOut.FirstOrDefault(x => x.ItemIDFK == item.First().si.ID && x.Store_GeneralFK == 0).ReceivedQty == 0 ? 0 : storeOut.FirstOrDefault(x => x.ItemIDFK == item.First().si.ID).ReceivedQty,

                            TotalQty = storeOut.FirstOrDefault(x => x.ItemIDFK == item.First().si.ID).ReceivedQty == 0 ? 0 :
                            storeOut.FirstOrDefault(x => x.ItemIDFK == item.First().si.ID && x.Store_GeneralFK == 0).ReceivedQty == 0 ? 0 : storeOut.FirstOrDefault(x => x.ItemIDFK == item.First().si.ID).ReceivedQty,

                        }).ToList();

            vmmodel.Datalist = data;
            vmmodel.ReportTypeName = ReportTypeName;
            return vmmodel;
        }

        public object ReportStoreInventoryType(VmStoreReport vmodel)
        {
            var vmmodel = new VmStoreReport
            {
                Datalist = new List<VmStoreReport>()
            };

            List<Store_StockIn> storeIn = new List<Store_StockIn>();
            List<Store_StockIn> storeInMain = new List<Store_StockIn>();
            List<Store_StockOut> storeOut = new List<Store_StockOut>();

            if (_db.Store_StockIn.Any())
            {
                storeIn = (from ri in _db.Common_RawItem
                           join si in _db.Store_StockIn on ri.ID equals si.ItemIDFK
                           join po in _db.Procurement_PurchaseOrder on si.IPOFK equals po.ID
                           join pos in _db.Procurement_PurchaseOrderSlave on po.ID equals pos.Procurement_PurchaseOrderFK
                           join u in _db.Common_Unit on ri.Common_UnitFK equals u.ID

                           where ri.Active && si.Active && po.Active && pos.Active && po.Status == 5 && si.Merchandising_StyleFK == vmodel.Merchandising_StyleFK
                           && po.OriginType == vmodel.ReportID
                           group new { ri, si, po, pos, u } by ri.ID into item

                           select new Store_StockIn
                           {
                               ItemIDFK = item.First().ri.ID,
                               ReceivedQty = item.First().si.ReceivedQty
                           }).ToList();

            }

            if (_db.Store_StockOut.Any())
            {
                storeOut = _db.Store_StockOut.Where(x => x.Active && x.Store_GeneralFK == 3 && x.Merchandising_StyleFK == vmodel.Merchandising_StyleFK
                ).GroupBy(i => i.ItemIDFK).Select(sin => new Store_StockOut
                {
                    ItemIDFK = sin.First().ItemIDFK,
                    ChallanNo = sin.First().ChallanNo,
                    ReceivedQty = sin.First().ReceivedQty
                }).ToList<Store_StockOut>();
            }

            var dataraw = (from si in _db.Store_StockIn
                           join sg in _db.Store_General on si.Store_GeneralFK equals sg.ID
                           join ri in _db.Common_RawItem on si.Common_RawItemFK equals ri.ID
                           join po in _db.Procurement_PurchaseOrder on si.IPOFK equals po.ID
                           join pos in _db.Procurement_PurchaseOrderSlave on po.ID equals pos.Procurement_PurchaseOrderFK
                           join u in _db.Common_Unit on ri.Common_UnitFK equals u.ID

                           where ri.Active && si.Active && si.Active && sg.Common_StoreTypeFK == 1 && si.Merchandising_StyleFK == vmodel.Merchandising_StyleFK
                           group new { si, sg, ri, pos, u } by new { ri.ID, sg.Common_StoreTypeFK } into item
                           select new VmStoreReport
                           {
                               ReportID = (int)item.Max(x => x.sg.Common_StoreTypeFK),
                               ItemName = item.First().ri.Name + "-" + item.First().ri.ID,
                               Unit = item.First().u.Name,
                               OpenningQty = item.First().pos.PurchaseQuantity,
                               StoreInQty = item.First().si.ReceivedQty,

                               StoreOutQty = storeOut.FirstOrDefault(x => x.ItemIDFK == item.First().si.ID).ReceivedQty == 0 ? 0 : storeOut.FirstOrDefault(x => x.ItemIDFK == item.First().si.ID).ReceivedQty,

                               ConsumeQty = 0,
                               BalanceQty = storeOut.FirstOrDefault(x => x.ItemIDFK == item.First().si.ID && x.Store_GeneralFK == 0).ReceivedQty == 0 ? 0 : storeOut.FirstOrDefault(x => x.ItemIDFK == item.First().si.ID).ReceivedQty,

                               TotalQty = storeOut.FirstOrDefault(x => x.ItemIDFK == item.First().si.ID).ReceivedQty == 0 ? 0 :
                               storeOut.FirstOrDefault(x => x.ItemIDFK == item.First().si.ID && x.Store_GeneralFK == 0).ReceivedQty == 0 ? 0 : storeOut.FirstOrDefault(x => x.ItemIDFK == item.First().si.ID).ReceivedQty,

                           }).ToList();

            var datawip = (from si in _db.Store_StockIn
                           join sg in _db.Store_General on si.Store_GeneralFK equals sg.ID
                           join ri in _db.Common_RawItem on si.Common_RawItemFK equals ri.ID
                           join po in _db.Procurement_PurchaseOrder on si.IPOFK equals po.ID
                           join pos in _db.Procurement_PurchaseOrderSlave on po.ID equals pos.Procurement_PurchaseOrderFK
                           join u in _db.Common_Unit on ri.Common_UnitFK equals u.ID

                           where ri.Active && si.Active && si.Active && sg.Common_StoreTypeFK == 2 && si.Merchandising_StyleFK == vmodel.Merchandising_StyleFK
                           group new { si, sg, ri, pos, u } by new { ri.ID, sg.Common_StoreTypeFK } into item
                           select new VmStoreReport
                           {
                               ReportID = (int)item.Max(x => x.sg.Common_StoreTypeFK),
                               ItemName = item.First().ri.Name + "-" + item.First().ri.ID,
                               Unit = item.First().u.Name,
                               OpenningQty = 0, //item.First().pos.PurchaseQuantity,
                               StoreInQty = item.First().si.ReceivedQty,

                               StoreOutQty = storeOut.FirstOrDefault(x => x.ItemIDFK == item.First().si.ID).ReceivedQty == 0 ? 0 : storeOut.FirstOrDefault(x => x.ItemIDFK == item.First().si.ID).ReceivedQty,

                               ConsumeQty = 0,
                               BalanceQty = storeOut.FirstOrDefault(x => x.ItemIDFK == item.First().si.ID && x.Store_GeneralFK == 0).ReceivedQty == 0 ? 0 : storeOut.FirstOrDefault(x => x.ItemIDFK == item.First().si.ID).ReceivedQty,

                               TotalQty = storeOut.FirstOrDefault(x => x.ItemIDFK == item.First().si.ID).ReceivedQty == 0 ? 0 :
                               storeOut.FirstOrDefault(x => x.ItemIDFK == item.First().si.ID && x.Store_GeneralFK == 0).ReceivedQty == 0 ? 0 : storeOut.FirstOrDefault(x => x.ItemIDFK == item.First().si.ID).ReceivedQty,

                           }).ToList();

            var datarawwip = (from si in _db.Store_StockIn
                              join sg in _db.Store_General on si.Store_GeneralFK equals sg.ID
                              join ri in _db.Common_RawItem on si.Common_RawItemFK equals ri.ID
                              join po in _db.Procurement_PurchaseOrder on si.IPOFK equals po.ID
                              join pos in _db.Procurement_PurchaseOrderSlave on po.ID equals pos.Procurement_PurchaseOrderFK
                              join u in _db.Common_Unit on ri.Common_UnitFK equals u.ID

                              where ri.Active && si.Active && si.Active && sg.Common_StoreTypeFK == 3 && si.Merchandising_StyleFK == vmodel.Merchandising_StyleFK
                              group new { si, sg, ri, pos, u } by new { ri.ID, sg.Common_StoreTypeFK } into item
                              select new VmStoreReport
                              {
                                  ReportID = (int)item.Max(x => x.sg.Common_StoreTypeFK),
                                  ItemName = item.First().ri.Name + "-" + item.First().ri.ID,
                                  Unit = item.First().u.Name,
                                  OpenningQty = 0, //item.First().pos.PurchaseQuantity,
                                  StoreInQty = item.First().si.ReceivedQty,

                                  StoreOutQty = storeOut.FirstOrDefault(x => x.ItemIDFK == item.First().si.ID).ReceivedQty == 0 ? 0 : storeOut.FirstOrDefault(x => x.ItemIDFK == item.First().si.ID).ReceivedQty,

                                  ConsumeQty = 0,
                                  BalanceQty = storeOut.FirstOrDefault(x => x.ItemIDFK == item.First().si.ID && x.Store_GeneralFK == 0).ReceivedQty == 0 ? 0 : storeOut.FirstOrDefault(x => x.ItemIDFK == item.First().si.ID).ReceivedQty,

                                  TotalQty = storeOut.FirstOrDefault(x => x.ItemIDFK == item.First().si.ID).ReceivedQty == 0 ? 0 :
                               storeOut.FirstOrDefault(x => x.ItemIDFK == item.First().si.ID && x.Store_GeneralFK == 0).ReceivedQty == 0 ? 0 : storeOut.FirstOrDefault(x => x.ItemIDFK == item.First().si.ID).ReceivedQty,

                              }).ToList();

            vmmodel.Datalistraw = dataraw;
            vmmodel.Datalistwip = datawip;
            vmmodel.Datalistrawwip = datarawwip;
            vmmodel.ReportTypeName = vmodel.StyleName;
            return vmmodel;
        }
        internal string GetUserDept(int id)
        {
            var data = (from u in _db.User_User
                        join d in _db.User_Department on u.User_DepartmentFK equals d.ID
                        where u.ID == id
                        select new
                        {
                            name = d.Name.FirstOrDefault()
                        }).ToString();

            return data;
        }

        public async Task<VmAssetDepreciation> GetAssetDepreciationAll(int id)
        {
            var assetRegister = new VmAssetDepreciation();
            var data = await Task.Run(() =>
            (from r in _db.Store_StockRegister
             join i in _db.Common_RawItem on r.RawItemFK equals i.ID
             join b in _db.Common_Brand on r.CommonBrandFK equals b.ID
             join m in _db.Common_Model on r.CommonModelFK equals m.ID
             join u in _db.Common_Unit on r.CommonUnitFK equals u.ID
             //join e in _db.HRMS_Employee on r.HREmployeeFK equals e.ID
             //join d in _db.User_Department on r.HREmployeeDepartmentFK equals d.ID
             where r.Active //&&  _db.Store_StockIn.Where(x => x.FixAsset)
             select new VmAssetDepreciation
             {
                 ID = r.ID,
                 CID = r.CID,
                 ItemSLNO = r.ItemSLNO,
                 ItemName = i.Name,
                 UnitPrice = r.UnitPrice,
                 LotQty = r.LotQty,
                 LifeTime = r.LifeTime,
                 CommonUnitName = u.Name,
                 CommonBrandName = b.Name,
                 CommonModelName = m.Name,
                 DepartmentName = "Admin",//d.Name,
                 EmployeeName = "Rafiq",//e.Name,
                 Remarks = r.Remarks,
             }).ToListAsync());
            assetRegister.DataLists = data;
            return assetRegister;
        }
        public async Task<VmAssetDepreciation> GetAssetDepreciation(VmAssetDepreciation s, string controller, string action)
        {
            var brandName = _db.Common_Brand.Find(s.CommonBrandFK).Name;
            var modelName = _db.Common_Model.Find(s.CommonModelFK).Name;
            var ids = (from r in _db.Store_StockRegister select r.StockInFK).Contains(28);

            var dataItems = await Task.Run(() => (from sin in _db.Store_StockIn
                                                  join pr in _db.Procurement_PurchaseOrderSlave on sin.ItemIDFK equals pr.Common_RawItemFK
                                                  join i in _db.Common_RawItem on sin.ItemIDFK equals i.ID into tempItem
                                                  join u in _db.Common_Unit on sin.CommonUnitFK equals u.ID into tempUnit
                                                  from item in tempItem.DefaultIfEmpty()
                                                  from unit in tempUnit.DefaultIfEmpty()
                                                      //where sin.Active && sin.FixAsset && !(from r in _db.Store_StockRegister select r.StockInFk).Contains(sin.ID)
                                                  where sin.Active && sin.FixAsset && sin.ItemIDFK == s.RawItemFK && !_db.Store_StockRegister.Any(x => x.StockInFK == sin.ID)
                                                  select new VmAssetDepreciation
                                                  {
                                                      ID = s.ID,
                                                      CID = "",
                                                      ItemSLNO = item.Name.ToUpper().Substring(0, 3),
                                                      ItemName = item.Name + " " + brandName + " " + modelName,
                                                      StockInFk = sin.ID,
                                                      LotQty = sin.ReceivedQty,
                                                      UnitPrice = pr.PurchasingPrice,
                                                      MinPrice = pr.PurchasingPrice,
                                                      User = "UserOne",
                                                      LifeTime = 1,
                                                      Remarks = "Test Data",
                                                      Status = 1,
                                                      CommonDepartmentFK = 1,
                                                      EmployeeFK = 1,
                                                      ReceivedDate = sin.ReceivedDate,
                                                      RawItemFK = s.RawItemFK,
                                                      CommonBrandFK = s.CommonBrandFK,
                                                      CommonModelFK = s.CommonModelFK,
                                                      CommonUnitFK = unit.ID,
                                                      CommonBrandName = brandName,
                                                      CommonModelName = modelName,
                                                      CommonUnitName = unit.Name,
                                                      CommonDepreciationFK = 1
                                                  }).FirstOrDefault());

            var list = new List<VmAssetDepreciation>();
            var vmStoreAsset = new VmAssetDepreciation
            {
                DataLists = new List<VmAssetDepreciation>()
            };
            if (dataItems == null) return vmStoreAsset;
            for (var i = 0; i < dataItems.LotQty; i++)
            {
                list.Add(dataItems);
            }
            var vmStoreAssets = new VmAssetDepreciation
            {
                DataLists = list
            };
            return vmStoreAssets;
        }
        public async Task<int> SaveAllAssetDepreciation(VmAssetDepreciation obj, string action, string controller, int id)
        {
            var result = 0;
            var assetList = new List<Store_AssetDepreciation>();
            foreach (var assetRegister in obj.DataLists)
            {
                var reg = new Store_AssetDepreciation
                {
                    ID = assetRegister.ID,
                    CID = assetRegister.CID,
                    ItemSLNO = assetRegister.ItemSLNO,
                    StockInFK = assetRegister.StockInFk,
                    UnitPrice = assetRegister.UnitPrice,
                    LotQty = assetRegister.LotQty,
                    MinPrice = assetRegister.MinPrice,
                    User = assetRegister.User,
                    UserID = assetRegister.UserID,
                    LifeTime = assetRegister.LifeTime,
                    Remarks = assetRegister.Remarks,
                    Status = 1,
                    CommonStoreFK = id == 0 ? 2 : 0,
                    RawItemFK = assetRegister.RawItemFK,
                    CommonBrandFK = assetRegister.CommonBrandFK,
                    CommonModelFK = assetRegister.CommonModelFK,
                    CommonUnitFK = assetRegister.CommonUnitFK,
                    HREmployeeFK = assetRegister.EmployeeFK,
                    HREmployeeDepartmentFK = assetRegister.CommonDepartmentFK,
                    CommonDepreciationFK = assetRegister.CommonDepreciationFK,
                    HREmployeeReceivedDate = assetRegister.ReceivedDate
                };
                assetList.Add(reg);
            }
            _db.Store_AssetDepreciation.AddRange(assetList);
            result = await _db.SaveChangesAsync();
            return result;
        }

    }
}
