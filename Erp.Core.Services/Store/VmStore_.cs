﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Erp.Core.Entity.User;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Erp.Core.Services.Store
{
    public class VmStoreGeneral : BaseVM 
    {
        [DisplayName("Store Name")]
        [Required(ErrorMessage = "Store Name is Required")]
        public string StoreName { get; set; }
        public string Location { get; set; }
        public string Department { get; set; }

        [DisplayName("Business Unit")]
        [Required(ErrorMessage = "Business Unit is Required")]
        public int HRMS_BusinessUnitFK { get; set; }

        [DisplayName("Unit Name")]
        [Required(ErrorMessage = "Unit Name is Required")]
        public int HRMS_UnitFK { get; set; }

        [DisplayName("Department Name")]
        [Required(ErrorMessage = "Department Name is Required")]
        public int User_DepartmentFK { get; set; }

        [DisplayName("Store Type")]
        [Required(ErrorMessage = "Store Type is Required")]
        public int Common_StoreTypeFK { get; set; }   
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }
        public IEnumerable<VmStoreGeneral> DataList { get; set; }
         
        public SelectList BusinessUnitList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        public SelectList UnitList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        public SelectList DepartmentList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        public SelectList StoreTypeList { get; set; } = new SelectList(new List<object>(), "Value", "Text");

    }

    public class VmStoreStockIn:BaseVM
    {
        [DisplayName("PO Number")]
        [Required(ErrorMessage = "PO Number is Required")]
        public int IPOFk { get; set; }

        [DisplayName("Supplier Name")]
        public string SupplierName { get; set; }

        [DisplayName("Challan No")]
        public string ChallanNO { get; set; }

        [DisplayName("Challan Date")]
        public string ChallanDate { get; set; }

        [DisplayName("Receive Date")]
        public string ReceiveDate { get; set; }
        [DisplayName("Bin")]
        [Required(ErrorMessage = "Bin is Required")]
        public int BinFk { get; set; }

        public List<VmStoreStockInItems> Items { get; set; }
        public IEnumerable<VmStoreStockIn> DataList { get; set; }
    }

    public class VmStoreStockInItems : BaseVM
    {
        public string ItemName { get; set; }
        public string Unit { get; set; }
        public string Description { get; set; }
        public decimal PurchaseQty { get; set; }
        public decimal RecieveQty { get; set; }
        public decimal DamagedQty { get; set; }
        public DateTime? WarrantyDate { get; set; }
    }

    public class VmStoreStockOut:BaseVM
    {
        [DisplayName("Requisition Number")]
        [Required(ErrorMessage = "Requisition Number is Required")]
        public int RequisitionFk { get; set; }

        [DisplayName("Transfer To")]
        public string TransferTo { get; set; }

        [DisplayName("Transfer Date")]
        public DateTime TransferDate { get; set; }
        [DisplayName("Requisition Date")]
        public DateTime RequisitionDate { get; set; }
        [DisplayName("Bin")]
        [Required(ErrorMessage = "Bin is Required")]
        public int BinFk { get; set; }

        [DisplayName("Contact Person")]
        public string ContactPerson { get; set; }

        [DisplayName("Reference No")]
        public string Reference { get; set; }

        public List<VmStoreStockOutItems> Items { get; set; }
        public IEnumerable<VmStoreStockOut> DataList { get; set; }
    }

    public class VmStoreStockOutItems : BaseVM
    {
        public string ItemName { get; set; }
        public string Unit { get; set; }
        public string Description { get; set; }
        public decimal RequiredQty { get; set; }
        public decimal RecieveQty { get; set; }
        public decimal StoreBalanceQty { get; set; }
        public decimal DeliverQty { get; set; }
    }
}
