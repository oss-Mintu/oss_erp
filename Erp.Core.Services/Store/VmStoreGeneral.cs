﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Erp.Core.Entity.Common;
using Erp.Core.Entity.Procurement;
using Erp.Core.Entity.Store;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Erp.Core.Services.Store
{
    public class VmStore : BaseVM
    {
        public string StoreName { get; set; }
        public string Location { get; set; }
        public string Description { get; set; }
        public int Capacity { get; set; }
        public int CommonUnitFK { get; set; }
        public List<VmStoreBin> Bin { get; set; }
        public int? User_DepartmentFK { get; set; }
        //public User_Department User_Department { get; set; }
    }
    public class VmStoreBin : BaseVM
    {
        [DisplayName("BIN Number")]
        //[Required(ErrorMessage = "BIN is Required")]
        public string CID { get; set; }

        [DisplayName("Line/Row in a Section")]
        [Range(1, int.MaxValue)]
        public int Row { get; set; }
        [Range(1, int.MaxValue)]
        public int Shelf { get; set; }
        [DisplayName("Rack Number")]
        public int Rack { get; set; }
        [Range(1, int.MaxValue)]
        public int Capacity { get; set; }
        public string Remarks { get; set; }

        [DisplayName("Column in a Rack")]
        [Range(1, int.MaxValue)]
        public int TotalShelf { get; set; }
        [DisplayName("Row in a Rack")]
        [Range(1, int.MaxValue)]
        public int TotalRow { get; set; }

        [Range(1, int.MaxValue)]
        public int Dimension { get; set; }
        [DisplayName("Unit Name")]
        //[Required(ErrorMessage = "Unit is Required")]
        public int CommonUnitFK { get; set; }
        [DisplayName("Unit Name")]
        public string CommonUnitName { get; set; }

        [DisplayName("Store Name")]
        [Required(ErrorMessage = "Store Name is Required")]
        public int StoreGeneralFK { get; set; }
        [DisplayName("Store Name")]
        public string StoreGeneralName { get; set; }

        [DisplayName("Section Name")]
        public int SectionFK { get; set; }

        public string Section { get; set; }
        public IEnumerable<VmStoreBin> DataList { get; set; }
    }

    public class VmStoreAssetRegister : BaseVM
    {
        [DisplayName("Product Sl No")]
        public string CID { get; set; }
        [DisplayName("Asset Tag No")]
        public string ItemSLNO { get; set; }
        [DisplayName("Item Name")]
        public string ItemName { get; set; }

        [DisplayName("Lot Qty")]
        public double LotQty { get; set; }
        [DisplayName("Unit Price")]
        public double UnitPrice { get; set; }
        public double MinPrice { get; set; }
        [DisplayName("Life Time Year's")]
        public int LifeTime { get; set; }
        public string Remarks { get; set; }
        public int Status { get; set; }
        [DisplayName("Store Name")]
        [Required(ErrorMessage = "Store Name is Required")]
        public int StoreGeneralFK { get; set; }
        [DisplayName("Department Name")]
        public int CommonDepartmentFK { get; set; }
        [DisplayName("Emplyee Name")]
        public int EmployeeFK { get; set; }
        public DateTime ReceivedDate { get; set; }

        public int CommonStoreFK { get; set; }
        public int StockInFk { get; set; }
        [DisplayName("Unit Name")]
        public int CommonUnitFK { get; set; }
        [DisplayName("Brand Name")]
        [Required(ErrorMessage = "Brand is Required")]
        public int CommonBrandFK { get; set; }
        [Required(ErrorMessage = "Model is Required")]
        [DisplayName("Model Name")]
        public int CommonModelFK { get; set; }
        [DisplayName("Item Name")]
        [Required(ErrorMessage = "Item Name is Required")]
        public int RawItemFK { get; set; }

        public int CommonDepreciationFK { get; set; }

        public string DepartmentName { get; set; }
        public string EmployeeName { get; set; }
        [DisplayName("Unit Name")]
        public string CommonUnitName { get; set; }
        [DisplayName("Brand Name")]
        public string CommonBrandName { get; set; }
        [DisplayName("Model Name")]
        public string CommonModelName { get; set; }
        public List<VmStoreAssetRegister> DataLists { get; set; }
        public IEnumerable<VmStoreAssetRegister> DataListasync { get; set; }
        public SelectList StoreList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        public SelectList RawItemList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        public SelectList BrandList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        public SelectList ModelList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
    }

    public class VmStoreSection : BaseVM
    {
        public string SectionName { get; set; }
        public string Description { get; set; }
        public int Capacity { get; set; }
        public int CommonUnitFK { get; set; }
        public List<VmStoreSection> SectionList { get; set; }
        [DisplayName("Store Name")]
        public int? AStore_GeneralFK { get; set; }
        public string StoreName { get; set; }
    }
    public class VmStoreOutPersonal : BaseVM
    {
        public string CID { get; set; }
        [DisplayName("Challan Number")]
        [Required(ErrorMessage = "Challan Number is Required")]
        public string ChallanNo { get; set; }

        [DataType(DataType.Date)]
        [DisplayName("Issue Date")]
        public DateTime ReceivedDate { get; set; } = DateTime.Now;

        public string Name { get; set; }
        public string Remarks { get; set; }

        [Required(ErrorMessage = "Issue Qty will be Getter Then 0")]
        [DisplayName(" Issue Qty ")]
        [Range(1, double.MaxValue)]
        public double ReceivedQty { get; set; } = 1;

        [DisplayName("Unit Name")]
        public int CommonUnitFK { get; set; }
        public int ItemIDFK { get; set; }
        [DisplayName("Purchase Order Number")]

        public int CommonStoreFK { get; set; }
        public int StoreBinFK { get; set; }

        [DisplayName("Store Name")]
        [Required(ErrorMessage = "Store Name is Required")]
        public int StoreGeneralFK { get; set; }
        [DisplayName("Department Name")]
        public int CommonDepartmentFK { get; set; }
        [DisplayName("Employee Name")]
        public int EmployeeFK { get; set; }

        [DisplayName("Brand Name")]
        [Required(ErrorMessage = "Brand is Required")]
        public int CommonBrandFK { get; set; }
        [Required(ErrorMessage = "Model is Required")]
        [DisplayName("Model Name")]
        public int CommonModelFK { get; set; }

        [DisplayName("Item Name")]
        [Required(ErrorMessage = "Item Name is Required")]
        public int RawItemFK { get; set; }

        [DisplayName(" Asset Tag Number ")]
        public int CommonAssetFK { get; set; }
        public string ItemName { get; set; }
        public string DepartmentName { get; set; }
        public string EmployeeName { get; set; }
        [DisplayName("Unit Name")]
        public string UnitName { get; set; }
        [DisplayName("Brand Name")]
        public string BrandName { get; set; }
        [DisplayName("Model Name")]
        public string ModelName { get; set; }
        [DisplayName("Store Name")]
        public string StoreName { get; set; }
        public List<VmStoreOutPersonal> DataLists { get; set; }
        public IEnumerable<VmStoreOutPersonal> DataListasync { get; set; }
        public SelectList StoreList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        public SelectList RawItemList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        public SelectList BrandList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        public SelectList ModelList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        public SelectList AssetList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        public SelectList DepartmentList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        public SelectList EmployeeList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        public SelectList UnitList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
    }
    public class VmStoreInMaster : BaseVM
    {
        public string StyleName { get; set; } 
        public string CID { get; set; }

        [DisplayName("Supplier Challan")]
        public string ChallanNo { get; set; }
        [DataType(DataType.Date)]
        [DisplayName("Challan Date")]
        [Required(ErrorMessage = "Challan Date is Required")]
        public DateTime ChallanDate { get; set; } = DateTime.Now;
        [DataType(DataType.Date)]
        [DisplayName("Receive Date")]
        [Required(ErrorMessage = "Receive Date is Required")]
        public DateTime ReceivedDate { get; set; } = DateTime.Now;

        public string Description { get; set; }
        public string StockType { get; set; }
        public string StoreInType { get; set; }
        public string Remarks { get; set; }

        [DisplayName("Supplier Name")]
        public string SupplierName { get; set; }
        [DisplayName("Purchase Order")]
        public string PurchaseOrder { get; set; }
        public int CommonStoreFK { get; set; }

        [DisplayName("Purchase Order Number")]
        public int PurchaseOrderFK { get; set; }
        public Procurement_PurchaseOrder Procurement_PurchaseOrder { get; set; }

        public int Common_SupplierFK { get; set; }

        [DisplayName("Supplier Name")]
        public int CommonSupplierFK { get; set; }
        public Common_Supplier ACommon_Supplier { get; set; }


        [DisplayName("Store Name")]
        public int Store_GeneralFK { get; set; }
        public string StoreName { get; set; }
        public Store_General Store_General { get; set; }
        [DisplayName("ChallanNo Internal")]
        public string ChallanNoFK { get; set; }

        public SelectList SupplierList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        public SelectList SupplierPoList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        public SelectList UnitList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        public SelectList PoList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        public SelectList ICList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        public SelectList StoreList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        public SelectList StoreInTypeList { get; set; } = new SelectList(new List<object>(), "Value", "Text");

        public List<VmStoreInMaster> DataListsMasterNew { get; set; }
        public List<VmStoreInMaster> DataListsMasterInternal { get; set; }

    }
    public class VmStoreIn : BaseVM
    {
        [DisplayName("Com. Challan Number")]
        public string CID { get; set; }
        [DisplayName("Supllier Challan Number")]
        [Required(ErrorMessage = "Challan Number is Required")]
        public string ChallanNo { get; set; }

        [DataType(DataType.Date)]
        [DisplayName("Challan Date")]
        [Required(ErrorMessage = "Challan Date is Required")]
        //[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime ChallanDate { get; set; } = DateTime.Now;
        [DataType(DataType.Date)]
        [DisplayName("Receive Date")]
        public DateTime ReceivedDate { get; set; } = DateTime.Now;
        [DataType(DataType.Date)]
        [DisplayName("Warrenty Date")]
        public DateTime WarrentyDate { get; set; }

        [DisplayName("Item Name")]
        public string Name { get; set; }

        public string Description { get; set; }
        [DisplayName("Stock Type")]
        public string StockType { get; set; }
        public string Remarks { get; set; }

        [Range(0, double.MaxValue)]
        [DisplayName("PO Qty")]
        public double PurchaseQuantity { get; set; }

        [Range(0, double.MaxValue)]
        [DisplayName("Remaining Qty")]
        public double RemainingQty { get; set; }
        [Range(0, double.MaxValue)]
        [DisplayName("Damage Qty")]
        public double DamagedQty { get; set; }
        [Range(0, double.MaxValue)]
        [DisplayName("Receive Qty")]
        public double ReceivedQty { get; set; }
        public double vmReceivedQty { get; set; }
        [DisplayName("Unit")]
        public int CommonUnitFK { get; set; }
        public string UnitName { get; set; }
        public int ItemIDFK { get; set; }
        [DisplayName("Purchase Order Number")]
        public int IPOFK { get; set; }
        public int ChallanNoFK { get; set; }

        [DisplayName("Supplier Name")]
        public int CommonSupplierFK { get; set; }
        public int CommonStoreFK { get; set; }
        public string CommonStoreName { get; set; }

        public int StoreBinFK { get; set; }
        public int CommonCategoryFK { get; set; }
        public int CommonSubCategoryFK { get; set; }
        [DisplayName("Asset")]
        public bool FixAsset { get; set; }
        [DisplayName("Bin Define")]
        public bool BinDefine { get; set; }

        [DisplayName("Reference Type")]
        public string StoreInType { get; set; }
        public List<VmStoreBin> BinDataLists { get; set; }
        public List<VmStoreIn> DataLists { get; set; }
        public List<VmStoreInReceived> DataListsReceived { get; set; }
        public VmStoreInMaster VmStoreInMaster { get; set; }
        public List<VmStoreInMaster> DataListsMaster { get; set; }
        public IEnumerable<VmStoreIn> DataList { get; set; }
        public IEnumerable<Common_RawItem> RawItemDataList { get; set; }

        public int StoreInMasterFK { get; set; }
        public int Common_RawItemFK { get; set; }
        public int? Common_ColorFK { get; set; }
        public decimal Common_Size { get; set; }
        public int Merchandising_StyleFK { get; set; }
        public int Merchandising_StyleSlaveFK { get; set; }
        public int Procurement_PurchaseOrderSlaveFK { get; set; }
        public int Store_RequisitionSlaveFK { get; set; }

        public bool IsLocked { get; set; }
        public double UnitPrice { get; set; }
        public double TotalPrice { get; set; }
        //public SelectList SupplierList { get; set; } 
        public SelectList SupplierList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        public SelectList SupplierPoList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        public SelectList UnitList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        public SelectList PoList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        public SelectList ICList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        public SelectList StoreList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        public SelectList StoreInTypeList { get; set; } = new SelectList(new List<object>(), "Value", "Text");

    }
    public class VmStoreOut : BaseVM
    {
        public string CID { get; set; }
        [DisplayName("Challan Number")]
        [Required(ErrorMessage = "Challan Number is Required")]
        public string ChallanNo { get; set; }
        [DataType(DataType.Date)]
        [DisplayName("Challan Date")]
        [Required(ErrorMessage = "Challan Date is Required")]

        public DateTime ChallanDate { get; set; } = DateTime.Now;
        [DataType(DataType.Date)]
        [DisplayName("Store Req. Date")]
        public DateTime ReceivedDate { get; set; } = DateTime.Now;
        [DataType(DataType.Date)]
        [DisplayName("Warrenty Date")]
        public DateTime WarrentyDate { get; set; }

        public string StyleName { get; set; }
        public string Name { get; set; }
        public string Common_Color { get; set; }
        public decimal Common_Size { get; set; }

        public int Merchandising_StyleFK { get; set; }
        public int Merchandising_StyleSlaveFK { get; set; }
        public int Procurement_PurchaseOrderSlaveFK { get; set; }
        public int Store_RequisitionSlaveFK { get; set; }
        public bool IsLocked { get; set; }
        public double UnitPrice { get; set; }
        public double TotalPrice { get; set; }
        public string Description { get; set; }
        [DisplayName("Stock Type")]
        public string StockType { get; set; }
        public string Remarks { get; set; }

        [Range(0, double.MaxValue)]
        [DisplayName("SR Qty")]
        public double RequisitionQuantity { get; set; }

        [Range(0, double.MaxValue)]
        [DisplayName("Store Available Qty")]
        public double StoreAvailableQty { get; set; }

        [Range(0, double.MaxValue)]
        [DisplayName("Remaining Qty")]
        public double RemainingQty { get; set; }

        [Range(0, double.MaxValue)]
        [DisplayName("Damage Qty")]
        public double DamagedQty { get; set; }
        [DisplayName("Store-Out Qty")]
        [Range(0, double.MaxValue)]
        public double ReceivedQty { get; set; }

        [DisplayName("Unit")]
        public int CommonUnitFK { get; set; }
        public string UnitName { get; set; }
        public int ItemIDFK { get; set; }
        [DisplayName("Purchase Order Number")]
        public int IPOFK { get; set; }
        public string IpoNumber { get; set; }

        [DisplayName("Supplier Name")]
        public int CommonSupplierFK { get; set; }
        public string CommonSupplierName { get; set; }
        public int CommonStoreFK { get; set; }
        
        public int StoreChallanFK { get; set; }

        public int StoreBinFK { get; set; }
        public int CommonCategoryFK { get; set; }
        public int CommonSubCategoryFK { get; set; }

        [DisplayName("Asset")]
        public bool FixAsset { get; set; }
        [DisplayName("Bin Define")]
        public bool BinDefine { get; set; }

        [DisplayName("Reference Type")]
        public string StoreInType { get; set; }
        public List<VmStoreBin> BinDataLists { get; set; }
        public List<VmStoreOut> DataLists { get; set; }
        public IEnumerable<VmStoreOut> DataList { get; set; }
        public IEnumerable<Common_RawItem> RawItemDataList { get; set; }

        public string InternalChallanNumber { get; set; }
        public string CommonStoreName { get; set; }
        public string CommonStoreNameTo { get; set; }
        public string ChallanNumberIn { get; set; }

        public int Common_RawItemFK { get; set; }
        public int Store_StockOutMasterFK { get; set; }
        public int Common_SupplierFK { get; set; }        
        public int? Common_ColorFK { get; set; }
        public int? Common_SizeFK { get; set; }
        public bool IsForSave { get; set; }
        public int Store_GeneralOutFK { get; set; }
        public int Store_GeneralInFK { get; set; }

        public SelectList SupplierList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        public SelectList SupplierPoList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        public SelectList UnitList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        public SelectList PoList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        public SelectList SupplierChallanList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        public SelectList StoreList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        public SelectList IsrList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        public SelectList StoreOutTypeList { get; set; } = new SelectList(new List<object>(), "Value", "Text");

    }
    public class VmStoreInReceived : BaseVM
    {
        [DisplayName("Com. Challan Number")]
        public string CID { get; set; }
        [DisplayName("Supllier Challan Number")]
        [Required(ErrorMessage = "Challan Number is Required")]
        public string ChallanNo { get; set; }

        [DataType(DataType.Date)]
        [DisplayName("Challan Date")]
        [Required(ErrorMessage = "Challan Date is Required")]
        //[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime ChallanDate { get; set; } = DateTime.Now;
        [DataType(DataType.Date)]
        [DisplayName("Receive Date")]
        public DateTime ReceivedDate { get; set; } = DateTime.Now;
        [DataType(DataType.Date)]
        [DisplayName("Warrenty Date")]
        public DateTime WarrentyDate { get; set; }

        [DisplayName("Item Name")]
        public string Name { get; set; }

        public string Description { get; set; }
        [DisplayName("Stock Type")]
        public string StockType { get; set; }
        public string Remarks { get; set; }

        [Range(0, double.MaxValue)]
        [DisplayName("PO Qty")]
        public double PurchaseQuantity { get; set; }

        [Range(0, double.MaxValue)]
        [DisplayName("Remaining Qty")]
        public double RemainingQty { get; set; }
        [Range(0, double.MaxValue)]
        [DisplayName("Damage Qty")]
        public double DamagedQty { get; set; }
        [Range(0, double.MaxValue)]
        [DisplayName("Receive Qty")]
        public double ReceivedQty { get; set; }
        public double vmReceivedQty { get; set; }

        public int CommonUnitFK { get; set; }
        public string UnitName { get; set; }
        public int ItemIDFK { get; set; }
        [DisplayName("Purchase Order Number")]
        public int IPOFK { get; set; }
        public int ChallanNoFK { get; set; }

        [DisplayName("Supplier Name")]
        public int CommonSupplierFK { get; set; }
        public int CommonStoreFK { get; set; }
        public string CommonStoreName { get; set; }

        public int StoreBinFK { get; set; }
        public int CommonCategoryFK { get; set; }
        public int CommonSubCategoryFK { get; set; }
        [DisplayName("Asset")]
        public bool FixAsset { get; set; }
        [DisplayName("Bin Define")]
        public bool BinDefine { get; set; }

        [DisplayName("Reference Type")]
        public string StoreInType { get; set; }
        public List<VmStoreBin> BinDataLists { get; set; }
        public List<VmStoreIn> DataLists { get; set; }
        public List<VmStoreInReceived> DataListsReceived { get; set; }
        public VmStoreInMaster VmStoreInMaster { get; set; }
        public List<VmStoreInMaster> DataListsMaster { get; set; }
        public IEnumerable<VmStoreIn> DataList { get; set; }
        public IEnumerable<Common_RawItem> RawItemDataList { get; set; }

        public int StoreInMasterFK { get; set; }
        public int Common_RawItemFK { get; set; }
        public int? Common_ColorFK { get; set; }
        public decimal Common_Size { get; set; }
        public int Merchandising_StyleFK { get; set; }
        public int Merchandising_StyleSlaveFK { get; set; }
        public int Procurement_PurchaseOrderSlaveFK { get; set; }
        public int Store_RequisitionSlaveFK { get; set; }

        public bool IsLocked { get; set; }
        public double UnitPrice { get; set; }
        public double TotalPrice { get; set; }
        //public SelectList SupplierList { get; set; } 
        public SelectList SupplierList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        public SelectList SupplierPoList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        public SelectList UnitList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        public SelectList PoList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        public SelectList ICList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        public SelectList StoreList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        public SelectList StoreInTypeList { get; set; } = new SelectList(new List<object>(), "Value", "Text");

    }
    public class VmAssetDepreciation : BaseVM
    {
        [DisplayName("Product Sl No")]
        public string CID { get; set; }
        [DisplayName("Asset Tag No")]
        public string ItemSLNO { get; set; }
        [DisplayName("Item Name")]
        public string ItemName { get; set; }

        [DisplayName("Lot Qty")]
        public double LotQty { get; set; }
        [DisplayName("Unit Price")]
        public double UnitPrice { get; set; }
        [DisplayName("Depreciation Starting Value")]
        public double MinPrice { get; set; }
        [DisplayName("Life Time Year's")]
        public int LifeTime { get; set; }
        public string Remarks { get; set; }

        [DisplayName("Exiting Item")]
        public bool ExitingItem { get; set; }
        [DisplayName("Warranty Expiry Date")]
        [DataType(DataType.Date)]
        public DateTime WarrantyExpiryDate { get; set; } = DateTime.Now;
        public int Status { get; set; }
        [DisplayName("Store Name")]
        [Required(ErrorMessage = "Store Name is Required")]
        public int StoreGeneralFK { get; set; }
        [DisplayName("Department Name")]
        public int CommonDepartmentFK { get; set; }
        [DisplayName("Emplyee Name")]
        public int EmployeeFK { get; set; }
        public DateTime ReceivedDate { get; set; }

        public int CommonStoreFK { get; set; }
        public int StockInFk { get; set; }
        [DisplayName("Unit Name")]
        public int CommonUnitFK { get; set; }
        [DisplayName("Brand Name")]
        [Required(ErrorMessage = "Brand is Required")]
        public int CommonBrandFK { get; set; }
        [Required(ErrorMessage = "Model is Required")]
        [DisplayName("Model Name")]
        public int CommonModelFK { get; set; }
        [DisplayName("Item Name")]
        [Required(ErrorMessage = "Item Name is Required")]
        public int RawItemFK { get; set; }

        public int CommonDepreciationFK { get; set; }

        public string DepartmentName { get; set; }
        public string EmployeeName { get; set; }
        [DisplayName("Unit Name")]
        public string CommonUnitName { get; set; }
        [DisplayName("Brand Name")]
        public string CommonBrandName { get; set; }
        [DisplayName("Model Name")]
        public string CommonModelName { get; set; }
        public List<VmAssetDepreciation> DataLists { get; set; }
        public IEnumerable<VmAssetDepreciation> DataListasync { get; set; }
        public SelectList StoreList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        public SelectList RawItemList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        public SelectList BrandList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        public SelectList ModelList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
    }
    public class VmAssetDepreciationDetails : BaseVM
    {
        [DisplayName("Depreciation")]
        public double Depreciation { get; set; }
        [DisplayName("Depreciation Value")]
        public double DepreciationValue { get; set; }
        public List<VmAssetDepreciationDetails> DataLists { get; set; }
    }

    //// Report VM /////
    ///
    public class VmStoreReport : BaseReport
    {
        [Display(Name = "Approved Style List")]
        public int Merchandising_StyleFK { get; set; }
        public int Sr { set; get; }
        [Display(Name = "Print Date")]
        public string PrintDate { get; set; } = DateTime.Now.ToShortDateString();
        [Display(Name = "SupplierName")]
        public string SupplierName { set; get; }
        [Display(Name = "Buyer Name")]
        public string BuyerName { set; get; }
        [Display(Name = "Style Name")]
        public string StyleName { set; get; }
        [Display(Name = "Item Name")]
        public string ItemName { set; get; }
        [Display(Name = "Item Code")]
        public string ItemCode { set; get; }

        [Display(Name = "PO Item Qty")]
        public double OpenningQty { set; get; }
        [Display(Name = "Store Received Qty")]
        public double StoreInQty { set; get; }
        [Display(Name = "Store Inssued Qty")]
        public double StoreOutQty { set; get; }

        public double StoreReceivedQty { set; get; }
        [Display(Name = "Prod. Store Qty")]
        public double BalanceQty { set; get; }
        public string StockType { set; get; }
        [Display(Name = "Prod. Consume Qty")]
        public double ConsumeQty { set; get; }
        [Display(Name = "Total Qty")]
        public double TotalQty { set; get; }
        public string Remarks { set; get; }
        public string Unit { set; get; }
        public List<VmStoreReport> Datalist { get; set; }
        public List<VmStoreReport> Datalistwip { get; set; }
        public List<VmStoreReport> Datalistraw { get; set; }
        public List<VmStoreReport> Datalistrawwip { get; set; }

        public ProcurementOriginTypeEnum OriginType { get; set; }
        public string OriginTypeName
        {
            get
            {
                return BaseFunctionalities.GetEnumDescription(this.OriginType);
            }
        }
        public SelectList OriginTypeList { get { return new SelectList(BaseFunctionalities.GetEnumList<ProcurementOriginTypeEnum>(), "Value", "Text"); } }

        public SelectList ApprovedStyleList { get; set; } = new SelectList(new List<object>());

        public SelectList RawItemList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        public SelectList RawCategoryList { get; set; } = new SelectList(new List<object>());
        public SelectList RawSubCategoryList { get; set; } = new SelectList(new List<object>());

        [DisplayName("Raw Category")]
        public int Common_RawCategoryFK { get; set; }
        [DisplayName("Raw Sub Category")]
        public int Common_RawSubCategoryFK { get; set; }
        [DisplayName("Raw Item Name")]
        public int Common_RawItemFK { get; set; }
    }

    public class VmStoreReportDetails : BaseReport 
    {        
        [Display(Name = "Approved Style List")]
        public int Merchandising_StyleFK { get; set; }
        public int Sr { set; get; }
        [Display(Name = "Print Date")]
        public string PrintDate { get; set; } = DateTime.Now.ToShortDateString();
        [Display(Name = "SupplierName")]
        public string SupplierName { set; get; }
        [Display(Name = "Buyer Name")]
        public string BuyerName { set; get; }
        [Display(Name = "Style Name")]
        public string StyleName { set; get; }
        [Display(Name = "Item Name")]
        public string ItemName { set; get; }
        [Display(Name = "Item Code")]
        public string ItemCode { set; get; }

        [Display(Name = "PO Item Qty")]
        public double OpenningQty { set; get; }

        [Display(Name = "Qty")]
        public double StoreInQty { set; get; }
        [Display(Name = "Value")]
        public double StoreInValue { set; get; }

        [Display(Name = "Qty")]
        public double StoreOutQty { set; get; }
        [Display(Name = "Value")]
        public double StoreOutValue { set; get; } 

        public double StoreReceivedQty { set; get; }

        [Display(Name = "Qty")]
        public double BalanceQty { set; get; }
        [Display(Name = "Value")]
        public double BalanceValue { set; get; }

        [Display(Name = "Qty")]
        public double ProductionQty { set; get; }
        [Display(Name = "Value")]
        public double ProductionValue { set; get; }

        public string StockType { set; get; }
        [Display(Name = "Prod.Qty")]
        public double ConsumeQty { set; get; }
        [Display(Name = "Total Qty")]
        public double TotalQty { set; get; }
        public string Remarks { set; get; }
        public string Unit { set; get; }
        public List<VmStoreReportDetails> Datalist { get; set; }
        public List<VmStoreReportDetails> Datalistwip { get; set; }
        public List<VmStoreReportDetails> Datalistraw { get; set; }
        public List<VmStoreReportDetails> Datalistrawwip { get; set; }

        public ProcurementOriginTypeEnum OriginType { get; set; }
        public string OriginTypeName
        {
            get
            {
                return BaseFunctionalities.GetEnumDescription(this.OriginType);
            }
        }
        public SelectList OriginTypeList { get { return new SelectList(BaseFunctionalities.GetEnumList<ProcurementOriginTypeEnum>(), "Value", "Text"); } }

        public SelectList ApprovedStyleList { get; set; } = new SelectList(new List<object>());

        public SelectList RawItemList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        public SelectList RawCategoryList { get; set; } = new SelectList(new List<object>());
        public SelectList RawSubCategoryList { get; set; } = new SelectList(new List<object>());

        [DisplayName("Raw Category")]
        public int Common_RawCategoryFK { get; set; }
        [DisplayName("Raw Sub Category")]
        public int Common_RawSubCategoryFK { get; set; }
        [DisplayName("Raw Item Name")]
        public int Common_RawItemFK { get; set; }
    }

    public class VmStoreItemWiseConsumption : BaseReport
    {
        [Display(Name = "Style Name")]
        public string StyleName { set; get; }
        public int Merchandising_StyleFK { get; set; }
        public int Common_RawItemFK { set; get; }
        [Display(Name = "Item Name")]
        public string Name { set; get; }
        [Display(Name = "Unit")]
        public string UnitName { set; get; }
        [Display(Name = "Cons.Dz")]
        public double ConsumptionDz { set; get; }
        [Display(Name = "Order Cons.Dz")]
        public double OrderConsumQty { set; get; }
        [Display(Name = "Finish Qty Dz")]
        public double FinishQtyDz { set; get; }

        [Display(Name = "Received Qty")]
        public double TotalRecMain { set; get; }

        [Display(Name = "Received Qty")]
        public double TotalRecCutting { set; get; }
        [Display(Name = "Received Qty")]
        public double TotalRecSewing { set; get; }
        [Display(Name = "Received Qty")]
        public double TotalRecIroning { set; get; }
        [Display(Name = "Received Qty")]
        public double TotalRecPacking { set; get; }
        [Display(Name = "Received. Qty")]
        public double TotalRecFinish { set; get; }

        [Display(Name = "Store Out Qty")]
        public double TotalOutMain { set; get; }
        [Display(Name = "Consume Qty")]
        public double TotalOutCutting { set; get; }
        [Display(Name = "Consume Qty")]
        public double TotalOutSewing { set; get; }
        [Display(Name = "Consume Qty")]
        public double TotalOutIroning { set; get; }
        [Display(Name = "Consume Qty")]
        public double TotalOutPacking { set; get; }
        [Display(Name = "Store Out Qty")]
        public double TotalOutFinish { set; get; }

        [Display(Name = "Remaining Qty")]
        public double TotalRemMain { set; get; } 
        [Display(Name = "Remaining Qty")]
        public double TotalRemCutting { set; get; }
        [Display(Name = "Remaining Qty")]
        public double TotalRemSewing { set; get; }
        [Display(Name = "Remaining Qty")]
        public double TotalRemIroning { set; get; }
        [Display(Name = "Remaining Qty")]
        public double TotalRemPacking { set; get; }
        [Display(Name = "Remaining Qty")]
        public double TotalRemFinish { set; get; }
        public List<VmStoreItemWiseConsumption> Datalist { get; set; }
        public List<VmStoreItemWiseConsumption> DatalistFinish { get; set; }
        public List<VmStoreItemWiseConsumption> DatalistFinishConsumtion { get; set; }
        
    }
    public class VmStoreItemWiseConsumptionStyleAll : BaseReport
    {
        [Display(Name = "Style Name")]
        public string StyleName { set; get; }
        public int Merchandising_StyleFK { get; set; }
        public int Common_RawItemFK { set; get; }
        public int Common_ColorFK { set; get; }
        public int Common_Size { set; get; }       
        [Display(Name = "Item Name")]
        public string Name { set; get; }
        [Display(Name = "Unit")]
        public string UnitName { set; get; }
        [Display(Name = "Cons.Dz")]
        public double ConsumptionDz { set; get; }
        [Display(Name = "Order Cons.Dz")]
        public double OrderConsumQty { set; get; }
        [Display(Name = "Finish Qty Dz")]
        public double FinishQtyDz { set; get; }

        [Display(Name = "Received Qty")]
        public double TotalRecMain { set; get; }

        [Display(Name = "Received Qty")]
        public double TotalRecCutting { set; get; }
        [Display(Name = "Received Qty")]
        public double TotalRecSewing { set; get; }
        [Display(Name = "Received Qty")]
        public double TotalRecIroning { set; get; }
        [Display(Name = "Received Qty")]
        public double TotalRecPacking { set; get; }
        [Display(Name = "Received. Qty")]
        public double TotalRecFinish { set; get; }

        [Display(Name = "Store Out Qty")]
        public double TotalOutMain { set; get; }
        [Display(Name = "Consume Qty")]
        public double TotalOutCutting { set; get; }
        [Display(Name = "Consume Qty")]
        public double TotalOutSewing { set; get; }
        [Display(Name = "Consume Qty")]
        public double TotalOutIroning { set; get; }
        [Display(Name = "Consume Qty")]
        public double TotalOutPacking { set; get; }
        [Display(Name = "Store Out Qty")]
        public double TotalOutFinish { set; get; }

        [Display(Name = "Remaining Qty")]
        public double TotalRemMain { set; get; }
        [Display(Name = "Remaining Qty")]
        public double TotalRemCutting { set; get; }
        [Display(Name = "Remaining Qty")]
        public double TotalRemSewing { set; get; }
        [Display(Name = "Remaining Qty")]
        public double TotalRemIroning { set; get; }
        [Display(Name = "Remaining Qty")]
        public double TotalRemPacking { set; get; }
        [Display(Name = "Remaining Qty")]
        public double TotalRemFinish { set; get; }
        public List<VmStoreItemWiseConsumptionStyleAll> Datalist { get; set; }
        public List<VmStoreItemWiseConsumptionStyleAll> DatalistFinish { get; set; }
        public List<VmStoreItemWiseConsumptionStyleAll> DatalistFinishConsumtion { get; set; }
    }
}
