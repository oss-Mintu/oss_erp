﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Erp.Core.Services.Store
{
    public class ReportRawItem
    {
        public int Sr { set; get; }
        [Display(Name = "Print Date")]
        public string PrintDate { get; set; } = DateTime.Now.ToShortDateString();
        [Display(Name = "SupplierName")]
        public string SupplierName { set; get; }
        [Display(Name = "Buyer Name")]
        public string BuyerName { set; get; }
        [Display(Name = "Style Name")]
        public string StyleName { set; get; }
        [Display(Name = "Item Name")]
        public string ItemName { set; get; }
        [Display(Name = "Item Code")]
        public string ItemCode { set; get; }
        [Display(Name = "Purchase Quantity")]
        public double PurchaseQuantity { set; get; }
        [Display(Name = "Purchasing Price")]
        public double PurchasingPrice { set; get; }
        [Display(Name = "Purchasing Value")]
        public double PurchasingValue { set; get; }
        [Display(Name = "Store Received Qty")]
        public double StoreReceivedQty { set; get; }
        [Display(Name = "StoreOut Qty")]
        public double StoreOutQty { set; get; }
        [Display(Name = "StoreIn Name")]
        public string StoreInName { set; get; }
        [Display(Name = "StoreOut Name")]
        public string StoreOutName { set; get; }
        
        public string Remarks { set; get; }

        public List<ReportRawItem> DataList { set; get; }
        public IEnumerable<ReportRawItem> DataLists { set; get; }
    }
}
