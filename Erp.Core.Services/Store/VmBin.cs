﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Erp.Core.Services.Store
{
    public class VmBin: BaseVM
    {

        public string CID { get; set; }
        public string Section { get; set; }
        public string Row { get; set; }
        public string Shelf { get; set; }
        public string Rack { get; set; }
        public double Capacity { get; set; }
        public string Remarks { get; set; }
        public int TotalRow { get; set; }
        public int TotalShelf { get; set; }
        public int Dimension { get; set; }
        public int CommonUnitFK { get; set; }
        public int StoreGeneralFK { get; set; }

        //public string CID { get; set; }
        //public string Section { get; set; }
        //[DisplayName("Row/Column")]
        //public string Row { get; set; }
        //public string Shelf { get; set; }
        //public string Rack { get; set; }
        //public string Store { get; set; }

        //[DisplayName("Store")]
        //[Required(ErrorMessage = "Store is Required")]
        //public int General_StoreFK { get; set; }
        //public IEnumerable<VmBin> DataList { get; set; }

    }
}
