﻿using Erp.Core.Entity.Common;
using Erp.Core.Services.Home;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

//using System.Text;

namespace Erp.Core.Services.Store
{
    public class VMStoreRequisition : BaseVM
    {
        public string CID { get; set; }
        public int SRActionId { get; set; }
        public SRActionEnum SRActionEnum { get { return (SRActionEnum)this.SRActionId; } }

        public string Description { get; set; }
        public SRStatusEnum Status { get; set; }
        public string AuthorizationStatus { get { return BaseFunctionalities.GetEnumDescription(Status); } }
        public DateTime CreationDate { get; set; } = DateTime.Now;
        public string CancellationRemaks { get; set; }
        public int VMClosedByUserFK { get; set; }
        public string VMClosedByUserName { get; set; }

        public string CreatedBy { get; set; } = "UserOne";
        public string VMDepartmentName { get; set; } = "Accounts";

        public string ItemNames { get; set; }
        public string FinishItemNames { get; set; }


        public List<VMStoreRequisition> DataList { get; set; }

        public string DraftStatusCount { get; set; }
        public string SubmittedStatusCount { get; set; }
        public string PrimaryStatusCount { get; set; }
        public string FinalStatusCount { get; set; }
        public string ProcurementStatusCount { get; set; }
        public string PartialPOStatusCount { get; set; }
        public string FullPOStatusCount { get; set; }
        public string InspectionStatusCount { get; set; }
        public string GoodsReceivedStatusCount { get; set; }
        public string ClosedStatusCount { get; set; }
        public string HoldStatusCount { get; set; }
        public int StyleId { get; set; }

        [Display(Name = "Approved Purchase Order List")]
        public int Procurement_PurchaseOrderFK { get; set; }

        [Display(Name = "In Store")]
        public int VMStoreToFK { get; set; }
        public string VMStoreToName { get; set; }
        public SelectList StoreToList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        [Display(Name = "Out Store")]
        public int VMStoreFromFK { get; set; }
        public string VMStoreFromName { get; set; }
        public SelectList StoreFromList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        public int CommonStoreFK { get; set; } 
    }

    public class VMStoreRequisitionSlave : VMStoreRequisition
    {
        public int Common_FinishItemFK { get; set; }
        public int Procurement_PurchaseRequisitionFK;
        public int VMMerchandising_StyleSlaveID;
        public string Common_Color;

        public int Store_RequisitionFK { get; set; }
        public string VMRawItemName { get; set; }
        [Required(ErrorMessage = "Raw Item is Required")]
        public int VMRawItemFK { get; set; }

        [Display(Name = "Raw Item Category")]
        public int VMRawCategoryFK { get; set; }
        public string VMRawCategoryName { get; set; }
        public int VMRawSubCategoryFK { get; set; }
        public string VMRawSubCategoryName { get; set; }

        public string DescriptionSlave { get; set; }
        public double BOMQuantity { get; set; }
        public double RequisitionQuantity { get; set; }

        public double AvailableQuantity { get; set; }

        public string VMUnitName { get; set; }
        [Required(ErrorMessage = "Required Date is Required")]
        [Display(Name = "Required Date")]
        public DateTime RequiredDate { get; set; } = DateTime.Now;
        public string RequiredDateString { get { return this.RequiredDate != DateTime.MinValue ? this.RequiredDate.ToString("yyyy-MM-dd") : ""; } }

        [Display(Name = "Sub Set")]
        public int? Merchandising_StyleSetPackFK { get; set; }
        public bool InspectionRequired { get; set; }
        public string Inspection { get { return this.InspectionRequired ? "Required" : "Not Required"; } }
        [Display(Name = "Request By")]
        public int? VMEmployeeFK { get; set; }
        public string VMEmployeeName { get; set; }

        public List<VMStoreRequisitionSlave> DataListSlave { get; set; }
        public SelectList RawItemList { get; set; } = new SelectList(new List<object>());
        public SelectList RawCategoryList { get; set; } = new SelectList(new List<object>());
        public SelectList RawSubCategoryList { get; set; } = new SelectList(new List<object>());
        public SelectList EmployeeList { get; set; } = new SelectList(new List<object>());
        public SelectList StyleList { get; set; } = new SelectList(new List<object>());
        [Display(Name = "Approved Style List")]
        public SelectList ApprovedStyleList { get; set; } = new SelectList(new List<object>());

        [Display(Name = "Approved PO List")]
        public SelectList ApprovedPOList { get; set; } = new SelectList(new List<object>());
        public SelectList Merchandising_StyleSetPackList { get; set; } = new SelectList(new List<object>());

        public int Common_RawItemFK { get; set; }
        public int? Common_ColorFK { get; set; }
        public int? Common_SizeFK { get; set; } 
        public decimal Common_Size { get; set; }
        public int Merchandising_StyleFK { get; set; }
        public int Merchandising_StyleSlaveFK { get; set; }
        public int Procurement_PurchaseOrderSlaveFK { get; set; }
        public bool IsLocked { get; set; }

        [Display(Name = "In Store")]
        public int StoreToFK { get; set; }
        [Display(Name = "Out Store")]
        public int StoreFromFK { get; set; }
        //public int CommonStoreFK { get; set; }
        [Display(Name = "Raw Item Type")]
        public int Common_RawItemTypeFK { get; set; }
        public bool IsForSave { get; set; }
        public int Store_GeneralOutFK { get; set; }
        public int Store_GeneralInFK { get; set; }        
    }

    public class VMStoreSRequisition : BaseVM
    {
        public string CID { get; set; }
        public int PRActionId { get; set; }
        public PRActionEnum PRActionEnum { get { return (PRActionEnum)this.PRActionId; } }
        public PRTypeEnum PRType { get; set; }
        public string PRTypeName { get { return BaseFunctionalities.GetEnumDescription(this.PRType); } }
        public SelectList PRTypeList { get { return new SelectList(BaseFunctionalities.GetEnumList<PRTypeEnum>(), "Value", "Text"); } }
        public ProcurementOriginTypeEnum OriginType { get; set; }
        public string OriginTypeName { get { return BaseFunctionalities.GetEnumDescription(this.OriginType); } }
        public SelectList OriginTypeList { get { return new SelectList(BaseFunctionalities.GetEnumList<ProcurementOriginTypeEnum>(), "Value", "Text"); } }

        public string Description { get; set; }
        public SRStatusEnum Status { get; set; }
        public string AuthorizationStatus { get { return BaseFunctionalities.GetEnumDescription(Status); } }
        public DateTime CreationDate { get; set; } = DateTime.Now;
        public string CancellationRemaks { get; set; }
        public int VMClosedByUserFK { get; set; }
        public string VMClosedByUserName { get; set; }

        public string CreatedBy { get; set; } = "UserOne";
        public string VMDepartmentName { get; set; } = "Accounts";

        public string ItemNames { get; set; }

        public List<VMStoreSRequisition> DataList { get; set; }

        public string DraftStatusCount { get; set; }
        public string SubmittedStatusCount { get; set; }
        public string PrimaryStatusCount { get; set; }
        public string FinalStatusCount { get; set; }
        public string ProcurementStatusCount { get; set; }
        public string PartialPOStatusCount { get; set; }
        public string FullPOStatusCount { get; set; }
        public string InspectionStatusCount { get; set; }
        public string GoodsReceivedStatusCount { get; set; }
        public string ClosedStatusCount { get; set; }
        public string HoldStatusCount { get; set; }
        public bool HasSlave { get; set; }


        [Display(Name = "In Store")]
        public int VMStoreToFK { get; set; }
        public string VMStoreToName { get; set; }
        public SelectList StoreToList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        [Display(Name = "Out Store")]
        public int VMStoreFromFK { get; set; }
        public string VMStoreFromName { get; set; }
        public SelectList StoreFromList { get; set; } = new SelectList(new List<object>(), "Value", "Text");

    }

    public class VMStoreSRequisitionSlave : VMStoreSRequisition
    {
        private double _RequisitionQuantity = 0;
        public double PRRaisedQuantity { get; set; }
        public double StyleQuantity { get; set; }
        public bool IsForSave { get; set; }
        public int Procurement_PurchaseRequisitionFK { get; set; }
        public string VMRawItemName { get; set; }
        [Required(ErrorMessage = "Raw Item is Required")]
        public int VMRawItemFK { get; set; }
        public int VMRawCategoryFK { get; set; }
        public string VMRawCategoryName { get; set; }
        public int VMRawSubCategoryFK { get; set; }
        public string VMRawSubCategoryName { get; set; }

        public string DescriptionSlave { get; set; }

        public double SuggestedPrice { get; set; }
        public double RestQuantity
        {
            get
            {
                return StyleQuantity - (PRRaisedQuantity);
            }
        }
        public double RequisitionQuantity
        {
            get
            {
                if (_RequisitionQuantity <= 0)
                {
                    if (StyleQuantity > (PRRaisedQuantity))
                    { _RequisitionQuantity = StyleQuantity - (PRRaisedQuantity); }
                }
                return _RequisitionQuantity;
            }
            set { _RequisitionQuantity = value; }
        }
        [Range(typeof(bool), "true", "true", ErrorMessage = "Requisition Quantity must be smaller than or equal to Rest Quantity")]
        public bool IsValidQuantity { get { return VMMerchandising_StyleID > 0 && RestQuantity >= RequisitionQuantity; } }
        public double AvailableQuantity { get; set; }

        public string VMUnitName { get; set; }
        [Required(ErrorMessage = "Required Date is Required")]
        public DateTime RequiredDate { get; set; }
        public string RequiredDateString { get { return this.RequiredDate != DateTime.MinValue ? this.RequiredDate.ToString("yyyy-MM-dd") : ""; } }

        public string SupplierNames { get; set; }

        public bool InspectionRequired { get; set; }
        public string Inspection { get { return this.InspectionRequired ? "Required" : "Not Required"; } }

        public int? VMEmployeeFK { get; set; }
        public string VMEmployeeName { get; set; }
        public int? VMMerchandising_StyleID { get; set; }
        public int? VMMerchandising_StyleSlaveID { get; set; }
        public int? VMMerchandising_BOFID { get; set; }
        public string BONoStyleName { get; set; }


        public List<VMStoreSRequisitionSlave> DataListSlave { get; set; }
        public SelectList RawItemList { get; set; } = new SelectList(new List<object>());
        public SelectList RawCategoryList { get; set; } = new SelectList(new List<object>());
        public SelectList RawSubCategoryList { get; set; } = new SelectList(new List<object>());
        public SelectList EmployeeList { get; set; } = new SelectList(new List<object>());
        [Display(Name = "By Style")]
        public SelectList ApprovedBOMStyleList { get; set; } = new SelectList(new List<object>());
        [Display(Name = "By Style")]
        public SelectList ApprovedBOFStyleList { get; set; } = new SelectList(new List<object>());
    }
}