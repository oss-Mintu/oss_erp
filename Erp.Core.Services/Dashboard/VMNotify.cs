﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Erp.Core.Services.Home
{
    public class VMNotify : BaseVM
    {
        public int StyleID { get; set; }
        public bool IsCloseShipment { get; set; }
        public int NoOfShipment { get; set; }
        public int TShipmentDone { get; set; }
        public bool IsBOMCreate { get; set; }
        public bool IsBOMComplete { get; set; }

        public int TDraftPR { get; set; }
        public int TSubmittedPR { get; set; }
        public int THoldPR { get; set; }
        public int TCancelPR { get; set; }
        public int TPartialPR { get; set; }
        public int TCompletePR { get; set; }

        public int PApprovedPR { get; set; }
        public int SApprovedPR { get; set; }
        public int FApprovedPR { get; set; }


        public int TDraftPO { get; set; }
        public int TSubmittedPO { get; set; }
        public int THoldPO { get; set; }
        public int TCancelPO { get; set; }
        public int TPartialPO { get; set; }
        public int TCompletePO { get; set; }
        
        public int PApprovedPO { get; set; }
        public int SApprovedPO { get; set; }
        public int FApprovedPO { get; set; }
        
        public int PartialReceived { get; set; }
        public int TGoodReceived { get; set; }
        public VMLogin VMLogin { get; set; }
        public List<VMNotification> DataList { get; set; }

        //Checking Property
        public bool IsUNoOfShipment { get; set; }
        public bool IsUTShipmentDone { get; set; }

        public bool IsUTDraftPR { get; set; }
        public bool IsUTSubmittedPR { get; set; }
        public bool IsUTHoldPR { get; set; }
        public bool IsUTCancelPR { get; set; }
        public bool IsUTPartialPR { get; set; }
        public bool IsUTCompletePR { get; set; }

        public bool IsUPApprovedPR { get; set; }
        public bool IsUSApprovedPR { get; set; }
        public bool IsUFApprovedPR { get; set; }

        public bool IsUTDraftPO { get; set; }
        public bool IsUTSubmittedPO { get; set; }
        public bool IsUTHoldPO { get; set; }
        public bool IsUTCancelPO { get; set; }
        public bool IsUTPartialPO { get; set; }
        public bool IsUTCompletePO { get; set; }
               
        public bool IsUPApprovedPO { get; set; }
        public bool IsUSApprovedPO { get; set; }
        public bool IsUFApprovedPO { get; set; }
               
        public bool IsUPartialReceived { get; set; }
        public bool IsUTGoodReceived { get; set; }
    }
}
