﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Erp.Core.Services.Production;
using Erp.Core.Services.Home;
using Microsoft.AspNetCore.Http;

namespace Erp.Core.Services.Dashboard
{
    public class DashboardService: BaseService
    {
        private readonly VMLogin _vmLogin;
        private int _UserID;
        public DashboardService(IErpDbContext db,HttpContext httpContext)
        {
            _db = db;
            _vmLogin = httpContext.Session.GetObjectFromJson<VMLogin>("LoginUser");
        }
        #region Merchandising
        public VmNotification GetMerchadisingDashboard()
        {
            VmNotification vMNotification = new VmNotification();
            if (_vmLogin.UserAccessLevelId == (int)UserAccessLevel.Basic)
            {
                switch (_vmLogin.UserDepartmentId)
                {
                    case (int)EnumDepartment.Merchandising:
                        _UserID = _vmLogin.UserID;
                        break;
                    case (int)EnumDepartment.Procurement:
                        _UserID = _vmLogin.UserID;
                        break;
                    case (int)EnumDepartment.Production:
                        _UserID = _vmLogin.UserID;
                        break;
                    case (int)EnumDepartment.Planning:
                        _UserID = _vmLogin.UserID;
                        break;
                    case (int)EnumDepartment.Store:
                        _UserID = _vmLogin.UserID;
                        break;
                    case (int)EnumDepartment.Commercial:
                        _UserID = _vmLogin.UserID;
                        break;
                    case (int)EnumDepartment.Accounts:
                        _UserID = _vmLogin.UserID;
                        break;
                    case (int)EnumDepartment.HRAdmin:
                        _UserID = _vmLogin.UserID;
                        break;
                }
                
                var vStyle = _db.Merchandising_Style.Where(a => a.Active == true && a.UserID == _UserID).Select(x => new { CBOM = x.IsComplete });
                vMNotification.CompleteBOM = vStyle.Count(a => a.CBOM == true);
                vMNotification.IncompleteBOM = vStyle.Count(a => a.CBOM == false);
            }
            else
            {
                switch (_vmLogin.UserDepartmentId)
                {
                    case (int)EnumDepartment.Merchandising:
                        _UserID = _vmLogin.UserID;
                        break;
                    case (int)EnumDepartment.Procurement:
                        _UserID = _vmLogin.UserID;
                        break;
                    case (int)EnumDepartment.Production:
                        _UserID = _vmLogin.UserID;
                        break;
                    case (int)EnumDepartment.Planning:
                        _UserID = _vmLogin.UserID;
                        break;
                    case (int)EnumDepartment.Store:
                        _UserID = _vmLogin.UserID;
                        break;
                    case (int)EnumDepartment.Commercial:
                        _UserID = _vmLogin.UserID;
                        break;
                    case (int)EnumDepartment.Accounts:
                        _UserID = _vmLogin.UserID;
                        break;
                    case (int)EnumDepartment.HRAdmin:
                        _UserID = _vmLogin.UserID;
                        break;
                }

                var vStyle = _db.Merchandising_Style.Where(a => a.Active == true).Select(x => new { CBOM = x.IsComplete });
                vMNotification.CompleteBOM = vStyle.Count(a => a.CBOM == true);
                vMNotification.IncompleteBOM = vStyle.Count(a => a.CBOM == false);
            }
            
           

            return vMNotification;

        }
        #endregion

        #region Procurment

        public VmNotification GetProcurmentDashboard()
        {
            VmNotification vMNotification = new VmNotification();
            

            return vMNotification;

        }
        #endregion
        public VmNotification GetAllDashboardData()
        {
            var mModel = GetMerchadisingDashboard();
            var pModel = GetProcurmentDashboard();
            VmNotification vmNotification = new VmNotification();
            vmNotification.NStyle = mModel.NStyle;

        
            return vmNotification;
        }

        #region Production
        public int GetDailyTotalProductionSectionWise(int RefId, int SectionId)
        {
            var VData = (from t in _db.Prod_ReferencePlan
                         join pd in _db.Prod_ReferencePlanFollowup on t.ID equals pd.Prod_ReferencePlanFK
                         where t.Prod_ReferenceFK == RefId && t.Active == true && pd.Active == true && t.SectionId == SectionId

                         select new Production.VMProductionReport
                         {

                             Quantity = (int)pd.Quantity,
                         }).ToList().Sum(a => a.Quantity);

            return VData;
        }
        public int GetDailyTotalTargetSectionWise(int RefId, int SectionId)
        {
            var VData = (from t in _db.Prod_ReferencePlan
                         where t.Prod_ReferenceFK == RefId && t.Active == true && t.SectionId == SectionId

                         select new VMProductionReport
                         {
                             TargetQty = t.PlanQuantity
                         }).ToList().Sum(c => c.TargetQty);

            return VData;
        }

        public VMLineChartGraph PeriodicProductionGraph(DateTime fromdate, DateTime todate)
        {
            VMLineChartGraph lg = new VMLineChartGraph();
            lg.datasets = new List<datasets>();
            List<datasets> vMProductionReports = new List<datasets>();
            DateTime fDate = fromdate;
            DateTime tDate = todate;

            DateTime CurrentData = DateTime.Today;
            int i = (int)(todate - fromdate).TotalDays + 1;
            string[] level = new string[i];

            List<int> planData = new List<int>();
            List<int> targetData = new List<int>();
            List<int> achievData = new List<int>();

            int j = 0;
            while (fDate <= tDate)
            {
                if (CurrentData == fDate)
                {
                    level[j] = "Today";
                }
                else
                {
                    level[j] = fDate.Day.ToString();
                }

                var getid = _db.Prod_Reference.Where(a => a.ReferenceDate == fDate && a.Active == true);
                if (getid.Any())
                {
                    int id = getid.FirstOrDefault().ID;
                    targetData.Add(GetDailyTotalTargetSectionWise(id, (int)EnumProcess.Sewing));
                    planData.Add(1500);
                    achievData.Add(GetDailyTotalProductionSectionWise(id, (int)EnumProcess.Sewing));
                }
                else
                {
                    targetData.Add(0);
                    planData.Add(0);
                    achievData.Add(0);
                }
                fDate = fDate.AddDays(1);
                ++j;
            }

            datasets plan = new datasets();
            plan.data = new List<int>();
            plan.label = "Planning";
            plan.fill = false;
            plan.backgroundColor = "red";
            plan.borderColor = "red";
            plan.data.AddRange(planData);
            vMProductionReports.Add(plan);

            datasets target = new datasets();

            target.data = new List<int>();
            target.label = "Excution Plan";
            target.fill = false;
            target.backgroundColor = "blue";
            target.borderColor = "blue";
            target.data.AddRange(targetData);
            vMProductionReports.Add(target);

            datasets achive = new datasets();
            achive.data = new List<int>();
            achive.label = "Production";
            achive.fill = false;
            achive.backgroundColor = "green";
            achive.borderColor = "green";
            achive.data.AddRange(achievData);
            vMProductionReports.Add(achive);


            lg.labels = level;
            lg.datasets.AddRange(vMProductionReports);

            return lg;



        }
        #endregion



    }
}
