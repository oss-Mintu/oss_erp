﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Erp.Core.Services.HRMS
{
    public class VMShift : BaseVM
    {
        //public VMShift()
        //{
        //    StartTime = DateTime.Now.TimeOfDay;
        //    EndTime = DateTime.Now.TimeOfDay;
        //}
        public int SId { get; set; }

        [Required]
        [DisplayName("Shift Name")]
        public string ShiftName { get; set; }

        [Required]
        [DisplayName("Start")]
        [DataType(DataType.Time)]
        public string StartTime { get; set; }

        [Required]
        [DisplayName("End")]
        [DataType(DataType.Time)]
        public string EndTime { get; set; }

        [Required]
        [DisplayName("Break Duration")]
        public int BreakTime { get; set; }
        [DisplayName("Remarks")]
        public string Remarks { get; set; }

        public IList<VMShift> VMShifts { get; set; }
    }

    public class VMShiftAssign : BaseVM
    {
        public VMShiftAssign()
        {
            BusinessUnitList = new SelectList(new List<object>(), "Value", "Text");
            UnitList = new SelectList(new List<object>(), "Value", "Text");
            SectionList = new SelectList(new List<object>(), "Value", "Text");
            DepartmentList = new SelectList(new List<object>(), "Value", "Text");
            ShiftList = new SelectList(new List<object>(), "Value", "Text");
        }
        public bool IsAssign { get; set; } = false;
        public bool IsForListView { get; set; }
        [DisplayName("Shift Name")]
        [Required(ErrorMessage ="Please Select Shift")]
        public int VMShiftID { get; set; }
        public string VMShiftName { get; set; }
        [DisplayName("Shift Name")]
        public int VMFilterByShiftID { get; set; }
        [DisplayName("Sister Concern")]
        public int VMBusinessUnitID { get; set; }
        public string VMBusinessUnitName { get; set; }
        public DateTime AssignedDate { get; set; }
        public string AssignedDateControlString { get { return this.AssignedDate != DateTime.MinValue ? this.AssignedDate.ToString("yyyy-MM-dd") : ""; } }
        public string AssignedDateGridString { get { return this.AssignedDate != DateTime.MinValue ? this.AssignedDate.ToString("MMM dd, yyyy") : ""; } }
        [DisplayName("Start Date")]
        [Required(ErrorMessage ="Start Date is Mandatory")]
        public DateTime StartDate { get; set; }
        public string StartDateControlString { get { return this.StartDate != DateTime.MinValue ? this.StartDate.ToString("yyyy-MM-dd") : ""; } }
        public string StartDateGridString { get { return this.StartDate != DateTime.MinValue ? this.StartDate.ToString("MMM dd, yyyy") : ""; } }
        [DisplayName("Start Date")]
        public DateTime? FilterByStartDate { get; set; }
        public string FilterByStartDateControlString { get { return this.FilterByStartDate != DateTime.MinValue && this.FilterByStartDate != null ? this.FilterByStartDate.Value.ToString("yyyy-MM-dd") : ""; } }
        [DisplayName("End Date")]
        [Required(ErrorMessage = "End Date is Mandatory")]
        public DateTime EndDate { get; set; }
        public string EndDateControlString { get { return this.EndDate != DateTime.MinValue ? this.EndDate.ToString("yyyy-MM-dd") : ""; } }
        public string EndDateGridString { get { return this.EndDate != DateTime.MinValue ? this.EndDate.ToString("MMM dd, yyyy") : ""; } }
        [DisplayName("End Date")]
        public DateTime? FilterByEndDate { get; set; }
        public string FilterByEndDateControlString { get { return this.FilterByEndDate != DateTime.MinValue && this.FilterByEndDate != null ? this.FilterByEndDate.Value.ToString("yyyy-MM-dd") : ""; } }
        [DisplayName("Department Name")]
        public int VMDepartmentID { get; set; }
        public string VMDepartmentName { get; set; }
        [DisplayName("Section Name")]
        public int VMSectionID { get; set; }
        public string VMSectionName { get; set; }
        [DisplayName("Business Unit")]
        public int VMUnitID { get; set; }
        public string VMUnitName { get; set; }
        public int VMEmployeeID { get; set; }
        public string VMEmployeeName { get; set; }
        public string VMEmployeeIdentity { get; set; }
        public string VMDesignationName { get; set; }

        public int? Count { get; set; }
        public int? Min { get; set; }

        public List<VMShiftAssign> DataList { get; set; } = new List<VMShiftAssign>();
        public SelectList BusinessUnitList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        public SelectList UnitList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        public SelectList SectionList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        public SelectList DepartmentList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        public SelectList ShiftList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
    }

    public class enumName
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
