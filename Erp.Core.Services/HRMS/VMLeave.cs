﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Erp.Core.Services.HRMS
{
    public class VMLeave : BaseVM
    {
        [Required]
        [DisplayName("Leave Type")]
        public int LeaveTypeId { get; set; }
        [Required]
        [DisplayName("Year")]
        public string year { get; set; }
        [DisplayName("Leave Days")]
        public int LeaveDays { get; set; }
        [DisplayName("Sister Concern")]
        public int BU_Id { get; set; }
        [DisplayName("Department Name")]
        public int Dept_ID { get; set; }
        [DisplayName("Section Name")]
        public int Section_ID { get; set; }
        [DisplayName("Business Unit")]
        public int Unit_ID { get; set; }
        [DisplayName("Employee")]
        public int EmployeeId { get; set; }
        public int EmployeeTypeId { get; set; }

        public SelectList B_UnitList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        public SelectList EmpType { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        public SelectList Unit_List { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        public SelectList Department_List { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        public SelectList Section_List { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        public SelectList LeaveTypeList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        public IList<Emplist> Emplists { get; set; }
    }

    public class Emplist : QueryPropertyVM
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string EmployeeIdentity { get; set; }
        public string Designation { get; set; }
        public string BusinessUnit { get; set; }
        public string Unit { get; set; }
        public string Section { get; set; }
        public string Department { get; set; }
        public string CardNo { get; set; }

        public string EmpType { get; set; }
        public int? AnnualLeaveDays { get; set; }
        public int? TakenAnnualLeaveDays { get; set; }
        public int? RemainingAnnualLeaveDays
        {
            get { return AnnualLeaveDays - TakenAnnualLeaveDays; }
        }

        public int? CasualLeaveDays { get; set; }
        public int? TakenCasualLeaveDays { get; set; }
        public int? RemainingCasualLeaveDays
        {
            get { return CasualLeaveDays - TakenCasualLeaveDays; }
        }

        public int? SickLeaveDays { get; set; }
        public int? TakenSickLeaveDays { get; set; }
        public int? RemainingSickLeaveDays
        {
            get { return SickLeaveDays - TakenSickLeaveDays; }
        }
        public int? MaternityLeaveDays { get; set; }
        public int? TakenMaternityLeaveDays { get; set; }
        public int? RemainingMaternityLeaveDays
        {
            get { return MaternityLeaveDays - TakenMaternityLeaveDays; }
        }

        public bool IsLeaveAssigned { get; set; }
        public bool IsWeekendAssigned { get; set; }

        public List<string> WeekendDays { get; set; }

    }

    public class VMOffDay : BaseVM
    {
        public int OId { get; set; }
        [Required]
        [DisplayName("Offday Title")]
        public string Title { get; set; }
        [Required]
        [DisplayName("Description")]
        public string Description { get; set; }
        [Required]
        [DisplayName("Applicable Date")]
        public DateTime? ApplicableDate { get; set; }
        [DisplayName("Replacement Date (If Any)")]
        public DateTime? ReplacementDate { get; set; }
    }
    public class VMWeekendDay
    {
        [Required]
        [DisplayName("Sister Concern")]
        public int BU_Id { get; set; }

        [DisplayName("Department Name")]
        public int Dept_ID { get; set; }

        [DisplayName("Section Name")]
        public int Section_ID { get; set; }

        [DisplayName("Business Unit")]
        public int Unit_ID { get; set; }

        [DisplayName("Weekend Day")]
        [Required]
        public string[] WeekendDay { get; set; }

        public IList<Emplist> Emplists { get; set; }

        //[DisplayName("Employee")]
        //[Required]
        //public int EmployeeId { get; set; }

        public SelectList B_UnitList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        public SelectList Unit_List { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        public SelectList Department_List { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        public SelectList Section_List { get; set; } = new SelectList(new List<object>(), "Value", "Text");
    }

    public class VMHoliday : BaseVM
    {
        public int Id { get; set; }
        [Required]
        [DisplayName("Holiday Name")]
        public string HoildayName { get; set; }
        [Required]
        [DisplayName("From Date")]
        public DateTime FromDate { get; set; } = DateTime.Now;
        [Required]
        [DisplayName("To Date")]
        public DateTime ToDate { get; set; } = DateTime.Now;

        [DisplayName("Description")]
        public string Description { get; set; }

        public int TotalDays { get; set; }

        public string[] WeekenedDay { get; set; }

        public bool IsCompanyWeekendDay { get; set; }

        public string PreviousHolidayName { get; set; }

        public List<VMHoliday> VMHolidays { get; set; }
    }
    public class VMLeaveApplication
    {   [Required]
        [DisplayName("Sistern Concern")]
        public int BU_Id { get; set; }
        [DisplayName("Department Name")]
        [Required]
        public int Dept_ID { get; set; }
        [DisplayName("Section Name")]
        [Required]
        public int Section_ID { get; set; }
        [DisplayName("Business Unit")]
        [Required]
        public int Unit_ID { get; set; }

        [DisplayName("Section")]
        public string SectionName { get; set; }
        [DisplayName("Designation")]
        public string DesignationName { get; set; }
        [DisplayName("Department")]
        public string DepartmentName { get; set; }
        [DisplayName("Unit")]
        [Required]
        public string UnitName { get; set; }
        [DisplayName("Employee ID")]
        public int? EmployeeId { get; set; }
        [DisplayName("Replace By")]
        public int ReplacementEmployeeId { get; set; }
        public string Name { get; set; }
        [DisplayName("Leave Type")]
        public int LeaveTypeId { get; set; }

        [DisplayName("From")]
        [Required]
        public DateTime? From { get; set; }
        [DisplayName("To")]
        [Required]
        public DateTime? To { get; set; }
        [DisplayName("Total Days")]
        public int TotalDays { get; set; }
        public string Purpose { get; set; }
        [DisplayName("Stay During Leave")]
        public string StayDuringLeave { get; set; }
        [DisplayName("Remarks")]
        public string Remarks { get; set; }
        [DisplayName("Payment Type")]
        public int LeavePaidType { get; set; }
        [DisplayName("Leave Status")]
        public int LeaveStatus { get; set; }
        [DisplayName("Approved By")]
        [Required]
        public int? ApprovedById { get; set; }
        [DisplayName("A.P.M/ P.M/ F.M")]
        [Required]
        public int? ManagerId { get; set; }
        [DisplayName("Section In charge")]
        [Required]
        public int? SectionInchargeId { get; set; }

        public DateTime? JoiningDate { get; set; }

        public IList<LeaveDays> LeaveDays { get; set; }
        public SelectList B_UnitList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        public SelectList Unit_List { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        public SelectList Department_List { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        public SelectList Section_List { get; set; } = new SelectList(new List<object>(), "Value", "Text");


    }

    public class LeaveDays : QueryPropertyVM
    {
        public int ID { get; set; }
        public int eId { get; set; }
        public string Name { get; set; }
        public string EmployeeIdentity { get; set; }
        public string CardNo { get; set; }
        [DisplayFormat(DataFormatString = "{0:d}")]
        public DateTime? From { get; set; }
        [DisplayFormat(DataFormatString = "{0:d}")]
        public DateTime? To { get; set; }
        public int TotalDays { get; set; }
        public int DeptStatus { get; set; }
        public int HRStatus { get; set; }
        public int? LeaveType { get; set; }
        public EnumLeaveType leaveName { get; set; }
    }
    public class VMEarnLeave
    {
        [DisplayName("Business Unit")]
        public string BU_Id { get; set; }
        [DisplayName("Joining Date")]
        public DateTime JoiningDate { get; set; }
        [DisplayName("Upto Date")]
        public DateTime To { get; set; }
        [DisplayName("Employee")]
        public int EmployeeId { get; set; }
    }
    public class VMLeaveSetup: BaseVM
    {
        public int Id { get; set; }
        public string Name { get; set; }
        [Required]
        [DisplayName("Leave Name")]
        public int LeaveTypeID { get; set; }

        [DisplayName("Leave Type")]
        public int LeaveType { get; set; }

        [Required]
        [DisplayName("Leave Days")]
        public int LeaveDays { get; set; }

        [Required]
        [DisplayName("Employee Types")]
        public int EmployeeTypeId { get; set; }
    }

    //property for SearchQuery
    public class QueryPropertyVM
    {
        public int? LeaveTypeId { get; set; }
        public string year { get; set; }
        public DateTime? from { get; set; }
        public DateTime? to { get; set; }
        public int? LeaveDays { get; set; }
        public int? BU_Id { get; set; }
        public int? Dept_ID { get; set; }
        public int? Section_ID { get; set; }
        public int? Unit_ID { get; set; }
        public int? EmployeeId { get; set; }
        public int? DesignationId { get; set; }
    }
    public class VMEarnLeaveCalculation
    {
        public VMEarnLeaveCalculation()
        {
            EarnLeaveDate = DateTime.Now;
            From = DateTime.Now;
            To = DateTime.Now;
        }
        [DisplayName("Date")]
        [Required]
        public DateTime EarnLeaveDate { get; set; }

        [DisplayName("From")]
        [Required]
        public DateTime From { get; set; }
        [DisplayName("To")]
        [Required]
        public DateTime To { get; set; }

        public IList<VMEarnLeaveCalculationList> VMEarnLeaveCalculationList { get; set; }

        [DisplayName("Business Unit")]
        [Required]
        public int? BU_Id { get; set; }
        [DisplayName("Department")]
        [Required]
        public int? Dept_ID { get; set; }
        [DisplayName("Section")]
        [Required]
        public int? Section_ID { get; set; }
        [DisplayName("Unit")]
        [Required]
        public int? Unit_ID { get; set; }
        public SelectList B_UnitList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        public SelectList Unit_List { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        public SelectList Department_List { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        public SelectList Section_List { get; set; } = new SelectList(new List<object>(), "Value", "Text");

    }
    public class VMEarnLeaveCalculationList
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string EmployeeIdentity { get; set; }
        public string Designation { get; set; }
        public string BusinessUnit { get; set; }
        public string Unit { get; set; }
        public string Section { get; set; }
        public string Department { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime JoiningDate { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime LastPaymentDate { get; set; }

        public int TotalMonth { get; set; }
        public int WorkingDays { get; set; }
        public int Holidays { get; set; }
        public int Offdays { get; set; }
        public int TotalLeave { get; set; }
        public int Absent { get; set; }
        public int TotalDeductionDays { get; set; }
        public int PayableWorkingDays { get; set; }
        public int TotalEarnLeave { get; set; }
        public int EnjoyableEarnLeaveDays { get; set; }
        public int PayableEarnLeave { get; set; }
        public int Stamp { get; set; }
        public decimal PayableEarnLeaveTk { get; set; }

        public bool IsSelected { get; set; }

    }
    public class VMServiceBenefitCalculation
    {
        public VMServiceBenefitCalculation()
        {
            From = DateTime.Now;
            To = DateTime.Now;
        }
        [DisplayName("Employee")]
        [Required]
        public int EmployeeID { get; set; }

        [DisplayName("From")]
        [Required]
        public DateTime From { get; set; }
        [DisplayName("To")]
        [Required]
        public DateTime To { get; set; }

        public IList<VMServiceBenefitCalculationList> VMServiceBenefitCalculationLists { get; set; }
    }
    public class VMServiceBenefitCalculationList
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string EmployeeIdentity { get; set; }
        public string Designation { get; set; }
        public string BusinessUnit { get; set; }
        public string Unit { get; set; }
        public string Section { get; set; }
        public string Department { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime JoiningDate { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime ResignationDate { get; set; }

        public int Year { get; set; }
        public int Month { get; set; }
        public int Day { get; set; }
        public int PayableServiceBenifitDays { get; set; }

        public decimal PresentSalary { get; set; }
        public decimal PresentBasic { get; set; }
        public decimal PresentBasicPerDay { get; set; }
        public decimal TotalAmount { get; set; }
        public int Stamp { get; set; }
        public decimal PayableAmount { get; set; }
    }

    public class EmployeeInfo
    {
        public int ID { get; set; }
        public int? dPid { get; set; }
        public int? dId { get; set; }
        public string EmIdentity { get; set; }
        public string EmployeeName { get; set; }
        public byte[] EmImage { get; set; }
        public string EmMName { get; set; }        
        public string EmFName { get; set; }
        public string EmHName { get; set; }
        public string EmGender { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public DateTime? JoinDate { get; set; }
        public string Department { get; set; }
        public string Designation { get; set; }
        public string Section { get; set; }
        public string Unit { get; set; }
        public string BusinessUnit { get; set; }
        public string PresentAddress { get; set; }
        public string PermanentAddress { get; set; }
        public string CardNo { get; set; }
        public string PLName { get; set; }
        public string Grade { get; set; }

        public int Year { get; set; }
        public int Month { get; set; }
        public int Day { get; set; }

        public decimal GrossSalary { get; set; }
        public decimal BasicPer { get; set; }
        public decimal BasicSalary { get { return ( BasicPer != 0 ? (GrossSalary / BasicPer / 100) : 0); } }
        public decimal HousePer { get; set; }
        public decimal HouseRent { get { return (BasicPer != 0 ? (GrossSalary / HousePer / 100) : 0); } }
        public decimal MedicalPer { get; set; }
        public decimal MedicalAllowance { get { return (BasicPer != 0 ? (GrossSalary / MedicalPer / 100) : 0); } }
        public decimal FoodPer { get; set; }
        public decimal FoodAllowannce { get { return (BasicPer != 0 ? (GrossSalary / FoodPer / 100) : 0); } }
        public decimal TransportPer { get; set; }
        public decimal TransportAllowance { get { return (BasicPer != 0 ? (GrossSalary / TransportPer / 100) : 0); } }

        public decimal TotalSalary { get { return BasicSalary+ HouseRent + MedicalAllowance + FoodAllowannce  + TransportAllowance; } }

        public List<HRMSJobDescriptionVM> JobDescription { get; set; }
    }
}
