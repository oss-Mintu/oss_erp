﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Erp.Core.Services.HRMS
{
    public class VMAttendance : BaseVM
    {
        //public int UserID { get; set; }
        public EnumAttendanceStatus AttendanceStatus { get; set; }
        public string AttendanceStatusString { get { return BaseFunctionalities.GetEnumDescription(AttendanceStatus); } }
        /* Present = 1,
           Late = 2,
           Absent = 3
        */
        public SelectList AttendanceStatusList { get { return new SelectList(BaseFunctionalities.GetEnumList<EnumAttendanceStatus>().Where(x=> (int)x.GetType().GetProperty("Value").GetValue(x, null) == 1 || (int)x.GetType().GetProperty("Value").GetValue(x, null) == 2 || (int)x.GetType().GetProperty("Value").GetValue(x, null) == 3).ToList(), "Value", "Text"); } }
        [DisplayName("Update Reason")]
        public string Remarks { get; set; }
        public SelectList RemarksList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        [DisplayName("Remakrs")]
        public string ExtendedRemarks { get; set; }
        [DisplayName("Section")]
        public int SectionId { get; set; }
        public string SectionName { get; set; }
        public SelectList SectionList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        [DisplayName("Department")]
        public int DepartmentID { get; set; }
        public string DepartmentName { get; set; }
        public SelectList DepartmentList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        [DisplayName("Shift")]
        public int ShiftId { get; set; }
        public string ShiftName { get; set; }
        public SelectList ShiftList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        [DisplayName("Business Unit")]
        public int BusinessUnitId { get; set; }
        public string BusinessUnitName { get; set; }
        public SelectList BusinessUnitList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        [DisplayName("Unit")]
        public int UnitId { get; set; }
        public string UnitName { get; set; }
        public SelectList UnitList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        [DisplayName("Employee")]
        public int EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeCardNo { get; set; }
        public SelectList EmployeeList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        [DisplayName("Date")]
        public DateTime AttendanceDate { get; set; }
        public string AttendanceDateControlString { get { return this.AttendanceDate != DateTime.MinValue ? this.AttendanceDate.ToString("yyyy-MM-dd") : ""; } }
        public string AttendanceDateGridString { get { return this.AttendanceDate != DateTime.MinValue ? this.AttendanceDate.ToString("MMMM dd, yyyy") : ""; } }
        public DateTime InTime { get; set; }
        public string InTimeString { get { return this.InTime != DateTime.MinValue ? this.InTime.ToString("hh:mm tt") : ""; } }
        public DateTime OutTime { get; set; }
        public string OutTimeString { get { return this.OutTime != DateTime.MinValue ? this.OutTime.ToString("hh:mm tt") : ""; } }
        [DisplayName("From Date")]
        
        public DateTime? FromDate { get; set; }
        public string FromDateControlString { get { return this.FromDate.HasValue && this.FromDate.Value != DateTime.MinValue ? this.FromDate.Value.ToString("yyyy-MM-dd") : ""; } }
        public string FromDateGridString { get { return this.FromDate.HasValue && this.FromDate.Value != DateTime.MinValue ? this.FromDate.Value.ToString("MMMM dd, yyyy") : ""; } }
        [DisplayName("To Date")]
        
        public DateTime? ToDate { get; set; }
        public string ToDateControlString { get { return this.ToDate.HasValue && this.ToDate.Value != DateTime.MinValue ? this.ToDate.Value.ToString("yyyy-MM-dd") : ""; } }
        public string ToDateGridString { get { return this.ToDate.HasValue && this.ToDate.Value != DateTime.MinValue ? this.ToDate.Value.ToString("MMMM dd, yyyy") : ""; } }

        public string DateRange { get { return this.FromDate.HasValue && this.ToDate.HasValue && this.FromDate != DateTime.MinValue && this.ToDate != DateTime.MinValue ? "Attendance History From " + this.FromDateGridString + " To " + this.ToDateGridString : ""; } }
        public List<VMAttendance> DataList { get; set; } = new List<VMAttendance>();
    }

    public class VMAttendanceRemarks : BaseVM
    {
        [DisplayName("Remark")]
        [Required(ErrorMessage ="What is the Remark?")]
        public string AttendanceRemark { get; set; }
        public List<VMAttendanceRemarks> DataList { get; set; } = new List<VMAttendanceRemarks>();
    }
}
