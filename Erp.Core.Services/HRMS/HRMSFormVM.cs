﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Erp.Core.Services.HRMS
{
    public class EmployeeFormVM: BaseVM
    {
        public int StatusId { get; set; }
        public SelectList Status { get { return new SelectList(BaseFunctionalities.GetEnumList<EnumEmployeeStatus>(), "Value", "Text"); } }

        public int EmployeeId { get; set; }
        public SelectList EmployeeList { get; set; } = new SelectList(new List<object>(), "Value", "Text");

        [Display(Name = "Application Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime ApplicationDate { get; set; } = DateTime.Now;
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "From Date")]
        public DateTime FromDate { get; set; } = DateTime.Now;
        [Display(Name = "To Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime ToDate { get; set; } = DateTime.Now;
        [Display(Name = "Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime Date1 { get; set; } = DateTime.Now;
        [Display(Name = "Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime Date2 { get; set; } = DateTime.Now;
        [Display(Name = "EDD Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime EDDDate { get; set; } = DateTime.Now;
        [Display(Name = "Payment Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime PaymentDate { get; set; } = DateTime.Now;

        public int TestValue { get; set; }
        public int TestValue1 { get; set; }
        public int TestValue2 { get; set; }

        public EmployeeInfo EmployeeInfo { get; set; }

    }
}
