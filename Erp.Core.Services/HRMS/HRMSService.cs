﻿using Erp.Core.Entity.Common;
using Erp.Core.Entity.HRMS;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Erp.Core.Services.HRMS
{
    public class HRMSService : BaseService
    {
        public HRMSService(IErpDbContext db) => _db = db;

        #region Dropdown
        public List<object> DepartmentDropDownList()
        {
            var List = new List<object>();
            _db.User_Department
                .Where(x => x.Active == true).Select(x => x).ToList()
                .ForEach(x => List.Add(new
                {
                    Value = x.ID,
                    Text = x.Name
                }));
            return List;
        }
        public List<object> BusinessUnitDropDownList()
        {
            var List = new List<object>();
            _db.HRMS_BusinessUnit
                .Where(x => x.Active).Select(x => x).ToList()
                .ForEach(x => List.Add(new
                {
                    Value = x.ID,
                    Text = x.Name
                }));
            return List;
        }

        public List<object> CompanyHierarchyLabelDropDownList()
        {
            var List = new List<object>();
            _db.HRMS_CompanyHierarchyLabel
                .Where(x => x.Active).Select(x => x).ToList()
                .ForEach(x => List.Add(new
                {
                    Value = x.ID,
                    Text = x.Name
                }));
            return List;
        }

        public List<object> UnitDropDownList()
        {
            var List = new List<object>();
            _db.HRMS_Unit
                .Where(x => x.Active).Select(x => x).ToList()
                .ForEach(x => List.Add(new
                {
                    Value = x.ID,
                    Text = x.Name
                }));
            return List;
        }
        public List<object> LeaveTypeDropDownList(int id)
        {
            if (id == 1)
            {
                var List = new List<object>();
                _db.HRMS_Leave
                    .Where(x => x.Active).Select(x => x).ToList()
                    .ForEach(x => List.Add(new
                    {
                        Value = x.ID,
                        Text = (EnumLeaveType)x.LeaveType,                       
                    }));
                return List;
            } else if (id == 2)
            {
                var List = new List<object>();
                _db.HRMS_Leave
                    .Where(x => x.Active && x.EmployeeType == 1).Select(x => x).ToList()
                    .ForEach(x => List.Add(new
                    {
                        Value = x.ID,
                        Text = (EnumLeaveType)x.LeaveType
                    }));
                return List;
            }
            else
            {
                var List = new List<object>();
                _db.HRMS_Leave
                    .Where(x => x.Active && x.EmployeeType == 2).Select(x => x).ToList()
                    .ForEach(x => List.Add(new
                    {
                        Value = x.ID,
                        Text = (EnumLeaveType)x.LeaveType
                        //Text = (EnumLeaveType)x.LeaveType + " (" + (EnumEmployeeType)x.EmployeeType + ")"

                    }));
                return List;
            }
        }

        public List<object> SectiontDropDownList()
        {
            var List = new List<object>();
            _db.HRMS_Section
                .Where(x => x.Active).Select(x => x).ToList()
                .ForEach(x => List.Add(new
                {
                    Value = x.ID,
                    Text = x.Name
                }));
            return List;
        }

        public IEnumerable LineDropDownList()
        {
            var linelist = new List<object>();
            linelist.Add(new { Text = "Line-1", Value = 1 });
            linelist.Add(new { Text = "Line-2", Value = 2 });
            linelist.Add(new { Text = "Line-3", Value = 3 });
            return linelist;
        }
        //public IEnumerable DDepartmentDropDownList()
        //{
        //    var department = new List<object>();
        //    department.Add(new { Text = "Admin", Value = 1 });
        //    department.Add(new { Text = "Commercial", Value = 2 });
        //    department.Add(new { Text = "Procurement", Value = 3 });
        //    department.Add(new { Text = "Marchendising", Value = 4 });
        //    department.Add(new { Text = "Store", Value = 5 });
        //    department.Add(new { Text = "Constraction", Value = 6 });
        //    department.Add(new { Text = "Accounts", Value = 7 });
        //    department.Add(new { Text = "Commercial", Value = 8 });
        //    return department;
        //}

        public async Task<SelectList> CountryDropDownListAsync()
        {
            return new SelectList(await _db.Common_Country.Where(x => x.Active).Select(x => new { Value = x.ID, Text = x.Name }).ToListAsync(), "Value", "Text");
        }
        public async Task<SelectList> DistrictDropDownListAsync(int id = 0)
        {
            return new SelectList(await _db.HRMS_District.Where(x => x.Active && (id <= 0 || x.Common_CountryFK == id)).Select(x => new { Value = x.ID, Text = x.Name }).ToListAsync(), "Value", "Text");
        }
        public async Task<SelectList> DegreeDropDownListAsync()
        {
            return new SelectList(await _db.HRMS_EducationalDegree.Where(x => x.Active).Select(x => new { Value = x.ID, Text = x.Name }).ToListAsync(), "Value", "Text");
        }
        public async Task<SelectList> PoliceStationDropDownListAsync(int id = 0)
        {
            return new SelectList(await _db.HRMS_PoliceStation.Where(x => x.Active && (id <= 0 || x.HRMS_DistrictFK == id)).Select(x => new { Value = x.ID, Text = x.Name }).ToListAsync(), "Value", "Text");
        }
        public async Task<SelectList> PostOfficeDropDownListAsync(int id = 0)
        {
            return new SelectList(await _db.HRMS_PostOffice.Where(x => x.Active && (id <= 0 || x.HRMS_PoliceStationFK == id)).Select(x => new { Value = x.ID, Text = x.Name }).ToListAsync(), "Value", "Text");
        }
        public async Task<SelectList> DesignationDropDownListAsync()
        {
            return new SelectList(await _db.HRMS_Designation.Where(x => x.Active).Select(x => new { Value = x.ID, Text = x.Name }).ToListAsync(), "Value", "Text");
        }
        public async Task<SelectList> ReportingEmployeeList()
        {
            return new SelectList(await _db.HRMS_Employee.Where(x => x.Active).Select(x => new { Value = x.ID, Text = x.Name + '(' + x.EmployeeIdentity + ')' }).ToListAsync(), "Value", "Text");
        }
        public async Task<SelectList> EmployeeHierarchyLevelList()
        {
            return new SelectList(await _db.HRMS_CompanyHierarchyLabel.Where(x => x.Active).Select(x => new { Value = x.ID, Text = x.Name }).ToListAsync(), "Value", "Text");
        }
        public async Task<SelectList> BusinessUnitDropDownListAsync()
        {
            return new SelectList(await _db.HRMS_BusinessUnit.Where(x => x.Active).Select(x => new { Value = x.ID, Text = x.Name }).ToListAsync(), "Value", "Text");
        }
        public async Task<SelectList> DepartmentDropDownListAsync()
        {
            return new SelectList(await _db.User_Department.Where(x => x.Active).Select(x => new { Value = x.ID, Text = x.Name }).ToListAsync(), "Value", "Text");
        }
        public async Task<SelectList> UnitDropDownListAsync(int id = 0)
        {
            return new SelectList(await _db.HRMS_Unit.Where(x => x.Active && (id <= 0 || x.HRMS_BusinessUnitFK == id)).Select(x => new { Value = x.ID, Text = x.Name }).ToListAsync(), "Value", "Text");
        }
        public async Task<List<HRMSUnitVM>> UnitListGetByBUIdAsync(int id = 0)
        {
            return await _db.HRMS_Unit.Where(x => x.Active && (id == 0 || id == x.HRMS_BusinessUnitFK)).Select(x => new HRMSUnitVM() { ID = x.ID, Name = x.Name }).ToListAsync();
        }
        //public async Task<List<HRMSSectionVM>> SectionListGetByDIdAsync(int id = 0)
        //{
        //    return await _db.HRMS_Section.Where(x => x.Active && (id == 0 || id == x.User_DepartmentFK)).Select(x => new HRMSSectionVM() { ID = x.ID, Name = x.Name }).ToListAsync();
        //}
        public async Task<List<HRMSSectionVM>> SectionListGetByDIdAsync(int id = 0)
        {
            return await _db.HRMS_Section.Where(x => x.Active && (id == 0 || id == x.HRMS_UnitFK)).Select(x => new HRMSSectionVM() { ID = x.ID, Name = x.Name }).ToListAsync();
        }
        public async Task<List<HRMSDesignationVM>> DesignationListGetByLIdAsync(int id = 0)
        {
            return await _db.HRMS_Designation.Where(x => x.Active && (id == 0 || id == x.HRMS_CompanyHierarchyLabelFK)).Select(x => new HRMSDesignationVM() { ID = x.ID, Name = x.Name + '(' + x.User_Department.Name +')' }).ToListAsync();
        }
        //public async Task<SelectList> SectionDropDownListAsync(int id = 0)
        //{
        //    return new SelectList(await _db.HRMS_Section.Where(x => x.Active && (id <= 0 || x.User_DepartmentFK == id)).Select(x => new { Value = x.ID, Text = x.Name }).ToListAsync(), "Value", "Text");
        //}
        public async Task<SelectList> SectionDropDownListAsync(int id = 0)
        {
            return new SelectList(await _db.HRMS_Section.Where(x => x.Active && (id <= 0 || x.HRMS_UnitFK == id)).Select(x => new { Value = x.ID, Text = x.Name }).ToListAsync(), "Value", "Text");
        }
        public async Task<List<HRMSDesignationVM>> DesignationListGetByDIdAsync(int id = 0)
        {
            return await _db.HRMS_Designation.Where(x => x.Active && (id == 0 || id == x.User_DepartmentFK)).Select(x => new HRMSDesignationVM() { ID = x.ID, Name = x.Name }).ToListAsync();
        }
        public async Task<SelectList> DesignationDropDownListAsync(int id = 0)
        {
            return new SelectList(await _db.HRMS_Designation.Where(x => x.Active && (id <= 0 || x.User_DepartmentFK == id)).Select(x => new { Value = x.ID, Text = x.Name }).ToListAsync(), "Value", "Text");
        }
        public async Task<SelectList> LineDropDownListAsync()
        {
            return new SelectList(await _db.Common_ProductionLine.Where(x => x.Active).Select(x => new { Value = x.ID, Text = x.Name }).ToListAsync(), "Value", "Text");
        }
        public async Task<SelectList> ShiftDropDownListAsync()
        {
            return new SelectList(await _db.HRMS_Shift.Where(x => x.Active).Select(x => new { Value = x.ID, Text = x.ShiftName }).ToListAsync(), "Value", "Text");
        }
        public async Task<SelectList> AttendanceRemarksDropDownListAsync()
        {
            return new SelectList(await _db.HRMS_AttendanceRemarks.Where(x => x.Active).Select(x => new { Value = x.AttendanceRemark, Text = x.AttendanceRemark }).ToListAsync(), "Value", "Text");
        }
        public async Task<SelectList> EmployeeDropDownListAsync()
        {
            return new SelectList(await _db.HRMS_Employee.Where(x => x.Active).Select(x => new { Value = x.ID, Text = x.CardNo + " : " + x.Name }).ToListAsync(), "Value", "Text");
        }

        public async Task<List<HRMSDistrictVM>> DistrictGetByBUIdAsync(int id)
        {
            return await _db.HRMS_District.Where(x => x.Active && (id == 0 || id == x.Common_CountryFK)).Select(x => new HRMSDistrictVM() { ID = x.ID, Name = x.Name }).ToListAsync();
        }
        public async Task<List<HRMSPoliceStationVM>> PoliceStationGetByBUIdAsync(int id)
        {
            return await _db.HRMS_PoliceStation.Where(x => x.Active && (id == 0 || id == x.HRMS_DistrictFK)).Select(x => new HRMSPoliceStationVM() { ID = x.ID, Name = x.Name }).ToListAsync();
        }
        public async Task<List<HRMSPostOfficeVM>> PostOfficeGetByBUIdAsync(int id)
        {
            return await _db.HRMS_PostOffice.Where(x => x.Active && (id == 0 || id == x.HRMS_PoliceStationFK)).Select(x => new HRMSPostOfficeVM() { ID = x.ID, Name = x.Name }).ToListAsync();
        }
        public async Task<List<VMLeaveSetup>> GetLeaveTypeByEmpType(int id)
        {
            return await _db.HRMS_Leave.Where(x => x.Active && (id == 0 || id == x.EmployeeType)).Select(x => new VMLeaveSetup() { ID = x.ID, Name = Convert.ToString((EnumLeaveType)x.LeaveType) }).ToListAsync();
        }
        public List<object> ShiftDropDownList()
        {
            var List = new List<object>();
            _db.HRMS_Shift
                .Where(x => x.Active).Select(x => x).ToList()
                .ForEach(x => List.Add(new
                {
                    Value = x.ID,
                    Text = x.ShiftName
                }));
            return List;
        }        
        public IEnumerable LeavePaidTypeDropDownList()
        {
            var PaymentTypeList = new List<object>();
            PaymentTypeList.Add(new SelectListItem
            {
                Text = "-Select Payment Type-",
                Value = ""
            });
            foreach (var eVal in Enum.GetValues(typeof(EnumLeavePaidType)))
            {
                PaymentTypeList.Add(new SelectListItem { Text = Enum.GetName(typeof(EnumLeavePaidType), eVal), Value = (Convert.ToInt32(eVal)).ToString() });
            }
            return PaymentTypeList;
        }

        public IEnumerable FixedLeaveTypeDropDownList()
        {
            var leaveList = new List<SelectListItem>();
            leaveList.Add(new SelectListItem
            {
                Text = "-Select Leave-",
                Value = ""
            });
            foreach (var eVal in Enum.GetValues(typeof(EnumLeaveType)))
            {
                leaveList.Add(new SelectListItem { Text = Enum.GetName(typeof(EnumLeaveType), eVal), Value = (Convert.ToInt32(eVal)).ToString()});
            }
            return leaveList;
        }
        public IEnumerable GetEmployeeType()
        {
            var EmployeeTypeList = new List<object>();
            EmployeeTypeList.Add(new SelectListItem
            {
                Text = "-Select Employee Type-",
                Value = ""
            });
            foreach (var eVal in Enum.GetValues(typeof(EnumStaffType)))
            {
                EmployeeTypeList.Add(new SelectListItem { Text = Enum.GetName(typeof(EnumStaffType), eVal), Value = (Convert.ToInt32(eVal)).ToString() });
            }
            return EmployeeTypeList;
        }
        public IEnumerable OffdayDropDownList()
        {
            var holidaylist = new List<object>();
            holidaylist.Add(new SelectListItem
            {
                Text = "-Select Offday-",
                Value = ""
            });
            foreach (var eVal in Enum.GetValues(typeof(EnumWeekendDay)))
            {
                holidaylist.Add(new SelectListItem { Text = Enum.GetName(typeof(EnumWeekendDay), eVal), Value = (Convert.ToInt32(eVal)).ToString() });
            }
            return holidaylist;
        }
        public List<Object> EmployeeDropDownList(int id)
        {
            if(id == (int)EnumStaffType.Worker)
            {
                var List = new List<object>();
                _db.HRMS_Employee
                    .Where(x => x.Active && x.StaffType == id).Select(x => x).ToList()
                    .ForEach(x => List.Add(new
                    {
                        Value = x.ID,
                        Text = x.EmployeeIdentity
                    }));
                return List;
            }
            else
            {
                var List = new List<object>();
                _db.HRMS_Employee
                    .Where(x => x.Active).Select(x => x).ToList()
                    .ForEach(x => List.Add(new
                    {
                        Value = x.ID,
                        Text = x.EmployeeIdentity
                    }));
                return List;
            }

        }

        public List<Object> EmployeeDropDownListExceptThisId(int? id)
        {
            var List = new List<object>();
            _db.HRMS_Employee
                .Where(x => x.Active && x.ID != id).Select(x => x).ToList()
                .ForEach(x => List.Add(new
                {
                    Value = x.ID,
                    Text = x.EmployeeIdentity
                }));
            return List;
        }
        public IEnumerable LeaveStatusDropDownList()
        {
            var leaveStatusList = new List<object>();
            leaveStatusList.Add(new SelectListItem
            {
                Text = "-Select Leave Status-",
                Value = ""
            });
            foreach (var eVal in Enum.GetValues(typeof(EnumLeaveStatus)))
            {
                leaveStatusList.Add(new SelectListItem { Text = Enum.GetName(typeof(EnumLeaveStatus), eVal), Value = (Convert.ToInt32(eVal)).ToString() });
            }
            return leaveStatusList;
        }
        #endregion

        #region Employee
        private async Task<List<VMEmployeeSpouse>> EmployeeSpouseListGetAsync(VMEmployeeSpouse model)
        {

            List<VMEmployeeSpouse> vMs = new List<VMEmployeeSpouse>();

            vMs = await _db.HRMS_EmployeeDependent.Where(x => x.Active && x.IsSpouse && x.HRMS_EmployeeFK == model.VMEmployeeId)
                .Select(x => new VMEmployeeSpouse()
                {
                    ID = x.ID,
                    SpouseDOB = x.DOB,
                    VMEmployeeId = x.HRMS_EmployeeFK,
                    SpouseName = x.Name,
                    SpouseContactNo = x.ContactNo,
                    SpouseBloodGroup = (EnumBloodGroup)x.BloodGroup
                })
                .ToListAsync();

            return vMs;
        }
        private async Task<List<VMEmployeeChildren>> EmployeeChildrenListGetAsync(VMEmployeeChildren model)
        {

            List<VMEmployeeChildren> vMs = new List<VMEmployeeChildren>();

            vMs = await _db.HRMS_EmployeeDependent.Where(x => x.Active && !x.IsSpouse && x.HRMS_EmployeeFK == model.VMEmployeeId)
                .Select(x => new VMEmployeeChildren()
                {
                    ID = x.ID,
                    ChildDOB = x.DOB,
                    VMEmployeeId = x.HRMS_EmployeeFK,
                    ChildName = x.Name,
                    ChildBloodGroup = (EnumBloodGroup)x.BloodGroup
                })
                .ToListAsync();

            return vMs;
        }
        private async Task<List<VMEmployeeEducation>> EmployeeEducationListGetAsync(VMEmployeeEducation model)
        {

            List<VMEmployeeEducation> vMs = new List<VMEmployeeEducation>();

            vMs = await _db.HRMS_EmployeeEducation.Where(x => x.Active && x.HRMS_EmployeeFK == model.VMEmployeeId)
                .Select(x => new VMEmployeeEducation()
                {
                    ID = x.ID,
                    VMEmployeeId = x.HRMS_EmployeeFK,
                    VMDegreeName = x.HRMS_EducationalDegree.Name,
                    VMDegreeId = x.HRMS_EducationalDegreeFK,
                    GroupOrSubject = x.GroupOrSubject,
                    Institute = x.Institute,
                    PassingYear = x.PassingYear,
                    GradingType = x.GradingType,
                    Result = x.Result
                })
                .ToListAsync();

            return vMs;
        }
        private async Task<List<VMEmployeeTraining>> EmployeeTrainingListGetAsync(VMEmployeeTraining model)
        {

            List<VMEmployeeTraining> vMs = new List<VMEmployeeTraining>();

            vMs = await _db.HRMS_EmployeeTraining.Where(x => x.Active && x.HRMS_EmployeeFK == model.VMEmployeeId)
                .Select(x => new VMEmployeeTraining()
                {
                    ID = x.ID,
                    VMEmployeeId = x.HRMS_EmployeeFK,
                    CourseName = x.CourseName,
                    Institute = x.Institute,
                    VMCountryID = x.CountryID,
                    VMCountryName = x.CountryName,
                    CourseStartDate = x.CourseStartDate,
                    CourseEndDate = x.CourseEndDate
                })
                .ToListAsync();

            return vMs;
        }
        private async Task<List<VMEmployeeExperience>> EmployeeExperienceListGetAsync(VMEmployeeExperience model)
        {

            List<VMEmployeeExperience> vMs = new List<VMEmployeeExperience>();

            vMs = await _db.HRMS_EmployeeExperience.Where(x => x.Active && x.HRMS_EmployeeFK == model.VMEmployeeId)
                .Select(x => new VMEmployeeExperience()
                {
                    ID = x.ID,
                    VMEmployeeId = x.HRMS_EmployeeFK,
                    CompanyName = x.CompanyName,
                    Department = x.Department,
                    Designation = x.Designation,
                    Location = x.Location,
                    JobStartDate = x.JobStartDate,
                    JobEndDate = x.JobEndDate,
                    Responsibility = x.Responsibility,
                    Remarks = x.Comments
                })
                .ToListAsync();

            return vMs;
        }
        private async Task<List<VMEmployeeSkill>> EmployeeSkillListGetAsync(VMEmployeeSkill model)
        {

            List<VMEmployeeSkill> vMs = new List<VMEmployeeSkill>();

            vMs = await _db.HRMS_EmployeeSkill.Where(x => x.Active && x.HRMS_EmployeeFK == model.VMEmployeeId)
                .Select(x => new VMEmployeeSkill()
                {
                    ID = x.ID,
                    VMEmployeeId = x.HRMS_EmployeeFK,
                    SkillName = x.Name,
                    SkillType = x.Type,
                    SkillDescription = x.Description
                })
                .ToListAsync();

            return vMs;
        }
        private async Task<List<VMEmployeeReference>> EmployeeReferenceListGetAsync(VMEmployeeReference model)
        {

            List<VMEmployeeReference> vMs = new List<VMEmployeeReference>();

            vMs = await _db.HRMS_EmployeeReference.Where(x => x.Active && x.HRMS_EmployeeFK == model.VMEmployeeId)
                .Select(x => new VMEmployeeReference()
                {
                    ID = x.ID,
                    VMEmployeeId = x.HRMS_EmployeeFK,
                    RefereeName = x.RefereeName,
                    ContactNo = x.ContactNo,
                    EmailId = x.EmailId,
                    Relation = x.Relation,
                    Address = x.Address
                })
                .ToListAsync();

            return vMs;
        }
        private async Task<List<VMEmployeeNominee>> EmployeeNomineeListGetAsync(VMEmployeeNominee model)
        {

            List<VMEmployeeNominee> vMs = new List<VMEmployeeNominee>();

            vMs = await _db.HRMS_EmployeeNominee.Where(x => x.Active && x.HRMS_EmployeeFK == model.VMEmployeeId)
                .Select(x => new VMEmployeeNominee()
                {
                    ID = x.ID,
                    VMEmployeeId = x.HRMS_EmployeeFK,
                    NomineeName = x.Name,
                    FatherName = x.FatherName,
                    MotherName = x.MotherName,
                    Relation = x.Relation,
                    NID = x.NID,
                    Email = x.Email,
                    Address = x.Address
                })
                .ToListAsync();

            return vMs;
        }
        public async Task<VMEmployeeSpouse> EmployeeSpouseSpecificGet(VMEmployeeSpouse model)
        {



            VMEmployeeSpouse VM = new VMEmployeeSpouse();

            VM = await Task.Run(() => (from ES in _db.HRMS_EmployeeDependent.Where(x => x.Active && x.IsSpouse && x.HRMS_EmployeeFK == model.VMEmployeeId && x.ID == model.ID)
                                       select new VMEmployeeSpouse()
                                       {
                                           ID = ES.ID,
                                           VMEmployeeId = ES.HRMS_EmployeeFK,
                                           SpouseName = ES.Name,
                                           SpouseDOB = ES.DOB,
                                           SpouseContactNo = ES.ContactNo,
                                           SpouseBloodGroup = (EnumBloodGroup)ES.BloodGroup
                                       }).FirstOrDefaultAsync());


            return VM;
        }
        public async Task<VMEmployeeChildren> EmployeeChildrenSpecificGet(VMEmployeeChildren model)
        {



            VMEmployeeChildren VM = new VMEmployeeChildren();

            VM = await Task.Run(() => (from ES in _db.HRMS_EmployeeDependent.Where(x => x.Active && !x.IsSpouse && x.HRMS_EmployeeFK == model.VMEmployeeId && x.ID == model.ID)
                                       select new VMEmployeeChildren()
                                       {
                                           ID = ES.ID,
                                           VMEmployeeId = ES.HRMS_EmployeeFK,
                                           ChildName = ES.Name,
                                           ChildDOB = ES.DOB,
                                           ChildBloodGroup = (EnumBloodGroup)ES.BloodGroup
                                       }).FirstOrDefaultAsync());

            return VM;
        }
        public async Task<VMEmployeeEducation> EmployeeEducationSpecificGet(VMEmployeeEducation model)
        {



            VMEmployeeEducation VM = new VMEmployeeEducation();

            VM = await Task.Run(() => (from EE in _db.HRMS_EmployeeEducation.Where(x => x.Active && x.HRMS_EmployeeFK == model.VMEmployeeId && x.ID == model.ID)
                                       select new VMEmployeeEducation()
                                       {
                                           ID = EE.ID,
                                           VMEmployeeId = EE.HRMS_EmployeeFK,
                                           VMDegreeId = EE.HRMS_EducationalDegreeFK,
                                           VMDegreeName = EE.HRMS_EducationalDegree.Name,
                                           Institute = EE.Institute,
                                           GroupOrSubject = EE.GroupOrSubject,
                                           PassingYear = EE.PassingYear,
                                           GradingType = EE.GradingType,
                                           Result = EE.Result
                                       }).FirstOrDefaultAsync());

            return VM;
        }
        public async Task<VMEmployeeTraining> EmployeeTrainingSpecificGet(VMEmployeeTraining model)
        {



            VMEmployeeTraining VM = new VMEmployeeTraining();
            if (model.ID > 0)
            {
                VM = await Task.Run(() => (from ET in _db.HRMS_EmployeeTraining.Where(x => x.Active && x.HRMS_EmployeeFK == model.VMEmployeeId && x.ID == model.ID)
                                           select new VMEmployeeTraining()
                                           {
                                               ID = ET.ID,
                                               VMEmployeeId = ET.HRMS_EmployeeFK,
                                               CourseName = ET.CourseName,
                                               Institute = ET.Institute,
                                               VMCountryID = ET.CountryID,
                                               VMCountryName = ET.CountryName,
                                               CourseStartDate = ET.CourseStartDate,
                                               CourseEndDate = ET.CourseEndDate
                                           }).FirstOrDefaultAsync());
            }
            


            VM.VMCountryList = await CountryDropDownListAsync();

            return VM;
        }
        public async Task<VMEmployeeExperience> EmployeeExperienceSpecificGet(VMEmployeeExperience model)
        {



            VMEmployeeExperience VM = new VMEmployeeExperience();

            VM = await Task.Run(() => (from EX in _db.HRMS_EmployeeExperience.Where(x => x.Active && x.HRMS_EmployeeFK == model.VMEmployeeId && x.ID == model.ID)
                                       select new VMEmployeeExperience()
                                       {
                                           ID = EX.ID,
                                           VMEmployeeId = EX.HRMS_EmployeeFK,
                                           CompanyName = EX.CompanyName,
                                           Designation = EX.Designation,
                                           Department = EX.Department,
                                           Responsibility = EX.Responsibility,
                                           JobStartDate = EX.JobStartDate,
                                           JobEndDate = EX.JobEndDate,
                                           Location = EX.Location,
                                           Remarks = EX.Comments
                                       }).FirstOrDefaultAsync());

            return VM;
        }
        public async Task<VMEmployeeSkill> EmployeeSkillSpecificGet(VMEmployeeSkill model)
        {



            VMEmployeeSkill VM = new VMEmployeeSkill();

            VM = await Task.Run(() => (from ES in _db.HRMS_EmployeeSkill.Where(x => x.Active && x.HRMS_EmployeeFK == model.VMEmployeeId && x.ID == model.ID)
                                       select new VMEmployeeSkill()
                                       {
                                           ID = ES.ID,
                                           VMEmployeeId = ES.HRMS_EmployeeFK,
                                           SkillType = ES.Type,
                                           SkillName = ES.Name,
                                           SkillDescription = ES.Description
                                       }).FirstOrDefaultAsync());

            return VM;
        }
        public async Task<VMEmployeeReference> EmployeeReferenceSpecificGet(VMEmployeeReference model)
        {



            VMEmployeeReference VM = new VMEmployeeReference();

            VM = await Task.Run(() => (from ER in _db.HRMS_EmployeeReference.Where(x => x.Active && x.HRMS_EmployeeFK == model.VMEmployeeId && x.ID == model.ID)
                                       select new VMEmployeeReference()
                                       {
                                           ID = ER.ID,
                                           VMEmployeeId = ER.HRMS_EmployeeFK,
                                           RefereeName = ER.RefereeName,
                                           ContactNo = ER.ContactNo,
                                           EmailId = ER.EmailId,
                                           Address = ER.Address,
                                           Relation = ER.Relation,
                                           IsVarified = ER.IsVarified,
                                           VarifyingComments = ER.VarifyingComments
                                           
                                       }).FirstOrDefaultAsync());

            return VM;
        }
        public async Task<VMEmployeeNominee> EmployeeNomineeSpecificGet(VMEmployeeNominee model)
        {



            VMEmployeeNominee VM = new VMEmployeeNominee();

            VM = await Task.Run(() => (from ER in _db.HRMS_EmployeeNominee.Where(x => x.Active && x.HRMS_EmployeeFK == model.VMEmployeeId && x.ID == model.ID)
                                       select new VMEmployeeNominee()
                                       {
                                           ID = ER.ID,
                                           NomineeName = ER.Name,
                                           FatherName = ER.FatherName,
                                           MotherName = ER.MotherName,
                                           Relation = ER.Relation,
                                           NID = ER.NID,
                                           Address = ER.Address,
                                           Email = ER.Email
                                       }).FirstOrDefaultAsync()); ;

            return VM;
        }
        public async Task<VMEmployee> EmployeeSpecificGet(VMEmployee model)
        {
            VMEmployee VM = new VMEmployee();

            if (model.ID > 0)
            {
                VM = await Task.Run(() => (from E in _db.HRMS_Employee
                                           join po in _db.HRMS_PostOffice on E.HRMS_PostOfficeFK equals po.ID into po_Join
                                           from po in po_Join.DefaultIfEmpty()
                                           join ps in _db.HRMS_PoliceStation on po.HRMS_PoliceStationFK equals ps.ID into ps_Join
                                           from ps in ps_Join.DefaultIfEmpty()
                                           join dis in _db.HRMS_District on ps.HRMS_DistrictFK equals dis.ID into dis_Join
                                           from dis in dis_Join.DefaultIfEmpty()
                                           join C in _db.Common_Country on dis.Common_CountryFK equals C.ID into C_Join
                                           from C in C_Join.DefaultIfEmpty()
                                           join D in _db.HRMS_Designation on E.HRMS_DesignationFK equals D.ID into D_join
                                           from D in D_join.DefaultIfEmpty()
                                           join C_D in _db.User_Department on E.User_DepartmentFK equals C_D.ID into C_D_Join
                                           from C_D in C_D_Join.DefaultIfEmpty()

                                           join S in _db.HRMS_Section on E.HRMS_SectionFK equals S.ID into S_join
                                           from S in S_join.DefaultIfEmpty()
                                           join U in _db.HRMS_Unit on S.HRMS_UnitFK equals U.ID into U_Join
                                           from U in U_Join.DefaultIfEmpty()
                                           join BU in _db.HRMS_BusinessUnit on U.HRMS_BusinessUnitFK equals BU.ID into BU_join
                                           from BU in BU_join.DefaultIfEmpty()
                                           join L in _db.Common_ProductionLine on E.Common_ProductionLineFK equals L.ID into L_join
                                           from L in L_join.DefaultIfEmpty()

                                           join I in _db.HRMS_EmployeeImage on E.ID equals I.HRMS_EmployeeFK into I_join
                                           from I in I_join.DefaultIfEmpty()
                                           where E.Active && E.ID == model.ID
                                           select new VMEmployee()
                                           {
                                               ID = E.ID,
                                               Name = E.Name,
                                               NameBN = E.NameBN,
                                               FatherName = E.FatherName,
                                               MotherName = E.MotherName,
                                               DOB = E.DOB,
                                               BloodGroup = (EnumBloodGroup)E.BloodGroup,
                                               Gender = E.Gender,
                                               MaritalStatus = E.MaritalStatus,
                                               NIDNo = E.NIDNo,
                                               PassportNo = E.PassportNo,
                                               Religion = E.Religion,
                                               VMPostOfficeId = E.HRMS_PostOfficeFK != null ? E.HRMS_PostOfficeFK.Value : 0,
                                               VMPostOfficeName = po != null ? po.Name : "",
                                               VMPoliceStationId = po.HRMS_PoliceStationFK.HasValue ? po.HRMS_PoliceStationFK.Value : 0,
                                               VMPoliceStationName = ps != null ? ps.Name : "",
                                               VMDistrictId = ps.HRMS_DistrictFK.HasValue ? ps.HRMS_DistrictFK.Value : 0,
                                               VMDistrictName = dis != null ? dis.Name : "",
                                               VMCountryId = dis.Common_CountryFK.HasValue ? dis.Common_CountryFK.Value : 0,
                                               VMCountryName = C != null ? C.Name : "",
                                               MobileNo = E.MobileNo,
                                               AltMobileNo = E.AltMobileNo,
                                               EmailId = E.Email,
                                               PresentAddress = E.PresentAddress,
                                               PermanentAddress = E.PermanentAddress,
                                               AltPersonName = E.AltPersonName,
                                               AltPersonContactNo = E.AltPersonContactNo,
                                               AltPersonAddress = E.AltPersonAddress,
                                               EmployeeIdentity = E.EmployeeIdentity,
                                               CardNo = E.CardNo,
                                               VMDesignationId = E.HRMS_DesignationFK.HasValue ? E.HRMS_DesignationFK.Value : 0,
                                               VMDesignationName = D != null ? D.Name : "",
                                               VMBusinessUnitId = U != null && U != null ? U.HRMS_BusinessUnitFK.Value : 0,
                                               VMBusinessUnitName = BU != null ? BU.Name : "",
                                               VMDepartmentId = S != null ? E.User_DepartmentFK : 0,
                                               VMDepartmentName = C_D != null ? C_D.Name : "",
                                               VMUnitId = S != null ? S.HRMS_UnitFK : 0,
                                               VMUnitName = U != null ? U.Name : "",
                                               VMSectionId = E.HRMS_SectionFK.HasValue ? E.HRMS_SectionFK.Value : 0,
                                               VMSectionName = S != null ? S.Name : "",
                                               VMLineId = E.Common_ProductionLineFK.HasValue ? E.Common_ProductionLineFK.Value : 0,
                                               VMLineName = L != null ? L.Name : "",
                                               JoiningDate = E.JoiningDate,
                                               EmploymentType = (EnumEmploymentType)E.EmployementType,
                                               StaffType = (EnumStaffType)E.StaffType,
                                               OvertimeEligibility = E.OvertimeEligibility,
                                               Grade = E.Grade,
                                               Image = I.Image,
                                               VMLineRoleId = E != null ? E.LineRoleId : 0,
                                               VMReportingEmployeeId = E != null ? E.ReportingDesignationId : 0
                                           }).FirstOrDefaultAsync());
            }


            if (VM.ID > 0)
            {

                VM.VMEmployeeSpouseList = await EmployeeSpouseListGetAsync(new VMEmployeeSpouse() { VMEmployeeId = model.ID });
                VM.VMEmployeeChildrenList = await EmployeeChildrenListGetAsync(new VMEmployeeChildren() { VMEmployeeId = model.ID });
                VM.VMEmployeeEducationList = await EmployeeEducationListGetAsync(new VMEmployeeEducation() { VMEmployeeId = model.ID });
                VM.VMEmployeeTrainingList = await EmployeeTrainingListGetAsync(new VMEmployeeTraining() { VMEmployeeId = model.ID });
                VM.VMEmployeeExperienceList = await EmployeeExperienceListGetAsync(new VMEmployeeExperience() { VMEmployeeId = model.ID });
                VM.VMEmployeeSkillList = await EmployeeSkillListGetAsync(new VMEmployeeSkill() { VMEmployeeId = model.ID });
                VM.VMEmployeeReferenceList = await EmployeeReferenceListGetAsync(new VMEmployeeReference() { VMEmployeeId = model.ID });
                VM.VMEmployeNomineeList = await EmployeeNomineeListGetAsync(new VMEmployeeNominee() { VMEmployeeId = model.ID });
            }


            return VM;
        }
        public async Task<List<VMEmployee>> EmployeeListGet(VMEmployee model)
        {
            List<VMEmployee> VM = new List<VMEmployee>();
            VM = await Task.Run(() => (from E in _db.HRMS_Employee
                                       join D in _db.HRMS_Designation on E.HRMS_DesignationFK equals D.ID into D_join
                                       from D in D_join.DefaultIfEmpty()


                                       join S in _db.HRMS_Section on E.HRMS_SectionFK equals S.ID into S_join
                                       from S in S_join.DefaultIfEmpty()
                                       join C_D in _db.User_Department on E.User_DepartmentFK equals C_D.ID into C_D_Join
                                       from C_D in C_D_Join.DefaultIfEmpty()
                                       join U in _db.HRMS_Unit on S.HRMS_UnitFK equals U.ID into U_Join
                                       from U in U_Join.DefaultIfEmpty()
                                       join BU in _db.HRMS_BusinessUnit on U.HRMS_BusinessUnitFK equals BU.ID into BU_join
                                       from BU in BU_join.DefaultIfEmpty()
                                       join L in _db.Common_ProductionLine on E.Common_ProductionLineFK equals L.ID into L_join
                                       from L in L_join.DefaultIfEmpty()

                                       where E.Active && (E.ID == model.ID || model.ID == 0)
                                       select new VMEmployee()
                                       {
                                           ID = E.ID,
                                           Name = E.Name,
                                           PresentStatus = (EnumEmployeeStatus)E.PresentStatus,
                                           EmployeeIdentity = E.EmployeeIdentity,
                                           VMDesignationId = E.HRMS_DesignationFK.HasValue ? E.HRMS_DesignationFK.Value : 0,
                                           VMDesignationName = D != null ? D.Name : "",
                                           VMBusinessUnitId = S != null && C_D != null && U != null ? U.HRMS_BusinessUnitFK : 0,
                                           VMBusinessUnitName = BU != null ? BU.Name : "",
                                           VMDepartmentId = S != null ? E.User_DepartmentFK : 0,
                                           VMDepartmentName = C_D != null ? C_D.Name : "",
                                           VMUnitId = S != null && C_D != null ? S.HRMS_UnitFK : 0,
                                           VMUnitName = U != null ? U.Name : "",
                                           VMSectionId = E.HRMS_SectionFK.HasValue ? E.HRMS_SectionFK.Value : 0,
                                           VMSectionName = S != null ? S.Name : "",
                                           VMLineId = E.Common_ProductionLineFK.HasValue ? E.Common_ProductionLineFK.Value : 0,
                                           VMLineName = L != null ? L.Name : "",
                                           VMReportingEmployeeId = E != null ? E.ReportingDesignationId: 0,
                                           VMLineRoleId = E != null ? E.LineRoleId : 0
                                       }).ToListAsync());


            return VM;
        }
        public async Task<int> EmployeeAdd(VMEmployee model)
        {
            var result = -1;

            HRMS_Employee en = new HRMS_Employee
            {

                Name = model.Name,
                NameBN = model.NameBN,
                FatherName = model.FatherName,
                MotherName = model.MotherName,
                DOB = model.DOB,
                BloodGroup = (int)model.BloodGroup,
                Gender = model.Gender,
                MaritalStatus = model.MaritalStatus,
                NIDNo = model.NIDNo,
                PassportNo = model.PassportNo,
                Religion = model.Religion,
                HRMS_PostOfficeFK = model.VMPostOfficeId,
                MobileNo = model.MobileNo,
                AltMobileNo = model.AltMobileNo,
                Email = model.EmailId,
                PresentAddress = model.PresentAddress,
                PermanentAddress = model.PermanentAddress,
                AltPersonName = model.AltPersonName,
                AltPersonContactNo = model.AltPersonContactNo,
                AltPersonAddress = model.AltPersonAddress,
                EmployeeIdentity = model.EmployeeIdentity,
                CardNo = model.CardNo,
                HRMS_DesignationFK = model.VMDesignationId,
                HRMS_SectionFK = model.VMSectionId,
                Common_ProductionLineFK = model.VMLineId,
                JoiningDate = model.JoiningDate,
                EmployementType = (int)model.EmploymentType,
                StaffType = (int)model.StaffType,
                OvertimeEligibility = model.OvertimeEligibility,
                Grade = model.Grade,
                PresentStatus = (int)model.PresentStatus,
                User = model.User,
                UserID = model.UserID,
                ReportingDesignationId = model.VMReportingEmployeeId,
                LineRoleId = model.VMLineRoleId
            };
            _db.HRMS_Employee.Add(en);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = en.ID;
            }
            return result;
        }
        public async Task<int> EmployeeEdit(VMEmployee model)
        {
            var result = -1;
            HRMS_Employee en = await _db.HRMS_Employee.FindAsync(model.ID);

            en.Name = model.Name;
            en.NameBN = model.NameBN;
            en.FatherName = model.FatherName;
            en.MotherName = model.MotherName;
            en.DOB = model.DOB;
            en.BloodGroup = (int)model.BloodGroup;
            en.Gender = model.Gender;
            en.MaritalStatus = model.MaritalStatus;
            en.NIDNo = model.NIDNo;
            en.PassportNo = model.PassportNo;
            en.Religion = model.Religion;
            en.HRMS_PostOfficeFK = model.VMPostOfficeId;
            en.MobileNo = model.MobileNo;
            en.AltMobileNo = model.AltMobileNo;
            en.Email = model.EmailId;
            en.PresentAddress = model.PresentAddress;
            en.PermanentAddress = model.PermanentAddress;
            en.AltPersonName = model.AltPersonName;
            en.AltPersonContactNo = model.AltPersonContactNo;
            en.AltPersonAddress = model.AltPersonAddress;
            en.EmployeeIdentity = model.EmployeeIdentity;
            en.CardNo = model.CardNo;
            en.HRMS_DesignationFK = model.VMDesignationId;
            en.HRMS_SectionFK = model.VMSectionId;
            en.Common_ProductionLineFK = model.VMLineId;
            en.JoiningDate = model.JoiningDate;
            en.EmployementType = (int)model.EmploymentType;
            en.StaffType = (int)model.StaffType;
            en.OvertimeEligibility = model.OvertimeEligibility;
            en.Grade = model.Grade;
            en.LineRoleId = model.VMLineRoleId;
            en.ReportingDesignationId = model.VMReportingEmployeeId;

            if (await _db.SaveChangesAsync() > 0)
            {
                result = en.ID;
            }
            return result;
        }
        public async Task<int> EmployeeBasicAdd(VMEmployee model)
        {
            var result = -1;
            HRMS_Employee en = new HRMS_Employee
            {
                Name = model.Name,
                NameBN = model.NameBN,
                FatherName = model.FatherName,
                MotherName = model.MotherName,
                DOB = model.DOB,
                BloodGroup = (int)model.BloodGroup,
                Gender = model.Gender,
                MaritalStatus = model.MaritalStatus,
                NIDNo = model.NIDNo,
                PassportNo = model.PassportNo,
                Religion = model.Religion,
                HRMS_PostOfficeFK = model.VMPostOfficeId,
                User = model.User,
                UserID = model.UserID,
            };
            _db.HRMS_Employee.Add(en);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = en.ID;
            }
            return result;
        }
        public async Task<int> EmployeeBasicEdit(VMEmployee model)
        {
            var result = -1;
            HRMS_Employee en = await _db.HRMS_Employee.FindAsync(model.ID);

            en.Name = model.Name;
            en.NameBN = model.NameBN;
            en.FatherName = model.FatherName;
            en.MotherName = model.MotherName;
            en.DOB = model.DOB;
            en.BloodGroup = (int)model.BloodGroup;
            en.Gender = model.Gender;
            en.MaritalStatus = model.MaritalStatus;
            en.NIDNo = model.NIDNo;
            en.PassportNo = model.PassportNo;
            en.Religion = model.Religion;
            en.HRMS_PostOfficeFK = model.VMPostOfficeId;


            if (await _db.SaveChangesAsync() > 0)
            {
                result = en.ID;
            }
            return result;
        }
        public async Task<int> EmployeeContactAdd(VMEmployee model)
        {
            var result = -1;
            HRMS_Employee en = new HRMS_Employee
            {
                MobileNo = model.MobileNo,
                AltMobileNo = model.AltMobileNo,
                Email = model.EmailId,
                PresentAddress = model.PresentAddress,
                PermanentAddress = model.PermanentAddress,
                AltPersonName = model.AltPersonName,
                AltPersonContactNo = model.AltPersonContactNo,
                AltPersonAddress = model.AltPersonAddress,
                User = model.User,
                UserID = model.UserID,
                
            };
            _db.HRMS_Employee.Add(en);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = en.ID;
            }
            return result;
        }
        public async Task<int> EmployeeContactEdit(VMEmployee model)
        {
            var result = -1;
            HRMS_Employee en = await _db.HRMS_Employee.FindAsync(model.ID);

            
            en.MobileNo = model.MobileNo;
            en.AltMobileNo = model.AltMobileNo;
            en.Email = model.EmailId;
            en.PresentAddress = model.PresentAddress;
            en.PermanentAddress = model.PermanentAddress;
            en.AltPersonName = model.AltPersonName;
            en.AltPersonContactNo = model.AltPersonContactNo;
            en.AltPersonAddress = model.AltPersonAddress;
            


            if (await _db.SaveChangesAsync() > 0)
            {
                result = en.ID;
            }
            return result;
        }
        public async Task<int> EmployeeOfficeAdd(VMEmployee model)
        {
            var result = -1;

            var EmpCount = _db.HRMS_Employee.Count();


            HRMS_Employee en = new HRMS_Employee
            {

                EmployeeIdentity = model.EmployeeIdentity,
                CardNo = model.CardNo,
                HRMS_DesignationFK = model.VMDesignationId,
                HRMS_SectionFK = model.VMSectionId,
                Common_ProductionLineFK = model.VMLineId,
                JoiningDate = model.JoiningDate,
                EmployementType = (int)model.EmploymentType,
                StaffType = (int)model.StaffType,
                OvertimeEligibility = model.OvertimeEligibility,
                Grade = model.Grade,
                User = model.User,
                UserID = model.UserID,
                LineRoleId = model.VMLineRoleId,
                ReportingDesignationId = model.VMReportingEmployeeId,
                User_DepartmentFK = model.VMDepartmentId
            };

            _db.HRMS_Employee.Add(en);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = en.ID;
            }
            return result;
        }
        public async Task<int> EmployeeOfficeEdit(VMEmployee model)
        {
            var result = -1;

            var EmpCount = _db.HRMS_Employee.Count(e => e.ID >= 0 && e.ID <= model.ID);
            string SCCode = _db.HRMS_BusinessUnit.Where(bu => bu.ID == model.VMBusinessUnitId).Select(u => u.Code).FirstOrDefault();
            string BUCode = _db.HRMS_Unit.Where(u => u.ID == model.VMUnitId).Select(u => u.Code).FirstOrDefault();
            string SCode = _db.HRMS_Section.Where(s => s.ID == model.VMSectionId).Select(u => u.Code).FirstOrDefault();
            string DCode = _db.User_Department.Where(d => d.ID == model.VMDepartmentId).Select(u => u.Code).FirstOrDefault();

            string EmpID = string.Format("{0:000000}", EmpCount);

            var eType = "";

            HRMS_Employee en = await _db.HRMS_Employee.FindAsync(model.ID);
            en.CardNo = model.CardNo;
            en.HRMS_DesignationFK = model.VMDesignationId;
            en.HRMS_SectionFK = model.VMSectionId;
            en.Common_ProductionLineFK = model.VMLineId;
            en.JoiningDate = model.JoiningDate;
            en.EmployementType = (int)model.EmploymentType;
            en.StaffType = (int)model.StaffType;
            en.OvertimeEligibility = model.OvertimeEligibility;
            en.Grade = model.Grade;
            en.LineRoleId = model.VMLineRoleId;
            en.ReportingDesignationId = model.VMReportingEmployeeId;

            if (en.StaffType == 1)
            {
                eType = "W";
            }
            else
            {
                eType = "B";
            }
            
            if(en.EmployeeIdentity != null || en.EmployeeIdentity != "")
            {
                string IDstrng = Convert.ToString(DCode + "/" + SCCode + "/" + BUCode + "/" + SCode + "-" + eType + EmpID);
                en.EmployeeIdentity = IDstrng;
            }
            else
            {
                string IDstrng = Convert.ToString(DCode + "/" + SCCode + "/" + BUCode + "/" + SCode + "-" + eType + EmpID);
                en.EmployeeIdentity = IDstrng;
            }

            if (await _db.SaveChangesAsync() > 0)
            {
                result = en.ID;
            }
            return result;
        }
        public async Task<int> EmployeeStatusUpdate(VMEmployee model)
        {
            var result = -1;
            HRMS_Employee en = await _db.HRMS_Employee.FindAsync(model.ID);
            if (model.ActionId > 3)
            {
                if (model.EmployeeAction == EnumEmployeeAction.Resign)
                {
                    en.PresentStatus = (int)EnumEmployeeStatus.Resigned;
                    en.QuitDate = model.QuitDate;
                }
                if (model.EmployeeAction == EnumEmployeeAction.UndoResign)
                {
                    en.PresentStatus = (int)EnumEmployeeStatus.Present;
                    en.QuitDate = (DateTime?)null;
                }

                if (model.EmployeeAction == EnumEmployeeAction.Terminate)
                {
                    en.PresentStatus = (int)EnumEmployeeStatus.Terminated;
                    en.QuitDate = model.QuitDate;
                }
                if (model.EmployeeAction == EnumEmployeeAction.UndoTerminate)
                {
                    en.PresentStatus = (int)EnumEmployeeStatus.Present;
                    en.QuitDate = (DateTime?)null;
                }


            }


            if (await _db.SaveChangesAsync() > 0)
            {
                result = en.ID;
            }
            return result;
        }
        public async Task<int> EmployeeDelete(int id)
        {
            var result = -1;
            HRMS_Employee selectSingle = await _db.HRMS_Employee.FindAsync(id);
            if (selectSingle != null)
            {
                selectSingle.Active = false;
                if (await _db.SaveChangesAsync() > 0)
                {
                    
                    result = selectSingle.ID;
                }
            }
            return result;
        }

        public async Task<int> SpouseAdd(VMEmployeeSpouse model)
        {
            var result = -1;
            HRMS_EmployeeDependent en = new HRMS_EmployeeDependent
            {
                HRMS_EmployeeFK = model.VMEmployeeId,
                Name = model.SpouseName,
                DOB = model.SpouseDOB,
                BloodGroup = (int)model.SpouseBloodGroup,
                ContactNo = model.SpouseContactNo,
                IsSpouse = true,
                User = model.User,
                UserID = model.UserID,

            };
            _db.HRMS_EmployeeDependent.Add(en);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = en.ID;
            }
            return result;
        }
        public async Task<int> SpouseEdit(VMEmployeeSpouse model)
        {
            var result = -1;
            HRMS_EmployeeDependent en = await _db.HRMS_EmployeeDependent.FirstOrDefaultAsync(x => x.ID == model.ID && x.IsSpouse);

            en.Name = model.SpouseName;
            en.DOB = model.SpouseDOB;
            en.ContactNo = model.SpouseContactNo;
            en.BloodGroup = (int)model.SpouseBloodGroup;

            if (await _db.SaveChangesAsync() > 0)
            {
                result = en.ID;
            }
            return result;
        }
        public async Task<int> SpouseDelete(int id)
        {
            var result = -1;
            HRMS_EmployeeDependent selectSingle = await _db.HRMS_EmployeeDependent.FirstOrDefaultAsync(x => x.ID == id && x.IsSpouse);
            if (selectSingle != null)
            {
                selectSingle.Active = false;
                if (await _db.SaveChangesAsync() > 0)
                {

                    result = selectSingle.ID;
                }
            }
            return result;
        }

        public async Task<int> NomineeAdd(VMEmployeeNominee model)
        {
            var result = -1;
            HRMS_EmployeeNominee en = new HRMS_EmployeeNominee
            {
                HRMS_EmployeeFK = model.VMEmployeeId,
                Name = model.NomineeName,
                FatherName = model.FatherName,
                MotherName = model.MotherName,
                Relation = model.Relation,
                NID = model.NID,
                Address = model.Address,
                Email = model.Email,
                User = model.User,
                UserID = model.UserID,

            };
            _db.HRMS_EmployeeNominee.Add(en);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = en.ID;
            }
            return result;
        }
        public async Task<int> NomineeEdit(VMEmployeeNominee model)
        {
            var result = -1;
            HRMS_EmployeeNominee en = await _db.HRMS_EmployeeNominee.FirstOrDefaultAsync(x => x.ID == model.ID);

            en.Name = model.NomineeName;
            //en.HRMS_EmployeeFK = model.VMEmployeeId;
            en.FatherName = model.FatherName;
            en.MotherName = model.MotherName;
            en.Relation = model.Relation;
            en.NID = model.NID;
            en.Address = model.Address;
            en.Email = model.Email;
            //en.User = model.User;
            //en.UserID = model.UserID;

            _db.HRMS_EmployeeNominee.Update(en);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = en.ID;
            }
            return result;
        }
        public async Task<int> NomineeDelete(int id)
        {
            var result = -1;
            HRMS_EmployeeNominee selectSingle = await _db.HRMS_EmployeeNominee.FirstOrDefaultAsync(x => x.ID == id);
            if (selectSingle != null)
            {
                selectSingle.Active = false;
                _db.HRMS_EmployeeNominee.Update(selectSingle);
                if (await _db.SaveChangesAsync() > 0)
                {

                    result = selectSingle.ID;
                }
            }
            return result;
        }

        public async Task<int> ChildrenAdd(VMEmployeeChildren model)
        {
            var result = -1;
            HRMS_EmployeeDependent en = new HRMS_EmployeeDependent
            {
                HRMS_EmployeeFK = model.VMEmployeeId,
                Name = model.ChildName,
                DOB = model.ChildDOB,
                BloodGroup = (int)model.ChildBloodGroup,
                ContactNo = "",
                IsSpouse = false,
                User = model.User,
                UserID = model.UserID,

            };
            _db.HRMS_EmployeeDependent.Add(en);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = en.ID;
            }
            return result;
        }
        public async Task<int> ChildrenEdit(VMEmployeeChildren model)
        {
            var result = -1;
            HRMS_EmployeeDependent en = await _db.HRMS_EmployeeDependent.FirstOrDefaultAsync(x => x.ID == model.ID && !x.IsSpouse);

            en.Name = model.ChildName;
            en.DOB = model.ChildDOB;
            
            en.BloodGroup = (int)model.ChildBloodGroup;

            if (await _db.SaveChangesAsync() > 0)
            {
                result = en.ID;
            }
            return result;
        }
        public async Task<int> ChildrenDelete(int id)
        {
            var result = -1;
            HRMS_EmployeeDependent selectSingle = await _db.HRMS_EmployeeDependent.FirstOrDefaultAsync(x => x.ID == id && !x.IsSpouse);
            if (selectSingle != null)
            {
                selectSingle.Active = false;
                if (await _db.SaveChangesAsync() > 0)
                {

                    result = selectSingle.ID;
                }
            }
            return result;
        }

        public async Task<int> EducationAdd(VMEmployeeEducation model)
        {
            var result = -1;
            HRMS_EmployeeEducation en = new HRMS_EmployeeEducation
            {
                HRMS_EmployeeFK = model.VMEmployeeId,
                HRMS_EducationalDegreeFK = model.VMDegreeId,
                GroupOrSubject = model.GroupOrSubject,

                PassingYear = model.PassingYear,
                Institute = model.Institute,
                GradingType = model.GradingType,
                Result = model.Result,
                User = model.User,
                UserID = model.UserID,

            };
            _db.HRMS_EmployeeEducation.Add(en);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = en.ID;
            }
            return result;
        }
        public async Task<int> EducationEdit(VMEmployeeEducation model)
        {
            var result = -1;
            HRMS_EmployeeEducation en = await _db.HRMS_EmployeeEducation.FindAsync(model.ID);

            en.HRMS_EducationalDegreeFK = model.VMDegreeId;
            en.GroupOrSubject = model.GroupOrSubject;

            en.PassingYear = model.PassingYear;
            en.Institute = model.Institute;
            en.GradingType = model.GradingType;
            en.Result = model.Result;

            if (await _db.SaveChangesAsync() > 0)
            {
                result = en.ID;
            }
            return result;
        }
        public async Task<int> EducationDelete(int id)
        {
            var result = -1;
            HRMS_EmployeeEducation selectSingle = await _db.HRMS_EmployeeEducation.FindAsync(id);
            if (selectSingle != null)
            {
                selectSingle.Active = false;
                if (await _db.SaveChangesAsync() > 0)
                {

                    result = selectSingle.ID;
                }
            }
            return result;
        }

        public async Task<int> TrainingAdd(VMEmployeeTraining model)
        {
            var result = -1;
            HRMS_EmployeeTraining en = new HRMS_EmployeeTraining
            {
                HRMS_EmployeeFK = model.VMEmployeeId,
                CourseName = model.CourseName,
                Institute = model.Institute,
                CountryID = model.VMCountryID,
                CountryName = model.VMCountryName,
                CourseStartDate = model.CourseStartDate,
                CourseEndDate = model.CourseEndDate,
                CourseDuration = model.CourseDuration,
                User = model.User,
                UserID = model.UserID,
            };
            _db.HRMS_EmployeeTraining.Add(en);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = en.ID;
            }
            return result;
        }
        public async Task<int> TrainingEdit(VMEmployeeTraining model)
        {
            var result = -1;
            HRMS_EmployeeTraining en = await _db.HRMS_EmployeeTraining.FindAsync(model.ID);

            en.CourseName = model.CourseName;
            en.Institute = model.Institute;

            
            en.CountryID = model.VMCountryID;
            en.CountryName = model.VMCountryName;
            en.CourseStartDate = model.CourseStartDate;
            en.CourseEndDate = model.CourseEndDate;
            en.CourseDuration = model.CourseDuration;

            if (await _db.SaveChangesAsync() > 0)
            {
                result = en.ID;
            }
            return result;
        }
        public async Task<int> TrainingDelete(int id)
        {
            var result = -1;
            HRMS_EmployeeTraining selectSingle = await _db.HRMS_EmployeeTraining.FindAsync(id);
            if (selectSingle != null)
            {
                selectSingle.Active = false;
                if (await _db.SaveChangesAsync() > 0)
                {

                    result = selectSingle.ID;
                }
            }
            return result;
        }

        public async Task<int> ExperienceAdd(VMEmployeeExperience model)
        {
            var result = -1;
            HRMS_EmployeeExperience en = new HRMS_EmployeeExperience
            {
                HRMS_EmployeeFK = model.VMEmployeeId,
                CompanyName = model.CompanyName,
                Department = model.Department,
                Designation = model.Designation,
                Location = model.Location,
                JobStartDate = model.JobStartDate,
                JobEndDate = model.JobEndDate,
                Duration = model.Duration,
                Responsibility = model.Responsibility,
                Comments = model.Remarks,
                UserID = model.UserID,
                User = model.User,
            };
            _db.HRMS_EmployeeExperience.Add(en);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = en.ID;
            }
            return result;
        }
        public async Task<int> ExperienceEdit(VMEmployeeExperience model)
        {
            var result = -1;
            HRMS_EmployeeExperience en = await _db.HRMS_EmployeeExperience.FindAsync(model.ID);

            en.CompanyName = model.CompanyName;
            en.Department = model.Department;
            en.Designation = model.Designation;
            en.Location = model.Location;
            en.JobStartDate = model.JobStartDate;
            en.JobEndDate = model.JobEndDate;
            en.Duration = model.Duration;
            en.Responsibility = model.Responsibility;
            en.Remarks = model.Remarks;


            if (await _db.SaveChangesAsync() > 0)
            {
                result = en.ID;
            }
            return result;
        }
        public async Task<int> ExperienceDelete(int id)
        {
            var result = -1;
            HRMS_EmployeeExperience selectSingle = await _db.HRMS_EmployeeExperience.FindAsync(id);
            if (selectSingle != null)
            {
                selectSingle.Active = false;
                if (await _db.SaveChangesAsync() > 0)
                {

                    result = selectSingle.ID;
                }
            }
            return result;
        }

        public async Task<int> SkillAdd(VMEmployeeSkill model)
        {
            var result = -1;
            HRMS_EmployeeSkill en = new HRMS_EmployeeSkill
            {
                HRMS_EmployeeFK = model.VMEmployeeId,
                Name = model.SkillName,
                Type = model.SkillType,
                Description = model.SkillDescription,
                User = model.User,
                UserID = model.UserID,
            };
            _db.HRMS_EmployeeSkill.Add(en);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = en.ID;
            }
            return result;
        }
        public async Task<int> SkillEdit(VMEmployeeSkill model)
        {
            var result = -1;
            HRMS_EmployeeSkill en = await _db.HRMS_EmployeeSkill.FindAsync(model.ID);

            en.Name = model.SkillName;
            en.Type = model.SkillType;
            en.Description = model.SkillDescription;
            

            if (await _db.SaveChangesAsync() > 0)
            {
                result = en.ID;
            }
            return result;
        }
        public async Task<int> SkillDelete(int id)
        {
            var result = -1;
            HRMS_EmployeeSkill selectSingle = await _db.HRMS_EmployeeSkill.FindAsync(id);
            if (selectSingle != null)
            {
                selectSingle.Active = false;
                if (await _db.SaveChangesAsync() > 0)
                {

                    result = selectSingle.ID;
                }
            }
            return result;
        }

        public async Task<int> ReferenceAdd(VMEmployeeReference model)
        {
            var result = -1;
            HRMS_EmployeeReference en = new HRMS_EmployeeReference
            {
                HRMS_EmployeeFK = model.VMEmployeeId,
                RefereeName = model.RefereeName,
                ContactNo = model.ContactNo,
                EmailId = model.EmailId,
                Relation = model.Relation,
                Address = model.Address,
                IsVarified = model.IsVarified,
                VarifyingComments = model.VarifyingComments,
                User = model.User,
                UserID = model.UserID,
            };
            _db.HRMS_EmployeeReference.Add(en);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = en.ID;
            }
            return result;
        }
        public async Task<int> ReferenceEdit(VMEmployeeReference model)
        {
            var result = -1;
            HRMS_EmployeeReference en = await _db.HRMS_EmployeeReference.FindAsync(model.ID);

            en.RefereeName = model.RefereeName;
            en.ContactNo = model.ContactNo;
            en.EmailId = model.EmailId;
            en.Relation = model.Relation;
            en.Address = model.Address;
            en.IsVarified = model.IsVarified;
            en.VarifyingComments = model.VarifyingComments;

            _db.HRMS_EmployeeReference.Update(en);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = en.ID;
            }
            return result;
        }
        public async Task<int> ReferenceDelete(int id)
        {
            var result = -1;
            HRMS_EmployeeReference selectSingle = await _db.HRMS_EmployeeReference.FindAsync(id);
            if (selectSingle != null)
            {
                selectSingle.Active = false;
                _db.HRMS_EmployeeReference.Update(selectSingle);
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = selectSingle.ID;
                }
            }
            return result;
        }

        public async Task<int> ImageAdd(string imagePath, int id)
        {
            var result = -1;
            HRMS_Employee en = await _db.HRMS_Employee.FindAsync(id);
            en.ImagePath = imagePath;
            _db.HRMS_Employee.Update(en);
            await _db.SaveChangesAsync();

            if (await _db.SaveChangesAsync() > 0)
            {
                result = en.ID;
            }

            return result;
        }

        public async Task<int> EmployeeImage( byte[] a , int id)
        {
            var result = -1;
            var emp = _db.HRMS_EmployeeImage.Where(ei => ei.HRMS_EmployeeFK == id).FirstOrDefault();
            if(emp != null)
            {
                emp.Image = a;
                _db.HRMS_EmployeeImage.Update(emp);
                await _db.SaveChangesAsync();
            }
            else
            {
                HRMS_EmployeeImage HEI = new HRMS_EmployeeImage();
                HEI.HRMS_EmployeeFK = id;
                HEI.Image = a;
                _db.HRMS_EmployeeImage.Add(HEI);
                await _db.SaveChangesAsync();
            }
            return result;
        }
        #endregion

        #region Shift
        public async Task<VMShiftAssign> ShiftAssignSpecificGet(VMShiftAssign model)
        {
            VMShiftAssign VM = new VMShiftAssign();

            VM = await Task.Run(() => (from SA in _db.HRMS_ShiftAssign
                                       join SH in _db.HRMS_Shift on SA.HRMS_ShiftFK equals SH.ID into SH_Join
                                       from SH in SH_Join.DefaultIfEmpty()
                                       join E in _db.HRMS_Employee on SA.HRMS_EmployeeFK equals E.ID into E_Join
                                       from E in E_Join.DefaultIfEmpty()
                                       join D in _db.HRMS_Designation on E.HRMS_DesignationFK equals D.ID into D_join
                                       from D in D_join.DefaultIfEmpty()

                                       join S in _db.HRMS_Section on E.HRMS_SectionFK equals S.ID into S_join
                                       from S in S_join.DefaultIfEmpty()
                                       join DP in _db.User_Department on E.User_DepartmentFK equals DP.ID into DP_join
                                       from DP in DP_join.DefaultIfEmpty()
                                       join U in _db.HRMS_Unit on S.HRMS_UnitFK equals U.ID into U_Join
                                       from U in U_Join.DefaultIfEmpty()
                                       join BU in _db.HRMS_BusinessUnit on U.HRMS_BusinessUnitFK equals BU.ID into BU_join
                                       from BU in BU_join.DefaultIfEmpty()

                                       where SA.Active
                                       && (model.VMBusinessUnitID > 0 || model.VMDepartmentID > 0 || model.VMUnitID > 0 || model.VMSectionID > 0 || model.VMShiftID > 0 || model.ID > 0)
                                       && (model.ID == SA.ID || model.ID == 0)
                                       && (BU.ID == model.VMBusinessUnitID || model.VMBusinessUnitID == 0)
                                       && (DP.ID == model.VMDepartmentID || model.VMDepartmentID == 0)
                                       && (U.ID == model.VMUnitID || model.VMUnitID == 0)
                                       && (S.ID == model.VMSectionID || model.VMSectionID == 0)
                                       && (SH.ID == model.VMShiftID || model.VMShiftID == 0)
                                       select new VMShiftAssign()
                                       {
                                           ID = SA.ID,
                                           VMShiftID = SH != null ? SH.ID : 0,
                                           VMShiftName = SH != null ? SH.ShiftName : "",
                                           VMEmployeeID = E != null ? E.ID : 0,
                                           AssignedDate = SA.AssignedDate,

                                           VMEmployeeName = E != null ? E.Name : "",

                                           VMEmployeeIdentity = E.EmployeeIdentity,
                                           VMDesignationName = D != null ? D.Name : "",
                                           VMBusinessUnitID = S != null && DP != null && U != null && U.HRMS_BusinessUnitFK.HasValue ? U.HRMS_BusinessUnitFK.Value : 0,
                                           VMBusinessUnitName = BU != null ? BU.Name : "",
                                           VMDepartmentID = S != null && E.User_DepartmentFK.HasValue ? E.User_DepartmentFK.Value : 0,
                                           VMDepartmentName = DP != null ? DP.Name : "",
                                           VMUnitID = S != null && DP != null && S.HRMS_UnitFK.HasValue ? S.HRMS_UnitFK.Value : 0,
                                           VMUnitName = U != null ? U.Name : "",
                                           VMSectionID = E.HRMS_SectionFK.HasValue ? E.HRMS_SectionFK.Value : 0,
                                           VMSectionName = S != null ? S.Name : "",


                                       }).FirstOrDefaultAsync());

            return VM;
        }
        public async Task<List<VMShiftAssign>> ShiftAssignListGet(VMShiftAssign model)
        {
            List<VMShiftAssign> VM = new List<VMShiftAssign>();
            if (model.IsForListView)
            {
                if(model.VMBusinessUnitID > 0)
                {
                    VM = await Task.Run(() => (from SA in _db.HRMS_ShiftAssign.OrderBy(x => x.AssignedDate)
                                               join SH in _db.HRMS_Shift on SA.HRMS_ShiftFK equals SH.ID into SH_Join
                                               from SH in SH_Join.DefaultIfEmpty()
                                               join E in _db.HRMS_Employee on SA.HRMS_EmployeeFK equals E.ID into E_Join
                                               from E in E_Join.DefaultIfEmpty()
                                               join D in _db.HRMS_Designation on E.HRMS_DesignationFK equals D.ID into D_join
                                               from D in D_join.DefaultIfEmpty()
                                               join S in _db.HRMS_Section on E.HRMS_SectionFK equals S.ID into S_join
                                               from S in S_join.DefaultIfEmpty()
                                               join DP in _db.User_Department on E.User_DepartmentFK equals DP.ID into DP_join
                                               from DP in DP_join.DefaultIfEmpty()
                                               join U in _db.HRMS_Unit on S.HRMS_UnitFK equals U.ID into U_Join
                                               from U in U_Join.DefaultIfEmpty()
                                               join BU in _db.HRMS_BusinessUnit on U.HRMS_BusinessUnitFK equals BU.ID into BU_join
                                               from BU in BU_join.DefaultIfEmpty()
                                               where SA.Active
                                               && (model.VMBusinessUnitID > 0 || model.VMDepartmentID > 0 || model.VMUnitID > 0 || model.VMSectionID > 0 || model.VMFilterByShiftID > 0 || (model.FilterByStartDate != null && model.FilterByStartDate != DateTime.MinValue && model.FilterByEndDate != null && model.FilterByEndDate != DateTime.MinValue))
                                               && (model.VMBusinessUnitID == 0 || BU.ID == model.VMBusinessUnitID)
                                               && (model.VMDepartmentID == 0 || DP.ID == model.VMDepartmentID)
                                               && (model.VMUnitID == 0 || U.ID == model.VMUnitID)
                                               && (model.VMSectionID == 0 || S.ID == model.VMSectionID)
                                               && (model.VMFilterByShiftID == 0 || SH.ID == model.VMFilterByShiftID)
                                               && ((model.FilterByStartDate == null || model.FilterByStartDate == DateTime.MinValue || model.FilterByEndDate == null || model.FilterByEndDate == DateTime.MinValue) || (SA.AssignedDate >= model.FilterByStartDate && SA.AssignedDate <= model.FilterByEndDate))

                                               group new
                                               {
                                                   SA,
                                                   SH,
                                                   E,
                                                   D,
                                                   DP,
                                                   BU,
                                                   U,
                                                   S
                                               }
                                               by new
                                               {
                                                   SA.HRMS_ShiftFK,
                                                   SA.HRMS_EmployeeFK,
                                                   SA.UID,
                                                   SH_Name = SH != null ? SH.ShiftName : "",
                                                   E_Name = E != null ? E.Name : "",
                                                   E_EmployeeIdentity = E != null ? E.EmployeeIdentity : "",
                                                   D_Name = D != null ? D.Name : "",
                                                   DP_Name = DP != null ? DP.Name : "",
                                                   BU_Name = BU != null ? BU.Name : "",
                                                   U_Name = U != null ? U.Name : "",
                                                   S_Name = S != null ? S.Name : ""
                                               } into g
                                               select new VMShiftAssign()
                                               {
                                                   //ID = SA.ID,
                                                   //VMShiftID = g.Key.SH != null ? g.Key.SH.ID : 0,
                                                   VMShiftName = g.Key.SH_Name,
                                                   //VMEmployeeID = g.Key.E != null ? g.Key.E.ID : 0,

                                                   VMEmployeeName = g.Key.E_Name,

                                                   StartDate = g.Min(x => x.SA.AssignedDate),
                                                   EndDate = g.Max(x => x.SA.AssignedDate),

                                                   VMEmployeeIdentity = g.Key.E_EmployeeIdentity,
                                                   VMDesignationName = g.Key.D_Name,
                                                   //VMBusinessUnitID = g.Key.E.HRMS_BusinessUnitFK.HasValue ? g.Key.E.HRMS_BusinessUnitFK.Value : 0,
                                                   VMBusinessUnitName = g.Key.BU_Name,
                                                   //VMDepartmentID = g.Key.E.User_DepartmentFK.HasValue ? g.Key.E.User_DepartmentFK.Value : 0,
                                                   VMDepartmentName = g.Key.DP_Name,
                                                   //VMUnitID = g.Key.E.HRMS_UnitFK.HasValue ? g.Key.E.HRMS_UnitFK.Value : 0,
                                                   VMUnitName = g.Key.U_Name,
                                                   //VMSectionID = g.Key.E.HRMS_SectionFK.HasValue ? g.Key.E.HRMS_SectionFK.Value : 0,
                                                   VMSectionName = g.Key.S_Name,


                                               }).ToListAsync());
                }
                else
                {
                    VM = await Task.Run(() => (from SA in _db.HRMS_ShiftAssign.OrderBy(x => x.AssignedDate)
                                               join SH in _db.HRMS_Shift on SA.HRMS_ShiftFK equals SH.ID into SH_Join
                                               from SH in SH_Join.DefaultIfEmpty()
                                               join E in _db.HRMS_Employee on SA.HRMS_EmployeeFK equals E.ID into E_Join
                                               from E in E_Join.DefaultIfEmpty()
                                               join D in _db.HRMS_Designation on E.HRMS_DesignationFK equals D.ID into D_join
                                               from D in D_join.DefaultIfEmpty()
                                               join S in _db.HRMS_Section on E.HRMS_SectionFK equals S.ID into S_join
                                               from S in S_join.DefaultIfEmpty()
                                               join DP in _db.User_Department on E.User_DepartmentFK equals DP.ID into DP_join
                                               from DP in DP_join.DefaultIfEmpty()
                                               join U in _db.HRMS_Unit on S.HRMS_UnitFK equals U.ID into U_Join
                                               from U in U_Join.DefaultIfEmpty()
                                               join BU in _db.HRMS_BusinessUnit on U.HRMS_BusinessUnitFK equals BU.ID into BU_join
                                               from BU in BU_join.DefaultIfEmpty()
                                               where SA.Active
                                               //&& (model.VMBusinessUnitID > 0 || model.VMDepartmentID > 0 || model.VMUnitID > 0 || model.VMSectionID > 0 || model.VMFilterByShiftID > 0 || (model.FilterByStartDate != null && model.FilterByStartDate != DateTime.MinValue && model.FilterByEndDate != null && model.FilterByEndDate != DateTime.MinValue))
                                               //&& (model.VMBusinessUnitID == 0 || BU.ID == model.VMBusinessUnitID)
                                               //&& (model.VMDepartmentID == 0 || DP.ID == model.VMDepartmentID)
                                               //&& (model.VMUnitID == 0 || U.ID == model.VMUnitID)
                                               //&& (model.VMSectionID == 0 || S.ID == model.VMSectionID)
                                               //&& (model.VMFilterByShiftID == 0 || SH.ID == model.VMFilterByShiftID)
                                               //&& ((model.FilterByStartDate == null || model.FilterByStartDate == DateTime.MinValue || model.FilterByEndDate == null || model.FilterByEndDate == DateTime.MinValue) || (SA.AssignedDate.Date >= model.FilterByStartDate.Value.Date && SA.AssignedDate.Date <= model.FilterByEndDate.Value.Date))

                                               group new
                                               {
                                                   SA,
                                                   SH,
                                                   E,
                                                   D,
                                                   DP,
                                                   BU,
                                                   U,
                                                   S
                                               }
                                               by new
                                               {
                                                   SA.HRMS_ShiftFK,
                                                   SA.HRMS_EmployeeFK,
                                                   SA.UID,
                                                   SH_Name = SH != null ? SH.ShiftName : "",
                                                   E_Name = E != null ? E.Name : "",
                                                   E_EmployeeIdentity = E != null ? E.EmployeeIdentity : "",
                                                   D_Name = D != null ? D.Name : "",
                                                   DP_Name = DP != null ? DP.Name : "",
                                                   BU_Name = BU != null ? BU.Name : "",
                                                   U_Name = U != null ? U.Name : "",
                                                   S_Name = S != null ? S.Name : ""
                                               } into g
                                               select new VMShiftAssign()
                                               {
                                                   Min = g.Min(x=>x.SA.ID),
                                                   Count = g.Count(),
                                                   //ID = SA.ID,
                                                   //VMShiftID = g.Key.SH != null ? g.Key.SH.ID : 0,
                                                   VMShiftName = g.Key.SH_Name,
                                                   //VMEmployeeID = g.Key.E != null ? g.Key.E.ID : 0,

                                                   VMEmployeeName = g.Key.E_Name,

                                                   StartDate = g.Min(x => x.SA.AssignedDate),
                                                   EndDate = g.Max(x => x.SA.AssignedDate),

                                                   VMEmployeeIdentity = g.Key.E_EmployeeIdentity,
                                                   VMDesignationName = g.Key.D_Name,
                                                   //VMBusinessUnitID = g.Key.E.HRMS_BusinessUnitFK.HasValue ? g.Key.E.HRMS_BusinessUnitFK.Value : 0,
                                                   VMBusinessUnitName = g.Key.BU_Name,
                                                   //VMDepartmentID = g.Key.E.User_DepartmentFK.HasValue ? g.Key.E.User_DepartmentFK.Value : 0,
                                                   VMDepartmentName = g.Key.DP_Name,
                                                   //VMUnitID = g.Key.E.HRMS_UnitFK.HasValue ? g.Key.E.HRMS_UnitFK.Value : 0,
                                                   VMUnitName = g.Key.U_Name,
                                                   //VMSectionID = g.Key.E.HRMS_SectionFK.HasValue ? g.Key.E.HRMS_SectionFK.Value : 0,
                                                   VMSectionName = g.Key.S_Name,


                                               }).ToListAsync());
                }
            }
            else
            {
                VM = await Task.Run(() => (from E in _db.HRMS_Employee
                                           join D in _db.HRMS_Designation on E.HRMS_DesignationFK equals D.ID into D_join
                                           from D in D_join.DefaultIfEmpty()
                                           join S in _db.HRMS_Section on E.HRMS_SectionFK equals S.ID into S_join
                                           from S in S_join.DefaultIfEmpty()
                                           join DP in _db.User_Department on E.User_DepartmentFK equals DP.ID into DP_join
                                           from DP in DP_join.DefaultIfEmpty()
                                           join U in _db.HRMS_Unit on S.HRMS_UnitFK equals U.ID into U_Join
                                           from U in U_Join.DefaultIfEmpty()
                                           join BU in _db.HRMS_BusinessUnit on U.HRMS_BusinessUnitFK equals BU.ID into BU_join
                                           from BU in BU_join.DefaultIfEmpty()

                                           where E.Active
                                           && (model.VMBusinessUnitID > 0 || model.VMDepartmentID > 0 || model.VMUnitID > 0 || model.VMSectionID > 0)
                                           && (BU.ID == model.VMBusinessUnitID || model.VMBusinessUnitID == 0)
                                           && (DP.ID == model.VMDepartmentID || model.VMDepartmentID == 0)
                                           && (U.ID == model.VMUnitID || model.VMUnitID == 0)
                                           && (S.ID == model.VMSectionID || model.VMSectionID == 0)
                                           select new VMShiftAssign()
                                           {
                                               VMEmployeeID = E != null ? E.ID : 0,

                                               VMEmployeeName = E != null ? E.Name : "",

                                               VMEmployeeIdentity = E.EmployeeIdentity,
                                               VMDesignationName = D != null ? D.Name : "",
                                               VMBusinessUnitID = S != null && DP != null && U != null && U.HRMS_BusinessUnitFK.HasValue ? U.HRMS_BusinessUnitFK.Value : 0,
                                               VMBusinessUnitName = BU != null ? BU.Name : "",
                                               VMDepartmentID = S != null && E.User_DepartmentFK.HasValue ? E.User_DepartmentFK.Value : 0,
                                               VMDepartmentName = DP != null ? DP.Name : "",
                                               VMUnitID = S != null && DP != null && S.HRMS_UnitFK.HasValue ? S.HRMS_UnitFK.Value : 0,
                                               VMUnitName = U != null ? U.Name : "",
                                               VMSectionID = E.HRMS_SectionFK.HasValue ? E.HRMS_SectionFK.Value : 0,
                                               VMSectionName = S != null ? S.Name : "",


                                           }).ToListAsync());

            }



            return VM;
        }

        public async Task<int> DeleteShiftAssigned(int id, int min)
        {
            var sa = await _db.HRMS_ShiftAssign.Where(s=> s.ID == id).Select(s=>s.ID).FirstOrDefaultAsync();
            for(var i = 0; i<min; i++)
            {
                HRMS_ShiftAssign en = await _db.HRMS_ShiftAssign.FindAsync(sa);
                en.Active = false;
                _db.HRMS_ShiftAssign.Update(en);
                await _db.SaveChangesAsync();
                sa++;
            }
            return 1;
        }
        public async Task<int> ShiftAssignListAdd(List<VMShiftAssign> model)
        {
            var result = -1;
            List<HRMS_ShiftAssign> en = new List<HRMS_ShiftAssign>();
            model.ForEach(x =>
            {
                DateTime assignedDate = x.StartDate;
                Guid guid = Guid.NewGuid();
                for (int i = 0; i <= (x.EndDate - x.StartDate).Days; i++)
                {
                    en.Add(new HRMS_ShiftAssign() { HRMS_ShiftFK = x.VMShiftID, HRMS_EmployeeFK = x.VMEmployeeID, AssignedDate = assignedDate, UID = guid, User = x.User, UserID = x.UserID, });
                    assignedDate = assignedDate.AddDays(1);
                }
            });
            List<HRMS_ShiftAssign> enAdd = new List<HRMS_ShiftAssign>();
            enAdd = en.Where(x => !_db.HRMS_ShiftAssign.Any(y => y.HRMS_EmployeeFK == x.HRMS_EmployeeFK && x.AssignedDate == y.AssignedDate)).ToList();
            await _db.HRMS_ShiftAssign
                .Where(x => en.Any(y => y.HRMS_EmployeeFK == x.HRMS_EmployeeFK && x.AssignedDate == y.AssignedDate))
                .ForEachAsync(x =>
                {
                    x.HRMS_ShiftFK = en.First(y => y.HRMS_EmployeeFK == x.HRMS_EmployeeFK && y.AssignedDate == x.AssignedDate).HRMS_ShiftFK;
                    x.UID = en.First(y => y.HRMS_EmployeeFK == x.HRMS_EmployeeFK && y.AssignedDate == x.AssignedDate).UID;
                });
            //List<HRMS_ShiftAssign> en = model.Select(x => new HRMS_ShiftAssign
            //{
            //    HRMS_ShiftFK = x.VMShiftID,
            //    HRMS_EmployeeFK = x.VMEmployeeID,
            //    AssignedDate = x.AssignedDate,
            //}).ToList();
            await _db.HRMS_ShiftAssign.AddRangeAsync(enAdd);
            
            if (await _db.SaveChangesAsync() > 0)
            {
                result = 1;
            }
            return result;
        }
        public async Task<int> ShiftAssignEdit(VMShiftAssign model)
        {
            var result = -1;
            HRMS_ShiftAssign en = await _db.HRMS_ShiftAssign.FindAsync(model.ID);

            en.HRMS_ShiftFK = model.VMShiftID;
            en.HRMS_EmployeeFK = model.VMEmployeeID;
            en.AssignedDate = model.AssignedDate;
            


            if (await _db.SaveChangesAsync() > 0)
            {
                result = en.ID;
            }
            return result;
        }
        public async Task<int> ShiftAssignDelete(int id)
        {
            var result = -1;
            HRMS_ShiftAssign selectSingle = await _db.HRMS_ShiftAssign.FindAsync(id);
            if (selectSingle != null)
            {
                selectSingle.Active = false;
                if (await _db.SaveChangesAsync() > 0)
                {

                    result = selectSingle.ID;
                }
            }
            return result;
        }

        public async Task<VMShift> AddShift(int? id, string Delete, VMShift model)
        {
            DateTime dateTimeA;
            DateTime dateTimeB;
            DateTime.TryParse(model.StartTime, out dateTimeA);
            DateTime.TryParse(model.EndTime, out dateTimeB);
            if (id != null && Delete == null)
            {
                var HS = _db.HRMS_Shift.Find(id);
                model.ID = HS.ID;
                model.ShiftName = HS.ShiftName;
                model.StartTime = HS.StartTime.ToString();
                model.EndTime = HS.EndTime.ToString();
                model.BreakTime = HS.BreakTime;
                model.Remarks = HS.ShiftRemarks;
            }
            else if(model != null && id == null && Delete == null)
            {
                if(model.ID != 0)
                {
                    HRMS_Shift HS = _db.HRMS_Shift.Find(model.ID);
                    HS.ShiftName = model.ShiftName;
                    HS.StartTime = dateTimeA.TimeOfDay;
                    HS.EndTime = dateTimeB.TimeOfDay;
                    HS.BreakTime = model.BreakTime;
                    HS.ShiftRemarks = model.Remarks;

                    _db.HRMS_Shift.Update(HS);
                    await _db.SaveChangesAsync();
                }
                else
                {
                    HRMS_Shift HS = new HRMS_Shift();
                    HS.ShiftName = model.ShiftName;
                    HS.StartTime = dateTimeA.TimeOfDay;
                    HS.EndTime = dateTimeB.TimeOfDay;
                    HS.BreakTime = model.BreakTime;
                    HS.ShiftRemarks = model.Remarks;

                    _db.HRMS_Shift.Add(HS);
                    await _db.SaveChangesAsync();
                }

            }
            else if (Delete == "Delete" & id != null)
            {
                var SA = _db.HRMS_ShiftAssign.Where(s => s.HRMS_ShiftFK == id).Any();
                if (SA != false)
                {
                    model.error = "Can't delete! Already in use.";
                }
                else
                {
                    HRMS_Shift hs = _db.HRMS_Shift.Find(id);
                    hs.Active = false;
                    _db.HRMS_Shift.Update(hs);
                    await _db.SaveChangesAsync();
                }
            }

            return model;
        }
        #endregion

        #region GeneralSettings
        public async Task<HRMSBusinessUnitVM> AddEditOrDeleteBusinessUnit(int? id, string Delete, HRMSBusinessUnitVM BUVM)
        {
            if (id != null && Delete == null)
            {
                var bu = _db.HRMS_BusinessUnit.Find(id);
                BUVM.ID = bu.ID;
                BUVM.Name = bu.Name;
                BUVM.BusinessUnitRemarks = bu.Description;
                BUVM.Code = bu.Code;
            }
            else if (BUVM != null && id == null && Delete == null)
            {
                if (BUVM.ID != 0)
                {
                    HRMS_BusinessUnit bu = _db.HRMS_BusinessUnit.Find(BUVM.ID);
                    bu.Name = BUVM.Name;
                    bu.Description = BUVM.BusinessUnitRemarks;
                    bu.Code = BUVM.Code;

                    _db.HRMS_BusinessUnit.Update(bu);
                    await _db.SaveChangesAsync();
                }
                else
                {
                    HRMS_BusinessUnit bu = new HRMS_BusinessUnit();
                    bu.Name = BUVM.Name;
                    bu.Description = BUVM.BusinessUnitRemarks;
                    bu.Code = BUVM.Code;

                    _db.HRMS_BusinessUnit.Add(bu);
                    await _db.SaveChangesAsync();
                }

            }
            else if (Delete == "Delete" & id != null)
            {
                HRMS_BusinessUnit bu = _db.HRMS_BusinessUnit.Find(id);
                var emp = (from e in _db.HRMS_Employee
                           join s in _db.HRMS_Section on e.HRMS_SectionFK equals s.ID into s_Join
                           from s in s_Join.DefaultIfEmpty()
                           join dp in _db.User_Department on e.User_DepartmentFK equals dp.ID into dp_Join
                           from dp in dp_Join.DefaultIfEmpty()
                           join u in _db.HRMS_Unit on s.HRMS_UnitFK equals u.ID into u_Join
                           from u in u_Join.DefaultIfEmpty()
                           join b in _db.HRMS_BusinessUnit on u.HRMS_BusinessUnitFK equals bu.ID into bu_Join
                           from b in bu_Join.DefaultIfEmpty()
                           where b.ID == id select e.ID).Any();

                if (emp != false)
                {
                    BUVM.error = "Can't Delete! Already in use!";
                    return BUVM;
                }
                else
                {
                    bu.Active = false;
                    _db.HRMS_BusinessUnit.Update(bu);
                    await _db.SaveChangesAsync();
                }
            }
            return BUVM;
        }

        public async Task<HRMSHierarchyVM> AddEditOrDeleteCompanyHierarchyLevel(int? id, string Delete, HRMSHierarchyVM HHVM)
        {
            if (id != null && Delete == null)
            {
                var hl = _db.HRMS_CompanyHierarchyLabel.Find(id);
                HHVM.ID = hl.ID;
                HHVM.Name = hl.Name;
                HHVM.HierarchyRemarks = hl.Description;
            }
            else if (HHVM != null && id == null && Delete == null)
            {
                if (HHVM.ID != 0)
                {
                    HRMS_CompanyHierarchyLabel hl = _db.HRMS_CompanyHierarchyLabel.Find(HHVM.ID);
                    hl.Name = HHVM.Name;
                    hl.Description = HHVM.HierarchyRemarks;

                    _db.HRMS_CompanyHierarchyLabel.Update(hl);
                    await _db.SaveChangesAsync();
                }
                else
                {
                    HRMS_CompanyHierarchyLabel hl = new HRMS_CompanyHierarchyLabel();
                    hl.Name = HHVM.Name;
                    hl.Description = HHVM.HierarchyRemarks;

                    _db.HRMS_CompanyHierarchyLabel.Add(hl);
                    await _db.SaveChangesAsync();
                }

            }
            else if (Delete == "Delete" & id != null)
            {
                var deptId = _db.HRMS_Designation.Where(e => e.HRMS_CompanyHierarchyLabelFK == id).Any();
                if (deptId != false)
                {
                    HHVM.error = "Cant Delete! Already use.";
                    return HHVM;
                }
                else
                {
                    HRMS_CompanyHierarchyLabel hl = _db.HRMS_CompanyHierarchyLabel.Find(id);
                    hl.Active = false;
                    await _db.SaveChangesAsync();
                }
            }

            return HHVM;
        }


        public async Task<List<HRMSDesignationVM>> HRMSDesignationListGet()
        {
            List<HRMSDesignationVM> vM = new List<HRMSDesignationVM>();

            vM = await Task.Run(() => (from P in _db.HRMS_Designation
                                       join D in _db.User_Department on P.User_DepartmentFK equals D.ID into D_Join
                                       from D in D_Join.DefaultIfEmpty()
                                       join C in _db.HRMS_CompanyHierarchyLabel on P.HRMS_CompanyHierarchyLabelFK equals C.ID into C_Join
                                       from C in C_Join.DefaultIfEmpty()
                                       where P.Active
                                       select new HRMSDesignationVM()
                                       {
                                           ID = P.ID,
                                           Name = P.Name,
                                           DesignationRemarks = P.Description,
                                           LevelId = P.HRMS_CompanyHierarchyLabelFK,
                                           AttendanceBonus = P.AttendanceBonus,
                                           DepartmentId = P.User_DepartmentFK,
                                           DepartmentName = D.Name,
                                           LevelName = C.Name
                                       }).ToListAsync());
            return vM;
        }

        public async Task<HRMSDesignationVM> AddEditOrDeleteDesignation(int? id, string Delete, HRMSDesignationVM DVM)
        {
            if (id != null && Delete == null)
            {
                var en = await Task.Run(() => (from P in _db.HRMS_Designation
                                               join D in _db.User_Department on P.User_DepartmentFK equals D.ID into D_Join
                                               from D in D_Join.DefaultIfEmpty()
                                               join C in _db.HRMS_CompanyHierarchyLabel on P.HRMS_CompanyHierarchyLabelFK equals C.ID into C_Join
                                               from C in C_Join.DefaultIfEmpty()
                                               where P.ID == id.Value && P.Active
                                               select new HRMSDesignationVM()
                                               {
                                                   ID = P.ID,
                                                   Name = P.Name,
                                                   DesignationRemarks = P.Description,
                                                   LevelId = P.HRMS_CompanyHierarchyLabelFK,
                                                   AttendanceBonus = P.AttendanceBonus,
                                                   NightBill = P.NightBill,
                                                   DepartmentId = P.User_DepartmentFK,
                                                   DepartmentName = D.Name,
                                                   LevelName = C.Name,
                                                   Remarks = P.Remarks
                                               }).FirstOrDefaultAsync());
                DVM.ID = en.ID;
                DVM.Name = en.Name;
                DVM.DesignationRemarks = en.DesignationRemarks;
                DVM.LevelId = en.LevelId;
                DVM.DesignationId = en.ID;
                DVM.AttendanceBonus = en.AttendanceBonus;
                DVM.DepartmentId = en.DepartmentId;
                DVM.NightBill = en.NightBill;
                DVM.DesignationId = en.ID;
                DVM.Remarks = en.Remarks;
            }
            else if (DVM != null && id == null && Delete == null)
            {
                if (DVM.ID != 0)
                {
                    HRMS_Designation hd = _db.HRMS_Designation.Find(DVM.ID);
                    hd.Name = DVM.Name;
                    hd.Description = DVM.DesignationRemarks;
                    hd.HRMS_CompanyHierarchyLabelFK = DVM.LevelId;
                    hd.AttendanceBonus = DVM.AttendanceBonus;
                    hd.User_DepartmentFK = DVM.DepartmentId;
                    hd.NightBill = DVM.NightBill;
                    hd.Remarks = DVM.Remarks;
                    _db.HRMS_Designation.Update(hd);
                    await _db.SaveChangesAsync();
                }
                else
                {
                    if(DVM.DesignationId != null)
                    {
                        HRMS_Designation hdn = _db.HRMS_Designation.Find(DVM.DesignationId);
                        hdn.Description = DVM.DesignationRemarks;
                        hdn.AttendanceBonus = DVM.AttendanceBonus;
                        hdn.NightBill = DVM.NightBill;
                        hdn.IsAttendanceBonusGiven = true;
                        _db.HRMS_Designation.Update(hdn);
                        await _db.SaveChangesAsync();
                    }
                    else
                    {
                        HRMS_Designation hd = new HRMS_Designation();
                        hd.Name = DVM.Name;
                        hd.Description = DVM.DesignationRemarks;
                        hd.HRMS_CompanyHierarchyLabelFK = DVM.LevelId;
                        hd.AttendanceBonus = DVM.AttendanceBonus;
                        hd.User_DepartmentFK = DVM.DepartmentId;
                        hd.IsAttendanceBonusGiven = false;
                        hd.Remarks = DVM.Remarks;
                        _db.HRMS_Designation.Add(hd);
                        await _db.SaveChangesAsync();
                    }
                }

            }
            else if (Delete == "Delete" & id != null)
            {
                var deptId = _db.HRMS_Employee.Where(e => e.HRMS_DesignationFK == id).Any();
                if (deptId != false)
                {
                    DVM.error = "Can't Delete! Already use.";
                }
                else
                {
                    HRMS_Designation hd = _db.HRMS_Designation.Find(id);
                    hd.Active = false;
                    _db.HRMS_Designation.Update(hd);
                    await _db.SaveChangesAsync();
                }
            }
            return DVM;
        }

        public async Task<List<HRMSUnitVM>> HRMSUnitListGet()
        {

            List<HRMSUnitVM> vM = new List<HRMSUnitVM>();

            vM = await Task.Run(() => (from U in _db.HRMS_Unit
                                       join BU in _db.HRMS_BusinessUnit on U.HRMS_BusinessUnitFK equals BU.ID into BU_Join
                                       from BU in BU_Join.DefaultIfEmpty()
                                       where U.Active && BU.Active
                                       select new HRMSUnitVM()
                                       {
                                           ID = U.ID,
                                           Name = U.Name,
                                           Code = U.Code,
                                           UnitRemarks = U.Description,
                                           VMBusinessUnitID = U.HRMS_BusinessUnitFK,
                                           VMBusinessUnitName = BU != null ? BU.Name : "",
                                       }).ToListAsync());
            return vM;
        }
        


        public async Task<List<HRMSDesignationVM>> HRMSDesignationGet()
        {

            List<HRMSDesignationVM> vM = new List<HRMSDesignationVM>();

            vM = await Task.Run(() => (from D in _db.HRMS_Designation
                                       where D.Active && D.AttendanceBonus != 0
                                       select new HRMSDesignationVM()
                                       {
                                           ID = D.ID,
                                           Name = D.Name + '(' + D.User_Department.Name + ')',
                                           AttendanceBonus = D.AttendanceBonus,
                                           NightBill = D.NightBill,
                                           Remarks = D.Remarks
                                       }).ToListAsync());
            return vM;
        }

        public async Task<List<HRMSJobDescriptionVM>> HRMSJobDescriptionGet()
        {

            List<HRMSJobDescriptionVM> vM = new List<HRMSJobDescriptionVM>();

            vM = await Task.Run(() => (from JD in _db.HRMS_JobDescription
                                       join Dp in _db.User_Department on JD.User_DepartmentFK equals Dp.ID into Dp_Join
                                       from Dp in Dp_Join.DefaultIfEmpty()
                                       join D in _db.HRMS_Designation on JD.HRMS_DesignationFK equals D.ID into D_Join
                                       from D in D_Join.DefaultIfEmpty()
                                       where JD.Active && D.Active
                                       select new HRMSJobDescriptionVM()
                                       {
                                           ID = JD.ID,
                                           JobDescription = JD.JobDescription,
                                           VMDepartmentName = Dp.Name,
                                           VMDesignationName = D.Name,
                                           VMDepartmentID = JD.User_DepartmentFK,
                                           VMDesignationID = JD.HRMS_DesignationFK,
                                       }).ToListAsync());
            return vM;
        }

        private bool CheckCompanyBank(int Id)
        {
            var vData = _db.Common_CompanyBank.Any(a => a.Common_CompanySetupFK == Id && a.Active == true);
            return vData;

        }

        public async Task<List<CommonCompanyVM>> CommonCompanyGet()
        {

            List<CommonCompanyVM> vM = new List<CommonCompanyVM>();

            vM = await Task.Run(() => (from cc in _db.Common_CompanySetup
                                       where cc.Active
                                       select new CommonCompanyVM()
                                       {
                                           ID = cc.ID,
                                           Name = cc.Name,
                                           Phone = cc.Phone,
                                           Email = cc.Email,
                                           ContactPerson = cc.ContactPerson,
                                           Address = cc.Address,
                                          
                                       }).ToListAsync());

            vM.ForEach(x => {
                x.Exist = CheckCompanyBank(x.ID) == true ? "label-success" : "label-danger";
            });
            return vM;
        }

        public async Task<HRMSUnitVM> AddEditOrDeleteUnit(int? id, string Delete, HRMSUnitVM HUVM)
        {
            if (id != null && Delete == null)
            {
                var hu = _db.HRMS_Unit.Include(x => x.HRMS_BusinessUnit).FirstOrDefault(x => x.ID == id);
                if (hu != null)
                {
                    HUVM.ID = hu.ID;
                    HUVM.Name = hu.Name;
                    HUVM.UnitRemarks = hu.Description;
                    HUVM.VMBusinessUnitID = hu.HRMS_BusinessUnitFK;
                    HUVM.VMBusinessUnitName = hu.HRMS_BusinessUnit.Name;
                    HUVM.Code = hu.Code;
                }

            }
            else if (HUVM != null && id == null && Delete == null)
            {
                if (HUVM.ID != 0)
                {
                    HRMS_Unit hu = _db.HRMS_Unit.Find(HUVM.ID);
                    hu.Name = HUVM.Name;
                    hu.Description = HUVM.UnitRemarks;
                    hu.HRMS_BusinessUnitFK = HUVM.VMBusinessUnitID;
                    hu.Code = HUVM.Code;
                    _db.HRMS_Unit.Update(hu);
                    await _db.SaveChangesAsync();
                }
                else
                {
                    HRMS_Unit hu = new HRMS_Unit();
                    hu.Name = HUVM.Name;
                    hu.Description = HUVM.UnitRemarks;
                    hu.HRMS_BusinessUnitFK = HUVM.VMBusinessUnitID;
                    hu.Code = HUVM.Code;
                    _db.HRMS_Unit.Add(hu);
                    await _db.SaveChangesAsync();
                }

            }
            else if (Delete == "Delete" & id != null)
            {

                HRMS_Unit hu = _db.HRMS_Unit.Find(id);
                var emp = (from e in _db.HRMS_Employee
                           join s in _db.HRMS_Section on e.HRMS_SectionFK equals s.ID into s_Join
                           from s in s_Join.DefaultIfEmpty()
                           join dp in _db.User_Department on e.User_DepartmentFK equals dp.ID into dp_Join
                           from dp in dp_Join.DefaultIfEmpty()
                           join u in _db.HRMS_Unit on s.HRMS_UnitFK equals u.ID into u_Join
                           from u in u_Join.DefaultIfEmpty()
                           join b in _db.HRMS_BusinessUnit on u.HRMS_BusinessUnitFK equals b.ID into bu_Join
                           from b in bu_Join.DefaultIfEmpty()
                           where u.ID == id
                           select e.ID).Any();

                if (emp != false)
                {
                    HUVM.error = "Can't Delete! Already in use!";
                    return HUVM;
                }
                else
                {
                    hu.Active = false;
                    _db.HRMS_Unit.Update(hu);
                    await _db.SaveChangesAsync();
                }
            }

            return HUVM;
        }

        public async Task<HRMSJobDescriptionVM> AddEditOrDeleteJobDescription(int? id, string Delete, HRMSJobDescriptionVM HJDVM)
        {
            if (id != null && Delete == null)
            {
                var jd = _db.HRMS_JobDescription.Include(x => x.User_Department).Include(x=>x.HRMS_Designation).FirstOrDefault(x => x.ID == id);
                if (jd != null)
                {
                    HJDVM.ID = jd.ID;
                    HJDVM.JobDescription = jd.JobDescription;
                    HJDVM.VMDepartmentID = jd.User_DepartmentFK;
                    HJDVM.VMDesignationID = jd.HRMS_DesignationFK;
                }

            }
            else if (HJDVM != null && id == null && Delete == null)
            {
                if (HJDVM.ID != 0)
                {
                    HRMS_JobDescription jd = _db.HRMS_JobDescription.Find(HJDVM.ID);
                    jd.JobDescription = HJDVM.JobDescription;
                    jd.HRMS_DesignationFK = HJDVM.VMDesignationID;
                    jd.User_DepartmentFK = HJDVM.VMDepartmentID;
                    _db.HRMS_JobDescription.Update(jd);
                    await _db.SaveChangesAsync();
                }
                else
                {
                    HRMS_JobDescription jd = new HRMS_JobDescription();
                    jd.JobDescription = HJDVM.JobDescription;
                    jd.HRMS_DesignationFK = HJDVM.VMDesignationID;
                    jd.User_DepartmentFK = HJDVM.VMDepartmentID;
                    _db.HRMS_JobDescription.Add(jd);
                    await _db.SaveChangesAsync();
                }

            }
            else if (Delete == "Delete" & id != null)
            {

                HRMS_JobDescription jd = _db.HRMS_JobDescription.Find(id);
                jd.Active = false;
                _db.HRMS_JobDescription.Update(jd);
                await _db.SaveChangesAsync();
            }

            return HJDVM;
        }

        public async Task<CommonCompanyVM> AddEditOrDeleteCompany(byte[] a, int? id, string Delete, CommonCompanyVM CCVM)
        {
            if (id != null && Delete == null)
            {
                var cs = _db.Common_CompanySetup.Find(id);
                if (cs != null)
                {
                    CCVM.ID = cs.ID;
                    CCVM.Name = cs.Name;
                    CCVM.Email = cs.Email;
                    CCVM.Phone = cs.Phone;
                    CCVM.ContactPerson = cs.ContactPerson;
                    CCVM.Address = cs.Address;
                    CCVM.ShortName = cs.ShortName;
                    if(cs.Image != null)
                    {
                        CCVM.Image = cs.Image;
                    }
                }

            }
            else if (CCVM != null && id == null && Delete == null)
            {
                if (CCVM.ID != 0)
                {
                    Common_CompanySetup ccs = _db.Common_CompanySetup.Find(CCVM.ID);
                    ccs.Name = CCVM.Name;
                    ccs.Email = CCVM.Email;
                    ccs.Phone = CCVM.Phone;
                    ccs.ContactPerson = CCVM.ContactPerson;
                    ccs.Address = CCVM.Address;
                    ccs.ShortName = CCVM.ShortName;
                   
                    if(a != null)
                    {
                        ccs.Image = a;
                    }
                    _db.Common_CompanySetup.Update(ccs);
                    await _db.SaveChangesAsync();
                }
                else
                {
                    Common_CompanySetup ccs = new Common_CompanySetup();
                    ccs.Name = CCVM.Name;
                    ccs.Email = CCVM.Email;
                    ccs.Phone = CCVM.Phone;
                    ccs.ContactPerson = CCVM.ContactPerson;
                    ccs.Address = CCVM.Address;
                    ccs.ShortName = CCVM.ShortName;
                   
                    if(a != null)
                    {
                        ccs.Image = a;
                    }
                    _db.Common_CompanySetup.Add(ccs);
                    await _db.SaveChangesAsync();
                }
            }
            else if (Delete == "Delete" & id != null)
            {

                Common_CompanySetup ccs = _db.Common_CompanySetup.Find(id);
                ccs.Active = false;
                _db.Common_CompanySetup.Update(ccs);
                await _db.SaveChangesAsync();
            }

            return CCVM;
        }

        public async Task<List<HRMSSectionVM>> HRMSSectionListGet()
        {
            List<HRMSSectionVM> vM = new List<HRMSSectionVM>();

            vM = await Task.Run(() => (from S in _db.HRMS_Section
                                       //join D in _db.User_Department on S.User_DepartmentFK equals D.ID into D_Join
                                       //from D in D_Join.DefaultIfEmpty()
                                       join U in _db.HRMS_Unit on S.HRMS_UnitFK equals U.ID into U_Join
                                       from U in U_Join.DefaultIfEmpty()
                                       join BU in _db.HRMS_BusinessUnit on U.HRMS_BusinessUnitFK equals BU.ID into BU_Join
                                       from BU in BU_Join.DefaultIfEmpty()
                                       where U.Active && BU.Active && S.Active
                                       select new HRMSSectionVM()
                                       {
                                           ID = S.ID,
                                           Name = S.Name,
                                           Code = S.Code,
                                           SectionRemarks = S.Description,
                                           //VMDepartmentID = S.User_DepartmentFK,
                                           //VMDepartmentName = D != null ? D.Name : "",
                                           VMUnitID = S != null ? S.HRMS_UnitFK : (int?)null,
                                           VMUnitName = S != null && U != null ? U.Name : "",
                                           VMBusinessUnitID = U != null && U != null ? U.HRMS_BusinessUnitFK : (int?)null,
                                           VMBusinessUnitName = U != null && U != null && BU != null ? BU.Name : "",
                                       }).ToListAsync());
            return vM;
        }

        public async Task<HRMSSectionVM> AddEditOrDeleteSection(int? id, string Delete, HRMSSectionVM HSVM)
        {
            if (id != null && Delete == null)
            {
                var en = await Task.Run(() => (from S in _db.HRMS_Section
                                               //join D in _db.User_Department on S.User_DepartmentFK equals D.ID into D_Join
                                               //from D in D_Join.DefaultIfEmpty()
                                               join U in _db.HRMS_Unit on S.HRMS_UnitFK equals U.ID into U_Join
                                               from U in U_Join.DefaultIfEmpty()
                                               join BU in _db.HRMS_BusinessUnit on U.HRMS_BusinessUnitFK equals BU.ID into BU_Join
                                               from BU in BU_Join.DefaultIfEmpty()
                                               where S.ID == id.Value && S.Active == true && U.Active == true && BU.Active == true
                                               select new HRMSSectionVM()
                                               {
                                                   ID = S.ID,
                                                   Name = S.Name,
                                                   Code = S.Code,
                                                   SectionRemarks = S.Description,
                                                   //VMDepartmentID = S.User_DepartmentFK,
                                                   //VMDepartmentName = D != null ? D.Name : "",
                                                   VMUnitID = U != null ? S.HRMS_UnitFK : (int?)null,
                                                   VMUnitName = U != null && U != null ? U.Name : "",
                                                   VMBusinessUnitID = BU != null && U != null ? U.HRMS_BusinessUnitFK : (int?)null,
                                                   VMBusinessUnitName = BU != null && U != null && BU != null ? BU.Name : "",
                                               }).FirstOrDefaultAsync());
                if (en != null)
                {
                    HSVM.ID = en.ID;
                    HSVM.Name = en.Name;
                    HSVM.SectionRemarks = en.SectionRemarks;
                    HSVM.VMBusinessUnitID = en.VMBusinessUnitID;
                    HSVM.VMUnitID = en.VMUnitID;
                    HSVM.VMDepartmentID = en.VMDepartmentID;
                    HSVM.Code = en.Code;
                }
            }
            else if (HSVM != null && id == null && Delete == null)
            {
                if (HSVM.ID != 0)
                {
                    HRMS_Section hs = _db.HRMS_Section.Find(HSVM.ID);
                    hs.Name = HSVM.Name;
                    hs.Description = HSVM.SectionRemarks;
                    hs.HRMS_UnitFK = HSVM.VMUnitID;
                    hs.Code = HSVM.Code;

                    _db.HRMS_Section.Update(hs);
                    await _db.SaveChangesAsync();
                }
                else
                {
                    HRMS_Section hs = new HRMS_Section();
                    hs.Name = HSVM.Name;
                    hs.Description = HSVM.SectionRemarks;
                    hs.HRMS_UnitFK = HSVM.VMUnitID;
                    hs.Code = HSVM.Code;

                    _db.HRMS_Section.Add(hs);
                    await _db.SaveChangesAsync();
                }

            }
            else if (Delete == "Delete" & id != null)
            {
                var DeptId = _db.HRMS_Employee.Where(s => s.HRMS_SectionFK == id).FirstOrDefault();
                if(DeptId != null)
                {
                    HSVM.error = "Can't delete! Already in use.";
                }
                else
                {
                    HRMS_Section hs = _db.HRMS_Section.Find(id);
                    hs.Active = false;
                    _db.HRMS_Section.Update(hs);
                    await _db.SaveChangesAsync();
                }
            }
            return HSVM;
        }

        public async Task<VMOffDay> AddEditOrDeleteOffDay(int? id, string Delete, VMOffDay VMOD)
        {

            if (id != null && Delete == null)
            {
                var cs = _db.HRMS_OffDay.Find(id);
                if (cs != null)
                {
                    VMOD.ID = cs.ID;
                    VMOD.Title = cs.Title;
                    VMOD.Description = cs.Description;
                    VMOD.ApplicableDate = cs.ApplicableDate;
                    VMOD.ReplacementDate = cs.ReplacementDate;
                }

            }
            else if (VMOD != null && id == null && Delete == null)
            {
                if (VMOD.ID != 0)
                {
                    HRMS_OffDay HO = _db.HRMS_OffDay.Find(VMOD.ID);
                    HO.Title = VMOD.Title;
                    HO.Description = VMOD.Description;
                    HO.ApplicableDate = VMOD.ApplicableDate;
                    HO.ReplacementDate = VMOD.ReplacementDate;

                    _db.HRMS_OffDay.Update(HO);
                    await _db.SaveChangesAsync();
                }
                else
                {
                    HRMS_OffDay HO = new HRMS_OffDay();
                    HO.Title = VMOD.Title;
                    HO.Description = VMOD.Description;
                    HO.ApplicableDate = VMOD.ApplicableDate;
                    HO.ReplacementDate = VMOD.ReplacementDate;

                    _db.HRMS_OffDay.Add(HO);
                    await _db.SaveChangesAsync();
                }

            }
            else if (Delete == "Delete" & id != null)
            {

                HRMS_OffDay ho = _db.HRMS_OffDay.Find(id);
                ho.Active = false;
                _db.HRMS_OffDay.Update(ho);
                await _db.SaveChangesAsync();
            }

            return VMOD;
        }

        public async Task<VMHoliday> AddEditOrDeleteHolidayDay(int? id, string Delete, VMHoliday VMH, string String)
        {
            if (String == "HolidayAssign")
            {
                if (id != null && Delete == null)
                {
                    var hd = _db.HRMS_Holiday.Find(id);
                    VMHoliday vM = new VMHoliday();

                    vM = await Task.Run(() => (from hh in _db.HRMS_Holiday
                                               where hh.HoildayName == hd.HoildayName && hh.Active && hh.IsCompanyWeekendDay == false
                                               group hh by hh.HoildayName into g
                                               select new VMHoliday()
                                               {
                                                   ID = g.Min(x => x.ID),
                                                   HoildayName = g.Key,
                                                   FromDate = g.Min(x => x.FromDate),
                                                   ToDate = g.Max(x => x.ToDate),
                                                   TotalDays = g.Count(),
                                                   Description = g.Key
                                               }).FirstOrDefault());
                    VMH.ID = vM.ID;
                    VMH.HoildayName = vM.HoildayName;
                    VMH.Description = vM.Description;
                    VMH.FromDate = vM.FromDate;
                    VMH.ToDate = vM.ToDate;
                    VMH.TotalDays = vM.TotalDays;
                    VMH.PreviousHolidayName = vM.HoildayName;
                }
                else if(VMH != null && id == null && Delete == null)
                {
                    if (VMH.ID != 0)
                    {
                        var aa = _db.HRMS_Holiday.Where(s => s.HoildayName == VMH.PreviousHolidayName).ToList();

                        for(var i = 0; i< VMH.TotalDays; i++)
                        {
                            if (i < aa.Count)
                            {
                                foreach (var a in aa)
                                {
                                    if (i == 0)
                                    {
                                        a.HoildayName = VMH.HoildayName;
                                        a.FromDate = VMH.FromDate;
                                        a.ToDate = VMH.FromDate;
                                        a.Description = VMH.Description;
                                        a.IsCompanyWeekendDay = false;
                                        a.TotalDays = 1;
                                        _db.HRMS_Holiday.Update(a);
                                        await _db.SaveChangesAsync();
                                    }
                                    else if(i>0 && VMH.TotalDays > aa.Count)
                                    {
                                        DateTime dta = VMH.FromDate;
                                        a.HoildayName = VMH.HoildayName;
                                        a.FromDate = dta.AddDays(i);
                                        a.ToDate = dta.AddDays(i);
                                        a.Description = VMH.Description;
                                        a.IsCompanyWeekendDay = false;
                                        a.TotalDays = 1;
                                        a.Active = true;
                                        _db.HRMS_Holiday.Update(a);
                                        await _db.SaveChangesAsync();
                                    }
                                    else if (i > 0 && VMH.TotalDays < aa.Count)
                                    {    
                                        if (i < VMH.TotalDays)
                                        {
                                            DateTime dta = VMH.FromDate;
                                            a.HoildayName = VMH.HoildayName;
                                            a.FromDate = dta.AddDays(i);
                                            a.ToDate = dta.AddDays(i);
                                            a.Description = VMH.Description;
                                            a.IsCompanyWeekendDay = false;
                                            a.TotalDays = 1;
                                            a.Active = true;
                                            _db.HRMS_Holiday.Update(a);
                                            await _db.SaveChangesAsync();
                                        }
                                        else
                                        {
                                            a.Active = false;
                                            _db.HRMS_Holiday.Update(a);
                                            await _db.SaveChangesAsync();
                                        }
                                    }
                                    i++;
                                }
                            }
                            if (i < VMH.TotalDays)
                            {
                                HRMS_Holiday hhd = new HRMS_Holiday();
                                DateTime dt = VMH.FromDate;
                                hhd.HoildayName = VMH.HoildayName;
                                hhd.FromDate = dt.AddDays(i);
                                hhd.ToDate = dt.AddDays(i);
                                hhd.Description = VMH.Description;
                                hhd.IsCompanyWeekendDay = false;
                                hhd.TotalDays = 1;
                                _db.HRMS_Holiday.Add(hhd);
                                await _db.SaveChangesAsync();
                            }
                            i++;
                        }
                    }
                    else
                    {
                        for(var i = 0; i<VMH.TotalDays; i++)
                        {
                            if (i == 0)
                            {
                                HRMS_Holiday HH = new HRMS_Holiday();
                                HH.HoildayName = VMH.HoildayName;
                                HH.Description = VMH.Description;
                                HH.FromDate = VMH.FromDate;
                                HH.ToDate = VMH.FromDate;
                                HH.TotalDays = 1;
                                HH.IsCompanyWeekendDay = false;

                                _db.HRMS_Holiday.Add(HH);
                                await _db.SaveChangesAsync();
                            }
                            else
                            {
                                DateTime dt = VMH.FromDate;
                                HRMS_Holiday HH = new HRMS_Holiday();
                                HH.HoildayName = VMH.HoildayName;
                                HH.Description = VMH.Description;
                                HH.FromDate = dt.AddDays(i);
                                HH.ToDate = dt.AddDays(i);
                                HH.TotalDays = 1;
                                HH.IsCompanyWeekendDay = false;

                                _db.HRMS_Holiday.Add(HH);
                                await _db.SaveChangesAsync();
                            }
                        }
                    }
                }
                else if(id != null && Delete == "Delete")
                {
                    var hh = _db.HRMS_Holiday.Find(id);
                    var hList = _db.HRMS_Holiday.Where( h => h.HoildayName == hh.HoildayName).ToListAsync();

                    foreach(var i in hList.Result)
                    {
                        i.Active = false;
                        _db.HRMS_Holiday.Update(i);
                        await _db.SaveChangesAsync();
                    }
                }
            }
            else
            {
                if (id != null && Delete == null)
                {
                    var hd = _db.HRMS_Holiday.Find(id);
                    VMH.ID = hd.ID;
                    VMH.WeekenedDay = new string[] { hd.HoildayName };
                    VMH.WeekenedDay[0] = hd.HoildayName;
                    VMH.IsCompanyWeekendDay = hd.IsCompanyWeekendDay;
                }
                else if(VMH != null && id == null && Delete == null)
                {
                    if(VMH.ID != 0)
                    {
                        HRMS_Holiday HH = _db.HRMS_Holiday.Find(VMH.ID);
                        foreach (var i in VMH.WeekenedDay)
                        {
                            HH.HoildayName = Convert.ToString(i);
                            HH.IsCompanyWeekendDay = true;

                            _db.HRMS_Holiday.Update(HH);
                            await _db.SaveChangesAsync();
                        }
                    }
                    else
                    {
                        if (VMH.WeekenedDay != null)
                        {
                            foreach (var i in VMH.WeekenedDay)
                            {
                                var weekendDay = _db.HRMS_Holiday.Where(h => h.HoildayName == Convert.ToString(i) && h.Active == true).Any();
                                if(weekendDay != false)
                                {
                                    VMH.error = "Already Added!";
                                }
                                else
                                {
                                    HRMS_Holiday HH = new HRMS_Holiday();
                                    HH.HoildayName = Convert.ToString(i);
                                    HH.IsCompanyWeekendDay = true;

                                    _db.HRMS_Holiday.Add(HH);
                                    await _db.SaveChangesAsync();
                                }
                            }
                        }
                    }
                }
                else if(id != null && Delete == "Delete")
                {
                    HRMS_Holiday HH = _db.HRMS_Holiday.Find(id);
                    HH.Active = false;
                    _db.HRMS_Holiday.Update(HH);
                    await _db.SaveChangesAsync();
                }
            }
            return VMH;
        }

        public async Task<List<VMHoliday>> HolidayGet()
        {

            List<VMHoliday> vM = new List<VMHoliday>();

            vM = await Task.Run(() => (from hh in _db.HRMS_Holiday
                                       where hh.Active == true && hh.IsCompanyWeekendDay == false
                                       group hh by hh.HoildayName into g
                                       select new VMHoliday()
                                       {
                                           ID = g.Min(x=>x.ID),
                                           HoildayName = g.Key,
                                           FromDate = g.Min(x=>x.FromDate),
                                           ToDate = g.Max(x=>x.ToDate),
                                           TotalDays = g.Count()

                                       }).ToListAsync());
            return vM;
        }

        public async Task<VMLeaveSetup> AddEditOrDeleteLeaveDay(int? id, string Delete, VMLeaveSetup VMLS)
        {
            if (id != null && Delete == null)
            {
                var hl = _db.HRMS_Leave.Find(id);
                VMLS.ID = hl.ID;
                VMLS.LeaveTypeID = hl.LeaveType;
                VMLS.EmployeeTypeId = hl.EmployeeType;
                VMLS.LeaveDays = hl.LeaveDays;
            }
            else if(VMLS != null && id == null && Delete == null)
            {
                if (VMLS.ID != 0)
                {
                    HRMS_Leave HL = _db.HRMS_Leave.Find(VMLS.ID);
                    HL.LeaveType = VMLS.LeaveTypeID;
                    HL.EmployeeType = VMLS.EmployeeTypeId;
                    HL.LeaveDays = VMLS.LeaveDays;
                    _db.HRMS_Leave.Update(HL);
                    await _db.SaveChangesAsync();
                }
                else
                {
                    var hl = _db.HRMS_Leave.Where(l => l.LeaveType == VMLS.LeaveTypeID && l.EmployeeType == VMLS.EmployeeTypeId && l.Active == true).Any();
                    if (hl != false)
                    {
                        VMLS.error = "Already Added!";
                    }
                    else
                    {
                        HRMS_Leave HL = new HRMS_Leave();
                        HL.LeaveType = VMLS.LeaveTypeID;
                        HL.EmployeeType = VMLS.EmployeeTypeId;
                        HL.LeaveDays = VMLS.LeaveDays;

                        _db.HRMS_Leave.Add(HL);
                        await _db.SaveChangesAsync();
                    }
                }
            }
            else if (Delete == "Delete" & id != null)
            {
                HRMS_Leave hl = _db.HRMS_Leave.Find(id);
                var el = _db.HRMS_LeaveApplication.Where(l => l.HRMS_LeaveFK == id).Any();
                if(el != false)
                {
                    VMLS.error = "Can't Delete! Already in use.";
                    return VMLS;
                }
                else
                {
                    hl.Active = false;
                    _db.HRMS_Leave.Update(hl);
                    await _db.SaveChangesAsync();
                }
            }
            return VMLS;
        }


        public async Task<HRMSDistrictVM> AddEditOrDeleteDistrict(int? id, string Delete, HRMSDistrictVM HDVM)
        {
            if (id != null && Delete == null)
            {
                var en = await Task.Run(() => (from D in _db.HRMS_District
                                               join C in _db.Common_Country on D.Common_CountryFK equals C.ID into C_Join
                                               from C in C_Join.DefaultIfEmpty()
                                               where D.ID == id.Value && D.Active && C.Active
                                               select new HRMSDistrictVM()
                                               {
                                                   ID = D.ID,
                                                   Name = D.Name,
                                                   CountryName = C.Name,
                                                   CountryId = D.Common_CountryFK
                                               }).FirstOrDefaultAsync());
                if (en != null)
                {
                    HDVM.ID = en.ID;
                    HDVM.Name = en.Name;
                    HDVM.CountryName = en.CountryName;
                    HDVM.CountryId = en.CountryId;
                }
            }
            else if (HDVM != null && id == null && Delete == null)
            {
                if (HDVM.ID != 0)
                {
                    HRMS_District hd = _db.HRMS_District.Find(HDVM.ID);
                    hd.Name = HDVM.Name;
                    hd.Common_CountryFK = HDVM.CountryId;

                    _db.HRMS_District.Update(hd);
                    await _db.SaveChangesAsync();
                }
                else
                {
                    HRMS_District hd = new HRMS_District();
                    hd.Name = HDVM.Name;
                    hd.Common_CountryFK = HDVM.CountryId;

                    _db.HRMS_District.Add(hd);
                    await _db.SaveChangesAsync();
                }

            }
            else if (Delete == "Delete" & id != null)
            {
                HRMS_District hd = _db.HRMS_District.Find(id);
                var emp = (from e in _db.HRMS_Employee
                           join po in _db.HRMS_PostOffice on e.HRMS_PostOfficeFK equals po.ID into po_Join
                           from po in po_Join.DefaultIfEmpty()
                           join ps in _db.HRMS_PoliceStation on po.HRMS_PoliceStationFK equals ps.ID into ps_Join
                           from ps in ps_Join.DefaultIfEmpty()
                           join dis in _db.HRMS_District on ps.HRMS_DistrictFK equals dis.ID into dis_Join
                           from dis in dis_Join.DefaultIfEmpty()
                           join C in _db.Common_Country on dis.Common_CountryFK equals C.ID into C_Join
                           from C in C_Join.DefaultIfEmpty()
                           where dis.ID == id
                           select e.ID).Any();

                if(emp != false)
                {
                    HDVM.error = "Can't Delete! Already in use!";
                    return HDVM;
                }
                else
                {
                    hd.Active = false;
                    await _db.SaveChangesAsync();
                }
            }
            return HDVM;
        }

        public async Task<HRMSPoliceStationVM> AddEditOrDeletePoliceStation(int? id, string Delete, HRMSPoliceStationVM HSVM)
        {
            if (id != null && Delete == null)
            {
                var en = await Task.Run(() => (from P in _db.HRMS_PoliceStation
                                               join D in _db.HRMS_District on P.HRMS_DistrictFK equals D.ID into D_Join
                                               from D in D_Join.DefaultIfEmpty()
                                               join C in _db.Common_Country on D.Common_CountryFK equals C.ID into C_Join
                                               from C in C_Join.DefaultIfEmpty()
                                               where P.ID == id.Value && P.Active
                                               select new HRMSPoliceStationVM()
                                               {
                                                   ID = P.ID,
                                                   Name = P.Name,
                                                   CountryName = C.Name,
                                                   DistrictName = D.Name,
                                                   CountryId = P.Common_CountryFK,
                                                   DistrictId = P.HRMS_DistrictFK
                                               }).FirstOrDefaultAsync());
                if (en != null)
                {
                    HSVM.ID = en.ID;
                    HSVM.Name = en.Name;
                    HSVM.CountryId = en.CountryId;
                    HSVM.DistrictId = en.DistrictId;
                }
            }
            else if (HSVM != null && id == null && Delete == null)
            {
                if (HSVM.ID != 0)
                {
                    HRMS_PoliceStation hp = _db.HRMS_PoliceStation.Find(HSVM.ID);
                    hp.Name = HSVM.Name;
                    hp.Common_CountryFK = HSVM.CountryId;
                    hp.HRMS_DistrictFK = HSVM.DistrictId;

                    _db.HRMS_PoliceStation.Update(hp);
                    await _db.SaveChangesAsync();
                }
                else
                {
                    HRMS_PoliceStation hp = new HRMS_PoliceStation();
                    hp.Name = HSVM.Name;
                    hp.Common_CountryFK = HSVM.CountryId;
                    hp.HRMS_DistrictFK = HSVM.DistrictId;

                    _db.HRMS_PoliceStation.Add(hp);
                    await _db.SaveChangesAsync();
                }

            }
            else if (Delete == "Delete" & id != null)
            {

                HRMS_PoliceStation hs = _db.HRMS_PoliceStation.Find(id);
                var emp = (from e in _db.HRMS_Employee
                           join po in _db.HRMS_PostOffice on e.HRMS_PostOfficeFK equals po.ID into po_Join
                           from po in po_Join.DefaultIfEmpty()
                           join ps in _db.HRMS_PoliceStation on po.HRMS_PoliceStationFK equals ps.ID into ps_Join
                           from ps in ps_Join.DefaultIfEmpty()
                           join dis in _db.HRMS_District on ps.HRMS_DistrictFK equals dis.ID into dis_Join
                           from dis in dis_Join.DefaultIfEmpty()
                           join C in _db.Common_Country on dis.Common_CountryFK equals C.ID into C_Join
                           from C in C_Join.DefaultIfEmpty()
                           where ps.ID == id
                           select e.ID).Any();

                if (emp != false)
                {
                    HSVM.error = "Can't Delete! Already in use!";
                    return HSVM;
                }
                else
                {
                    hs.Active = false;
                    _db.HRMS_PoliceStation.Update(hs);
                    await _db.SaveChangesAsync();
                }
            }
            return HSVM;
        }

        public async Task<HRMSDegreeVM> AddEditOrDeleteDegree(int? id, string Delete, HRMSDegreeVM HDVM)
        {
            if (id != null && Delete == null)
            {
                var d = await Task.Run(() => (from ED in _db.HRMS_EducationalDegree
                                               where ED.ID == id.Value && ED.Active
                                               select new HRMSDegreeVM()
                                               {
                                                   ID = ED.ID,
                                                   Name = ED.Name,
                                                   Description = ED.Description,
                                               }).FirstOrDefaultAsync());
                if (d != null)
                {
                    HDVM.ID = d.ID;
                    HDVM.Name = d.Name;
                    HDVM.Description = d.Description;
                }
            }
            else if (HDVM != null && id == null && Delete == null)
            {
                if (HDVM.ID != 0)
                {
                    HRMS_EducationalDegree ed = _db.HRMS_EducationalDegree.Find(HDVM.ID);
                    ed.Name = HDVM.Name;
                    ed.Description = HDVM.Description;

                    _db.HRMS_EducationalDegree.Update(ed);
                    await _db.SaveChangesAsync();
                }
                else
                {
                    HRMS_EducationalDegree ed = new HRMS_EducationalDegree();
                    ed.Name = HDVM.Name;
                    ed.Description = HDVM.Description;

                    _db.HRMS_EducationalDegree.Add(ed);
                    await _db.SaveChangesAsync();
                }

            }
            else if (Delete == "Delete" & id != null)
            {

                HRMS_EducationalDegree ed = _db.HRMS_EducationalDegree.Find(id);
                var emp = (from e in _db.HRMS_EmployeeEducation
                           join dg in _db.HRMS_EducationalDegree on e.HRMS_EducationalDegreeFK equals dg.ID into dg_Join
                           from dg in dg_Join.DefaultIfEmpty()
                           where dg.ID == id
                           select dg.ID).Any();

                if (emp != false)
                {
                    HDVM.error = "Can't Delete! Already in use!";
                    return HDVM;
                }
                else
                {
                    ed.Active = false;
                    _db.HRMS_EducationalDegree.Update(ed);
                    await _db.SaveChangesAsync();
                }
            }
            return HDVM;
        }
        public async Task<HRMSPostOfficeVM> AddEditOrDeletePostOffice(int? id, string Delete, HRMSPostOfficeVM HSVM)
        {
            if (id != null && Delete == null)
            {
                var en = await Task.Run(() => (from PO in _db.HRMS_PostOffice
                                               join D in _db.HRMS_District on PO.HRMS_DistrictFK equals D.ID into D_Join
                                               from D in D_Join.DefaultIfEmpty()
                                               join C in _db.Common_Country on PO.Common_CountryFK equals C.ID into C_Join
                                               from C in C_Join.DefaultIfEmpty()
                                               join P in _db.HRMS_PoliceStation on PO.HRMS_PoliceStationFK equals P.ID into P_Join
                                               from P in P_Join.DefaultIfEmpty()
                                               where PO.ID == id.Value && PO.Active && D.Active && P.Active && C.Active
                                               select new HRMSPostOfficeVM()
                                               {
                                                   ID = PO.ID,
                                                   Name = PO.Name,
                                                   CountryId = PO.Common_CountryFK,
                                                   DistrictId = PO.HRMS_DistrictFK,
                                                   PoliceStationId = PO.HRMS_PoliceStationFK,
                                                   Code = PO.PostCode
                                               }).FirstOrDefaultAsync());
                if (en != null)
                {
                    HSVM.ID = en.ID;
                    HSVM.Name = en.Name;
                    HSVM.CountryId = en.CountryId;
                    HSVM.DistrictId = en.DistrictId;
                    HSVM.PoliceStationId = en.PoliceStationId;
                    HSVM.Code = en.Code;
                }
            }
            else if (HSVM != null && id == null && Delete == null)
            {
                if (HSVM.ID != 0)
                {
                    HRMS_PostOffice hpo = _db.HRMS_PostOffice.Find(HSVM.ID);
                    hpo.Name = HSVM.Name;
                    hpo.Common_CountryFK = HSVM.CountryId;
                    hpo.HRMS_DistrictFK = HSVM.DistrictId;
                    hpo.HRMS_PoliceStationFK = HSVM.PoliceStationId;
                    hpo.PostCode = HSVM.Code;

                    _db.HRMS_PostOffice.Update(hpo);
                    await _db.SaveChangesAsync();
                }
                else
                {
                    HRMS_PostOffice hpo = new HRMS_PostOffice();
                    hpo.Name = HSVM.Name;
                    hpo.Common_CountryFK = HSVM.CountryId;
                    hpo.HRMS_DistrictFK = HSVM.DistrictId;
                    hpo.HRMS_PoliceStationFK = HSVM.PoliceStationId;
                    hpo.PostCode = HSVM.Code;

                    _db.HRMS_PostOffice.Add(hpo);
                    await _db.SaveChangesAsync();
                }

            }
            else if (Delete == "Delete" & id != null)
            {

                HRMS_PostOffice hpo = _db.HRMS_PostOffice.Find(id);
                var emp = (from e in _db.HRMS_Employee
                           join po in _db.HRMS_PostOffice on e.HRMS_PostOfficeFK equals po.ID into po_Join
                           from po in po_Join.DefaultIfEmpty()
                           join ps in _db.HRMS_PoliceStation on po.HRMS_PoliceStationFK equals ps.ID into ps_Join
                           from ps in ps_Join.DefaultIfEmpty()
                           join dis in _db.HRMS_District on ps.HRMS_DistrictFK equals dis.ID into dis_Join
                           from dis in dis_Join.DefaultIfEmpty()
                           join C in _db.Common_Country on dis.Common_CountryFK equals C.ID into C_Join
                           from C in C_Join.DefaultIfEmpty()
                           where ps.ID == id
                           select e.ID).Any();

                if (emp != false)
                {
                    HSVM.error = "Can't Delete! Already in use!";
                    return HSVM;
                }
                else
                {
                    hpo.Active = false;
                    _db.HRMS_PostOffice.Update(hpo);
                    await _db.SaveChangesAsync();
                }
            }
            return HSVM;
        }

        public async Task<List<CountryToPostOficeListVM>> CountryToPostOficeList(int id)
        {
            List<CountryToPostOficeListVM> vM = new List<CountryToPostOficeListVM>();
            if (id == 1)
            {
                vM = await Task.Run(() => (from D in _db.HRMS_District
                                           join C in _db.Common_Country on D.Common_CountryFK equals C.ID into C_Join
                                           from C in C_Join.DefaultIfEmpty()
                                           where D.Active && C.Active
                                           select new CountryToPostOficeListVM()
                                           {
                                               ID = D.ID,
                                               DistrictName = D.Name,
                                               CountryName = C.Name
                                           }).ToListAsync());
                return vM;
            }
            else if(id == 2)
            {
                vM = await Task.Run(() => (from P in _db.HRMS_PoliceStation
                                           join D in _db.HRMS_District on P.HRMS_DistrictFK equals D.ID into D_Join
                                           from D in D_Join.DefaultIfEmpty()
                                           join C in _db.Common_Country on P.Common_CountryFK equals C.ID into C_Join
                                           from C in C_Join.DefaultIfEmpty()
                                           where D.Active && C.Active && P.Active
                                           select new CountryToPostOficeListVM()
                                           {
                                               ID = P.ID,
                                               DistrictName = D.Name,
                                               CountryName = C.Name,
                                               PoliceStationName = P.Name
                                           }).ToListAsync());
                return vM;

            }
            else if(id == 3)
            {
                vM = await Task.Run(() => (from PO in _db.HRMS_PostOffice
                                           join P in _db.HRMS_PoliceStation on PO.HRMS_PoliceStationFK equals P.ID into P_Join
                                           from P in P_Join.DefaultIfEmpty()
                                           join D in _db.HRMS_District on PO.HRMS_DistrictFK equals D.ID into D_Join
                                           from D in D_Join.DefaultIfEmpty()
                                           join C in _db.Common_Country on PO.Common_CountryFK equals C.ID into C_Join
                                           from C in C_Join.DefaultIfEmpty()
                                           where D.Active && C.Active && PO.Active
                                           select new CountryToPostOficeListVM()
                                           {
                                               ID = PO.ID,
                                               DistrictName = D.Name,
                                               CountryName = C.Name,
                                               PoliceStationName = P.Name,
                                               PostOfficeName = PO.Name,
                                               PostCode = PO.PostCode
                                           }).ToListAsync());
                return vM;
            }

            return vM;
        }
        public async Task<List<HRMSDegreeVM>> DegreeList()
        {
            List<HRMSDegreeVM> vM = new List<HRMSDegreeVM>();
            vM = await Task.Run(() => (from D in _db.HRMS_EducationalDegree
                                       where D.Active
                                       select new HRMSDegreeVM()
                                       {
                                           ID = D.ID,
                                           Name = D.Name,
                                           Description = D.Description
                                       }).ToListAsync());
            return vM;
        }

        public async Task<VMAttendanceRemarks> AttendanceRemarksSpecificGet(int id)
        {
            VMAttendanceRemarks VM = new VMAttendanceRemarks();

            VM = await Task.Run(() => (from AR in _db.HRMS_AttendanceRemarks

                                       where AR.Active && AR.ID == id
                                       select new VMAttendanceRemarks()
                                       {
                                           ID = AR.ID,

                                           AttendanceRemark = AR.AttendanceRemark
                                       }).FirstOrDefaultAsync());

            return VM;
        }

        public async Task<List<VMAttendanceRemarks>> AttendanceRemarksListGet()
        {
            List<VMAttendanceRemarks> VM = new List<VMAttendanceRemarks>();

            VM = await Task.Run(() => (from AR in _db.HRMS_AttendanceRemarks

                                       where AR.Active
                                       select new VMAttendanceRemarks()
                                       {
                                           ID = AR.ID,

                                           AttendanceRemark = AR.AttendanceRemark
                                       }).ToListAsync());


            return VM;
        }

        public async Task<int> AttendanceRemarksAdd(VMAttendanceRemarks model)
        {
            var result = -1;

            HRMS_AttendanceRemarks en = new HRMS_AttendanceRemarks() { AttendanceRemark = model.AttendanceRemark, User = model.User, UserID = model.UserID, };

            await _db.HRMS_AttendanceRemarks.AddRangeAsync(en);

            if (await _db.SaveChangesAsync() > 0)
            {
                result = 1;
            }
            return result;
        }
        public async Task<int> AttendanceRemarksEdit(VMAttendanceRemarks model)
        {
            var result = -1;
            HRMS_AttendanceRemarks en = await _db.HRMS_AttendanceRemarks.FindAsync(model.ID);

            en.AttendanceRemark = model.AttendanceRemark;



            if (await _db.SaveChangesAsync() > 0)
            {
                result = en.ID;
            }
            return result;
        }
        public async Task<int> AttendanceRemarksDelete(int id)
        {
            var result = -1;
            HRMS_AttendanceRemarks selectSingle = await _db.HRMS_AttendanceRemarks.FindAsync(id);
            if (selectSingle != null)
            {
                selectSingle.Active = false;
                if (await _db.SaveChangesAsync() > 0)
                {

                    result = selectSingle.ID;
                }
            }
            return result;
        }
        #endregion

        #region Leave Assign

        public async Task<IQueryable<Emplist>> GetEmployees(VMLeave VML)
        {
            var emp = await (from em in _db.HRMS_Employee
                       join LA in _db.HRMS_EmployeeLeaveAssign on em.ID equals LA.HRMS_EmployeeFK into abc
                       from LA in abc.DefaultIfEmpty()
                       join s in _db.HRMS_Section on em.HRMS_SectionFK equals s.ID into s_Join
                       from s in s_Join.DefaultIfEmpty()
                       join dp in _db.User_Department on em.User_DepartmentFK equals dp.ID into dp_Join
                       from dp in dp_Join.DefaultIfEmpty()
                       join u in _db.HRMS_Unit on s.HRMS_UnitFK equals u.ID into u_Join
                       from u in u_Join.DefaultIfEmpty()
                       join bu in _db.HRMS_BusinessUnit on u.HRMS_BusinessUnitFK equals bu.ID into bu_Join
                       from bu in bu_Join.DefaultIfEmpty()
                       select new Emplist
                       {
                           ID = em.ID,
                           EmployeeIdentity = em.EmployeeIdentity,
                           Name = em.Name,
                           Designation = em.HRMS_Designation.Name,
                           BusinessUnit = bu.Name,
                           Department = dp.Name,
                           Unit = u.Name,
                           Section = em.HRMS_Section.Name,
                           EmpType = Convert.ToString((EnumStaffType)em.StaffType),
                           AnnualLeaveDays = LA.AnnualLeaveDays,
                           TakenAnnualLeaveDays = LA.TakenAnnualLeaveDays,
                           SickLeaveDays = LA.SickLeaveDays,
                           TakenSickLeaveDays = LA.TakenSickLeaveDays,
                           CasualLeaveDays = LA.CasualLeaveDays,
                           TakenCasualLeaveDays = LA.TakenCasualLeaveDays,
                           IsLeaveAssigned = em.IsLeaveAssigned,
                           //for Query Property
                           BU_Id = bu.ID,
                           Unit_ID = u.ID,
                           Section_ID = em.HRMS_SectionFK,
                           Dept_ID = dp.ID,
                           LeaveTypeId = LA.HRMS_LeaveFK,
                           EmployeeId = em.ID
                       }).ToListAsync();
            var empList = emp.AsQueryable();

            if (VML.BU_Id != 0 && VML.Unit_ID == 0 && VML.Dept_ID == 0 && VML.Section_ID == 0)
            {
                empList = empList.Where(e => e.BU_Id == VML.BU_Id);
                return empList;
            }
            else if (VML.BU_Id != 0 && VML.Unit_ID != 0 && VML.Dept_ID == 0 && VML.Section_ID == 0)
            {
                empList = empList.Where(e => e.BU_Id == VML.BU_Id && e.Unit_ID == VML.Unit_ID);

                return empList;
            }
            else if (VML.BU_Id != 0 && VML.Unit_ID != 0 && VML.Dept_ID != 0 && VML.Section_ID == 0)
            {
                empList = empList.Where(e => e.BU_Id == VML.BU_Id && e.Unit_ID == VML.Unit_ID && e.Dept_ID == VML.Dept_ID);

                return empList;

            }
            else if (VML.BU_Id != 0 && VML.Unit_ID != 0 && VML.Dept_ID != 0 && VML.Section_ID != 0)
            {
                empList = empList.Where(e => e.BU_Id == VML.BU_Id && e.Unit_ID == VML.Unit_ID && e.Dept_ID == VML.Dept_ID && e.Section_ID == VML.Section_ID);

                return empList;
            }

            return null;
        }

        public async Task<EmployeeInfo> getEmployeeInfo(int id)
        {
            var emp = await Task.Run(() => (from em in _db.HRMS_Employee.Where(s => s.ID == id)
                                            join s in _db.HRMS_Section on em.HRMS_SectionFK equals s.ID into s_Join
                                            from s in s_Join.DefaultIfEmpty()
                                            join dp in _db.User_Department on em.User_DepartmentFK equals dp.ID into dp_Join
                                            from dp in dp_Join.DefaultIfEmpty()
                                            join u in _db.HRMS_Unit on s.HRMS_UnitFK equals u.ID into u_Join
                                            from u in u_Join.DefaultIfEmpty()
                                            join bu in _db.HRMS_BusinessUnit on u.HRMS_BusinessUnitFK equals bu.ID into bu_Join
                                            from bu in bu_Join.DefaultIfEmpty()
                                            join d in _db.HRMS_Designation on em.HRMS_DesignationFK equals d.ID into d_Join
                                            from d in d_Join.DefaultIfEmpty()
                                            join I in _db.HRMS_EmployeeImage on em.ID equals I.HRMS_EmployeeFK into I_Join
                                            from I in I_Join.DefaultIfEmpty()
                                            join ed in _db.HRMS_EmployeeDependent on em.ID equals ed.HRMS_EmployeeFK into ed_Join
                                            from ed in ed_Join.DefaultIfEmpty()
                                            join pl in _db.Common_ProductionLine on em.Common_ProductionLineFK equals pl.ID into pl_Join
                                            from pl in pl_Join.DefaultIfEmpty()
                                            join peod in _db.Payroll_EODRecordMaster on em.ID equals peod.EmployeeFk into peod_Join
                                            from peod in peod_Join.DefaultIfEmpty()
                                            join pss in _db.Payroll_SalaryStructure on peod.SalaryStructureFk equals pss.ID into pss_Join
                                            from pss in pss_Join.DefaultIfEmpty()
                                            select new EmployeeInfo
                                            {
                                                ID = em.ID,
                                                dPid = dp.ID,
                                                dId = d.ID,
                                                EmployeeName = em.NameBN,
                                                BusinessUnit = bu.Name,
                                                Unit = u.Name,
                                                Section = s.Name,
                                                Designation = d.Name,
                                                Department = dp.Name,
                                                JoinDate = em.JoiningDate,
                                                DateOfBirth = em.DOB,
                                                EmGender = em.Gender,
                                                EmFName = em.FatherName,
                                                EmMName = em.MotherName,
                                                EmHName = ed.Name,
                                                EmImage = I.Image,
                                                EmIdentity = em.EmployeeIdentity,
                                                PresentAddress = em.PresentAddress,
                                                PermanentAddress = em.PermanentAddress,
                                                Grade = em.Grade,
                                                CardNo = em.CardNo,
                                                Year = findDate(em.JoiningDate, DateTime.Now, "Year"),
                                                Month = findDate(em.JoiningDate, DateTime.Now, "Month"),
                                                Day = findDate(em.JoiningDate, DateTime.Now, "Days"),
                                                GrossSalary = peod != null ? peod.GrossSalary : 0,
                                                HousePer = pss != null ? pss.HouseRentInPercent : 0,
                                                MedicalPer = pss != null ? pss.MedicalAllowance : 0,
                                                FoodPer = pss != null ? pss.FoodlAllowance : 0,
                                                TransportPer = pss != null ? pss.TransportAllowance : 0,
                                                BasicPer = pss != null ? pss.BasicInPercent : 0
                                            }).FirstOrDefaultAsync());

            List<HRMSJobDescriptionVM> HJDVM = jobDescriptionList(emp.dPid, emp.dId);
            if(HJDVM != null)
            {
                emp.JobDescription = HJDVM;
            }
            else
            {
                return emp;
            }

            return emp;
        }

        public List<HRMSJobDescriptionVM> jobDescriptionList(int? dPid, int? dId)
        {
            var jList = (from j in _db.HRMS_JobDescription
                         where j.User_DepartmentFK == dPid && j.HRMS_DesignationFK == dId
                         select new HRMSJobDescriptionVM
                             {
                                 ID = j.ID,
                                 JobDescription = j.JobDescription
                             }).ToList();
            if (jList.Any())
            {
                return jList;
            }
            else
            {
                List<HRMSJobDescriptionVM> HJDVM = new List<HRMSJobDescriptionVM>();
                HJDVM.Add(new HRMSJobDescriptionVM { ID = 1, JobDescription = "Not Assigned" });
                return HJDVM;
            }
        }

        public async Task<object> GetEmployeeLeaveInfo(int id)
        {
            var leaveInfo = await _db.HRMS_EmployeeLeaveAssign.Where(l => l.HRMS_EmployeeFK == id).FirstOrDefaultAsync();
            return leaveInfo;
        }

        public async Task<List<VMShift>> GetShift()
        {
            var shift = await (from s in _db.HRMS_Shift
                               where s.Active
                               select new VMShift
                               {
                                   ID = s.ID,
                                   ShiftName = s.ShiftName,
                                   StartTime = DateTime.Parse(Convert.ToString(s.StartTime)).ToString("hh:mm tt"),
                                   EndTime = DateTime.Parse(Convert.ToString(s.EndTime)).ToString("hh:mm tt"),
                                   BreakTime = s.BreakTime,
                                   Remarks = s.ShiftRemarks
                               }).ToListAsync();
            return shift;
        }

        public async Task<List<HRMSBusinessUnitVM>> GetBusinessUnit()
        {
            var bu = await (from b in _db.HRMS_BusinessUnit
                            where b.Active == true
                            select new HRMSBusinessUnitVM
                            {
                                ID = b.ID,
                                Name = b.Name,
                                BusinessUnitRemarks = b.Description,
                                Code = b.Code
                            }).ToListAsync();
            return bu;
        }

        public async Task<VMLeaveApplication> AddLeaveApplication(VMLeaveApplication VMLA)
        {
            HRMS_LeaveApplication HLA = new HRMS_LeaveApplication();
            HLA.HRMS_LeaveFK = VMLA.LeaveTypeId;
            HLA.LeavePaidType = VMLA.LeavePaidType;
            HLA.From = VMLA.From;
            HLA.To = VMLA.To;
            HLA.TotalDays = VMLA.TotalDays;
            HLA.Purpose = VMLA.Purpose;
            HLA.StayDuringLeave = VMLA.StayDuringLeave;
            HLA.ReplacementEmployeeId = VMLA.ReplacementEmployeeId;
            HLA.HRMS_EmployeeManagerFK = VMLA.ManagerId;
            HLA.HRMS_EmployeeApprovedByFK = VMLA.ApprovedById;
            HLA.HRMS_EmployeeSectionInchargeFK = VMLA.SectionInchargeId;
            HLA.HRMS_EmployeeFK = VMLA.EmployeeId;

            _db.HRMS_LeaveApplication.Add(HLA);
            await _db.SaveChangesAsync();

            return VMLA;
        }

        public async Task<IQueryable<LeaveDays>> getLeaveApplicationList(VMLeaveApplication VMLA)
        {
            LeaveDays ld = new LeaveDays();
            var Llist = await (from LA in _db.HRMS_LeaveApplication
                               join em in _db.HRMS_Employee on LA.HRMS_EmployeeFK equals em.ID into abc_join
                               from em in abc_join.DefaultIfEmpty()
                               join s in _db.HRMS_Section on em.HRMS_SectionFK equals s.ID
                               join dp in _db.User_Department on em.User_DepartmentFK equals dp.ID into dp_join
                               from dp in dp_join.DefaultIfEmpty()
                               join u in _db.HRMS_Unit on s.HRMS_UnitFK equals u.ID
                               join bu in _db.HRMS_BusinessUnit on u.HRMS_BusinessUnitFK equals bu.ID
                               join l in _db.HRMS_Leave on LA.HRMS_LeaveFK equals l.ID into l_join
                               from l in l_join.DefaultIfEmpty()
                               where LA.Active == true
                               //&& (VMLA.BU_Id > 0 || VMLA.Dept_ID > 0 || VMLA.Unit_ID > 0 || VMLA.Section_ID > 0 || VMLA.EmployeeId > 0 || (VMLA.From != null && VMLA.To != null) || VMLA.LeaveStatus > 0)
                               //&& (VMLA.BU_Id == 0 || bu.ID == VMLA.BU_Id)
                               //&& (VMLA.Dept_ID == 0 || dp.ID == VMLA.Dept_ID)
                               //&& (VMLA.Unit_ID == 0 || u.ID == VMLA.Unit_ID)
                               //&& (VMLA.Section_ID == 0 || s.ID == VMLA.Section_ID)
                               //&& (VMLA.EmployeeId == 0 || LA.HRMS_EmployeeFK == VMLA.EmployeeId)
                               //&& (VMLA.From == null && VMLA == null || LA.Time >= VMLA.From && LA.Time <= VMLA.To)
                               //&& (VMLA.LeaveStatus == 0 || LA.LeaveStatus == VMLA.LeaveStatus)
                               orderby LA.Time descending
                               select new LeaveDays
                               {
                                   ID = LA.ID,
                                   eId = em.ID,
                                   EmployeeIdentity = em.EmployeeIdentity,
                                   Name = em.Name,
                                   From = LA.Time,
                                   //To = LA.Time,
                                   TotalDays = LA.TotalDays,
                                   DeptStatus = LA.LeaveDeptStatus,
                                   HRStatus = LA.LeaveStatus,
                                   LeaveType = LA.HRMS_LeaveFK,
                                   leaveName = (EnumLeaveType)LA.HRMS_Leave.LeaveType,
                                   //for Query Property
                                   BU_Id = u.HRMS_BusinessUnitFK,
                                   Unit_ID = s.HRMS_UnitFK,
                                   Section_ID = em.HRMS_SectionFK,
                                   Dept_ID = em.User_DepartmentFK,
                                   EmployeeId = LA.HRMS_EmployeeFK
                               }).ToListAsync();

            var leaveList = Llist.AsQueryable();

            if (VMLA.BU_Id == 0 && VMLA.EmployeeId == null && VMLA.Unit_ID == 0 && VMLA.Dept_ID == 0 && VMLA.Section_ID == 0 && VMLA.LeaveStatus == 0 && VMLA.From == null && VMLA.To == null)
            {
                leaveList = leaveList.Where(e => e.BU_Id == VMLA.BU_Id && e.From >= VMLA.From && e.From <= VMLA.To);

                return leaveList;
            }
            if (VMLA.BU_Id != 0 && VMLA.EmployeeId == null && VMLA.Unit_ID == 0 && VMLA.Dept_ID == 0 && VMLA.Section_ID == 0 && VMLA.LeaveStatus == 0 && VMLA.From != null && VMLA.To != null)
            {
                leaveList = leaveList.Where(e => e.BU_Id == VMLA.BU_Id && e.From >= VMLA.From && e.From <= VMLA.To);

                return leaveList;
            }
            if (VMLA.BU_Id != 0 && VMLA.EmployeeId == null && VMLA.Unit_ID == 0 && VMLA.Dept_ID == 0 && VMLA.Section_ID == 0 && VMLA.LeaveStatus != 0 && VMLA.From != null && VMLA.To != null)
            {
                leaveList = leaveList.Where(e => e.BU_Id == VMLA.BU_Id && e.HRStatus == VMLA.LeaveStatus && e.From >= VMLA.From && e.From <= VMLA.To);
                return leaveList;
            }
            else if (VMLA.BU_Id != 0 && VMLA.EmployeeId != null && VMLA.Unit_ID == 0 && VMLA.Dept_ID == 0 && VMLA.Section_ID == 0 && VMLA.LeaveStatus == 0 && VMLA.From != null && VMLA.To != null)
            {
                leaveList = leaveList.Where(e => e.BU_Id == VMLA.BU_Id && e.EmployeeId == VMLA.EmployeeId && e.From >= VMLA.From && e.From <= VMLA.To);

                return leaveList;
            }
            else if (VMLA.BU_Id != 0  && VMLA.Unit_ID == 0 && VMLA.Dept_ID == 0 && VMLA.Section_ID == 0 && VMLA.EmployeeId != null && VMLA.LeaveStatus != 0 && VMLA.From != null && VMLA.To != null)
            {
                leaveList = leaveList.Where(e => e.BU_Id == VMLA.BU_Id && e.EmployeeId == VMLA.EmployeeId && e.HRStatus == VMLA.LeaveStatus && e.From >= VMLA.From && e.From <= VMLA.To);

                return leaveList;
            }
            else if (VMLA.BU_Id != 0 && VMLA.Unit_ID != 0 && VMLA.Section_ID == 0 && VMLA.Dept_ID == 0 && VMLA.EmployeeId == null && VMLA.LeaveStatus == 0 && VMLA.From != null && VMLA.To != null)
            {
                leaveList = leaveList.Where(e => e.BU_Id == VMLA.BU_Id && e.Unit_ID == VMLA.Unit_ID && e.From >= VMLA.From && e.From <= VMLA.To);

                return leaveList;
            }
            else if (VMLA.BU_Id != 0 && VMLA.Unit_ID != 0 && VMLA.Section_ID == 0 && VMLA.Dept_ID != 0  && VMLA.EmployeeId == null  && VMLA.LeaveStatus == 0 && VMLA.From != null && VMLA.To != null)
            {
                leaveList = leaveList.Where(e => e.BU_Id == VMLA.BU_Id && e.Unit_ID == VMLA.Unit_ID && e.Dept_ID == VMLA.Dept_ID && e.From >= VMLA.From && e.From <= VMLA.To);

                return leaveList;
            }
            else if (VMLA.BU_Id != 0 && VMLA.Unit_ID != 0 && VMLA.Section_ID == 0 && VMLA.Dept_ID != 0  && VMLA.EmployeeId != null && VMLA.LeaveStatus == 0 && VMLA.From != null && VMLA.To != null)
            {
                leaveList = leaveList.Where(e => e.BU_Id == VMLA.BU_Id && e.Unit_ID == VMLA.Unit_ID && e.Dept_ID == VMLA.Dept_ID  && e.EmployeeId == VMLA.EmployeeId && e.From >= VMLA.From && e.From <= VMLA.To);

                return leaveList;

            }
            else if (VMLA.BU_Id != 0 && VMLA.Unit_ID != 0 && VMLA.Dept_ID != 0 && VMLA.Section_ID != 0 && VMLA.EmployeeId == null && VMLA.LeaveStatus != 0 && VMLA.From != null && VMLA.To != null)
            {
                leaveList = leaveList.Where(e => e.BU_Id == VMLA.BU_Id && e.Unit_ID == VMLA.Unit_ID && e.Dept_ID == VMLA.Dept_ID && e.Section_ID == VMLA.Section_ID && e.From >= VMLA.From && e.From <= VMLA.To);

                return leaveList;
            }
            else if (VMLA.BU_Id != 0 && VMLA.Unit_ID != 0 && VMLA.Dept_ID != 0 && VMLA.Section_ID != 0 && VMLA.EmployeeId != null && VMLA.LeaveStatus != 0 && VMLA.From != null && VMLA.To != null)
            {
                leaveList = leaveList.Where(e => e.BU_Id == VMLA.BU_Id && e.Unit_ID == VMLA.Unit_ID && e.Dept_ID == VMLA.Dept_ID && e.Section_ID == VMLA.Section_ID &&  e.EmployeeId == VMLA.EmployeeId && e.HRStatus == VMLA.LeaveStatus && e.From >= VMLA.From && e.From <= VMLA.To);
                return leaveList;
            }

            return null;
        }

        public async Task<VMLeave> AddAssignLeave(VMLeave VML)
        {
            foreach (var i in VML.Emplists)
            {
                if (i.IsLeaveAssigned == true)
                {
                    HRMS_Employee isAssignedEmp = _db.HRMS_Employee.Find(i.ID);
                    HRMS_Leave _leave = _db.HRMS_Leave.Find(VML.LeaveTypeId);
                    if (isAssignedEmp.IsLeaveAssigned != true)
                    {
                        isAssignedEmp.IsLeaveAssigned = true;
                        _db.HRMS_Employee.Update(isAssignedEmp);
                        await _db.SaveChangesAsync();

                    }
                    HRMS_EmployeeLeaveAssign ELA = _db.HRMS_EmployeeLeaveAssign.Where(e => e.HRMS_EmployeeFK == i.ID && e.Year == int.Parse(VML.year) && e.Active).FirstOrDefault();
                    if (ELA != null)
                    {
                        if (ELA.Year != int.Parse(VML.year) && ELA.Year < int.Parse(VML.year))
                        {
                            ELA.Active = false;
                            _db.HRMS_EmployeeLeaveAssign.Update(ELA);

                            HRMS_EmployeeLeaveAssign HELA = new HRMS_EmployeeLeaveAssign();
                            HELA.HRMS_EmployeeFK = i.ID;
                            HELA.HRMS_LeaveFK = VML.LeaveTypeId;

                            if (_leave.LeaveType == 1)
                            {
                                HELA.CasualLeaveDays = VML.LeaveDays;
                            }
                            else if (_leave.LeaveType == 2)
                            {
                                HELA.AnnualLeaveDays = VML.LeaveDays;
                            }
                            else if (_leave.LeaveType == 3)
                            {
                                HELA.SickLeaveDays = VML.LeaveDays;
                            }
                            else if (_leave.LeaveType == 4)
                            {
                                HELA.MaternityLeaveDays = VML.LeaveDays;
                            }
                            else if (_leave.LeaveType == 5)
                            {
                                HELA.EarnLeaveDays = VML.LeaveDays;
                            }

                            HELA.Year = int.Parse(VML.year);
                            _db.HRMS_EmployeeLeaveAssign.Add(HELA);
                        }
                        else
                        {
                            ELA.HRMS_EmployeeFK = i.ID;
                            ELA.HRMS_LeaveFK = VML.LeaveTypeId;
                            if (_leave.LeaveType == 1)
                            {
                                ELA.CasualLeaveDays = VML.LeaveDays;
                            }
                            else if (_leave.LeaveType == 2)
                            {
                                ELA.AnnualLeaveDays = VML.LeaveDays;
                            }
                            else if (_leave.LeaveType == 3)
                            {
                                ELA.SickLeaveDays = VML.LeaveDays;
                            }
                            else if (_leave.LeaveType == 4)
                            {
                                ELA.MaternityLeaveDays = VML.LeaveDays;
                            }
                            else if (_leave.LeaveType == 5)
                            {
                                ELA.EarnLeaveDays = VML.LeaveDays;
                            }

                            ELA.Year = int.Parse(VML.year);

                            _db.HRMS_EmployeeLeaveAssign.Update(ELA);
                        }
                        await _db.SaveChangesAsync();
                    }
                    else
                    {
                        HRMS_EmployeeLeaveAssign HELA = new HRMS_EmployeeLeaveAssign();
                        HELA.HRMS_EmployeeFK = i.ID;
                        HELA.HRMS_LeaveFK = VML.LeaveTypeId;

                        if (_leave.LeaveType == 1)
                        {
                            HELA.CasualLeaveDays = VML.LeaveDays;
                        }
                        else if (_leave.LeaveType == 2)
                        {
                            HELA.AnnualLeaveDays = VML.LeaveDays;
                        }
                        else if (_leave.LeaveType == 3)
                        {
                            HELA.SickLeaveDays = VML.LeaveDays;
                        }
                        else if (_leave.LeaveType == 4)
                        {
                            HELA.MaternityLeaveDays = VML.LeaveDays;
                        }
                        else if (_leave.LeaveType == 5)
                        {
                            HELA.EarnLeaveDays = VML.LeaveDays;
                        }

                        HELA.Year = int.Parse(VML.year);

                        _db.HRMS_EmployeeLeaveAssign.Add(HELA);
                        await _db.SaveChangesAsync();
                    }
                }
            }

            return VML;
        }

        public async Task<IQueryable<Emplist>> getAssinedLeaveList(VMLeave VML)
        {
            var emp = await (from LA in _db.HRMS_EmployeeLeaveAssign
                             join em in _db.HRMS_Employee on LA.HRMS_EmployeeFK equals em.ID into abc
                             from em in abc.DefaultIfEmpty()
                             join d in _db.HRMS_Designation on em.HRMS_DesignationFK equals d.ID into bcd
                             from d in bcd.DefaultIfEmpty()

                             join s in _db.HRMS_Section on em.HRMS_SectionFK equals s.ID into efg
                             from s in efg.DefaultIfEmpty()
                             join dp in _db.User_Department on em.User_DepartmentFK equals dp.ID into cde
                             from dp in cde.DefaultIfEmpty()
                             join u in _db.HRMS_Unit on s.HRMS_UnitFK equals u.ID into def
                             from u in def.DefaultIfEmpty()
                             join b in _db.HRMS_BusinessUnit on u.HRMS_BusinessUnitFK equals b.ID into fgh
                             from b in fgh.DefaultIfEmpty()
                             where em.IsLeaveAssigned == true && em.Active
                                           //&& (VML.BU_Id > 0 || VML.Dept_ID > 0 || VML.Unit_ID > 0 || VML.Section_ID > 0 && VML.EmployeeId > 0)
                                           //&& (VML.EmployeeId == 0 || b.ID == VML.BU_Id && em.ID == VML.EmployeeId)
                                           //&& (VML.BU_Id == 0 || b.ID == VML.BU_Id)
                                           //&& (VML.Unit_ID == 0 || b.ID == VML.BU_Id && u.ID == VML.Unit_ID)
                                           //&& (VML.Dept_ID == 0 || b.ID == VML.BU_Id && u.ID == VML.Unit_ID && dp.ID == VML.Dept_ID)
                                           //&& (VML.Section_ID == 0 || b.ID == VML.BU_Id && u.ID == VML.Unit_ID && dp.ID == VML.Dept_ID && s.ID == VML.Section_ID)
                                           //&& (VML.EmployeeId == 0 || b.ID == VML.BU_Id && u.ID == VML.Unit_ID && dp.ID == VML.Dept_ID && s.ID == VML.Section_ID && em.ID == VML.Section_ID)
                             select new Emplist
                             {
                                 ID = em.ID,
                                 EmployeeIdentity = em.EmployeeIdentity,
                                 Name = em.Name,
                                 Designation = d.Name,
                                 Unit = u.Name,
                                 Section = s.Name,
                                 BusinessUnit = b.Name,
                                 Department = dp.Name,
                                 IsLeaveAssigned = em.IsLeaveAssigned,
                                 AnnualLeaveDays = LA.AnnualLeaveDays,
                                 TakenAnnualLeaveDays = LA.TakenAnnualLeaveDays,
                                 SickLeaveDays = LA.SickLeaveDays,
                                 TakenSickLeaveDays = LA.TakenSickLeaveDays,
                                 CasualLeaveDays = LA.CasualLeaveDays,
                                 TakenCasualLeaveDays = LA.TakenCasualLeaveDays,

                                 //for Query Property
                                 BU_Id = u.HRMS_BusinessUnitFK,
                                 Unit_ID = s.HRMS_UnitFK,
                                 Section_ID = em.HRMS_SectionFK,
                                 Dept_ID = em.User_DepartmentFK,
                                 year = Convert.ToString(LA.Year),
                                 LeaveTypeId = LA.HRMS_LeaveFK

                             }).ToListAsync();

            var empList = emp.AsQueryable();

            if (VML.BU_Id != 0 && VML.EmployeeId == 0 && VML.Unit_ID == 0 && VML.Dept_ID == 0 && VML.Section_ID == 0 && VML.year == null && VML.LeaveTypeId == 0)
            {
                empList = empList.Where(e => e.BU_Id == VML.BU_Id);

                return empList;
            }
            else if (VML.BU_Id != 0 && VML.EmployeeId != 0 && VML.Unit_ID == 0 && VML.Dept_ID == 0 && VML.Section_ID == 0 && VML.year == null && VML.LeaveTypeId == 0)
            {
                empList = empList.Where(e => e.BU_Id == VML.BU_Id && e.ID == VML.EmployeeId);

                return empList;
            }
            else if (VML.BU_Id != 0 && VML.EmployeeId == 0 && VML.Unit_ID == 0 && VML.Dept_ID == 0 && VML.Section_ID == 0 && VML.year != null && VML.LeaveTypeId == 0)
            {
                empList = empList.Where(e => e.BU_Id == VML.BU_Id && e.year == VML.year.Trim());

                return empList;
            }
            else if (VML.BU_Id != 0 && VML.EmployeeId != 0 && VML.Unit_ID == 0 && VML.Dept_ID == 0 && VML.Section_ID == 0 && VML.year != null && VML.LeaveTypeId == 0)
            {
                var year = VML.year.Trim();
                empList = empList.Where(e => e.BU_Id == VML.BU_Id && e.ID == VML.EmployeeId && e.year == year);

                return empList;
            }
            else if (VML.BU_Id != 0 && VML.Unit_ID != 0 && VML.Dept_ID == 0 && VML.Section_ID == 0 && VML.year == null && VML.LeaveTypeId == 0)
            {
                empList = empList.Where(e => e.BU_Id == VML.BU_Id && e.Unit_ID == VML.Unit_ID);

                return empList;
            }
            else if (VML.BU_Id != 0 && VML.Unit_ID != 0 && VML.Dept_ID != 0 && VML.Section_ID == 0 && VML.year == null && VML.LeaveTypeId == 0)
            {
                empList = empList.Where(e => e.BU_Id == VML.BU_Id && e.Unit_ID == VML.Unit_ID && e.Dept_ID == VML.Dept_ID);

                return empList;

            }
            else if (VML.BU_Id != 0 && VML.Unit_ID != 0 && VML.Dept_ID != 0 && VML.Section_ID != 0 && VML.year == null && VML.LeaveTypeId == 0)
            {
                empList = empList.Where(e => e.BU_Id == VML.BU_Id && e.Unit_ID == VML.Unit_ID && e.Dept_ID == VML.Dept_ID && e.Section_ID == VML.Section_ID);

                return empList;
            }
            else if (VML.BU_Id != 0 && VML.Unit_ID != 0 && VML.Dept_ID != 0 && VML.Section_ID != 0 && VML.year != null && VML.LeaveTypeId == 0)
            {
                var year = VML.year.Trim();
                empList = empList.Where(e => e.BU_Id == VML.BU_Id && e.Unit_ID == VML.Unit_ID && e.Dept_ID == VML.Dept_ID && e.Section_ID == VML.Section_ID && e.year == VML.year);

                return empList;
            }
            else if (VML.BU_Id != 0 && VML.Unit_ID != 0 && VML.Dept_ID != 0 && VML.Section_ID != 0 && VML.year != null && VML.LeaveTypeId != 0)
            {
                var year = VML.year.Trim();
                empList = empList.Where(e => e.BU_Id == VML.BU_Id && e.Unit_ID == VML.Unit_ID && e.Dept_ID == VML.Dept_ID && e.Section_ID == VML.Section_ID && e.year == VML.year && e.LeaveTypeId == VML.LeaveTypeId);

                return empList;
            }
            else if (VML.BU_Id != 0 && VML.Unit_ID != 0 && VML.Dept_ID != 0 && VML.Section_ID != 0 && VML.year != null && VML.LeaveTypeId != 0 && VML.EmployeeId == 0)
            {
                var year = VML.year.Trim();
                empList = empList.Where(e => e.BU_Id == VML.BU_Id && e.Unit_ID == VML.Unit_ID && e.Dept_ID == VML.Dept_ID && e.Section_ID == VML.Section_ID && e.year == year && e.LeaveTypeId == VML.LeaveTypeId && e.ID == VML.EmployeeId);

                return empList;
            }

            return empList;
        }

        public async Task<object> SetLeaveStatusUpdate(int id, bool status, int eId, int TotalDays, int LeaveType)
        {
            HRMS_LeaveApplication hla = _db.HRMS_LeaveApplication.Include(l => l.HRMS_Leave).Where(l => l.ID == id).FirstOrDefault();

            HRMS_EmployeeLeaveAssign hela = _db.HRMS_EmployeeLeaveAssign.Where(e => e.HRMS_EmployeeFK == eId).FirstOrDefault();

            if (hla != null)
            {
                var lStatus = hla.LeaveStatus;
                var dStatus = hla.LeaveDeptStatus;

                if (status != false)
                {
                    if (dStatus != 5)
                    {
                        hla.LeaveDeptStatus = dStatus + 1;
                        hla.LeaveStatus = 1;
                        if (hla.LeaveDeptStatus == 4)
                        {
                            hla.LeaveDeptStatus = 5;
                            hla.LeaveStatus = 2;
                            var a = hla.HRMS_Leave.LeaveType;
                            if (hla.HRMS_Leave.LeaveType == 1)
                            {
                                if (hela.TakenCasualLeaveDays != null)
                                {
                                    hela.TakenCasualLeaveDays += TotalDays;
                                }
                                else
                                {
                                    hela.TakenCasualLeaveDays = TotalDays;
                                }
                            }
                            else if (hla.HRMS_Leave.LeaveType == 2)
                            {
                                if (hela.TakenAnnualLeaveDays != null)
                                {
                                    hela.TakenAnnualLeaveDays += TotalDays;
                                }
                                else
                                {
                                    hela.TakenAnnualLeaveDays = TotalDays;
                                }
                            }
                            else if (hla.HRMS_Leave.LeaveType == 3)
                            {
                                if (hela.TakenSickLeaveDays != null)
                                {
                                    hela.TakenSickLeaveDays += TotalDays;
                                }
                                else
                                {
                                    hela.TakenSickLeaveDays = TotalDays;
                                }
                            }
                            else if (hla.HRMS_Leave.LeaveType == 4)
                            {
                                if (hela.TakenMaternityLeaveDays != null)
                                {
                                    hela.TakenMaternityLeaveDays += TotalDays;
                                }
                                else
                                {
                                    hela.TakenMaternityLeaveDays = TotalDays;
                                }
                            }
                            else
                            {
                                if (hela.TakenEarnLeaveDays != null)
                                {
                                    hela.TakenEarnLeaveDays += TotalDays;
                                }
                                else
                                {
                                    hela.TakenEarnLeaveDays = TotalDays;
                                }

                            }
                        }
                        _db.HRMS_LeaveApplication.Update(hla);
                        _db.HRMS_EmployeeLeaveAssign.Update(hela);
                        await _db.SaveChangesAsync();
                    }
                }
                else
                {
                    hla.LeaveStatus = 3;
                    hla.LeaveDeptStatus = 6;
                    _db.HRMS_LeaveApplication.Update(hla);
                    await _db.SaveChangesAsync();
                }
            }
            else if (status != false)
            {
                hla.LeaveStatus = 1;
                hla.LeaveDeptStatus = 1;
                _db.HRMS_LeaveApplication.Update(hla);
                await _db.SaveChangesAsync();
            }

            await _db.SaveChangesAsync();
            return null;
        }
        #endregion

        #region Weekend Assign
        public IQueryable<Emplist> GetEmployeesForWeekend(VMWeekendDay VMWD)
        {
            var emp = (from em in _db.HRMS_Employee
                       join s in _db.HRMS_Section on em.HRMS_SectionFK equals s.ID into s_Join
                       from s in s_Join.DefaultIfEmpty()
                       join dp in _db.User_Department on em.User_DepartmentFK equals dp.ID into dp_Join
                       from dp in dp_Join.DefaultIfEmpty()
                       join u in _db.HRMS_Unit on s.HRMS_UnitFK equals u.ID into u_Join
                       from u in u_Join.DefaultIfEmpty()
                       join bu in _db.HRMS_BusinessUnit on u.HRMS_BusinessUnitFK equals bu.ID into bu_Join
                       from bu in bu_Join.DefaultIfEmpty()
                       join wa in (_db.HRMS_EmployeeWeekendAssign.Where(wa=>wa.Active).ToList()) on em.ID equals wa.HRMS_EmployeeFK into wa_Join
                       from wa in wa_Join.DefaultIfEmpty()
                       where em.Active
                       select new Emplist
                       {
                           ID = em.ID,
                           EmployeeIdentity = em.EmployeeIdentity,
                           Name = em.Name,
                           Designation = em.HRMS_Designation.Name,
                           BusinessUnit = bu.Name,
                           Department = dp.Name,
                           Unit = u.Name,
                           Section = s.Name,
                           WeekendDays = wa != null ? (_db.HRMS_EmployeeWeekend.Where(w => w.HRMS_EmployeeWeekendAssignFK == wa.ID).Select(s => s.WeekendDay).ToList()) : null,
                           //for Query Property
                           BU_Id = bu.ID,
                           Unit_ID = u.ID,
                           Dept_ID = dp.ID,
                           Section_ID = em.HRMS_SectionFK,
                           IsWeekendAssigned = em.IsWeekendAssigned

                       }).ToList();
            var empList = emp.AsQueryable();
            if (VMWD.BU_Id != 0 && VMWD.Unit_ID == 0 && VMWD.Dept_ID == 0 && VMWD.Section_ID == 0)
            {
                empList = empList.Where(e => e.BU_Id == VMWD.BU_Id);
                return empList;
            }
            else if (VMWD.BU_Id != 0 && VMWD.Unit_ID != 0 && VMWD.Dept_ID == 0 && VMWD.Section_ID == 0)
            {
                empList = empList.Where(e => e.BU_Id == VMWD.BU_Id && e.Unit_ID == VMWD.Unit_ID);

                return empList;
            }
            else if (VMWD.BU_Id != 0 && VMWD.Unit_ID != 0 && VMWD.Dept_ID != 0 && VMWD.Section_ID == 0)
            {
                empList = empList.Where(e => e.BU_Id == VMWD.BU_Id && e.Unit_ID == VMWD.Unit_ID && e.Dept_ID == VMWD.Dept_ID);

                return empList;

            }
            else if (VMWD.BU_Id != 0 && VMWD.Unit_ID != 0 && VMWD.Dept_ID != 0 && VMWD.Section_ID != 0)
            {
                empList = empList.Where(e => e.BU_Id == VMWD.BU_Id && e.Unit_ID == VMWD.Unit_ID && e.Dept_ID == VMWD.Dept_ID && e.Section_ID == VMWD.Section_ID);

                return empList;
            }
            return empList;
        }
        public IQueryable<Emplist> GetEmployeesForWeekendFilter(VMWeekendDay VMWD)
        {
            var emp = (from ewa in _db.HRMS_EmployeeWeekendAssign
                       join em in _db.HRMS_Employee on ewa.HRMS_EmployeeFK equals em.ID into abc
                       from em in abc.DefaultIfEmpty()
                       join s in _db.HRMS_Section on em.HRMS_SectionFK equals s.ID into s_Join
                       from s in s_Join.DefaultIfEmpty()
                       join dp in _db.User_Department on em.User_DepartmentFK equals dp.ID into dp_Join
                       from dp in dp_Join.DefaultIfEmpty()
                       join u in _db.HRMS_Unit on s.HRMS_UnitFK equals u.ID into u_Join
                       from u in u_Join.DefaultIfEmpty()
                       join bu in _db.HRMS_BusinessUnit on u.HRMS_BusinessUnitFK equals bu.ID into bu_Join
                       from bu in bu_Join.DefaultIfEmpty()
                       where ewa.Active
                       select new Emplist
                       {
                           ID = em.ID,
                           EmployeeIdentity = em.EmployeeIdentity,
                           Name = em.Name,
                           Designation = em.HRMS_Designation.Name,
                           BusinessUnit = bu.Name,
                           Department = dp.Name,
                           Unit = u.Name,
                           Section = em.HRMS_Section.Name,
                           WeekendDays = _db.HRMS_EmployeeWeekend.Where(w => w.HRMS_EmployeeWeekendAssignFK == ewa.ID).Select(s => s.WeekendDay).ToList(),
                           //for Query Property
                           BU_Id = u.HRMS_BusinessUnitFK,
                           Unit_ID = s.HRMS_UnitFK,
                           Section_ID = em.HRMS_SectionFK,
                           Dept_ID = em.User_DepartmentFK,
                           IsWeekendAssigned = em.IsWeekendAssigned
                       }).ToList();
            var empList = emp.AsQueryable();
            if (VMWD.BU_Id != 0 && VMWD.Unit_ID == 0 && VMWD.Dept_ID == 0 && VMWD.Section_ID == 0)
            {
                empList = empList.Where(e => e.BU_Id == VMWD.BU_Id);
                return empList;
            }
            else if (VMWD.BU_Id != 0 && VMWD.Unit_ID != 0 && VMWD.Dept_ID == 0 && VMWD.Section_ID == 0)
            {
                empList = empList.Where(e => e.BU_Id == VMWD.BU_Id && e.Unit_ID == VMWD.Unit_ID);

                return empList;
            }
            else if (VMWD.BU_Id != 0 && VMWD.Unit_ID != 0 && VMWD.Dept_ID != 0 && VMWD.Section_ID == 0)
            {
                empList = empList.Where(e => e.BU_Id == VMWD.BU_Id && e.Unit_ID == VMWD.Unit_ID && e.Dept_ID == VMWD.Dept_ID);

                return empList;

            }
            else if (VMWD.BU_Id != 0 && VMWD.Unit_ID != 0 && VMWD.Dept_ID != 0 && VMWD.Section_ID != 0)
            {
                empList = empList.Where(e => e.BU_Id == VMWD.BU_Id && e.Unit_ID == VMWD.Unit_ID && e.Dept_ID == VMWD.Dept_ID && e.Section_ID == VMWD.Section_ID);

                return empList;
            }
            return null;
        }
        public async Task<VMWeekendDay> AddAssignWeekend(VMWeekendDay VMWD)
        {
            foreach (var i in VMWD.Emplists)
            {
                if (i.IsWeekendAssigned == true)
                {
                    //var j = 0;
                    HRMS_Employee isAssignedEmp = _db.HRMS_Employee.Find(i.ID);
                    if (isAssignedEmp.IsWeekendAssigned != true)
                    {
                        isAssignedEmp.IsWeekendAssigned = true;
                        _db.HRMS_Employee.Update(isAssignedEmp);
                        await _db.SaveChangesAsync();

                    }

                    HRMS_EmployeeWeekendAssign ewa = _db.HRMS_EmployeeWeekendAssign.Where(e => e.HRMS_EmployeeFK == isAssignedEmp.ID).LastOrDefault();
                    if (ewa != null)
                    {
                        ewa.Active = false;
                        _db.HRMS_EmployeeWeekendAssign.Update(ewa);
                        await _db.SaveChangesAsync();
                    }
                    HRMS_EmployeeWeekendAssign HEWA = new HRMS_EmployeeWeekendAssign();
                    HEWA.HRMS_EmployeeFK = i.ID;
                    _db.HRMS_EmployeeWeekendAssign.Add(HEWA);
                    await _db.SaveChangesAsync();
                    foreach (var e in VMWD.WeekendDay)
                    {
                        HRMS_EmployeeWeekend HEW = new HRMS_EmployeeWeekend();
                        HEW.HRMS_EmployeeWeekendAssignFK = HEWA.ID;
                        HEW.WeekendDay = Convert.ToString(e);
                        _db.HRMS_EmployeeWeekend.Add(HEW);
                        await _db.SaveChangesAsync();
                    }

                    //else
                    //{
                    //    HRMS_EmployeeWeekendAssign EWA = _db.HRMS_EmployeeWeekendAssign.Include(ew=>ew.HRMS_EmployeeWeekends).Where(e => e.HRMS_EmployeeFK == i.ID).FirstOrDefault();
                    //    if (EWA != null)
                    //    {
                    //        EWA.HRMS_EmployeeFK = i.ID;
                    //        var HEW = _db.HRMS_EmployeeWeekend.Where(e => e.HRMS_EmployeeWeekendAssignFK == EWA.ID).ToList();
                    //        foreach(var h in HEW)
                    //        {
                    //            _db.HRMS_EmployeeWeekend.Remove(h);
                    //        }
                    //        _db.HRMS_EmployeeWeekendAssign.Update(EWA);
                    //        await _db.SaveChangesAsync();
                    //        foreach (var e in VMWD.WeekendDay)
                    //        {
                    //            //HRMS_EmployeeWeekend HEW = _db.HRMS_EmployeeWeekend.Where(a => a.WeekendDay == Convert.ToString(e) && a.HRMS_EmployeeWeekendAssignFK == EWA.ID).FirstOrDefault();
                    //            //if (HEW != null)
                    //            //{
                    //            //    HEW.WeekendDay = Convert.ToString(e);
                    //            //    _db.HRMS_EmployeeWeekend.Update(HEW);
                    //            //    await _db.SaveChangesAsync();
                    //            //}
                    //            //else
                    //            //{
                    //            //    HRMS_EmployeeWeekend EW = new HRMS_EmployeeWeekend();
                    //            //    EW.HRMS_EmployeeWeekendAssignFK = EWA.ID;
                    //            //    EW.WeekendDay = Convert.ToString(e);
                    //            //    _db.HRMS_EmployeeWeekend.Add(EW);
                    //            //    await _db.SaveChangesAsync();
                    //            //}

                    //            HRMS_EmployeeWeekend EW = new HRMS_EmployeeWeekend();
                    //            EW.HRMS_EmployeeWeekendAssignFK = EWA.ID;
                    //            EW.WeekendDay = Convert.ToString(e);
                    //            _db.HRMS_EmployeeWeekend.Add(EW);
                    //            await _db.SaveChangesAsync();
                    //        }
                    //    }
                    //    else
                    //    {
                    //        HRMS_EmployeeWeekendAssign HEWA = new HRMS_EmployeeWeekendAssign();
                    //        HEWA.HRMS_EmployeeFK = i.ID;
                    //        _db.HRMS_EmployeeWeekendAssign.Add(HEWA);
                    //        await _db.SaveChangesAsync();
                    //        foreach (var e in VMWD.WeekendDay)
                    //        {
                    //            HRMS_EmployeeWeekend HEW = new HRMS_EmployeeWeekend();
                    //            HEW.HRMS_EmployeeWeekendAssignFK = HEWA.ID;
                    //            HEW.WeekendDay = Convert.ToString(e);
                    //            _db.HRMS_EmployeeWeekend.Add(HEW);
                    //            await _db.SaveChangesAsync();
                    //        }
                    //    }
                    //}
                }

                //else
                //{
                //    HRMS_Employee isAssignedEmp = _db.HRMS_Employee.Find(i.ID);
                //    if (isAssignedEmp.IsWeekendAssigned == true)
                //    {
                //        isAssignedEmp.IsWeekendAssigned = false;
                //        _db.HRMS_Employee.Update(isAssignedEmp);

                //        HRMS_EmployeeWeekendAssign HEWA = _db.HRMS_EmployeeWeekendAssign.Include(e => e.HRMS_EmployeeWeekends).Where(e => e.HRMS_EmployeeFK == i.ID).FirstOrDefault();
                //        _db.HRMS_EmployeeWeekendAssign.Remove(HEWA);
                //        await _db.SaveChangesAsync();
                //    }
                //}
            }
            return VMWD;
        }
        #endregion

        #region Card Generation

        public async Task<IList<Emplist>> GetEmployeesForCardGeneration(HRMSEmployeesCardGenerationVM VMWD)
        {
            List<Emplist> emp = new List<Emplist>();
            if(VMWD.SectionId != 0 && VMWD.DesignationId != 0)
            {
                emp = await Task.Run(() => (from em in _db.HRMS_Employee
                                            join s in _db.HRMS_Section on em.HRMS_SectionFK equals s.ID into S_Join
                                            from s in S_Join.DefaultIfEmpty()
                                            join dp in _db.User_Department on em.User_DepartmentFK equals dp.ID into dp_Join
                                            from dp in dp_Join.DefaultIfEmpty()
                                            join u in _db.HRMS_Unit on s.HRMS_UnitFK equals u.ID into u_join
                                            from u in u_join.DefaultIfEmpty()
                                            join bu in _db.HRMS_BusinessUnit on u.HRMS_BusinessUnitFK equals bu.ID into bu_Join
                                            from bu in bu_Join.DefaultIfEmpty()
                                            where em.HRMS_SectionFK == VMWD.SectionId
                            && em.HRMS_DesignationFK == VMWD.DesignationId
                            && em.Active
                                            select new Emplist
                                            {
                                                ID = em.ID,
                                                EmployeeIdentity = em.EmployeeIdentity,
                                                Name = em.Name,
                                                Designation = em.HRMS_Designation.Name,
                                                BusinessUnit = bu.Name,
                                                Department = dp.Name,
                                                Unit = u.Name,
                                                Section = em.HRMS_Section.Name,
                                                //for Query Property
                                                BU_Id = u.HRMS_BusinessUnitFK,
                                                Unit_ID = s.HRMS_UnitFK,
                                                Section_ID = em.HRMS_SectionFK,
                                                Dept_ID = em.User_DepartmentFK,
                                                IsWeekendAssigned = em.IsWeekendAssigned

                                            }).ToListAsync());
            }
            else
            {
                emp = await Task.Run(() => (from em in _db.HRMS_Employee
                                            join s in _db.HRMS_Section on em.HRMS_SectionFK equals s.ID into S_Join
                                            from s in S_Join.DefaultIfEmpty()
                                            join dp in _db.User_Department on em.User_DepartmentFK equals dp.ID into dp_Join
                                            from dp in dp_Join.DefaultIfEmpty()
                                            join u in _db.HRMS_Unit on s.HRMS_UnitFK equals u.ID into u_join
                                            from u in u_join.DefaultIfEmpty()
                                            join bu in _db.HRMS_BusinessUnit on u.HRMS_BusinessUnitFK equals bu.ID into bu_Join
                                            from bu in bu_Join.DefaultIfEmpty()
                                            where em.HRMS_SectionFK == VMWD.SectionId
                            && em.Active
                                            select new Emplist
                                            {
                                                ID = em.ID,
                                                EmployeeIdentity = em.EmployeeIdentity,
                                                Name = em.Name,
                                                Designation = em.HRMS_Designation.Name,
                                                BusinessUnit = bu.Name,
                                                Department = dp.Name,
                                                Unit = u.Name,
                                                Section = em.HRMS_Section.Name,
                                                //for Query Property
                                                BU_Id = u.HRMS_BusinessUnitFK,
                                                Unit_ID = s.HRMS_UnitFK,
                                                Section_ID = em.HRMS_SectionFK,
                                                Dept_ID = em.User_DepartmentFK,
                                                IsWeekendAssigned = em.IsWeekendAssigned

                                            }).ToListAsync());
            }
            return emp;
        }
        public async Task<IList<HRMSEmployeeIdCardVM>> GenerateEmployeesIdCard(HRMSEmployeesCardGenerationVM VMWD)
        {
            if(VMWD.Emplists != null)
            {
                List<HRMSEmployeeIdCardVM> HEICVM = new List<HRMSEmployeeIdCardVM>();

                foreach (var i in VMWD.Emplists)
                {
                    if(i.IsWeekendAssigned == true)
                    {
                        var Elist = await Task.Run(() => (from e in _db.HRMS_Employee
                                                          join d in _db.HRMS_Designation on e.HRMS_DesignationFK equals d.ID into abc
                                                          from d in abc.DefaultIfEmpty()
                                                          join s in _db.HRMS_Section on e.HRMS_SectionFK equals s.ID into cde
                                                          from s in cde.DefaultIfEmpty()
                                                          join ei in _db.HRMS_EmployeeImage on e.ID equals ei.HRMS_EmployeeFK into ei_Join
                                                          from ei in ei_Join.DefaultIfEmpty()
                                                          where e.ID == i.ID
                                                          select new HRMSEmployeeIdCardVM
                                                          {
                                                              Name = e.Name,
                                                              ID = e.ID,
                                                              Designation = d.Name,
                                                              Section = s.Name,
                                                              BloodGroup = Convert.ToString((EnumBloodGroup)e.BloodGroup),
                                                              JoiningDate = e.JoiningDate,
                                                              IDCardIssueDate = VMWD.IDCardIssueDate,
                                                              IDCardExpireDate = VMWD.IDCardExpireDate,
                                                              NIDNo = e.NIDNo,
                                                              EmergencyPhoneNo = e.MobileNo,
                                                              PermanentAddress = e.PermanentAddress,
                                                              Image = ei.Image,
                                                              ImagePath = ei.Image != null ?  Convert.ToBase64String(ei.Image) : "Not in Image!",
                                                          }).FirstOrDefaultAsync());                        

                        HEICVM.Add(new HRMSEmployeeIdCardVM
                        {                            
                            Name = Elist.Name,
                            ID = Elist.ID,
                            Designation = Elist.Designation,
                            Section = Elist.Section,
                            BloodGroup = Elist.BloodGroup,
                            JoiningDate = Elist.JoiningDate,
                            IDCardIssueDate = Elist.IDCardIssueDate,
                            IDCardExpireDate = Elist.IDCardExpireDate,
                            NIDNo = Elist.NIDNo,
                            EmergencyPhoneNo = Elist.EmergencyPhoneNo,
                            PermanentAddress = Elist.PermanentAddress,
                            Image = Elist.Image,
                            ImagePath = "data:image/jpeg;base64, " + Elist.ImagePath
                        });
                    }
                }

                return HEICVM;
            }
            return null;
        }



        #endregion

        #region Earn Leave Calculation
        public async Task<IQueryable<VMEarnLeaveCalculationList>> GetEmployeesforEarnLeave(VMEarnLeaveCalculation VMELC, int id)
        {
            if (id == 0)
            {
                var emp = await (from e in _db.HRMS_Employee
                                 join d in _db.HRMS_Designation on e.HRMS_DesignationFK equals d.ID into d_Join
                                 from d in d_Join.DefaultIfEmpty()
                                 join s in _db.HRMS_Section on e.HRMS_SectionFK equals s.ID into s_Join
                                 from s in s_Join.DefaultIfEmpty()
                                 join dp in _db.User_Department on e.User_DepartmentFK equals dp.ID into dp_Join
                                 from dp in dp_Join.DefaultIfEmpty()
                                 join u in _db.HRMS_Unit on s.HRMS_UnitFK equals u.ID into u_Join
                                 from u in u_Join.DefaultIfEmpty()
                                 join bu in _db.HRMS_BusinessUnit on u.HRMS_BusinessUnitFK equals bu.ID into bu_Join
                                 from bu in bu_Join.DefaultIfEmpty()
                                 where s.ID == VMELC.Section_ID
                                 && e.Active
                                 select new VMEarnLeaveCalculationList
                                 {
                                     ID = e.ID,
                                     Name = e.Name,
                                     EmployeeIdentity = e.EmployeeIdentity,
                                     Department = dp.Name,
                                     Designation = d.Name,
                                     Section = s.Name,
                                     Unit = u.Name,
                                     JoiningDate = e.JoiningDate,
                                     TotalMonth = ((VMELC.EarnLeaveDate.Year - e.JoiningDate.Year) * 12) + VMELC.EarnLeaveDate.Month - e.JoiningDate.Month,
                                     WorkingDays = findDate(e.JoiningDate, VMELC.EarnLeaveDate, "wd"),
                                     Holidays = 0,
                                     Offdays = findDate(e.JoiningDate, VMELC.EarnLeaveDate, "od"),
                                     TotalLeave = 0,
                                     Absent = 5,
                                     TotalDeductionDays = 0,
                                     PayableWorkingDays = 0,
                                     TotalEarnLeave = 0,
                                     EnjoyableEarnLeaveDays = 0,
                                     PayableEarnLeave = 0,
                                     Stamp = 10,
                                     PayableEarnLeaveTk = 0
                                 }).ToListAsync();


                var eList = emp.AsQueryable();

                foreach (var i in eList)
                {
                    var elcal = _db.HRMS_EmployeeEarnLeaveCalculation.Where(e => e.HRMS_EmployeeFK == i.ID).LastOrDefault();
                    var TakenEarnLeave = _db.HRMS_EmployeeLeaveAssign.Where(la => la.HRMS_EmployeeFK == i.ID).Select(la => la.TakenEarnLeaveDays).FirstOrDefault();
                    i.EnjoyableEarnLeaveDays = (int) (TakenEarnLeave != null ? TakenEarnLeave : 0);
                    if (elcal != null)
                    {
                        i.LastPaymentDate = elcal.PaymentDate;

                        var hday = _db.HRMS_Holiday.Where(s => s.FromDate >= elcal.PaymentDate && s.FromDate <= VMELC.EarnLeaveDate).ToList();
                        i.Holidays = hday.Count;
                        i.Offdays = findDate(elcal.PaymentDate, VMELC.EarnLeaveDate, "od");
                        i.TotalMonth = ((VMELC.EarnLeaveDate.Year - elcal.PaymentDate.Year) * 12) + VMELC.EarnLeaveDate.Month - elcal.PaymentDate.Month;
                        i.WorkingDays = findDate(elcal.PaymentDate, VMELC.EarnLeaveDate, "wd");
                        var tLeave = _db.HRMS_LeaveApplication.Where(l => l.From >= elcal.PaymentDate && l.To <= VMELC.EarnLeaveDate && l.HRMS_EmployeeFK == i.ID).ToList();
                        var tl = tLeave.Sum(s => s.TotalDays);
                        i.TotalLeave = tl;
                        i.TotalDeductionDays = i.Holidays + i.Offdays + i.TotalLeave + i.Absent;
                        i.PayableWorkingDays = i.WorkingDays - i.TotalDeductionDays;
                        i.TotalEarnLeave = (i.PayableWorkingDays / 18);
                        i.PayableEarnLeave = i.TotalEarnLeave - i.EnjoyableEarnLeaveDays;
                        var basic = 1500;
                        i.PayableEarnLeaveTk = basic * i.PayableEarnLeave - i.Stamp;
                    }
                    else
                    {
                        var hday = _db.HRMS_Holiday.Where(s => s.FromDate >= i.JoiningDate && s.FromDate <= VMELC.EarnLeaveDate).ToList();
                        i.Holidays = hday.Count;

                        var tLeave = _db.HRMS_LeaveApplication.Where(l => l.From >= i.JoiningDate && l.To <= VMELC.EarnLeaveDate && l.HRMS_EmployeeFK == i.ID).ToList();
                        var tl = tLeave.Sum(s => s.TotalDays);
                        i.TotalLeave = tl;

                        i.TotalDeductionDays = i.Holidays + i.Offdays + i.TotalLeave + i.Absent;
                        i.PayableWorkingDays = i.WorkingDays - i.TotalDeductionDays;
                        i.TotalEarnLeave = (i.PayableWorkingDays / 18);
                        i.PayableEarnLeave = i.TotalEarnLeave - i.EnjoyableEarnLeaveDays;
                        var basic = 1500;
                        i.PayableEarnLeaveTk = basic * i.PayableEarnLeave - i.Stamp;
                        i.LastPaymentDate = i.JoiningDate;
                    }
                }

                return eList;
            }
            else
            {
                var emp = await (from e in _db.HRMS_Employee
                                 join d in _db.HRMS_Designation on e.HRMS_DesignationFK equals d.ID into d_Join
                                 from d in d_Join.DefaultIfEmpty()
                                 join s in _db.HRMS_Section on e.HRMS_SectionFK equals s.ID into s_Join
                                 from s in s_Join.DefaultIfEmpty()
                                 join dp in _db.User_Department on e.User_DepartmentFK equals dp.ID into dp_Join
                                 from dp in dp_Join.DefaultIfEmpty()
                                 join u in _db.HRMS_Unit on s.HRMS_UnitFK equals u.ID into u_Join
                                 from u in u_Join.DefaultIfEmpty()
                                 join bu in _db.HRMS_BusinessUnit on u.HRMS_BusinessUnitFK equals bu.ID into bu_Join
                                 from bu in bu_Join.DefaultIfEmpty()
                                 where e.Active
                                 select new VMEarnLeaveCalculationList
                                 {
                                     ID = e.ID,
                                     Name = e.Name,
                                     EmployeeIdentity = e.EmployeeIdentity,
                                     Department = dp.Name,
                                     Designation = d.Name,
                                     Section = s.Name,
                                     Unit = u.Name,
                                     JoiningDate = e.JoiningDate,
                                     TotalMonth = ((VMELC.EarnLeaveDate.Year - e.JoiningDate.Year) * 12) + VMELC.EarnLeaveDate.Month - e.JoiningDate.Month,
                                     WorkingDays = findDate(e.JoiningDate, VMELC.EarnLeaveDate, "wd"),
                                     Holidays = 0,
                                     Offdays = findDate(e.JoiningDate, VMELC.EarnLeaveDate, "od"),
                                     TotalLeave = 0,
                                     Absent = 5,
                                     TotalDeductionDays = 0,
                                     PayableWorkingDays = 0,
                                     TotalEarnLeave = 0,
                                     EnjoyableEarnLeaveDays = 0,
                                     PayableEarnLeave = 0,
                                     Stamp = 10,
                                     PayableEarnLeaveTk = 0
                                 }).ToListAsync();

                var eList = emp.AsQueryable();

                foreach (var i in eList)
                {
                    var elcal = _db.HRMS_EmployeeEarnLeaveCalculation.Where(e => e.HRMS_EmployeeFK == i.ID).LastOrDefault();
                    var basic = (from erm in _db.Payroll_EODRecordMaster
                                 join er in _db.Payroll_EODRecord on erm.ID equals er.Payroll_EODRecordMasterFk
                                 where erm.EmployeeFk == i.ID && er.EODReferenceFk == 1 && erm.Active == true
                                 select new { amount = er.Amount }).FirstOrDefault();

                    var TakenEarnLeave = _db.HRMS_EmployeeLeaveAssign.Where(la => la.HRMS_EmployeeFK == i.ID).Select(la => la.TakenEarnLeaveDays).FirstOrDefault();
                    i.EnjoyableEarnLeaveDays = (int) (TakenEarnLeave != null ? TakenEarnLeave : 0);

                    if (elcal != null)
                    {
                        i.LastPaymentDate = elcal.PaymentDate;

                        var hday = _db.HRMS_Holiday.Where(s => s.FromDate >= elcal.PaymentDate && s.FromDate <= VMELC.EarnLeaveDate).ToList();
                        i.Holidays = hday.Count;
                        i.Offdays = findDate(elcal.PaymentDate, VMELC.EarnLeaveDate, "od");
                        i.TotalMonth = ((VMELC.EarnLeaveDate.Year - elcal.PaymentDate.Year) * 12) + VMELC.EarnLeaveDate.Month - elcal.PaymentDate.Month;
                        i.WorkingDays = findDate(elcal.PaymentDate, VMELC.EarnLeaveDate, "wd");
                        var tLeave = _db.HRMS_LeaveApplication.Where(l => l.From >= elcal.PaymentDate && l.To <= VMELC.EarnLeaveDate && l.HRMS_EmployeeFK == i.ID).ToList();
                        var tl = tLeave.Sum(s => s.TotalDays);
                        i.TotalLeave = tl;

                        i.TotalDeductionDays = i.Holidays + i.Offdays + i.TotalLeave + i.Absent;
                        i.PayableWorkingDays = i.WorkingDays - i.TotalDeductionDays;
                        i.TotalEarnLeave = (i.PayableWorkingDays / 18);
                        i.PayableEarnLeave = i.TotalEarnLeave - i.EnjoyableEarnLeaveDays;
                        i.PayableEarnLeaveTk = basic.amount * i.PayableEarnLeave - i.Stamp;
                    }
                    else
                    {
                        var hday = _db.HRMS_Holiday.Where(s => s.FromDate >= i.JoiningDate && s.FromDate <= VMELC.EarnLeaveDate).ToList();
                        i.Holidays = hday.Count;

                        var tLeave = _db.HRMS_LeaveApplication.Where(l => l.From >= i.JoiningDate && l.To <= VMELC.EarnLeaveDate && l.HRMS_EmployeeFK == i.ID).ToList();
                        var tl = tLeave.Sum(s => s.TotalDays);
                        i.TotalLeave = tl;

                        i.TotalDeductionDays = i.Holidays + i.Offdays + i.TotalLeave + i.Absent;
                        i.PayableWorkingDays = i.WorkingDays - i.TotalDeductionDays;
                        i.TotalEarnLeave = (i.PayableWorkingDays / 18);
                        i.PayableEarnLeave = i.TotalEarnLeave - i.EnjoyableEarnLeaveDays;
                        i.PayableEarnLeaveTk = basic.amount * i.PayableEarnLeave - i.Stamp;
                        i.LastPaymentDate = i.JoiningDate;
                    }
                }

                return eList;
            }
        }
        public int findDate(DateTime first, DateTime last, string String)
        {
            if (String == "wd")
            {
                System.DateTime First = first;
                System.DateTime Last = last;
                System.TimeSpan diffResult = Last - First;

                return diffResult.Days;
            }
            else if (String == "od")
            {
                //var hd = _db.HRMS_EmployeeWeekendAssign.Include(s => s.HRMS_EmployeeWeekends).Where(s => s.HRMS_EmployeeFK == ID).FirstOrDefault();

                //var wa = (from ew in _db.HRMS_EmployeeWeekend
                //          join ewa in _db.HRMS_EmployeeWeekendAssign on ew.HRMS_EmployeeWeekendAssignFK equals ewa.ID
                //          where ew.HRMS_EmployeeWeekendAssignFK == ID
                //          select new
                //          {
                //              day = ew.WeekendDay
                //          }).ToList();
                TimeSpan ts = last - first;
                int count = (int)Math.Floor(ts.TotalDays / 7);
                int remainder = (int)(ts.TotalDays % 7);
                int sinceLastDay = (int)(last.DayOfWeek - DayOfWeek.Friday);
                if (sinceLastDay < 0) sinceLastDay += 7;
                if (remainder >= sinceLastDay) count++;
                return count;
            }
            else if (String == "Year")
            {
                DateTime zeroTime = new DateTime(1, 1, 1);
                TimeSpan span = last - first;
                int years = (zeroTime + span).Year - 1;
                return years;
            }
            else if (String == "Month")
            {
                DateTime zeroTime = new DateTime(1, 1, 1);
                TimeSpan span = last - first;
                int month = (zeroTime + span).Month - 1;
                return month;
            }
            else if (String == "Day")
            {
                DateTime zeroTime = new DateTime(1, 1, 1);
                TimeSpan span = last - first;
                int days = (zeroTime + span).Day - 1;
                return days;
            }
            return 0;
        }
        public async Task<VMEarnLeaveCalculation> AddEmployeesEarnLeaveCalculation(VMEarnLeaveCalculation VMELC)
        {

            foreach (var i in VMELC.VMEarnLeaveCalculationList)
            {
                if (i.IsSelected == true)
                {
                    HRMS_EmployeeEarnLeaveCalculation HEELC = new HRMS_EmployeeEarnLeaveCalculation();
                    HEELC.PaymentDate = VMELC.EarnLeaveDate;
                    HEELC.HRMS_EmployeeFK = i.ID;
                    HEELC.TotalMonth = i.TotalMonth;
                    HEELC.WorkingDays = i.WorkingDays;
                    HEELC.Holidays = i.Holidays;
                    HEELC.Offdays = i.Offdays;
                    HEELC.TotalLeave = i.TotalLeave;
                    HEELC.Absent = i.Absent;
                    HEELC.TotalDeductionDays = i.TotalDeductionDays;
                    HEELC.PayableWorkingDays = i.PayableWorkingDays;
                    HEELC.TotalEarnLeave = i.TotalEarnLeave;
                    HEELC.EnjoyableEarnLeaveDays = i.EnjoyableEarnLeaveDays;
                    HEELC.PayableEarnLeave = i.PayableEarnLeave;
                    HEELC.Stamp = i.Stamp;
                    HEELC.PayableEarnLeaveTk = i.PayableEarnLeaveTk;

                    _db.HRMS_EmployeeEarnLeaveCalculation.Add(HEELC);
                    await _db.SaveChangesAsync();
                }
            }
            return VMELC;
        }
        public async Task<IQueryable<VMServiceBenefitCalculationList>> GetEmployeesforServiceBenefit(VMServiceBenefitCalculation vmsbc)
        {
            var emp = await (from e in _db.HRMS_Employee
                             join d in _db.HRMS_Designation on e.HRMS_DesignationFK equals d.ID into d_Join
                             from d in d_Join.DefaultIfEmpty()
                             join s in _db.HRMS_Section on e.HRMS_SectionFK equals s.ID into s_Join
                             from s in s_Join.DefaultIfEmpty()
                             join dp in _db.User_Department on e.User_DepartmentFK equals dp.ID into dp_Join
                             from dp in dp_Join.DefaultIfEmpty()
                             join u in _db.HRMS_Unit on s.HRMS_UnitFK equals u.ID into u_Join
                             from u in u_Join.DefaultIfEmpty()
                             join bu in _db.HRMS_BusinessUnit on u.HRMS_BusinessUnitFK equals bu.ID into bu_Join
                             from bu in bu_Join.DefaultIfEmpty()
                             where e.ID == vmsbc.EmployeeID
                             select new VMServiceBenefitCalculationList
                             {
                                 ID = e.ID,
                                 Name = e.Name,
                                 EmployeeIdentity = e.EmployeeIdentity,
                                 Department = dp.Name,
                                 Designation = d.Name,
                                 Section = s.Name,
                                 Unit = u.Name,
                                 JoiningDate = e.JoiningDate,
                                 ResignationDate = DateTime.Now,
                                 Year = findDate(e.JoiningDate, DateTime.Now, "Year"),
                                 Month = findDate(e.JoiningDate, DateTime.Now, "Month"),
                                 Day = findDate(e.JoiningDate, DateTime.Now, "Day"),
                                 PayableServiceBenifitDays = 0,
                                 PresentSalary = 0,
                                 PresentBasic = 0,
                                 PresentBasicPerDay = 0,
                                 TotalAmount = 0,
                                 Stamp = 10,
                                 PayableAmount = 0
                             }).ToListAsync();
            var EmpServiceInfo = emp.AsQueryable();

            foreach (var i in EmpServiceInfo)
            {
                var basicSalary = (from erm in _db.Payroll_EODRecordMaster
                                   join er in _db.Payroll_EODRecord on erm.ID equals er.Payroll_EODRecordMasterFk
                                   where erm.EmployeeFk == i.ID && er.EODReferenceFk == 1 && erm.Active == true
                                   select new { Gross = erm.GrossSalary, Basic = er.Amount }).FirstOrDefault();


                if (basicSalary != null)
                {
                    var bpd = basicSalary.Basic / 30;

                    if (i.Year >= 5)
                    {
                        i.PresentSalary = basicSalary.Gross;
                        i.PresentBasic = basicSalary.Basic;
                        i.PresentBasicPerDay = bpd;
                        i.PayableServiceBenifitDays = i.Year * 14;
                        i.TotalAmount = bpd * i.PayableServiceBenifitDays;

                        i.PayableAmount = i.TotalAmount - i.Stamp;
                    }
                    else if (i.Year >= 10)
                    {
                        i.PresentSalary = basicSalary.Gross;
                        i.PresentBasic = basicSalary.Basic;
                        i.PresentBasicPerDay = bpd;
                        i.PayableServiceBenifitDays = i.Year * 45;
                        i.TotalAmount = bpd * i.PayableServiceBenifitDays;

                        i.PayableAmount = i.TotalAmount - i.Stamp;
                    }
                }
                else
                {
                    var bpd = 0;
                    if (i.Year >= 5)
                    {
                        i.PresentSalary = basicSalary.Gross;
                        i.PayableServiceBenifitDays = i.PayableServiceBenifitDays * 14;
                        i.TotalAmount = bpd * i.PayableServiceBenifitDays;
                    }
                    else if (i.Year >= 10)
                    {
                        i.PresentSalary = basicSalary.Gross;
                        i.PayableServiceBenifitDays = i.PayableServiceBenifitDays * 45;
                        i.TotalAmount = bpd * i.PayableServiceBenifitDays;
                    }
                }
            }
            return EmpServiceInfo;
        }
        public async Task<VMServiceBenefitCalculation> AddEmployeesServiceBenifitCalculation(VMServiceBenefitCalculation VMELC)
        {
            foreach (var i in VMELC.VMServiceBenefitCalculationLists)
            {
                HRMS_EmployeeServiceBenifit HESB = new HRMS_EmployeeServiceBenifit();
                HESB.HRMS_EmployeeFK = i.ID;
                HESB.ResignationDate = i.ResignationDate;
                HESB.Year = i.Year;
                HESB.Month = i.Month;
                HESB.Day = i.Day;
                HESB.PayableServiceBenifitDays = i.PayableServiceBenifitDays;
                HESB.PresentSalary = i.PresentSalary;
                HESB.PresentBasic = i.PresentBasic;
                HESB.PresentBasicPerDay = i.PresentBasicPerDay;
                HESB.PayableAmount = i.PayableAmount;
                HESB.Stamp = i.Stamp;
                HESB.TotalAmount = i.TotalAmount;

                _db.HRMS_EmployeeServiceBenifit.Add(HESB);
                await _db.SaveChangesAsync();
            }
            return VMELC;
        }
        public async Task<IEnumerable<VMServiceBenefitCalculationList>> EmployeesServiceBenefitList(VMServiceBenefitCalculation VMELC)
        {
            var SBList = await (from sb in _db.HRMS_EmployeeServiceBenifit
                                join e in _db.HRMS_Employee on sb.HRMS_EmployeeFK equals e.ID into e_Join
                                from e in e_Join.DefaultIfEmpty()
                                join s in _db.HRMS_Section on e.HRMS_SectionFK equals s.ID into s_Join
                                from s in s_Join.DefaultIfEmpty()
                                join d in _db.User_Department on e.User_DepartmentFK equals d.ID into d_Join
                                from d in d_Join.DefaultIfEmpty()
                                join u in _db.HRMS_Unit on s.HRMS_UnitFK equals u.ID into u_Join
                                from u in u_Join.DefaultIfEmpty()
                                join bu in _db.HRMS_BusinessUnit on u.HRMS_BusinessUnitFK equals bu.ID into bu_Join
                                from bu in bu_Join.DefaultIfEmpty()
                                where sb.Time >= VMELC.From && sb.Time <= VMELC.To
                                select new VMServiceBenefitCalculationList()
                                {
                                    ID = sb.ID,
                                    Section = s.Name,
                                    EmployeeIdentity = e.EmployeeIdentity,
                                    Name = e.Name,
                                    JoiningDate = e.JoiningDate,
                                    ResignationDate = e.JoiningDate,
                                    Year = sb.Year,
                                    Month = sb.Month,
                                    Day = sb.Day,
                                    PayableServiceBenifitDays = sb.PayableServiceBenifitDays,
                                    PresentSalary = sb.PresentSalary,
                                    PresentBasic = sb.PresentBasic,
                                    PresentBasicPerDay = sb.PresentBasicPerDay,
                                    TotalAmount = sb.TotalAmount,
                                    Stamp = sb.Stamp,
                                    PayableAmount = sb.PayableAmount
                                }).ToListAsync();
            return SBList;
        }

        public async Task<IEnumerable<VMEarnLeaveCalculationList>> EmployeesEarnLeaveList(VMEarnLeaveCalculation VMELC)
        {
            var SBList = await (from eb in _db.HRMS_EmployeeEarnLeaveCalculation
                                join e in _db.HRMS_Employee on eb.HRMS_EmployeeFK equals e.ID into e_Join
                                from e in e_Join.DefaultIfEmpty()
                                join d in _db.HRMS_Designation on e.HRMS_DesignationFK equals d.ID into d_Join
                                from d in d_Join.DefaultIfEmpty()

                                join s in _db.HRMS_Section on e.HRMS_SectionFK equals s.ID into s_Join
                                from s in s_Join.DefaultIfEmpty()
                                join dp in _db.User_Department on e.User_DepartmentFK equals dp.ID into dp_Join
                                from dp in dp_Join.DefaultIfEmpty()
                                join u in _db.HRMS_Unit on s.HRMS_UnitFK equals u.ID into u_Join
                                from u in u_Join.DefaultIfEmpty()
                                join bu in _db.HRMS_BusinessUnit on u.HRMS_BusinessUnitFK equals bu.ID into bu_Join
                                from bu in bu_Join.DefaultIfEmpty()
                                where eb.Time >= VMELC.From && eb.Time <= VMELC.To
                                select new VMEarnLeaveCalculationList()
                                {
                                    ID = eb.ID,
                                    Designation = d.Name,
                                    Department = dp.Name,
                                    Unit = u.Name,
                                    Section = s.Name,
                                    EmployeeIdentity = e.EmployeeIdentity,
                                    Name = e.Name,
                                    JoiningDate = e.JoiningDate,
                                    TotalMonth = eb.TotalMonth,
                                    WorkingDays = eb.WorkingDays,
                                    Holidays = eb.Holidays,
                                    Offdays = eb.Offdays,
                                    TotalLeave = eb.TotalLeave,
                                    Absent = eb.Absent,
                                    TotalDeductionDays = eb.TotalDeductionDays,
                                    PayableWorkingDays = eb.PayableWorkingDays,
                                    TotalEarnLeave = eb.TotalEarnLeave,
                                    EnjoyableEarnLeaveDays = eb.EnjoyableEarnLeaveDays,
                                    PayableEarnLeave = eb.PayableEarnLeave,
                                    Stamp = eb.Stamp,
                                    PayableEarnLeaveTk = eb.PayableEarnLeaveTk
                                }).ToListAsync();
            return SBList;
        }

        public async Task<IEnumerable<VMEarnLeaveCalculationList>> EarnLeaveCalculationList(VMEarnLeaveCalculation VMELC)
        {
            var ELList = await (from el in _db.HRMS_EmployeeEarnLeaveCalculation
                                join e in _db.HRMS_Employee on el.HRMS_EmployeeFK equals e.ID into e_Join
                                from e in e_Join.DefaultIfEmpty()
                                join s in _db.HRMS_Section on e.HRMS_SectionFK equals s.ID into s_Join
                                from s in s_Join.DefaultIfEmpty()
                                join dp in _db.User_Department on e.User_DepartmentFK equals dp.ID into dp_Join
                                from dp in dp_Join.DefaultIfEmpty()
                                join u in _db.HRMS_Unit on s.HRMS_UnitFK equals u.ID into u_Join
                                from u in u_Join.DefaultIfEmpty()
                                join bu in _db.HRMS_BusinessUnit on u.HRMS_BusinessUnitFK equals bu.ID into bu_Join
                                from bu in bu_Join.DefaultIfEmpty()
                                where el.Time >= VMELC.From && el.Time <= VMELC.To
                                select new VMEarnLeaveCalculationList()
                                {
                                    ID = el.ID,
                                    //Designation = d.Name,
                                    Department = dp.Name,
                                    Unit = u.Name,
                                    //Section = s.Name,
                                    //CardNo = e.CardNo,
                                    Name = e.Name,
                                    //JoiningDate = e.JoiningDate,
                                    TotalMonth = el.TotalMonth,
                                    WorkingDays = el.WorkingDays,
                                    Holidays = el.Holidays,
                                    Offdays = el.Offdays,
                                    TotalLeave = el.TotalLeave,
                                    Absent = el.Absent,
                                    TotalDeductionDays = el.TotalDeductionDays,
                                    PayableWorkingDays = el.PayableWorkingDays,
                                    TotalEarnLeave = el.TotalEarnLeave,
                                    EnjoyableEarnLeaveDays = el.EnjoyableEarnLeaveDays,
                                    PayableEarnLeave = el.PayableEarnLeave,
                                    Stamp = el.Stamp,
                                    PayableEarnLeaveTk = el.PayableEarnLeaveTk
                                }).ToListAsync();

            return ELList;
        }
        #endregion

        #region Attendance
        public async Task<List<VMAttendance>> AttendanceHistoryListGet(VMAttendance model)
        {

            List<VMAttendance> VM = new List<VMAttendance>();
            if(model.BusinessUnitId > 0 || model.DepartmentID > 0 || model.UnitId > 0 || model.SectionId > 0 || model.ShiftId > 0 || model.EmployeeId > 0 || (model.FromDate != null && model.FromDate != DateTime.MinValue && model.ToDate != null && model.ToDate != DateTime.MinValue))
            {
                VM = await Task.Run(() => (from AH in _db.HRMS_AttendanceHistory.OrderBy(x => x.Date)
                                           join SH in _db.HRMS_Shift on AH.HRMS_ShiftFK equals SH.ID into SH_Join
                                           from SH in SH_Join.DefaultIfEmpty()
                                           join E in _db.HRMS_Employee on AH.HRMS_EmployeeFK equals E.ID into E_Join
                                           from E in E_Join.DefaultIfEmpty()
                                           
                                           
                                           
                                           join S in _db.HRMS_Section on E.HRMS_SectionFK equals S.ID into S_join
                                           from S in S_join.DefaultIfEmpty()
                                           join DP in _db.User_Department on E.User_DepartmentFK equals DP.ID into DP_join
                                           from DP in DP_join.DefaultIfEmpty()
                                           join U in _db.HRMS_Unit on S.HRMS_UnitFK equals U.ID into U_Join
                                           from U in U_Join.DefaultIfEmpty()
                                           join BU in _db.HRMS_BusinessUnit on U.HRMS_BusinessUnitFK equals BU.ID into BU_join
                                           from BU in BU_join.DefaultIfEmpty()
                                           where AH.Active
                                           && (model.BusinessUnitId == 0 || BU.ID == model.BusinessUnitId)
                                           && (model.DepartmentID == 0 || DP.ID == model.DepartmentID)
                                           && (model.UnitId == 0 || U.ID == model.UnitId)
                                           && (model.SectionId == 0 || S.ID == model.SectionId)
                                           && (model.ShiftId == 0 || SH.ID == model.ShiftId)
                                           && (model.EmployeeId == 0 || E.ID == model.EmployeeId)
                                           && ((model.FromDate == null || model.FromDate == DateTime.MinValue || model.ToDate == null || model.ToDate == DateTime.MinValue) || (AH.Date.Date >= model.FromDate.Value.Date && AH.Date.Date <= model.ToDate.Value.Date))
                                           orderby AH.Date
                                           select new VMAttendance()
                                           {
                                               ID = AH.ID,
                                               ShiftName = SH.ShiftName,

                                               EmployeeName = E.Name,
                                               EmployeeCardNo = E.CardNo,

                                               AttendanceStatus = (EnumAttendanceStatus)AH.AttendanceStatus,
                                               AttendanceDate = AH.Date,
                                               InTime = AH.InTime,
                                               OutTime = AH.OutTime,
                                               Remarks = AH.IsEdited.HasValue && AH.IsEdited.Value ? AH.Remarks : "",

                                               BusinessUnitName = BU.Name,
                                               DepartmentName = DP.Name,
                                               UnitName = U.Name,
                                               SectionName = S.Name,


                                           }).ToListAsync());
            }
            





            return VM;
        }

        
        public async Task<int> AttendanceHistoryEdit(VMAttendance model)
        {

            

            var result = -1;
            HRMS_AttendanceHistory en = await _db.HRMS_AttendanceHistory.FindAsync(model.ID);

            

            HRMS_AttendanceHistoryLog enLog = new HRMS_AttendanceHistoryLog()
            {
                HRMS_AttendanceHistoryFK = en.ID,
                HRMS_EmployeeFK = en.HRMS_EmployeeFK,
                Date = en.Date,
                InTime = en.InTime,
                OutTime = en.OutTime,
                TotalTime = en.TotalTime,
                Late = en.Late,
                OverTime = en.OverTime,
                AttendanceStatus = en.AttendanceStatus,
                CardNo = en.CardNo,
                HRMS_ShiftFK = en.HRMS_ShiftFK,
                LeavePaidType = en.LeavePaidType,
                EntryDate = en.EntryDate,
                UpdateDate = en.UpdateDate,
                EntryByUserFK = en.EntryByUserFK,

                UpdateByUserFK = en.UpdateByUserFK,
                PayableOverTime = en.PayableOverTime,
                IsEdited = en.IsEdited
            };

            en.InTime = model.InTime;
            en.OutTime = model.OutTime;
            en.AttendanceStatus = (int)model.AttendanceStatus;
            en.Remarks = model.Remarks;
            en.IsEdited = true;
            en.UpdateByUserFK = model.UserID;

            _db.HRMS_AttendanceHistoryLog.Add(enLog);



            if (await _db.SaveChangesAsync() > 0)
            {
                result = en.ID;
            }
            return result;
        }

        public async Task<List<VMEmployee>> EmployeeListGetAsync(int shiftId)
        {
            return await Task.Run(() => (from SA in _db.HRMS_ShiftAssign.Where(x => x.HRMS_ShiftFK == shiftId)
                                         join E in _db.HRMS_Employee.Where(x => x.Active) on SA.HRMS_EmployeeFK equals E.ID
                                         group E by new { E.ID, E.CardNo, E.Name } into g
                                         select new VMEmployee()
                                         {
                                             ID = g.Key.ID,
                                             CardNo = g.Key.CardNo,
                                             Name = g.Key.Name
                                         }).ToListAsync());
        }


        public async Task<int> AttendanceProcess(VMAttendance model)
        {
            var result = -1;

            var ProcessStartDate = new SqlParameter("@ProcessStartDate", SqlDbType.NVarChar, 8)
            {
                Value = model.FromDate.Value == DateTime.MinValue ? DateTime.Now.ToString("yyyyMMdd") : model.FromDate.Value.ToString("yyyyMMdd")
            };

            var ProcessEndDate = new SqlParameter("@ProcessEndDate", SqlDbType.NVarChar, 8)
            {
                Value = model.ToDate.Value == DateTime.MinValue ? DateTime.Now.ToString("yyyyMMdd") : model.ToDate.Value.ToString("yyyyMMdd")
            };
            var EmployeeID = new SqlParameter("@EmployeeID", SqlDbType.Int)
            {
                Value = model.EmployeeId
            };
            var ShiftID = new SqlParameter("@ShiftID", SqlDbType.Int)
            {
                Value = model.ShiftId
            };
            var UserID = new SqlParameter("@UserID", SqlDbType.Int)
            {
                Value = model.UserID
            };


            var command = "EXEC [AttendanceProcess] @ProcessStartDate, @ProcessEndDate, @EmployeeID, @ShiftID, @UserID";
            result = await _db.ExecuteSqlCommandAsync(command, ProcessStartDate, ProcessEndDate, EmployeeID, ShiftID, UserID);

            return result;
        }

        public int CheckInOutLastSourceId()
        {
            return _db.HRMS_CheckInOuts.Select(x => x.SourceId).DefaultIfEmpty(0).Max();
        }
        public async Task<int> CheckInOutDataDump(List<HRMS_CheckInOuts> vM)
        {
            int result = -1;

            await _db.HRMS_CheckInOuts.AddRangeAsync(vM);
            result = await _db.SaveChangesAsync();
            return result;
        }
        #endregion

        #region HolidayIntegrationPlan

        public List<VMHoliday> GetCompanyWeekend()
        {
            var vData = _db.HRMS_Holiday.Where(a => a.Active == true && a.IsCompanyWeekendDay == true).Select(a=>new VMHoliday {
                HoildayName=a.HoildayName
            }).ToList();
            return vData;
        }

        public List<VMHoliday> GetAllHoliday(DateTime From, DateTime To)
        {
            var vData = _db.HRMS_Holiday.Where(a => a.Active == true
            && a.IsCompanyWeekendDay == false
            && a.FromDate >= From && a.ToDate <= To).Select(a => new VMHoliday
            {
                FromDate = a.FromDate,
                ToDate = a.ToDate,
                HoildayName = a.HoildayName,
                Description = a.Description
            }).ToList();

            return vData;
        }

        #endregion
    }
}
