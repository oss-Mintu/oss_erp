﻿using Erp.Core.Services.Home;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Erp.Core.Services.HRMS
{
    public class HRMSBusinessUnitVM : BaseVM // Sister Concernt 
    {
        public int? BId { get; set; }
        public string Name { get; set; }
        public string BusinessUnitRemarks { get; set; }

        public IList<HRMSSettingsList> HRMSSettingsLists { get; set; }
        public IList<HRMSBusinessUnitVM> BULists { get; set; }
    }

    public class HRMSDesignationVM : BaseVM
    {
        public int DId { get; set; }
        [Display(Name = "Level Name")]
        [Required]
        public int? LevelId { get; set; }

        [Display(Name = "Designation")]
        public int? DesignationId { get; set; }

        [Required]
        public int? DepartmentId { get; set; }

        [Required]
        public string Name { get; set; }
        public string DesignationRemarks { get; set; }
        public decimal AttendanceBonus { get; set; }
        public decimal NightBill { get; set; }
        public IList<HRMSSettingsList> HRMSSettingsLists { get; set; }
        public string DepartmentName { get; set; }
        public string LevelName { get; set; }
        public string Remarks { get; set; }
        public List<HRMSDesignationVM> DataList { get; set; }

        public SelectList VMLevelList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        public SelectList Designation_List { get; set; } = new SelectList(new List<object>(), "Value", "Text");
    }

    public class HRMSUnitVM : BaseVM // Business Unit 
    {
        public int UId { get; set; }
        [Required]
        public string Name { get; set; }
        public string UnitRemarks { get; set; }

        [Display(Name = "Business Unit")]
        [Range(1, int.MaxValue, ErrorMessage = "Business Unit is Required")]
        public int? VMBusinessUnitID { get; set; }
        public string VMBusinessUnitName { get; set; }
        public SelectList VMBusinessUnitList { get; set; } = new SelectList(new List<object>(), "Value", "Text");

        public List<HRMSUnitVM> DataList { get; set; }
    }

    public class HRMSJobDescriptionVM : BaseVM
    {
        public int JId { get; set; }
        public string JobDescription { get; set; }

        [Display(Name = "Designation")]
        [Range(1, int.MaxValue, ErrorMessage = "Designation is Required")]
        public int? VMDesignationID { get; set; }
        public string VMDesignationName { get; set; }
        public SelectList VMDesignationList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        [Display(Name = "Department")]
        [Range(1, int.MaxValue, ErrorMessage = "Department is Required")]
        public int? VMDepartmentID { get; set; }
        public string VMDepartmentName { get; set; }
        public SelectList VMDepartmentList { get; set; } = new SelectList(new List<object>(), "Value", "Text");

        public List<HRMSJobDescriptionVM> DataList { get; set; }
    }

    public class HRMSSectionVM : BaseVM
    {
        public int SId { get; set; }
        public string Name { get; set; }
        public string SectionRemarks { get; set; }
        [Display(Name = "Sister Concern")]
        [Range(1, int.MaxValue, ErrorMessage = "Business Unit is Required")]
        public int? VMBusinessUnitID { get; set; }
        public string VMBusinessUnitName { get; set; }
        public SelectList VMBusinessUnitList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        [Display(Name = "Business Unit")]
        [Range(1, int.MaxValue, ErrorMessage = "Unit is Required")]
        public int? VMUnitID { get; set; }
        public string VMUnitName { get; set; }
        public SelectList VMUnitList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        [Display(Name = "Department")]
        [Range(1, int.MaxValue, ErrorMessage = "Department is Required")]
        public int? VMDepartmentID { get; set; }
        public string VMDepartmentName { get; set; }
        public SelectList VMDepartmentList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        public List<HRMSSectionVM> DataList { get; set; }
    }

    public class HRMSHierarchyVM : BaseVM
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string HierarchyRemarks { get; set; }

        public IList<HRMSSettingsList> HRMSSettingsLists { get; set; }
    }

    public class HRMSSettingsList
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Remarks { get; set; }
    }

    public class HRMSDegreeVM: BaseVM
    {
        public int DId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public IList<HRMSDegreeVM> DegList { get; set; }
    }
    public class HRMSDistrictVM : BaseVM
    {
        public int DId { get; set; }
        public string Name { get; set; }
        public string Remarks { get; set; }

        public int? CountryId { get; set; }
        public string CountryName { get; set; }
        public SelectList CountryList { get; set; } = new SelectList(new List<object>(), "Value", "Text");

        public List<CountryToPostOficeListVM> DataList { get; set; }
    }

    public class CountryToPostOficeListVM : BaseVM
    {
        public string CountryName { get; set; }
        public string DistrictName { get; set; }
        public string PoliceStationName { get; set; }
        public string PostOfficeName { get; set; }

        public string PostCode { get; set; }
    }
    public class HRMSPoliceStationVM : BaseVM
    {
        public int PSId { get; set; }
        public string Name { get; set; }
        public string Remarks { get; set; }

        public int? CountryId { get; set; }
        public string CountryName { get; set; }
        public int? DistrictId { get; set; }
        public string DistrictName { get; set; }
        public SelectList CountryList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        public SelectList DistrictList { get; set; } = new SelectList(new List<object>(), "Value", "Text");

        public List<CountryToPostOficeListVM> DataList { get; set; }
    }

    public class HRMSPostOfficeVM : BaseVM
    {
        public int POId { get; set; }
        public string Name { get; set; }
        //public string Code { get; set; }
        public string Remarks { get; set; }

        public int? CountryId { get; set; }
        public int? DistrictId { get; set; }
        public int? PoliceStationId { get; set; }
        public SelectList CountryList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        public SelectList DistrictList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        public SelectList PoliceStationList { get; set; } = new SelectList(new List<object>(), "Value", "Text");

        public List<CountryToPostOficeListVM> DataList { get; set; }
    }

    public class HRMSEmployeesCardGenerationVM
    {
        public HRMSEmployeesCardGenerationVM()
        {
            IDCardIssueDate = DateTime.Now;
        }
        [Required]
        [Display(Name ="Section")]
        public int SectionId { get; set; }

        [Display(Name = "Designation")]
        public int DesignationId { get; set; }

        [Required]
        public int Quantity { get; set; }
        [Required]
        [Display(Name = "Issue Date")]
        public DateTime? IDCardIssueDate { get; set; }
        [Required]
        [Display(Name = "Expired Date")]
        public DateTime? IDCardExpireDate { get; set; }

        public IList<Emplist> Emplists { get; set; }
        public IList<HRMSEmployeeIdCardVM> EmployeeIdCards { get; set; }
    }
    public class HRMSEmployeeIdCardVM : BaseVM
    {
        public string Name { get; set; }
        public string Designation { get; set; }
        public string Section { get; set; }
        public string BloodGroup { get; set; }
        public DateTime JoiningDate { get; set; } = DateTime.Now;
        public DateTime? IDCardIssueDate { get; set; } = DateTime.Now.Date;
        public DateTime? IDCardExpireDate { get; set; } = DateTime.Now.Date;
        public string NIDNo { get; set; }
        public string EmergencyPhoneNo { get; set; }
        public string PermanentAddress { get; set; }
        public string ImagePath { get; set; }
        public byte[] Image { get; set; }
        public string IssueDate { get; set; }
        public string ExpireDate { get; set; }
    }

    public class CommonCompanyVM : BaseVM
    {
        public int Id { get; set; }
        [Required]
        [Display(Name ="Company Name")]
        public string Name { get; set; }
        [Required]
        [Display(Name = "Short Name")]
        public string ShortName { get; set; }
        [Required]
        public string Phone { get; set; }
        public string Email { get; set; }
        [Display(Name = "Contact Person")]
        public string ContactPerson { get; set; }
        [Required]
        [Display(Name = "Address")]
        public string Address { get; set; }
        
        public SelectList VMCompanyList { get; set; } = new SelectList(new List<object>(), "Value", "Text");

        public byte[] Image { get; set; }


        public string Exist { get; set; }


        public List<CommonCompanyVM> DataList { get; set; }
    }

}
