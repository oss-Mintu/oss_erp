﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Erp.Core.Services.HRMS
{
    public class VMEmployee : BaseVM
    {
        public int EmployeeActionId { get; set; }
        public EnumEmployeeAction EmployeeAction { get { return (EnumEmployeeAction)this.EmployeeActionId; } }


        [DisplayName("Employee ID")]
        //[Required(ErrorMessage = "Enter Card No")]
        public string EmployeeIdentity { get; set; } = "";

        [DisplayName("Punch Card No")]
        [Required(ErrorMessage = "Enter Punch Card No")]
        public string CardNo { get; set; } = "";

        [DisplayName("Name (English)")]
        [Required(ErrorMessage = "Enter Name")]
        public string Name { get; set; } = "";

        [DisplayName("Name (Bangla)")]
        [Required(ErrorMessage = "Enter Bangla Name")]
        public string NameBN { get; set; } = "";

        [DisplayName("Father Name")]
        [Required(ErrorMessage = "Enter Father Name")]
        public string FatherName { get; set; } = "";

        [DisplayName("Mother Name")]
        public string MotherName { get; set; } = "";

        [DisplayName("Date Of Birth")]
        [Required(ErrorMessage = "Enter Date of Birth")]
        [DataType(DataType.Date)]
        public DateTime DOB { get; set; }
        public string DOBControlString { get { return this.DOB != DateTime.MinValue ? this.DOB.ToString("yyyy-MM-dd") : ""; } }
        public string DOBTableString { get { return this.DOB != DateTime.MinValue ? this.DOB.ToString("MMM dd, yyyy") : ""; } }

        [Required(ErrorMessage = "Enter Gender")]
        public string Gender { get; set; } = "";


        [DisplayName("Designation")]
        [Required(ErrorMessage = "Enter Designation")]
        public int? VMDesignationId { get; set; }
        public string VMDesignationName { get; set; }
        public SelectList VMDesignationList { get; set; } = new SelectList(new List<object>());

        [DisplayName("Business Unit")]
        //[Required(ErrorMessage = "Enter Unit")]
        public int? VMUnitId { get; set; }
        [DisplayName("Business Unit")]
        public string VMUnitName { get; set; }
        public SelectList VMUnitList { get; set; } = new SelectList(new List<object>());

        [DisplayName("Department")]
        //[Required(ErrorMessage = "Enter Department")]
        public int? VMDepartmentId { get; set; }
        [DisplayName("Department")]
        public string VMDepartmentName { get; set; }
        public SelectList VMDepartmentList { get; set; } = new SelectList(new List<object>());

        [DisplayName("Section")]
        [Required(ErrorMessage = "Enter Section")]
        public int? VMSectionId { get; set; }
        [DisplayName("Section")]
        public string VMSectionName { get; set; }
        public SelectList VMSectionList { get; set; } = new SelectList(new List<object>());

        [DisplayName("Country")]
        [Required(ErrorMessage = "Select Country")]
        [Range(1, int.MaxValue, ErrorMessage = "Select Country")]
        public int? VMCountryId { get; set; }
        [DisplayName("Country")]
        public string VMCountryName { get; set; }
        public SelectList VMCountryList { get; set; } = new SelectList(new List<object>());

        [DisplayName("District")]
        [Required(ErrorMessage = "Select District")]
        [Range(1, int.MaxValue, ErrorMessage = "Select District")]
        public int? VMDistrictId { get; set; }
        [DisplayName("District")]
        public string VMDistrictName { get; set; }
        public SelectList VMDistrictList { get; set; } = new SelectList(new List<object>());

        [DisplayName("Police Station")]
        [Required(ErrorMessage = "Select Police Station")]
        [Range(1, int.MaxValue, ErrorMessage = "Select Police Station")]
        public int? VMPoliceStationId { get; set; }
        [DisplayName("Police Station")]
        public string VMPoliceStationName { get; set; }
        public SelectList VMPoliceStationList { get; set; } = new SelectList(new List<object>());

        [DisplayName("Post Office")]
        [Required(ErrorMessage = "Select Post Office")]
        [Range(1, int.MaxValue, ErrorMessage = "Select Post Office")]
        public int? VMPostOfficeId { get; set; }
        [DisplayName("Post Office")]
        public string VMPostOfficeName { get; set; }
        public SelectList VMPostOfficeList { get; set; } = new SelectList(new List<object>());

        [DisplayName("Degree")]
        public int VMDegreeId { get; set; }
        public string VMDegreeName { get; set; }
        public SelectList VMDegreeList { get; set; } = new SelectList(new List<object>());

        [DisplayName("Line No.")]
        [Required(ErrorMessage = "Enter Line No.")]
        public int? VMLineId { get; set; }
        [DisplayName("Line No.")]
        public string VMLineName { get; set; }
        public SelectList VMLineList { get; set; } = new SelectList(new List<object>());

        [DisplayName("Sister Concern")]
        public int? VMBusinessUnitId { get; set; }
        public string VMBusinessUnitName { get; set; }
        public SelectList VMBusinessUnitList { get; set; } = new SelectList(new List<object>());

        [DisplayName("Blood Group")]
        //[Required(ErrorMessage = "Enter Blood Group")]
        public EnumBloodGroup BloodGroup { get; set; }
        public string BloodGroupName { get { return BaseFunctionalities.GetEnumDescription(BloodGroup); } }
        public SelectList BloodGroupList { get { return new SelectList(BaseFunctionalities.GetEnumList<EnumBloodGroup>(), "Value", "Text"); } }

        [DisplayName("Joining Date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Required(ErrorMessage = "Enter Joining Date")]
        [DataType(DataType.Date)]
        public DateTime JoiningDate { get; set; }
        public string JoiningDateControlString { get { return this.JoiningDate != DateTime.MinValue ? this.JoiningDate.ToString("yyyy-MM-dd") : ""; } }
        public string JoiningDateTableString { get { return this.JoiningDate != DateTime.MinValue ? this.JoiningDate.ToString("MMM dd, yyyy") : ""; } }

        [DisplayName("Religion")]
        public string Religion { get; set; } = "";

        [DisplayName("Contact No.")]
        [Required(ErrorMessage = "Enter Contact No.")]
        public string MobileNo { get; set; } = "";

        [DisplayName("Alt. Contact No.")]
        public string AltMobileNo { get; set; } = "";

        [DisplayName("Alt. Person Name")]
        public string AltPersonName { get; set; } = "";

        [DisplayName("Alt. Person Contact No.")]
        public string AltPersonContactNo { get; set; } = "";

        [DisplayName("Alt. Person Address")]

        public string AltPersonAddress { get; set; } = "";

        [DisplayName("Emergency Contact No.")]
        public string EmergencyContactNo { get; set; } = "";

        [DisplayName("Office Contact")]
        public string OfficeContact { get; set; } = "";

        [DisplayName("Land Phone")]
        public string LandPhone { get; set; } = "";

        [DisplayName("Email")]
        [DataType(DataType.EmailAddress)]
        public string EmailId { get; set; } = "";

        [DisplayName("Employement Type")]
        public EnumEmploymentType EmploymentType { get; set; }
        public string EmploymentTypeName { get { return BaseFunctionalities.GetEnumDescription((EnumEmploymentType)EmploymentType); } }
        public SelectList EmploymentTypeList { get { return new SelectList(BaseFunctionalities.GetEnumList<EnumEmploymentType>(), "Value", "Text"); } }

        [DisplayName("NID No.")]
        // [Required(ErrorMessage = "Enter NID.")]
        public string NIDNo { get; set; } = "";

        [DisplayName("Passport No.")]
        public string PassportNo { get; set; } = "";

        [DisplayName("Present Address")]
        [StringLength(maximumLength: 100, MinimumLength = 5)]
        public string PresentAddress { get; set; } = "";

        [DisplayName("Permanent Address")]
        // [Required(ErrorMessage = "Enter Address")]
        [StringLength(maximumLength: 100, MinimumLength = 5)]
        public string PermanentAddress { get; set; } = "";

        [DisplayName("Marital Status")]
        public string MaritalStatus { get; set; } = "";

        public string Photo { get; set; }
        public string ImagePath { get; set; }
        public byte[] Image { get; set; }

        [DisplayName("Present Status")]
        public EnumEmployeeStatus PresentStatus { get; set; } = EnumEmployeeStatus.Present;

        [DisplayName("Staff Type")]
        [Required(ErrorMessage = "Staff Type is Required.")]
        public EnumStaffType StaffType { get; set; }
        public string StaffTypeName { get { return BaseFunctionalities.GetEnumDescription(StaffType); } }
        public SelectList StaffTypeList { get { return new SelectList(BaseFunctionalities.GetEnumList<EnumStaffType>(), "Value", "Text"); } }

        [Required(ErrorMessage = "Employee OT Eligibility should be required."), DisplayName("Over Time Eligibility")]
        public string OvertimeEligibility { get; set; } = "";

        [DisplayName("Grade")]
        [Required(ErrorMessage = "Grade is Required.")]
        public string Grade { get; set; } = "";


        [DisplayName("Quit Date")]
        public DateTime QuitDate { get; set; }
        public string QuitDateControlString { get { return this.QuitDate != DateTime.MinValue ? this.QuitDate.ToString("yyyy-MM-dd") : ""; } }
        public string QuitDateTableString { get { return this.QuitDate != DateTime.MinValue ? this.QuitDate.ToString("MMM dd, yyyy") : ""; } }


        public List<VMEmployee> DataList { get; set; } = new List<VMEmployee>();
        public List<VMEmployeeSpouse> VMEmployeeSpouseList { get; set; } = new List<VMEmployeeSpouse>();
        public List<VMEmployeeChildren> VMEmployeeChildrenList { get; set; } = new List<VMEmployeeChildren>();

        public List<VMEmployeeEducation> VMEmployeeEducationList { get; set; } = new List<VMEmployeeEducation>();
        public List<VMEmployeeTraining> VMEmployeeTrainingList { get; set; } = new List<VMEmployeeTraining>();
        public List<VMEmployeeExperience> VMEmployeeExperienceList { get; set; } = new List<VMEmployeeExperience>();
        public List<VMEmployeeSkill> VMEmployeeSkillList { get; set; } = new List<VMEmployeeSkill>();
        public List<VMEmployeeReference> VMEmployeeReferenceList { get; set; } = new List<VMEmployeeReference>();
        public List<VMEmployeeNominee> VMEmployeNomineeList { get; set; } = new List<VMEmployeeNominee>();

        [DisplayName("Select Line Role")]
        [Required(ErrorMessage = "Select Line Role")]
        public int? VMLineRoleId { get; set; }
        public SelectList VMLineRoleList { get { return new SelectList(BaseFunctionalities.GetEnumList<RoleTypeEnum>(), "Value", "Text"); } }

        [DisplayName("Reporting To")]
        [Required(ErrorMessage = "Select Designation")]
        public int? VMReportingEmployeeId { get; set; }
        public string VMReportingDesignationName { get; set; }
        public SelectList VMReportingEmployeeList { get; set; } = new SelectList(new List<object>());
    }

    public class VMEmployeeSpouse : BaseVM
    {
        [Required(ErrorMessage = "First Save Employee Info")]

        [Range(1, int.MaxValue, ErrorMessage = "First Save Employee Info")]
        public int VMEmployeeId { get; set; }
        [DisplayName("Spouse Name")]


        public string SpouseName { get; set; }

        //[Required(ErrorMessage = "")]
        [DisplayName("Date of Birth")]
        public DateTime SpouseDOB { get; set; }
        public string SpouseDOBControlString { get { return this.SpouseDOB != DateTime.MinValue ? this.SpouseDOB.ToString("yyyy-MM-dd") : ""; } }
        public string SpouseDOBTableString { get { return this.SpouseDOB != DateTime.MinValue ? this.SpouseDOB.ToString("MMM dd, yyyy") : ""; } }

        [DisplayName("Contact No.")]

        public string SpouseContactNo { get; set; }

        [DisplayName("Blood Group")]
        public EnumBloodGroup SpouseBloodGroup { get; set; }
        //[Required(ErrorMessage = "Enter Blood Group")]
        public string SpouseBloodGroupName { get { return BaseFunctionalities.GetEnumDescription(this.SpouseBloodGroup); } }
        public SelectList SpouseBloodGroupList { get { return new SelectList(BaseFunctionalities.GetEnumList<EnumBloodGroup>(), "Value", "Text"); } }
    }

    public class VMEmployeeChildren : BaseVM
    {
        [Required(ErrorMessage = "First Save Employee Info")]

        [Range(1, int.MaxValue, ErrorMessage = "First Save Employee Info")]
        public int VMEmployeeId { get; set; }
        [DisplayName("Name")]

        public string ChildName { get; set; }
        [DisplayName("Blood Group")]
        public EnumBloodGroup ChildBloodGroup { get; set; }
        public string ChildBloodGroupName { get { return BaseFunctionalities.GetEnumDescription(this.ChildBloodGroup); } }
        public SelectList ChildBloodGroupList { get { return new SelectList(BaseFunctionalities.GetEnumList<EnumBloodGroup>(), "Value", "Text"); } }

        [DisplayName("Date Of Birth")]
        public DateTime ChildDOB { get; set; }
        public string ChildDOBControlString { get { return this.ChildDOB != DateTime.MinValue ? this.ChildDOB.ToString("yyyy-MM-dd") : ""; } }
        public string ChildDOBTableString { get { return this.ChildDOB != DateTime.MinValue ? this.ChildDOB.ToString("MMM dd, yyyy") : ""; } }
    }

    public class VMEmployeeEducation : BaseVM
    {
        [Required(ErrorMessage = "First Save Employee Info")]

        [Range(1, int.MaxValue, ErrorMessage = "First Save Employee Info")]
        public int VMEmployeeId { get; set; }

        [DisplayName("Degree")]
        public int VMDegreeId { get; set; }
        public string VMDegreeName { get; set; }
        public SelectList VMDegreeList { get; set; } = new SelectList(new List<object>());

        [DisplayName("Institute")]
        public string Institute { get; set; }

        [DisplayName("Group/Subject")]
        public string GroupOrSubject { get; set; }

        [DisplayName("Passing Year")]
        public string PassingYear { get; set; }

        [DisplayName("CGPA/GPA/Division")]
        public string GradingType { get; set; }


        [DisplayName("Result")]
        public string Result { get; set; }

    }

    public class VMEmployeeTraining : BaseVM
    {
        [Required(ErrorMessage = "First Save Employee Info")]

        [Range(1, int.MaxValue, ErrorMessage = "First Save Employee Info")]
        public int VMEmployeeId { get; set; }
        [DisplayName("Course Name")]
        public string CourseName { get; set; }
        [DisplayName("Institute")]
        public string Institute { get; set; }
        [DisplayName("Country")]
        public int VMCountryID { get; set; }
        public string VMCountryName { get; set; }
        public SelectList VMCountryList { get; set; } = new SelectList(new List<object>());
        [DisplayName("Start Date")]
        [DataType(DataType.Date)]
        public DateTime CourseStartDate { get; set; }
        public string CourseStartDateControlString { get { return this.CourseStartDate != DateTime.MinValue ? this.CourseStartDate.ToString("yyyy-MM-dd") : ""; } }
        public string CourseStartDateTableString { get { return this.CourseStartDate != DateTime.MinValue ? this.CourseStartDate.ToString("MMM dd, yyyy") : ""; } }
        [DisplayName("End Date")]
        [DataType(DataType.Date)]
        public DateTime CourseEndDate { get; set; }
        public string CourseEndDateControlString { get { return this.CourseEndDate != DateTime.MinValue ? this.CourseEndDate.ToString("yyyy-MM-dd") : ""; } }
        public string CourseEndDateTableString { get { return this.CourseEndDate != DateTime.MinValue ? this.CourseEndDate.ToString("MMM dd, yyyy") : ""; } }
        [DisplayName("Duration")]
        public string CourseDuration { get { return Math.Abs((this.CourseEndDate.Month - this.CourseStartDate.Month) + 12 * (this.CourseEndDate.Year - this.CourseStartDate.Year)).ToString() + " Months"; } }
    }

    public class VMEmployeeExperience : BaseVM
    {
        [Required(ErrorMessage = "First Save Employee Info")]

        [Range(1, int.MaxValue, ErrorMessage = "First Save Employee Info")]
        public int VMEmployeeId { get; set; }
        [DisplayName("Company Name")]
        public string CompanyName { get; set; }
        [DisplayName("Designation")]
        public string Designation { get; set; }
        [DisplayName("Department")]
        public string Department { get; set; }
        [DisplayName("Responsibility")]
        public string Responsibility { get; set; }
        [DisplayName("Start Date")]
        [DataType(DataType.Date)]
        public DateTime JobStartDate { get; set; }
        public string JobStartDateControlString { get { return this.JobStartDate != DateTime.MinValue ? this.JobStartDate.ToString("yyyy-MM-dd") : ""; } }
        public string JobStartDateTableString { get { return this.JobStartDate != DateTime.MinValue ? this.JobStartDate.ToString("MMM dd, yyyy") : ""; } }
        [DisplayName("End Date")]
        [DataType(DataType.Date)]
        public DateTime JobEndDate { get; set; }
        public string JobEndDateControlString { get { return this.JobEndDate != DateTime.MinValue ? this.JobEndDate.ToString("yyyy-MM-dd") : ""; } }
        public string JobEndDateTableString { get { return this.JobEndDate != DateTime.MinValue ? this.JobEndDate.ToString("MMM dd, yyyy") : ""; } }
        public string Duration { get { return Math.Abs((this.JobEndDate.Month - this.JobStartDate.Month) + 12 * (this.JobEndDate.Year - this.JobStartDate.Year)).ToString() + " Months"; } }
        [DisplayName("Job Location")]
        public string Location { get; set; }
        public string Remarks { get; set; }
    }

    public class VMEmployeeSkill : BaseVM
    {
        [Required(ErrorMessage = "First Save Employee Info")]

        [Range(1, int.MaxValue, ErrorMessage = "First Save Employee Info")]
        public int VMEmployeeId { get; set; }
        [DisplayName("Type")]
        public string SkillType { get; set; }
        [DisplayName("Skill")]
        public string SkillName { get; set; }
        [DisplayName("Description")]
        public string SkillDescription { get; set; }
    }

    public class VMEmployeeReference : BaseVM
    {
        [Required(ErrorMessage = "First Save Employee Info")]

        [Range(1, int.MaxValue, ErrorMessage = "First Save Employee Info")]
        public int VMEmployeeId { get; set; }
        [DisplayName("Name")]
        public string RefereeName { get; set; }
        [DisplayName("Contact No.")]
        public string ContactNo { get; set; }
        [DisplayName("Email")]
        [DataType(DataType.EmailAddress)]
        public string EmailId { get; set; }
        [DisplayName("Address")]
        public string Address { get; set; }
        [DisplayName("Relation")]
        public string Relation { get; set; }
        [DisplayName("Is Varified")]
        public bool IsVarified { get; set; }
        [DisplayName("Comments")]
        public string VarifyingComments { get; set; }

    }

    public class VMEmployeeNominee : BaseVM
    {
        [Required(ErrorMessage = "First Save Employee Info")]

        [Range(1, int.MaxValue, ErrorMessage = "First Save Employee Info")]
        public int VMEmployeeId { get; set; }

        [DisplayName("Name")]
        public string NomineeName { get; set; }
        [DisplayName("Father Name")]
        public string FatherName { get; set; }
        [DisplayName("Mother Name")]
        public string MotherName { get; set; }
        [DisplayName("Relation")]
        public string Relation { get; set; }
        [DisplayName("NID")]
        public string NID { get; set; }
        [DisplayName("Address")]
        public string Address { get; set; }
        [DisplayName("Contact No")]
        public string ContactNo { get; set; }
        [DisplayName("Email")]
        public string Email { get; set; }

    }
}