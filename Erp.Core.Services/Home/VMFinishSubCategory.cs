﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Erp.Core.Services.Home
{
    public class VMFinishSubCategory: BaseVM
    {
        public string Name { get; set; }

        public string FinishCategory { get; set; }
        [Required(ErrorMessage = "FinishCategory is Required.")]
        public int FinishCategoryFK { get; set; }
        public IEnumerable<VMFinishSubCategory> DataList { get; set; }
    }
}
