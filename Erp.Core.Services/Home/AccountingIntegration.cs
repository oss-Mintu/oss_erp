﻿using System;
using System.Collections.Generic;
using System.Text;
using Erp.Core.Entity.Accounting;
using System.Linq;
namespace Erp.Core.Services.Home
{
    public enum AccountingEntityType
    {
        Buyer = 1,
        Supplier,
        RawItem,
        FinishedItem,
        Journal,
    }
    public class AccountingIntegration : BaseService
    {
        private readonly int buyerParent = 1; //Remind Rezwan to handle it properly later
        //private readonly int supplierParent = 1; //Remind Rezwan to handle it properly later
        //private readonly int rawParent = 1; //Remind Rezwan to handle it properly later
        //private readonly int finishedParent = 1; //Remind Rezwan to handle it properly later
        public AccountingIntegration(IErpDbContext db) => _db = db;

        public int IntegrateBasic(AccountingEntityType accountingEntityType, IntegrationCRUD integrationCRUD, int id, string name, string remark)
        {
            if (accountingEntityType == AccountingEntityType.Buyer)
            {

                if(integrationCRUD == IntegrationCRUD.Add)
                {
                    Accounting_Head accounting_Head = new Accounting_Head { Accounting_Chart2FK = buyerParent, Name = name, Editable = false, Remarks = remark };
                    _db.AddAsync(accounting_Head);
                    _db.SaveChangesAsync();
                    return accounting_Head.ID;
                }
                else if (integrationCRUD == IntegrationCRUD.Edit)
                {
                    Accounting_Head accounting_Head = _db.Accounting_Head.Where(x => x.ID == id).FirstOrDefault();
                    accounting_Head.Name = name;
                    accounting_Head.Remarks = remark;
                    _db.SaveChangesAsync();
                    return accounting_Head.ID;
                }
                else 
                {
                   //Never Delete account info
                    return 0;
                }

            }
            else if (accountingEntityType == AccountingEntityType.Supplier)
            {
            }
            else if (accountingEntityType == AccountingEntityType.RawItem)
            { }
            else if (accountingEntityType == AccountingEntityType.FinishedItem)
            { }
            else 
            {
                return 0;
            }
            return 0;
        }
    }
}
