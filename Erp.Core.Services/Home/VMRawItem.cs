﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Erp.Core.Services.Home
{
    public class VMRawItem : BaseVM
    {
        public string Name { get; set; }
        [DisplayName("Category")]
        public string RawCategory { get; set; }
        [Required(ErrorMessage = "Category is Required.")]
        public int RawCategoryFK { get; set; }
        [DisplayName("SubCategory")]
        public string RawSubCategory { get; set; }
        [Required(ErrorMessage = "SubCategory is Required.")]
        public int RawSubCategoryFK { get; set; }
        public int TempFK { get; set; }
        public int TempFK2 { get; set; }
        public string Unit { get; set; }
        [Required(ErrorMessage = "Unit  is Required.")]
        public int Common_UnitFK { get; set; }
        public string GSM { get; set; }
        public double Quantity { get; set; }
        public int AccountHeadID { get; set; }
        public IEnumerable<VMRawItem> DataList { get; set; }
    }
    public class VmBrand : BaseVM
    {
        [DisplayName("Item Name")]
        public int RawItemFK { get; set; }
        public string Name { get; set; }
        public IEnumerable<VmBrand> DataList { get; set; }
        public SelectList RawItemList { get; set; } = new SelectList(new List<object>());
    }
    public class VmModel : BaseVM
    {
        [DisplayName("Brand Name")]
        public int BrandFK { get; set; }
        public string Name { get; set; }
        public IEnumerable<VmModel> DataList { get; set; }
        public string BrandName { get; internal set; }
    }
    public class VmLCType : BaseVM
    {
        public string Name { get; set; }
        public int ECIOrBBDFk { get; set; }
        public string ECIOrBBName { get; set; }

        public IEnumerable<VmLCType> DataList { get; set; }
    }
    public class VmApplicantBank : BaseVM
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string ContactNo { get; set; }
        public int? Common_CountryFK { get; set; }
        public string SwiftCode { get; set; }
        public IEnumerable<VmApplicantBank> DataList { get; set; }
    }
    public class VmBuyerBank : BaseVM
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string ContactNo { get; set; }
        public int? Common_CountryFK { get; set; }
        public string Country { get; set; }
        public string SwiftCode { get; set; }
        public int Common_BankFK { get; set; }
        public string ContactPerson { get; set; }
        public string BankName { get; set; }
        public int Common_BuyerFK { get; set; }
        public string BuyerName { get; set; }
        public string Email { get; set; }
        public List<VmBuyerBank> DataList { get; set; }
    }
    public class VmReceivingBank : BaseVM
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string ContactPerson { get; set; }
        public string ContactNo { get; set; }
        public string SwiftCode { get; set; }
        public int? Common_CountryFK { get; set; }
        public int Common_BankFK { get; set; }
        public string BankName { get; set; }
        public int Common_CompanySetupFK { get; set; }
        public string CompanySetup { get; set; }
        public string Email { get; set; }
        public IEnumerable<VmReceivingBank> DataList { get; set; }
        public List<VmReceivingBank> Datalist { get; set; }
    }
    public class VmLienBank : BaseVM
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string ContactPerson { get; set; }
        public string ContactNo { get; set; }
        public int? Common_CountryFK { get; set; }
        public string SwiftCode { get; set; }
        public int Common_BankFK { get; set; }
        public string BankName { get; set; }
        public string Email { get; set; }
        public IEnumerable<VmLienBank> DataList { get; set; }
    }
}
