﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Erp.Core.Services.Home
{
    public class VMRawSubCategory: BaseVM
    {
        public string Name { get; set; }
        public string RawCategory { get; set; }
        [Required(ErrorMessage = "Category is Required.")]
        public int RawCategoryFK { get; set; }
        public IEnumerable<VMRawSubCategory> DataList { get; set; }
    }
}
