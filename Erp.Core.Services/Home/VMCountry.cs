﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Erp.Core.Services.Home
{
   public class VMCountry: BaseVM
    {
        public string Name { get; set; }
        //public string Code { get; set; }

        public IEnumerable<VMCountry> DataList { get; set; }
    }
}
