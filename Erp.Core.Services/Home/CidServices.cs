﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace Erp.Core.Services.Home
{
    public class CidServices
    {
        private IErpDbContext _db { get; set; }
        public CidServices(IErpDbContext db) => _db = db;

        public string OrderCID(int buyerId, string buyerPO)
        {
            var vData = _db.Common_Buyer.FirstOrDefault(a => a.ID == buyerId).BuyerID;
            string serial = (_db.Merchandising_BuyerOrder.Count() + 1).ToString().PadLeft(5, '0');
            string month = DateTime.Today.Month > 9 ? DateTime.Today.Month.ToString() : "0" + DateTime.Today.Month;
            string OrderCID = vData + "/" + serial + "/" + (DateTime.Today.Year % 100).ToString() +"/"+ month + "/" + buyerPO;
            return OrderCID;
        }

        public string CBSStyleCID(int itemid)
        {
            string itemcode = _db.Common_FinishItem.FirstOrDefault(a => a.ID == itemid).Remarks;
            string counter = (_db.Merchandising_CBSStyle.Count() + 1).ToString().PadLeft(5, '0');
            string year = (DateTime.Today.Year % 100).ToString();
            string month = DateTime.Today.Month > 9 ? DateTime.Today.Month.ToString() : "0" + DateTime.Today.Month;
            string CBSStyleCID = "CBS/" + counter + "/" + year + "/" + month + "/" + itemcode;
            return CBSStyleCID;
        }

        public string GetStyleCID(int oid, int itemID)
        {
            int styleCount = _db.Merchandising_Style.Where(a => a.Merchandising_BuyerOrderFK == oid).Count() + 1;
            string OrderCID = _db.Merchandising_BuyerOrder.FirstOrDefault(a => a.ID == oid).CID;
            string itemcode = _db.Common_FinishItem.FirstOrDefault(a => a.ID == itemID).Remarks;
            string[] word = OrderCID.Split('/');
            string year = (DateTime.Today.Year % 100).ToString();
            string month = DateTime.Today.Month > 9 ? DateTime.Today.Month.ToString() : "0" + DateTime.Today.Month;
            string orderSerial = word[0] + "/" + word[1] + GetAlphabeticCode(styleCount) + "/" + year + "/" + month + "/" + itemcode;
            return orderSerial;
        }

        public string StyleCID()
        {
            var serial = _db.Merchandising_Style.Count();
            var counter = (serial + 1).ToString().PadLeft(3, '0');
            string StyleCID = DateTime.Today.Year % 100 + "/" + counter;
            return StyleCID;
        }

        public string CBSCID(int buyerId, int styleId)
        {
            var vData = _db.Common_Buyer.FirstOrDefault(a => a.ID == buyerId).BuyerID;
            string itemcode = (from t1 in _db.Merchandising_CBSStyle
                               join t2 in _db.Common_FinishItem on t1.Common_FinishItemFK equals t2.ID
                               select new { code = t2.Remarks }).FirstOrDefault().code;

            string serial = (_db.Merchandising_CBS.Count() + 1).ToString().PadLeft(5, '0');
            string month = DateTime.Today.Month > 9 ? DateTime.Today.Month.ToString() : "0" + DateTime.Today.Month;
            string CBSCID = "CBS/" + vData + "/" + serial + "/" + (DateTime.Today.Year % 100).ToString() + "/" + month + "/" + itemcode;
            return CBSCID;
        }

        public int GetBinMaxId()
        {
            try
            {
                return _db.Store_Bin.Any() == false ? 1 : _db.Store_Bin.Max(p => p == null ? 1 : p.ID + 1);

            }
            catch (Exception ex)
            {
                return 100000000;
            }
        }

        public string ProductionCID()
        {
            var serial = _db.Prod_Reference.Count();
            var counter = (serial + 1).ToString().PadLeft(3, '0');
            string PDRCID = "DPR/" + DateTime.Today.Year % 100 + "/" + counter;
            return PDRCID;
        }

        public string ProductionSectionCID(int SectionID,DateTime RefDate,int RefID)
        {
            string CID = string.Empty;
            int Counter = 0;
            string month = RefDate.Month > 9 ? RefDate.Month.ToString() : "0" + RefDate.Month;
            string day = RefDate.Day > 9 ? RefDate.Day.ToString() : "0" + RefDate.Day;
            Counter = _db.Prod_ReferencePlan.Where(a => a.Prod_ReferenceFK == RefID && a.Active && a.SectionId == SectionID).Count() + 1;
            switch (SectionID)
            {
                case 1:
                    CID = "PCT" + (DateTime.Today.Year % 100).ToString() + month + day + Counter.ToString().PadLeft(2, '0');
                    break;
                case 2:
                    CID = "PSE" + (DateTime.Today.Year % 100).ToString() + month + day + Counter.ToString().PadLeft(2, '0');
                    break;
                case 3:
                    CID = "PIR" + (DateTime.Today.Year % 100).ToString() + month + day + Counter.ToString().PadLeft(2, '0');
                    break;
                case 4:
                    CID = "PPK" + (DateTime.Today.Year % 100).ToString() + month + day + Counter.ToString().PadLeft(2, '0');
                    break;
            }

            return CID;
        }

        public string GetChallanNo(string deptId)
        {
            var c = string.Empty;
            if (!string.IsNullOrEmpty(deptId))
            {
                c = _db.User_Department.FirstOrDefault(x => x.ID == Convert.ToInt32(deptId)).Name.Substring(0, 1);
            }
            try
            {
                var yourValue = _db.Store_StockOut.Any() == false ? 1 : _db.Store_StockOut.Max(x => x.ID) + 1;
                return "SIC" + DateTime.Now.Year.ToString().Substring(2, 2) + DateTime.Now.Month.ToString().PadLeft(2, '0') + '-' + yourValue.ToString().PadLeft(5, '0') + "-" + c;
            }
            catch (Exception ex)
            {
                return "SIC1908-00001-C";
            }
        }
        public string GetRefStoreIn(string deptId)
        {
            var c = string.Empty;
            if (!string.IsNullOrEmpty(deptId))
            {
                c = _db.User_Department.FirstOrDefault(x => x.ID == Convert.ToInt32(deptId)).Name.Substring(0, 1);
            }
            try
            {
                var yourValue = _db.Store_StockIn.Any() == false ? 1 : _db.Store_StockIn.Max(x => x.ID) + 1;
                return "SIC" + DateTime.Now.Year.ToString().Substring(2, 2) + DateTime.Now.Month.ToString().PadLeft(2, '0') + '-' + yourValue.ToString().PadLeft(5, '0') + "-" + c;
            }
            catch (Exception ex)
            {
                return "SIC1908-00001-C";
            }
        }
        public string GetRefStoreOut(string deptId)
        {
            var c = string.Empty;
            if (!string.IsNullOrEmpty(deptId))
            {
                c = _db.User_Department.FirstOrDefault(x => x.ID == Convert.ToInt32(deptId)).Name.Substring(0, 1);
            }
            try
            {
                var yourValue = _db.Store_StockOut.Any() == false ? 1 : _db.Store_StockOut.Max(x => x.ID) + 1;
                return "SO" + DateTime.Now.Year.ToString().Substring(2, 2) + DateTime.Now.Month.ToString().PadLeft(2, '0') + '-' + yourValue.ToString().PadLeft(5, '0') + "-" + c;
            }
            catch (Exception ex)
            {
                return "SO1908-00001-C";
            }
        }
        public string GetRefStoreOutSaleReturn(string deptId)
        {
            var c = string.Empty;
            if (!string.IsNullOrEmpty(deptId))
            {
                c = _db.User_Department.FirstOrDefault(x => x.ID == Convert.ToInt32(deptId)).Name.Substring(0, 1);
            }
            try
            {
                var yourValue = _db.Store_StockOut.Any() == false ? 1 : _db.Store_StockOut.Max(x => x.ID) + 1;
                return "SOSR" + DateTime.Now.Year.ToString().Substring(2, 2) + DateTime.Now.Month.ToString().PadLeft(2, '0') + '-' + yourValue.ToString().PadLeft(5, '0') + "-" + c;
            }
            catch (Exception ex)
            {
                return "SOSR1908-00001-C";
            }
        }
        public string GetRequisitionRefNo(string deptId)
        {
            try
            {
                var c = string.Empty;
                if (!string.IsNullOrEmpty(deptId))
                {
                    c = _db.User_Department.FirstOrDefault(x => x.ID == Convert.ToInt32(deptId)).Name.Substring(0, 1);
                }

                var yourValue = _db.Store_Requisition.Any() == false ? 1 : _db.Store_Requisition.MaxAsync(x => x.ID).Result + 1;
                return "SR" + DateTime.Now.Year.ToString().Substring(2, 2) + DateTime.Now.Month.ToString().PadLeft(2, '0') + '-' + yourValue.ToString().PadLeft(5, '0') + "-" + c;
            }
            catch (Exception ex)
            {
                return "SR1908-00001-C";
            }
        }

        public string GetRefStoreInMaster(string deptId)
        {
            var c = string.Empty;
            if (!string.IsNullOrEmpty(deptId))
            {
                c = _db.User_Department.FirstOrDefault(x => x.ID == Convert.ToInt32(deptId)).Name.Substring(0, 1);
            }

            try
            {
                var yourValue = _db.Store_StockInMaster.Any() == false ? 1 : _db.Store_StockInMaster.Max(x => x.ID) + 1;
                return "SIC" + DateTime.Now.Year.ToString().Substring(2, 2) + DateTime.Now.Month.ToString().PadLeft(2, '0') + '-' + yourValue.ToString().PadLeft(5, '0') + "-" + c;
            }
            catch (Exception ex)
            {
                return "SIC1908-00001-C";
            }
        }
        public string GetRefStoreInSlave(string deptId)
        {
            var c = string.Empty;
            if (!string.IsNullOrEmpty(deptId))
            {
                c = _db.User_Department.FirstOrDefault(x => x.ID == Convert.ToInt32(deptId)).Name.Substring(0, 1);
            }

            try
            {
                var yourValue = _db.Store_StockIn.Any() == false ? 1 : _db.Store_StockIn.Max(x => x.ID) + 1;
                return "SIC" + DateTime.Now.Year.ToString().Substring(2, 2) + DateTime.Now.Month.ToString().PadLeft(2, '0') + '-' + yourValue.ToString().PadLeft(5, '0') + "A-" + c;
            }
            catch (Exception ex)
            {
                return "SIC1908-00001A-C";
            }
        }

        public string GetRefStoreOutMaster(string deptId)
        {
            var c = string.Empty;
            if (!string.IsNullOrEmpty(deptId))
            {
                c = _db.User_Department.FirstOrDefault(x => x.ID == Convert.ToInt32(deptId)).Name.Substring(0, 1);
            }

            try
            {
                var yourValue = _db.Store_StockOutMaster.Any() == false ? 1 : _db.Store_StockOutMaster.Max(x => x.ID) + 1;
                return "SOC" + DateTime.Now.Year.ToString().Substring(2, 2) + DateTime.Now.Month.ToString().PadLeft(2, '0') + '-' + yourValue.ToString().PadLeft(5, '0') + "A-" + c;
            }
            catch (Exception ex)
            {
                return "SOC1908-00001A-C";
            }
        }

        public string GetFinishItemCID(int catid)
        {
            string catCode = _db.Common_FinishCategory.FirstOrDefault(a => a.ID == catid).Remarks;
            int counter = (from t1 in _db.Common_FinishItem
                           join t2 in _db.Common_FinishSubCategory on t1.Common_FinishSubCategoryFK equals t2.ID
                           where t2.Common_FinishCategoryFK == catid
                           select new { }).Count() + 1;
            string code = catCode + counter.ToString().PadLeft(2, '0');
            return code;
        }

        public string GetAlphabeticCode(int counter)
        {
            String[] allphabet = new string[26] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
            if (counter <= 26 && counter >= 1)
            {
                return allphabet[--counter];
            }
            else
            {
                return "";
            }
        }


        public string ProductionUnitCode(int unitId)
        {
            string UnitCode = string.Empty;
            var vUnit = _db.HRMS_Unit.Where(c => c.Active == true).ToList();
            int i = 0;
            int ProductionUnit = 1;
            ProductionUnit += _db.Common_ProductionUnit.Count(a => a.HRMS_UnitFK == unitId && a.Active==true);
            if (vUnit.Any())
            {
                foreach(var v in vUnit)
                {
                    ++i;
                    if (unitId==v.ID)
                    {
                        UnitCode = GetAlphabeticCode(i) + ProductionUnit;
                    }
                }
            }
            return UnitCode;
        }
        public string ProLineCode(int stepId,int FloorUnit)
        {
            string UnitCode = string.Empty;
            var FloorUnitCode = (from t1 in _db.Common_SectionLine
                                 join t2 in _db.Common_ProductionFloor on t1.Common_ProductionFloorFK equals t2.ID
                                 join t3 in _db.Common_ProductionUnit on t2.Common_ProductionUnitFK equals t3.ID
                                 where t1.ID == FloorUnit && t1.Active == true
                                 select new
                                 {
                                     Cid = t3.Code+ t2.Code+ t1.Code

                                 }).ToList();

            if (FloorUnitCode.Any())
            {
                UnitCode = FloorUnitCode.FirstOrDefault().Cid;
            }
           int ProductionLine = 1;
            ProductionLine += _db.Common_ProductionLine.Count(a => a.ProductionStepFK==stepId && a.Active==true);
          
            switch (stepId)
            {
                case (int)EnumProcess.Cutting:
                  
                    UnitCode = UnitCode + "C"+GetCounter(ProductionLine);
                    break;
                case (int)EnumProcess.Sewing:
                   
                    UnitCode = UnitCode + "S" + GetCounter(ProductionLine);
                    break;
                case (int)EnumProcess.Ironning:
                    
                    UnitCode = UnitCode + "I" + GetCounter(ProductionLine);
                    break;
                case (int)EnumProcess.Packing:
                   
                    UnitCode = UnitCode + "P" + GetCounter(ProductionLine);
                    break;
            }

           return UnitCode;
        }

        private string GetCounter(int Number)
        {
            string cNumber = string.Empty;

            cNumber = Number > 9 == true ? Number.ToString() : Number.ToString().PadLeft(2, '0');

            return cNumber;
        }
    }
}
