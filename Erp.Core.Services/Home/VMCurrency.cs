﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Erp.Core.Services.Home
{
    public class VMCurrency:BaseVM
    {
        public string Name { get; set; }
        public decimal ConversionRateToBDT { get; set; }
        public decimal PreviousConversionRateToBDT { get; set; }
        public IEnumerable<VMCurrency> DataList { get; set; }

    }
}
