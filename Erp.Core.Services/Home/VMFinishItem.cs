﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Erp.Core.Services.Home
{
    public class VMFinishItem: BaseVM
    {
        public string Name { get; set; }
        public string FinishSubCategory { get; set; }
        //[Required(ErrorMessage = "FinishSubCategory is Required.")]
        public int FinishSubCategoryFK { get; set; }

        public string FinishCategory { get; set; }
        [Required(ErrorMessage = "FinishCategory is Required.")]
        public int FinishCategoryFK { get; set; }
        public string Unit { get; set; }
        [Required(ErrorMessage = "Unit  is Required.")]
        public int Common_UnitFK { get; set; }
       // public string Code { get; set; }
        public IEnumerable<VMFinishItem> DataList { get; set; }

    }
}
