﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Erp.Core.Services.Home
{
    public class VMLogin : BaseVM
    {
        public string UserName { get; set; } 
        public string Password { get; set; }

        public string LblError { get; set; }
        public bool Locked { get;  set; }
        public int UserAccessLevelId { get; set; }
        public int UserDepartmentId { get; set; }

        public int EmployeeId { get; set; }
        public string Mobile { get; set; }
        public int User_RoleFK { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public int HRMS_EmployeeFK { get; set; }
        public string Photo { get; set; }
        public string Name { get; set; }
    }
}
