﻿using Erp.Core.Entity.Common;
using Erp.Core.Entity.Merchandising;
using Erp.Core.Services.Integration;
using Erp.Core.Services.Merchandising;
using Erp.Core.Services.User;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Erp.Core.Services.Store;
using Erp.Core.Services.Production;

namespace Erp.Core.Services.Home
{
    public class HomeService : BaseService
    {
        private readonly HttpContext _httpContext;


        public HomeService(IErpDbContext db, HttpContext httpContext)
        {
            _db = db;
            CidServices = new CidServices(db);
            _httpContext = httpContext;
        }

        public VMLogin UserLogin(string userName, string password)
        {
            string hashedPassword = GetHashedPassword(password);
            int userId = 0;
            var v = (from t1 in _db.User_User
                     where t1.UserName == userName && t1.Password == hashedPassword && t1.Active == true
                     select new VMLogin
                     {
                         Name = t1.Name,
                         Photo = t1.Photo,
                         HRMS_EmployeeFK = t1.HRMS_EmployeeFK,
                         Email = t1.Email,
                         Address = t1.Address,
                         UserName = t1.UserName,
                         Mobile = t1.Mobile,
                         User_RoleFK = t1.User_RoleFK,
                         ID = t1.ID,
                         Locked = t1.Locked,
                         Password = t1.Password,
                         UserAccessLevelId = t1.User_AccessLevelFK,
                         UserDepartmentId = t1.User_DepartmentFK,
                         EmployeeId = t1.HRMS_EmployeeFK
                     }).FirstOrDefault();
            if (v != null)
            {
                userId = v.ID;
            }
            if (userId > 0)
            {
                if (v.Locked == true)
                {
                    v.LblError = "You are not authorised!";
                    return v;
                }
                else
                {
                    DefinePermission(userId);
                    return v;
                }
            }
            else
            {
                VMLogin vM = new VMLogin();
                vM.LblError = "User / Password does not match!";
                return vM;
            }

        }
        private void DefinePermission(int userId)
        {


            string tempMenu = GetMenu(userId);
            int tml = tempMenu.Length / 10;


            _httpContext.Session.SetString("Menu1", tempMenu.Substring(0, tml));
            _httpContext.Session.SetString("Menu2", tempMenu.Substring(tml, tml));
            _httpContext.Session.SetString("Menu3", tempMenu.Substring(tml * 2, tml));
            _httpContext.Session.SetString("Menu4", tempMenu.Substring(tml * 3, tml));
            _httpContext.Session.SetString("Menu5", tempMenu.Substring(tml * 4, tml));
            _httpContext.Session.SetString("Menu6", tempMenu.Substring(tml * 5, tml));
            _httpContext.Session.SetString("Menu7", tempMenu.Substring(tml * 6, tml));
            _httpContext.Session.SetString("Menu8", tempMenu.Substring(tml * 7, tml));
            _httpContext.Session.SetString("Menu9", tempMenu.Substring(tml * 8, tml));
            _httpContext.Session.SetString("Menu10", tempMenu.Substring(tml * 9, tempMenu.Length - (tml * 9)));

            string tempAMenu = GetAloneMethod(userId);
            tml = tempAMenu.Length / 10;

            _httpContext.Session.SetString("Alone1", tempAMenu.Substring(0, tml));
            _httpContext.Session.SetString("Alone2", tempAMenu.Substring(tml, tml));
            _httpContext.Session.SetString("Alone3", tempAMenu.Substring(tml * 2, tml));
            _httpContext.Session.SetString("Alone4", tempAMenu.Substring(tml * 3, tml));
            _httpContext.Session.SetString("Alone5", tempAMenu.Substring(tml * 4, tml));
            _httpContext.Session.SetString("Alone6", tempAMenu.Substring(tml * 5, tml));
            _httpContext.Session.SetString("Alone7", tempAMenu.Substring(tml * 6, tml));
            _httpContext.Session.SetString("Alone8", tempAMenu.Substring(tml * 7, tml));
            _httpContext.Session.SetString("Alone9", tempAMenu.Substring(tml * 8, tml));
            _httpContext.Session.SetString("Alone10", tempAMenu.Substring(tml * 9, tempAMenu.Length - (tml * 9)));

        }

        public string GetMenu(int userId)
        {
            string str = "";
            var v = (from t1 in _db.User_RoleMenuItem
                     join t2 in _db.User_MenuItem on t1.User_MenuItemFk equals t2.ID
                     join t3 in _db.User_SubMenu on t2.User_SubMenuFk equals t3.ID
                     join t4 in _db.User_Menu on t3.User_MenuFk equals t4.ID
                     where t1.User_RoleFK == (from userUser in _db.User_User where userUser.ID == userId select userUser.User_RoleFK).FirstOrDefault()
                     && t2.IsAlone == false && t2.Active == true && t1.IsAllowed == true
                     select new
                     {
                         Menu = t4.Name,
                         SubMenu = t3.Name,
                         ItemMenu = t2.Name,
                         Method = t2.Method,
                         SUbMenuFK = t2.User_SubMenuFk,
                         MenuFk = t3.User_MenuFk,
                         Priority = t4.Priority
                     }).OrderBy(x => x.SUbMenuFK).OrderBy(x => x.MenuFk).OrderBy(x => x.Priority).ToList().Distinct();
            string Menu = "", SubMenu = "", Menuitem = "", Method = "";
            foreach (var a in v)
            {
                if (Menu != a.Menu)
                {
                    Menu = a.Menu;
                    str += "]" + a.Menu;
                }
                Menu = a.Menu;

                if (SubMenu != a.SubMenu)
                {
                    SubMenu = a.SubMenu;
                    str += "$" + a.SubMenu;
                }
                Menuitem = a.ItemMenu;
                Method = a.Method;
                str += "#" + Menuitem + "|" + Method + "^"; // Menu +
                //str += a.Menu + "!" + a.SubMenu + "$" + a.ItemMenu + "#" + a.Method + "|";
            }
            //string readyStr = FirstCharToUpper(str);
            return str + "=";
        }
        public static string FirstCharToUpper(string value)
        {
            char[] array = value.ToCharArray();
            // Handle the first letter in the string.  
            if (array.Length >= 1)
            {
                if (char.IsLower(array[0]))
                {
                    array[0] = char.ToUpper(array[0]);
                }
            }
            // Scan through the letters, checking for spaces.  
            // ... Uppercase the lowercase letters following spaces.  
            for (int i = 1; i < array.Length; i++)
            {
                if (array[i - 1] == ' ')
                {
                    if (char.IsLower(array[i]))
                    {
                        array[i] = char.ToUpper(array[i]);
                    }
                }
            }
            return new string(array);
        }
        public string GetAloneMethod(int userId)
        {

            string str = "";
            var v = (from t1 in _db.User_RoleMenuItem
                     join t2 in _db.User_MenuItem on t1.User_MenuItemFk equals t2.ID
                     where t1.User_RoleFK == (from userUser in _db.User_User where userUser.ID == userId select userUser.User_RoleFK).FirstOrDefault()
                     && t2.IsAlone == true
                     && t2.Active == true
                       && t1.IsAllowed == true
                     select new
                     {
                         Method = t2.Method,
                         //Name = t3.Name

                     }).ToList();
            foreach (var a in v)
            {
                string temp = a.Method.Replace("/", "@(");
                str += "@" + temp + ")|";
            }
            return str;
        }

        public bool CheckPermission(string Controller, string Method, string routeValue, string routeValue2)
        {

            bool flag = false;
            Controller = Controller.ToLower();
            Method = Method.ToLower();

            var MenuC1 = _httpContext.Session.GetString("Menu1");
            var MenuC2 = _httpContext.Session.GetString("Menu2");
            var MenuC3 = _httpContext.Session.GetString("Menu3");
            var MenuC4 = _httpContext.Session.GetString("Menu4");
            var MenuC5 = _httpContext.Session.GetString("Menu5");
            var MenuC6 = _httpContext.Session.GetString("Menu6");
            var MenuC7 = _httpContext.Session.GetString("Menu7");
            var MenuC8 = _httpContext.Session.GetString("Menu8");
            var MenuC9 = _httpContext.Session.GetString("Menu9");
            var MenuC10 = _httpContext.Session.GetString("Menu10");
            var AloneC1 = _httpContext.Session.GetString("Alone1");
            var AloneC2 = _httpContext.Session.GetString("Alone2");
            var AloneC3 = _httpContext.Session.GetString("Alone3");
            var AloneC4 = _httpContext.Session.GetString("Alone4");
            var AloneC5 = _httpContext.Session.GetString("Alone5");
            var AloneC6 = _httpContext.Session.GetString("Alone6");
            var AloneC7 = _httpContext.Session.GetString("Alone7");
            var AloneC8 = _httpContext.Session.GetString("Alone8");
            var AloneC9 = _httpContext.Session.GetString("Alone9");
            var AloneC10 = _httpContext.Session.GetString("Alone10");


            string menu = AloneC1.ToString().ToLower() + AloneC2.ToString().ToLower() + AloneC3.ToString().ToLower() + AloneC4.ToString().ToLower() + AloneC5.ToString().ToLower() + AloneC6.ToString().ToLower() + AloneC7.ToString().ToLower() + AloneC8.ToString().ToLower() + AloneC9.ToString().ToLower() + AloneC10.ToString().ToLower()
                       + MenuC1.ToString().ToLower() + MenuC2.ToString().ToLower() + MenuC3.ToString().ToLower() + MenuC4.ToString().ToLower() + MenuC5.ToString().ToLower() + MenuC6.ToString().ToLower() + MenuC7.ToString().ToLower() + MenuC8.ToString().ToLower() + MenuC9.ToString().ToLower() + MenuC10.ToString().ToLower();
            if (routeValue != null && routeValue2 != null)
            {
                string aloneUrlWithId = "@" + Controller + "@(" + Method + "/" + routeValue + "/" + routeValue2 + ")";
                if (menu.Contains(aloneUrlWithId))
                {
                    flag = true;
                }
                aloneUrlWithId = Controller + "/" + Method + "/" + routeValue + "/" + routeValue2 + "^";
                if (menu.Contains(aloneUrlWithId))
                {
                    flag = true;
                }
            }

            if (routeValue != null)
            {
                string aloneUrlWithId = "@" + Controller + "@(" + Method + "/" + routeValue + ")";
                if (menu.Contains(aloneUrlWithId))
                {
                    flag = true;
                }
                aloneUrlWithId = Controller + "/" + Method + "/" + routeValue + "^";
                if (menu.Contains(aloneUrlWithId))
                {
                    flag = true;
                }
            }

            if (menu.Contains("@" + Controller + "@(" + Method + ")"))
            {
                flag = true;
            }

            if (menu.Contains(Controller + "/" + Method + "^"))
            {
                flag = true;
            }
            return flag;
        }
        public string GetHashedPassword(string password)
        {
            char[] array = password.ToCharArray();
            Array.Reverse(array);
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(array);
            byte[] hash = md5.ComputeHash(inputBytes);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("x2"));
            }
            return sb.ToString();
        }

        #region Unit
        public async Task<VMUnit> GetUnit()
        {
            VMUnit _unit = new VMUnit
            {
                DataList = await Task.Run(() => _db.Common_Unit.Where(x => x.Active == true).ToList().ConvertAll(
                    x => new VMUnit { ID = x.ID, Name = x.Name }).OrderByDescending(x => x.ID))
            };
            return _unit;
        }
        public async Task<int> UnitAdd(VMUnit _unit)
        {
            var result = -1;

            Common_Unit unit = new Common_Unit
            {
                Name = _unit.Name,
                User = "UserOne",
            };
            _db.Common_Unit.Add(unit);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = unit.ID;
            }
            return result;
        }
        public async Task<int> UnitEdit(VMUnit _unit)
        {
            var result = -1;

            Common_Unit unit = _db.Common_Unit.Find(_unit.ID);

            unit.Name = _unit.Name;


            if (await _db.SaveChangesAsync() > 0)
            {
                result = unit.ID;
            }
            return result;
        }
        public async Task<int> UnitDelete(int id)
        {
            var result = -1;
            Common_Unit _unit = _db.Common_Unit.Find(id);
            if (_unit != null)
            {
                _unit.Active = false;
                //_db.Common_Unit.Remove(_unit);
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = _unit.ID;
                }
            }
            return result;
        }
        #endregion
        #region Buyer
        private bool CheckBuyerBank(int Id)
        {
            var vData = _db.Common_BuyerBank.Any(a => a.Common_BuyerFK == Id && a.Active == true);
            return vData;

        }
        private bool CheckNotifyParty(int Id)
        {
            var vData = _db.Common_BuyerNotifyParty.Any(a => a.Common_BuyerFK == Id && a.Active == true);
            return vData;

        }
        public async Task<VMBuyer> GetBuyer()
        {
            var vData = (from x in _db.Common_Buyer
                         join y in _db.Common_Country on x.Common_CountryFK equals y.ID
                         where x.Active == true
                         select new VMBuyer
                         {
                             ID = x.ID,
                             Name = x.Name,
                             BuyerID = x.BuyerID,
                             Address = x.Address,
                             ContactPerson = x.ContactPerson,
                             Phone = x.Phone,
                             Common_CountryFK = x.Common_CountryFK,
                             Country = y.Name,
                             Email = x.Email

                         }).OrderByDescending(a => a.ID).ToList();

            vData.ForEach(x =>
            {
                x.Exist = CheckBuyerBank(x.ID) == true ? "label-success" : "label-danger";
                x.Exist1 = CheckNotifyParty(x.ID) == true ? "label-success" : "label-danger";
            });
            VMBuyer _buyer = new VMBuyer
            {
                DataList = await Task.Run(() => vData)
            };
            return _buyer;
        }
        public VMBuyer GetBuyerByID(int ID)
        {
            var vData = (from x in _db.Common_Buyer
                         where x.ID == ID
                         select new VMBuyer
                         {
                             ID = x.ID,
                             Name = x.Name,
                             BuyerID = x.BuyerID,
                             Address = x.Address,
                             ContactPerson = x.ContactPerson,
                             Phone = x.Phone,
                             Common_CountryFK = x.Common_CountryFK,
                             Email = x.Email,
                             ActionId = 2
                         }).FirstOrDefault();
            return vData;
        }
        public async Task<int> BuyerAdd(VMBuyer _buyer)
        {
            var result = -1;
            IntegrationService _integrationService = new IntegrationService(_db, _httpContext);
            if (!_db.Common_Buyer.Any(a => a.Name.Equals(_buyer.Name)))
            {
                var acHeadId = await _integrationService.AccountingHeadCreate(_buyer.Name, 1289);
                Common_Buyer buyer = new Common_Buyer
                {
                    Name = _buyer.Name,
                    BuyerID = _buyer.BuyerID,
                    Phone = _buyer.Phone,
                    ContactPerson = _buyer.ContactPerson,
                    Email = _buyer.Email,
                    Address = _buyer.Address,
                    User = "User",
                    Common_CountryFK = _buyer.Common_CountryFK,
                    AccHeadID = acHeadId
                };
                _db.Common_Buyer.Add(buyer);

                if (await _db.SaveChangesAsync() > 0)
                {
                    result = buyer.ID;
                    await _integrationService.AccountingHeadAdjust(acHeadId, result, (int)IntegratedAccountHeadTypeEnum.Buyer);
                }
            }
            return result;
        }
        public async Task<int> BuyerEdit(VMBuyer _buyer)
        {
            var result = -1;
            IntegrationService _integrationService = new IntegrationService(_db, _httpContext);
            Common_Buyer buyer = _db.Common_Buyer.Find(_buyer.ID);
            var checkName = _db.Common_Buyer.Where(a => a.Name.Equals(_buyer.Name));
            if (buyer != null)
            {
                if (checkName.Any())
                {
                    if (buyer.ID == checkName.FirstOrDefault().ID)
                    {
                        buyer.Name = _buyer.Name;
                        buyer.BuyerID = _buyer.BuyerID;
                        buyer.Phone = _buyer.Phone;
                        buyer.ContactPerson = _buyer.ContactPerson;
                        buyer.Email = _buyer.Email;
                        buyer.Address = _buyer.Address;
                        buyer.Common_CountryFK = _buyer.Common_CountryFK;
                        var aHeadId = await _integrationService.AccountingHeadEdit(buyer.AccHeadID, buyer.Name);
                        buyer.AccHeadID = aHeadId;
                        _db.Common_Buyer.Update(buyer);

                        if (await _db.SaveChangesAsync() > 0)
                        {
                            result = buyer.ID;
                            await _integrationService.AccountingHeadAdjust(aHeadId, result, (int)IntegratedAccountHeadTypeEnum.Supplier);
                        }
                    }
                }
                else
                {
                    buyer.Name = _buyer.Name;
                    buyer.BuyerID = _buyer.BuyerID;
                    buyer.Phone = _buyer.Phone;
                    buyer.ContactPerson = _buyer.ContactPerson;
                    buyer.Email = _buyer.Email;
                    buyer.Address = _buyer.Address;
                    buyer.Common_CountryFK = _buyer.Common_CountryFK;
                    if (await _db.SaveChangesAsync() > 0)
                    {
                        result = buyer.ID;
                        await _integrationService.AccountingHeadEdit(buyer.AccHeadID, buyer.Name);
                        if (result == -1)
                        {
                            result = buyer.ID;
                        }
                    }
                }
            }
            return result;
        }
        public async Task<int> BuyerDelete(int id)
        {
            var result = -1;
            IntegrationService _integrationService = new IntegrationService(_db, _httpContext);
            Common_Buyer _buyer = _db.Common_Buyer.Find(id);
            await _integrationService.AccountingHeadEdit(_buyer.AccHeadID, "Deleted" + _buyer.Name);
            if (_buyer != null)
            {
                _buyer.Active = false;
                //_db.Common_Buyer.Remove(_buyer);
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = _buyer.ID;
                }
            }
            return result;
        }
        #endregion
        #region BuyerNotifyParty
        public List<VMBuyerNotify> GetBuyerNotifyByBuyerID(int BuyerID)
        {
            var vData = (from x in _db.Common_BuyerNotifyParty
                         join z in _db.Common_Buyer on x.Common_BuyerFK equals z.ID
                         join y in _db.Common_Country on x.Common_CountryFK equals y.ID
                         where x.Active == true && x.Common_BuyerFK == BuyerID
                         select new VMBuyerNotify
                         {
                             ID = x.ID,
                             Name = x.Name,
                             //Common_BuyerFK = x.Common_BuyerFK,
                             Address = x.Address,
                             ContactPerson = x.ContactPerson,
                             Phone = x.Phone,
                             Common_CountryFK = x.Common_CountryFK,
                             CountryName = y.Name,
                             Email = x.Email
                         }).ToList();

            return vData;
        }

        public VMBuyerNotify GetBuyerNotify(int id)
        {

            var vData = _db.Common_Buyer.Where(a => a.ID == id).Select(a => new VMBuyerNotify
            {
                Common_BuyerFK = a.ID,
                BuyerName = a.Name

            }).FirstOrDefault();

            VMBuyerNotify mBuyerNotify = new VMBuyerNotify();
            mBuyerNotify.Common_BuyerFK = vData.Common_BuyerFK;
            mBuyerNotify.BuyerName = vData.BuyerName;
            mBuyerNotify.DataList = new List<VMBuyerNotify>();
            mBuyerNotify.DataList = GetBuyerNotifyByBuyerID(id);
            return mBuyerNotify;
        }

        public async Task<int> BuyerNotifyAdd(VMBuyerNotify vMBuyerNotify)
        {
            var result = -1;

            Common_BuyerNotifyParty notifyParty = new Common_BuyerNotifyParty();
            notifyParty.Common_CountryFK = vMBuyerNotify.Common_CountryFK;
            notifyParty.Common_BuyerFK = vMBuyerNotify.Common_BuyerFK;
            notifyParty.Name = vMBuyerNotify.Name;
            notifyParty.ContactPerson = vMBuyerNotify.ContactPerson;
            notifyParty.Phone = vMBuyerNotify.Phone;
            notifyParty.Email = vMBuyerNotify.Email;
            notifyParty.Address = vMBuyerNotify.Address;
            notifyParty.User = "UserOne";

            _db.Common_BuyerNotifyParty.Add(notifyParty);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = notifyParty.ID;
            }
            return result;
        }
        public async Task<int> BuyerNotifyEdit(VMBuyerNotify vMBuyerNotify)
        {
            var result = -1;

            Common_BuyerNotifyParty notifyParty = _db.Common_BuyerNotifyParty.Find(vMBuyerNotify.ID);

            notifyParty.Common_CountryFK = vMBuyerNotify.Common_CountryFK;
            notifyParty.Name = vMBuyerNotify.Name;
            notifyParty.ContactPerson = vMBuyerNotify.ContactPerson;
            notifyParty.Phone = vMBuyerNotify.Phone;
            notifyParty.Email = vMBuyerNotify.Email;
            notifyParty.Address = vMBuyerNotify.Address;


            if (await _db.SaveChangesAsync() > 0)
            {
                result = notifyParty.ID;
            }
            return result;
        }
        public async Task<int> BuyerNotifyDelete(int id)
        {
            var result = -1;
            Common_BuyerNotifyParty notifyParty = _db.Common_BuyerNotifyParty.Find(id);
            if (notifyParty != null)
            {
                notifyParty.Active = false;

                if (await _db.SaveChangesAsync() > 0)
                {
                    result = notifyParty.ID;
                }
            }
            return result;
        }

        #endregion
        #region Supplier
        public async Task<VMSupplier> GetSupplier()
        {
            VMSupplier vMSupplier = new VMSupplier
            {
                DataList = await Task.Run(() => _db.Common_Supplier
                .Include(x => x.Common_Country)
                .Where(a => a.Active == true).ToList()
                .ConvertAll(
                    x => new VMSupplier
                    {
                        ID = x.ID,
                        Code = x.Code,
                        Name = x.Name,
                        VMCommon_CountryID = x.Common_CountryFK,
                        VMCommon_CountryName = x.Common_Country.Name,
                        IsForeignSupplier = x.IsForeignSupplier,
                        SupplierType = (EnumSupplierType)x.SupplierType,
                        Email = x.Email,
                        Phone = x.Phone,
                        Address = x.Address,
                        ContactPerson=x.ContactPerson
                    }).OrderByDescending(x => x.ID))
            };
            return vMSupplier;
        }
        public async Task<int> SupplierAdd(VMSupplier vMSupplier)
        {
            var result = -1;
            int acHeadId = 0;
            IntegrationService _integrationService = new IntegrationService(_db, _httpContext);
            Common_Supplier enExist = _db.Common_Supplier.Where(x => x.Active && vMSupplier.Name.Trim().ToUpper() == x.Name.ToUpper()).FirstOrDefault();
            if (enExist != null && enExist.ID > 0) { return result; }
            Common_Supplier codeExist = _db.Common_Supplier.Where(x => x.Active && vMSupplier.Code.Trim().ToUpper() == x.Code.ToUpper()).FirstOrDefault();
            if (codeExist != null && codeExist.ID > 0) { return result; }

            if ((int)vMSupplier.SupplierType == 1 || (int)vMSupplier.SupplierType == 2 || (int)vMSupplier.SupplierType == 3)
            {
                acHeadId = await _integrationService.AccountingHeadCreate(vMSupplier.Name, 1300);
            }
            if ((int)vMSupplier.SupplierType == 4 || (int)vMSupplier.SupplierType == 5)
            {
                acHeadId = await _integrationService.AccountingHeadCreate(vMSupplier.Name, 1301);
            }
            Common_Supplier supplier = new Common_Supplier
            {
                User = vMSupplier.User,
                UserID = vMSupplier.UserID,
                Name = vMSupplier.Name,
                Code = vMSupplier.Code,
                Common_CountryFK = vMSupplier.VMCommon_CountryID,
                SupplierType = (int)vMSupplier.SupplierType,
                IsForeignSupplier = vMSupplier.IsForeignSupplier,
                ContactPerson = vMSupplier.ContactPerson,
                Email = vMSupplier.Email,
                Phone = vMSupplier.Phone,
                Address = vMSupplier.Address,
                AccHeadID = acHeadId
            };

            _db.Common_Supplier.Add(supplier);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = supplier.ID;
                await _integrationService.AccountingHeadAdjust(acHeadId, result, (int)IntegratedAccountHeadTypeEnum.Supplier);
            }
            return result;
        }
        public async Task<int> SupplierEdit(VMSupplier vMSupplier)
        {
            var result = -1;
            IntegrationService _integrationService = new IntegrationService(_db, _httpContext);
            Common_Supplier enExist = _db.Common_Supplier.Where(x => x.Active && x.ID != vMSupplier.ID && vMSupplier.Name.Trim().ToUpper() == x.Name.ToUpper()).FirstOrDefault();
            if (enExist != null && enExist.ID > 0) { return result; }

            var codeExist = _db.Common_Supplier.Where(x => x.Active && x.Code.ToUpper()==vMSupplier.Code.Trim().ToUpper()).Skip(vMSupplier.ID).ToList();
            
            
            if (codeExist.Count > 0) { return result; }

            Common_Supplier supplier = _db.Common_Supplier.Find(vMSupplier.ID);

            supplier.Name = vMSupplier.Name;
            supplier.Code = vMSupplier.Code;
            supplier.Common_CountryFK = vMSupplier.VMCommon_CountryID;
            supplier.SupplierType = (int)vMSupplier.SupplierType;
            supplier.IsForeignSupplier = vMSupplier.IsForeignSupplier;
            supplier.ContactPerson = vMSupplier.ContactPerson;
            supplier.Email = vMSupplier.Email;
            supplier.Phone = vMSupplier.Phone;
            supplier.Address = vMSupplier.Address;
            var aHeadId = await _integrationService.AccountingHeadEdit(supplier.AccHeadID, supplier.Name);
            supplier.AccHeadID = aHeadId;
            _db.Common_Supplier.Update(supplier);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = supplier.ID;
                await _integrationService.AccountingHeadAdjust(aHeadId, result, (int)IntegratedAccountHeadTypeEnum.Supplier);
            }
            return result;
        }
        public async Task<int> SupplierDelete(int id)
        {
            var result = -1;
            IntegrationService _integrationService = new IntegrationService(_db, _httpContext);
            Common_Supplier supplier = _db.Common_Supplier.Find(id);

            if (supplier != null)
            {

                supplier.Active = false;
                //_db.Common_Supplier.Remove(supplier);
                if (await _db.SaveChangesAsync() > 0)
                {
                    await _integrationService.AccountingHeadEdit(supplier.AccHeadID, "Deleted " + supplier.Name);
                    result = supplier.ID;
                }
            }
            return result;
        }
        #endregion
        #region Currency
        public async Task<VMCurrency> GetCurrency()
        {
            VMCurrency _currency = new VMCurrency
            {
                DataList = await Task.Run(() => _db.Common_Currency.Where(a => a.Active == true).ToList().ConvertAll(
                    x => new VMCurrency { ID = x.ID, Name = x.Name, ConversionRateToBDT = x.ConversionRateToBDT, PreviousConversionRateToBDT = x.ConversionRateToBDT }).OrderByDescending(x => x.ID))
            };
            return _currency;
        }
        public async Task<int> CurrencyAdd(VMCurrency _currency)
        {
            var result = -1;

            Common_Currency currency = new Common_Currency
            {
                Name = _currency.Name,
                ConversionRateToBDT = _currency.ConversionRateToBDT,
                User = "UserOne",
            };
            _db.Common_Currency.Add(currency);

            Common_CurrencyChangesLog currencyLog = new Common_CurrencyChangesLog
            {
                Common_CurrencyFK = currency.ID,
                PreviousConversionRateToBDT = _currency.ConversionRateToBDT,
                ChangesConversionRateToBDT = _currency.ConversionRateToBDT,
            };
            _db.Common_CurrencyChangesLog.Add(currencyLog);

            if (await _db.SaveChangesAsync() > 0)
            {
                result = currency.ID;
            }
            return result;
        }
        public async Task<int> CurrencyEdit(VMCurrency _currency)
        {
            var result = -1;

            Common_Currency currency = _db.Common_Currency.Find(_currency.ID);
            currency.Name = _currency.Name;
            currency.ConversionRateToBDT = _currency.ConversionRateToBDT;

            Common_CurrencyChangesLog currencyLog = new Common_CurrencyChangesLog
            {
                Common_CurrencyFK = currency.ID,
                PreviousConversionRateToBDT = _currency.PreviousConversionRateToBDT,
                ChangesConversionRateToBDT = _currency.ConversionRateToBDT,
            };
            _db.Common_CurrencyChangesLog.Add(currencyLog);

            if (await _db.SaveChangesAsync() > 0)
            {
                result = currency.ID;
            }
            return result;
        }
        public async Task<int> CurrencyDelete(int id)
        {
            var result = -1;
            Common_Currency _currency = _db.Common_Currency.Find(id);
            if (_currency != null)
            {
                _currency.Active = false;
                //_db.Common_Currency.Remove(_currency);
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = _currency.ID;
                }
            }
            return result;
        }
        #endregion
        #region Brand
        public async Task<VmBrand> GetBrand()
        {
            var brand = new VmBrand
            {
                DataList = await Task.Run(() => _db.Common_Brand.Where(a => a.Active).ToList().ConvertAll(
                    x => new VmBrand { ID = x.ID, Name = x.Name }).OrderByDescending(x => x.ID))
            };
            return brand;
        }
        public async Task<int> BrandAdd(VmBrand model)
        {
            var result = -1;

            var brand = new Common_Brand
            {
                Name = model.Name,
                User = "UserOne",
            };
            _db.Common_Brand.Add(brand);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = brand.ID;
            }
            return result;
        }
        public async Task<int> BrandEdit(VmBrand model)
        {
            var result = -1;

            var brand = _db.Common_Brand.Find(model.ID);

            brand.Name = model.Name;


            if (await _db.SaveChangesAsync() > 0)
            {
                result = brand.ID;
            }
            return result;
        }
        public async Task<int> BrandDelete(int id)
        {
            var result = -1;
            var model = _db.Common_Brand.Find(id);
            if (model != null)
            {
                _db.Common_Brand.Remove(model);
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = model.ID;
                }
            }
            return result;
        }
        public IEnumerable GetBrandList()
        {
            var brandList = new List<object>();
            foreach (var brand in _db.Common_Brand.Where(x => x.Active).ToList())
            {
                brandList.Add(new { Text = brand.Name, Value = brand.ID });
            }
            return brandList;
        }
        #endregion
        #region Model
        public async Task<VmModel> GetModel()
        {
            VmModel vmModel = new VmModel();
            var aModel = await Task.Run(() => (from t1 in _db.Common_Model
                                               join t2 in _db.Common_Brand on t1.Common_BrandFK equals t2.ID
                                               where t1.Active == true
                                               select new VmModel
                                               {
                                                   ID = t1.ID,
                                                   Name = t1.Name,
                                                   BrandName = t2.Name,
                                                   BrandFK = t2.ID
                                               }).ToList());

            vmModel.DataList = aModel;
            return vmModel;

            //var model = new VmModel
            //{
            //    DataList = await Task.Run(() => _db.Common_Model.Where(a => a.Active).ToList().ConvertAll(
            //        x => new VmModel { ID = x.ID, Name = x.Name }).OrderByDescending(x => x.ID))
            //};
            //return model;
        }
        public async Task<int> ModelAdd(VmModel model)
        {
            var result = -1;

            var _model = new Common_Model
            {
                Common_BrandFK = model.BrandFK,
                Name = model.Name,
                User = "UserOne",
            };
            _db.Common_Model.Add(_model);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = model.ID;
            }
            return result;
        }
        public async Task<int> ModelEdit(VmModel model)
        {
            var result = -1;

            var _model = _db.Common_Model.Find(model.ID);
            _model.Common_BrandFK = model.BrandFK;
            _model.Name = model.Name;

            if (await _db.SaveChangesAsync() > 0)
            {
                result = model.ID;
            }
            return result;
        }
        public async Task<int> ModelDelete(int id)
        {
            var result = -1;
            var model = _db.Common_Model.Find(id);
            if (model == null) return result;
            _db.Common_Model.Remove(model);
            if (await _db.SaveChangesAsync() <= 0) return result;
            result = model.ID;
            return result;
        }

        #endregion
        #region Raw Category
        private string GetType(int Id)
        {
            List<EnumModel> enumlist = ((EnumCategoryType[])Enum.GetValues(typeof(EnumCategoryType))).Select(c => new EnumModel() { Value = (int)c, Text = c.ToString() }).ToList();
            string name = enumlist.Where(c=>c.Value==Id).FirstOrDefault().Text;
            return name;
        }
        #region BackDateCode
        //public async Task<VMRawCategory> GetRawCategory()
        //{
        //    VMRawCategory vMRawCategory = new VMRawCategory
        //    {
        //        DataList = await Task.Run(() => _db.Common_RawCategory.Where(a => a.Active == true).ToList().ConvertAll(
        //            x => new VMRawCategory { ID = x.ID, Name = x.Name }).OrderByDescending(x => x.ID))
        //    };

        //    return vMRawCategory;
        //}
        #endregion

        public VMRawCategory GetRawCategory()
        {
            var Data = (from t1 in _db.Common_RawCategory
                        where t1.Active == true
                        select new VMRawCategory
                        {
                            ID = t1.ID,
                            Name = t1.Name,
                            CategoryTypeFK= t1.CategoryTypeFK==null?0:(int)t1.CategoryTypeFK
                        }).OrderByDescending(x => x.ID).ToList();

            Data.ForEach(a =>
            {
                a.CategoryType = GetType(a.CategoryTypeFK);
            });
            VMRawCategory vmRawCategory = new VMRawCategory
            {
                DataList = Data
            };
            return vmRawCategory;
        }
        public async Task<int> RawCategoryAdd(VMRawCategory vMRawCategory)
        {
            var result = -1;

            Common_RawCategory rawCategory = new Common_RawCategory
            {
                Name = vMRawCategory.Name,
                CategoryTypeFK=vMRawCategory.CategoryTypeFK,
                User = "UserOne",
            };
            _db.Common_RawCategory.Add(rawCategory);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = rawCategory.ID;
            }
            return result;
        }

        public async Task<int> RawCategoryEdit(VMRawCategory vMRawCategory)
        {
            var result = -1;

            Common_RawCategory rawCategory = _db.Common_RawCategory.Find(vMRawCategory.ID);

            rawCategory.Name = vMRawCategory.Name;
            rawCategory.CategoryTypeFK = vMRawCategory.CategoryTypeFK;

            if (await _db.SaveChangesAsync() > 0)
            {
                result = rawCategory.ID;
            }
            return result;
        }
        public async Task<int> RawCategoryDelete(int id)
        {
            var result = -1;
            Common_RawCategory rawCategory = _db.Common_RawCategory.Find(id);
            if (rawCategory != null)
            {
                rawCategory.Active = false;
                //_db.Common_RawCategory.Remove(rawCategory);
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = rawCategory.ID;
                }
            }
            return result;
        }
        #endregion
        #region Raw Sub-Category
        public async Task<VMRawSubCategory> GetRawSubCategory()
        {
            VMRawSubCategory vMRawSubCategory = new VMRawSubCategory();
            var a = await Task.Run(() => (from t1 in _db.Common_RawSubCategory
                                          join t2 in _db.Common_RawCategory
                                          on t1.Common_RawCategoryFK equals t2.ID
                                          where t1.Active == true
                                          select new VMRawSubCategory
                                          {
                                              ID = t1.ID,
                                              Name = t1.Name,
                                              RawCategory = t2.Name,
                                              RawCategoryFK = t2.ID
                                          }).ToList());

            vMRawSubCategory.DataList = a;
            return vMRawSubCategory;
        }
        public async Task<int> RawSubCategoryAdd(VMRawSubCategory vMRawSubCategory)
        {
            var result = -1;

            Common_RawSubCategory rawSubCategory = new Common_RawSubCategory
            {
                Name = vMRawSubCategory.Name,
                Common_RawCategoryFK = vMRawSubCategory.RawCategoryFK,
                User = "UserOne",
            };
            _db.Common_RawSubCategory.Add(rawSubCategory);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = rawSubCategory.ID;
            }
            return result;
        }
        public async Task<int> RawSubCategoryEdit(VMRawSubCategory vMRawSubCategory)
        {
            var result = -1;

            Common_RawSubCategory rawSubCategory = _db.Common_RawSubCategory.Find(vMRawSubCategory.ID);

            rawSubCategory.Name = vMRawSubCategory.Name;
            rawSubCategory.Common_RawCategoryFK = vMRawSubCategory.RawCategoryFK;


            if (await _db.SaveChangesAsync() > 0)
            {
                result = rawSubCategory.ID;
            }
            return result;
        }
        public async Task<int> RawSubCategoryDelete(int id)
        {
            var result = -1;
            Common_RawSubCategory rawSubCategory = _db.Common_RawSubCategory.Find(id);
            if (rawSubCategory != null)
            {
                rawSubCategory.Active = false;
                //_db.Common_RawSubCategory.Remove(rawSubCategory);
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = rawSubCategory.ID;
                }
            }
            return result;
        }
        #endregion
        #region Raw Item
        //public List<VMRawItem> GetRawItem()
        //{
            
        //    var rawItem =(from t1 in _db.Common_RawItem
        //                  join t2 in _db.Common_RawSubCategory
        //                  on t1.Common_RawSubCategoryFK equals t2.ID
        //                  join t3 in _db.Common_RawCategory
        //                  on t2.Common_RawCategoryFK equals t3.ID
        //                  join t4 in _db.Common_Unit on t1.Common_UnitFK equals t4.ID
        //                  where t1.Active == true && t3.CategoryTypeFK !=2
        //                  select new VMRawItem
        //                  {
        //                    ID = t1.ID,
        //                    Name = t1.Name,
        //                    RawSubCategory = t2.Name,
        //                    RawCategory = t3.Name,
        //                    Unit = t4.Name,
        //                    RawSubCategoryFK = t2.ID,
        //                    RawCategoryFK = t3.ID,
        //                    Common_UnitFK = t4.ID
        //                    }).ToList();
        
        //    return rawItem;
        //}
        public async Task<VMRawItem> GetRawItem()
        {
            VMRawItem vMRawItem = new VMRawItem();
            var rawItem = await Task.Run(() => (from t1 in _db.Common_RawItem
                                                join t2 in _db.Common_RawSubCategory
                                                on t1.Common_RawSubCategoryFK equals t2.ID
                                                join t3 in _db.Common_RawCategory
                                                on t2.Common_RawCategoryFK equals t3.ID
                                                join t4 in _db.Common_Unit on t1.Common_UnitFK equals t4.ID
                                                where t1.Active == true && t3.CategoryTypeFK != 2
                                                select new VMRawItem
                                                {
                                                    ID = t1.ID,
                                                    Name = t1.Name,
                                                    RawSubCategory = t2.Name,
                                                    RawCategory = t3.Name,
                                                    Unit = t4.Name,
                                                    RawSubCategoryFK = t2.ID,
                                                    TempFK = t2.ID,
                                                    RawCategoryFK = t3.ID,
                                                    Common_UnitFK = t4.ID,
                                                }).AsEnumerable());
            vMRawItem.DataList = rawItem;
            return vMRawItem;
        }
        public VMRawItem RawItemGetByID(int id)
        {
            var rawItem = (from t1 in _db.Common_RawItem
                               join t2 in _db.Common_RawSubCategory
                               on t1.Common_RawSubCategoryFK equals t2.ID
                               join t3 in _db.Common_RawCategory
                               on t2.Common_RawCategoryFK equals t3.ID
                               join t4 in _db.Common_Unit on t1.Common_UnitFK equals t4.ID
                               where t1.Active == true && t1.ID == id
                               select new VMRawItem
                               {
                                   Name = t1.Name,
                                   RawSubCategoryFK = t2.ID,
                                   RawCategoryFK = t3.ID,
                                   Common_UnitFK = t4.ID,
                                   ActionId = (int)ActionEnum.Edit
                               }).FirstOrDefault();
            return rawItem;
        }
        public async Task<int> RawItemAdd(VMRawItem vMRawItem)
        {
            var result = -1;

            Common_RawItem rawItem = new Common_RawItem
            {
                Name = vMRawItem.Name,
                Common_RawSubCategoryFK = vMRawItem.RawSubCategoryFK,
                Common_UnitFK = vMRawItem.Common_UnitFK,
                GSM = vMRawItem.GSM,

                User = "UserOne",
            };
            _db.Common_RawItem.Add(rawItem);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = rawItem.ID;
            }
            return result;
        }
        public async Task<int> RawItemEdit(VMRawItem vMRawItem)
        {
            var result = -1;

            Common_RawItem rawItem = _db.Common_RawItem.Find(vMRawItem.ID);

            rawItem.Name = vMRawItem.Name;
            rawItem.Common_RawSubCategoryFK = vMRawItem.RawSubCategoryFK;
            rawItem.Common_UnitFK = vMRawItem.Common_UnitFK;
            rawItem.GSM = vMRawItem.GSM;
            if (await _db.SaveChangesAsync() > 0)
            {
                result = rawItem.ID;
            }
            return result;
        }
        public async Task<int> RawItemDelete(int id)
        {
            var result = -1;
            Common_RawItem rawItem = _db.Common_RawItem.Find(id);
            if (rawItem != null)
            {
                rawItem.Active = false;
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = rawItem.ID;
                }
            }
            return result;
        }

        #endregion
        #region Asset Item
        public async Task<VMRawItem> GetAssetItem()
        {
            VMRawItem vMRawItem = new VMRawItem();
            var rawItem = await Task.Run(() => (from t1 in _db.Common_RawItem
                                                join t2 in _db.Common_RawSubCategory
                                                on t1.Common_RawSubCategoryFK equals t2.ID
                                                join t3 in _db.Common_RawCategory
                                                on t2.Common_RawCategoryFK equals t3.ID
                                                join t4 in _db.Common_Unit on t1.Common_UnitFK equals t4.ID
                                                where t1.Active == true && t3.CategoryTypeFK==2
                                                select new VMRawItem
                                                {
                                                    ID = t1.ID,
                                                    Name = t1.Name,
                                                    RawSubCategory = t2.Name,
                                                    RawCategory = t3.Name,
                                                    Unit = t4.Name,
                                                    RawSubCategoryFK = t2.ID,
                                                    TempFK=t2.ID,
                                                    RawCategoryFK = t3.ID,
                                                    Common_UnitFK = t4.ID,
                                                    AccountHeadID= t1.AccountHead==null?0:(int)t1.AccountHead


                                                }).AsEnumerable());
            vMRawItem.DataList = rawItem;
            return vMRawItem;
        }
        public async Task<int> AssetItemAdd(VMRawItem vMRawItem)
        {
            var result = -1;

            Common_RawItem rawItem = new Common_RawItem
            {
                Name = vMRawItem.Name,
                Common_RawSubCategoryFK = vMRawItem.RawSubCategoryFK,
                Common_UnitFK = vMRawItem.Common_UnitFK,
                AccountHead = vMRawItem.AccountHeadID,
                User = "UserOne",
            };
            _db.Common_RawItem.Add(rawItem);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = rawItem.ID;
            }
            return result;
        }
        public async Task<int> AssetItemEdit(VMRawItem vMRawItem)
        {
            var result = -1;

            Common_RawItem rawItem = _db.Common_RawItem.Find(vMRawItem.ID);

            rawItem.Name = vMRawItem.Name;
            rawItem.Common_RawSubCategoryFK = vMRawItem.RawSubCategoryFK;
            rawItem.Common_UnitFK = vMRawItem.Common_UnitFK;
            rawItem.AccountHead = vMRawItem.AccountHeadID;
            if (await _db.SaveChangesAsync() > 0)
            {
                result = rawItem.ID;
            }
            return result;
        }
        public async Task<int> AssetItemDelete(int id)
        {
            var result = -1;
            Common_RawItem rawItem = _db.Common_RawItem.Find(id);
            if (rawItem != null)
            {
                rawItem.Active = false;
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = rawItem.ID;
                }
            }
            return result;
        }
        #endregion
        #region FinishCategory
        public async Task<VMFinishCategory> GetFinishCategory()
        {
            VMFinishCategory vMFinishCategory = new VMFinishCategory
            {
                DataList = await Task.Run(() => _db.Common_FinishCategory.Where(a => a.Active == true).ToList().ConvertAll(
                    x => new VMFinishCategory { ID = x.ID, Name = x.Name, Code = x.Remarks }).OrderByDescending(x => x.ID))
            };
            return vMFinishCategory;


        }
        public async Task<int> FinishCategoryAdd(VMFinishCategory vMFinishCategory)
        {
            var result = -1;

            Common_FinishCategory finishCategory = new Common_FinishCategory
            {
                Name = vMFinishCategory.Name,
                User = "UserOne",
            };
            _db.Common_FinishCategory.Add(finishCategory);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = finishCategory.ID;
            }
            return result;
        }
        public async Task<int> FinishCategoryEdit(VMFinishCategory vMFinishCategory)
        {
            var result = -1;

            Common_FinishCategory finishCategory = _db.Common_FinishCategory.Find(vMFinishCategory.ID);

            finishCategory.Name = vMFinishCategory.Name;


            if (await _db.SaveChangesAsync() > 0)
            {
                result = finishCategory.ID;
            }
            return result;
        }
        public async Task<int> FinishCategoryDelete(int id)
        {
            var result = -1;
            Common_FinishCategory finishCategory = _db.Common_FinishCategory.Find(id);
            if (finishCategory != null)
            {
                finishCategory.Active = false;
                //_db.Common_FinishCategory.Remove(finishCategory);
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = finishCategory.ID;
                }
            }
            return result;
        }
        #endregion
        #region Finish Sub-Category
        public async Task<VMFinishSubCategory> GetFinishSubCategory()
        {
            VMFinishSubCategory vMFinishSubCategory = new VMFinishSubCategory();
            var a = await Task.Run(() => (from t1 in _db.Common_FinishSubCategory
                                          join t2 in _db.Common_FinishCategory
                                          on t1.Common_FinishCategoryFK equals t2.ID
                                          where t1.Active == true
                                          select new VMFinishSubCategory
                                          {
                                              ID = t1.ID,
                                              Name = t1.Name,
                                              FinishCategory = t2.Name,
                                              FinishCategoryFK = t2.ID
                                          }).ToList());

            vMFinishSubCategory.DataList = a;
            return vMFinishSubCategory;
        }
        public async Task<int> FinishSubCategoryAdd(VMFinishSubCategory vMFinishSubCategory)
        {
            var result = -1;

            Common_FinishSubCategory finishSubCategory = new Common_FinishSubCategory
            {
                Name = vMFinishSubCategory.Name,
                Common_FinishCategoryFK = vMFinishSubCategory.FinishCategoryFK,
                User = "UserOne",
            };
            _db.Common_FinishSubCategory.Add(finishSubCategory);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = finishSubCategory.ID;
            }
            return result;
        }
        public async Task<int> FinishSubCategoryEdit(VMFinishSubCategory vMFinishSubCategory)
        {
            var result = -1;

            Common_FinishSubCategory finishSubCategory = _db.Common_FinishSubCategory.Find(vMFinishSubCategory.ID);

            finishSubCategory.Name = vMFinishSubCategory.Name;
            finishSubCategory.Common_FinishCategoryFK = vMFinishSubCategory.FinishCategoryFK;


            if (await _db.SaveChangesAsync() > 0)
            {
                result = finishSubCategory.ID;
            }
            return result;
        }
        public async Task<int> FinishSubCategoryDelete(int id)
        {
            var result = -1;
            Common_FinishSubCategory finishSubCategory = _db.Common_FinishSubCategory.Find(id);
            if (finishSubCategory != null)
            {
                _db.Common_FinishSubCategory.Remove(finishSubCategory);
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = finishSubCategory.ID;
                }
            }
            return result;
        }
        #endregion
        #region Finish Item
        public async Task<VMFinishItem> GetFinishItem()
        {
            VMFinishItem vMFinishItem = new VMFinishItem();
            var finishItem = await Task.Run(() => (from t1 in _db.Common_FinishItem
                                                   join t2 in _db.Common_FinishSubCategory
                                                   on t1.Common_FinishSubCategoryFK equals t2.ID
                                                   join t3 in _db.Common_FinishCategory
                                                   on t2.Common_FinishCategoryFK equals t3.ID
                                                   join t4 in _db.Common_Unit on t1.Common_UnitFK equals t4.ID
                                                   where t1.Active == true
                                                   select new VMFinishItem
                                                   {
                                                       ID = t1.ID,
                                                       Name = t1.Name,
                                                       FinishSubCategory = t2.Name,
                                                       FinishSubCategoryFK = t2.ID,
                                                       FinishCategory = t3.Name,
                                                       FinishCategoryFK = t3.ID,
                                                       Common_UnitFK = t4.ID,
                                                       Unit = t4.Name,
                                                       Code = t1.Remarks
                                                   }).AsEnumerable());
            vMFinishItem.DataList = finishItem;
            return vMFinishItem;
        }
        public async Task<int> FinishItemAdd(VMFinishItem vMFinishItem)
        {
            var result = -1;

            Common_FinishItem finishItem = new Common_FinishItem();
            finishItem.Name = vMFinishItem.Name;
            finishItem.Common_FinishSubCategoryFK = vMFinishItem.FinishSubCategoryFK;
            finishItem.Common_UnitFK = vMFinishItem.Common_UnitFK;
            finishItem.Remarks = CidServices.GetFinishItemCID(vMFinishItem.FinishCategoryFK);

            finishItem.User = "UserOne";
            _db.Common_FinishItem.Add(finishItem);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = finishItem.ID;
            }
            return result;
        }
        public async Task<int> FinishItemEdit(VMFinishItem vMFinishItem)
        {
            var result = -1;

            Common_FinishItem finishItem = _db.Common_FinishItem.Find(vMFinishItem.ID);

            finishItem.Name = vMFinishItem.Name;
            finishItem.Common_FinishSubCategoryFK = vMFinishItem.FinishSubCategoryFK;
            finishItem.Common_UnitFK = vMFinishItem.Common_UnitFK;


            if (await _db.SaveChangesAsync() > 0)
            {
                result = finishItem.ID;
            }
            return result;
        }
        public async Task<int> FinishItemDelete(int id)
        {
            var result = -1;
            Common_FinishItem finishItem = _db.Common_FinishItem.Find(id);
            if (finishItem != null)
            {
                finishItem.Active = false;
                //_db.Common_FinishItem.Remove(finishItem);
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = finishItem.ID;
                }
            }
            return result;
        }

        #endregion        
        #region Color
        public async Task<VMColor> ColorGet()
        {
            VMColor _color = new VMColor
            {
                DataList = await Task.Run(() => _db.Common_Color.Where(a => a.Active == true).ToList().ConvertAll(
                    x => new VMColor { ID = x.ID, Name = x.Name }).OrderByDescending(x => x.ID))
            };
            return _color;
        }
        public async Task<int> ColorAdd(VMColor model)
        {
            var result = -1;

            Common_Color color = new Common_Color
            {
                Name = model.Name
            };
            _db.Common_Color.Add(color);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = color.ID;
            }
            return result;
        }
        public async Task<int> ColorEdit(VMColor model)
        {
            var result = -1;
            Common_Color color = _db.Common_Color.Find(model.ID);
            color.Name = model.Name;

            if (color != null)
            {
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = color.ID;
                }
            }
            return result;
        }
        public async Task<int> ColorDelete(int id)
        {
            var result = -1;
            Common_Color color = _db.Common_Color.Find(id);
            if (color != null)
            {
                color.Active = false;
                //_db.Common_Color.Remove(color);
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = color.ID;
                }
            }
            return result;
        }
        #endregion
        #region Size
        public async Task<VMSize> SizeGet()
        {
            VMSize _size = new VMSize
            {
                DataList = await Task.Run(() => _db.Common_Size.Where(a => a.Active == true).ToList().ConvertAll(
                    x => new VMSize { ID = x.ID, Name = x.Name }).OrderByDescending(x => x.ID))
            };
            return _size;
        }
        public async Task<int> SizeAdd(VMSize model)
        {
            var result = -1;

            Common_Size size = new Common_Size
            {
                Name = model.Name
            };
            _db.Common_Size.Add(size);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = size.ID;
            }
            return result;
        }
        public async Task<int> SizeEdit(VMSize model)
        {
            var result = -1;
            Common_Size size = _db.Common_Size.Find(model.ID);
            size.Name = model.Name;

            if (size != null)
            {
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = size.ID;
                }
            }
            return result;
        }
        public async Task<int> SizeDelete(int id)
        {
            var result = -1;
            Common_Size size = _db.Common_Size.Find(id);
            if (size != null)
            {
                size.Active = false;
                //_db.Common_Size.Remove(size);
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = size.ID;
                }
            }
            return result;
        }


        #endregion
        #region Port
        public List<VMPort> AllPort(int id)
        {

            var a = _db.Common_CountryPort.Where(t1 => t1.Common_CountryFK == id).Select(x => new VMPort { ID = x.ID, Name = x.Name }).ToList();

            return a;
        }
        public async Task<VMPort> GetPort()
        {
            VMPort vMPort = new VMPort();
            var port = (from t1 in _db.Common_CountryPort
                        join t2 in _db.Common_Country
                        on t1.Common_CountryFK equals t2.ID
                        where t1.Active == true

                        select new VMPort
                        {
                            ID = t1.ID,
                            Name = t1.Name,
                            PortCode = t1.Code,
                            Common_CountryFK = (int)t1.Common_CountryFK,
                            Country = t2.Name,
                            CountryCode = t2.Code


                        }).ToList();

            vMPort.DataList = await Task.Run(() => port);
            return vMPort;
        }
        public async Task<int> PortAdd(VMPort vMPort)
        {
            var result = -1;
            Common_CountryPort port = new Common_CountryPort
            {
                Name = vMPort.Name,
                Code = vMPort.PortCode,
                Common_CountryFK = vMPort.Common_CountryFK,
                User = "UserOne",
            };
            _db.Common_CountryPort.Add(port);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = port.ID;
            }


            return result;
        }
        public async Task<int> PortEdit(VMPort vMPort)
        {
            var result = -1;

            Common_CountryPort port = _db.Common_CountryPort.Find(vMPort.ID);

            port.Name = vMPort.Name;
            port.Code = vMPort.PortCode;
            port.Common_CountryFK = vMPort.Common_CountryFK;


            if (await _db.SaveChangesAsync() > 0)
            {
                result = port.ID;
            }
            return result;
        }
        public async Task<int> PortDelete(int id)
        {
            var result = -1;
            Common_CountryPort port = _db.Common_CountryPort.Find(id);
            if (port != null)
            {
                port.Active = false;
                //_db.Common_CountryPort.Remove(port);
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = port.ID;
                }
            }
            return result;
        }

        public VMPort GetPortByCountryID(int ID)
        {
            VMPort oPort = new VMPort();
            var a = AllPort(ID);
            string[] PortIds = new string[a.Count];

            for (int i = 0; i < a.Count; i++)
            {
                PortIds[i] = a[i].ID.ToString();
            }
            MultiSelectList lstPort = new MultiSelectList(PortDropDownList(), "Value", "Text", PortIds);
            oPort.PortByList = lstPort;
            oPort.Common_CountryFK = ID;
            return oPort;
        }


        public bool CheckPort(string port)
        {
            var s = _db.Common_CountryPort.Any(a => a.Name.Equals(port));
            return s;
        }
        #endregion
        #region Country
        public async Task<VMCountry> CountryGet()
        {
            VMCountry vMCountry = new VMCountry
            {
                DataList = await Task.Run(() => _db.Common_Country.Where(a => a.Active == true).ToList().ConvertAll(
                    x => new VMCountry { ID = x.ID, Name = x.Name, Code = x.Code }).OrderByDescending(x => x.ID))
            };
            return vMCountry;
        }
        public async Task<int> CountryAdd(VMCountry model)
        {
            var result = -1;

            Common_Country country = new Common_Country
            {
                Name = model.Name,
                Code = model.Code
            };
            _db.Common_Country.Add(country);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = country.ID;
            }
            return result;
        }
        public async Task<int> CountryEdit(VMCountry model)
        {
            var result = -1;
            Common_Country country = _db.Common_Country.Find(model.ID);
            country.Name = model.Name;
            country.Code = model.Code;

            if (country != null)
            {
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = country.ID;
                }
            }
            return result;
        }
        public async Task<int> CountryDelete(int id)
        {
            var result = -1;
            Common_Country Country = _db.Common_Country.Find(id);
            if (Country != null)
            {
                Country.Active = false;

                if (await _db.SaveChangesAsync() > 0)
                {
                    result = Country.ID;
                }
            }
            return result;
        }


        #endregion
        #region Yarn Type
        public async Task<VMYarnType> GetYarnType()
        {
            VMYarnType _yarnType = new VMYarnType
            {
                DataList = await Task.Run(() => _db.Merchandising_YarnType.Where(a => a.Active == true).ToList().ConvertAll(
                    x => new VMYarnType
                    {
                        ID = x.ID,
                        Name = x.Name
                    }).OrderByDescending(x => x.ID))
            };
            return _yarnType;
        }
        public async Task<int> YarnTypeAdd(VMYarnType _vmYarnType)
        {
            var result = -1;


            Merchandising_YarnType _yarnType = new Merchandising_YarnType
            {
                Name = _vmYarnType.Name
            };
            _db.Merchandising_YarnType.Add(_yarnType);

            if (await _db.SaveChangesAsync() > 0)
            {
                result = _yarnType.ID;
            }
            return result;
        }
        public async Task<int> YarnTypeEdit(VMYarnType _vmYarnType)
        {
            var result = -1;

            Merchandising_YarnType yarnType = _db.Merchandising_YarnType.Find(_vmYarnType.ID);

            yarnType.Name = _vmYarnType.Name;

            if (await _db.SaveChangesAsync() > 0)
            {
                result = yarnType.ID;
            }
            return result;
        }
        public async Task<int> YarnTypeDelete(int id)
        {
            var result = -1;
            Merchandising_YarnType _yarnType = _db.Merchandising_YarnType.Find(id);
            if (_yarnType != null)
            {
                _yarnType.Active = false;
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = _yarnType.ID;
                }
            }
            return result;
        }
        #endregion
        #region Agent

        public async Task<VMAgent> AgentGet()
        {
            VMAgent vMAgent = new VMAgent
            {
                DataList = await Task.Run(() => _db.Common_Agent.Where(a => a.Active == true).ToList().ConvertAll(
                    x => new VMAgent { ID = x.ID, Name = x.Name, Phone = x.Phone, Email = x.Email, ContactPerson = x.ContactPerson, Address = x.Address }).OrderByDescending(x => x.ID))
            };
            return vMAgent;
        }
        public async Task<int> AgentAdd(VMAgent model)
        {
            var result = -1;

            Common_Agent agent = new Common_Agent
            {
                Name = model.Name,
                Phone = model.Phone,
                Email = model.Email,
                ContactPerson = model.ContactPerson,
                Address = model.Address

            };
            _db.Common_Agent.Add(agent);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = agent.ID;
            }
            return result;
        }
        public async Task<int> AgentEdit(VMAgent model)
        {
            var result = -1;
            Common_Agent agent = _db.Common_Agent.Find(model.ID);
            agent.Name = model.Name;
            agent.Phone = model.Phone;
            agent.Email = model.Email;
            agent.ContactPerson = model.ContactPerson;
            agent.Address = model.Address;


            if (agent != null)
            {
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = agent.ID;
                }
            }
            return result;
        }
        public async Task<int> AgentDelete(int id)
        {
            var result = -1;
            Common_Agent agent = _db.Common_Agent.Find(id);
            if (agent != null)
            {
                agent.Active = false;

                if (await _db.SaveChangesAsync() > 0)
                {
                    result = agent.ID;
                }
            }
            return result;
        }


        #endregion
        #region InspectionAgent
        public async Task<VMInspectionAgent> InspectionAgentGet()
        {
            VMInspectionAgent vMInspectionAgent = new VMInspectionAgent
            {
                DataList = await Task.Run(() => _db.Common_InspectionAgent.Where(a => a.Active == true).ToList().ConvertAll(
                    x => new VMInspectionAgent { ID = x.ID, Name = x.Name, Phone = x.Phone, Email = x.Email, ContactPerson = x.ContactPerson, Address = x.Address }).OrderByDescending(x => x.ID))
            };
            return vMInspectionAgent;
        }
        public async Task<int> InspectionAgentAdd(VMInspectionAgent model)
        {
            var result = -1;

            Common_InspectionAgent inspectionAgent = new Common_InspectionAgent
            {
                Name = model.Name,
                Phone = model.Phone,
                Email = model.Email,
                ContactPerson = model.ContactPerson,
                Address = model.Address

            };
            _db.Common_InspectionAgent.Add(inspectionAgent);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = inspectionAgent.ID;
            }
            return result;
        }
        public async Task<int> InspectionAgentEdit(VMInspectionAgent model)
        {
            var result = -1;
            Common_InspectionAgent inspectionAgent = _db.Common_InspectionAgent.Find(model.ID);
            inspectionAgent.Name = model.Name;
            inspectionAgent.Phone = model.Phone;
            inspectionAgent.Email = model.Email;
            inspectionAgent.ContactPerson = model.ContactPerson;
            inspectionAgent.Address = model.Address;


            if (inspectionAgent != null)
            {
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = inspectionAgent.ID;
                }
            }
            return result;
        }
        public async Task<int> InspectionAgentDelete(int id)
        {
            var result = -1;
            Common_InspectionAgent inspectionAgent = _db.Common_InspectionAgent.Find(id);
            if (inspectionAgent != null)
            {
                inspectionAgent.Active = false;

                if (await _db.SaveChangesAsync() > 0)
                {
                    result = inspectionAgent.ID;
                }
            }
            return result;
        }


        #endregion
        #region LcType
        public async Task<VmLCType> GetLCType()
        {
            var brand = new VmLCType
            {
                DataList = await Task.Run(() => _db.Common_LcType.Where(a => a.Active).ToList().ConvertAll(
                    x => new VmLCType { ID = x.ID, Name = x.Name, ECIOrBBName = x.IsECIOrBB == 1 ? "ECI" : "Back to Back" }).OrderByDescending(x => x.ID))
            };
            return brand;
        }
        public async Task<int> LCTypeAdd(VmLCType model)
        {
            var result = -1;

            var brand = new Common_LcType
            {
                Name = model.Name,
                IsECIOrBB = model.ECIOrBBDFk,
                User = "UserOne",
            };
            _db.Common_LcType.Add(brand);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = brand.ID;
            }
            return result;
        }
        public async Task<int> LCTypeEdit(VmLCType model)
        {
            var result = -1;

            var brand = _db.Common_LcType.Find(model.ID);

            brand.Name = model.Name;
            brand.IsECIOrBB = model.ECIOrBBDFk;
            _db.Common_LcType.Update(brand);

            if (await _db.SaveChangesAsync() > 0)
            {
                result = brand.ID;
            }
            return result;
        }
        public async Task<int> LCTypeDelete(int id)
        {
            var result = -1;
            var model = _db.Common_LcType.Find(id);
            if (model != null)
            {
                model.Active = false;
                // _db.Common_LcType.Remove(model);
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = model.ID;
                }
            }
            return result;
        }
        #endregion
        #region ApplicantBank
        public async Task<VmApplicantBank> GetApplicantBank()
        {
            var brand = new VmApplicantBank
            {
                DataList = await Task.Run(() => _db.Common_BuyerBank.Where(a => a.Active).ToList().ConvertAll(
                    x => new VmApplicantBank { ID = x.ID, Name = x.Name }).OrderByDescending(x => x.ID))
            };
            return brand;
        }
        public async Task<int> ApplicantBankAdd(VmApplicantBank model)
        {
            var result = -1;

            var brand = new Common_BuyerBank
            {
                Name = model.Name,
                User = "UserOne",
            };
            _db.Common_BuyerBank.Add(brand);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = brand.ID;
            }
            return result;
        }
        public async Task<int> ApplicantBankEdit(VmApplicantBank model)
        {
            var result = -1;

            var brand = _db.Common_BuyerBank.Find(model.ID);

            brand.Name = model.Name;


            if (await _db.SaveChangesAsync() > 0)
            {
                result = brand.ID;
            }
            return result;
        }
        public async Task<int> ApplicantBankDelete(int id)
        {
            var result = -1;
            var model = _db.Common_BuyerBank.Find(id);
            if (model != null)
            {
                model.Active = false;
                //_db.Common_BuyerBank.Remove(model);
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = model.ID;
                }
            }
            return result;
        }
        #endregion
        #region LienBank

        public async Task<VmLienBank> GetLienBank()
        {
            VmLienBank vmLienBank = new VmLienBank();
            var bank = await Task.Run(() => (from t1 in _db.Common_LienBank
                                             join t2 in _db.Common_Bank on t1.Common_BankFK equals t2.ID
                                             where t1.Active == true
                                             select new VmLienBank
                                             {
                                                 ID = t1.ID,
                                                 Name = t1.Name,
                                                 Address = t1.Address,
                                                 ContactNo = t1.ContactNo,
                                                 ContactPerson = t1.ContactPerson,
                                                 Email = t1.Email,
                                                 SwiftCode = t1.SwiftCode,
                                                 Common_BankFK = t2.ID,
                                                 BankName = t2.Name

                                             }).AsEnumerable());
            vmLienBank.DataList = bank;
            return vmLienBank;



        }
        public async Task<int> LienBankAdd(VmLienBank model)
        {
            var result = -1;

            var bank = new Common_LienBank
            {
                Name = model.Name,
                Address = model.Address,
                SwiftCode = model.SwiftCode,
                ContactNo = model.ContactNo,
                ContactPerson = model.ContactPerson,
                Email = model.Email,
                Common_BankFK = model.Common_BankFK,
                User = "UserOne",
            };
            _db.Common_LienBank.Add(bank);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = bank.ID;
            }
            return result;
        }
        public async Task<int> LienBankEdit(VmLienBank model)
        {
            var result = -1;

            var bank = _db.Common_LienBank.Find(model.ID);

            bank.Name = model.Name;
            bank.Address = model.Address;
            bank.SwiftCode = model.SwiftCode;
            bank.ContactNo = model.ContactNo;
            bank.ContactPerson = model.ContactPerson;
            bank.Email = model.Email;
            bank.Common_BankFK = model.Common_BankFK;

            if (await _db.SaveChangesAsync() > 0)
            {
                result = bank.ID;
            }
            return result;
        }
        public async Task<int> LienBankDelete(int id)
        {
            var result = -1;
            var model = _db.Common_LienBank.Find(id);
            if (model != null)
            {
                model.Active = false;
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = model.ID;
                }
            }
            return result;
        }
        #endregion
        #region ReceivingBank

        public List<VmReceivingBank> GetReceivingBank(int companyId)
        {
            VmReceivingBank vmReceivingBank = new VmReceivingBank();
            var vData = (from t1 in _db.Common_CompanyBank
                         join t2 in _db.Common_Bank on t1.Common_BankFK equals t2.ID

                         where t1.Active == true && t1.Common_CompanySetupFK == companyId
                         select new VmReceivingBank
                         {
                             ID = t1.ID,
                             Name = t1.Name,
                             Address = t1.Address,
                             ContactNo = t1.ContactNo,
                             ContactPerson = t1.ContactPerson,
                             SwiftCode = t1.SwiftCode,
                             Email = t1.Email,
                             Common_BankFK = t2.ID,
                             BankName = t2.Name

                         }).ToList();

            return vData;
        }
        public VmReceivingBank ReceivingBankDetail(int id)
        {
            var vData = _db.Common_CompanySetup.Where(a => a.ID == id).Select(a => new VmReceivingBank
            {
                Common_CompanySetupFK = a.ID,
                CompanySetup = a.Name

            }).FirstOrDefault();

            VmReceivingBank vmReceivingBank = new VmReceivingBank();
            vmReceivingBank.Common_CompanySetupFK = vData.Common_CompanySetupFK;
            vmReceivingBank.CompanySetup = vData.CompanySetup;
            vmReceivingBank.DataList = new List<VmReceivingBank>();
            vmReceivingBank.DataList = GetReceivingBank(id);
            return vmReceivingBank;



        }
        public async Task<int> ReceivingBankAdd(VmReceivingBank model)
        {
            var result = -1;

            var bank = new Common_CompanyBank
            {
                Name = model.Name,
                Address = model.Address,
                SwiftCode = model.SwiftCode,
                ContactNo = model.ContactNo,
                ContactPerson = model.ContactPerson,
                Email = model.Email,
                Common_CompanySetupFK = model.Common_CompanySetupFK,
                Common_BankFK = model.Common_BankFK,
                User = "UserOne"


            };
            _db.Common_CompanyBank.Add(bank);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = bank.ID;
            }
            return result;
        }
        public async Task<int> ReceivingBankEdit(VmReceivingBank model)
        {
            var result = -1;

            var bank = _db.Common_CompanyBank.Find(model.ID);

            bank.Name = model.Name;
            bank.Address = model.Address;
            bank.SwiftCode = model.SwiftCode;
            bank.ContactNo = model.ContactNo;
            bank.ContactPerson = model.ContactPerson;
            bank.Email = model.Email;
            bank.Common_BankFK = model.Common_BankFK;

            if (await _db.SaveChangesAsync() > 0)
            {
                result = bank.ID;
            }
            return result;
        }
        public async Task<int> ReceivingBankDelete(int id)
        {
            var result = -1;
            var model = _db.Common_CompanyBank.Find(id);
            if (model != null)
            {
                model.Active = false;

                if (await _db.SaveChangesAsync() > 0)
                {
                    result = model.ID;
                }
            }
            return result;
        }
        #endregion
        #region Common Bank
        public async Task<VMBank> BankGet()
        {
            VMBank vMBank = new VMBank();
            var bank = await Task.Run(() => (from t1 in _db.Common_Bank
                                             join t2 in _db.Common_Country on t1.Common_CountryFK equals t2.ID

                                             where t1.Active == true
                                             select new VMBank
                                             {
                                                 ID = t1.ID,
                                                 Name = t1.Name,
                                                 SwiftCode = t1.SwiftCode,
                                                 Flag = t1.Flag,
                                                 Common_CountryFK = t2.ID,
                                                 Country = t2.Name
                                             }).AsEnumerable());
            vMBank.DataList = bank;
            return vMBank;



        }
        public async Task<int> BankAdd(VMBank model)
        {
            var result = -1;

            Common_Bank bank = new Common_Bank
            {
                Name = model.Name,
                SwiftCode = model.SwiftCode,
                Flag = model.Flag,
                Common_CountryFK = model.Common_CountryFK

            };
            _db.Common_Bank.Add(bank);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = bank.ID;
            }
            return result;
        }
        public async Task<int> BankEdit(VMBank model)
        {
            var result = -1;
            Common_Bank bank = _db.Common_Bank.Find(model.ID);
            bank.Name = model.Name;
            bank.SwiftCode = model.SwiftCode;
            bank.Flag = model.Flag;
            bank.Common_CountryFK = model.Common_CountryFK;
            if (bank != null)
            {
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = bank.ID;
                }
            }
            return result;
        }
        public async Task<int> BankDelete(int id)
        {
            var result = -1;
            Common_Bank bank = _db.Common_Bank.Find(id);
            if (bank != null)
            {
                bank.Active = false;

                if (await _db.SaveChangesAsync() > 0)
                {
                    result = bank.ID;
                }
            }
            return result;
        }
        #endregion
        #region BuyerBank

        public List<VmBuyerBank> GetBuyerBank(int BuyerId)
        {
            VmBuyerBank VmBuyerBank = new VmBuyerBank();
            var vData = (from t1 in _db.Common_BuyerBank
                         join t2 in _db.Common_Bank on t1.Common_BankFK equals t2.ID
                         join t4 in _db.Common_Buyer on t1.Common_BuyerFK equals t4.ID
                         where t1.Active == true && t1.Common_BuyerFK == BuyerId
                         select new VmBuyerBank
                         {
                             ID = t1.ID,
                             Name = t1.Name,
                             Address = t1.Address,
                             ContactNo = t1.ContactNo,
                             ContactPerson = t1.ContactPerson,
                             SwiftCode = t1.SwiftCode,
                             Email = t1.Email,
                             Common_BankFK = t2.ID,
                             BankName = t2.Name

                         }).ToList();

            return vData;
        }
        public VmBuyerBank GetBuyerBankInfo(int id)
        {

            var vData = _db.Common_Buyer.Where(a => a.ID == id).Select(a => new VmBuyerBank
            {
                Common_BuyerFK = a.ID,
                BuyerName = a.Name

            }).FirstOrDefault();

            VmBuyerBank vmBuyerBank = new VmBuyerBank();
            vmBuyerBank.Common_BuyerFK = vData.Common_BuyerFK;
            vmBuyerBank.BuyerName = vData.BuyerName;
            vmBuyerBank.DataList = new List<VmBuyerBank>();
            vmBuyerBank.DataList = GetBuyerBank(id);
            return vmBuyerBank;
        }
        public async Task<int> BuyerBankAdd(VmBuyerBank model)
        {
            var result = -1;

            var bank = new Common_BuyerBank
            {
                Name = model.Name,
                Address = model.Address,
                SwiftCode = model.SwiftCode,
                ContactNo = model.ContactNo,
                ContactPerson = model.ContactPerson,
                Common_BuyerFK = model.Common_BuyerFK,
                Common_BankFK = model.Common_BankFK,
                Email = model.Email,
                User = "UserOne",
            };
            _db.Common_BuyerBank.Add(bank);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = bank.ID;
            }
            return result;
        }
        public async Task<int> BuyerBankEdit(VmBuyerBank model)
        {
            var result = -1;

            var bank = _db.Common_BuyerBank.Find(model.ID);

            bank.Name = model.Name;
            bank.Address = model.Address;
            bank.SwiftCode = model.SwiftCode;
            bank.ContactNo = model.ContactNo;
            bank.ContactPerson = model.ContactPerson;
            bank.Common_BankFK = model.Common_BankFK;
            bank.Email = model.Email;
            if (await _db.SaveChangesAsync() > 0)
            {
                result = bank.ID;
            }
            return result;
        }
        public async Task<int> BuyerBankDelete(int id)
        {
            var result = -1;
            var model = _db.Common_BuyerBank.Find(id);
            if (model != null)
            {
                model.Active = false;
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = model.ID;
                }
            }
            return result;
        }
        #endregion
        #region DropDown  For Common Service
        public List<object> Raw_CategoryDropDownList()
        {
            var Raw_CategoryList = new List<object>();
            foreach (var RawCategory in _db.Common_RawCategory.Where(a => a.Active == true && a.CategoryTypeFK !=2).ToList())
            {
                Raw_CategoryList.Add(new { Text = RawCategory.Name, Value = RawCategory.ID });
            }
            return Raw_CategoryList;
        }
        #region For AssetItem
        public List<object> SpecificRaw_CategoryDropDownList()
        {
            var Raw_CategoryList = new List<object>();
            foreach (var RawCategory in _db.Common_RawCategory.Where(a => a.Active == true && a.CategoryTypeFK==2).ToList())
            {
                Raw_CategoryList.Add(new { Text = RawCategory.Name, Value = RawCategory.ID });
            }
            return Raw_CategoryList;
        }
        public List<object> SpecificRaw_SubCategoryDropDownList()
        {
            var Raw_SubCategoryList = new List<object>();
            var subcategoryItem = (from a in _db.Common_RawSubCategory
                                   join b in _db.Common_RawCategory on a.Common_RawCategoryFK equals b.ID
                                   where a.Active == true && b.CategoryTypeFK == 2
                                   select new
                                   {
                                       ID=a.ID,
                                       Name=a.Name
                                   }).ToList();
            foreach (var RawSubCategory in subcategoryItem)
            {
                Raw_SubCategoryList.Add(new { Text = RawSubCategory.Name, Value = RawSubCategory.ID });
            }
            return Raw_SubCategoryList;
        }
        #endregion
        public List<object> Raw_SubCategoryDropDownList()
        {
            var Raw_SubCategoryList = new List<object>();
            foreach (var RawSubCategory in _db.Common_RawSubCategory.Where(a => a.Active == true).ToList())
            {
                Raw_SubCategoryList.Add(new { Text = RawSubCategory.Name, Value = RawSubCategory.ID });
            }
            return Raw_SubCategoryList;
        }

        public List<object> FinishCategoryDropDownList()
        {
            var FinishCategoryList = new List<object>();
            foreach (var FinishCategory in _db.Common_FinishCategory.Where(a => a.Active == true).ToList())
            {
                FinishCategoryList.Add(new { Text = FinishCategory.Name, Value = FinishCategory.ID });
            }
            return FinishCategoryList;
        }
        public List<object> FinishCategoryDropDowncascading(int id)
        {
            var subCategoryList = new List<object>();
            foreach (var SubCategory in _db.Common_FinishSubCategory.Where(x => x.Common_FinishCategoryFK == id && x.Active == true).ToList())
            {
                subCategoryList.Add(new { Text = SubCategory.Name, Value = SubCategory.ID });
            }
            return subCategoryList;
        }

        public List<object> FinishSubCategoryDropDownList()
        {
            var FinishSubCategoryList = new List<object>();
            foreach (var FinishSubCategory in _db.Common_FinishSubCategory.Where(a => a.Active == true).ToList())
            {
                FinishSubCategoryList.Add(new { Text = FinishSubCategory.Name, Value = FinishSubCategory.ID });
            }
            return FinishSubCategoryList;
        }
        public List<object> FinishSubCategoryDropDownCascading(int id)
        {
            var FinishSubCategoryList = new List<object>();
            foreach (var FinishSubCategory in _db.Common_FinishSubCategory.Where(a => a.Active == true && a.ID == id).ToList())
            {
                FinishSubCategoryList.Add(new { Text = FinishSubCategory.Name, Value = FinishSubCategory.ID });
            }
            return FinishSubCategoryList;
        }
        public List<object> FinishItemDropDownList()
        {
            var FinishItemList = new List<object>();
            foreach (var FinishItem in _db.Common_FinishItem.Where(a => a.Active == true).ToList())
            {
                FinishItemList.Add(new { Text = FinishItem.Name, Value = FinishItem.ID });
            }
            return FinishItemList;
        }
        public List<object> FinishItemDropDownListCascading(int id)
        {
            var FinishItemList = new List<object>();
            foreach (var FinishItem in _db.Common_FinishItem.Where(a => a.Active == true && a.ID == id).ToList())
            {
                FinishItemList.Add(new { Text = FinishItem.Name, Value = FinishItem.ID });
            }
            return FinishItemList;
        }


        public List<object> RawSubCategoryDropDowncascading(int id)
        {
            var subCategoryList = new List<object>();
            foreach (var SubCategory in _db.Common_RawSubCategory.Where(x => x.Common_RawCategoryFK == id && x.Active == true).ToList())
            {
                subCategoryList.Add(new { Text = SubCategory.Name, Value = SubCategory.ID });
            }
            return subCategoryList;
        }
        #region For Asset Item
        public List<VMRawItem> SpecificRawSubCategoryDropDowncascading(int id)
        {
            //var subCategoryList = new List<object>();
            var subCategoryItem = (from t1 in _db.Common_RawSubCategory
                                   join t2 in _db.Common_RawCategory on t1.Common_RawCategoryFK equals t2.ID
                                   where t1.Active == true /*&& t2.CategoryTypeFK == 2*/
                                   select new VMRawItem
                                   {
                                       ID = t1.ID,
                                       Name = t1.Name

                                   }).ToList();

            return subCategoryItem;
        }
        #endregion
        public List<object> RawItemDropDowncascading(int id)
        {
            var subCategoryList = new List<object>();
            foreach (var SubCategory in _db.Common_RawSubCategory.Where(x => x.Common_RawCategoryFK == id && x.Active == true).ToList())
            {
                subCategoryList.Add(new { Text = SubCategory.Name, Value = SubCategory.ID });
            }
            return subCategoryList;
        }

        public List<object> UnitDropDownList()
        {
            var UnitList = new List<object>();
            foreach (var Unit in _db.Common_Unit.Where(a => a.Active == true).ToList())
            {
                UnitList.Add(new { Text = Unit.Name, Value = Unit.ID });
            }
            return UnitList;
        }
        public List<object> DepartmentDropDownList()
        {
            var DepartmentList = new List<object>();
            foreach (var Department in _db.User_Department.Where(a => a.Active == true).ToList())
            {
                DepartmentList.Add(new { Text = Department.Name, Value = Department.ID });
            }
            return DepartmentList;
        }

        public async Task<SelectList> CountryDropDownListAsync()
        {
            return new SelectList(await _db.Common_Country.Where(x => x.Active).Select(x => new { Value = x.ID, Text = x.Name }).ToListAsync(), "Value", "Text");
        }
        public List<object> CountryDropDownList()
        {
            var CountryList = new List<object>();
            foreach (var Country in _db.Common_Country.Where(a => a.Active == true).ToList())
            {
                CountryList.Add(new { Text = Country.Name, Value = Country.ID });
            }
            return CountryList;
        }

        public List<object> BankForeignDropDownList()
        {
            var BankList = new List<object>();
            var ForeigBank = (from t1 in _db.Common_Bank
                              join t2 in _db.Common_Country on t1.Common_CountryFK equals t2.ID
                              where t1.Active == true && t1.Flag == false

                              select new
                              {
                                  ID = t1.ID,
                                  Name = t2.Name + " | " + t1.Name
                              }).ToList();

            foreach (var Bank in ForeigBank)
            {
                BankList.Add(new { Text = Bank.Name, Value = Bank.ID });
            }


            return BankList;
        }
        public List<object> BankLocalDropDownList()
        {
            var BankList = new List<object>();

            var localBank = (from t1 in _db.Common_Bank
                             join t2 in _db.Common_Country on t1.Common_CountryFK equals t2.ID
                             where t1.Active == true && t1.Flag == true

                             select new
                             {
                                 ID = t1.ID,
                                 Name = t2.Name + " | " + t1.Name
                             }).ToList();

            foreach (var Bank in localBank)
            {
                BankList.Add(new { Text = Bank.Name, Value = Bank.ID });
            }


            return BankList;
        }
        public List<object> BankWithCountryDDL()
        {
            var BankList = new List<object>();
            var lienBank = (from t1 in _db.Common_Bank
                            join t2 in _db.Common_Country on t1.Common_CountryFK equals t2.ID
                            where t1.Active == true

                            select new
                            {
                                ID = t1.ID,
                                Name = t2.Name + " | " + t1.Name
                            }).ToList();

            foreach (var Bank in lienBank)
            {
                BankList.Add(new { Text = Bank.Name, Value = Bank.ID });
            }
            return BankList;
        }
        public List<object> ECIOrBBDropDownList()
        {
            var eCIOrBBList = new List<object>();

            eCIOrBBList.Add(new { Text = "ECI", Value = 1 });
            eCIOrBBList.Add(new { Text = "Back to Back", Value = 2 });

            return eCIOrBBList;
        }

        public List<object> CountryWithPortDropDownList()
        {
            var CountryList = new List<object>();

            var country = (from t1 in _db.Common_Country
                           where t1.Active == true
                           select new
                           {
                               Name = t1.Name + "(" + t1.Code + ")",
                               ID = t1.ID

                           }).OrderByDescending(x => x.ID);
            foreach (var Country in country)
            {
                CountryList.Add(new { Text = Country.Name, Value = Country.ID });
            }
            return CountryList;
        }

        public List<object> PortDropDownList()
        {
            var portList = new List<object>();
            foreach (var port in _db.Common_CountryPort.ToList())
            {
                portList.Add(new { Text = port.Name, Value = port.ID });
            }
            return portList;
        }
        public List<object> PortByIdDropDownList(int id)
        {
            var portList = new List<object>();
            foreach (var port in _db.Common_CountryPort.Where(x => x.Common_CountryFK == id).ToList())
            {
                portList.Add(new { Text = port.Name, Value = port.ID });
            }
            return portList;
        }

        public List<object> RawItemDropDownList()
        {
            IntegrationService _integrationService = new IntegrationService(_db, _httpContext);
            return _integrationService.RawItemDropDownList();
        }
        public List<object> GetRawItemList()
        {
            var brandList = new List<object>();
            foreach (var brand in _db.Common_RawItem.Where(x => x.Active).ToList())
            {
                brandList.Add(new { Text = brand.Name, Value = brand.ID });
            }
            return brandList;
        }
        public List<object> GetSpecificRawItemList(int id)
        {
            var brandList = new List<object>();
            foreach (var brand in _db.Common_RawItem.Where(x => x.Active && x.Common_RawSubCategoryFK == id).ToList())
            {
                brandList.Add(new { Text = brand.Name, Value = brand.ID });
            }
            return brandList;
        }

        public List<object> DDLCategoryType()
        {
            var ItemList = new List<object>();
        
            List<EnumModel> enumlist = ((EnumCategoryType[])Enum.GetValues(typeof(EnumCategoryType))).Select(c => new EnumModel() { Value =(int)c, Text = c.ToString() }).ToList();

            foreach (var v in enumlist)
            {
                ItemList.Add(new { Value = v.Value, Text = v.Text });
            }
            return ItemList;
        }

        public List<object> AccountHeadDropDown()
        {
            List<object> HeadList = new List<object>();
            var AccountHead = (from t1 in _db.Accounting_Chart1
                                join t2 in _db.Accounting_Chart2 on t1.ID equals t2.Accounting_Chart1FK
                                join t3 in _db.Accounting_Head on t2.ID equals t3.Accounting_Chart2FK
                                where t1.Accounting_TypeFK == 1 && t3.Active == true
                                select new
                                {
                                    ID = t3.ID,
                                    Name = t3.Name
                                }).ToList();
            foreach (var head in AccountHead)
            {
                HeadList.Add(new { Text = head.Name, Value = head.ID });
            }
            return HeadList;
        }

        #endregion
        #region Api
        public List<VMApiSize> GetAllSizes()
        {
            var Data = (from t1 in _db.Common_Size
                        where t1.Active == true
                        select new VMApiSize
                        {
                            ID = t1.ID,
                            Name = t1.Name
                        }).ToList();
            return Data;

        }

        public List<VMApiColor> GetAllColor()
        {
            var data = (from t1 in _db.Common_Color
                        where t1.Active == true
                        select new VMApiColor
                        {
                            ID = t1.ID,
                            Name = t1.Name

                        }).ToList();

            return data;
        }

        public List<VMLogin> GetUserName(string userName, string password)
        {

            string hashedPassword = GetHashedPassword(password);
            var data = (from t1 in _db.User_User
                        where t1.UserName == userName && t1.Password == hashedPassword && t1.Active == true
                        select new VMLogin
                        {
                            ID = t1.ID,
                            UserName = t1.UserName,
                            IsActive = t1.Active

                        }).ToList();
            List<VMLogin> a = new List<VMLogin>();
            if (data.Any())
            {
                return data;
            }
            else
            {
                a.Add(new VMLogin() { IsActive = false });
                return a;
            }
        }

        #endregion
        public List<VMProductionReport> GetDailyTotalProductionSectionWise(int RefId, int SectionId)
        {
            var VData = (from t in _db.Prod_ReferencePlan
                         join pd in _db.Prod_ReferencePlanFollowup on t.ID equals pd.Prod_ReferencePlanFK
                         join p in _db.Merchandising_StyleShipmentSchedule on pd.Merchandising_StyleShipmentScheduleFK equals p.ID
                         join r in _db.Merchandising_Style on t.Merchandising_StyleFK equals r.ID
                         join s in _db.Merchandising_BuyerOrder on r.Merchandising_BuyerOrderFK equals s.ID
                         where t.Prod_ReferenceFK == RefId && t.Active == true && pd.Active == true && t.SectionId == SectionId
                         group pd.Quantity by new { s.CID, r.StyleName, t.Merchandising_StyleFK, t.PlanQuantity } into all
                         select new VMProductionReport
                         {
                             StyleID = (int)all.Key.Merchandising_StyleFK,
                             BuyerPO = all.Key.CID,
                             StyleName = all.Key.StyleName,
                             TargetQty = all.Key.PlanQuantity,
                             Quantity = (int)all.Sum()
                         }).ToList();

            return VData;
        }

        public List<VMProductionReport> GetDailyProductionBySizeWise(int RefId, int SectionId)
        {
            var VData = (from t in _db.Prod_ReferencePlan
                         join pd in _db.Prod_ReferencePlanFollowup on t.ID equals pd.Prod_ReferencePlanFK
                         join ra in _db.Merchandising_StyleShipmentRatio on pd.Merchandising_StyleShipmentRatioFK equals ra.ID
                         join me in _db.Merchandising_StyleMeasurement on ra.Merchandising_StyleMeasurementFK equals me.ID
                         join si in _db.Common_Size on me.Common_SizeFK equals si.ID
                         join co in _db.Common_Color on me.Common_ColorFK equals co.ID
                         where t.Prod_ReferenceFK == RefId && t.Active == true && pd.Active == true && t.SectionId == SectionId
                         group pd.Quantity by new { co, si.Name, si.ID, t.Merchandising_StyleFK } into all
                         select new VMProductionReport
                         {
                             StyleID = (int)all.Key.Merchandising_StyleFK,
                             ColorName = all.Key.co.Name,
                             Size = all.Key.Name,
                             Quantity = (int)all.Sum()
                         }).ToList();

            return VData;
        }

        public List<VMApiRejectType> GetAllRejectType()
        {
            var data = (from t1 in _db.Common_RejectType
                        where t1.Active == true
                        select new VMApiRejectType
                        {
                            ID = t1.ID,
                            RejectOne = t1.RejectOne,
                            RejectTwo = t1.RejectTwo,
                            RejectThree = t1.RejectThree,
                            RejectFour = t1.RejectFour,
                            RejectFive = t1.RejectFive,
                            RejectSix = t1.RejectSix

                        }).ToList();
            return data;
        }
    }

    public class EnumModel
    {
        public int Value { get; set; }
        public string Text { get; set; }
    }
}
