﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Erp.Core.Services.Home
{
    public class VMPort: BaseVM
    {
        public string Name { get; set; }
        public string Country { get; set; }
        [Required(ErrorMessage = "Country is Required.")]
        public int Common_CountryFK { get; set; }
        public string PortCode { get; set; }
        public string CountryCode { get; set; }
        public List<string> PortIds  { get;set;}
        public IEnumerable<VMPort> DataList { get; set; }
        public List<string> PortList { get; set; }
        public MultiSelectList PortByList { get; set; }
    }
}
