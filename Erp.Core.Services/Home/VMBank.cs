﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Erp.Core.Services.Home
{
    public class VMBank:BaseVM
    {
        public string Name { get; set; }
        public string SwiftCode { get; set; }
        public bool  Flag { get; set; }

        public int Common_CountryFK { get; set; }
        public string Country { get; set; }

        public IEnumerable<VMBank> DataList { get; set; }
    }
}
