﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Erp.Core.Services.Home
{
    public class VMApiSize
    {
        public int ID { get; set; }
        public string Name { get; set; }


    }
    public class VMApiColor
    {
        public int ID { get; set; }
        public string Name { get; set; }


    }


    public class VMApiRejectType
    {
        public int ID { get; set; }
        public string RejectOne { get; set; }
        public string RejectTwo { get; set; }
        public string RejectThree { get; set; }
        public string RejectFour { get; set; }
        public string RejectFive { get; set; }
        public string RejectSix { get; set; }

    }
}
