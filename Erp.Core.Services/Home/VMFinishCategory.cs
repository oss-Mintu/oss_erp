﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Erp.Core.Services.Home
{
    public class VMFinishCategory: BaseVM
    {
        public string Name { get; set; }
        //public string Code { get; set; }
        public IEnumerable<VMFinishCategory> DataList { get; set; }
    }
}
