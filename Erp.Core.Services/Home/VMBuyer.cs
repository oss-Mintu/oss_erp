﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Erp.Core.Services.Home
{
    public class VMBuyer:BaseVM
    {
        [DisplayName("Buyer ID")]
        [MaxLength(5, ErrorMessage = "Upto 5 Chracter")]
        public string BuyerID { get; set; }

        public string Name { get; set; }

        [MaxLength(20, ErrorMessage = "Upto 20 Chracter")]
        public string Phone { get; set; }

        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [DisplayName("Contact Person")]
        public string ContactPerson { get; set; }

        [DataType(DataType.MultilineText)]
        public string Address { get; set; }

        [DisplayName("Country")]
        public int Common_CountryFK { get; set; }

        public string Country { get; set; }
        public string Exist { get; set; }
        public string Exist1 { get; set; }
        public IEnumerable<VMBuyer> DataList { get; set; }

       
    }
}
