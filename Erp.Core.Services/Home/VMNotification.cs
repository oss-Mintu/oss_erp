﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Erp.Core.Services.Home
{
    public class VMNotification: BaseVM
    {
        public int TStyle { get; set; }
        public int TCompleteStyle { get; set; }
        public int NStyle { get; set; }
        public int IncompleteBOM { get; set; }
        public int CompleteBOM { get; set; }
        public int TPR { get; set; }
        public int TCompletePR { get; set; }
        public int TPartialPR { get; set; }
        public int TCancelPR { get; set; }
        public int TCompletePO { get; set; }
        public int TPartialPO { get; set; }
        public int TCancelPO { get; set; }

        public int DayShipment { get; set; }
        public int MonthShipment { get; set; }
        public int ShipmentCountDay { get; set; }
        public int ShipmentCountMonth { get; set; }
        public int TDraftPR { get; set; }
        public int TSubmittedPR { get; set; }
        public int PRApproved1 { get; set; }
        public int PRApproved2 { get; set; }
        public int PRApproved3 { get; set; }
        public int PRHold { get; set; }
        public int PartialPO { get; set; }

        public int FullPO { get; set; }
        public int TDratfPO { get; set; }

        public int TSubmittedPO { get; set; }
        public int POApproved1 { get; set; }
        public int POApproved2 { get; set; }
        public int POApproved3 { get; set; }
        public int POHold { get; set; }
        public int PartialReceived { get; set; }
        public int TGoodReceived { get; set; }
        public  VMLogin VMLogin { get; set; }
        public List<VMNotification> DataList { get; set; }




    }
}
