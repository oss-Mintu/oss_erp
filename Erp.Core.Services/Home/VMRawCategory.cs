﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Erp.Core.Services.Home
{
    public class VMRawCategory:BaseVM
    {
        public string Name{ get; set; }
      
        public int CategoryTypeFK { get; set; }
        [DisplayName("Category Type")]
        public string CategoryType { get; set; }
        public IEnumerable<VMRawCategory> DataList { get; set; }
    }
}
