﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Erp.Core.Services.Home
{
    public class VMColor : BaseVM
    {
        public string Name { get; set; }
        public IEnumerable<VMColor> DataList { get; set; }
    }
}
