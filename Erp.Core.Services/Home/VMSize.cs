﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Erp.Core.Services.Home
{
    public class VMSize : BaseVM
    {
        public string Name { get; set; }
        public IEnumerable<VMSize> DataList { get; set; }
    }
}
