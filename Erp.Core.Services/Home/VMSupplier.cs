﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Erp.Core.Services.Home
{
    public class VMSupplier : BaseVM
    {
        [Required(ErrorMessage = "Supplier Name is Required")]
        [DisplayName("Supplier Name")]
        //[MinLength(1, ErrorMessage = "Supplier Name must be 1 to 50 characters long")]
        //[MaxLength(50,ErrorMessage ="Supplier Name must be 1 to 50 characters long")]
        public string Name { get; set; }
        //[Required(ErrorMessage = "Supplier Code is Required")]
        //[DisplayName("Supplier Code")]

        //public string Code { get; set; }
        [Required(ErrorMessage = "Supplier Type is Required")]
        [Range(1, int.MaxValue, ErrorMessage = "Supplier Type is Required")]
        [DisplayName("Supplier Type")]
        public EnumSupplierType SupplierType { get; set; }
        public string SupplierTypeName { get { return BaseFunctionalities.GetEnumDescription(SupplierType); } }
        public SelectList SupplierTypeList { get { return new SelectList(BaseFunctionalities.GetEnumList<EnumSupplierType>(), "Value", "Text"); } }
        [Required(ErrorMessage = "Supplier Origin is Required")]
        [DisplayName("Foreign Supplier")]
        public bool IsForeignSupplier { get; set; } = false;
        public string Foreign { get { return this.IsForeignSupplier ? "Foreign" : "Local"; } }
        [DisplayName("Contact Person")]
        public string ContactPerson { get; set; }
        [DisplayName("Supplier Email")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [DisplayName("Supplier Phone")]
        public string Phone { get; set; }
        [DisplayName("Supplier Address")]
        public string Address { get; set; }
        [Required(ErrorMessage = "Supplier Country is Required")]
        [Range(1, int.MaxValue, ErrorMessage = "Supplier Country is Required")]
        [DisplayName("Supplier Country")]
        public int VMCommon_CountryID { get; set; }
        public string VMCommon_CountryName { get; set; }
        public SelectList VMCommon_CountryList { get; set; } = new SelectList(new List<object>());
        public IEnumerable<VMSupplier> DataList { get; set; }
    }
}
