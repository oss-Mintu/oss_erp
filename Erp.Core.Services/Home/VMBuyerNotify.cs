﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Erp.Core.Services.Home
{
    public class VMBuyerNotify : BaseVM
    {
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string ContactPerson { get; set; }
        public string Address { get; set; }
        public int Common_CountryFK { get; set; }
        public string CountryName { get; set; }
        public int Common_BuyerFK { get; set; }
        public string BuyerName { get; set; }

        public List<VMBuyerNotify> DataList { get; set; }
    }
}
