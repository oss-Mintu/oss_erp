﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Erp.Core.Services.Home
{
    public class VMAgent:BaseVM
    {

        public string Name { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string ContactPerson { get; set; }
        public string Address { get; set; }

        public IEnumerable<VMAgent> DataList { get; set; }
    }
}
