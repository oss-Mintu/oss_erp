﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Erp.Core.Services.Home
{
    public class VMUnit : BaseVM
    {
        public string Name { get; set; }
        public IEnumerable<VMUnit> DataList { get; set; }

    }
}
