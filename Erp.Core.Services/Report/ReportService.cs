﻿using Erp.Core.Entity.Common;
using Erp.Core.Entity.Procurement;
using Erp.Core.Entity.Store;
using Erp.Core.Services.Home;
using Erp.Core.Services.Integration;
using Erp.Core.Services.Procurement;
using Erp.Core.Services.Store;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace Erp.Core.Services.Report
{
    public class ReportService : BaseService
    {
        private readonly HttpContext _httpContext;
        private readonly IntegrationService integrationService;
        public ReportService(IErpDbContext db, HttpContext httpContext)
        {
            _db = db;
            integrationService = new IntegrationService(_db, httpContext);
            _httpContext = httpContext;
        }

        #region Common
        public async Task<List<object>> BuyerDDLAsync()
        {
            return
            await _db.Common_Buyer
        .Where(x => x.Active).Select(x => new { Value = x.ID, Text = x.Name }).ToListAsync<object>();
        }
        public async Task<List<object>> BuyerPOByBuyerDDLAsync(int id = 0)
        {
            return await _db.Merchandising_BuyerOrder
        .Where(x => x.Active && (id <= 0 || x.Common_BuyerFK == id)).Select(x => new { Value = x.ID, Text = x.BuyerPO }).ToListAsync<object>();
        }
        public async Task<List<object>> StyleByBPODDLAsync(int id = 0)
        {
            var vData = (from t1 in _db.Merchandising_Style
                         join t2 in _db.Merchandising_BuyerOrder on t1.Merchandising_BuyerOrderFK equals t2.ID
                         where t1.Active == true
                         select new { Value = t1.ID, Text = t1.CID + t2.BuyerPO + "/" + t1.StyleName }).OrderByDescending(x => x.Value).ToListAsync<object>();
            return await vData;
        }

        public async Task<List<object>> SupplierDDLAsync()
        {
            return await _db.Common_Supplier.Where(x => x.Active).Select(x => new { Value = x.ID, Text = x.Name + " (" + x.Code + ")" }).ToListAsync<object>();
        }
        public async Task<List<object>> SupplierPOBySupplierDDLAsync(int id = 0)
        {
            return await _db.Procurement_PurchaseOrder
        .Where(x => x.Active && (id <= 0 || x.Common_SupplierFK == id)).Select(x => new { Value = x.ID, Text = x.CID }).ToListAsync<object>();
        }

        public List<object> DDLReportType()
        {
            var ItemList = new List<object>();
            ItemList.Add(new { Value = 1, Text = "Order Wise", });
            ItemList.Add(new { Value = 2, Text = "Style Wise" });
            ItemList.Add(new { Value = 3, Text = "Supplier Wise" });
            ItemList.Add(new { Value = 4, Text = "Preodic" });

            return ItemList;
        }

        public List<object> DDLOrder()
        {
            var OrderList = new List<object>();
            var vData = _db.Merchandising_BuyerOrder.Where(x => x.Active == true).Select(t1 => new { ID = t1.ID,Name = t1.CID }).ToList();
                         
            foreach (var order in vData)
            {
                OrderList.Add(new { Text = order.Name, Value = order.ID });
            }
            return OrderList;
        }
        public List<object> DDLStyle()
        {
            var StyleList = new List<object>();
            var vData = (from o in _db.Merchandising_Style
                        join p in _db.Merchandising_BuyerOrder on o.Merchandising_BuyerOrderFK equals p.ID
                        where o.Active == true 
                        select new { ID = o.ID, Name = o.CID+p.BuyerPO+"/" + o.StyleName }).ToList();
            foreach (var style in vData)
            {
                StyleList.Add(new { Text = style.Name, Value = style.ID });
            }
            return StyleList;
        }

        public List<object> DDLSupplier()
        {
            var SupplierList = new List<object>();
            var vData= _db.Common_Supplier.Where(x => x.Active).Select(x => new { ID = x.ID, Name = x.Name + " (" + x.Code + ")" }).ToList();
            foreach (var supplier in vData)
            {
                SupplierList.Add(new { Text = supplier.Name, Value = supplier.ID });
            }
            return SupplierList;
        }
        #endregion

        #region BPO

        public async Task<VMBPOReportSlave> BPOInventorySummaryGet(VMBPOReportSlave model)
        {

            VMBPOReportSlave vM = new VMBPOReportSlave();
            vM.DataListSlave = new List<VMBPOReportSlave>();

            vM = await Task.Run(() => _db.Merchandising_Style
                                       .Include(x => x.Merchandising_BuyerOrder)
                                       .Include(x => x.Merchandising_BuyerOrder.Common_Buyer)
                                       .Include(x => x.Common_FinishItem)
                                       .Include(x => x.Common_FinishItem.Common_FinishSubCategory)
                                       .Include(x => x.Common_FinishItem.Common_FinishSubCategory.Common_FinishCategory)
                                       .Where(x => x.Active && x.ID == model.VMMerchandising_StyleID)
                                       .Select(x => new VMBPOReportSlave
                                       {
                                           VMMerchandising_StyleID = x.ID,
                                           CID = x.Merchandising_BuyerOrder.Common_Buyer.BuyerID + "/" + x.Merchandising_BuyerOrder.BuyerPO + "/" + x.CID + "/" + x.StyleName,
                                           FirstMove = x.FirstMove,
                                           UpdateDate = x.Time,
                                           ItemName = x.Common_FinishItem.Name,
                                           Class = x.Common_FinishItem.Common_FinishSubCategory.Common_FinishCategory.Name,
                                           Fabrication = x.Fabrication,
                                           Reference = x.ReferenceNo,
                                           Style = x.StyleName,

                                           PackQty = x.PackQty,
                                           PackPieceQty = x.PackPieceQty,
                                           PieceQty = x.PieceQty,


                                           DozonQty = (double)decimal.Divide(x.PieceQty, 12),
                                           BuyerPO = x.Merchandising_BuyerOrder.BuyerPO,
                                           OrderDate = x.Merchandising_BuyerOrder.OrderDate,
                                           Season = x.Merchandising_BuyerOrder.Season,
                                           BuyerName = x.Merchandising_BuyerOrder.Common_Buyer.Name,

                                           PackPrice = (double)x.PackPrice,
                                           StylePrice = (double)decimal.Multiply(x.PackPrice, x.PackQty),
                                           Description = x.Description,


                                           //SizeName = string.Join(", ", _db.Merchandising_StyleMeasurement
                                           //                             .Include(y => y.Common_Size)
                                           //                             .Where(y => y.Active==true && y.Merchandising_StyleFK == x.ID)
                                           //                             .Select(y => y.Common_Size.Name).AsEnumerable()),

                                           SizeName = string.Join(", ", _db.Merchandising_StyleMeasurement
                                                                        .Include(y => y.Common_Size)
                                                                        .Where(y => y.Active == true && y.Merchandising_StyleFK == x.ID)
                                                                        .GroupBy(a => a.Common_Size.Name, (key, group) => new { Size = key, Items = group.ToList() }).Select(a => a.Size).AsEnumerable()),

                                           UserID = x.UserID,
                                       }).OrderByDescending(x => x.CID).FirstOrDefaultAsync());



            vM.DataListSlave = await Task.Run(() => (from MSS in _db.Merchandising_StyleSlave.Where(x => x.Active && x.Merchandising_StyleFK == vM.VMMerchandising_StyleID)
                                                     join RI in _db.Common_RawItem on MSS.Common_RawItemFK equals RI.ID into RI_Join
                                                     from RI in RI_Join.DefaultIfEmpty()
                                                     join U in _db.Common_Unit on RI.Common_UnitFK equals U.ID into U_Join
                                                     from U in U_Join.DefaultIfEmpty()
                                                     join C in _db.Common_Color on MSS.Common_ColorFK equals C.ID into C_Join
                                                     from C in C_Join.DefaultIfEmpty()

                                                     select new VMBPOReportSlave
                                                     {
                                                         StyleColorName = MSS != null && C != null ? C.Name : "",
                                                         StyleGSM = MSS != null ? (double)MSS.GSM : 0,
                                                         StyleQuantity = (double)MSS.RequiredQty,
                                                         VMRawItemFK = RI != null ? RI.ID : 0,
                                                         VMRawItemName = RI != null ? RI.Name : "",
                                                         VMUnitName = U != null ? U.Name : "",

                                                     }).OrderByDescending(x => x.ID).ToListAsync());

            Common_CompanySetup company = await _db.Common_CompanySetup.FirstOrDefaultAsync(x => x.Active);
            if (company != null && company.ID > 0)
            {
                vM.CompanyName = company.Name;
                vM.CompanyShortName = company.ShortName;
                vM.CompanyAddress = company.Address;
                vM.CompanyPhone = company.Phone;
                vM.CompanyEmail = company.Email;
            }
            return vM;
        }
        #endregion

        #region PurchaseOrder 
        public async Task<VMPOReportSlave> POWithSlaveListGet(VMPOReportSlave model)
        {

            VMPOReportSlave vM = new VMPOReportSlave();
            vM.DataListSlave = new List<VMPOReportSlave>();

            vM = await Task.Run(() => (from PO in _db.Procurement_PurchaseOrder.Include(x => x.Procurement_PurchaseOrderSlave)
                                                     .Where(x => x.Active)
                                                    .Where(x => x.ID == (int)model.Procurement_PurchaseOrderFK)
                                       join C in _db.Common_Currency on PO.Common_CurrencyFK equals C.ID into C_Join
                                       from C in C_Join.DefaultIfEmpty()
                                       join E in _db.User_User on PO.UserID equals E.ID into E_Join
                                       from E in E_Join.DefaultIfEmpty()
                                       join S in _db.Common_Supplier on PO.Common_SupplierFK equals S.ID into S_Join
                                       from S in S_Join.DefaultIfEmpty()
                                       select new VMPOReportSlave
                                       {
                                           Procurement_PurchaseOrderFK = PO.ID,
                                           CID = PO.CID,
                                           OriginType = (ProcurementOriginTypeEnum)PO.OriginType,
                                           OrderDate = PO.OrderDate,
                                           POPaymentMethod = (SupplierPaymentMethodEnum)PO.POPaymentMethod,
                                           POType = (POTypeEnum)PO.POType,
                                           CreationDate = PO.CreationDate,
                                           CreatedBy = PO.CreatedBy,
                                           ApprovedDate = PO.ApprovedDate.HasValue ? PO.ApprovedDate.Value : DateTime.MinValue,
                                           RevisionNo = PO.RevisionNo,
                                           Description = PO.Description,
                                           Status = (POStatusEnum)PO.Status,
                                           VMEmployeeFK = E != null ? E.ID : 0,
                                           VMEmployeeName = E != null ? E.Name : "",
                                           VMSupplierFK = PO.Common_SupplierFK,
                                           VMSupplierName = S != null ? S.Name : "",
                                           VMSupplierAddress = S != null ? S.Address : "",
                                           VMSupplierCode = S != null ? S.Code : "",
                                           VMSupplierCPName = S != null ? S.ContactPerson : "",
                                           VMSupplierEmail = S != null ? S.Email : "",
                                           VMSupplierPhone = S != null ? S.Phone : "",
                                           VMCurrencyFK = PO.Common_CurrencyFK,
                                           VMCurrencyName = C != null ? C.Name : "",
                                           TermsAndCondition = PO.TermsAndCondition

                                       }).OrderByDescending(x => x.ID).FirstOrDefaultAsync());



            vM.DataListSlave = await Task.Run(() => (from POS in _db.Procurement_PurchaseOrderSlave.Where(x => x.Active && x.Procurement_PurchaseOrderFK == vM.Procurement_PurchaseOrderFK)
                                                     join RI in _db.Common_RawItem on POS.Common_RawItemFK equals RI.ID into RI_Join
                                                     from RI in RI_Join.DefaultIfEmpty()
                                                     join U in _db.Common_Unit on RI.Common_UnitFK equals U.ID into U_Join
                                                     from U in U_Join.DefaultIfEmpty()
                                                     join MS in _db.Merchandising_Style on POS.Merchandising_StyleID equals MS.ID into MS_Join
                                                     from MS in MS_Join.DefaultIfEmpty()
                                                     join MBO in _db.Merchandising_BuyerOrder on MS.Merchandising_BuyerOrderFK equals MBO.ID into MBO_Join
                                                     from MBO in MBO_Join.DefaultIfEmpty()
                                                     join MSS in _db.Merchandising_StyleSlave on POS.Merchandising_StyleSlaveFK equals MSS.ID into MSS_Join
                                                     from MSS in MSS_Join.DefaultIfEmpty()
                                                     join C in _db.Common_Color on MSS.Common_ColorFK equals C.ID into C_Join
                                                     from C in C_Join.DefaultIfEmpty()
                                                     select new VMPOReportSlave
                                                     {
                                                         BONoStyleName = MS != null && MBO != null ? MBO.BuyerPO + "/" + MS.StyleName : "",
                                                         StyleColorName = MSS != null && C != null ? C.Name : "",
                                                         StyleGSM = MSS != null ? (double)MSS.GSM : 0,
                                                         Procurement_PurchaseOrderFK = POS != null ? POS.Procurement_PurchaseOrderFK : 0,
                                                         Procurement_PurchaseRequisitionSlaveFK = POS != null ? POS.Procurement_PurchaseRequisitionSlaveFK.Value : 0,
                                                         ID = POS != null ? POS.ID : 0,
                                                         PurchaseQuantity = POS != null ? POS.PurchaseQuantity : 0,
                                                         PurchasingPrice = POS != null ? POS.PurchasingPrice : 0,
                                                         DescriptionSlave = POS != null ? POS.Description : "",
                                                         VMRawItemFK = RI != null ? RI.ID : 0,
                                                         VMRawItemName = RI != null ? RI.Name : "",
                                                         VMUnitName = U != null ? U.Name : "",
                                                         //AvailableQuantity = RI != null ? RI.Quantity : 0 //AvailableQuantity has to come from Store
                                                     }).OrderByDescending(x => x.ID).ToListAsync());

            Common_CompanySetup company = await _db.Common_CompanySetup.FirstOrDefaultAsync(x => x.Active);
            if (company != null && company.ID > 0)
            {
                vM.CompanyName = company.Name;
                vM.CompanyShortName = company.ShortName;
                vM.CompanyAddress = company.Address;
                vM.CompanyPhone = company.Phone;
                vM.CompanyEmail = company.Email;
            }
            return vM;
        }
        public async Task<VMPOReportSlave> POInventorySummaryGet(VMPOReportSlave model)
        {

            VMPOReportSlave vM = new VMPOReportSlave();
            vM.DataListSlave = new List<VMPOReportSlave>();

            vM = await Task.Run(() => (from PO in _db.Procurement_PurchaseOrder.Include(x => x.Procurement_PurchaseOrderSlave)
                                                     .Where(x => x.Active)
                                                    .Where(x => x.ID == (int)model.Procurement_PurchaseOrderFK)

                                       join C in _db.Common_Currency on PO.Common_CurrencyFK equals C.ID into C_Join
                                       from C in C_Join.DefaultIfEmpty()
                                       join E in _db.User_User on PO.UserID equals E.ID into E_Join
                                       from E in E_Join.DefaultIfEmpty()
                                       join S in _db.Common_Supplier on PO.Common_SupplierFK equals S.ID into S_Join
                                       from S in S_Join.DefaultIfEmpty()



                                       select new VMPOReportSlave
                                       {
                                           Procurement_PurchaseOrderFK = PO.ID,
                                           CID = PO.CID,
                                           OriginType = (ProcurementOriginTypeEnum)PO.OriginType,
                                           OrderDate = PO.OrderDate,
                                           POPaymentMethod = (SupplierPaymentMethodEnum)PO.POPaymentMethod,
                                           POType = (POTypeEnum)PO.POType,
                                           CreationDate = PO.CreationDate,
                                           CreatedBy = PO.CreatedBy,
                                           ApprovedDate = PO.ApprovedDate.HasValue ? PO.ApprovedDate.Value : DateTime.MinValue,
                                           RevisionNo = PO.RevisionNo,
                                           Description = PO.Description,
                                           Status = (POStatusEnum)PO.Status,
                                           VMEmployeeFK = E != null ? E.ID : 0,
                                           VMEmployeeName = E != null ? E.Name : "",
                                           VMSupplierFK = PO.Common_SupplierFK,
                                           VMSupplierName = S != null ? S.Name : "",
                                           VMSupplierAddress = S != null ? S.Address : "",
                                           VMSupplierCode = S != null ? S.Code : "",
                                           VMSupplierCPName = S != null ? S.ContactPerson : "",
                                           VMSupplierEmail = S != null ? S.Email : "",
                                           VMSupplierPhone = S != null ? S.Phone : "",
                                           VMCurrencyFK = PO.Common_CurrencyFK,
                                           VMCurrencyName = C != null ? C.Name : "",
                                           TermsAndCondition = PO.TermsAndCondition

                                       }).OrderByDescending(x => x.ID).FirstOrDefaultAsync());



            vM.DataListSlave = await Task.Run(() => (from POS in _db.Procurement_PurchaseOrderSlave.Where(x => x.Active && x.Procurement_PurchaseOrderFK == vM.Procurement_PurchaseOrderFK)
                                                     join RI in _db.Common_RawItem on POS.Common_RawItemFK equals RI.ID into RI_Join
                                                     from RI in RI_Join.DefaultIfEmpty()
                                                     join U in _db.Common_Unit on RI.Common_UnitFK equals U.ID into U_Join
                                                     from U in U_Join.DefaultIfEmpty()
                                                     join MS in _db.Merchandising_Style on POS.Merchandising_StyleID equals MS.ID into MS_Join
                                                     from MS in MS_Join.DefaultIfEmpty()
                                                     join MBO in _db.Merchandising_BuyerOrder on MS.Merchandising_BuyerOrderFK equals MBO.ID into MBO_Join
                                                     from MBO in MBO_Join.DefaultIfEmpty()
                                                     join MSS in _db.Merchandising_StyleSlave on POS.Merchandising_StyleSlaveFK equals MSS.ID into MSS_Join
                                                     from MSS in MSS_Join.DefaultIfEmpty()
                                                     join C in _db.Common_Color on MSS.Common_ColorFK equals C.ID into C_Join
                                                     from C in C_Join.DefaultIfEmpty()
                                                     select new VMPOReportSlave
                                                     {
                                                         BONoStyleName = MS != null && MBO != null ? MBO.BuyerPO + "/" + MS.StyleName : "",
                                                         StyleColorName = MSS != null && C != null ? C.Name : "",
                                                         StyleGSM = MSS != null ? (double)MSS.GSM : 0,
                                                         Procurement_PurchaseOrderFK = POS != null ? POS.Procurement_PurchaseOrderFK : 0,
                                                         Procurement_PurchaseRequisitionSlaveFK = POS != null ? POS.Procurement_PurchaseRequisitionSlaveFK.Value : 0,
                                                         ID = POS != null ? POS.ID : 0,
                                                         PurchaseQuantity = POS != null ? POS.PurchaseQuantity : 0,
                                                         ReceivedQuantity = _db.Store_StockIn.Where(x => x.Merchandising_StyleFK == POS.Merchandising_StyleID && x.ItemIDFK == POS.Common_RawItemFK).Select(x => x.ReceivedQty).DefaultIfEmpty(0).Sum(),
                                                         ReturnQuantity = _db.Store_StockOut.Where(x => x.Merchandising_StyleFK == POS.Merchandising_StyleID && x.ItemIDFK == POS.Common_RawItemFK).Select(x => x.ReceivedQty).DefaultIfEmpty(0).Sum(),
                                                         PurchasingPrice = POS != null ? POS.PurchasingPrice : 0,
                                                         DescriptionSlave = POS != null ? POS.Description : "",
                                                         VMRawItemFK = RI != null ? RI.ID : 0,
                                                         VMRawItemName = RI != null ? RI.Name : "",
                                                         VMUnitName = U != null ? U.Name : "",
                                                         //AvailableQuantity = RI != null ? RI.Quantity : 0 //AvailableQuantity has to come from Store
                                                     }).OrderByDescending(x => x.ID).ToListAsync());



            Common_CompanySetup company = await _db.Common_CompanySetup.FirstOrDefaultAsync(x => x.Active);
            if (company != null && company.ID > 0)
            {
                vM.CompanyName = company.Name;
                vM.CompanyShortName = company.ShortName;
                vM.CompanyAddress = company.Address;
                vM.CompanyPhone = company.Phone;
                vM.CompanyEmail = company.Email;
            }
            return vM;
        }
        #endregion

        #region ItemInventory

        public VMPOReportSlave PORawInventory(VMPOReportSlave model)
        {
            VMPOReportSlave vM = new VMPOReportSlave();
            vM.DataListSlave = new List<VMPOReportSlave>();

            if (model.ID == 1)
            {
                vM.TermsAndCondition = "Single Order Wise PO Invoice Inventory";
                #region FilterByOrder
                if (model.FromDate != null && model.ToDate != null)
                {
                    vM.DataListSlave = (from PO in _db.Procurement_PurchaseOrder
                                        join POS in _db.Procurement_PurchaseOrderSlave on PO.ID equals POS.Procurement_PurchaseOrderFK
                                        join ST in _db.Merchandising_Style on POS.Merchandising_StyleID equals ST.ID
                                        join order in _db.Merchandising_BuyerOrder on ST.Merchandising_BuyerOrderFK equals order.ID
                                        join S in _db.Common_Supplier on PO.Common_SupplierFK equals S.ID into S_Join
                                        from S in S_Join.DefaultIfEmpty()
                                        join Cur in _db.Common_Currency on PO.Common_CurrencyFK equals Cur.ID into C_Join
                                        from C in C_Join.DefaultIfEmpty()
                                        join Item in _db.Common_RawItem on POS.Common_RawItemFK equals Item.ID into Item_Join
                                        from Itm in Item_Join.DefaultIfEmpty()
                                        join Unit in _db.Common_Unit on Itm.Common_UnitFK equals Unit.ID
                                        where ST.Merchandising_BuyerOrderFK == model.OrderID && PO.OrderDate >= model.FromDate && PO.OrderDate <= model.ToDate
                                        && POS.Active == true && PO.Active == true
                                        where PO.Status == 2 || PO.Status == 3 || PO.Status == 4 || PO.Status == 5
                                        select new VMPOReportSlave
                                        {
                                            Procurement_PurchaseOrderFK = PO.ID,
                                            CID = PO.CID,
                                            StyleName = ST.CID+order.BuyerPO+"/" + ST.StyleName,
                                            OriginType = (ProcurementOriginTypeEnum)PO.OriginType,
                                            OrderDate = PO.OrderDate,
                                            POPaymentMethod = (SupplierPaymentMethodEnum)PO.POPaymentMethod,
                                            POType = (POTypeEnum)PO.POType,
                                            Description = PO.Description,
                                            Status = (POStatusEnum)PO.Status,
                                            VMSupplierFK = PO.Common_SupplierFK,
                                            VMSupplierName = S != null ? S.Name : "",
                                            VMSupplierCode = S != null ? S.Code : "",
                                            VMCurrencyName = C != null ? C.Name : "",
                                            VMUnitName = Unit.Name,
                                            VMRawItemName = Itm != null ? Itm.Name : "",
                                            VMRawItemFK = POS.Common_RawItemFK,
                                            VMMerchandising_StyleID = POS.Merchandising_StyleSlaveFK,
                                            VMMerchandising_StyleSlaveID = POS.Merchandising_StyleSlaveFK,
                                            PurchaseQuantity = POS.PurchaseQuantity,

                                            DataListSlave = (from SS in _db.Store_StockIn
                                                             join S in _db.Store_StockInMaster on SS.Store_StockInMasterFK equals S.ID
                                                             join Inv in _db.Procurement_PurchaseInvoiceSlave on SS.ID equals Inv.Store_StockInFK into Inv_join
                                                             from Invoice in Inv_join.DefaultIfEmpty()
                                                             where SS.Common_RawItemFK == POS.Common_RawItemFK && SS.Merchandising_StyleSlaveFK == POS.Merchandising_StyleSlaveFK && SS.Active == true
                                                             select new VMPOReportSlave
                                                             {
                                                                 ChallanNo = SS.ChallanNo,
                                                                 ReceiveDate = SS.ChallanDate,
                                                                 ReceivedQuantity = SS.ReceivedQty,
                                                                 PORate = Convert.ToDecimal(SS.UnitPrice),
                                                                 POValue = Convert.ToDecimal(SS.TotalPrice),
                                                                 InvoiceQty = Invoice != null ? Convert.ToDecimal(Invoice.InvoiceQuantity) : 0,
                                                                 InvoiceRate = Invoice != null ? Convert.ToDecimal(Invoice.InvoicePrice) : 0
                                                             }).ToList()
                                        }).OrderByDescending(x => x.OrderDate).ToList();

                    if (vM.DataListSlave.Any())
                    {
                        vM.DataListSlave.ForEach(a=>{

                            if (a.DataListSlave.Any())
                            {
                                a.DataListSlave.ForEach(x => {
                                    x.InvoicePrice= decimal.Multiply(x.InvoiceQty, x.InvoiceRate);
                                });
                                a.TotalInvoiceValue = a.DataListSlave.Sum(x => x.InvoicePrice);
                                a.TotalInvoiceQty = a.DataListSlave.Sum(x => x.InvoiceQty);
                            }
                        });
                    }
                    
                }
                else
                {
                    vM.DataListSlave = (from PO in _db.Procurement_PurchaseOrder
                                        join POS in _db.Procurement_PurchaseOrderSlave on PO.ID equals POS.Procurement_PurchaseOrderFK
                                        join ST in _db.Merchandising_Style on POS.Merchandising_StyleID equals ST.ID
                                        join order in _db.Merchandising_BuyerOrder on ST.Merchandising_BuyerOrderFK equals order.ID
                                        join S in _db.Common_Supplier on PO.Common_SupplierFK equals S.ID into S_Join
                                        from S in S_Join.DefaultIfEmpty()
                                        join Cur in _db.Common_Currency on PO.Common_CurrencyFK equals Cur.ID into C_Join
                                        from C in C_Join.DefaultIfEmpty()
                                        join Item in _db.Common_RawItem on POS.Common_RawItemFK equals Item.ID into Item_Join
                                        from Itm in Item_Join.DefaultIfEmpty()
                                        join Unit in _db.Common_Unit on Itm.Common_UnitFK equals Unit.ID
                                        where ST.Merchandising_BuyerOrderFK == model.OrderID && POS.Active== true && PO.Active==true
                                        where PO.Status == 2 || PO.Status == 3 || PO.Status == 4 || PO.Status == 5
                                        select new VMPOReportSlave
                                        {
                                            Procurement_PurchaseOrderFK = PO.ID,
                                            CID = PO.CID,
                                            StyleName = ST.CID+order.BuyerPO+"/" + ST.StyleName,
                                            OriginType = (ProcurementOriginTypeEnum)PO.OriginType,
                                            OrderDate = PO.OrderDate,
                                            POPaymentMethod = (SupplierPaymentMethodEnum)PO.POPaymentMethod,
                                            POType = (POTypeEnum)PO.POType,
                                            Description = PO.Description,
                                            Status = (POStatusEnum)PO.Status,
                                            VMSupplierFK = PO.Common_SupplierFK,
                                            VMSupplierName = S != null ? S.Name : "",
                                            VMSupplierCode = S != null ? S.Code : "",
                                            VMCurrencyName = C != null ? C.Name : "",
                                            VMUnitName = Unit.Name,
                                            VMRawItemName = Itm != null ? Itm.Name : "",
                                            VMRawItemFK = POS.Common_RawItemFK,
                                            VMMerchandising_StyleID = POS.Merchandising_StyleSlaveFK,
                                            VMMerchandising_StyleSlaveID = POS.Merchandising_StyleSlaveFK,
                                            PurchaseQuantity = POS.PurchaseQuantity,

                                            DataListSlave = (from SS in _db.Store_StockIn
                                                             join S in _db.Store_StockInMaster on SS.Store_StockInMasterFK equals S.ID
                                                             join Inv in _db.Procurement_PurchaseInvoiceSlave on SS.ID equals Inv.Store_StockInFK into Inv_join
                                                             from Invoice in Inv_join.DefaultIfEmpty()
                                                             where SS.Common_RawItemFK == POS.Common_RawItemFK && SS.Merchandising_StyleSlaveFK == POS.Merchandising_StyleSlaveFK && SS.Active == true
                                                             select new VMPOReportSlave
                                                             {
                                                                 ChallanNo = SS.ChallanNo,
                                                                 ReceiveDate = SS.ChallanDate,
                                                                 ReceivedQuantity = SS.ReceivedQty,
                                                                 PORate = Convert.ToDecimal(SS.UnitPrice),
                                                                 POValue = Convert.ToDecimal(SS.TotalPrice),
                                                                 InvoiceQty = Invoice != null ? Convert.ToDecimal(Invoice.InvoiceQuantity) : 0,
                                                                 InvoiceRate = Invoice != null ? Convert.ToDecimal(Invoice.InvoicePrice) : 0
                                                             }).ToList()
                                        }).OrderByDescending(x => x.OrderDate).ToList();

                    if (vM.DataListSlave.Any())
                    {
                        vM.DataListSlave.ForEach(a => {

                            if (a.DataListSlave.Any())
                            {
                                a.DataListSlave.ForEach(x => {
                                    x.InvoicePrice = decimal.Multiply(x.InvoiceQty, x.InvoiceRate);
                                });
                                a.TotalInvoiceValue = a.DataListSlave.Sum(x => x.InvoicePrice);
                                a.TotalInvoiceQty = a.DataListSlave.Sum(x => x.InvoiceQty);
                            }
                        });
                    }
                }
                #endregion
                
                //if (vData.Any())
                //{
                //    vData.ForEach(a => {
                //        VMPOReportSlave vModel = new VMPOReportSlave();
                //        vModel.Procurement_PurchaseOrderFK = a.Procurement_PurchaseOrderFK;
                //        vModel.VMRawItemFK = a.VMRawItemFK;
                //        vModel.VMMerchandising_StyleID = a.VMMerchandising_StyleID;
                //        vModel.VMMerchandising_StyleSlaveID = a.VMMerchandising_StyleSlaveID;
                //        vModel.StyleName = a.StyleName;
                //        vModel.CID = a.CID;
                //        vModel.OriginType = a.OriginType;
                //        vModel.OrderDate = a.OrderDate;
                //        vModel.POPaymentMethod = a.POPaymentMethod;
                //        vModel.POType = a.POType;
                //        vModel.Description = a.Description;
                //        vModel.Status = a.Status;
                //        vModel.VMSupplierFK = a.VMSupplierFK;
                //        vModel.VMSupplierName = a.VMSupplierName;
                //        vModel.VMSupplierCode = a.VMSupplierCode;
                //        vModel.VMCurrencyName = a.VMCurrencyName;
                //        vModel.VMUnitName = a.VMUnitName;
                //        vModel.VMRawItemName = a.VMRawItemName;
                //        vModel.PurchaseQuantity = a.PurchaseQuantity;

                //        if (a.vChallan.Any())
                //        {
                //            vModel.DataListSlave = new List<VMPOReportSlave>();
                //            a.vChallan.ForEach(b => {
                //                VMPOReportSlave vcModel = new VMPOReportSlave();
                //                vcModel.ChallanNo = b.ChallanNo;
                //                vcModel.ReceiveDate = b.ReceiveDate;
                //                vcModel.ReceivedQuantity = b.ReceiveQty;
                //                vcModel.InvoiceQty = (decimal)b.InvQty;
                //                vcModel.InvoiceRate = (decimal)b.InvRate;
                //                vcModel.InvoicePrice = decimal.Multiply(vcModel.InvoiceQty, vcModel.InvoiceRate);
                //                vModel.DataListSlave.Add(vcModel);
                //            });
                //            vModel.TotalInvoiceValue = vModel.DataListSlave.Sum(x => x.InvoicePrice);
                //            vModel.TotalInvoiceQty = vModel.DataListSlave.Sum(x => x.InvoiceQty);
                //        }
                //        vM.DataListSlave.Add(vModel);
                //    });
                //}
            }
            else if (model.ID == 2) {
                vM.TermsAndCondition = "Single Style Wise PO Invoice Inventory";
                #region FilterByStyle
                if (model.FromDate != null && model.ToDate != null)
                {
                    vM.DataListSlave = (from PO in _db.Procurement_PurchaseOrder
                                        join POS in _db.Procurement_PurchaseOrderSlave on PO.ID equals POS.Procurement_PurchaseOrderFK
                                        join ST in _db.Merchandising_Style on POS.Merchandising_StyleID equals ST.ID
                                        join order in _db.Merchandising_BuyerOrder on ST.Merchandising_BuyerOrderFK equals order.ID
                                        join S in _db.Common_Supplier on PO.Common_SupplierFK equals S.ID into S_Join
                                        from S in S_Join.DefaultIfEmpty()
                                        join Cur in _db.Common_Currency on PO.Common_CurrencyFK equals Cur.ID into C_Join
                                        from C in C_Join.DefaultIfEmpty()
                                        join Item in _db.Common_RawItem on POS.Common_RawItemFK equals Item.ID into Item_Join
                                        from Itm in Item_Join.DefaultIfEmpty()
                                        join Unit in _db.Common_Unit on Itm.Common_UnitFK equals Unit.ID
                                        where ST.ID == model.StyleID && PO.OrderDate >= model.FromDate && PO.OrderDate <= model.ToDate
                                        && POS.Active == true && PO.Active == true
                                        where PO.Status == 2 || PO.Status == 3 || PO.Status == 4 || PO.Status == 5
                                        select new VMPOReportSlave
                                        {
                                            Procurement_PurchaseOrderFK = PO.ID,
                                            CID = PO.CID,
                                            StyleName = ST.CID+order.BuyerPO+"/" + ST.StyleName,
                                            OriginType = (ProcurementOriginTypeEnum)PO.OriginType,
                                            OrderDate = PO.OrderDate,
                                            POPaymentMethod = (SupplierPaymentMethodEnum)PO.POPaymentMethod,
                                            POType = (POTypeEnum)PO.POType,
                                            Description = PO.Description,
                                            Status = (POStatusEnum)PO.Status,
                                            VMSupplierFK = PO.Common_SupplierFK,
                                            VMSupplierName = S != null ? S.Name : "",
                                            VMSupplierCode = S != null ? S.Code : "",
                                            VMCurrencyName = C != null ? C.Name : "",
                                            VMUnitName = Unit.Name,
                                            VMRawItemName = Itm != null ? Itm.Name : "",
                                            VMRawItemFK = POS.Common_RawItemFK,
                                            VMMerchandising_StyleID = POS.Merchandising_StyleSlaveFK,
                                            VMMerchandising_StyleSlaveID = POS.Merchandising_StyleSlaveFK,
                                            PurchaseQuantity = POS.PurchaseQuantity,

                                            DataListSlave = (from SS in _db.Store_StockIn
                                                             join S in _db.Store_StockInMaster on SS.Store_StockInMasterFK equals S.ID
                                                             join Inv in _db.Procurement_PurchaseInvoiceSlave on SS.ID equals Inv.Store_StockInFK into Inv_join
                                                             from Invoice in Inv_join.DefaultIfEmpty()
                                                             where SS.Common_RawItemFK == POS.Common_RawItemFK && SS.Merchandising_StyleSlaveFK == POS.Merchandising_StyleSlaveFK && SS.Active == true
                                                             select new VMPOReportSlave
                                                             {
                                                                 ChallanNo = SS.ChallanNo,
                                                                 ReceiveDate = SS.ChallanDate,
                                                                 ReceivedQuantity = SS.ReceivedQty,
                                                                 PORate = Convert.ToDecimal(SS.UnitPrice),
                                                                 POValue = Convert.ToDecimal(SS.TotalPrice),
                                                                 InvoiceQty = Invoice != null ? Convert.ToDecimal(Invoice.InvoiceQuantity) : 0,
                                                                 InvoiceRate = Invoice != null ? Convert.ToDecimal(Invoice.InvoicePrice) : 0
                                                             }).ToList()
                                        }).OrderByDescending(x => x.OrderDate).ToList();

                    if (vM.DataListSlave.Any())
                    {
                        vM.DataListSlave.ForEach(a => {

                            if (a.DataListSlave.Any())
                            {
                                a.DataListSlave.ForEach(x => {
                                    x.InvoicePrice = decimal.Multiply(x.InvoiceQty, x.InvoiceRate);
                                });
                                a.TotalInvoiceValue = a.DataListSlave.Sum(x => x.InvoicePrice);
                                a.TotalInvoiceQty = a.DataListSlave.Sum(x => x.InvoiceQty);
                            }
                        });
                    }

                }
                else
                {
                    vM.DataListSlave = (from PO in _db.Procurement_PurchaseOrder
                                        join POS in _db.Procurement_PurchaseOrderSlave on PO.ID equals POS.Procurement_PurchaseOrderFK
                                        join ST in _db.Merchandising_Style on POS.Merchandising_StyleID equals ST.ID
                                        join order in _db.Merchandising_BuyerOrder on ST.Merchandising_BuyerOrderFK equals order.ID
                                        join S in _db.Common_Supplier on PO.Common_SupplierFK equals S.ID into S_Join
                                        from S in S_Join.DefaultIfEmpty()
                                        join Cur in _db.Common_Currency on PO.Common_CurrencyFK equals Cur.ID into C_Join
                                        from C in C_Join.DefaultIfEmpty()
                                        join Item in _db.Common_RawItem on POS.Common_RawItemFK equals Item.ID into Item_Join
                                        from Itm in Item_Join.DefaultIfEmpty()
                                        join Unit in _db.Common_Unit on Itm.Common_UnitFK equals Unit.ID
                                        where ST.ID == model.StyleID && POS.Active == true && PO.Active == true
                                        where PO.Status == 2 || PO.Status == 3 || PO.Status == 4 || PO.Status == 5
                                        select new VMPOReportSlave
                                        {
                                            Procurement_PurchaseOrderFK = PO.ID,
                                            CID = PO.CID,
                                            StyleName = ST.CID+order.BuyerPO+"/" + ST.StyleName,
                                            OriginType = (ProcurementOriginTypeEnum)PO.OriginType,
                                            OrderDate = PO.OrderDate,
                                            POPaymentMethod = (SupplierPaymentMethodEnum)PO.POPaymentMethod,
                                            POType = (POTypeEnum)PO.POType,
                                            Description = PO.Description,
                                            Status = (POStatusEnum)PO.Status,
                                            VMSupplierFK = PO.Common_SupplierFK,
                                            VMSupplierName = S != null ? S.Name : "",
                                            VMSupplierCode = S != null ? S.Code : "",
                                            VMCurrencyName = C != null ? C.Name : "",
                                            VMUnitName = Unit.Name,
                                            VMRawItemName = Itm != null ? Itm.Name : "",
                                            VMRawItemFK = POS.Common_RawItemFK,
                                            VMMerchandising_StyleID = POS.Merchandising_StyleSlaveFK,
                                            VMMerchandising_StyleSlaveID = POS.Merchandising_StyleSlaveFK,
                                            PurchaseQuantity = POS.PurchaseQuantity,

                                            DataListSlave = (from SS in _db.Store_StockIn
                                                             join S in _db.Store_StockInMaster on SS.Store_StockInMasterFK equals S.ID
                                                             join Inv in _db.Procurement_PurchaseInvoiceSlave on SS.ID equals Inv.Store_StockInFK into Inv_join
                                                             from Invoice in Inv_join.DefaultIfEmpty()
                                                             where SS.Common_RawItemFK == POS.Common_RawItemFK && SS.Merchandising_StyleSlaveFK == POS.Merchandising_StyleSlaveFK && SS.Active == true
                                                             select new VMPOReportSlave
                                                             {
                                                                 ChallanNo = SS.ChallanNo,
                                                                 ReceiveDate = SS.ChallanDate,
                                                                 ReceivedQuantity = SS.ReceivedQty,
                                                                 PORate = Convert.ToDecimal(SS.UnitPrice),
                                                                 POValue = Convert.ToDecimal(SS.TotalPrice),
                                                                 InvoiceQty = Invoice != null ? Convert.ToDecimal(Invoice.InvoiceQuantity) : 0,
                                                                 InvoiceRate = Invoice != null ? Convert.ToDecimal(Invoice.InvoicePrice) : 0
                                                             }).ToList()
                                        }).OrderByDescending(x => x.OrderDate).ToList();

                    if (vM.DataListSlave.Any())
                    {
                        vM.DataListSlave.ForEach(a => {

                            if (a.DataListSlave.Any())
                            {
                                a.DataListSlave.ForEach(x => {
                                    x.InvoicePrice = decimal.Multiply(x.InvoiceQty, x.InvoiceRate);
                                });
                                a.TotalInvoiceValue = a.DataListSlave.Sum(x => x.InvoicePrice);
                                a.TotalInvoiceQty = a.DataListSlave.Sum(x => x.InvoiceQty);
                            }
                        });
                    }
                }
                #endregion
            }
            else if (model.ID == 3) {
                vM.TermsAndCondition = "Single Supplier Wise PO Invoice Inventory";
                #region FilterBySupplier
                if (model.FromDate != null && model.ToDate != null)
                {
                    vM.DataListSlave = (from PO in _db.Procurement_PurchaseOrder
                                        join POS in _db.Procurement_PurchaseOrderSlave on PO.ID equals POS.Procurement_PurchaseOrderFK
                                        join ST in _db.Merchandising_Style on POS.Merchandising_StyleID equals ST.ID
                                        join order in _db.Merchandising_BuyerOrder on ST.Merchandising_BuyerOrderFK equals order.ID
                                        join S in _db.Common_Supplier on PO.Common_SupplierFK equals S.ID into S_Join
                                        from S in S_Join.DefaultIfEmpty()
                                        join Cur in _db.Common_Currency on PO.Common_CurrencyFK equals Cur.ID into C_Join
                                        from C in C_Join.DefaultIfEmpty()
                                        join Item in _db.Common_RawItem on POS.Common_RawItemFK equals Item.ID into Item_Join
                                        from Itm in Item_Join.DefaultIfEmpty()
                                        join Unit in _db.Common_Unit on Itm.Common_UnitFK equals Unit.ID
                                        where PO.Common_SupplierFK == model.SupplierID && PO.OrderDate >= model.FromDate && PO.OrderDate <= model.ToDate
                                        && POS.Active == true && PO.Active == true
                                        where PO.Status == 2 || PO.Status == 3 || PO.Status == 4 || PO.Status == 5
                                        select new VMPOReportSlave
                                        {
                                            Procurement_PurchaseOrderFK = PO.ID,
                                            CID = PO.CID,
                                            StyleName = ST.CID+order.BuyerPO+"/" + ST.StyleName,
                                            OriginType = (ProcurementOriginTypeEnum)PO.OriginType,
                                            OrderDate = PO.OrderDate,
                                            POPaymentMethod = (SupplierPaymentMethodEnum)PO.POPaymentMethod,
                                            POType = (POTypeEnum)PO.POType,
                                            Description = PO.Description,
                                            Status = (POStatusEnum)PO.Status,
                                            VMSupplierFK = PO.Common_SupplierFK,
                                            VMSupplierName = S != null ? S.Name : "",
                                            VMSupplierCode = S != null ? S.Code : "",
                                            VMCurrencyName = C != null ? C.Name : "",
                                            VMUnitName = Unit.Name,
                                            VMRawItemName = Itm != null ? Itm.Name : "",
                                            VMRawItemFK = POS.Common_RawItemFK,
                                            VMMerchandising_StyleID = POS.Merchandising_StyleSlaveFK,
                                            VMMerchandising_StyleSlaveID = POS.Merchandising_StyleSlaveFK,
                                            PurchaseQuantity = POS.PurchaseQuantity,

                                            DataListSlave = (from SS in _db.Store_StockIn
                                                             join S in _db.Store_StockInMaster on SS.Store_StockInMasterFK equals S.ID
                                                             join Inv in _db.Procurement_PurchaseInvoiceSlave on SS.ID equals Inv.Store_StockInFK into Inv_join
                                                             from Invoice in Inv_join.DefaultIfEmpty()
                                                             where SS.Common_RawItemFK == POS.Common_RawItemFK && SS.Merchandising_StyleSlaveFK == POS.Merchandising_StyleSlaveFK && SS.Active == true
                                                             select new VMPOReportSlave
                                                             {
                                                                 ChallanNo = SS.ChallanNo,
                                                                 ReceiveDate = SS.ChallanDate,
                                                                 ReceivedQuantity = SS.ReceivedQty,
                                                                 PORate = Convert.ToDecimal(SS.UnitPrice),
                                                                 POValue = Convert.ToDecimal(SS.TotalPrice),
                                                                 InvoiceQty = Invoice != null ? Convert.ToDecimal(Invoice.InvoiceQuantity) : 0,
                                                                 InvoiceRate = Invoice != null ? Convert.ToDecimal(Invoice.InvoicePrice) : 0
                                                             }).ToList()
                                        }).OrderByDescending(x => x.OrderDate).ToList();

                    if (vM.DataListSlave.Any())
                    {
                        vM.DataListSlave.ForEach(a => {

                            if (a.DataListSlave.Any())
                            {
                                a.DataListSlave.ForEach(x => {
                                    x.InvoicePrice = decimal.Multiply(x.InvoiceQty, x.InvoiceRate);
                                });
                                a.TotalInvoiceValue = a.DataListSlave.Sum(x => x.InvoicePrice);
                                a.TotalInvoiceQty = a.DataListSlave.Sum(x => x.InvoiceQty);
                            }
                        });
                    }

                }
                else
                {
                    vM.DataListSlave = (from PO in _db.Procurement_PurchaseOrder
                                        join POS in _db.Procurement_PurchaseOrderSlave on PO.ID equals POS.Procurement_PurchaseOrderFK
                                        join ST in _db.Merchandising_Style on POS.Merchandising_StyleID equals ST.ID
                                        join order in _db.Merchandising_BuyerOrder on ST.Merchandising_BuyerOrderFK equals order.ID
                                        join S in _db.Common_Supplier on PO.Common_SupplierFK equals S.ID into S_Join
                                        from S in S_Join.DefaultIfEmpty()
                                        join Cur in _db.Common_Currency on PO.Common_CurrencyFK equals Cur.ID into C_Join
                                        from C in C_Join.DefaultIfEmpty()
                                        join Item in _db.Common_RawItem on POS.Common_RawItemFK equals Item.ID into Item_Join
                                        from Itm in Item_Join.DefaultIfEmpty()
                                        join Unit in _db.Common_Unit on Itm.Common_UnitFK equals Unit.ID
                                        where PO.Common_SupplierFK == model.SupplierID && POS.Active == true && PO.Active == true
                                        where PO.Status == 2 || PO.Status == 3 || PO.Status == 4 || PO.Status == 5
                                        select new VMPOReportSlave
                                        {
                                            Procurement_PurchaseOrderFK = PO.ID,
                                            CID = PO.CID,
                                            StyleName = ST.CID+order.BuyerPO+"/" + ST.StyleName,
                                            OriginType = (ProcurementOriginTypeEnum)PO.OriginType,
                                            OrderDate = PO.OrderDate,
                                            POPaymentMethod = (SupplierPaymentMethodEnum)PO.POPaymentMethod,
                                            POType = (POTypeEnum)PO.POType,
                                            Description = PO.Description,
                                            Status = (POStatusEnum)PO.Status,
                                            VMSupplierFK = PO.Common_SupplierFK,
                                            VMSupplierName = S != null ? S.Name : "",
                                            VMSupplierCode = S != null ? S.Code : "",
                                            VMCurrencyName = C != null ? C.Name : "",
                                            VMUnitName = Unit.Name,
                                            VMRawItemName = Itm != null ? Itm.Name : "",
                                            VMRawItemFK = POS.Common_RawItemFK,
                                            VMMerchandising_StyleID = POS.Merchandising_StyleSlaveFK,
                                            VMMerchandising_StyleSlaveID = POS.Merchandising_StyleSlaveFK,
                                            PurchaseQuantity = POS.PurchaseQuantity,

                                            DataListSlave = (from SS in _db.Store_StockIn
                                                             join S in _db.Store_StockInMaster on SS.Store_StockInMasterFK equals S.ID
                                                             join Inv in _db.Procurement_PurchaseInvoiceSlave on SS.ID equals Inv.Store_StockInFK into Inv_join
                                                             from Invoice in Inv_join.DefaultIfEmpty()
                                                             where SS.Common_RawItemFK == POS.Common_RawItemFK && SS.Merchandising_StyleSlaveFK == POS.Merchandising_StyleSlaveFK && SS.Active == true
                                                             select new VMPOReportSlave
                                                             {
                                                                 ChallanNo = SS.ChallanNo,
                                                                 ReceiveDate = SS.ChallanDate,
                                                                 ReceivedQuantity = SS.ReceivedQty,
                                                                 PORate = Convert.ToDecimal(SS.UnitPrice),
                                                                 POValue = Convert.ToDecimal(SS.TotalPrice),
                                                                 InvoiceQty = Invoice != null ? Convert.ToDecimal(Invoice.InvoiceQuantity) : 0,
                                                                 InvoiceRate = Invoice != null ? Convert.ToDecimal(Invoice.InvoicePrice) : 0
                                                             }).ToList()
                                        }).OrderByDescending(x => x.OrderDate).ToList();

                    if (vM.DataListSlave.Any())
                    {
                        vM.DataListSlave.ForEach(a => {

                            if (a.DataListSlave.Any())
                            {
                                a.DataListSlave.ForEach(x => {
                                    x.InvoicePrice = decimal.Multiply(x.InvoiceQty, x.InvoiceRate);
                                });
                                a.TotalInvoiceValue = a.DataListSlave.Sum(x => x.InvoicePrice);
                                a.TotalInvoiceQty = a.DataListSlave.Sum(x => x.InvoiceQty);
                            }
                        });
                    }
                }
                #endregion
            }
            else if (model.ID == 4) {
                vM.TermsAndCondition = "Preodic Wise PO Invoice Inventory";
                vM.DataListSlave = (from PO in _db.Procurement_PurchaseOrder
                                    join POS in _db.Procurement_PurchaseOrderSlave on PO.ID equals POS.Procurement_PurchaseOrderFK
                                    join ST in _db.Merchandising_Style on POS.Merchandising_StyleID equals ST.ID
                                    join order in _db.Merchandising_BuyerOrder on ST.Merchandising_BuyerOrderFK equals order.ID
                                    join S in _db.Common_Supplier on PO.Common_SupplierFK equals S.ID into S_Join
                                    from S in S_Join.DefaultIfEmpty()
                                    join Cur in _db.Common_Currency on PO.Common_CurrencyFK equals Cur.ID into C_Join
                                    from C in C_Join.DefaultIfEmpty()
                                    join Item in _db.Common_RawItem on POS.Common_RawItemFK equals Item.ID into Item_Join
                                    from Itm in Item_Join.DefaultIfEmpty()
                                    join Unit in _db.Common_Unit on Itm.Common_UnitFK equals Unit.ID
                                    where POS.Active == true && PO.Active == true
                                    where PO.Status == 2 || PO.Status == 3 || PO.Status == 4 || PO.Status == 5
                                    select new VMPOReportSlave
                                    {
                                        Procurement_PurchaseOrderFK = PO.ID,
                                        CID = PO.CID,
                                        StyleName = ST.CID+order.BuyerPO+"/" + ST.StyleName,
                                        OriginType = (ProcurementOriginTypeEnum)PO.OriginType,
                                        OrderDate = PO.OrderDate,
                                        POPaymentMethod = (SupplierPaymentMethodEnum)PO.POPaymentMethod,
                                        POType = (POTypeEnum)PO.POType,
                                        Description = PO.Description,
                                        Status = (POStatusEnum)PO.Status,
                                        VMSupplierFK = PO.Common_SupplierFK,
                                        VMSupplierName = S != null ? S.Name : "",
                                        VMSupplierCode = S != null ? S.Code : "",
                                        VMCurrencyName = C != null ? C.Name : "",
                                        VMUnitName = Unit.Name,
                                        VMRawItemName = Itm != null ? Itm.Name : "",
                                        VMRawItemFK = POS.Common_RawItemFK,
                                        VMMerchandising_StyleID = POS.Merchandising_StyleSlaveFK,
                                        VMMerchandising_StyleSlaveID = POS.Merchandising_StyleSlaveFK,
                                        PurchaseQuantity = POS.PurchaseQuantity,

                                        DataListSlave = (from SS in _db.Store_StockIn
                                                         join S in _db.Store_StockInMaster on SS.Store_StockInMasterFK equals S.ID
                                                         join Inv in _db.Procurement_PurchaseInvoiceSlave on SS.ID equals Inv.Store_StockInFK into Inv_join
                                                         from Invoice in Inv_join.DefaultIfEmpty()
                                                         where SS.Common_RawItemFK == POS.Common_RawItemFK && SS.Merchandising_StyleSlaveFK == POS.Merchandising_StyleSlaveFK && SS.Active == true
                                                         select new VMPOReportSlave
                                                         {
                                                             ChallanNo = SS.ChallanNo,
                                                             ReceiveDate = SS.ChallanDate,
                                                             ReceivedQuantity = SS.ReceivedQty,
                                                             PORate = Convert.ToDecimal(SS.UnitPrice),
                                                             POValue = Convert.ToDecimal(SS.TotalPrice),
                                                             InvoiceQty = Invoice != null ? Convert.ToDecimal(Invoice.InvoiceQuantity) : 0,
                                                             InvoiceRate = Invoice != null ? Convert.ToDecimal(Invoice.InvoicePrice) : 0
                                                         }).ToList()
                                    }).OrderByDescending(x => x.OrderDate).ToList();

                if (vM.DataListSlave.Any())
                {
                    vM.DataListSlave.ForEach(a => {

                        if (a.DataListSlave.Any())
                        {
                            a.DataListSlave.ForEach(x => {
                                x.InvoicePrice = decimal.Multiply(x.InvoiceQty, x.InvoiceRate);
                            });
                            a.TotalInvoiceValue = a.DataListSlave.Sum(x => x.InvoicePrice);
                            a.TotalInvoiceQty = a.DataListSlave.Sum(x => x.InvoiceQty);
                        }
                    });
                }
            }

            return vM;
        }

        #endregion

        public async Task<VMBPOReportSlave> POReportGet(VMBPOReportSlave model)
        {

            VMBPOReportSlave vM = new VMBPOReportSlave();

            var vData = _db.Merchandising_Style
                                       .Include(x => x.Merchandising_BuyerOrder)
                                       .Include(x => x.Merchandising_BuyerOrder.Common_Buyer)
                                       .Include(x => x.Common_FinishItem)
                                       .Include(x => x.Common_FinishItem.Common_FinishSubCategory)
                                       .Include(x => x.Common_FinishItem.Common_FinishSubCategory.Common_FinishCategory)
                                       .Where(x => x.Active && x.ID == model.VMMerchandising_StyleID)
                                       .Select(x => new VMBPOReportSlave
                                       {
                                           VMMerchandising_StyleID = x.ID,
                                           CID = x.CID + x.StyleName,
                                           FirstMove = x.FirstMove,
                                           UpdateDate = x.Time,
                                           ItemName = x.Common_FinishItem.Name,
                                           Class = x.Common_FinishItem.Common_FinishSubCategory.Common_FinishCategory.Name,
                                           Fabrication = x.Fabrication,
                                           Reference = x.ReferenceNo,
                                           Style = x.StyleName,

                                           PackQty = x.PackQty,
                                           PackPieceQty = x.PackPieceQty,
                                           PieceQty = x.PieceQty,


                                           DozonQty = (double)decimal.Divide(x.PieceQty, 12),
                                           BuyerPO = x.Merchandising_BuyerOrder.BuyerPO,
                                           OrderDate = x.Merchandising_BuyerOrder.OrderDate,
                                           Season = x.Merchandising_BuyerOrder.Season,
                                           BuyerName = x.Merchandising_BuyerOrder.Common_Buyer.Name,

                                           PackPrice = (double)x.PackPrice,
                                           UnitPrice=x.UnitPrice,
                                           StylePrice = (double)decimal.Multiply(x.PackPrice, x.PackQty),
                                           Description = x.Description,

                                           SizeName = string.Join(", ", _db.Merchandising_StyleMeasurement
                                                                        .Include(y => y.Common_Size)
                                                                        .Where(y => y.Active == true && y.Merchandising_StyleFK == x.ID)
                                                                        .GroupBy(a => a.Common_Size.Name, (key, group) => new { Size = key, Items = group.ToList() }).Select(a => a.Size).AsEnumerable()),

                                           UserID = x.UserID,
                                       }).OrderByDescending(x => x.CID).FirstOrDefault();

            vM = vData;


            var vPO = (from t1 in _db.Procurement_PurchaseOrderSlave
                       join t2 in _db.Procurement_PurchaseOrder on t1.Procurement_PurchaseOrderFK equals t2.ID
                       join t3 in _db.Common_Supplier on t2.Common_SupplierFK equals t3.ID
                       where t1.Active == true && t1.Merchandising_StyleID == model.VMMerchandising_StyleID
                       group t1 by new { t1.Procurement_PurchaseOrderFK, t2.CID, t2.OrderDate,t3.Name } into all
                       select new VMBPOReportSlave
                       {
                           CID = all.Key.CID,
                           OrderDate = all.Key.OrderDate,
                           ID = all.Key.Procurement_PurchaseOrderFK,
                           SupplierName=all.Key.Name

                       }).ToList();



            vM.DataListSlave = new List<VMBPOReportSlave>();
            int s1 = 0;
            int s2 = 0;
            if (vPO.Any())
            {
                foreach (var v in vPO)
                {
                    VMBPOReportSlave m = new VMBPOReportSlave();
                    m.ID = ++s1;
                    m.CID = v.CID;
                    m.OrderDate = v.OrderDate;
                    m.SupplierName = v.SupplierName;
                    m.DataListSlave = new List<VMBPOReportSlave>();

                    v.DataListSlave = (from a1 in _db.Procurement_PurchaseOrderSlave
                                       join a2 in _db.Common_RawItem on a1.Common_RawItemFK equals a2.ID into RI_Join
                                       from RI in RI_Join.DefaultIfEmpty()
                                       join U in _db.Common_Unit on RI.Common_UnitFK equals U.ID into U_Join
                                       from U in U_Join.DefaultIfEmpty()
                                       where a1.Procurement_PurchaseOrderFK == v.ID && a1.Active == true
                                       select new VMBPOReportSlave
                                       {
                                           VMRawItemFK = RI != null ? RI.ID : 0,
                                           VMRawItemName = RI != null ? RI.Name : "",
                                           VMUnitName = U != null ? U.Name : "",
                                           DescriptionSlave = a1.Description,
                                           StyleQuantity = a1.PurchaseQuantity,
                                           PurchasingPrice = a1.PurchasingPrice
                                       }).ToList();



                    if (v.DataListSlave.Any())
                    {
                        foreach (var v1 in v.DataListSlave)
                        {
                            v1.ID = ++s2;
                            m.DataListSlave.Add(v1);
                        }
                    }

                    vM.DataListSlave.Add(m);
                }
            }

            Common_CompanySetup company = await _db.Common_CompanySetup.FirstOrDefaultAsync(x => x.Active);
            if (company != null && company.ID > 0)
            {
                vM.CompanyName = company.Name;
                vM.CompanyShortName = company.ShortName;
                vM.CompanyAddress = company.Address;
                vM.CompanyPhone = company.Phone;
                vM.CompanyEmail = company.Email;
            }
            return vM;
        }

        public async Task<VMBPOReportSlave> PRReportGet(VMBPOReportSlave model)
        {

            VMBPOReportSlave vM = new VMBPOReportSlave();

            var vData = _db.Merchandising_Style
                                       .Include(x => x.Merchandising_BuyerOrder)
                                       .Include(x => x.Merchandising_BuyerOrder.Common_Buyer)
                                       .Include(x => x.Common_FinishItem)
                                       .Include(x => x.Common_FinishItem.Common_FinishSubCategory)
                                       .Include(x => x.Common_FinishItem.Common_FinishSubCategory.Common_FinishCategory)
                                       .Where(x => x.Active && x.ID == model.VMMerchandising_StyleID)
                                       .Select(x => new VMBPOReportSlave
                                       {
                                           VMMerchandising_StyleID = x.ID,
                                           CID = x.Merchandising_BuyerOrder.Common_Buyer.BuyerID + "/" + x.Merchandising_BuyerOrder.BuyerPO + "/" + x.CID + "/" + x.StyleName,
                                           FirstMove = x.FirstMove,
                                           UpdateDate = x.Time,
                                           ItemName = x.Common_FinishItem.Name,
                                           Class = x.Common_FinishItem.Common_FinishSubCategory.Common_FinishCategory.Name,
                                           Fabrication = x.Fabrication,
                                           Reference = x.ReferenceNo,
                                           Style = x.StyleName,

                                           PackQty = x.PackQty,
                                           PackPieceQty = x.PackPieceQty,
                                           PieceQty = x.PieceQty,


                                           DozonQty = (double)decimal.Divide(x.PieceQty, 12),
                                           BuyerPO = x.Merchandising_BuyerOrder.BuyerPO,
                                           OrderDate = x.Merchandising_BuyerOrder.OrderDate,
                                           Season = x.Merchandising_BuyerOrder.Season,
                                           BuyerName = x.Merchandising_BuyerOrder.Common_Buyer.Name,

                                           PackPrice = (double)x.PackPrice,
                                           StylePrice = (double)decimal.Multiply(x.PackPrice, x.PackQty),
                                           Description = x.Description,

                                           SizeName = string.Join(", ", _db.Merchandising_StyleMeasurement
                                                                        .Include(y => y.Common_Size)
                                                                        .Where(y => y.Active == true && y.Merchandising_StyleFK == x.ID)
                                                                        .GroupBy(a => a.Common_Size.Name, (key, group) => new { Size = key, Items = group.ToList() }).Select(a => a.Size).AsEnumerable()),

                                           UserID = x.UserID,
                                       }).OrderByDescending(x => x.CID).FirstOrDefault();

            vM = vData;

            var vBomData = (from MSS in _db.Merchandising_StyleSlave.Where(x => x.Active && x.Merchandising_StyleFK == vM.VMMerchandising_StyleID)
                            join RI in _db.Common_RawItem on MSS.Common_RawItemFK equals RI.ID
                            join RSC in _db.Common_RawSubCategory on RI.Common_RawSubCategoryFK equals RSC.ID
                            join RC in _db.Common_RawCategory on RSC.Common_RawCategoryFK equals RC.ID
                            join U in _db.Common_Unit on RI.Common_UnitFK equals U.ID into U_Join
                            from U in U_Join.DefaultIfEmpty()
                            join C in _db.Common_Color on MSS.Common_ColorFK equals C.ID into C_Join
                            from C in C_Join.DefaultIfEmpty()
                            select new VMBPOReportSlave
                            {
                                StyleColorName = MSS != null && C != null ? C.Name : "",
                                StyleGSM = MSS != null ? (double)MSS.GSM : 0,
                                StyleQuantity = (double)MSS.RequiredQty,
                                VMRawItemFK = RI != null ? RI.ID : 0,
                                VMRawItemName = RI != null ? RI.Name : "",
                                VMUnitName = U != null ? U.Name : "",
                                VMRawSubCategoryName = RSC.Name,
                                VMRawCategoryName = RC.Name

                            }).OrderByDescending(x => x.ID).ToListAsync();

            var vPO = (from t1 in _db.Procurement_PurchaseOrder
                       join t2 in _db.Procurement_PurchaseOrderSlave on t1.ID equals t2.Merchandising_StyleID
                       where t1.Active == true && t2.Merchandising_StyleID == model.VMMerchandising_StyleID
                       group t1 by new { t1.CID, t1.OrderDate } into all
                       select new VMBPOReportSlave
                       {
                           CID = all.Key.CID,
                           OrderDate = all.Key.OrderDate,
                           DataListSlave = (from a1 in _db.Procurement_PurchaseOrderSlave
                                            join a2 in _db.Common_RawItem on a1.Common_RawItemFK equals a2.ID into RI_Join
                                            from RI in RI_Join.DefaultIfEmpty()
                                            join U in _db.Common_Unit on RI.Common_UnitFK equals U.ID into U_Join
                                            from U in U_Join.DefaultIfEmpty()
                                            where a1.Merchandising_StyleID == model.VMMerchandising_StyleID && a1.Active == true
                                            select new VMBPOReportSlave
                                            {
                                                VMRawItemFK = RI != null ? RI.ID : 0,
                                                VMRawItemName = RI != null ? RI.Name : "",
                                                VMUnitName = U != null ? U.Name : "",
                                                DescriptionSlave = a1.Description,
                                                StyleQuantity = a1.PurchaseQuantity,
                                                PurchasingPrice = a1.PurchasingPrice
                                            }).ToList()
                       }).ToList();
            vM.DataListSlave = new List<VMBPOReportSlave>();
            int s1 = 0;
            int s2 = 0;
            if (vPO.Any())
            {
                foreach (var v in vPO)
                {
                    VMBPOReportSlave m = new VMBPOReportSlave();
                    m.ID = ++s1;
                    m.CID = v.CID;
                    m.OrderDate = v.OrderDate;
                    m.DataListSlave = new List<VMBPOReportSlave>();
                    if (v.DataListSlave.Any())
                    {
                        foreach (var v1 in v.DataListSlave)
                        {
                            v1.ID = ++s2;
                            m.DataListSlave.Add(v1);
                        }
                    }

                    vM.DataListSlave.Add(m);
                }
            }

            Common_CompanySetup company = await _db.Common_CompanySetup.FirstOrDefaultAsync(x => x.Active);
            if (company != null && company.ID > 0)
            {
                vM.CompanyName = company.Name;
                vM.CompanyShortName = company.ShortName;
                vM.CompanyAddress = company.Address;
                vM.CompanyPhone = company.Phone;
                vM.CompanyEmail = company.Email;
            }
            return vM;
        }

        #region Store Inventory
        public VmStoreReport ReportStore(int id = 0)
        {
            var vmmodel = new VmStoreReport();

            var data = (from ri in _db.Common_RawItem
                        join si in _db.Store_StockIn on ri.ID equals si.ItemIDFK
                        where ri.Active
                        //group s by s into g
                        select new VmStoreReport
                        {
                            SupplierName = "Test Supplier",
                            //StyleName = ms.CID + " " + s.Name,
                            BuyerName = ri.Name,
                            ItemCode = ri.ID.ToString(),
                            ItemName = ri.Name,
                            OpenningQty = 10000,
                            StoreReceivedQty = si.ReceivedQty,
                            TotalQty = (si.ReceivedQty),

                            //StoreOutQty = o.ReceivedQty,
                            //StoreInQty = o.ReceivedQty,
                            //MainStoreQty = o.ReceivedQty,
                            Remarks = ri.Remarks

                        }).ToList();
            vmmodel.Datalist = data;
            return vmmodel;
        }
        public VmStoreReport StoreInItemWiseReport(VmStoreReport vmodel)
        {
            var vmmodel = new VmStoreReport
            {
                Datalist = new List<VmStoreReport>()
            };

            List<Store_StockIn> storeIn = new List<Store_StockIn>();
            List<Store_StockIn> storeInMain = new List<Store_StockIn>();
            List<Store_StockOut> storeOut = new List<Store_StockOut>();

            if (_db.Store_StockIn.Any())
            {
                storeIn = (from ri in _db.Common_RawItem
                           join si in _db.Store_StockIn on ri.ID equals si.ItemIDFK
                           join po in _db.Procurement_PurchaseOrder on si.IPOFK equals po.ID
                           //join pos in _db.Procurement_PurchaseOrderSlave on po.ID equals pos.Procurement_PurchaseOrderFK
                           join u in _db.Common_Unit on ri.Common_UnitFK equals u.ID

                           where ri.Active && si.Active && po.Active && po.Status == 5
                           && po.OriginType == vmodel.ReportID
                           group new { ri, si, po, u } by ri.ID into item

                           select new Store_StockIn
                           {
                               ItemIDFK = item.First().ri.ID,
                               ReceivedQty = item.Sum(x => x.si.ReceivedQty)
                           }).ToList();

            }

            if (_db.Store_StockOut.Any())
            {
                storeOut = _db.Store_StockOut.Where(x => x.Active && x.Store_GeneralFK == 3
                ).GroupBy(i => i.ItemIDFK).Select(sin => new Store_StockOut
                {
                    ItemIDFK = sin.First().ItemIDFK,
                    ChallanNo = sin.First().ChallanNo,
                    ReceivedQty = sin.First().ReceivedQty
                }).ToList<Store_StockOut>();
            }
            var ReportTypeName = "Regular";
            if (vmodel.ReportID == 1)
            {
                ReportTypeName = "BOM/BOF";
            }
            else if (vmodel.ReportID == 2)
            {
                ReportTypeName = "BOM";
            }
            else if (vmodel.ReportID == 3)
            {
                ReportTypeName = "BOF";
            }


            var data = (from ri in _db.Common_RawItem
                        join si in _db.Store_StockIn on ri.ID equals si.ItemIDFK
                        join po in _db.Procurement_PurchaseOrder on si.IPOFK equals po.ID
                        //join pos in _db.Procurement_PurchaseOrderSlave on po.ID equals pos.Procurement_PurchaseOrderFK
                        join u in _db.Common_Unit on ri.Common_UnitFK equals u.ID

                        where ri.Active && si.Active && po.Active && po.Status == 5
                        && po.OriginType == vmodel.ReportID
                        group new { ri, si, po, u } by ri.ID into item

                        select new VmStoreReport
                        {
                            //ReportID = item.First().ri.ID,
                            ItemName = item.First().ri.Name + "-" + item.First().ri.ID,
                            Unit = item.First().u.Name,
                            OpenningQty = 0,  //item.First().pos.PurchaseQuantity,
                            StoreInQty = item.Sum(x => x.si.ReceivedQty),

                            //StoreInQty = storeIn.FirstOrDefault(x => x.ItemIDFK == item.First().si.ID).ReceivedQty == null ? 0  : storeIn.FirstOrDefault(x => x.ItemIDFK == item.First().si.ID).ReceivedQty,
                            StoreOutQty = storeOut.FirstOrDefault(x => x.ItemIDFK == item.First().si.ID).ReceivedQty == 0 ? 0 : storeOut.FirstOrDefault(x => x.ItemIDFK == item.First().si.ID).ReceivedQty,

                            ConsumeQty = 0,
                            BalanceQty = storeOut.FirstOrDefault(x => x.ItemIDFK == item.First().si.ID && x.Store_GeneralFK == 0).ReceivedQty == 0 ? 0 : storeOut.FirstOrDefault(x => x.ItemIDFK == item.First().si.ID).ReceivedQty,

                            TotalQty = storeOut.FirstOrDefault(x => x.ItemIDFK == item.First().si.ID).ReceivedQty == 0 ? 0 :
                            storeOut.FirstOrDefault(x => x.ItemIDFK == item.First().si.ID && x.Store_GeneralFK == 0).ReceivedQty == 0 ? 0 : storeOut.FirstOrDefault(x => x.ItemIDFK == item.First().si.ID).ReceivedQty,

                        }).ToList();

            vmmodel.Datalist = data;
            vmmodel.ReportTypeName = ReportTypeName;
            return vmmodel;
        }


        public List<object> ApprovedStyleList(int id = 0)
        {
            return integrationService.DDLBOMStyleList();
        }
        public object ReportStoreInventoryType(VmStoreReport vmodel)
        {
            var vmmodel = new VmStoreReport
            {
                Datalist = new List<VmStoreReport>()
            };

            List<Store_StockIn> storeIn = new List<Store_StockIn>();
            List<Store_StockIn> storeInMain = new List<Store_StockIn>();
            List<Store_StockOut> storeOut = new List<Store_StockOut>();

            if (_db.Store_StockIn.Any())
            {
                storeIn = (from ri in _db.Common_RawItem
                           join si in _db.Store_StockIn on ri.ID equals si.ItemIDFK
                           join po in _db.Procurement_PurchaseOrder on si.IPOFK equals po.ID
                           join pos in _db.Procurement_PurchaseOrderSlave on po.ID equals pos.Procurement_PurchaseOrderFK
                           join u in _db.Common_Unit on ri.Common_UnitFK equals u.ID

                           where ri.Active && si.Active && po.Active && pos.Active && po.Status == 5 && si.Merchandising_StyleFK == vmodel.Merchandising_StyleFK
                           && po.OriginType == vmodel.ReportID
                           group new { ri, si, po, pos, u } by ri.ID into item

                           select new Store_StockIn
                           {
                               ItemIDFK = item.First().ri.ID,
                               ReceivedQty = item.First().si.ReceivedQty
                           }).ToList();

            }

            if (_db.Store_StockOut.Any())
            {
                storeOut = _db.Store_StockOut.Where(x => x.Active && x.Store_GeneralFK == 3 && x.Merchandising_StyleFK == vmodel.Merchandising_StyleFK
                ).GroupBy(i => i.ItemIDFK).Select(sin => new Store_StockOut
                {
                    ItemIDFK = sin.First().ItemIDFK,
                    ChallanNo = sin.First().ChallanNo,
                    ReceivedQty = sin.First().ReceivedQty
                }).ToList<Store_StockOut>();
            }

            var dataraw = (from si in _db.Store_StockIn
                           join sg in _db.Store_General on si.Store_GeneralFK equals sg.ID
                           join ri in _db.Common_RawItem on si.Common_RawItemFK equals ri.ID
                           join po in _db.Procurement_PurchaseOrder on si.IPOFK equals po.ID
                           join pos in _db.Procurement_PurchaseOrderSlave on po.ID equals pos.Procurement_PurchaseOrderFK
                           join u in _db.Common_Unit on ri.Common_UnitFK equals u.ID

                           where ri.Active && si.Active && si.Active && sg.Common_StoreTypeFK == 1 && si.Merchandising_StyleFK == vmodel.Merchandising_StyleFK
                           group new { si, sg, ri, pos, u } by new { ri.ID, sg.Common_StoreTypeFK } into item
                           select new VmStoreReport
                           {
                               ReportID = (int)item.Max(x => x.sg.Common_StoreTypeFK),
                               ItemName = item.First().ri.Name + "-" + item.First().ri.ID,
                               Unit = item.First().u.Name,
                               OpenningQty = item.First().pos.PurchaseQuantity,
                               StoreInQty = item.First().si.ReceivedQty,

                               StoreOutQty = storeOut.FirstOrDefault(x => x.ItemIDFK == item.First().si.ID).ReceivedQty == 0 ? 0 : storeOut.FirstOrDefault(x => x.ItemIDFK == item.First().si.ID).ReceivedQty,

                               ConsumeQty = 0,
                               BalanceQty = storeOut.FirstOrDefault(x => x.ItemIDFK == item.First().si.ID && x.Store_GeneralFK == 0).ReceivedQty == 0 ? 0 : storeOut.FirstOrDefault(x => x.ItemIDFK == item.First().si.ID).ReceivedQty,

                               TotalQty = storeOut.FirstOrDefault(x => x.ItemIDFK == item.First().si.ID).ReceivedQty == 0 ? 0 :
                               storeOut.FirstOrDefault(x => x.ItemIDFK == item.First().si.ID && x.Store_GeneralFK == 0).ReceivedQty == 0 ? 0 : storeOut.FirstOrDefault(x => x.ItemIDFK == item.First().si.ID).ReceivedQty,

                           }).ToList();

            var datawip = (from si in _db.Store_StockIn
                           join sg in _db.Store_General on si.Store_GeneralFK equals sg.ID
                           join ri in _db.Common_RawItem on si.Common_RawItemFK equals ri.ID
                           join po in _db.Procurement_PurchaseOrder on si.IPOFK equals po.ID
                           join pos in _db.Procurement_PurchaseOrderSlave on po.ID equals pos.Procurement_PurchaseOrderFK
                           join u in _db.Common_Unit on ri.Common_UnitFK equals u.ID

                           where ri.Active && si.Active && si.Active && sg.Common_StoreTypeFK == 2 && si.Merchandising_StyleFK == vmodel.Merchandising_StyleFK
                           group new { si, sg, ri, pos, u } by new { ri.ID, sg.Common_StoreTypeFK } into item
                           select new VmStoreReport
                           {
                               ReportID = (int)item.Max(x => x.sg.Common_StoreTypeFK),
                               ItemName = item.First().ri.Name + "-" + item.First().ri.ID,
                               Unit = item.First().u.Name,
                               OpenningQty = 0, //item.First().pos.PurchaseQuantity,
                               StoreInQty = item.First().si.ReceivedQty,

                               StoreOutQty = storeOut.FirstOrDefault(x => x.ItemIDFK == item.First().si.ID).ReceivedQty == 0 ? 0 : storeOut.FirstOrDefault(x => x.ItemIDFK == item.First().si.ID).ReceivedQty,

                               ConsumeQty = 0,
                               BalanceQty = storeOut.FirstOrDefault(x => x.ItemIDFK == item.First().si.ID && x.Store_GeneralFK == 0).ReceivedQty == 0 ? 0 : storeOut.FirstOrDefault(x => x.ItemIDFK == item.First().si.ID).ReceivedQty,

                               TotalQty = storeOut.FirstOrDefault(x => x.ItemIDFK == item.First().si.ID).ReceivedQty == 0 ? 0 :
                               storeOut.FirstOrDefault(x => x.ItemIDFK == item.First().si.ID && x.Store_GeneralFK == 0).ReceivedQty == 0 ? 0 : storeOut.FirstOrDefault(x => x.ItemIDFK == item.First().si.ID).ReceivedQty,

                           }).ToList();

            var datarawwip = (from si in _db.Store_StockIn
                              join sg in _db.Store_General on si.Store_GeneralFK equals sg.ID
                              join ri in _db.Common_RawItem on si.Common_RawItemFK equals ri.ID
                              join po in _db.Procurement_PurchaseOrder on si.IPOFK equals po.ID
                              join pos in _db.Procurement_PurchaseOrderSlave on po.ID equals pos.Procurement_PurchaseOrderFK
                              join u in _db.Common_Unit on ri.Common_UnitFK equals u.ID

                              where ri.Active && si.Active && si.Active && sg.Common_StoreTypeFK == 3 && si.Merchandising_StyleFK == vmodel.Merchandising_StyleFK
                              group new { si, sg, ri, pos, u } by new { ri.ID, sg.Common_StoreTypeFK } into item
                              select new VmStoreReport
                              {
                                  ReportID = (int)item.Max(x => x.sg.Common_StoreTypeFK),
                                  ItemName = item.First().ri.Name + "-" + item.First().ri.ID,
                                  Unit = item.First().u.Name,
                                  OpenningQty = 0, //item.First().pos.PurchaseQuantity,
                                  StoreInQty = item.First().si.ReceivedQty,

                                  StoreOutQty = storeOut.FirstOrDefault(x => x.ItemIDFK == item.First().si.ID).ReceivedQty == 0 ? 0 : storeOut.FirstOrDefault(x => x.ItemIDFK == item.First().si.ID).ReceivedQty,

                                  ConsumeQty = 0,
                                  BalanceQty = storeOut.FirstOrDefault(x => x.ItemIDFK == item.First().si.ID && x.Store_GeneralFK == 0).ReceivedQty == 0 ? 0 : storeOut.FirstOrDefault(x => x.ItemIDFK == item.First().si.ID).ReceivedQty,

                                  TotalQty = storeOut.FirstOrDefault(x => x.ItemIDFK == item.First().si.ID).ReceivedQty == 0 ? 0 :
                               storeOut.FirstOrDefault(x => x.ItemIDFK == item.First().si.ID && x.Store_GeneralFK == 0).ReceivedQty == 0 ? 0 : storeOut.FirstOrDefault(x => x.ItemIDFK == item.First().si.ID).ReceivedQty,

                              }).ToList();

            vmmodel.Datalistraw = dataraw;
            vmmodel.Datalistwip = datawip;
            vmmodel.Datalistrawwip = datarawwip;
            vmmodel.ReportTypeName = vmodel.StyleName;
            return vmmodel;
        }

        public async Task<SelectList> RawItemDropDownList(int id)
        {
            return new SelectList(
                await _db.Common_RawItem.Where(x => x.Active && _db.Store_StockIn.Any(s => s.FixAsset && s.ItemIDFK == x.ID)).Select(x => new { Value = x.ID, Text = x.Name }).ToListAsync()
                , "Value", "Text");
        }
        public List<object> RawCategoryDropDownList()
        {
            var List = new List<object>();
            _db.Common_RawCategory.Where(x => x.Active).Select(x => x).ToList()
            .ForEach(x => List.Add(new { Value = x.ID, Text = x.Name }));
            return List;
        }
        public object ReportStoreInventoryDetails(VmStoreReport vmodel)
        {
            var vmmodel = new VmStoreReportDetails
            {
                Datalist = new List<VmStoreReportDetails>()
            };

            List<Store_StockIn> storeIn = new List<Store_StockIn>();

            var v = (from t1 in _db.Store_StockIn
                     join t2 in _db.Store_General on t1.Store_GeneralFK equals t2.ID
                     join t3 in _db.Common_RawItem on t1.Common_RawItemFK equals t3.ID
                     join t4 in _db.Common_Unit on t1.CommonUnitFK equals t4.ID
                     join t5 in _db.Procurement_PurchaseOrderSlave on t1.Procurement_PurchaseOrderSlaveFK equals t5.ID
                     join t6 in _db.Merchandising_StyleSlave on t5.Merchandising_StyleSlaveFK equals t6.ID

                     where t1.Active && t2.Active && t3.Active && t4.Active && t5.Active && t2.Common_StoreTypeFK == 1
                     group new { t1, t2, t3, t4, t5 } by new { t1.Common_RawItemFK, t2.Common_StoreTypeFK, t6.Merchandising_StyleFK } into Group
                     select new VmStoreReportDetails
                     {
                         ReportID = Group.Key.Common_StoreTypeFK.Value,
                         Common_RawItemFK = Group.Key.Common_RawItemFK,
                         Merchandising_StyleFK = Group.Key.Merchandising_StyleFK,
                         ItemName = Group.First().t3.Name + "-" + Group.Key.Common_RawItemFK,
                         Unit = Group.First().t4.Name,

                         StoreInQty = Group.Sum(x => x.t1.ReceivedQty),
                         StoreInValue = Group.Sum(x => x.t5.PurchasingPrice * x.t1.ReceivedQty)

                     }).ToList();

            var v2 = (from t1 in _db.Store_StockIn
                      join t2 in _db.Store_General on t1.Store_GeneralFK equals t2.ID
                      join t3 in _db.Common_RawItem on t1.Common_RawItemFK equals t3.ID
                      join t4 in _db.Common_Unit on t1.CommonUnitFK equals t4.ID
                      join t5 in _db.Procurement_PurchaseOrderSlave on t1.Procurement_PurchaseOrderSlaveFK equals t5.ID
                      join t6 in _db.Merchandising_StyleSlave on t5.Merchandising_StyleSlaveFK equals t6.ID

                      where t1.Active && t2.Active && t3.Active && t4.Active && t5.Active && t2.Common_StoreTypeFK == 2
                      group new { t1, t2, t3, t4, t5 } by new { t1.Common_RawItemFK, t2.Common_StoreTypeFK, t6.Merchandising_StyleFK } into Group
                      select new VmStoreReportDetails
                      {
                          ReportID = Group.Key.Common_StoreTypeFK.Value,
                          Common_RawItemFK = Group.Key.Common_RawItemFK,
                          Merchandising_StyleFK = Group.Key.Merchandising_StyleFK,
                          ItemName = Group.First().t3.Name + "-" + Group.Key.Common_RawItemFK,
                          Unit = Group.First().t4.Name,

                          StoreOutQty = Group.Sum(x => x.t1.ReceivedQty),
                          StoreOutValue = Group.Sum(x => x.t5.PurchasingPrice * x.t1.ReceivedQty)

                      }).ToList();

            var v3 = (from t1 in _db.Store_StockIn
                      join t2 in _db.Store_General on t1.Store_GeneralFK equals t2.ID
                      join t3 in _db.Common_RawItem on t1.Common_RawItemFK equals t3.ID
                      join t4 in _db.Common_Unit on t1.CommonUnitFK equals t4.ID
                      join t5 in _db.Procurement_PurchaseOrderSlave on t1.Procurement_PurchaseOrderSlaveFK equals t5.ID
                      join t6 in _db.Merchandising_StyleSlave on t5.Merchandising_StyleSlaveFK equals t6.ID

                      where t1.Active && t2.Active && t3.Active && t4.Active && t5.Active && t2.Common_StoreTypeFK == 3
                      group new { t1, t2, t3, t4, t5, t6 } by new { t1.Common_RawItemFK, t2.Common_StoreTypeFK, t6.Merchandising_StyleFK } into Group
                      select new VmStoreReportDetails
                      {
                          ReportID = Group.Key.Common_StoreTypeFK.Value,
                          Common_RawItemFK = Group.Key.Common_RawItemFK,
                          Merchandising_StyleFK = Group.Key.Merchandising_StyleFK,
                          ItemName = Group.First().t3.Name + "-" + Group.Key.Common_RawItemFK,
                          Unit = Group.First().t4.Name,

                          BalanceQty = Group.Sum(x => x.t1.ReceivedQty),
                          BalanceValue = Group.Sum(x => x.t5.PurchasingPrice * x.t1.ReceivedQty),

                          ProductionQty = Group.Sum(x => x.t1.ReceivedQty),
                          ProductionValue = Group.Sum(x => x.t1.ReceivedQty) * Convert.ToDouble(_db.Merchandising_Style.FirstOrDefault(x => x.ID == Group.FirstOrDefault().t6.Merchandising_StyleFK).UnitPrice),

                          // ((200 / 12 )  * 2 == 33 KG
                          //ProductionQty = Group.Sum(x => (x.t1.ReceivedQty / 12) * Convert.ToDouble(x.t6.Consumption)),

                          // 2.45 * ((200 / 12 )  * 2 == 33 KG
                          //ProductionValue = Group.Sum(x => x.t5.PurchasingPrice * (x.t1.ReceivedQty / 12) * Convert.ToDouble(x.t6.Consumption)),
                          // OpenningQty = Group.First().t6.

                      }).ToList();

            if (v.Any())
            {
                v.ForEach(a =>
                {
                    var sin = a.StoreInQty;
                    var sinv = a.StoreInValue;
                    var outqty = v2.Where(x => x.Common_RawItemFK == a.Common_RawItemFK);
                    if (outqty.Any())
                    {
                        a.StoreInQty = outqty.FirstOrDefault().StoreInQty;
                        a.StoreInValue = outqty.FirstOrDefault().StoreInValue;
                        a.StoreOutQty = outqty.FirstOrDefault().StoreOutQty;
                        a.StoreOutValue = outqty.FirstOrDefault().StoreOutValue;
                    }

                    var balance = v3.Where(x => x.Merchandising_StyleFK == a.Merchandising_StyleFK);
                    if (balance.Any())
                    {
                        a.BalanceQty = balance.FirstOrDefault().BalanceQty;
                        a.BalanceValue = balance.FirstOrDefault().BalanceValue;
                        a.StoreOutQty = balance.FirstOrDefault().BalanceQty - sin;
                        a.StoreOutValue = balance.FirstOrDefault().BalanceValue - sinv;

                        a.ProductionQty = balance.FirstOrDefault().ProductionQty;
                        a.ProductionValue = balance.FirstOrDefault().ProductionValue;
                    }
                });
                v.ToList();
            }

            vmmodel.Datalistraw = v;
            vmmodel.ReportTypeName = vmodel.StyleName;

            return vmmodel;
        }

        public object GetInventoryRepoets(int styleId)
        {
            try
            {
                //// Settings.  
                //SqlParameter usernameParam = new SqlParameter("@StyeleId", styleId.ToString() ?? (object)DBNull.Value);

                //// Processing.  [dbo].[Sp_ItemWiseConsumption]
                //string sqlQuery = "EXEC [dbo].[Sp_ItemWiseConsumption] " + "@StyeleId";
                //var userType = _db.ExecuteSqlCommandAsync(sqlQuery, usernameParam);

                var styeleId = new SqlParameter("@StyeleId", SqlDbType.Int) { Value = styleId };
                var command = "EXEC [dbo].[Sp_ItemWiseConsumption] @StyeleId";
                var result = _db.ExecuteSqlCommandAsync(command, styeleId);

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public object ReportStoreInventoryDetailsConsumption(VmStoreReport vmodel)
        {

            var vmmodel = new VmStoreItemWiseConsumption
            {
                Datalist = new List<VmStoreItemWiseConsumption>()
            };

            var storeIn = new List<Store_StockIn>();

            var v = _db.Store_ItemWiseConsumption.Where(x => x.Active && x.ID == 1 && x.Merchandising_StyleFK == vmodel.Merchandising_StyleFK).Select(s => new VmStoreItemWiseConsumption
            {
                Merchandising_StyleFK = s.Merchandising_StyleFK,
                Common_RawItemFK = s.Common_RawItemFK,
                Name = s.Name,
                UnitName = s.UnitName,
                ConsumptionDz = Convert.ToDouble(s.ConsumptionDz),
                OrderConsumQty = Convert.ToDouble(s.OrderConsumQty),
                FinishQtyDz = Convert.ToDouble(s.FinishQtyDz),

                TotalRecMain = Convert.ToDouble(s.TotalRecMain),
                TotalRecCutting = Convert.ToDouble(s.TotalRecCutting),
                TotalRecSewing = Convert.ToDouble(s.TotalRecSewing),
                TotalRecIroning = Convert.ToDouble(s.TotalRecIroning),
                TotalRecPacking = Convert.ToDouble(s.TotalRecPacking),
                TotalRecFinish = Convert.ToDouble(s.TotalRecFinish),

                TotalOutMain = Convert.ToDouble(s.TotalRecCutting) + Convert.ToDouble(s.TotalRecSewing) + Convert.ToDouble(s.TotalRecIroning) + Convert.ToDouble(s.TotalRecPacking), //Convert.ToDouble(s.TotalOutMain),
                TotalOutCutting = Convert.ToDouble(s.TotalOutCutting) - Convert.ToDouble(s.TotalRecSewing),
                TotalOutSewing = Convert.ToDouble(s.TotalOutSewing) - Convert.ToDouble(s.TotalRecIroning),
                TotalOutIroning = Convert.ToDouble(s.TotalOutIroning) - Convert.ToDouble(s.TotalRecPacking),
                TotalOutPacking = Convert.ToDouble(s.TotalOutPacking) - Convert.ToDouble(s.TotalRecFinish),
                TotalOutFinish = Convert.ToDouble(s.TotalRecCutting) + Convert.ToDouble(s.TotalRecSewing) + Convert.ToDouble(s.TotalRecIroning) + Convert.ToDouble(s.TotalRecPacking),// + Convert.ToDouble(s.TotalOutFinish),

                TotalRemMain = Convert.ToDouble(s.TotalRecMain) - Convert.ToDouble(s.TotalOutMain),
                TotalRemCutting = Convert.ToDouble(s.TotalRecCutting) - Convert.ToDouble(s.TotalOutCutting),
                TotalRemSewing = Convert.ToDouble(s.TotalRecSewing) - Convert.ToDouble(s.TotalOutSewing),
                TotalRemIroning = Convert.ToDouble(s.TotalRecIroning) - Convert.ToDouble(s.TotalOutIroning),
                TotalRemPacking = Convert.ToDouble(s.TotalRecPacking) - Convert.ToDouble(s.TotalOutPacking),
                TotalRemFinish = Convert.ToDouble(s.TotalRecFinish) - Convert.ToDouble(s.TotalOutFinish)

            }).ToList();


            var v2 = _db.Store_ItemWiseConsumption.Where(x => x.Active == false && x.ID == 0 && x.Merchandising_StyleFK == vmodel.Merchandising_StyleFK).Select(s => new VmStoreItemWiseConsumption
            {
                Merchandising_StyleFK = s.Merchandising_StyleFK,
                Common_RawItemFK = s.Common_RawItemFK,
                Name = s.Name,
                UnitName = s.UnitName,
                ConsumptionDz = Convert.ToDouble(s.ConsumptionDz),
                OrderConsumQty = Convert.ToDouble(s.OrderConsumQty),
                FinishQtyDz = Convert.ToDouble(s.FinishQtyDz),

                TotalRecMain = Convert.ToDouble(s.TotalRecMain),
                TotalRecCutting = Convert.ToDouble(s.TotalRecCutting),
                TotalRecSewing = Convert.ToDouble(s.TotalRecSewing),
                TotalRecIroning = Convert.ToDouble(s.TotalRecIroning),
                TotalRecPacking = Convert.ToDouble(s.TotalRecPacking),
                TotalRecFinish = Convert.ToDouble(s.TotalRecFinish),

                TotalOutMain = Convert.ToDouble(s.TotalOutMain),
                TotalOutCutting = Convert.ToDouble(s.TotalOutCutting),
                TotalOutSewing = Convert.ToDouble(s.TotalOutSewing),
                TotalOutIroning = Convert.ToDouble(s.TotalOutIroning),
                TotalOutPacking = Convert.ToDouble(s.TotalOutPacking),
                TotalOutFinish = Convert.ToDouble(s.TotalOutFinish),

                TotalRemMain = Convert.ToDouble(s.TotalRecMain) - Convert.ToDouble(s.TotalOutMain),
                TotalRemCutting = Convert.ToDouble(s.TotalRecCutting) - Convert.ToDouble(s.TotalOutCutting),
                TotalRemSewing = Convert.ToDouble(s.TotalRecSewing) - Convert.ToDouble(s.TotalOutSewing),
                TotalRemIroning = Convert.ToDouble(s.TotalRecIroning) - Convert.ToDouble(s.TotalOutIroning),
                TotalRemPacking = Convert.ToDouble(s.TotalRecPacking) - Convert.ToDouble(s.TotalOutPacking),
                TotalRemFinish = Convert.ToDouble(s.TotalRecFinish) - Convert.ToDouble(s.TotalOutFinish)

            }).ToList();

            var v3 = _db.Store_ItemWiseConsumption.Where(x => x.Active == false && x.ID == 3 && x.Merchandising_StyleFK == vmodel.Merchandising_StyleFK).Select(s => new VmStoreItemWiseConsumption
            {
                Merchandising_StyleFK = s.Merchandising_StyleFK,
                Common_RawItemFK = s.Common_RawItemFK,
                Name = s.Name,
                UnitName = s.UnitName,
                ConsumptionDz = Convert.ToDouble(s.ConsumptionDz),
                OrderConsumQty = Convert.ToDouble(s.OrderConsumQty),
                FinishQtyDz = Convert.ToDouble(s.FinishQtyDz),

                TotalRecMain = Convert.ToDouble(s.TotalRecMain),
                TotalRecCutting = Convert.ToDouble(s.TotalRecCutting),
                TotalRecSewing = Convert.ToDouble(s.TotalRecSewing),
                TotalRecIroning = Convert.ToDouble(s.TotalRecIroning),
                TotalRecPacking = Convert.ToDouble(s.TotalRecPacking),
                TotalRecFinish = Convert.ToDouble(s.TotalRecFinish),

                TotalOutMain = Convert.ToDouble(s.TotalOutMain),
                TotalOutCutting = Convert.ToDouble(s.TotalOutCutting),
                TotalOutSewing = Convert.ToDouble(s.TotalOutSewing),
                TotalOutIroning = Convert.ToDouble(s.TotalOutIroning),
                TotalOutPacking = Convert.ToDouble(s.TotalOutPacking),
                TotalOutFinish = Convert.ToDouble(s.TotalOutFinish),

                TotalRemMain = Convert.ToDouble(s.TotalRecMain) - Convert.ToDouble(s.TotalOutMain),
                TotalRemCutting = Convert.ToDouble(s.TotalRecCutting) - Convert.ToDouble(s.TotalOutCutting),
                TotalRemSewing = Convert.ToDouble(s.TotalRecSewing) - Convert.ToDouble(s.TotalOutSewing),
                TotalRemIroning = Convert.ToDouble(s.TotalRecIroning) - Convert.ToDouble(s.TotalOutIroning),
                TotalRemPacking = Convert.ToDouble(s.TotalRecPacking) - Convert.ToDouble(s.TotalOutPacking),
                TotalRemFinish = Convert.ToDouble(s.TotalRecFinish) - Convert.ToDouble(s.TotalOutFinish)

            }).ToList();
            vmmodel.Datalist = v;
            vmmodel.DatalistFinish = v2;
            vmmodel.DatalistFinishConsumtion = v3;
            vmmodel.ReportTypeName = vmodel.StyleName;
            return vmmodel;
        }

        public object ReportStoreInventoryDetailsConsumptionDetails(VmStoreReport vmodel)
        {
            var vmmodel = new VmStoreItemWiseConsumption
            {
                Datalist = new List<VmStoreItemWiseConsumption>()
            };

            var storeIn = new List<Store_StockIn>();

            var v = _db.Store_ItemWiseConsumption.Where(x => x.Active && x.ID == 2 && x.Merchandising_StyleFK == vmodel.Merchandising_StyleFK).Select(s => new VmStoreItemWiseConsumption
            {
                Merchandising_StyleFK = s.Merchandising_StyleFK,
                Common_RawItemFK = s.Common_RawItemFK,
                Name = s.Name,
                UnitName = s.UnitName,
                ConsumptionDz = Convert.ToDouble(s.ConsumptionDz),
                OrderConsumQty = Convert.ToDouble(s.OrderConsumQty),
                FinishQtyDz = Convert.ToDouble(s.FinishQtyDz),

                TotalRecMain = Convert.ToDouble(s.TotalRecMain),
                TotalRecCutting = Convert.ToDouble(s.TotalRecCutting),
                TotalRecSewing = Convert.ToDouble(s.TotalRecSewing),
                TotalRecIroning = Convert.ToDouble(s.TotalRecIroning),
                TotalRecPacking = Convert.ToDouble(s.TotalRecPacking),
                TotalRecFinish = Convert.ToDouble(s.TotalRecFinish),

                TotalOutMain = Convert.ToDouble(s.TotalRecCutting) + Convert.ToDouble(s.TotalRecSewing) + Convert.ToDouble(s.TotalRecIroning) + Convert.ToDouble(s.TotalRecPacking),// + Convert.ToDouble(s.TotalOutFinish),
                TotalOutCutting = Convert.ToDouble(s.TotalOutCutting),
                TotalOutSewing = Convert.ToDouble(s.TotalOutSewing),
                TotalOutIroning = Convert.ToDouble(s.TotalOutIroning),
                TotalOutPacking = Convert.ToDouble(s.TotalOutPacking),
                TotalOutFinish = Convert.ToDouble(s.TotalRecCutting) + Convert.ToDouble(s.TotalRecSewing) + Convert.ToDouble(s.TotalRecIroning) + Convert.ToDouble(s.TotalRecPacking),// + Convert.ToDouble(s.TotalOutFinish),

                TotalRemMain = Convert.ToDouble(s.TotalRecMain) - (Convert.ToDouble(s.TotalRecCutting) + Convert.ToDouble(s.TotalRecSewing) + Convert.ToDouble(s.TotalRecIroning) + Convert.ToDouble(s.TotalRecPacking)), //Convert.ToDouble(s.TotalOutMain),
                TotalRemCutting = 0,
                TotalRemSewing = 0,
                TotalRemIroning = 0,
                TotalRemPacking = 0,
                TotalRemFinish = (Convert.ToDouble(s.TotalRecCutting) + Convert.ToDouble(s.TotalRecSewing) + Convert.ToDouble(s.TotalRecIroning) + Convert.ToDouble(s.TotalRecPacking) - Convert.ToDouble(s.TotalRecFinish))

            }).OrderBy(x => x.Name).ToList();

            var v2 = _db.Store_ItemWiseConsumption.Where(x => x.Active == false && x.ID == 0 && x.Merchandising_StyleFK == vmodel.Merchandising_StyleFK).Select(s => new VmStoreItemWiseConsumption
            {
                Merchandising_StyleFK = s.Merchandising_StyleFK,
                Common_RawItemFK = s.Common_RawItemFK,
                Name = s.Name,
                UnitName = s.UnitName,
                ConsumptionDz = Convert.ToDouble(s.ConsumptionDz),
                OrderConsumQty = Convert.ToDouble(s.OrderConsumQty),
                FinishQtyDz = Convert.ToDouble(s.FinishQtyDz),

                TotalRecMain = Convert.ToDouble(s.TotalRecMain),
                TotalRecCutting = Convert.ToDouble(s.TotalRecCutting),
                TotalRecSewing = Convert.ToDouble(s.TotalRecSewing),
                TotalRecIroning = Convert.ToDouble(s.TotalRecIroning),
                TotalRecPacking = Convert.ToDouble(s.TotalRecPacking),
                TotalRecFinish = Convert.ToDouble(s.TotalRecFinish),

                TotalOutMain = Convert.ToDouble(s.TotalOutMain),
                TotalOutCutting = Convert.ToDouble(s.TotalOutCutting),
                TotalOutSewing = Convert.ToDouble(s.TotalOutSewing),
                TotalOutIroning = Convert.ToDouble(s.TotalOutIroning),
                TotalOutPacking = Convert.ToDouble(s.TotalOutPacking),
                TotalOutFinish = Convert.ToDouble(s.TotalOutFinish),

                TotalRemMain = Convert.ToDouble(s.TotalRecMain) - Convert.ToDouble(s.TotalOutMain),
                TotalRemCutting = Convert.ToDouble(s.TotalRecCutting) - Convert.ToDouble(s.TotalOutCutting),
                TotalRemSewing = Convert.ToDouble(s.TotalRecSewing) - Convert.ToDouble(s.TotalOutSewing),
                TotalRemIroning = Convert.ToDouble(s.TotalRecIroning) - Convert.ToDouble(s.TotalOutIroning),
                TotalRemPacking = Convert.ToDouble(s.TotalRecPacking) - Convert.ToDouble(s.TotalOutPacking),
                TotalRemFinish = Convert.ToDouble(s.TotalRecFinish) - Convert.ToDouble(s.TotalOutFinish)

            }).ToList();

            var v3 = _db.Store_ItemWiseConsumption.Where(x => x.Active == false && x.ID == 3 && x.Merchandising_StyleFK == vmodel.Merchandising_StyleFK).Select(s => new VmStoreItemWiseConsumption
            {
                Merchandising_StyleFK = s.Merchandising_StyleFK,
                Common_RawItemFK = s.Common_RawItemFK,
                Name = s.Name,
                UnitName = s.UnitName,
                ConsumptionDz = Convert.ToDouble(s.ConsumptionDz),
                OrderConsumQty = Convert.ToDouble(s.OrderConsumQty),
                FinishQtyDz = Convert.ToDouble(s.FinishQtyDz),

                TotalRecMain = Convert.ToDouble(s.TotalRecMain),
                TotalRecCutting = Convert.ToDouble(s.TotalRecCutting),
                TotalRecSewing = Convert.ToDouble(s.TotalRecSewing),
                TotalRecIroning = Convert.ToDouble(s.TotalRecIroning),
                TotalRecPacking = Convert.ToDouble(s.TotalRecPacking),
                TotalRecFinish = Convert.ToDouble(s.TotalRecFinish),

                TotalOutMain = Convert.ToDouble(s.TotalOutMain),
                TotalOutCutting = Convert.ToDouble(s.TotalOutCutting),
                TotalOutSewing = Convert.ToDouble(s.TotalOutSewing),
                TotalOutIroning = Convert.ToDouble(s.TotalOutIroning),
                TotalOutPacking = Convert.ToDouble(s.TotalOutPacking),
                TotalOutFinish = Convert.ToDouble(s.TotalOutFinish),

                TotalRemMain = Convert.ToDouble(s.TotalRecMain) - Convert.ToDouble(s.TotalOutMain),
                TotalRemCutting = Convert.ToDouble(s.TotalRecCutting) - Convert.ToDouble(s.TotalOutCutting),
                TotalRemSewing = Convert.ToDouble(s.TotalRecSewing) - Convert.ToDouble(s.TotalOutSewing),
                TotalRemIroning = Convert.ToDouble(s.TotalRecIroning) - Convert.ToDouble(s.TotalOutIroning),
                TotalRemPacking = Convert.ToDouble(s.TotalRecPacking) - Convert.ToDouble(s.TotalOutPacking),
                TotalRemFinish = Convert.ToDouble(s.TotalRecFinish) - Convert.ToDouble(s.TotalOutFinish)

            }).ToList();

            vmmodel.Datalist = v;
            vmmodel.DatalistFinish = v2;
            vmmodel.DatalistFinishConsumtion = v3;
            vmmodel.ReportTypeName = vmodel.StyleName;
            return vmmodel;
        }
        public object ReportStoreInventoryDetailsConsumptionAllStyle()
        {
            var vmmodel = new VmStoreItemWiseConsumptionStyleAll
            {
                Datalist = new List<VmStoreItemWiseConsumptionStyleAll>()
            };
            var storeIn = new List<Store_StockIn>();
            var v = _db.Store_ItemWiseConsumptionAllStyle.Where(x => x.Active && x.ID == 2 ).Select(s => new VmStoreItemWiseConsumptionStyleAll
            {
                Merchandising_StyleFK = s.Merchandising_StyleFK,
                StyleName = s.StyleName, 
                Common_RawItemFK = s.Common_RawItemFK,
                Name = s.Name,
                UnitName = s.UnitName,
                ConsumptionDz = Convert.ToDouble(s.ConsumptionDz),
                OrderConsumQty = Convert.ToDouble(s.OrderConsumQty),
                FinishQtyDz = Convert.ToDouble(s.FinishQtyDz),

                TotalRecMain = Convert.ToDouble(s.TotalRecMain),
                TotalRecCutting = Convert.ToDouble(s.TotalRecCutting),
                TotalRecSewing = Convert.ToDouble(s.TotalRecSewing),
                TotalRecIroning = Convert.ToDouble(s.TotalRecIroning),
                TotalRecPacking = Convert.ToDouble(s.TotalRecPacking),
                TotalRecFinish = Convert.ToDouble(s.TotalRecFinish),

                TotalOutMain = Convert.ToDouble(s.TotalRecCutting) + Convert.ToDouble(s.TotalRecSewing) + Convert.ToDouble(s.TotalRecIroning) + Convert.ToDouble(s.TotalRecPacking),// + Convert.ToDouble(s.TotalOutFinish),
                TotalOutCutting = Convert.ToDouble(s.TotalOutCutting),
                TotalOutSewing = Convert.ToDouble(s.TotalOutSewing),
                TotalOutIroning = Convert.ToDouble(s.TotalOutIroning),
                TotalOutPacking = Convert.ToDouble(s.TotalOutPacking),
                TotalOutFinish = Convert.ToDouble(s.TotalRecCutting) + Convert.ToDouble(s.TotalRecSewing) + Convert.ToDouble(s.TotalRecIroning) + Convert.ToDouble(s.TotalRecPacking),// + Convert.ToDouble(s.TotalOutFinish),

                TotalRemMain = Convert.ToDouble(s.TotalRecMain) - (Convert.ToDouble(s.TotalRecCutting) + Convert.ToDouble(s.TotalRecSewing) + Convert.ToDouble(s.TotalRecIroning) + Convert.ToDouble(s.TotalRecPacking)), //Convert.ToDouble(s.TotalOutMain),
                TotalRemCutting = 0,
                TotalRemSewing = 0,
                TotalRemIroning = 0,
                TotalRemPacking = 0,
                TotalRemFinish = (Convert.ToDouble(s.TotalRecCutting) + Convert.ToDouble(s.TotalRecSewing) + Convert.ToDouble(s.TotalRecIroning) + Convert.ToDouble(s.TotalRecPacking) - Convert.ToDouble(s.TotalRecFinish))

            }).OrderBy(x => x.Merchandising_StyleFK).ToList();
            var v2 = _db.Store_ItemWiseConsumptionAllStyle.Where(x => x.Active == false && x.ID == 0 ).Select(s => new VmStoreItemWiseConsumptionStyleAll
            {
                Merchandising_StyleFK = s.Merchandising_StyleFK,
                Common_RawItemFK = s.Common_RawItemFK,
                StyleName = s.StyleName,
                Name = s.Name,
                UnitName = s.UnitName,
                ConsumptionDz = Convert.ToDouble(s.ConsumptionDz),
                OrderConsumQty = Convert.ToDouble(s.OrderConsumQty),
                FinishQtyDz = Convert.ToDouble(s.FinishQtyDz),

                TotalRecMain = Convert.ToDouble(s.TotalRecMain),
                TotalRecCutting = Convert.ToDouble(s.TotalRecCutting),
                TotalRecSewing = Convert.ToDouble(s.TotalRecSewing),
                TotalRecIroning = Convert.ToDouble(s.TotalRecIroning),
                TotalRecPacking = Convert.ToDouble(s.TotalRecPacking),
                TotalRecFinish = Convert.ToDouble(s.TotalRecFinish),

                TotalOutMain = Convert.ToDouble(s.TotalOutMain),
                TotalOutCutting = Convert.ToDouble(s.TotalOutCutting),
                TotalOutSewing = Convert.ToDouble(s.TotalOutSewing),
                TotalOutIroning = Convert.ToDouble(s.TotalOutIroning),
                TotalOutPacking = Convert.ToDouble(s.TotalOutPacking),
                TotalOutFinish = Convert.ToDouble(s.TotalOutFinish),

                TotalRemMain = Convert.ToDouble(s.TotalRecMain) - Convert.ToDouble(s.TotalOutMain),
                TotalRemCutting = Convert.ToDouble(s.TotalRecCutting) - Convert.ToDouble(s.TotalOutCutting),
                TotalRemSewing = Convert.ToDouble(s.TotalRecSewing) - Convert.ToDouble(s.TotalOutSewing),
                TotalRemIroning = Convert.ToDouble(s.TotalRecIroning) - Convert.ToDouble(s.TotalOutIroning),
                TotalRemPacking = Convert.ToDouble(s.TotalRecPacking) - Convert.ToDouble(s.TotalOutPacking),
                TotalRemFinish = Convert.ToDouble(s.TotalRecFinish) - Convert.ToDouble(s.TotalOutFinish)

            }).OrderBy(x => x.Merchandising_StyleFK).ToList();
            var v3 = _db.Store_ItemWiseConsumptionAllStyle.Where(x => x.Active == false && x.ID == 3 ).Select(s => new VmStoreItemWiseConsumptionStyleAll
            {
                Merchandising_StyleFK = s.Merchandising_StyleFK,
                Common_RawItemFK = s.Common_RawItemFK,
                StyleName = s.StyleName,
                Name = s.Name,
                UnitName = s.UnitName,
                ConsumptionDz = Convert.ToDouble(s.ConsumptionDz),
                OrderConsumQty = Convert.ToDouble(s.OrderConsumQty),
                FinishQtyDz = Convert.ToDouble(s.FinishQtyDz),

                TotalRecMain = Convert.ToDouble(s.TotalRecMain),
                TotalRecCutting = Convert.ToDouble(s.TotalRecCutting),
                TotalRecSewing = Convert.ToDouble(s.TotalRecSewing),
                TotalRecIroning = Convert.ToDouble(s.TotalRecIroning),
                TotalRecPacking = Convert.ToDouble(s.TotalRecPacking),
                TotalRecFinish = Convert.ToDouble(s.TotalRecFinish),

                TotalOutMain = Convert.ToDouble(s.TotalOutMain),
                TotalOutCutting = Convert.ToDouble(s.TotalOutCutting),
                TotalOutSewing = Convert.ToDouble(s.TotalOutSewing),
                TotalOutIroning = Convert.ToDouble(s.TotalOutIroning),
                TotalOutPacking = Convert.ToDouble(s.TotalOutPacking),
                TotalOutFinish = Convert.ToDouble(s.TotalOutFinish),

                TotalRemMain = Convert.ToDouble(s.TotalRecMain) - Convert.ToDouble(s.TotalOutMain),
                TotalRemCutting = Convert.ToDouble(s.TotalRecCutting) - Convert.ToDouble(s.TotalOutCutting),
                TotalRemSewing = Convert.ToDouble(s.TotalRecSewing) - Convert.ToDouble(s.TotalOutSewing),
                TotalRemIroning = Convert.ToDouble(s.TotalRecIroning) - Convert.ToDouble(s.TotalOutIroning),
                TotalRemPacking = Convert.ToDouble(s.TotalRecPacking) - Convert.ToDouble(s.TotalOutPacking),
                TotalRemFinish = Convert.ToDouble(s.TotalRecFinish) - Convert.ToDouble(s.TotalOutFinish)

            }).OrderBy(x => x.Merchandising_StyleFK).ToList();

            vmmodel.Datalist = v;

            vmmodel.DatalistFinish = v2;
            vmmodel.DatalistFinishConsumtion = v3;

            vmmodel.ReportTypeName = "";
            return vmmodel;
        }
        public object ReportStoreInventoryDetailsConsumptionStyleWise(VmStoreReport vmodel)
        {
            var vmmodel = new VmStoreItemWiseConsumptionStyleAll
            {
                Datalist = new List<VmStoreItemWiseConsumptionStyleAll>()
            };
            var storeIn = new List<Store_StockIn>();
            var v = _db.Store_ItemWiseConsumptionAllStyle.Where(x => x.Active && x.ID == 2 &&  x.Merchandising_StyleFK == vmodel.Merchandising_StyleFK).Select(s => new VmStoreItemWiseConsumptionStyleAll
            {
                Merchandising_StyleFK = s.Merchandising_StyleFK,
                StyleName = s.StyleName,
                Common_RawItemFK = s.Common_RawItemFK,
                Name = s.Name,
                UnitName = s.UnitName,
                ConsumptionDz = Convert.ToDouble(s.ConsumptionDz),
                OrderConsumQty = Convert.ToDouble(s.OrderConsumQty),
                FinishQtyDz = Convert.ToDouble(s.FinishQtyDz),

                TotalRecMain = Convert.ToDouble(s.TotalRecMain),
                TotalRecCutting = Convert.ToDouble(s.TotalRecCutting),
                TotalRecSewing = Convert.ToDouble(s.TotalRecSewing),
                TotalRecIroning = Convert.ToDouble(s.TotalRecIroning),
                TotalRecPacking = Convert.ToDouble(s.TotalRecPacking),
                TotalRecFinish = Convert.ToDouble(s.TotalRecFinish),

                TotalOutMain = Convert.ToDouble(s.TotalRecCutting) + Convert.ToDouble(s.TotalRecSewing) + Convert.ToDouble(s.TotalRecIroning) + Convert.ToDouble(s.TotalRecPacking),// + Convert.ToDouble(s.TotalOutFinish),
                TotalOutCutting = Convert.ToDouble(s.TotalOutCutting),
                TotalOutSewing = Convert.ToDouble(s.TotalOutSewing),
                TotalOutIroning = Convert.ToDouble(s.TotalOutIroning),
                TotalOutPacking = Convert.ToDouble(s.TotalOutPacking),
                TotalOutFinish = Convert.ToDouble(s.TotalRecCutting) + Convert.ToDouble(s.TotalRecSewing) + Convert.ToDouble(s.TotalRecIroning) + Convert.ToDouble(s.TotalRecPacking),// + Convert.ToDouble(s.TotalOutFinish),

                TotalRemMain = Convert.ToDouble(s.TotalRecMain) - (Convert.ToDouble(s.TotalRecCutting) + Convert.ToDouble(s.TotalRecSewing) + Convert.ToDouble(s.TotalRecIroning) + Convert.ToDouble(s.TotalRecPacking)), //Convert.ToDouble(s.TotalOutMain),
                TotalRemCutting = 0,
                TotalRemSewing = 0,
                TotalRemIroning = 0,
                TotalRemPacking = 0,
                TotalRemFinish = (Convert.ToDouble(s.TotalRecCutting) + Convert.ToDouble(s.TotalRecSewing) + Convert.ToDouble(s.TotalRecIroning) + Convert.ToDouble(s.TotalRecPacking) - Convert.ToDouble(s.TotalRecFinish))

            }).OrderBy(x => x.Merchandising_StyleFK).ToList();
            var v2 = _db.Store_ItemWiseConsumptionAllStyle.Where(x => x.Active == false && x.ID == 0 && x.Merchandising_StyleFK == vmodel.Merchandising_StyleFK).Select(s => new VmStoreItemWiseConsumptionStyleAll
            {
                Merchandising_StyleFK = s.Merchandising_StyleFK,
                Common_RawItemFK = s.Common_RawItemFK,
                StyleName = s.StyleName,
                Name = s.Name,
                UnitName = s.UnitName,
                ConsumptionDz = Convert.ToDouble(s.ConsumptionDz),
                OrderConsumQty = Convert.ToDouble(s.OrderConsumQty),
                FinishQtyDz = Convert.ToDouble(s.FinishQtyDz),

                TotalRecMain = Convert.ToDouble(s.TotalRecMain),
                TotalRecCutting = Convert.ToDouble(s.TotalRecCutting),
                TotalRecSewing = Convert.ToDouble(s.TotalRecSewing),
                TotalRecIroning = Convert.ToDouble(s.TotalRecIroning),
                TotalRecPacking = Convert.ToDouble(s.TotalRecPacking),
                TotalRecFinish = Convert.ToDouble(s.TotalRecFinish),

                TotalOutMain = Convert.ToDouble(s.TotalOutMain),
                TotalOutCutting = Convert.ToDouble(s.TotalOutCutting),
                TotalOutSewing = Convert.ToDouble(s.TotalOutSewing),
                TotalOutIroning = Convert.ToDouble(s.TotalOutIroning),
                TotalOutPacking = Convert.ToDouble(s.TotalOutPacking),
                TotalOutFinish = Convert.ToDouble(s.TotalOutFinish),

                TotalRemMain = Convert.ToDouble(s.TotalRecMain) - Convert.ToDouble(s.TotalOutMain),
                TotalRemCutting = Convert.ToDouble(s.TotalRecCutting) - Convert.ToDouble(s.TotalOutCutting),
                TotalRemSewing = Convert.ToDouble(s.TotalRecSewing) - Convert.ToDouble(s.TotalOutSewing),
                TotalRemIroning = Convert.ToDouble(s.TotalRecIroning) - Convert.ToDouble(s.TotalOutIroning),
                TotalRemPacking = Convert.ToDouble(s.TotalRecPacking) - Convert.ToDouble(s.TotalOutPacking),
                TotalRemFinish = Convert.ToDouble(s.TotalRecFinish) - Convert.ToDouble(s.TotalOutFinish)

            }).OrderBy(x => x.Merchandising_StyleFK).ToList();
            var v3 = _db.Store_ItemWiseConsumptionAllStyle.Where(x => x.Active == false && x.ID == 3 && x.Merchandising_StyleFK == vmodel.Merchandising_StyleFK).Select(s => new VmStoreItemWiseConsumptionStyleAll
            {
                Merchandising_StyleFK = s.Merchandising_StyleFK,
                Common_RawItemFK = s.Common_RawItemFK,
                StyleName = s.StyleName,
                Name = s.Name,
                UnitName = s.UnitName,
                ConsumptionDz = Convert.ToDouble(s.ConsumptionDz),
                OrderConsumQty = Convert.ToDouble(s.OrderConsumQty),
                FinishQtyDz = Convert.ToDouble(s.FinishQtyDz),

                TotalRecMain = Convert.ToDouble(s.TotalRecMain),
                TotalRecCutting = Convert.ToDouble(s.TotalRecCutting),
                TotalRecSewing = Convert.ToDouble(s.TotalRecSewing),
                TotalRecIroning = Convert.ToDouble(s.TotalRecIroning),
                TotalRecPacking = Convert.ToDouble(s.TotalRecPacking),
                TotalRecFinish = Convert.ToDouble(s.TotalRecFinish),

                TotalOutMain = Convert.ToDouble(s.TotalOutMain),
                TotalOutCutting = Convert.ToDouble(s.TotalOutCutting),
                TotalOutSewing = Convert.ToDouble(s.TotalOutSewing),
                TotalOutIroning = Convert.ToDouble(s.TotalOutIroning),
                TotalOutPacking = Convert.ToDouble(s.TotalOutPacking),
                TotalOutFinish = Convert.ToDouble(s.TotalOutFinish),

                TotalRemMain = Convert.ToDouble(s.TotalRecMain) - Convert.ToDouble(s.TotalOutMain),
                TotalRemCutting = Convert.ToDouble(s.TotalRecCutting) - Convert.ToDouble(s.TotalOutCutting),
                TotalRemSewing = Convert.ToDouble(s.TotalRecSewing) - Convert.ToDouble(s.TotalOutSewing),
                TotalRemIroning = Convert.ToDouble(s.TotalRecIroning) - Convert.ToDouble(s.TotalOutIroning),
                TotalRemPacking = Convert.ToDouble(s.TotalRecPacking) - Convert.ToDouble(s.TotalOutPacking),
                TotalRemFinish = Convert.ToDouble(s.TotalRecFinish) - Convert.ToDouble(s.TotalOutFinish)

            }).OrderBy(x => x.Merchandising_StyleFK).ToList();

            vmmodel.Datalist = v;

            vmmodel.DatalistFinish = v2;
            vmmodel.DatalistFinishConsumtion = v3;

            vmmodel.ReportTypeName = "";
            return vmmodel;
        }
        public object ReportStoreInventoryDetailsConsumptionItemWise()
        {
            var vmmodel = new VmStoreItemWiseConsumptionStyleAll
            {
                Datalist = new List<VmStoreItemWiseConsumptionStyleAll>(),
                DatalistFinish = new List<VmStoreItemWiseConsumptionStyleAll>(),
                DatalistFinishConsumtion = new List<VmStoreItemWiseConsumptionStyleAll>()
            };   
            
            var v = _db.Store_ItemWiseConsumptionAllStyle.Where(x => x.Active == false && x.ID == 5).Select(s => new VmStoreItemWiseConsumptionStyleAll
            {
                Merchandising_StyleFK = s.Merchandising_StyleFK,
                StyleName = s.StyleName,
                Common_RawItemFK = s.Common_RawItemFK,
                Name = s.Name,
                UnitName = s.UnitName,
                ConsumptionDz = Convert.ToDouble(s.ConsumptionDz),
                OrderConsumQty = Convert.ToDouble(s.OrderConsumQty),
                FinishQtyDz = Convert.ToDouble(s.FinishQtyDz),

                TotalRecMain = Convert.ToDouble(s.TotalRecMain),
                TotalRecCutting = Convert.ToDouble(s.TotalRecCutting),
                TotalRecSewing = Convert.ToDouble(s.TotalRecSewing),
                TotalRecIroning = Convert.ToDouble(s.TotalRecIroning),
                TotalRecPacking = Convert.ToDouble(s.TotalRecPacking),
                TotalRecFinish = Convert.ToDouble(s.TotalRecFinish),

                TotalOutMain = Convert.ToDouble(s.TotalRecCutting) + Convert.ToDouble(s.TotalRecSewing) + Convert.ToDouble(s.TotalRecIroning) + Convert.ToDouble(s.TotalRecPacking),// + Convert.ToDouble(s.TotalOutFinish),
                TotalOutCutting = Convert.ToDouble(s.TotalOutCutting),
                TotalOutSewing = Convert.ToDouble(s.TotalOutSewing),
                TotalOutIroning = Convert.ToDouble(s.TotalOutIroning),
                TotalOutPacking = Convert.ToDouble(s.TotalOutPacking),
                TotalOutFinish = Convert.ToDouble(s.TotalRecCutting) + Convert.ToDouble(s.TotalRecSewing) + Convert.ToDouble(s.TotalRecIroning) + Convert.ToDouble(s.TotalRecPacking),// + Convert.ToDouble(s.TotalOutFinish),

                TotalRemMain = Convert.ToDouble(s.TotalRecMain) - (Convert.ToDouble(s.TotalRecCutting) + Convert.ToDouble(s.TotalRecSewing) + Convert.ToDouble(s.TotalRecIroning) + Convert.ToDouble(s.TotalRecPacking)), //Convert.ToDouble(s.TotalOutMain),
                TotalRemCutting = 0,
                TotalRemSewing = 0,
                TotalRemIroning = 0,
                TotalRemPacking = 0,
                TotalRemFinish = (Convert.ToDouble(s.TotalRecCutting) + Convert.ToDouble(s.TotalRecSewing) + Convert.ToDouble(s.TotalRecIroning) + Convert.ToDouble(s.TotalRecPacking) - Convert.ToDouble(s.TotalRecFinish))

            }).OrderBy(x => x.Name).ToList();
            var v2 = _db.Store_ItemWiseConsumptionAllStyle.Where(x => x.Active == false && x.ID == 7).Select(s => new VmStoreItemWiseConsumptionStyleAll
            {
                Merchandising_StyleFK = s.Merchandising_StyleFK,
                Common_RawItemFK = s.Common_RawItemFK,
                StyleName = s.StyleName,
                Name = s.Name,
                UnitName = s.UnitName,
                ConsumptionDz = Convert.ToDouble(s.ConsumptionDz),
                OrderConsumQty = Convert.ToDouble(s.OrderConsumQty),
                FinishQtyDz = Convert.ToDouble(s.FinishQtyDz),

                TotalRecMain = Convert.ToDouble(s.TotalRecMain),
                TotalRecCutting = Convert.ToDouble(s.TotalRecCutting),
                TotalRecSewing = Convert.ToDouble(s.TotalRecSewing),
                TotalRecIroning = Convert.ToDouble(s.TotalRecIroning),
                TotalRecPacking = Convert.ToDouble(s.TotalRecPacking),
                TotalRecFinish = Convert.ToDouble(s.TotalRecFinish),

                TotalOutMain = Convert.ToDouble(s.TotalOutMain),
                TotalOutCutting = Convert.ToDouble(s.TotalOutCutting),
                TotalOutSewing = Convert.ToDouble(s.TotalOutSewing),
                TotalOutIroning = Convert.ToDouble(s.TotalOutIroning),
                TotalOutPacking = Convert.ToDouble(s.TotalOutPacking),
                TotalOutFinish = Convert.ToDouble(s.TotalOutFinish),

                TotalRemMain = Convert.ToDouble(s.TotalRecMain) - Convert.ToDouble(s.TotalOutMain),
                TotalRemCutting = Convert.ToDouble(s.TotalRecCutting) - Convert.ToDouble(s.TotalOutCutting),
                TotalRemSewing = Convert.ToDouble(s.TotalRecSewing) - Convert.ToDouble(s.TotalOutSewing),
                TotalRemIroning = Convert.ToDouble(s.TotalRecIroning) - Convert.ToDouble(s.TotalOutIroning),
                TotalRemPacking = Convert.ToDouble(s.TotalRecPacking) - Convert.ToDouble(s.TotalOutPacking),
                TotalRemFinish = Convert.ToDouble(s.TotalRecFinish) - Convert.ToDouble(s.TotalOutFinish)

            }).OrderBy(x => x.Merchandising_StyleFK).ToList();
            var v3 = _db.Store_ItemWiseConsumptionAllStyle.Where(x => x.Active == false && x.ID == 6 && x.OrderConsumQty > 0).Select(s => new VmStoreItemWiseConsumptionStyleAll
            {
                Merchandising_StyleFK = s.Merchandising_StyleFK,
                Common_RawItemFK = s.Common_RawItemFK,
                StyleName = s.StyleName,
                Name = s.Name,
                UnitName = s.UnitName,
                ConsumptionDz = Convert.ToDouble(s.ConsumptionDz),
                OrderConsumQty = Convert.ToDouble(s.OrderConsumQty),
                FinishQtyDz = Convert.ToDouble(s.FinishQtyDz),

                TotalRecMain = Convert.ToDouble(s.TotalRecMain),
                TotalRecCutting = Convert.ToDouble(s.TotalRecCutting),
                TotalRecSewing = Convert.ToDouble(s.TotalRecSewing),
                TotalRecIroning = Convert.ToDouble(s.TotalRecIroning),
                TotalRecPacking = Convert.ToDouble(s.TotalRecPacking),
                TotalRecFinish = Convert.ToDouble(s.TotalRecFinish),

                TotalOutMain = Convert.ToDouble(s.TotalOutMain),
                TotalOutCutting = Convert.ToDouble(s.TotalOutCutting),
                TotalOutSewing = Convert.ToDouble(s.TotalOutSewing),
                TotalOutIroning = Convert.ToDouble(s.TotalOutIroning),
                TotalOutPacking = Convert.ToDouble(s.TotalOutPacking),
                TotalOutFinish = Convert.ToDouble(s.TotalOutFinish),

                TotalRemMain = Convert.ToDouble(s.TotalRecMain) - Convert.ToDouble(s.TotalOutMain),
                TotalRemCutting = Convert.ToDouble(s.TotalRecCutting) - Convert.ToDouble(s.TotalOutCutting),
                TotalRemSewing = Convert.ToDouble(s.TotalRecSewing) - Convert.ToDouble(s.TotalOutSewing),
                TotalRemIroning = Convert.ToDouble(s.TotalRecIroning) - Convert.ToDouble(s.TotalOutIroning),
                TotalRemPacking = Convert.ToDouble(s.TotalRecPacking) - Convert.ToDouble(s.TotalOutPacking),
                TotalRemFinish = Convert.ToDouble(s.TotalRecFinish) - Convert.ToDouble(s.TotalOutFinish)

            }).OrderBy(x => x.Merchandising_StyleFK).ToList();

            vmmodel.Datalist = v;

            vmmodel.DatalistFinish = v2;
            vmmodel.DatalistFinishConsumtion = v3;

            vmmodel.ReportTypeName = "";
            return vmmodel;
        }
         
        public async Task<VMSRReportSlave> ProductionSRGet(VMSRReportSlave model)
        {
            VMSRReportSlave vM = new VMSRReportSlave();
            vM.DataListSlave = new List<VMSRReportSlave>();

            vM = await Task.Run(() => (from SR in _db.Store_Requisition.Include(x => x.Store_RequisitionSlave)
                                                     .Where(x => x.Active)
                                                    .Where(x => x.ID == (int)model.Store_RequisitionFK)

                                       //join C in _db.Common_Currency on PO.Common_CurrencyFK equals C.ID into C_Join
                                       //from C in C_Join.DefaultIfEmpty()
                                       join E in _db.User_User on SR.UserID equals E.ID into E_Join
                                       from E in E_Join.DefaultIfEmpty()
                                       //join S in _db.Common_Supplier on SR.fk equals S.ID into S_Join
                                       //from S in S_Join.DefaultIfEmpty()

                                       select new VMSRReportSlave
                                       {
                                           Store_RequisitionFK = SR.ID,
                                           CID = SR.CID,
                                          // OriginType = (ProcurementOriginTypeEnum)SR.OriginType,
                                           OrderDate = SR.CreationDate,
                                          // POPaymentMethod = (SupplierPaymentMethodEnum)SR.POPaymentMethod,
                                           POType = (POTypeEnum)SR.RequisitionType,
                                           CreationDate = SR.CreationDate,
                                           CreatedBy = SR.User,
                                           //ApprovedDate = SR.ApprovedDate.HasValue ? SR.ApprovedDate.Value : DateTime.MinValue,
                                           //RevisionNo = SR.RevisionNo,
                                           Description = SR.Description,
                                           Status = (POStatusEnum)SR.Status,
                                           VMEmployeeFK = E != null ? E.ID : 0,
                                           VMEmployeeName = E != null ? E.Name : "",

                                           // VMSupplierFK = SR.Common_SupplierFK,
                                           //VMSupplierName = S != null ? S.Name : "",
                                           //VMSupplierAddress = S != null ? S.Address : "",
                                           //VMSupplierCode = S != null ? S.Code : "",
                                           //VMSupplierCPName = S != null ? S.ContactPerson : "",
                                           //VMSupplierEmail = S != null ? S.Email : "",
                                           //VMSupplierPhone = S != null ? S.Phone : "",
                                           //VMCurrencyFK = SR.Common_CurrencyFK,
                                           //VMCurrencyName = C != null ? C.Name : "",
                                           //TermsAndCondition = SR.TermsAndCondition

                                       }).OrderByDescending(x => x.ID).FirstOrDefaultAsync());

            vM.DataListSlave = await Task.Run(() => (from SRS in _db.Store_RequisitionSlave.Where(x => x.Active && x.Store_RequisitionFK == vM.Store_RequisitionFK)
                                                     join RI in _db.Common_RawItem on SRS.Common_RawItemFK equals RI.ID into RI_Join
                                                     from RI in RI_Join.DefaultIfEmpty()
                                                     join U in _db.Common_Unit on RI.Common_UnitFK equals U.ID into U_Join
                                                     from U in U_Join.DefaultIfEmpty()
                                                     join MS in _db.Merchandising_Style on SRS.Merchandising_StyleFK equals MS.ID into MS_Join
                                                     from MS in MS_Join.DefaultIfEmpty()
                                                     join MBO in _db.Merchandising_BuyerOrder on MS.Merchandising_BuyerOrderFK equals MBO.ID into MBO_Join
                                                     from MBO in MBO_Join.DefaultIfEmpty()
                                                     join MSS in _db.Merchandising_StyleSlave on SRS.Merchandising_StyleSlaveFK equals MSS.ID into MSS_Join
                                                     from MSS in MSS_Join.DefaultIfEmpty()
                                                     join C in _db.Common_Color on MSS.Common_ColorFK equals C.ID into C_Join
                                                     from C in C_Join.DefaultIfEmpty()
                                                     select new VMSRReportSlave
                                                     {
                                                         BONoStyleName = MS != null && MBO != null ? MBO.BuyerPO + "/" + MS.StyleName : "",
                                                         StyleColorName = MSS != null && C != null ? C.Name : "",
                                                         StyleGSM = MSS != null ? (double)MSS.GSM : 0,
                                                         Store_RequisitionFK = SRS != null ? SRS.Store_RequisitionFK : 0,
                                                         // Store_RequisitionSlaveFK = SRS != null ? SRS.Store_RequisitionSlaveFK.Value : 0,
                                                         ID = SRS != null ? SRS.ID : 0,
                                                         PurchaseQuantity = SRS != null ? SRS.RequisitionQuantity : 0,
                                                         //PurchasingPrice = SRS != null ? SRS.PurchasingPrice : 0,
                                                         DescriptionSlave = SRS != null ? SRS.Description : "",
                                                         VMRawItemFK = RI != null ? RI.ID : 0,
                                                         VMRawItemName = RI != null ? RI.Name : "",
                                                         VMUnitName = U != null ? U.Name : "",
                                                         //AvailableQuantity = RI != null ? RI.Quantity : 0 //AvailableQuantity has to come from Store
                                                     }).OrderByDescending(x => x.ID).ToListAsync());

            Common_CompanySetup company = await _db.Common_CompanySetup.FirstOrDefaultAsync(x => x.Active);
            if (company != null && company.ID > 0)
            {
                vM.CompanyName = company.Name;
                vM.CompanyShortName = company.ShortName;
                vM.CompanyAddress = company.Address;
                vM.CompanyPhone = company.Phone;
                vM.CompanyEmail = company.Email;
            }
            return vM;
        }
        #endregion
    }
}