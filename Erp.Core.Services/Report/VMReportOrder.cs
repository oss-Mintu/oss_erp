﻿using Erp.Core.Entity.Common;
using Erp.Core.Services.Home;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
//using System.Text;

namespace Erp.Core.Services.Report
{
    public class VMReportOrder : BaseVM
    {
        [Display(Name ="Buyer :")]
        public int BuyerID { get; set; }
        public string BuyerName { get; set; }
        public SelectList BuyerList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        [Display(Name = "Supplier :")]
        public int SupplierID { get; set; }
        public string SupplierName { get; set; }
        public SelectList SupplierList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        [Display(Name = "Supplier PO :")]
        public string SupplierPOID { get; set; }
        public string SupplierPONO { get; set; }
        public SelectList SupplierPOList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        [Display(Name ="Buyer PO :")]
        public int BuyerPOID { get; set; }
        public string BuyerPOName { get; set; }
        public SelectList BuyerPOList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
        [Display(Name ="Style :")]
        public int StyleID { get; set; }
        public string StyleName { get; set; }
        public SelectList StyleList { get; set; } = new SelectList(new List<object>(), "Value", "Text");
    }


}