﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
//using System.Text;

namespace Erp.Core.Services.Report
{
    public class VMBPOReport : BaseVM
    {

        public DateTime CreationDate { get; set; } = DateTime.Now;
        public string CreationDateControlString { get { return this.CreationDate != DateTime.MinValue ? this.CreationDate.ToString("yyyy-MM-dd") : ""; } }
        public string CreationDateTableString { get { return this.CreationDate != DateTime.MinValue ? this.CreationDate.ToString("MMM dd, yyyy") : ""; } }
        public ProcurementOriginTypeEnum OriginType { get; set; }
        public string OriginTypeName { get { return BaseFunctionalities.GetEnumDescription(this.OriginType); } }


        public string CreatedBy { get; set; }
        [Required(ErrorMessage = "Order Date is Required")]
        public DateTime OrderDate { get; set; }
        public string OrderDateControlString { get { return this.OrderDate != DateTime.MinValue ? this.OrderDate.ToString("yyyy-MM-dd") : ""; } }
        public string OrderDateTableString { get { return this.OrderDate != DateTime.MinValue ? this.OrderDate.ToString("MMM dd, yyyy") : ""; } }

        public DateTime ApprovedDate { get; set; }
        public string ApprovedDateControlString { get { return this.ApprovedDate != DateTime.MinValue ? this.ApprovedDate.ToString("yyyy-MM-dd") : ""; } }
        public string ApprovedDateTableString { get { return this.ApprovedDate != DateTime.MinValue ? this.ApprovedDate.ToString("MMM dd, yyyy") : ""; } }

        public string VMCurrencyName { get; set; }
        [Required(ErrorMessage = "Currency is Required")]
        public int VMCurrencyFK { get; set; }
        [Required(ErrorMessage = "Terms & Conditions is Required")]
        public string TermsAndCondition { get; set; }




        public string ProtectedStyleID { get; set; }
        public string CID { get; set; }
        public DateTime FirstMove { get; set; }
        public string FirstMoveControlString { get { return this.FirstMove != DateTime.MinValue ? this.FirstMove.ToString("yyyy-MM-dd") : ""; } }
        public string FirstMoveTableString { get { return this.FirstMove != DateTime.MinValue ? this.FirstMove.ToString("MMM dd, yyyy") : ""; } }
        public DateTime UpdateDate { get; set; }
        public string UpdateDateControlString { get { return this.UpdateDate != DateTime.MinValue ? this.UpdateDate.ToString("yyyy-MM-dd") : ""; } }
        public string UpdateDateTableString { get { return this.UpdateDate != DateTime.MinValue ? this.UpdateDate.ToString("MMM dd, yyyy") : ""; } }
        public string ItemName { get; set; }
        public string Class { get; set; }
        public string Fabrication { get; set; }
        public string Reference { get; set; }
        public string Style { get; set; }
        public int PackQty { get; set; }
        public int PackPieceQty { get; set; }
        public int PieceQty { get; set; }
        
        public double DozonQty { get; set; }
        public string DozonQtyString { get { return DozonQty.ToString("0.00000"); } }
        public string BuyerPO { get; set; }
        public string Season { get; set; }
        public string BuyerName { get; set; }
        public string SupplierName { get; set; }
        public double PackPrice { get; set; }
        public string PackPriceString
        {
            get { return PackPrice.ToString("0.00000"); }
        }
        public decimal UnitPrice { get; set; }
        public string UnitPriceString
        {
            get { return UnitPrice.ToString("0.00000"); }
        }
        public double StylePrice { get; set; }
        public string StylePriceString { get { return StylePrice.ToString("0.00000"); } }
        public string SizeName { get; set; }
        public string VMBuyerCode { get; set; }
        public string VMBuyerAddress { get; set; }
        public string VMBuyerEmail { get; set; }
        public string VMBuyerPhone { get; set; }
        public string VMBuyerCPName { get; set; }
        public string CompanyName { get; set; }
        public string CompanyShortName { get; set; }
        public string CompanyAddress { get; set; }
        public string CompanyPhone { get; set; }
        public string CompanyEmail { get; set; }

        [Required(ErrorMessage = "Supplier is Required")]
        public int VMBuyerFK { get; set; }

        public string Description { get; set; }

        public int RevisionNo { get; set; }

        [Required(ErrorMessage = "Requestor is Required")]
        public int VMEmployeeFK { get; set; }
        public string VMEmployeeName { get; set; }
        
        public POStatusEnum Status { get; set; } = POStatusEnum.Draft;
        public string AuthorizationStatus { get { return BaseFunctionalities.GetEnumDescription(Status); } }
        public List<VMBPOReport> DataList { get; set; }
        
        public SupplierPaymentMethodEnum POPaymentMethod { get; set; } = SupplierPaymentMethodEnum.Cash;
        public string POPaymentMethodName { get { return BaseFunctionalities.GetEnumDescription(POPaymentMethod); } }
        
        public POTypeEnum POType { get; set; } = POTypeEnum.Procurement;
        public string POTypeName { get { return BaseFunctionalities.GetEnumDescription(POType); } }
    }

    public class VMBPOReportSlave : VMBPOReport
    {
        public bool IsForSave { get; set; }
        public double ProcuredQuantity { get; set; }
        public double SuggestedPrice { get; set; }
        [Required(ErrorMessage = "Purchase Price is Required")]
        public double PurchasingPrice { get; set; }
        [Required(ErrorMessage = "Raw Item is Required")]
        public int Procurement_PurchaseRequisitionSlaveFK { get; set; }
        public int Procurement_PurchaseOrderFK { get; set; }
        public int VMRawItemFK { get; set; }
        public string VMRawItemName { get; set; }
        public string VMRawCategoryName { get; set; }
        public string VMRawSubCategoryName { get; set; }
        public string DescriptionSlave { get; set; }

        public double RequisitionQuantity { get; set; }

        public double AvailableQuantity { get; set; }

        public double RestQuantity{ get { return RequisitionQuantity - (ProcuredQuantity); } }

        public double StyleQuantity{ get; set; }
        public double ReceivedQuantity { get; set; }
        public double UsedQuantity { get; set; }
        public double UsableQuantity { get { return this.ReceivedQuantity - this.UsedQuantity > 0 ? this.ReceivedQuantity - this.UsedQuantity : 0; } }
        

        public double TotalPrice { get { return StyleQuantity * PurchasingPrice; } }
        public string VMUnitName { get; set; }
        public string StyleColorName { get; set; }
        public double StyleGSM { get; set; }

        public string StyleColorGSM { get { return !string.IsNullOrEmpty(StyleColorName) && StyleGSM > 0 ? StyleColorName + "/" + StyleGSM.ToString() : !string.IsNullOrEmpty(StyleColorName) ? StyleGSM.ToString() : ""; } }

        public bool InspectionRequired { get; set; }
        public string Inspection { get { return this.InspectionRequired ? "Required" : "Not Required"; } }
        public int? VMMerchandising_StyleID { get; set; }
        public int? VMMerchandising_StyleSlaveID { get; set; }
        public int? VMMerchandising_BOFID { get; set; }
        public string BONoStyleName { get; set; }
        public List<VMBPOReportSlave> DataListSlave { get; set; }
    }
}