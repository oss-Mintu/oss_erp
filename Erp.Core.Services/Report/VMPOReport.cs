﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
//using System.Text;

namespace Erp.Core.Services.Report
{
    public class VMPOReport : BaseVM
    {
        public DateTime CreationDate { get; set; } = DateTime.Now;
        public string CreationDateControlString { get { return this.CreationDate != DateTime.MinValue ? this.CreationDate.ToString("yyyy-MM-dd") : ""; } }
        public string CreationDateTableString { get { return this.CreationDate != DateTime.MinValue ? this.CreationDate.ToString("MMM dd, yyyy") : ""; } }
        public ProcurementOriginTypeEnum OriginType { get; set; }
        public string OriginTypeName { get { return BaseFunctionalities.GetEnumDescription(this.OriginType); } }
        
        public string CreatedBy { get; set; }
        [Required(ErrorMessage = "Order Date is Required")]
        public DateTime OrderDate { get; set; }
        [Required(ErrorMessage = "Delivery Date is Required")]
        public DateTime DeliveryDate { get; set; }
        public string OrderDateControlString { get { return this.OrderDate != DateTime.MinValue ? this.OrderDate.ToString("yyyy-MM-dd") : ""; } }
        public string OrderDateTableString { get { return this.OrderDate != DateTime.MinValue ? this.OrderDate.ToString("MMM dd, yyyy") : ""; } }

        public DateTime ApprovedDate { get; set; }
        public string ApprovedDateControlString { get { return this.ApprovedDate != DateTime.MinValue ? this.ApprovedDate.ToString("yyyy-MM-dd") : ""; } }
        public string ApprovedDateTableString { get { return this.ApprovedDate != DateTime.MinValue ? this.ApprovedDate.ToString("MMM dd, yyyy") : ""; } }

        public DateTime ReceiveDate { get; set; }
        public string ReceiveDateControlString { get { return this.ReceiveDate != DateTime.MinValue ? this.ReceiveDate.ToString("yyyy-MM-dd") : ""; } }
        public string ReceiveDateTableString { get { return this.ReceiveDate != DateTime.MinValue ? this.ReceiveDate.ToString("MMM dd, yyyy") : ""; } }

        public string VMCurrencyName { get; set; }
        [Required(ErrorMessage = "Currency is Required")]
        public int VMCurrencyFK { get; set; }
        [Required(ErrorMessage = "Terms & Conditions is Required")]
        public string TermsAndCondition { get; set; }

        public string StyleName { get; set; }


        public string ProtectedPOID { get; set; }
        public string CID { get; set; }
        public string VMSupplierName { get; set; }
        public string VMSupplierCode { get; set; }
        public string VMSupplierAddress { get; set; }
        public string VMSupplierEmail { get; set; }
        public string VMSupplierPhone { get; set; }
        public string VMSupplierCPName { get; set; }


        public string CompanyName { get; set; }
        public string CompanyShortName { get; set; }
        public string CompanyAddress { get; set; }
        public string CompanyPhone { get; set; }
        public string CompanyEmail { get; set; }

        [Required(ErrorMessage = "Supplier is Required")]
        public int VMSupplierFK { get; set; }

        public string Description { get; set; }

        public int RevisionNo { get; set; }

        [Required(ErrorMessage = "Requestor is Required")]
        public int VMEmployeeFK { get; set; }
        public string VMEmployeeName { get; set; }
        
        public POStatusEnum Status { get; set; } = POStatusEnum.Draft;
        public string AuthorizationStatus { get { return BaseFunctionalities.GetEnumDescription(Status); } }
        public List<VMPOReport> DataList { get; set; }
        
        public SupplierPaymentMethodEnum POPaymentMethod { get; set; } = SupplierPaymentMethodEnum.Cash;
        public string POPaymentMethodName { get { return BaseFunctionalities.GetEnumDescription(POPaymentMethod); } }
        
        public POTypeEnum POType { get; set; } = POTypeEnum.Procurement;
        public string POTypeName { get { return BaseFunctionalities.GetEnumDescription(POType); } }

        public int? OrderID { get; set; }

        public int? StyleID { get; set; }

        public int? SupplierID { get; set; }

        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }


    }

    public class VMPOReportSlave : VMPOReport
    {
        public int Procurement_PurchaseOrderFK { get; set; }
        public int Procurement_PurchaseRequisitionSlaveFK { get; set; }

        public bool IsForSave { get; set; }
        private double _PurchaseQuantity = 0;
        public double ProcuredQuantity { get; set; }
        public double SuggestedPrice { get; set; }
        [Required(ErrorMessage = "Purchase Price is Required")]
        public double PurchasingPrice { get; set; }
        [Required(ErrorMessage = "Raw Item is Required")]
        
        public int VMRawItemFK { get; set; }
        public string VMRawItemName { get; set; }
        public string DescriptionSlave { get; set; }
        public double RequisitionQuantity { get; set; }
        public double AvailableQuantity { get; set; }
        public double RestQuantity
        {
            get
            {
                return RequisitionQuantity - (ProcuredQuantity);
            }
        }

        public double PurchaseQuantity
        {
            get
            {
                if (_PurchaseQuantity <= 0)
                {
                    if (RequisitionQuantity > (ProcuredQuantity + AvailableQuantity))
                    { _PurchaseQuantity = RequisitionQuantity - (ProcuredQuantity + AvailableQuantity); }
                }
                return _PurchaseQuantity;
            }

            set { _PurchaseQuantity = value; }
        }
        public double ReceivedQuantity { get; set; }
        public double ReturnQuantity { get; set; }
        public double TotalQuantity { get { return this.ReceivedQuantity - this.ReturnQuantity > 0 ? this.ReceivedQuantity - this.ReturnQuantity : 0; } }
        public double BalanceQuantity { get { return this.PurchaseQuantity - this.TotalQuantity > 0 ? this.PurchaseQuantity - this.TotalQuantity : 0; } }
        public decimal InvoiceQty { get; set; }
        public decimal InvoicePrice { get; set; }
        public decimal TotalInvoiceValue { get; set; }
        public decimal TotalInvoiceQty { get; set; }

        public decimal PORate { get; set; }
        public decimal POValue { get; set; }

        public double TotalPrice { get { return PurchaseQuantity * PurchasingPrice; } }
        public string VMUnitName { get; set; }
        public string StyleColorName { get; set; }
        public double StyleGSM { get; set; }

        public string StyleColorGSM { get { return !string.IsNullOrEmpty(StyleColorName) && StyleGSM > 0 ? StyleColorName + "/" + StyleGSM.ToString() : !string.IsNullOrEmpty(StyleColorName) ? StyleGSM.ToString() : ""; } }

        public bool InspectionRequired { get; set; }
        public string Inspection { get { return this.InspectionRequired ? "Required" : "Not Required"; } }
        public int? VMMerchandising_StyleID { get; set; }
        public int? VMMerchandising_StyleSlaveID { get; set; }
        public int? VMMerchandising_BOFID { get; set; }
        public string BONoStyleName { get; set; }
        public string ChallanNo { get; set; }
        public decimal InvoiceRate { get; set; }
        public List<VMPOReportSlave> DataListSlave { get; set; }
    }

    public class VMSRReport : BaseVM
    {

        public DateTime CreationDate { get; set; } = DateTime.Now;
        public string CreationDateControlString { get { return this.CreationDate != DateTime.MinValue ? this.CreationDate.ToString("yyyy-MM-dd") : ""; } }
        public string CreationDateTableString { get { return this.CreationDate != DateTime.MinValue ? this.CreationDate.ToString("MMM dd, yyyy") : ""; } }
        public ProcurementOriginTypeEnum OriginType { get; set; }
        public string OriginTypeName { get { return BaseFunctionalities.GetEnumDescription(this.OriginType); } }


        public string CreatedBy { get; set; }
        [Required(ErrorMessage = "Order Date is Required")]
        public DateTime OrderDate { get; set; }
        [Required(ErrorMessage = "Delivery Date is Required")]
        public DateTime DeliveryDate { get; set; }
        public string OrderDateControlString { get { return this.OrderDate != DateTime.MinValue ? this.OrderDate.ToString("yyyy-MM-dd") : ""; } }
        public string OrderDateTableString { get { return this.OrderDate != DateTime.MinValue ? this.OrderDate.ToString("MMM dd, yyyy") : ""; } }

        public DateTime ApprovedDate { get; set; }
        public string ApprovedDateControlString { get { return this.ApprovedDate != DateTime.MinValue ? this.ApprovedDate.ToString("yyyy-MM-dd") : ""; } }
        public string ApprovedDateTableString { get { return this.ApprovedDate != DateTime.MinValue ? this.ApprovedDate.ToString("MMM dd, yyyy") : ""; } }

        public string VMCurrencyName { get; set; }
        [Required(ErrorMessage = "Currency is Required")]
        public int VMCurrencyFK { get; set; }
        [Required(ErrorMessage = "Terms & Conditions is Required")]
        public string TermsAndCondition { get; set; }




        public string ProtectedPOID { get; set; }
        public string CID { get; set; }
        public string VMSupplierName { get; set; }
        public string VMSupplierCode { get; set; }
        public string VMSupplierAddress { get; set; }
        public string VMSupplierEmail { get; set; }
        public string VMSupplierPhone { get; set; }
        public string VMSupplierCPName { get; set; }
        public string CompanyName { get; set; }
        public string CompanyShortName { get; set; }
        public string CompanyAddress { get; set; }
        public string CompanyPhone { get; set; }
        public string CompanyEmail { get; set; }

        [Required(ErrorMessage = "Supplier is Required")]
        public int VMSupplierFK { get; set; }

        public string Description { get; set; }

        public int RevisionNo { get; set; }

        [Required(ErrorMessage = "Requestor is Required")]
        public int VMEmployeeFK { get; set; }
        public string VMEmployeeName { get; set; }

        public POStatusEnum Status { get; set; } = POStatusEnum.Draft;
        public string AuthorizationStatus { get { return BaseFunctionalities.GetEnumDescription(Status); } }
        public List<VMPOReport> DataList { get; set; }

        public SupplierPaymentMethodEnum POPaymentMethod { get; set; } = SupplierPaymentMethodEnum.Cash;
        public string POPaymentMethodName { get { return BaseFunctionalities.GetEnumDescription(POPaymentMethod); } }

        public POTypeEnum POType { get; set; } = POTypeEnum.Procurement;
        public string POTypeName { get { return BaseFunctionalities.GetEnumDescription(POType); } }



    }
    public class VMSRReportSlave : VMSRReport 
    {
        public bool IsForSave { get; set; }
        private double _PurchaseQuantity = 0;

        public double ProcuredQuantity { get; set; }

        public double SuggestedPrice { get; set; }
        [Required(ErrorMessage = "Purchase Price is Required")]
        public double PurchasingPrice { get; set; }
        [Required(ErrorMessage = "Raw Item is Required")]
        public int Store_RequisitionSlaveFK { get; set; } 
        public int Store_RequisitionFK { get; set; }
        public int VMRawItemFK { get; set; }
        public string VMRawItemName { get; set; }

        public string DescriptionSlave { get; set; }

        public double RequisitionQuantity { get; set; }

        public double AvailableQuantity { get; set; }

        public double RestQuantity
        {
            get
            {
                return RequisitionQuantity - (ProcuredQuantity);
            }
        }


        public double PurchaseQuantity
        {
            get
            {
                if (_PurchaseQuantity <= 0)
                {
                    if (RequisitionQuantity > (ProcuredQuantity + AvailableQuantity))
                    { _PurchaseQuantity = RequisitionQuantity - (ProcuredQuantity + AvailableQuantity); }
                }
                return _PurchaseQuantity;
            }

            set { _PurchaseQuantity = value; }
        }
        public double ReceivedQuantity { get; set; }
        public double ReturnQuantity { get; set; }
        public double TotalQuantity { get { return this.ReceivedQuantity - this.ReturnQuantity > 0 ? this.ReceivedQuantity - this.ReturnQuantity : 0; } }
        public double BalanceQuantity { get { return this.PurchaseQuantity - this.TotalQuantity > 0 ? this.PurchaseQuantity - this.TotalQuantity : 0; } }

        public double TotalPrice { get { return PurchaseQuantity * PurchasingPrice; } }
        public string VMUnitName { get; set; }
        public string StyleColorName { get; set; }
        public double StyleGSM { get; set; }

        public string StyleColorGSM { get { return !string.IsNullOrEmpty(StyleColorName) && StyleGSM > 0 ? StyleColorName + "/" + StyleGSM.ToString() : !string.IsNullOrEmpty(StyleColorName) ? StyleGSM.ToString() : ""; } }

        public bool InspectionRequired { get; set; }
        public string Inspection { get { return this.InspectionRequired ? "Required" : "Not Required"; } }
        public int? VMMerchandising_StyleID { get; set; }
        public int? VMMerchandising_StyleSlaveID { get; set; }
        public int? VMMerchandising_BOFID { get; set; }
        public string BONoStyleName { get; set; }
        public List<VMSRReportSlave> DataListSlave { get; set; } 
    }
}