﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Erp.Core.Services.Payroll
{
    public class NightAndHolidayBill
    {
        [DisplayName("Degignation")]
        public int DegignationId { get; set; }
        [DisplayName("Night Bill")]
        public decimal NightBill { get; set; }
    }
}
