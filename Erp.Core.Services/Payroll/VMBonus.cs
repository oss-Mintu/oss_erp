﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Erp.Core.Services.Payroll
{
    public class VMBonusSettings : BaseVM
    {
        [DisplayName("From Month")]
        public int VMFromMonth { get; set; }
        [DisplayName("To Month")]
        public int VMToMonth { get; set; }
        [DisplayName("Bonus Rate (%)")]
        public decimal VMBonusRate { get; set; }

        [DisplayName("Bonus Apply On")]
        public int VMIsBasicOrGrossId { get; set; }
        public SelectList VMIsBasicOrGrossList => new SelectList(BaseFunctionalities.GetEnumList<EnumSalaryType>(), "Value", "Text");
        public string GrossOrBasic { get; set; }

        public string Remarks { get; set; }
        public IEnumerable<VMBonusSettings> DataList { get; set; }
    }

    public class VMBonusMaster : BaseVM
    {
        [DisplayName("Bonus Title")]
        public string VMBonusTitle { get; set; }
        [DisplayName("Payment Date")]
        public DateTime VMPaymentDate { get; set; }
        [DisplayName("Remarks")]
        public string VMRemarks { get; set; }
        public IEnumerable<VMBonusMaster> DataList { get; set; }
    }

    public class VMBonusDetails : BaseVM
    {
        public string EmployeeID { get; set; }
        public string EmployeeName { get; set; }
        public string Designation { get; set; }
        public string Department { get; set; }
        public string Section { get; set; }
        public DateTime JoiningDate { get; set; }
        public decimal GrossSalary { get; set; }
        public decimal BasicSalary { get; set; }
        public string Tenure { get; set; }
        public decimal BonusRate { get; set; }
        public decimal BonusAmount { get; set; }
        public decimal StampCharge { get; set; }
        public decimal TotalPayable { get; set; }
        public IEnumerable<VMBonusDetails> DataList { get; set; }
    }

}
