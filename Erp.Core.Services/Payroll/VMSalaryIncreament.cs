﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Erp.Core.Services.Payroll
{
    public class VMSalaryIncreament : BaseVM
    {
        [DisplayName("Business Unit")]
        public int VMBusinessUnitId { get; set; }
        [DisplayName("Business Unit")]
        public string VMBusinessUnitName { get; set; }
        public SelectList VMBusinessUnitList { get; set; } = new SelectList(new List<object>());

        [DisplayName("Unit")]
        public int VMUnitId { get; set; }
        public SelectList VMUnitList { get; set; } = new SelectList(new List<object>());

        [DisplayName("Department")]
        public int VMDepartmentId { get; set; }
        public SelectList VMDepartmentList { get; set; } = new SelectList(new List<object>());

        [DisplayName("Section")]
        public int VMSectionId { get; set; }
        public SelectList VMSectionList { get; set; } = new SelectList(new List<object>());

        [DisplayName("From Date")]
        public DateTime FromDate { get; set; }

        [DisplayName("To Date")]
        public DateTime ToDate { get; set; }
        public int EODMasterId { get; set; }

        public int EmployeeId { get; set; }
        public string EmployeeIdentity { get; set; }
        public string EmployeeName { get; set; }
        public string Designation { get; set; }
        public DateTime JoiningDate { get; set; }
        public decimal PreviousBasic { get; set; }
        public decimal PreviousGross { get; set; }
        public decimal IncreamentPercent { get; set; }
        public decimal IncreamentOfBasicAmount { get; set; }
        public decimal IncreamentedBasic { get; set; }
        public decimal IncreamentedGross { get; set; }
        public decimal IncreamentedHouse { get; set; }
        public decimal TotalIncrementedAmount { get; set; }
        public DateTime EffectedDate { get; set; }
        public int Status { get; set; }
        public IEnumerable<VMSalaryIncreament> DataList { get; set; }
    }
}
