﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Erp.Core.Services.Payroll
{
    public class VMEmployeeSalary : BaseVM
    {
        [DisplayName("Business Unit")]
        public int VMBusinessUnitId { get; set; }
        [DisplayName("Business Unit")]
        public string VMBusinessUnitName { get; set; }
        public SelectList VMBusinessUnitList { get; set; } = new SelectList(new List<object>());

        [DisplayName("Unit")]
        public int VMUnitId { get; set; }
        public SelectList VMUnitList { get; set; } = new SelectList(new List<object>());

        [DisplayName("Department")]
        public int VMDepartmentId { get; set; }
        public SelectList VMDepartmentList { get; set; } = new SelectList(new List<object>());

        [DisplayName("Section")]
        public int VMSectionId { get; set; }
        public SelectList VMSectionList { get; set; } = new SelectList(new List<object>());

        [DisplayName("Employee")]
        public int EmployeeId { get; set; }
        public SelectList VMEmployeeList { get; set; } = new SelectList(new List<object>());

        [DisplayName("Salary Allocation")]
        public int AllocationId { get; set; }
        public int[] AllocationIds { get; set; }
        public SelectList AllocationList => new SelectList(BaseFunctionalities.GetEnumList<EnumSalaryBenefitType>(), "Value", "Text");

        [DisplayName("Gross Salary")]
        public decimal GrossSalary { get; set; }

        [DisplayName("Holiday & Night Bill Eligibility")]
        public int BillEligibility { get; set; }
        public int[] BillEligibilityIds { get; set; }
        public SelectList BillEligibilityList => new SelectList(BaseFunctionalities.GetEnumList<EnumHolidayAndNightBillType>(), "Value", "Text");

        [DisplayName("Remarks")]
        public string Remarks { get; set; }

        public int EmployeeIdForEdit { get; set; } //Redundant Due to some issue with EmployeeId=0
        public int[] AllocationIdForEdit { get; set; } //Redundant Due to some issue with AllocationId=0
        public decimal GrossSalaryForEdit { get; set; } //Redundant Due to some issue with GrossSalary=0
        public string RemarksForEdit { get; set; }
        public int[] BillEligibilityForEdit { get; set; }
        [DisplayName("Night Bill")]
        public decimal NightBillForEdit { get; set; }
        [DisplayName("Holiday Bill")]
        public decimal HolidayBillForEdit { get; set; }
        public int EODMasterId { get; set; }
        public List<VMSalaryDetails> EmployeeSalaryList { get; set; }
    }

    public class VMSalaryDetails
    {
        public int EmployeeId { get; set; }
        public int EODMasterId { get; set; }
        public string EmployeeIdentity { get; set; }
        public string EmployeeName { get; set; }
        public string Designation { get; set; }
        public int SalaryAllowance { get; set; }
        public decimal BasicAmount { get; set; }
        public decimal HouseRent { get; set; }
        public decimal OthersAllowance { get; set; }
        public decimal TotalSalary { get; set; }
        public decimal OTRate { get; set; }
        public decimal AttendacneBonus { get; set; }
        public int BillEligibility { get; set; }
        public decimal NightBill { get; set; }
        public decimal HolidayBill { get; set; }
        public string Remarks { get; set; }
    }

    public class VMSalaryStructure : BaseVM
    {
        public int StructureType { get; set; }
        [DisplayName("Medical Allowance")]
        public decimal MedicalAllowance { get; set; }
        [DisplayName("Food Allowance")]
        public decimal FoodlAllowance { get; set; }
        [DisplayName("Transport Allowance")]
        public decimal TransportAllowance { get; set; }
        [DisplayName("Stamp Charge")]
        public decimal StampCharge { get; set; }
        [DisplayName("Basic Salary (%)")]
        public decimal BasicInPercent { get; set; }
        [DisplayName("Monthly Working Days")]
        public int MonthlyWorkingDays { get; set; }
        [DisplayName("Daily Working Hours")]
        public int DailyWorkingHour { get; set; }
        [DisplayName("OT Rate (%)")]
        public decimal OvertimeRate { get; set; }
        [DisplayName("House Rent (%)")]
        public decimal HouseRentInPercent { get; set; }
        public bool Status { get; set; } //check salary structure already used or not

        public VMSalaryStructure DataListForWhite { get; set; }
        public VMSalaryStructure DataListForBlue { get; set; }
    }

    public class VMCalculatedSalaryStructure
    {
        public int ID { get; set; }
        public decimal GrossSalary { get; set; }

        public decimal MedicalAllowance { get; set; }
        public decimal FoodlAllowance { get; set; }
        public decimal TransportAllowance { get; set; }
        public decimal StampCharge { get; set; }
        public decimal OtherAllowance => MedicalAllowance + FoodlAllowance + TransportAllowance;

        public decimal BasicInPercent { get; set; }
        public decimal BasicInRatio => 1 + (BasicInPercent / 100);
        public decimal BasicSalary => (GrossSalary - OtherAllowance) / BasicInRatio;

        public decimal HouseRentInPercent { get; set; }
        public decimal HouseRentInRatio => HouseRentInPercent / 100;
        public decimal HouseRent => BasicSalary * HouseRentInRatio;

        public int MonthlyWorkingDays { get; set; }
        public int DailyWorkingHour { get; set; }
        public int MonthlyWorkingHour => MonthlyWorkingDays * DailyWorkingHour;

        public decimal OvertimeRate { get; set; }
        public decimal OvertimeRateInRatio => OvertimeRate / 100;
        public decimal OverTimeAmount => (BasicSalary / MonthlyWorkingHour) * OvertimeRateInRatio;
    }
}
