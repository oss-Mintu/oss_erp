﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Erp.Core.Entity.Payroll;
using Microsoft.Extensions.DependencyInjection;
using System.Data;
using System.Data.SqlClient;

namespace Erp.Core.Services.Payroll
{
    /// <summary>
    /// This class has implement all business logic for Payroll module
    /// </summary>
    public class PayrollService : BaseService
    {
        public PayrollService(IErpDbContext db) => _db = db;
        /// <summary>
        /// This region hold all Employee salary mechanism
        /// </summary>
        #region Employee Salary
        ///<summary>
        /// Create & Edit salary structure (settings)
        /// </summary>
        public async Task<VMSalaryStructure> SalaryStructureGet()
        {
            var white = await Task.Run(() => _db.Payroll_SalaryStructure.Where(x => x.Active == true && x.StructerType == 1).Select(x => new VMSalaryStructure
            {
                ID = x.ID,
                MedicalAllowance = x.MedicalAllowance,
                FoodlAllowance = x.FoodlAllowance,
                TransportAllowance = x.TransportAllowance,
                StampCharge = x.StampCharge,
                BasicInPercent = x.BasicInPercent,
                HouseRentInPercent = x.HouseRentInPercent,
                MonthlyWorkingDays = x.MonthlyWorkingDays,
                DailyWorkingHour = x.DailyWorkingHour,
                OvertimeRate = x.OvertimeRate,
                Status = (_db.Payroll_EODRecordMaster.Any(y => y.SalaryStructureFk == x.ID && y.Active == true))
            }).FirstOrDefault());

            var blue = await Task.Run(() => _db.Payroll_SalaryStructure.Where(x => x.Active == true && x.StructerType == 2).Select(x => new VMSalaryStructure
            {
                ID = x.ID,
                MedicalAllowance = x.MedicalAllowance,
                FoodlAllowance = x.FoodlAllowance,
                TransportAllowance = x.TransportAllowance,
                StampCharge = x.StampCharge,
                BasicInPercent = x.BasicInPercent,
                HouseRentInPercent = x.HouseRentInPercent,
                MonthlyWorkingDays = x.MonthlyWorkingDays,
                DailyWorkingHour = x.DailyWorkingHour,
                OvertimeRate = x.OvertimeRate,
                Status = (_db.Payroll_EODRecordMaster.Any(y => y.SalaryStructureFk == x.ID && y.Active == true))
            }).FirstOrDefault());
            var data = new VMSalaryStructure
            {
                DataListForWhite = white,
                DataListForBlue = blue
            };
            return data;

        }
        public async Task<int> SalaryStructureAdd(VMSalaryStructure vmmodel)
        {
            int result = -1;
            //Delete (Inactive) previous payroll structure while creating new one.
            var previousStructure = _db.Payroll_SalaryStructure.Where(x => x.Active == true && x.StructerType == vmmodel.StructureType).SingleOrDefault();
            if (previousStructure != null)
            {
                previousStructure.Active = false;
            }

            //Add New Salary Structure
            var model = new Payroll_SalaryStructure
            {
                StructerType = vmmodel.StructureType,
                MedicalAllowance = vmmodel.MedicalAllowance,
                FoodlAllowance = vmmodel.FoodlAllowance,
                TransportAllowance = vmmodel.TransportAllowance,
                StampCharge = vmmodel.StampCharge,
                BasicInPercent = vmmodel.BasicInPercent,
                HouseRentInPercent = vmmodel.HouseRentInPercent,
                MonthlyWorkingDays = vmmodel.MonthlyWorkingDays,
                DailyWorkingHour = vmmodel.DailyWorkingHour,
                OvertimeRate = vmmodel.OvertimeRate,
                User = vmmodel.User,
                Active = true

            };
            _db.Payroll_SalaryStructure.Add(model);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = 1;
            }
            return result;
        }
        public async Task<int> SalaryStructureEdit(VMSalaryStructure vmmodel)
        {
            int result = -1;
            var structure = _db.Payroll_SalaryStructure.Where(x => x.ID == vmmodel.ID && x.Active == true).SingleOrDefault();
            if (structure != null)
            {
                structure.MedicalAllowance = vmmodel.MedicalAllowance;
                structure.FoodlAllowance = vmmodel.FoodlAllowance;
                structure.TransportAllowance = vmmodel.TransportAllowance;
                structure.StampCharge = vmmodel.StampCharge;
                structure.BasicInPercent = vmmodel.BasicInPercent;
                structure.MonthlyWorkingDays = vmmodel.MonthlyWorkingDays;
                structure.DailyWorkingHour = vmmodel.DailyWorkingHour;
                structure.OvertimeRate = vmmodel.OvertimeRate;
                structure.HouseRentInPercent = vmmodel.HouseRentInPercent;
            }
            if (await _db.SaveChangesAsync() > 0)
            {
                result = 1;
            }
            return result;
        }
        /// <summary>
        /// Initialize Employee salary view model
        /// </summary>
        /// <remarks>
        /// Load all required dropdown & set required value on first load
        /// </remarks>
        /// <param name="model"></param>
        /// <returns>
        /// VMEmployeeSalary view model
        /// </returns>
        public async Task<VMEmployeeSalary> EmployeeSalaryGet(VMEmployeeSalary model)
        {
            var VM = new VMEmployeeSalary
            {
                VMBusinessUnitId = model.VMBusinessUnitId,
                VMUnitId = model.VMUnitId,
                VMDepartmentId = model.VMDepartmentId,
                VMSectionId = model.VMSectionId,
                VMBusinessUnitList = await BusinessUnitDropDownListAsync(),
                VMUnitList = await UnitDropDownListAsync(),
                VMDepartmentList = await DepartmentDropDownListAsync(),
                VMSectionList = await SectionDropDownListAsync()
            };
            return VM;
        }
        /// <summary>
        /// Filter individually or altogether, Employee list along with there already added salary 
        /// </summary>
        /// <param name="bunit"></param> Business unit
        /// <param name="unit"></param> Unit
        /// <param name="dept"></param> Department
        /// <param name="section"></param> Section
        /// <returns>
        /// Employee list for dropdown whose salary not created yet
        /// Employee salary list which already added
        /// </returns>
        public VMEmployeeSalary FilterEmployeeSalary(int bunit, int unit, int dept, int section)
        {
            // Filtered employee list
            var data = (from E in _db.HRMS_Employee
                        join S in _db.HRMS_Section on E.HRMS_SectionFK equals S.ID into S_join
                        from S in S_join.DefaultIfEmpty()
                        join DP in _db.User_Department on E.User_DepartmentFK equals DP.ID into DP_join
                        from DP in DP_join.DefaultIfEmpty()
                        join U in _db.HRMS_Unit on S.HRMS_UnitFK equals U.ID into U_Join
                        from U in U_Join.DefaultIfEmpty()
                        join BU in _db.HRMS_BusinessUnit on U.HRMS_BusinessUnitFK equals BU.ID into BU_join
                        from BU in BU_join.DefaultIfEmpty()
                        where E.Active == true && E.PresentStatus == 1
                        && (BU.ID == bunit || bunit == 0)
                        && (DP.ID == dept || dept == 0)
                        && (U.ID == unit || unit == 0)
                        && (S.ID == section || section == 0)
                        select new
                        {
                            E.ID,
                            E.Name,
                            E.EmployeeIdentity
                        }).ToList();

            // Get filtered employee salary filter (pivot)
            var createdsalarylist = (from t1 in _db.Payroll_EODRecordMaster
                                     join t2 in _db.Payroll_EODRecord on t1.ID equals t2.Payroll_EODRecordMasterFk
                                     join t3 in _db.HRMS_Employee on t1.EmployeeFk equals t3.ID
                                     join t4 in _db.HRMS_Designation on t3.HRMS_DesignationFK equals t4.ID

                                     join S in _db.HRMS_Section on t3.HRMS_SectionFK equals S.ID into S_join
                                     from S in S_join.DefaultIfEmpty()
                                     join DP in _db.User_Department on t3.User_DepartmentFK equals DP.ID into DP_join
                                     from DP in DP_join.DefaultIfEmpty()
                                     join U in _db.HRMS_Unit on S.HRMS_UnitFK equals U.ID into U_Join
                                     from U in U_Join.DefaultIfEmpty()
                                     join BU in _db.HRMS_BusinessUnit on U.HRMS_BusinessUnitFK equals BU.ID into BU_join
                                     from BU in BU_join.DefaultIfEmpty()

                                     where t1.Active == true && t2.Active == true && t3.Active == true && t3.Active == true
                                     && t3.PresentStatus == 1 && (BU.ID == bunit) &&
                                     (unit == 0 || U.ID == unit) &&
                                     (dept == 0 || DP.ID == dept) &&
                                     (section == 0 || S.ID == section)
                                     select new
                                     {
                                         EODMasterId = t1.ID,
                                         EmployeeId = t3.ID,
                                         EmployeeName = t3.Name,
                                         t3.EmployeeIdentity,
                                         t1.GrossSalary,
                                         t1.SalaryAllowance,
                                         t1.BillEligibily,
                                         EODReference = t2.EODReferenceFk,
                                         t2.Amount,
                                         Designation = t4.Name,
                                         t1.Remarks
                                     }).GroupBy(x => new { x.EODMasterId, x.EmployeeId, x.Designation, x.EmployeeIdentity, x.EmployeeName, x.GrossSalary, x.Remarks, x.SalaryAllowance, x.BillEligibily }).Select(g => new VMSalaryDetails
                                     {
                                         EODMasterId = g.Key.EODMasterId,
                                         EmployeeId = g.Key.EmployeeId,
                                         EmployeeIdentity = g.Key.EmployeeIdentity,
                                         EmployeeName = g.Key.EmployeeName,
                                         Designation = g.Key.Designation,
                                         SalaryAllowance = g.Key.SalaryAllowance,
                                         BillEligibility = g.Key.BillEligibily,
                                         BasicAmount = g.Where(x => x.EODReference == 1).Sum(x => x.Amount),
                                         HouseRent = g.Where(x => x.EODReference == 22).Sum(x => x.Amount),
                                         OthersAllowance = g.Where(x => x.EODReference == 2 || x.EODReference == 3 || x.EODReference == 4).Sum(x => x.Amount),
                                         TotalSalary = g.Key.GrossSalary,
                                         AttendacneBonus = g.Where(x => x.EODReference == 21).Sum(x => x.Amount),
                                         OTRate = g.Where(x => x.EODReference == 19).Sum(x => x.Amount),
                                         HolidayBill = g.Where(x => x.EODReference == 23).Sum(x => x.Amount),
                                         NightBill = g.Where(x => x.EODReference == 24).Sum(x => x.Amount),
                                         Remarks = g.Key.Remarks
                                     }).OrderByDescending(x => x.EODMasterId).ToList();
            // Bind dropdown data & list to view model
            VMEmployeeSalary vm = new VMEmployeeSalary
            {
                VMEmployeeList = new SelectList(data.Where(x => createdsalarylist.All(y => y.EmployeeId != x.ID)).Select(z => new { Value = z.ID, Text = z.EmployeeIdentity + " :- " + z.Name }).ToList(), "Value", "Text"), // ignore employee list which salary already created
                EmployeeSalaryList = createdsalarylist
            };
            return vm;

        }
        /// <summary>
        /// Create Employee salary with breakdown from gross salary according to salary structure (white or blue)
        /// </summary>
        /// <remarks>
        /// Steps:
        /// 1. Get employee Employement type (worker or employee), Joining date, Night Bill & Attendance Bonus <see cref="GetEmployeeTypeAndAttendanceBonus()"/>
        /// 2. Get salary structure of this particular employee type <see cref="GetSalaryStructureForParticularEmployee()"/>
        /// 3. Save Master Model
        /// 4. Get EOD Reference i,e. salary breakdown head (database design instructed from Rezwan vai)
        /// 5. Get Breakdown salary amount of each head <see cref="GetEodAmountByReference()"/>
        /// 6. Save Child Model
        /// *** Each employee salary will have multiple rows ***
        /// </remarks>
        /// <param name="vmmodel">VMEmployeeSalary view model</param>
        /// <returns>
        /// Return save changes status
        /// </returns>
        public async Task<int> EmployeeSalaryAdd(VMEmployeeSalary vmmodel)
        {
            int result = -1;

            var employee = GetEmployeeTypeAndAttendanceBonus(vmmodel.EmployeeId);
            var salarydetails = GetSalaryStructureForParticularEmployee(employee.Item1, vmmodel.GrossSalary);

            // Not applicable for old employee who have increament after current date in current year; for old employee It will DateTime.Now()
            // By default increament date will be 1 year after the joining date
            var increamentdate = new DateTime(DateTime.Now.AddYears(1).Year, employee.Item3.Month, employee.Item3.Day);

            var mastermodel = new Payroll_EODRecordMaster
            {
                GrossSalary = vmmodel.GrossSalary,
                EmployeeFk = vmmodel.EmployeeId,
                Remarks = vmmodel.Remarks,
                SalaryAllowance = vmmodel.AllocationId, // 1= OT & Attendance Bonus, 2= Only OT, 3= Only Attendance Bonus & 4= No Bonus Allocations
                BillEligibily = vmmodel.BillEligibility,// 1=Only Holiday Bill, 2= Only Night Bill, 3= Holiday Bill & Night Bill , 4= No Bill Allocation
                NextIncreamentDate = increamentdate,
                AffectedDate = employee.Item3, //affected date initially joining date
                IsIncreamentedSalary = false, // Track new salary or increamnted salary (due to same table for increamnet as well as new one)
                SalaryStructureFk = salarydetails.ID, //salary structure in which it calculated from 
                Active = true,
                Time = DateTime.Now,
                User = vmmodel.User
            };
            _db.Payroll_EODRecordMaster.Add(mastermodel);


            var references = _db.Payroll_EODReference.Where(x => x.Active == true && x.IsMandatory == true).ToList(); // IsMandatory currently non functional, In case of new added salary head might be needed
            foreach (var reference in references)
            {
                var model = new Payroll_EODRecord
                {
                    Payroll_EODRecordMasterFk = mastermodel.ID,
                    EODReferenceFk = reference.ID,
                    Active = true,
                    Time = DateTime.Now,
                    User = vmmodel.User
                };
                model.Amount = GetEodAmountByReference(reference.ID, vmmodel.AllocationId, employee.Item1, employee.Item2, vmmodel.BillEligibility, employee.Item4, salarydetails);
                _db.Payroll_EODRecord.Add(model);
            }
            if (await _db.SaveChangesAsync() > 0)
            {
                result = 1;
            }
            return result;
        }
        /// <summary>
        /// Edit salary in case any mistake on data entry (Edit only, not for increament or adjustment)
        /// </summary>
        /// <remarks>
        /// 1. Get the salary which will be edited
        /// 2. Overwrite master model
        /// 3. Get employee Employement type(worker or employee), Joining date, Night Bill & Attendance Bonus in case of it's been changed at the time it's edit<see cref= "GetEmployeeTypeAndAttendanceBonus()" />
        /// 4. Get salary structure of this particular employee type in case of it's been changed at the time it's edit <see cref="GetSalaryStructureForParticularEmployee()"/>
        /// 5. Re-calculate salary head amount
        /// 6. Overwrite child model with those amounts
        /// </remarks>
        /// <param name="vmmodel">VMEmployeeSalary view model</param>
        /// <returns>
        /// Return save changes status
        /// </returns>
        public async Task<int> EmployeeSalaryEdit(VMEmployeeSalary vmmodel, bool active = true)
        {
            int result = -1;
            var master = _db.Payroll_EODRecordMaster.Single(x => x.ID == vmmodel.EODMasterId);
            master.GrossSalary = vmmodel.GrossSalary;
            master.SalaryAllowance = vmmodel.AllocationId;
            master.BillEligibily = vmmodel.BillEligibility;
            master.Remarks = vmmodel.RemarksForEdit;
            master.UpdateDate = DateTime.Now;
            master.User = vmmodel.User;

            var employee = GetEmployeeTypeAndAttendanceBonus(master.EmployeeFk);
            var salarydetails = GetSalaryStructureForParticularEmployee(employee.Item1, vmmodel.GrossSalary);

            _db.Payroll_EODRecord.Where(x => x.Payroll_EODRecordMasterFk == vmmodel.EODMasterId && x.Active == active).ToList()
             .ForEach(x =>
             {
                 x.Amount = GetEodAmountByReference(x.EODReferenceFk, vmmodel.AllocationId, employee.Item1, employee.Item2, vmmodel.BillEligibility, vmmodel.NightBillForEdit, salarydetails, true, vmmodel.HolidayBillForEdit);
             });
            if (await _db.SaveChangesAsync() > 0)
            {
                result = 1;
            }
            return result;
        }
        /// <summary>
        /// This method return particular employee's Type (worker or Staff), Joining date, Night bill & Attendance Bonus
        /// </summary>
        /// <param name="empid">Employee Id</param>
        /// <returns>
        /// Returns Employee Type (int)
        /// </returns>
        private (int, decimal, DateTime, decimal) GetEmployeeTypeAndAttendanceBonus(int empid)
        {
            var employeedetails = _db.HRMS_Employee.Where(x => x.Active == true && x.ID == empid)
                .Join(_db.HRMS_Designation.Where(x => x.Active == true), x => x.HRMS_DesignationFK,
                y => y.ID, (x, y) =>
                   new
                   {
                       EmployeeType = x.StaffType, // 1= staff, 2= worker
                       AttendacneBonus = y.AttendanceBonus, // From designation
                       y.NightBill, // From designation
                       JoinigDate = x.JoiningDate
                   }).SingleOrDefault();
            return (employeedetails.EmployeeType, employeedetails.AttendacneBonus, employeedetails.JoinigDate, employeedetails.NightBill);
        }
        /// <summary>
        /// This method return salary breakdown amount based on some condition
        /// <see cref="VMCalculatedSalaryStructure"/> for details calculation
        /// </summary>
        /// <remarks>
        /// ******* HARD CODED REFERENCE (HEAD) ID WHICH ALREADY SAVED ON DATABASE (i,e. SALARY HEADS ARE FIXED) ******
        /// ******* HAVE TO GENERATE SCRIPT AND SAVE IT ON DATABASE TO REMAIN THESE ID AS UNCHANGED ******
        /// </remarks>
        /// <param name="refid"></param> Breakdown head reference
        /// <param name="allocationId"></param> Which allocation benifits (OT & attendance bonus) will apply
        /// <param name="employeetype"></param> Staff or worker type
        /// <param name="attendacnebonus"></param> Attendance Bonus according to particular employee designation 
        /// <param name="billeligibility"></param> Which Bill (Holiday & Night bill) will apply
        /// <param name="nightbill"></param> Night Bill according to particular employee designation 
        /// <param name="salarystructure"></param> Salary Structure under this breakdown will calculated
        /// <param name="isedit">(Optional)</param> To track new or edit salary which reflects calculation
        /// <param name="holidaybill">(Optional)</param> To keep edited holiday bill while editing
        /// <returns> Returns calculated amount as decimal</returns>
        private decimal GetEodAmountByReference(int refid, int allocationId, int employeetype, decimal attendacnebonus, int billeligibility, decimal nightbill, VMCalculatedSalaryStructure salarystructure, bool isedit = false, decimal holidaybill = 0)
        {

            decimal amount = 0;
            switch (refid)
            {
                case 1:
                    amount = Math.Round(salarystructure.BasicSalary, 0, MidpointRounding.AwayFromZero); //Basic amount
                    break;
                case 2:
                    amount = salarystructure.FoodlAllowance;
                    break;
                case 3:
                    amount = salarystructure.MedicalAllowance;
                    break;
                case 4:
                    amount = salarystructure.TransportAllowance;
                    break;
                case 8:
                    amount = salarystructure.StampCharge;
                    break;
                case 19:
                    // 1 = OT & Attendance Bonus, 2 = Only OT, 3 = Only Attendance Bonus & 4 = No Bonus Allocations
                    amount = allocationId == 1 || allocationId == 2 ? Math.Round(salarystructure.OverTimeAmount, 2) : 0;//Overtime rate 
                    break;
                case 21:
                    // Attendance bonus will apply only for worker based on his bonus eligibility
                    amount = employeetype == 2 && allocationId == 1 || allocationId == 3 ? attendacnebonus : 0;//Attendance Bonus According To The Designation
                    break;
                case 22:
                    amount = Math.Round(salarystructure.HouseRent, 0, MidpointRounding.AwayFromZero); //house rent
                    break;
                case 23 when isedit == false: // In case of new salary (from calculation)
                    // 1=Only Holiday Bill, 2= Only Night Bill, 3= Holiday Bill & Night Bill , 4= No Bill Allocation
                    amount = billeligibility == 1 || billeligibility == 3 ? Math.Round((salarystructure.BasicSalary / 30), 0, MidpointRounding.AwayFromZero) : 0; //Holiday Bill
                    break;
                case 23 when isedit == true: // In case of Edited salary to keep edited holiday bill (from UI)
                    amount = billeligibility == 1 || billeligibility == 3 ? holidaybill : 0; //Holiday Bill
                    break;
                case 24:
                    amount = billeligibility == 2 || billeligibility == 3 ? nightbill : 0;//Night bill According To The Designation
                    break;
                default:
                    break;
            }
            return amount;
        }
        /// <summary>
        /// This method return calculated salary according to it's structure type
        /// <see cref="VMCalculatedSalaryStructure"/> for details breakdown calcuation
        /// </summary>
        /// <param name="employeetype"></param> which structure will it appy according to it's type (1= Staff , 2= Worker)
        /// <param name="grosssalary"></param> Gross salary which will breakdown
        /// <returns>Returns calculated salary as VMCalculatedSalaryStructure view model</returns>
        private VMCalculatedSalaryStructure GetSalaryStructureForParticularEmployee(int employeetype, decimal grosssalary)
        {
            var salarystructure = _db.Payroll_SalaryStructure.Where(x => x.Active == true && x.StructerType == employeetype).Select(x => new VMCalculatedSalaryStructure
            {
                ID = x.ID,
                GrossSalary = grosssalary,
                MedicalAllowance = x.MedicalAllowance,
                FoodlAllowance = x.FoodlAllowance,
                TransportAllowance = x.TransportAllowance,
                StampCharge = x.StampCharge,
                BasicInPercent = x.BasicInPercent,
                DailyWorkingHour = x.DailyWorkingHour,
                MonthlyWorkingDays = x.MonthlyWorkingDays,
                HouseRentInPercent = x.HouseRentInPercent,
                OvertimeRate = x.OvertimeRate
            }).FirstOrDefault();
            return salarystructure;
        }
        #endregion

        /// <summary>
        /// This region hold all mechanism for salary increament
        /// </summary>
        #region Increament Salary
        ///<summary>
        /// Iniatialize page for Increament salary
        ///</summary>
        public async Task<VMSalaryIncreament> IncreamentSalaryGet(VMSalaryIncreament model)
        {
            DateTime now = DateTime.Now;
            var startDate = new DateTime(now.Year, now.Month, 1); // initially start date is first day of current motnth
            var endDate = startDate.AddMonths(1).AddDays(-1); // initially start date is last day of current motnth

            var VM = new VMSalaryIncreament
            {
                VMBusinessUnitId = model.VMBusinessUnitId,
                VMUnitId = model.VMUnitId,
                VMDepartmentId = model.VMDepartmentId,
                VMSectionId = model.VMSectionId,
                VMBusinessUnitList = await BusinessUnitDropDownListAsync(),
                VMUnitList = await UnitDropDownListAsync(),
                VMDepartmentList = await DepartmentDropDownListAsync(),
                VMSectionList = await SectionDropDownListAsync(),
                FromDate = startDate,
                ToDate = endDate
            };
            return VM;
        }
        /// <summary>
        /// Filter employee by <paramref name="bunit"/> Business Unit,<paramref name="unit"/> Unit, <paramref name="section"/> Section & <paramref name="dept"/> department 
        /// Individually or altogther to have the list, who will have increament within a date range 
        /// </summary>
        /// <param name="bunit"></param> Business Unit
        /// <param name="unit"></param> Unit
        /// <param name="dept"></param> Department
        /// <param name="section"></param> Section
        /// <param name="fromdate"></param> From Date
        /// <param name="todate"></param> To Date
        /// <returns>
        /// Returns list of pending increament salary list as VMSalaryIncreament view model
        /// </returns>
        public VMSalaryIncreament FilterIncreamentEmployeeSalary(int bunit, int unit, int dept, int section, DateTime fromdate, DateTime todate)
        {
            var vmodel = new VMSalaryIncreament();
            var list = new List<VMSalaryIncreament>();
            // get pending employee who will have increament within the given date range
            var data = (from t1 in _db.Payroll_EODRecordMaster
                        join t2 in _db.Payroll_EODRecord on t1.ID equals t2.Payroll_EODRecordMasterFk
                        join t3 in _db.HRMS_Employee on t1.EmployeeFk equals t3.ID
                        join t4 in _db.HRMS_Designation on t3.HRMS_DesignationFK equals t4.ID

                        join S in _db.HRMS_Section on t3.HRMS_SectionFK equals S.ID into S_join
                        from S in S_join.DefaultIfEmpty()
                        join DP in _db.User_Department on t3.User_DepartmentFK equals DP.ID into DP_join
                        from DP in DP_join.DefaultIfEmpty()
                        join U in _db.HRMS_Unit on S.HRMS_UnitFK equals U.ID into U_Join
                        from U in U_Join.DefaultIfEmpty()
                        join BU in _db.HRMS_BusinessUnit on U.HRMS_BusinessUnitFK equals BU.ID into BU_join
                        from BU in BU_join.DefaultIfEmpty()

                        where t1.NextIncreamentDate >= fromdate && t1.NextIncreamentDate <= todate && // NextIncreamentDate reflects the day of suggested increament
                        !_db.Payroll_EODRecordMaster.Where(x => x.IsIncreamentedSalary == true && (x.Status == 1 || x.Status == 2)).Select(x => x.PreviousSalaryRecord).Contains(t1.ID) && // to avoid already approved increamnt from HR
                        t1.Active == true && t2.Active == true && t3.Active == true && t4.Active == true
                        && t3.PresentStatus == 1 && (BU.ID == bunit) &&
                        (unit == 0 || U.ID == unit) &&
                        (dept == 0 || DP.ID == dept) &&
                        (section == 0 || S.ID == section)
                        select new
                        {
                            EODMasterId = t1.ID,
                            EmployeeName = t3.Name,
                            t3.EmployeeIdentity,
                            t3.StaffType,
                            t1.GrossSalary,
                            EODReference = t2.EODReferenceFk,
                            t2.Amount,
                            Designation = t4.Name,
                            EffectDate = t1.NextIncreamentDate,
                            t3.JoiningDate,
                            EmployeeId = t3.ID
                        }).GroupBy(x => new { x.EODMasterId, x.Designation, x.EmployeeIdentity, x.EmployeeName, x.GrossSalary, x.EffectDate, x.JoiningDate, x.EmployeeId, x.StaffType }).Select(g => new
                        {
                            g.Key.EODMasterId,
                            g.Key.EmployeeIdentity,
                            g.Key.EmployeeName,
                            g.Key.Designation,
                            g.Key.StaffType,
                            PreviousBasic = g.Where(x => x.EODReference == 1).Sum(x => x.Amount),
                            HouseRent = g.Where(x => x.EODReference == 22).Sum(x => x.Amount),
                            PreviousGross = g.Key.GrossSalary,
                            g.Key.EffectDate,
                            g.Key.JoiningDate,
                            g.Key.EmployeeId
                        }).ToList();

            if (data.Any())
            {
                // Load suggestion and compare current & increament salary structure
                data.ForEach(item =>
                {
                    var structure = _db.Payroll_SalaryStructure.Where(x => x.StructerType == item.StaffType && x.Active == true).FirstOrDefault();
                    var vm = new VMSalaryIncreament
                    {
                        EODMasterId = item.EODMasterId,
                        EmployeeIdentity = item.EmployeeIdentity,
                        EmployeeName = item.EmployeeName,
                        Designation = item.Designation,
                        PreviousBasic = item.PreviousBasic,
                        PreviousGross = item.PreviousGross,
                        IncreamentPercent = 5, // By default 5% increament (as suggestion)
                        EffectedDate = new DateTime(item.EffectDate.Year, item.EffectDate.Month, 1), // new increament salary will affected from, by deafault from 1st day of efected month (as suggestion)
                        JoiningDate = item.JoiningDate
                    };
                    vm.IncreamentOfBasicAmount = Math.Round(vm.PreviousBasic * vm.IncreamentPercent / 100); // Increamented amount (Suggestion) by 5%
                    vm.IncreamentedBasic = vm.PreviousBasic + vm.IncreamentOfBasicAmount; // Increamented Basic amount (Suggestion)
                    vm.IncreamentedHouse = Math.Round(vm.IncreamentedBasic * (structure.HouseRentInPercent / 100), 0, MidpointRounding.AwayFromZero); // Increamented house rent (Suggestion)
                    vm.IncreamentedGross = vm.IncreamentedBasic + vm.IncreamentedHouse + structure.MedicalAllowance + structure.FoodlAllowance + structure.TransportAllowance; // Increamented Gross amount (Suggestion)
                    vm.TotalIncrementedAmount = vm.IncreamentOfBasicAmount + (vm.IncreamentedHouse - item.HouseRent); // Total Increament amount (Suggestion)
                    list.Add(vm);
                    // ***** By consulting from Emrul vai ***** 
                });
            }
            vmodel.DataList = list;
            return vmodel;
        }
        /// <summary>
        /// This method is responsible to approve new increamented salary with breakdown
        /// </summary>
        /// <param name="eodmasterid"></param> Master Id from which will it increament
        /// <param name="grosssalary"></param> New increamented Gross salary
        /// <param name="user"></param> 
        /// <param name="effecteddate"></param> New increamented Salary  effected from
        /// <see cref="GetEmployeeTypeAndAttendanceBonus()"/> Employee's Type (worker or Staff), Joining date, Night bill & Attendance Bonus
        /// <seealso cref="GetEodAmountByReference()"/> To Get salary breakdown amount
        /// <seealso cref=""/> cref="GetSalaryStructureForParticularEmployee()"/> Salary breakdown amounts of particular Employee and type
        /// <returns>
        /// Returns previous salary Master model ID to track in case of promotion and increament in promotional history
        /// <see cref="EmployeePromotionAddAsync()"/>
        /// </returns>
        public async Task<int> ApproveIncreamentedSalary(int eodmasterid, decimal grosssalary, string user, DateTime effecteddate)
        {
            int result = -1;

            var master = _db.Payroll_EODRecordMaster.Where(x => x.ID == eodmasterid && x.Active == true).Single(); // Current master model from it will increament
            var details = _db.Payroll_EODRecord.Where(x => x.Payroll_EODRecordMasterFk == eodmasterid && x.Active == true); // Current child model from it will increament
            var references = _db.Payroll_EODReference.Where(x => x.Active == true && x.IsMandatory == true).ToList(); // Get all salary breakdown reference (heads)
            var employee = GetEmployeeTypeAndAttendanceBonus(master.EmployeeFk); // Get current employee's Type (worker or Staff), Joining date, Night bill & Attendance Bonus
            var salarydetails = GetSalaryStructureForParticularEmployee(employee.Item1, grosssalary); // Get salary breakdown amount according to employee type and corresponding structure

            var holidaybill = details.Single(x => x.EODReferenceFk == 23).Amount;
            var nightbill = details.Single(x => x.EODReferenceFk == 24).Amount;
            var nextincreamentdate = new DateTime(effecteddate.Year + 1, effecteddate.Month, employee.Item3.Day); // Next increamnt will be 1 year after it's affected month,year and joining day

            // Create new master
            var mastermodel = new Payroll_EODRecordMaster
            {
                GrossSalary = grosssalary,
                EmployeeFk = master.EmployeeFk,
                Remarks = master.Remarks,
                SalaryAllowance = master.SalaryAllowance,
                NextIncreamentDate = nextincreamentdate,
                BillEligibily = master.BillEligibily,
                SalaryStructureFk = salarydetails.ID, //salary structure 
                // to avoid multiple salary structure always affect date will be 1st day of month
                // other wise two structure salary need to apply if not on first day of mmonth (ex, middle of month have to apply previous+new sturcture)
                // consulting with Emrul vai and Romo HR policy no double structure will allowed for monthly salary
                AffectedDate = new DateTime(effecteddate.Year, effecteddate.Month, 1), // Hard coded first day
                IsIncreamentedSalary = true, // to track increamnt salary list
                PreviousSalaryRecord = eodmasterid, // to track previous salary
                Active = false, // Inactive upon HR approved initially for next level approval 
                Time = DateTime.Now,
                User = user,
                Status = 1 // 1= Approved from HR
            };
            _db.Payroll_EODRecordMaster.Add(mastermodel);

            foreach (var reference in references)
            {
                var model = new Payroll_EODRecord
                {
                    Payroll_EODRecordMasterFk = mastermodel.ID,
                    EODReferenceFk = reference.ID,
                    Active = false,
                    Time = DateTime.Now,
                    User = user
                };
                // get salary breakdown amount same as edit salary mechanism
                model.Amount = GetEodAmountByReference(reference.ID, master.SalaryAllowance, employee.Item1, employee.Item2, master.BillEligibily, nightbill, salarydetails, true, holidaybill);
                _db.Payroll_EODRecord.Add(model);
            }
            if (await _db.SaveChangesAsync() > 0)
            {
                result = mastermodel.ID;
            }

            return result;
        }
        /// <summary>
        /// Load Approved increament from HR for management approval
        /// </summary>
        /// <returns>
        /// Returns VMSalaryIncreament as view model consist list of approved increament by HR 
        /// </returns>
        public async Task<VMSalaryIncreament> ManagementApprovalOfIncreamentSalaryGet()
        {
            // load approved increamnet salary from HR
            var data = await Task.Run(() => (from t1 in _db.Payroll_EODRecordMaster
                                             join t2 in _db.Payroll_EODRecord on t1.ID equals t2.Payroll_EODRecordMasterFk
                                             join t3 in _db.HRMS_Employee on t1.EmployeeFk equals t3.ID
                                             join t4 in _db.HRMS_Designation on t3.HRMS_DesignationFK equals t4.ID
                                             join t5 in _db.Payroll_EODRecordMaster on t1.PreviousSalaryRecord equals t5.ID
                                             join t6 in _db.Payroll_EODRecord on t5.ID equals t6.Payroll_EODRecordMasterFk
                                             where
                                             t1.Active == false &&
                                             t2.Active == false &&
                                             t3.Active == true &&
                                             t4.Active == true &&
                                             t3.PresentStatus == 1 &&
                                             t1.IsIncreamentedSalary == true &&
                                             t1.Status == 1
                                             select new
                                             {
                                                 EODMasterId = t1.ID,
                                                 EmployeeName = t3.Name,
                                                 t3.EmployeeIdentity,
                                                 NewGross = t1.GrossSalary,
                                                 NewEODReference = t2.EODReferenceFk,
                                                 NewAmount = t2.Amount,
                                                 PrevGross = t5.GrossSalary,
                                                 PrevEODReference = t6.EODReferenceFk,
                                                 PrevAmount = t6.Amount,
                                                 Designation = t4.Name,
                                                 t3.JoiningDate,
                                                 EmployeeId = t3.ID,
                                                 EffectDate = t1.AffectedDate
                                             }).GroupBy(x => new { x.EODMasterId, x.Designation, x.EmployeeIdentity, x.EmployeeName, x.NewGross, x.PrevGross, x.JoiningDate, x.EmployeeId, x.EffectDate }).Select(g => new VMSalaryIncreament
                                             {
                                                 EODMasterId = g.Key.EODMasterId,
                                                 EmployeeIdentity = g.Key.EmployeeIdentity,
                                                 EmployeeName = g.Key.EmployeeName,
                                                 Designation = g.Key.Designation,
                                                 PreviousBasic = g.Where(x => x.PrevEODReference == 1).Max(x => x.PrevAmount),
                                                 PreviousGross = g.Key.PrevGross,
                                                 IncreamentedBasic = g.Where(x => x.NewEODReference == 1).Max(x => x.NewAmount),
                                                 IncreamentedGross = g.Key.NewGross,
                                                 JoiningDate = g.Key.JoiningDate,
                                                 EmployeeId = g.Key.EmployeeId,
                                                 EffectedDate = g.Key.EffectDate,
                                                 TotalIncrementedAmount = g.Key.NewGross - g.Key.PrevGross
                                             }).ToList());

            // use aggrigate, to avoid multiple approval (increamnet request from HR to management) of same employee
            // Only get latest request for a particular employee from HR (Safety purpose)
            var latestincrementreq = data.GroupBy(x => x.EmployeeId,
                                (k, g) => g.Aggregate((a, x) => (x.EODMasterId > a.EODMasterId) ? x : a)).ToList();

            var vmodel = new VMSalaryIncreament
            {
                DataList = latestincrementreq
            };
            return vmodel;
        }
        /// <summary>
        /// Approve or Reject Management status
        /// </summary>
        /// <remarks>
        /// ******* Salary will affected only upon the call by stored procedure in Schedular on last day of each month <see cref="ERP\erp\Erp.Infrastructure\Database\StoredProcedures\Payroll\EffectIncreamentedSalaryToPayroll.sql"/> ***** 
        /// ******* Schedular will be execute last day of the each month and active all salary those affected date on that month ****
        /// ******* This is all due to sudden change from Shah Alam vai (Could have better way :( ) ******
        /// </remarks>
        /// <param name="id"></param>
        /// <param name="status"></param>
        public async Task<int> ManagementApprovalProcess(int id, int status)
        {
            int result = -1;
            var master = _db.Payroll_EODRecordMaster.Where(x => x.ID == id).Single();
            master.Status = status; //1=Pending Approval from HR (Default) 2= Management Approve, 3= Reject
            if (await _db.SaveChangesAsync() > 0)
            {
                result = 1;
            }
            return result;
        }
        #endregion

        ///<summary>
        /// This region hold all mechanism for Payroll generate, Approval process & Regenerate/ Recalculation 
        /// </summary>
        #region Payroll Process
        /// <summary>
        /// Initialize and load payroll data (Master Data)
        /// </summary>
        /// <param name="status"></param> Null= Load all data, 1= Pending (Default) & 2= Processed, 3= Closed 
        /// <param name="approvalstatus"></param>  Null= Load all data, 1= Approved (Default), 2= Hold, 3= Reprocess Needed, 4= Hold Then Approved, 5= Reprocessed, 6= Reprocessed Then Approved
        /// <returns>
        /// Return VMPayroll view model
        /// </returns>
        public async Task<VMPayroll> PayrollGet(int[] status = null, int[] approvalstatus = null)
        {
            DateTime now = DateTime.Now;
            var startDate = new DateTime(now.Year, now.Month - 1, 1);
            var endDate = startDate.AddMonths(1).AddDays(-1);
            var data = await Task.Run(() => (from t1 in _db.Payroll_PayrollMaster
                                             join t2 in _db.HRMS_BusinessUnit on t1.HRMS_BusinessUnitFK equals t2.ID
                                             where t1.Active == true &&
                                                    t2.Active == true &&
                                                    (status == null || status.Contains(t1.Status))
                                             select new VMPayroll
                                             {
                                                 ID = t1.ID,
                                                 PayrollTitle = t1.PayrollTitle,
                                                 FromDate = t1.FromDate,
                                                 ToDate = t1.ToDate,
                                                 PaymentDate = t1.PaymentDate,
                                                 VMBusinessUnitName = t2.Name,
                                                 Remarks = t1.Remarks,
                                                 TotalMember = _db.Payroll_PayrollDetails.Count(x => x.Payroll_PayrollMasterFk == t1.ID && (approvalstatus == null || approvalstatus.Contains(x.ApprovalStatus))),
                                                 Status = t1.Status == 1 ? "Pending" : t1.Status == 2 ? "Processed" : t1.Status == 3 ? "Closed" : "",
                                                 StatusClass = t1.Status == 1 ? "label label-warning" : t1.Status == 2 ? "label label-info" : t1.Status == 3 ? "label label-success" : "",
                                                 StatusId = t1.Status,
                                                 TotalAmount = (_db.Payroll_PayrollDetails.Where(x => x.Payroll_PayrollMasterFk == t1.ID && (approvalstatus == null || approvalstatus.Contains(x.ApprovalStatus))).Select(x => x.FinalSalary).DefaultIfEmpty(0).Sum())
                                             }).OrderByDescending(x => x.ID).AsEnumerable());
            var VM = new VMPayroll
            {
                DataList = data,
                FromDate = startDate,
                ToDate = endDate,
                PaymentDate = now,
                VMBusinessUnitList = await BusinessUnitDropDownListAsync()
            };

            return VM;
        }
        /// <summary>
        /// Generate Payroll for a date range and specific Business unit though Stored Procedure
        /// <see cref="ERP\erp\Erp.Infrastructure\Database\StoredProcedures\Payroll\GenerateEmployeeSalary.sql"/>
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<int> GeneratePayroll(VMPayroll model)
        {
            int result = -1;

            var title = new SqlParameter("@PayrollTitle", SqlDbType.NVarChar)
            {
                Value = model.PayrollTitle
            };
            var paymentdate = new SqlParameter("@Paymentdate", SqlDbType.DateTime2)
            {
                Value = model.PaymentDate
            };
            var remarks = new SqlParameter("@Remarks", SqlDbType.NVarChar)
            {
                Value = model.Remarks ?? ""
            };
            var fromdate = new SqlParameter("@FromDate", SqlDbType.NVarChar)
            {
                Value = model.FromDate.ToString("yyyyMMdd")
            };
            var todate = new SqlParameter("@ToDate", SqlDbType.NVarChar)
            {
                Value = model.ToDate.ToString("yyyyMMdd")
            };
            var user = new SqlParameter("@User", SqlDbType.NVarChar)
            {
                Value = model.User
            };
            var businessunit = new SqlParameter("@BusinessUnit", SqlDbType.Int)
            {
                Value = model.VMBusinessUnitId
            };

            var command = "EXEC [GenerateEmployeeSalary] @PayrollTitle={0},@Paymentdate={1},@Remarks={2}, @FromDate={3}, @Todate={4}, @User={5}, @BusinessUnit={6}";
            result = await _db.ExecuteSqlCommandAsync(command, title, paymentdate, remarks, fromdate, todate, user, businessunit);
            return result;
        }
        /// <summary>
        /// Load generated employee salary in a payroll (Child Page)
        /// </summary>
        /// <param name="payrollid"></param> Payroll ID (Master Id)
        /// <param name="status"></param> Null= Load all data, 1= Pending (Default) & 2= Processed, 3= Closed 
        /// <returns>
        /// Returns VMPayrollDetails view model
        /// </returns> 
        public async Task<VMPayrollDetails> PayrollDetailsGet(int payrollid, int[] status = null)
        {
            var data = await Task.Run(() => (from t1 in _db.Payroll_PayrollDetails
                                             join t2 in _db.HRMS_Employee on t1.EmployeeFk equals t2.ID
                                             join t4 in _db.HRMS_Section on t2.HRMS_SectionFK equals t4.ID
                                             join t3 in _db.User_Department on t2.User_DepartmentFK equals t3.ID
                                             join t5 in _db.HRMS_Designation on t2.HRMS_DesignationFK equals t5.ID
                                             where t1.Payroll_PayrollMasterFk == payrollid &&
                                                    t2.Active == true &&
                                                    t3.Active == true &&
                                                    t4.Active == true &&
                                                    (status == null || status.Contains(t1.ApprovalStatus))
                                             select new VMPayrollDetails
                                             {
                                                 ID = t1.ID,
                                                 PayrollId = t1.Payroll_PayrollMasterFk,
                                                 EmployeeIdentity = t2.EmployeeIdentity,
                                                 EmployeeName = t2.Name,
                                                 Designation = t5.Name,
                                                 DepartmentName = t3.Name,
                                                 SectionName = t4.Name,
                                                 GrossSalary = t1.GrossSalary,
                                                 BasicSalary = t1.BasicSalary,
                                                 TotalDeduction = t1.TotalDeduction,
                                                 AttendanceBonus = t1.AttendanceBonus,
                                                 PayableOverTimeAmount = t1.PayableOverTimeAmount,
                                                 StampCharge = t1.StampCharge,
                                                 FinalSalary = t1.FinalSalary,
                                                 ApprovalStatus = t1.ApprovalStatus == 1 ? "Approved" : t1.ApprovalStatus == 2 ? "On Hold" : t1.ApprovalStatus == 3 ? "Reprocess" : t1.ApprovalStatus == 4 ? "Hold & Approved" : t1.ApprovalStatus == 5 ? "Reprocessed" : t1.ApprovalStatus == 6 ? "Reprocessed & approved" : "",
                                                 ApprovalStatusClass = t1.ApprovalStatus == 1 ? "label label-success" : t1.ApprovalStatus == 2 ? "label label-danger" : t1.ApprovalStatus == 3 ? "label label-warning" : t1.ApprovalStatus == 4 ? "label label-success" : t1.ApprovalStatus == 5 ? "label label-info" : t1.ApprovalStatus == 6 ? "label label-success" : "",
                                                 ApprovalStatusId = t1.ApprovalStatus,
                                                 PaymentStatusId = t1.PaymentStatus
                                             }).AsEnumerable());
            var VM = new VMPayrollDetails
            {
                DataList = data,
                OverAllStatus = _db.Payroll_PayrollMaster.Single(x => x.ID == payrollid && x.Active == true).Status
            };
            return VM;
        }
        /// <summary>
        /// Change Payroll Status (Master model) Pending--> Processed--> Closed
        /// </summary>
        /// <param name="payrollmasterid"></param> Payroll ID (Master Id) which status will change
        /// <param name="status"></param>  1= Pending (Default) & 2= Processed, 3= Closed 
        /// <returns></returns>
        public async Task<int> ChangePayrollStatus(int payrollmasterid, int status)
        {
            int result = -1;
            var details = _db.Payroll_PayrollMaster.Where(x => x.ID == payrollmasterid && x.Active == true).SingleOrDefault();
            if (details != null)
            {
                details.Status = status;
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = 1;
                }
            }
            return result;
        }
        /// <summary>
        /// Change particular employee salary under a generated payroll i,e. PayrollDetails (Child model)
        /// </summary>
        /// <param name="payrolldetailsid"></param> Payroll Details ID (Child Id) which status will change
        /// <param name="status"></param> 1= Approved (Default), 2= Hold, 3= Reprocess Needed, 4= Hold Then Approved, 5= Reprocessed, 6= Reprocessed Then Approved
        public async Task<int> ChangePayrollDetailsStatus(int payrolldetailsid, int status)
        {
            int result = -1;
            var details = _db.Payroll_PayrollDetails.Where(x => x.ID == payrolldetailsid && x.Active == true).SingleOrDefault();
            if (details != null)
            {
                switch (status)
                {
                    // If approved then default "Payment Status" is Paid
                    // Else "Payment Status" is Unpaid for all "Approval Status"
                    case 1:
                        details.ApprovalStatus = status;
                        details.PaymentStatus = 1;
                        break;
                    default:
                        details.ApprovalStatus = status;
                        details.PaymentStatus = 2;
                        break;
                }
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = 1;
                }
            }
            return result;
        }
        /// <summary>
        /// Change Payment Status Paid or Unpaid 
        /// </summary>
        /// <param name="payrolldetailsid"></param> Payroll Details ID (Child Id) which status will change
        /// <param name="status"></param> 1= Paid (Default) & 2= Unpaid
        /// <returns></returns>
        public async Task<int> ChangePaymentStatus(int payrolldetailsid, int status)
        {
            int result = -1;
            var details = _db.Payroll_PayrollDetails.Where(x => x.ID == payrolldetailsid && x.Active == true).SingleOrDefault();
            if (details != null)
            {
                details.PaymentStatus = status;
            }
            if (await _db.SaveChangesAsync() > 0)
            {
                result = 1;
            }
            return result;
        }
        /// <summary>
        /// Regenerate/ Recalculate particular employee salary under a payroll 
        /// <see cref="ERP\erp\Erp.Infrastructure\Database\StoredProcedures\Payroll\GenerateEmployeeSalary.sql"/>
        /// </summary>
        /// <param name="payrolldetailsid"></param> Payroll Details ID (Child Id) which status will Regenerate/ Recalculate.
        /// <param name="userid"></param>
        /// <returns></returns>
        public async Task<int> RegenerateSpecificEmployeeSalary(int payrolldetailsid, string userid)
        {
            int result = -1;

            var payrolldetails = new SqlParameter("@PayrollDetailsId", SqlDbType.Int)
            {
                Value = payrolldetailsid
            };
            var user = new SqlParameter("@User", SqlDbType.NVarChar)
            {
                Value = userid
            };
            var command = "EXEC [GenerateEmployeeSalary] @User={0}, @PayrollDetailsId={1}";

            result = await _db.ExecuteSqlCommandAsync(command, user, payrolldetails);

            return result;
        }

        #endregion

        ///<summary>
        /// This region hold all information for Promotion
        /// </summary>
        #region Promotion with or without increament
        /// <summary>
        /// Initialize VMPromotion view model for promotion
        /// </summary>
        public async Task<VMPromotion> EmployeePromotionGet()
        {
            var VM = new VMPromotion
            {
                VmEmployeeList = await EmployeeDropDownListAsync(),
                VMBusinessUnitList = await BusinessUnitDropDownListAsync(),
                VMLineList = await LineListAsync(),
                PromotionDate = DateTime.Today
            };
            return VM;
        }
        /// <summary>
        /// Load employee particulars along with salary
        /// </summary>
        /// <param name="id"></param> Employee ID
        public async Task<VMPromotion> EmployeeParticularGetById(int id)
        {
            var data = await Task.Run(() => (from t1 in _db.HRMS_Employee
                                             join t2 in _db.HRMS_Section on t1.HRMS_SectionFK equals t2.ID
                                             join t3 in _db.User_Department on t1.User_DepartmentFK equals t3.ID
                                             join t4 in _db.HRMS_Unit on t2.HRMS_UnitFK equals t4.ID
                                             join t5 in _db.HRMS_BusinessUnit on t4.HRMS_BusinessUnitFK equals t5.ID
                                             where t1.ID == id && t1.Active == true
                                             select new VMPromotion
                                             {
                                                 VMBusinessUnitId = t5.ID,
                                                 VMUnitId = t4.ID,
                                                 VMDepartmentId = t3.ID,
                                                 VMSectionId = t2.ID,
                                                 VMDesignationId = t1.HRMS_DesignationFK ?? 0,
                                                 VMLineId = t1.Common_ProductionLineFK ?? 0,
                                                 VmGradeId = t1.Grade
                                             }).Single());

            var salary = _db.Payroll_EODRecordMaster.SingleOrDefault(x => x.EmployeeFk == id && x.Active == true);
            data.GrossSalary = salary != null ? salary.GrossSalary : 0;
            data.VmEffectedDate = salary != null ? salary.NextIncreamentDate.ToShortDateString() : DateTime.Today.ToShortDateString();
            return data;
        }
        /// <summary>
        /// Create Promotion with or without increament and save previous employement history 
        /// </summary>
        /// <remarks>
        /// model.IsIncreamen= 1 (Only Promotion- No Increament)
        /// model.IsIncreamen= 2 (Only Increament)
        /// model.IsIncreamen= 3 (Promotion with Increament)
        /// </remarks>
        /// <see cref="ApproveIncreamentedSalary()"/>
        /// <param name="model"></param>
        public async Task<int> EmployeePromotionAddAsync(VMPromotion model)
        {
            int result = -1;
            // Get Previous employement data which will change
            var prevEmpData = _db.HRMS_Employee.Single(x => x.ID == model.VmEmployeeFk && x.Active);
            //Get Previous salary reference of a employee (Master model) 
            int? prevEodId = _db.Payroll_EODRecordMaster.FirstOrDefault(x => x.EmployeeFk == model.VmEmployeeFk && x.Active == true).ID;
            int? presentEodId = null;

            var checkPendingIncreament = _db.Payroll_EODRecordMaster.Where(x => x.EmployeeFk == model.VmEmployeeFk && x.Active == false && x.Status == 1).FirstOrDefault();
            if (checkPendingIncreament != null)
            {
                var salarydetails = _db.Payroll_EODRecord.Where(x => x.Payroll_EODRecordMasterFk == checkPendingIncreament.ID);
                VMEmployeeSalary vmmodel = new VMEmployeeSalary
                {
                    EODMasterId = checkPendingIncreament.ID,
                    GrossSalary = model.GrossSalary,
                    AllocationId = checkPendingIncreament.SalaryAllowance,
                    BillEligibility = checkPendingIncreament.BillEligibily,
                    HolidayBillForEdit = salarydetails.Single(x => x.EODReferenceFk == 23).Amount,
                    NightBillForEdit = salarydetails.Single(x => x.EODReferenceFk == 24).Amount,
                    RemarksForEdit = checkPendingIncreament.Remarks,
                    User = model.User
                };
                await EmployeeSalaryEdit(vmmodel, false);
            }
            else
            {
                // If Promotion with Increament then keep track previous of present and previous salary structure of a employee in promotion history table.
                // It could be used to find who had promotion along with increament & increament details from (previous & present) in a single history table
                if (model.IsIncreament == 3)
                {
                    presentEodId = await ApproveIncreamentedSalary(prevEodId.Value, model.GrossSalary, model.User, model.EffectedDate);
                }
                // If only increament no promotion history will not keep. Only increament will apply as increament rules.
                else if (model.IsIncreament == 2)
                {
                    result = await ApproveIncreamentedSalary(prevEodId.Value, model.GrossSalary, model.User, model.EffectedDate);
                    return result;
                }
            }
            // Get Previous Promotion history
            var prevPromotion = _db.Payroll_EmployeePromotionalHistory.Where(x => x.HRMS_EmployeeFK == model.VmEmployeeFk).OrderByDescending(x => x.ID).FirstOrDefault();
            // Add New promotion into model
            await _db.Payroll_EmployeePromotionalHistory.AddAsync(new Payroll_EmployeePromotionalHistory
            {
                // New designation, department etc..
                HRMS_EmployeeFK = model.VmEmployeeFk,
                HRMS_SectionFK = model.VMSectionId,
                HRMS_DesignationFK = model.VMDesignationId,
                Common_ProductionLineFK = model.VMLineId,
                Grade = "No Grade", // pending management decision on whether it needed or not

                PromotionType = model.IsIncreament, // 1= Only Promotion- No Increament, 2= Only Increament & 3= Promotion with Increament
                PromotionDate = model.PromotionDate,
                PreviousPromotionDate = prevPromotion == null ? prevEmpData.JoiningDate : prevPromotion.PromotionDate, // Track previous promotion date in case 1st promotion then joining date consider as previous promotion date
                Payroll_EODRecordMasterFk = presentEodId ?? prevEodId, // New salary (if increament) else Null
                PreviousPayroll_EODRecordMasterFk = prevEodId, // Previous salary history  (if increament) else Null 
                Active = true,
                User = model.User,
                Time = DateTime.Now,
                Remarks = ""
            });

            // Previous Employement History
            prevEmpData.HRMS_SectionFK = model.VMSectionId;
            prevEmpData.HRMS_DesignationFK = model.VMDesignationId;
            prevEmpData.Common_ProductionLineFK = model.VMLineId;
            prevEmpData.Grade = model.VmGradeId;
            if (await _db.SaveChangesAsync() > 0)
            {
                result = 1;
            }
            return result;

        }
        /// <summary>
        /// Load Promotion History
        /// </summary>
        /// <returns>
        /// returns VMPromotion view model
        /// </returns>
        public async Task<VMPromotion> PromotionList()
        {
            // Present Employement History
            var data = await Task.Run(() => (from t1 in _db.Payroll_EmployeePromotionalHistory
                                             join t2 in _db.HRMS_Employee on t1.HRMS_EmployeeFK equals t2.ID
                                             join t3 in _db.HRMS_Designation on t2.HRMS_DesignationFK equals t3.ID into t3_join
                                             from t3 in t3_join.DefaultIfEmpty()
                                             join t4 in _db.HRMS_Section on t2.HRMS_SectionFK equals t4.ID into t4_join
                                             from t4 in t4_join.DefaultIfEmpty()
                                             join t5 in _db.User_Department on t2.User_DepartmentFK equals t5.ID into t5_join
                                             from t5 in t5_join.DefaultIfEmpty()
                                             join t6 in _db.HRMS_Unit on t4.HRMS_UnitFK equals t6.ID into t6_Join
                                             from t6 in t6_Join.DefaultIfEmpty()
                                             join t7 in _db.HRMS_BusinessUnit on t6.HRMS_BusinessUnitFK equals t7.ID into t7_join
                                             from t7 in t7_join.DefaultIfEmpty()
                                             join t8 in _db.Common_ProductionLine on t2.Common_ProductionLineFK equals t8.ID into t8_join
                                             from t8 in t8_join.DefaultIfEmpty()

                                                 // Previous Employement History
                                             join t9 in _db.HRMS_Designation on t1.HRMS_DesignationFK equals t9.ID into t9_join
                                             from t9 in t9_join.DefaultIfEmpty()
                                             join t10 in _db.HRMS_Section on t1.HRMS_SectionFK equals t10.ID into t10_join
                                             from t10 in t10_join.DefaultIfEmpty()
                                             join t11 in _db.User_Department on t2.User_DepartmentFK equals t11.ID into t11_join
                                             from t11 in t11_join.DefaultIfEmpty()
                                             join t12 in _db.HRMS_Unit on t4.HRMS_UnitFK equals t12.ID into t12_Join
                                             from t12 in t12_Join.DefaultIfEmpty()
                                             join t13 in _db.HRMS_BusinessUnit on t12.HRMS_BusinessUnitFK equals t13.ID into t13_join
                                             from t13 in t13_join.DefaultIfEmpty()
                                             join t14 in _db.Common_ProductionLine on t1.Common_ProductionLineFK equals t14.ID into t14_join
                                             from t14 in t14_join.DefaultIfEmpty()

                                             join t15 in _db.Payroll_EODRecordMaster on t1.Payroll_EODRecordMasterFk equals t15.ID into t15_join
                                             from t15 in t15_join.DefaultIfEmpty()
                                             join t16 in _db.Payroll_EODRecordMaster on t1.PreviousPayroll_EODRecordMasterFk equals t16.ID into t16_join
                                             from t16 in t16_join.DefaultIfEmpty()
                                             orderby t1.ID descending
                                             select new VMPromotion
                                             {
                                                 EmployeeId = t2.EmployeeIdentity,
                                                 EmployeeName = t2.Name,
                                                 CurrentDesignation = t3.Name ?? "",
                                                 CurrentBUnit = t7.Name ?? "",
                                                 CurrentUnit = t6.Name ?? "",
                                                 CurrentSection = t4.Name ?? "",
                                                 CurrentLine = t8.Name ?? "",
                                                 CurrentDepartment = t5.Name ?? "",
                                                 PreviousDesignation = t9.Name ?? "",
                                                 PreviousBUnit = t13.Name ?? "",
                                                 PreviousUnit = t12.Name ?? "",
                                                 PreviousSection = t10.Name ?? "",
                                                 PreviousLine = t14.Name ?? "",
                                                 PreviousDepartment = t11.Name ?? "",
                                                 CurrentSalary = t15.GrossSalary,
                                                 PreviousSalary = t16.GrossSalary
                                             }).AsEnumerable());
            VMPromotion vm = new VMPromotion
            {
                DataList = data
            };
            return vm;
        }
        #endregion


        /// <summary>
        /// Settings for Bonus (For dynamic)
        /// </summary>
        #region Bonus
        public async Task<VMBonusSettings> BonusSettingsGet()
        {
            var data = await Task.Run(() => _db.Payroll_BonusSettings.Where(x => x.Active == true).Select(x => new VMBonusSettings
            {
                ID = x.ID,
                VMFromMonth = x.FromMonth,
                VMToMonth = x.ToMonth,
                VMBonusRate = x.BonusRate,
                GrossOrBasic = x.IsBasicOrGross == 1 ? "Gross Salary" : "Basic Salary",
                VMIsBasicOrGrossId = x.IsBasicOrGross,
                Remarks = x.Remarks
            }).AsEnumerable());
            VMBonusSettings VM = new VMBonusSettings
            {
                DataList = data
            };
            return VM;
        }
        public async Task<int> BonusSettingsAddAsync(VMBonusSettings model)
        {
            int result = -1;
            _db.Payroll_BonusSettings.Add(new Payroll_BonusSettings
            {
                FromMonth = model.VMFromMonth,
                ToMonth = model.VMToMonth,
                IsBasicOrGross = model.VMIsBasicOrGrossId,
                BonusRate = model.VMBonusRate,
                Remarks = model.Remarks
            });
            if (await _db.SaveChangesAsync() > 0)
            {
                result = 1;
            }
            return result;
        }
        public async Task<int> BonusSettingsEditAsync(VMBonusSettings model)
        {
            int result = -1;
            Payroll_BonusSettings settings = await Task.Run(() => _db.Payroll_BonusSettings.Where(x => x.ID == model.ID).SingleOrDefault());

            if (settings != null)
            {
                settings.FromMonth = model.VMFromMonth;
                settings.ToMonth = model.VMToMonth;
                settings.IsBasicOrGross = model.VMIsBasicOrGrossId;
                settings.BonusRate = model.VMBonusRate;
                settings.Remarks = model.Remarks;
            }
            if (await _db.SaveChangesAsync() > 0)
            {
                result = 1;
            }
            return result;
        }
        public async Task<VMBonusMaster> BonusMasterGet()
        {
            var data = await Task.Run(() => new VMBonusMaster
            {
                VMPaymentDate = DateTime.Today,
                DataList = _db.Payroll_BonusMaster.Where(x => x.Active == true).Select(x => new VMBonusMaster
                {
                    ID = x.ID,
                    VMBonusTitle = x.BonusTitle,
                    VMPaymentDate = x.PaymentDate,
                    VMRemarks = x.Remarks
                }).OrderByDescending(x => x.ID).AsEnumerable()
            });
            return data;
        }
        /// <summary>
        /// Create Bonus using stored procedure
        /// </summary>
        /// <see cref="ERP\erp\Erp.Infrastructure\Database\StoredProcedures\Payroll\GenerateBonus.sql"/>
        /// <param name="model"></param>
        public async Task<int> BonusMasterAdd(VMBonusMaster model)
        {
            int result = -1;
            var title = new SqlParameter("@BonusTitle", SqlDbType.NVarChar)
            {
                Value = model.VMBonusTitle
            };
            var paymentdate = new SqlParameter("@PaymentDate", SqlDbType.NVarChar)
            {
                Value = model.VMPaymentDate.ToString("yyyyMMdd")
            };
            var remarks = new SqlParameter("@Remarks", SqlDbType.NVarChar)
            {
                Value = model.VMRemarks ?? ""
            };
            var user = new SqlParameter("@UserID", SqlDbType.NVarChar)
            {
                Value = model.User
            };
            var command = "EXEC [GenerateBonus] @BonusTitle={0},@PaymentDate={1},@Remarks={2}, @UserID={3}";
            result = await _db.ExecuteSqlCommandAsync(command, title, paymentdate, remarks, user);
            return result;
        }
        /// <summary>
        /// Load all bonuses of employee
        /// </summary>
        /// <param name="id"></param> Bonus master ID
        public async Task<VMBonusDetails> BonusDetailsGet(int id)
        {

            var data = await Task.Run(() => (from t1 in _db.Payroll_BonusDetails
                                             join t2 in _db.HRMS_Employee on t1.HRMS_EmployeeFK equals t2.ID
                                             join t3 in _db.HRMS_Designation on t2.HRMS_DesignationFK equals t3.ID into t3_join
                                             from t3 in t3_join.DefaultIfEmpty()
                                             join t4 in _db.HRMS_Section on t2.HRMS_SectionFK equals t4.ID into t4_join
                                             from t4 in t4_join.DefaultIfEmpty()
                                             join t5 in _db.User_Department on t2.User_DepartmentFK equals t5.ID into t5_Join
                                             from t5 in t5_Join.DefaultIfEmpty()
                                             where t1.Payroll_BonusMasterFk == id && t1.Active == true && t2.Active == true
                                             select new VMBonusDetails
                                             {
                                                 EmployeeName = t2.Name,
                                                 EmployeeID = t2.EmployeeIdentity,
                                                 Designation = t3.Name,
                                                 Section = t4.Name,
                                                 Department = t5.Name,
                                                 JoiningDate = t2.JoiningDate,
                                                 Tenure = t1.Tenure,
                                                 GrossSalary = t1.GrossSalary,
                                                 BasicSalary = t1.BasicSalary,
                                                 BonusRate = t1.BonusRate,
                                                 BonusAmount = t1.BonusAmount + t1.StampCharge,
                                                 StampCharge = t1.StampCharge,
                                                 TotalPayable = t1.BonusAmount
                                             }).AsEnumerable());
            var model = new VMBonusDetails
            {
                DataList = data
            };
            return model;
        }
        #endregion

        /// <summary>
        /// This region have all Dropdown used in payroll
        /// </summary>
        #region Dropdown
        public async Task<SelectList> BusinessUnitDropDownListAsync()
        {
            return new SelectList(await _db.HRMS_BusinessUnit.Where(x => x.Active).Select(x => new { Value = x.ID, Text = x.Name }).ToListAsync(), "Value", "Text");
        }
        public async Task<SelectList> UnitDropDownListAsync()
        {
            return new SelectList(await _db.HRMS_Unit.Where(x => x.Active).Select(x => new { Value = x.ID, Text = x.Name }).ToListAsync(), "Value", "Text");
        }
        public async Task<SelectList> DepartmentDropDownListAsync()
        {
            return new SelectList(await _db.User_Department.Where(x => x.Active).Select(x => new { Value = x.ID, Text = x.Name }).ToListAsync(), "Value", "Text");
        }
        public async Task<SelectList> SectionDropDownListAsync()
        {
            return new SelectList(await _db.HRMS_Section.Where(x => x.Active).Select(x => new { Value = x.ID, Text = x.Name }).ToListAsync(), "Value", "Text");
        }
        public async Task<SelectList> DesignationDropDownListAsync()
        {
            return new SelectList(await _db.HRMS_Designation.Where(x => x.Active).Select(x => new { Value = x.ID, Text = x.Name }).ToListAsync(), "Value", "Text");
        }
        public async Task<SelectList> LineListAsync()
        {
            return new SelectList(await _db.Common_ProductionLine.Where(x => x.Active).Select(x => new { Value = x.ID, Text = x.Name }).ToListAsync(), "Value", "Text");
        }
        public async Task<SelectList> EmployeeDropDownListAsync()
        {
            return new SelectList(await _db.HRMS_Employee.Where(x => x.Active).Select(x => new { Value = x.ID, Text = x.EmployeeIdentity + " :- " + x.Name }).ToListAsync(), "Value", "Text");
        }

        public IEnumerable AllocationDropDownList()
        {
            var allocationlist = new List<object>
            {
                new { Text = "OT & Attendance Bonus", Value = "1" },
                new { Text = "Only OT", Value = "2" },
                new { Text = "Only Attendance Bonus", Value = "3" },
                new { Text = "None", Value = "4" }
            };
            return allocationlist;
        }
        public IEnumerable DesignationDropDownList()
        {
            var designationlist = new List<object>();
            designationlist.Add(new { Text = "Sr. Engineer", Value = "1" });
            designationlist.Add(new { Text = "Jr. Engineer", Value = "2" });
            designationlist.Add(new { Text = "Trainee Engineer", Value = "3" });
            return designationlist;
        }
        public IEnumerable LineDropDownList()
        {
            var linelist = new List<object>();
            linelist.Add(new { Text = "Line-1", Value = 1 });
            linelist.Add(new { Text = "Line-2", Value = 2 });
            linelist.Add(new { Text = "Line-3", Value = 3 });
            return linelist;
        }
        public IEnumerable GradeTypeDropDownList()
        {
            var grade = new List<object>();
            grade.Add(new { Text = "Grade 1", Value = "Grade 1" });
            grade.Add(new { Text = "Grade 2", Value = "Grade 2" });
            grade.Add(new { Text = "Grade 3", Value = "Grade 3" });
            grade.Add(new { Text = "Grade 4", Value = "Grade 4" });
            grade.Add(new { Text = "Grade 5", Value = "Grade 5" });
            grade.Add(new { Text = "Grade 6", Value = "Grade 6" });
            grade.Add(new { Text = "Grade 7", Value = "Grade 7" });
            grade.Add(new { Text = "None", Value = "None" });
            return grade;
        }
        public IEnumerable DepartmentDropDownList()
        {
            var department = new List<object>();
            department.Add(new { Text = "Admin", Value = 1 });
            department.Add(new { Text = "Commercial", Value = 2 });
            department.Add(new { Text = "Procurement", Value = 3 });
            department.Add(new { Text = "Marchendising", Value = 4 });
            department.Add(new { Text = "Store", Value = 5 });
            department.Add(new { Text = "Constraction", Value = 6 });
            department.Add(new { Text = "Accounts", Value = 7 });
            department.Add(new { Text = "Commercial", Value = 8 });
            return department;
        }
        #endregion
    }
}
