﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Erp.Core.Services.Payroll
{
    public class VMPromotion
    {
        [DisplayName("Employee")]
        public int VmEmployeeFk { get; set; }
        public SelectList VmEmployeeList { get; set; } = new SelectList(new List<object>());

        [DisplayName("Business Unit")]
        public int VMBusinessUnitId { get; set; }
        public SelectList VMBusinessUnitList { get; set; } = new SelectList(new List<object>());

        [DisplayName("Unit")]
        public int VMUnitId { get; set; }
        public SelectList VMUnitList { get; set; } = new SelectList(new List<object>());

        [DisplayName("Department")]
        public int VMDepartmentId { get; set; }
        public SelectList VMDepartmentList { get; set; } = new SelectList(new List<object>());

        [DisplayName("Section")]
        public int VMSectionId { get; set; }
        public SelectList VMSectionList { get; set; } = new SelectList(new List<object>());

        [DisplayName("Line")]
        public int VMLineId { get; set; }
        public SelectList VMLineList { get; set; } = new SelectList(new List<object>());

        [DisplayName("Designation")]
        public int VMDesignationId { get; set; }
        public SelectList VMDesignationList { get; set; } = new SelectList(new List<object>());

        [DisplayName("Promotion Date")]
        public DateTime PromotionDate { get; set; }
        [DisplayName("Grade")]
        public string VmGradeId { get; set; }
        [DisplayName("Promotion & Increament Type")]
        public int IsIncreament { get; set; }
        [DisplayName("New Gross Salary")]
        public decimal GrossSalary { get; set; }
        [DisplayName("New Increament Effective From")]
        public DateTime EffectedDate { get; set; }
        public string VmEffectedDate { get; set; }
        public string User { get; set; }

        public string EmployeeId { get; set; }
        public string EmployeeName { get; set; }
        public string CurrentBUnit { get; set; }
        public string CurrentUnit { get; set; }
        public string CurrentSection { get; set; }
        public string CurrentDepartment { get; set; }
        public string CurrentDesignation { get; set; }
        public string CurrentLine { get; set; }
        public decimal? CurrentSalary { get; set; }

        public string PreviousBUnit { get; set; }
        public string PreviousUnit { get; set; }
        public string PreviousSection { get; set; }
        public string PreviousDepartment { get; set; }
        public string PreviousDesignation { get; set; }
        public string PreviousLine { get; set; }
        public decimal? PreviousSalary { get; set; }

        public IEnumerable<VMPromotion> DataList { get; set; }
    }
}