﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Erp.Core.Services.Payroll
{
    public class VMPayroll : BaseVM
    {
        [DisplayName("Payroll Title")]
        public string PayrollTitle { get; set; }
        [DisplayName("From Date")]
        public DateTime FromDate { get; set; }
        [DisplayName("To Date")]
        public DateTime ToDate { get; set; }
        [DisplayName("Payment Date")]
        public DateTime PaymentDate { get; set; }
        public string Remarks { get; set; }
        [DisplayName("Business Unit")]
        public int VMBusinessUnitId { get; set; }
        public string VMBusinessUnitName { get; set; }
        public SelectList VMBusinessUnitList { get; set; } = new SelectList(new List<object>());
        public int TotalMember { get; set; }
        public string Status { get; set; }
        public string StatusClass { get; set; }
        public int StatusId { get; set; }
        public decimal TotalAmount { get; set; }
        public IEnumerable<VMPayroll> DataList { get; set; }
    }

    public class VMPayrollDetails : BaseVM
    {
        public int PayrollId { get; set; }
        public int EmployeeId { get; set; }
        public string EmployeeIdentity { get; set; }
        public string EmployeeName { get; set; }
        public string Designation { get; set; }
        public string DepartmentName { get; set; }
        public string SectionName { get; set; }
        public int EmployeeFk { get; set; }
        public decimal GrossSalary { get; set; }
        public decimal BasicSalary { get; set; }
        public decimal HouseRent { get; set; }
        public decimal OtherAllowance { get; set; }
        public int TotalPresents { get; set; }
        public int HolidayOffdays { get; set; }
        public int LeaveDays { get; set; }
        public int AbsentDays { get; set; }
        public decimal TotalDeduction { get; set; }
        public decimal DeductedSalary { get; set; }
        public decimal AttendanceBonus { get; set; }
        public int PayableOverTimeHours { get; set; }
        public decimal OverTimeRate { get; set; }
        public decimal PayableOverTimeAmount { get; set; }
        public decimal StampCharge { get; set; }
        public decimal FinalSalary { get; set; }
        public string ApprovalStatus { get; set; }
        public string ApprovalStatusClass { get; set; }
        public int ApprovalStatusId { get; set; }
        public int PaymentStatusId { get; set; }
        public int OverAllStatus { get; set; }
        public IEnumerable<VMPayrollDetails> DataList { get; set; }
    }
}
