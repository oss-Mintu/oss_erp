﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Erp.Core.Services.Integration
{
   public class MessageService
    {
        private readonly ITempDataDictionary _tempData;
        public MessageService(IHttpContextAccessor httpContextAccessor, ITempDataDictionaryFactory tempDataDictionaryFactory)
        {
            _tempData = tempDataDictionaryFactory.GetTempData(httpContextAccessor.HttpContext);
        }     
        public void Information(string message, bool dismissable = false)
        {
            AddMessage(MessageStyle.Information, message, dismissable);
        }
        public void Success(string message, bool dismissable = false)
        {
            AddMessage(MessageStyle.Success, message, dismissable);
        }
        public void Warning(string message, bool dismissable = false)
        {
            AddMessage(MessageStyle.Warning, message, dismissable);
        }
        public void Danger(string message, bool dismissable = false)
        {
            AddMessage(MessageStyle.Danger, message, dismissable); 
        }
        public void NoMessage(string message, bool dismissable = false)
        {
            AddMessage(MessageStyle.Danger, message, dismissable); 
        }
        private void AddMessage(string messageStyle, string message, bool dismissable)
        {
            var messages = _tempData.ContainsKey(Message.TempDataKey)
                ? (List<Message>)_tempData[Message.TempDataKey] : new List<Message>();
            messages.Add(new Message
            {
                MessageStyle = messageStyle,
                Messages = message,
                Dismissable = dismissable
            });
            _tempData[Message.TempDataKey] = JsonConvert.SerializeObject(messages); ;
        }
    }
}
