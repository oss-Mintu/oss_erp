﻿using System;
using System.Collections.Generic;
using Erp.Core.Entity.Common;
using Erp.Core.Services;
using Microsoft.Extensions.Configuration;
using Npgsql;
using PostgreSQLCopyHelper;

namespace PostgreSQLBulkInsertWebApp.Service
{
    public class BulkInsertService : BaseService
    {
        private readonly IConfiguration _iconfiguration;
        public BulkInsertService(IConfiguration iconfiguration)
        {
            _iconfiguration = iconfiguration;
        }

        public int SaveProductData(List<Common_Product> data)
        {
            PostgreSQLCopyHelper<Common_Product> insert = null;
            var entities = new List<Common_Product>();

            foreach (var pd in data)
            {
                insert = new PostgreSQLCopyHelper<Common_Product>("public", "common_product")
                    .MapInteger("id", x => x.id)
                    .MapText("barcode", x => pd.barcode)
                    .MapText("description", x => pd.description)
                    .MapText("size", x => pd.size)
                    .MapText("vatcode", x => pd.vatcode)   //vatcode is numeric in DB, this is a String (new column for this test)
                    .MapInteger("quantity", x => pd.quantity)
                    .MapDouble("cost", x => pd.cost)
                    .MapDouble("price", x => pd.price);

                entities.Add(pd);
            }

            using (var connection = new NpgsqlConnection(_iconfiguration.GetConnectionString("DefaultPostGreConnection")))  //"Host=127.0.0.1;Database=ZERP;Username=postgres;Password=1qaz"))
            {
                try
                {
                    connection.Open();
                    insert.SaveAll(connection, entities);
                    int lineCount = entities.Count;
                    //TempData["SuccessMessage"] = lineCount + " Records Inserted!";
                }
                catch (Exception er)
                {
                    string str = er.Message;
                    //TempData["ErrorMessage"] = er.Message;
                    //TempData["ErrorMessage"] = "Error: importing records!";
                }
            }
            return 1;
        }

        public int SaveProductDataList(List<Common_Product> data)
        {
            PostgreSQLCopyHelper<Common_Product> insert = null;
            var entities = new List<Common_Product>();

            foreach (var pd in data)
            //while ((pd = data.to != null)
            {
                insert = new PostgreSQLCopyHelper<Common_Product>("public", "Common_Product")
                            //.MapUUID("q_guid", x => Guid.NewGuid())
                            .MapText("barcode", x => pd.barcode)
                            .MapText("description", x => pd.description)
                            .MapText("size", x => pd.size)
                            .MapText("vatcode", x => pd.vatcode)   //vatcode is numeric in DB, this is a String (new column for this test)
                            .MapInteger("quantity", x => pd.quantity)
                            .MapDouble("cost", x => pd.cost)
                            .MapDouble("price", x => pd.price);

                entities.Add(pd);
            }
            using (var connection = new NpgsqlConnection("Host=127.0.0.1;Database=ZERP;Username=postgres;Password=1qaz"))
            {
                try
                {
                    connection.Open();
                    insert.SaveAll(connection, entities);
                    int lineCount = entities.Count;
                    //TempData["SuccessMessage"] = lineCount + " Records Inserted!";
                }
                catch (Exception er)
                {
                    string str = er.Message;
                    //TempData["ErrorMessage"] = er.Message;
                    //TempData["ErrorMessage"] = "Error: importing records!";
                }
            }
            return 1;
        }

        public List<Common_Product> GetProductDataList()
        {
            var dt = new List<Common_Product>
            {
                new Common_Product
                {
                    id = 1, barcode = "1111", vatcode = "2222", description = "Description", size = "5Fit",
                    quantity = 10, cost = 123.25, price = 1000.50
                },
                new Common_Product
                {
                    id = 2, barcode = "1111", vatcode = "2222", description = "Description", size = "5Fit",
                    quantity = 10, cost = 123.25, price = 1000.50
                },
                new Common_Product
                {
                    id = 3, barcode = "1111", vatcode = "2222", description = "Description", size = "5Fit",
                    quantity = 10, cost = 123.25, price = 1000.50
                },
                new Common_Product
                {
                    id = 4, barcode = "1111", vatcode = "2222", description = "Description", size = "5Fit",
                    quantity = 10, cost = 123.25, price = 1000.50
                },
                new Common_Product
                {
                    id = 5, barcode = "1111", vatcode = "2222", description = "Description", size = "5Fit",
                    quantity = 10, cost = 123.25, price = 1000.50
                },
            };
            return dt;
        }

    }
}
