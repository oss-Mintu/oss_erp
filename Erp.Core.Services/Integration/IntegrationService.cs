﻿using Erp.Core.Services.Accounting;
using Erp.Core.Services.Commercial;
using Erp.Core.Services.Home;
using Erp.Core.Services.HRMS;
using Erp.Core.Services.Inventory;
using Erp.Core.Services.Merchandising;
using Erp.Core.Services.Procurement;
using Erp.Core.Services.Production;
using Erp.Core.Services.Shipment;
using Erp.Core.Services.Store;
using Erp.Core.Services.User;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Erp.Core.Services.Integration
{
    public class IntegrationService : BaseService
    {
        //private MessageService _messageService;
        //IHttpContextAccessor contextAccessor;
        //ITempDataDictionaryFactory tempDataDictionaryFactory;

        private AccountingService _accountingService;
        private InventoryService _inventoryService;
        private readonly HttpContext _httpContext;

        // private InventoryService _inventoryService;
        private HomeService _homeService;
        public IntegrationService(IErpDbContext db, HttpContext httpContext)
        {
            _db = db;
            _httpContext = httpContext;
            _accountingService = new AccountingService(db);
            _inventoryService = new InventoryService(db);
            _homeService = new HomeService(db, httpContext);

            //_messageService = new MessageService(contextAccessor, tempDataDictionaryFactory);
        }
        private MerchandisingService merchandisingService;
        private ProcurementService procurementService;
        private ProductionService productionService;
        private CommercialService commercialService;
        private HRMSService HRMSService;
        private StoreService storeService;
        private UserService UserService;
        private IntegrationService integrationService;
        private ShipmentService shipmentService;


        public async Task<int> AccountingHeadCreate(string headName, int chart2ID)
        {
            int result = -1;
            VMAccountHead vMAccountHead = new VMAccountHead { Accounting_Chart2FK = chart2ID, Name = headName, Editable = false };

            result = await _accountingService.AccountHeadAdd(vMAccountHead);
            //if (a == -1)
            //{
            //    result = _accountingService.AccountHeadIdGet(vMAccountHead);
            //}
            return result;
        }
        public async Task<int> AccountingHeadEdit(int iD, string newHeadName)
        {
            VMAccountHead vMAccountHead = new VMAccountHead { ID = iD, Name = newHeadName };
            return await _accountingService.AccountHeadEdit(vMAccountHead);
        }
        //AccountingHeadCreateandAdjust
        public async Task<int> AccountingHeadCreateandAdjust(string headName, int baseheadId, int baseHeadType, int chart2ID)
        {
            VMAccountHead vMAccountHead = new VMAccountHead { Name = headName, BaseHeadId = baseheadId, HeadType = baseHeadType, Accounting_Chart2FK = chart2ID };
            return await _accountingService.AccountHeadAdd(vMAccountHead);
        }

        public async Task<int> AccountingHeadAdjust(int iD, int BaseHeadId, int HeadType)
        {
            VMAccountHead vMAccountHead = new VMAccountHead { ID = iD, BaseHeadId = BaseHeadId, HeadType = HeadType };
            return await _accountingService.AccountHeadAdjust(vMAccountHead);
        }
        
        /// <summary>
        /// Journal Master Push
        /// </summary>
        /// <param name="vMJournal"></param>
        /// <returns></returns>
        public async Task<int> JournalMasterPush(VMJournal vMJournal)
        {
            return await _accountingService.JournalAdd(vMJournal);
        }
        /// <summary>
        /// Journal Slave Push
        /// </summary>
        /// <param name="vMJournalSlave"></param>
        /// <returns></returns>
        public async Task<int> JournalSlavePush(VMJournalSlave vMJournalSlave)
        {
            return await _accountingService.JournalDetailsAdd(vMJournalSlave);
        }
        public async Task<int> AccountingJournalPush(int drHeadID, int crHeadID, decimal amount, decimal dollarAmmount, decimal ConversionRate, string integrationType, int accountingCostCenterFk)
        {
            int result = -1;
            VMJournalSlave vMJournalSlave = new VMJournalSlave
            {
                JournalType = (int)JournalEnum.IntegrationVoucher
            };
            //vMJournalSlave.Title = vMJournalSlave.Description;
            var integrationTypes = integrationType.Split(' ', 2);
            vMJournalSlave.Title = "Integrated Journal: " + integrationTypes[0];
            vMJournalSlave.Description = integrationTypes[1];
            vMJournalSlave.Date = DateTime.Now;
            vMJournalSlave.Finalized = true;
            vMJournalSlave.Approved = true;
            vMJournalSlave.Accounting_CostCenterFK = accountingCostCenterFk;
            vMJournalSlave.DataListSlave = new List<VMJournalSlave>();
            vMJournalSlave.DataListSlave.Add(new VMJournalSlave
            {
                Description = vMJournalSlave.Description,
                Debit = amount,
                Credit = 0,
                Accounting_HeadFK = drHeadID,
                ConversionRateToBDT = ConversionRate,
                ForeignCurrencyAmount = dollarAmmount
            });
            vMJournalSlave.DataListSlave.Add(new VMJournalSlave
            {
                Description = vMJournalSlave.Description,
                Debit = 0,
                Credit = amount,
                Accounting_HeadFK = crHeadID,
                ConversionRateToBDT = ConversionRate,
                ForeignCurrencyAmount = dollarAmmount
            });
            result = await _accountingService.AccountingJournalMasterPush(vMJournalSlave);
            return result;
        }
        //int fromStoreId, int toStoreID, decimal amount, string description, int InventoryType
        public async Task<int> InventroyPush(VmInventory vM, InventoryTransactionTriggerTypeEnum  triggerEnum,int drHeadID, int crHeadID, decimal amount, decimal dollarAmmount, decimal ConversionRate, string integrationType, int accountingCostCenterFk)
        {
            int result = -1;
            switch (triggerEnum)
            {
                case InventoryTransactionTriggerTypeEnum.PurchaseInvoice:
                    result = await _inventoryService.InventoryDataPush(vM);
                    result = await AccountingJournalPush(drHeadID,crHeadID,amount, dollarAmmount, ConversionRate, integrationType,accountingCostCenterFk);
                    break;
                case InventoryTransactionTriggerTypeEnum.StoreRequisition:
                    result = await _inventoryService.InventoryDataPush(vM);
                    if (crHeadID != drHeadID)
                    {
                        result = await AccountingJournalPush(drHeadID, crHeadID, amount, dollarAmmount, ConversionRate, integrationType, accountingCostCenterFk);
                    }
                    break;
                case InventoryTransactionTriggerTypeEnum.StoreStockIn:
                    result = await _inventoryService.InventoryDataPush(vM);
                    result = await  AccountingJournalPush(drHeadID, crHeadID, amount, dollarAmmount, ConversionRate, integrationType, accountingCostCenterFk);
                    break;
            }
            return result;
        }

        public decimal getHeadWiseTrialBalance(int headId, DateTime fromdate, DateTime todate)
        {
            decimal result = _accountingService.TrialBalanceGetByDateHeadWise(headId, fromdate, todate);
            return result;
        }
        public IEnumerable<VMLedger> GetJournalHistory()
        {
            return _accountingService.GetJournalHistory();
        }
        public async Task<VMOrder> OrderGetNotInLC()
        {
            merchandisingService = new MerchandisingService(_db,null);
            return await merchandisingService.OrderGet();
        }
        public async Task<VMPurchaseOrder> PurchaseOrderGetNotInLC()
        {
            procurementService = new ProcurementService(_db, _httpContext);
            return await procurementService.PurchaseOrderGet();
        }

        public async Task<VMSupplier> GetSupplierList()
        {
            
            return await _homeService.GetSupplier();
        }
        public async Task<VMSupplier> GetBBLCWiseSupplierList()
        {
            commercialService = new CommercialService(_db, _httpContext);
            return await commercialService.GetBBLCSuppliers();
        }
        public async Task<VmBBLC> GetBBLCSupplierWise(int id)
        {
            commercialService = new CommercialService(_db, _httpContext);
            return await commercialService.GetBBLCSupplierWise(id); 
        }
        public async Task<VMCommercial_BBLCPaymentInformation> BBLCPaymentInformationGet(int id)
        {
            commercialService = new CommercialService(_db, _httpContext);
            return await commercialService.BBLCPaymentInformationGet(id); 
        }
        public async Task<int> BBLCPaymentDocument(VMCommercial_BBLCPaymentInformation vmBBLCPaymentInfo)
        {
            commercialService = new CommercialService(_db, _httpContext);
            return await commercialService.CommercialBBLCPaymentInformationAdd(vmBBLCPaymentInfo);
        }
        public VMStyle StyleGetByID(int ID)
        {
            merchandisingService = new MerchandisingService(_db, _httpContext);
            var model = merchandisingService.StyleById(ID);
            return model;
        }

        public List<object> DDLBOMStyleList()
        {
            merchandisingService = new MerchandisingService(_db, _httpContext);
            var StyleList = new List<object>();
            StyleList = merchandisingService.DDLBOMStyleList();
            return StyleList;
        }

        public List<VMBOM> BOMGetByStyleID(int StyleID, int TypeID = 0)
        {
            merchandisingService = new MerchandisingService(_db, _httpContext);
            return merchandisingService.PRItemGetByStyleID(StyleID, TypeID);
        }

        #region Message 
        public void GetMessage(string messageStyle, string message, bool dismissable)
        {
            //messageService = new MessageService(_messageService);
            switch (messageStyle)
            {
                case MessageStyle.Information:
                    _messageService.Information(message, dismissable);
                    break;
                case MessageStyle.Success:
                    _messageService.Success(message, dismissable);
                    break;
                case MessageStyle.Warning:
                    _messageService.Warning(message, dismissable);
                    break;
                case MessageStyle.Danger:
                    _messageService.Danger(message, dismissable);
                    break;
                default:
                    _messageService.NoMessage(message, dismissable);
                    break;
            }
        }
        #endregion Message

        public List<object> GetStyleName()
        {
            merchandisingService = new MerchandisingService(_db, _httpContext);
            return merchandisingService.StyleDropDownList();
        }

        public VMStyle GetAllStyle()
        {
            merchandisingService = new MerchandisingService(_db, _httpContext);
            return merchandisingService.GetAllStyle();
        }

        public List<object> OrderCategoryDropDownList()
        {
            merchandisingService = new MerchandisingService(_db, _httpContext);
            return merchandisingService.OrderCategoryDropDownList();
            //ViewBag.OrderCategoryList = new SelectList(_service.OrderCategoryDropDownList(), "Value", "Text");
            //ViewBag.OrderSubCategoryList = new SelectList(_service.OrderSubCategoryByItemIDDropDownList(model.vmStyle.Mkt_ItemFK), "Value", "Text");
            //ViewBag.OrderItemList = new SelectList(_service.OrderItemByItemIDDropDownList(model.vmStyle.Mkt_ItemFK), "Value", "Text");

        }
        //
       

        public List<object> OrderSubCategoryDropDownList(int id)
        {
            merchandisingService = new MerchandisingService(_db, _httpContext);
            return merchandisingService.FinishItemBySubCategoryIDDropDownList(id);
        }
        public List<object> OrderRawItemInfoGet(int id)
        {
            merchandisingService = new MerchandisingService(_db, _httpContext);
            return merchandisingService.OrderItemByItemIDDropDownList(id);
        }

        public List<VMHoliday> GetCompanyWeekend()
        {
            HRMSService = new HRMSService(_db);
            return HRMSService.GetCompanyWeekend();
        }

        public List<VMHoliday> GetAllHoliday(DateTime From, DateTime To)
        {
            HRMSService = new HRMSService(_db);
            return HRMSService.GetAllHoliday(From, To);
        }

        public List<object> SrStyleDropDownList()
        {
            storeService = new StoreService(_db, _httpContext);
            return storeService.SRStyleDropDownList();
        }

        public int ItemPoValueListStyleWise(int styleId, int itemId)
        {
            merchandisingService = new MerchandisingService(_db, _httpContext);
            return merchandisingService.ItemPoValueListStyleWise(styleId, itemId);
        }

        public List<object> FinishCategoryDropDownList()
        {
            return _homeService.FinishCategoryDropDownList();
        }
        public List<object> FinishSubCategoryDropDownList()
        {
            return _homeService.FinishSubCategoryDropDownList();
        }
        public List<object> FinishSubCategoryDropDownCascading(int id)
        {
            return _homeService.FinishSubCategoryDropDownCascading(id);
        }
        public List<object> FinishItemDropDownList()
        {
            return _homeService.FinishItemDropDownList();
        }
        public List<object> FinishItemDropDownListCascading(int id)
        {
            return _homeService.FinishItemDropDownListCascading(id);
        }

        public List<object> RawItemDropDownList()
        {
            storeService = new StoreService(_db, _httpContext);
            return storeService.ProItemDropDownList();
        }
        
        public List<object> RawCategoryDropDownList()
        {
            return _homeService.Raw_CategoryDropDownList();
        }
        public List<object> RawSubCategoryDropDownList()
        {
            return _homeService.Raw_SubCategoryDropDownList();
        }
        public List<object> RawSubCategoryDropDowncascading(int id)
        {
            return _homeService.RawSubCategoryDropDowncascading(id);
        }
        public List<object> RawItemDropDownList(int id)
        {
            return _homeService.GetRawItemList();
        }
        public List<object> RawItemDropDownListSpecific(int id)
        {
            return _homeService.GetSpecificRawItemList(id);
        }
        //Raw_CategoryDropDownList
        //Raw_SubCategoryDropDownList
        //RawSubCategoryDropDowncascading

        public async Task<SelectList> BusinessUnitDropDownListAsync()
        {
            HRMSService = new HRMSService(_db);
            return await HRMSService.BusinessUnitDropDownListAsync();
        }

        public async Task<SelectList> UnitDropDownListAsync()
        {
            HRMSService = new HRMSService(_db);
            return await HRMSService.UnitDropDownListAsync();
        }

        public async Task<List<HRMSUnitVM>> UnitListGetByBUIdAsync(int id = 0)
        {
            HRMSService = new HRMSService(_db);
            return await HRMSService.UnitListGetByBUIdAsync(id);
        }
        public async Task<SelectList> DepartmentDropDownListAsync(int? id = 0)
        {
            UserService = new UserService(_db);
            return await UserService.DepartmentDropDownListAsync(id);
        }
        public async Task<List<VMUserDepartment>> DepartmentListGetByUIdAsync(int id = 0)
        {
            UserService = new UserService(_db);
            return await UserService.DepartmentListGetByUIdAsync(id);
        }

        public async Task<List<VMPurchaseOrder>> POListGetForAccounts(DateTime FromDate, DateTime ToDate, int nSupplierId = 0)
        {
            procurementService = new ProcurementService(_db, _httpContext);
            return await procurementService.POListGetForAccounts(FromDate, ToDate, nSupplierId);
        }
        /// <summary>
        /// update PO status from GoddsReceived. PartialGoodsReceived(id=11),GoodsReceived(id=7)
        /// </summary>
        /// <param name="POId">PO Header Table ID</param>
        /// <param name="id">StatusEnum ID</param>
        /// <returns></returns>
        /// 
        public async Task<int> POGoodsReceivedStatusUpdateFromSIn(int POId, int id = 0)
        {
            procurementService = new ProcurementService(_db, _httpContext);
            return await procurementService.POGoodsReceivedStatusUpdateFromSIn(POId, id);
        }

        /// <summary>
        /// update PI status from Accounts. PartialPaid(id=6),FullPaid(id=7)
        /// </summary>
        /// <param name="PIId">PI Header Table ID</param>
        /// <param name="id">StatusEnum ID</param>
        /// <returns></returns>
        public async Task<int> PurchaseInvoicePaidStatusUpdateFromAcc(int PIId, int id = 0)
        {
            procurementService = new ProcurementService(_db, _httpContext);
            return await procurementService.PurchaseInvoicePaidStatusUpdateFromAcc(PIId, id);
        }
        public async Task<decimal> GetRawItemUnitPriceBy(int itemIdfk, double receivedQty, int poSlaveId)
        {
            procurementService = new ProcurementService(_db, _httpContext);
            return await procurementService.POSlaveGetSpecificPrice(new VMPurchaseOrderSlave() { ID = poSlaveId });
        }

        public IEnumerable<VMShipmentBillOfExchange> ShipmentBillOfExchangesGet()
        {
            shipmentService = new ShipmentService(_db, _httpContext);
            return  shipmentService.ShipmentBillOfExchangesGet();
        }
        public async Task<VMShipmentBillOfExchangeSlave> ShipmentBillOfExchangeGet(int id)
        {
            shipmentService = new ShipmentService(_db, _httpContext);
            return await shipmentService.ShipmentBillOfExchangeGet(id);
        }
        public async Task<VMShipmentBillOfExchange> ShipmentBillOfExchangeGetSingle(int id=0)
        {
            shipmentService = new ShipmentService(_db, _httpContext);
            return await shipmentService.ShipmentBillOfExchangesGetSingle(id);
        }

        public async Task<VMFinancialPosition> FinancialPositionGetByDateForMis(int id = 0)
        {
            var frmdt = "2000-01-01";
            var fromDate = Convert.ToDateTime(frmdt);           
            return await Task.Run(() => _accountingService.FinancialPositionGetByDate(fromDate, DateTime.Today.Date));
        }
        public async Task<VMIncomeStatement> ComprehensiveIncomeSearchForMis(int id = 0)
        {
            var frmdt = "2000-01-01";
            var fromDate = Convert.ToDateTime(frmdt);
            return await Task.Run(() => _accountingService.ComprehensiveIncomeGetByDate(fromDate, DateTime.Today.Date));

        }
        #region Production
        public List<VMDailyProduction> GetTotalDoneQtyBySectionColorSizeWise(int styleid,int sectionid)
        {
            productionService = new ProductionService(_db,_httpContext);
            var vData = productionService.GetTotalDoneQtyBySectionColorSizeWise(styleid, sectionid);
            return vData;
        }

        #endregion
    }
}
