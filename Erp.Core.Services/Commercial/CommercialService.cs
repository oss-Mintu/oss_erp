﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Erp.Core.Entity.Merchandising;
using Erp.Core.Entity.Procurement;
using Erp.Core.Services.Home;
using Erp.Core.Services.Integration;
using Erp.Core.Services.Merchandising;
using Erp.Core.Services.Procurement;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;


namespace Erp.Core.Services.Commercial
{
    public class CommercialService : BaseService
    {
        private readonly HttpContext _httpContext;

        private readonly IntegrationService integrationService;
        private IntegrationService _integrationService;
        public CommercialService(IErpDbContext db, HttpContext httpContext)
        {
            _db = db;
            integrationService = new IntegrationService(_db, httpContext);
            _integrationService = new IntegrationService(_db, httpContext);
            _httpContext = httpContext;
        }

        public async Task<VMCommercialUDSlave> CommercialUDDetailsGet(int id)
        {

            VMCommercialUDSlave vmCommercialUDSlave = new VMCommercialUDSlave
            {
                VMCommercialUD = await Task.Run(() => CommercialSingleUDGet(id)),

                VmECI = new VmECI
                {
                    DataList = await Task.Run(() => EciDataByUDGet(id))
                },
                VmBBLC = new VmBBLC
                {
                    DataList = await Task.Run(() => GetBBLCDataByUD(id))
                }
            };
            return vmCommercialUDSlave;

        }



        public VMCommercialUD CommercialUDGet()
        {
            VMCommercialUD commercialUD = new VMCommercialUD
            {
                DataList = (from t1 in _db.Commercial_UD
                            join t2 in _db.Common_Buyer on t1.Common_BuyerFk equals t2.ID
                            where t1.Active == true
                            select new VMCommercialUD
                            {
                                ID = t1.ID,
                                CID = t1.CID,
                                Common_BuyerFk = t1.Common_BuyerFk,
                                CommonBuyerName = t2.Name,
                                Date = t1.Date,
                                LimitDeclaration = t1.LimitDeclaration,
                                Ref = t1.Ref,
                                IsActive = t1.Active,
                                UserID = t1.UserID,
                                ScanedFile = t1.ScanedFile,
                                Remarks = t1.Remarks,
                                IsClose = t1.IsClose,

                                VmECIList = (from x in _db.Commercial_ECI
                                             where x.Active == true && x.Commercial_UDFk == t1.ID && x.Common_BuyerFK == t1.Common_BuyerFk
                                             select new VmECI
                                             {
                                                 ID = x.ID,
                                                 ECINo = x.ECINo,
                                                 TotalValue = x.TotalValue,
                                                 IsClose = t1.IsClose

                                             }).OrderByDescending(o => o.ID).ToList(),


                            }).OrderByDescending(x => x.ID).AsEnumerable()
            };

            return commercialUD;
        }
        public VMCommercialUD CommercialSingleUDGet(int id)
        {
            var singleUD = (from t1 in _db.Commercial_UD
                            join t2 in _db.Common_Buyer on t1.Common_BuyerFk equals t2.ID
                            where t1.Active == true && t1.ID == id
                            select new VMCommercialUD
                            {
                                ID = t1.ID,
                                CID = t1.CID,
                                Common_BuyerFk = t1.Common_BuyerFk,
                                CommonBuyerName = t2.Name,
                                Date = t1.Date,
                                LimitDeclaration = t1.LimitDeclaration,
                                Ref = t1.Ref,
                                IsActive = t1.Active,
                                UserID = t1.UserID,
                                ScanedFile = t1.ScanedFile,
                                Remarks = t1.Remarks,
                                IsClose = t1.IsClose,
                                TotalECIValue = (from x in _db.Commercial_ECI
                                                 where x.Active == true && x.Commercial_UDFk == t1.ID
                                                 select decimal.Add(x.TotalValue, (from xx in _db.Commercial_ECIValueAmendment where xx.Active && xx.Commercial_ECIFk == x.ID select xx.ValueAmendment).DefaultIfEmpty(0).Sum())).DefaultIfEmpty(0).Sum(),//, x.ValueAmendment


                                TotalBPOValue = (from x in _db.Merchandising_BuyerOrder //Buyer PO
                                                 join y in _db.Commercial_ECI on x.Commercial_ECIFk equals y.ID
                                                 join z in _db.Commercial_UD on y.Commercial_UDFk equals z.ID
                                                 where x.Active == true && y.Active == true && z.Active == true
                                                 && z.ID == t1.ID && x.Common_BuyerFK == t1.Common_BuyerFk
                                                 select x.BuyerOrderPOValue).DefaultIfEmpty(0).Sum(),

                                BTBDocumentLimitValue = (from x in _db.Merchandising_BuyerOrder //Buyer PO
                                                         join y in _db.Commercial_ECI on x.Commercial_ECIFk equals y.ID
                                                         join z in _db.Commercial_UD on y.Commercial_UDFk equals z.ID
                                                         where x.Active == true && y.Active == true && z.Active == true
                                                         && z.ID == t1.ID
                                                         select decimal.Multiply(decimal.Divide(x.BuyerOrderPOValue, 100), t1.LimitDeclaration)).DefaultIfEmpty(0).Sum(),
                                BTBDocumentValue = (from x in _db.Commercial_BBLC
                                                    where x.Active == true && x.Commercial_UDFk == t1.ID
                                                    select decimal.Add(x.BBValue, (from xx in _db.Commercial_BBLCValueAmendment where xx.Active && xx.Commercial_BBLCFk == x.ID select xx.ValueAmendment).DefaultIfEmpty(0).Sum())).DefaultIfEmpty(0).Sum(),
                                UtilizedValue = (from x in _db.Procurement_PurchaseOrder
                                                 join y in _db.Commercial_BBLC on x.Commercial_BBLCFk equals y.ID
                                                 where x.Active == true && y.Active == true && y.Commercial_UDFk == t1.ID
                                                 select x.TotalPOValue).DefaultIfEmpty(0).Sum(),


                                VmECIList = (from x in _db.Commercial_ECI
                                             where x.Active == true && x.Commercial_UDFk == t1.ID && x.Common_BuyerFK == t1.Common_BuyerFk
                                             select new VmECI
                                             {
                                                 ID = x.ID,
                                                 ECINo = x.ECINo,
                                                 TotalValue = decimal.Add(x.TotalValue, (from xx in _db.Commercial_ECIValueAmendment where xx.Active && xx.Commercial_ECIFk == x.ID select xx.ValueAmendment).DefaultIfEmpty(0).Sum()),
                                                 IsClose = x.IsClose,
                                             }).OrderByDescending(o => o.ID).ToList(),

                            }).OrderByDescending(x => x.ID).FirstOrDefault();


            return singleUD;
        }


        public async Task<int> CommercialUDAdd(VMCommercialUD vmCommercialUD)
        {

            #region Genarate CID
            int totalUD = _db.Commercial_UD.Count();

            if (totalUD == 0)
            {
                totalUD = 1;
            }
            else
            {
                totalUD++;
            }
            var newString = totalUD.ToString().PadLeft(5, '0');
            string genaratedCID = "UD" + DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + newString;
            #endregion

            var result = -1;
            var commUd = new Commercial_UD
            {
                CID = genaratedCID,
                Ref = vmCommercialUD.Ref,
                Date = vmCommercialUD.Date,
                LimitDeclaration = vmCommercialUD.LimitDeclaration,
                Common_BuyerFk = vmCommercialUD.Common_BuyerFk,
                User = vmCommercialUD.User,
                UserID = vmCommercialUD.UserID,
                Remarks = vmCommercialUD.Remarks,
                ScanedFile = vmCommercialUD.ScanedFile

            };
            _db.Commercial_UD.Add(commUd);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = commUd.ID;
            }
            return result;
        }

        public async Task<int> CommercialBBLCPaymentInformationAdd(VMCommercial_BBLCPaymentInformation vmBBLCPaymentInfo)
        {


            var result = -1;
            var commercialBBLCPaymentInformation = new Commercial_BBLCPaymentInformation
            {
                Commercial_BBLCFk = vmBBLCPaymentInfo.VmBBLC.ID,
                Date = DateTime.Now,
                B2bLCPayment = vmBBLCPaymentInfo.B2bLCPayment,
                UserID = vmBBLCPaymentInfo.UserID,

            };
            var bb = (from cbblc in _db.Commercial_BBLC
                      join s in _db.Common_Supplier on cbblc.Common_SupplierFK equals s.ID into s_Join
                      from s in s_Join.DefaultIfEmpty()
                      join c in _db.Common_Currency on cbblc.Common_CurrencyFK equals c.ID into c_Join
                      from c in c_Join.DefaultIfEmpty()
                      where cbblc.ID == vmBBLCPaymentInfo.VmBBLC.ID
                      select new
                      {
                          supplierAccID = s.AccHeadID,
                          currencyConversionValue = c.ConversionRateToBDT,
                          BB_No = cbblc.BBNo,
                          BBLCCID = cbblc.CID,
                      }).FirstOrDefault();

            _db.Commercial_BBLCPaymentInformation.Add(commercialBBLCPaymentInformation);
            if (await _db.SaveChangesAsync() > 0)
            {
                decimal ConvertedAmount = Convert.ToDecimal(vmBBLCPaymentInfo.B2bLCPayment * bb.currencyConversionValue);
                string BBLCref = "BBLC - " + "BB No : " + bb.BB_No + "BB CID" + bb.BBLCCID;
                await _integrationService.AccountingJournalPush(bb.supplierAccID, vmBBLCPaymentInfo.Accounting_HeadFK, ConvertedAmount, vmBBLCPaymentInfo.B2bLCPayment, bb.currencyConversionValue, BBLCref, (int)CostCenterEnum.General);
                result = commercialBBLCPaymentInformation.ID;

            }
            return result;
        }



        public async Task<int> CommercialUDEdit(VMCommercialUD vmCommercialUD)
        {
            var result = -1;
            var commercialUD = _db.Commercial_UD.Find(vmCommercialUD.ID);
            commercialUD.Ref = vmCommercialUD.Ref;
            commercialUD.Date = vmCommercialUD.Date;
            commercialUD.LimitDeclaration = vmCommercialUD.LimitDeclaration;
            commercialUD.Common_BuyerFk = vmCommercialUD.Common_BuyerFk;
            commercialUD.User = vmCommercialUD.User;
            commercialUD.UserID = vmCommercialUD.UserID;
            commercialUD.Remarks = vmCommercialUD.Remarks;
            commercialUD.ScanedFile = vmCommercialUD.ScanedFile;

            if (await _db.SaveChangesAsync() > 0)
            {
                result = commercialUD.ID;
            }
            return result;
        }
        public async Task<int> CommercialUDDelete(int id)
        {
            var result = -1;
            if (id == 0) return result;
            var commUd = _db.Commercial_UD.Find(id);
            commUd.Active = false;
            if (await _db.SaveChangesAsync() > 0)
            {
                result = commUd.ID;
            }
            return result;
        }

        public async Task<int> CommercialUDClose(int id)
        {
            var commUdID = -1;
            List<Commercial_ECI> commercialECIList = new List<Commercial_ECI>();
            List<Commercial_BBLC> commercialBBLCList = new List<Commercial_BBLC>();

            if (id == 0) return commUdID;
            var commUd = _db.Commercial_UD.Find(id);
            commUd.IsClose = true;
            if (await _db.SaveChangesAsync() > 0)
            {
                commUdID = commUd.ID;
            }
            if (commUdID > 0)
            {
                commercialECIList = _db.Commercial_ECI.Where(x => x.Commercial_UDFk == commUdID && x.Active == true).ToList();
                commercialECIList.ForEach(x => x.IsClose = true);
                await _db.SaveChangesAsync();

                commercialBBLCList = _db.Commercial_BBLC.Where(x => x.Commercial_UDFk == commUdID && x.Active == true).ToList();
                commercialBBLCList.ForEach(x => x.IsClose = true);
                await _db.SaveChangesAsync();
            }

            return commUdID;
        }
        public async Task<int> CommercialUDReOpen(int id)
        {
            var commUdID = -1;
            List<Commercial_ECI> commercialECIList = new List<Commercial_ECI>();
            List<Commercial_BBLC> commercialBBLCList = new List<Commercial_BBLC>();

            if (id == 0) return commUdID;
            var commUd = _db.Commercial_UD.Find(id);
            commUd.IsClose = false;
            if (await _db.SaveChangesAsync() > 0)
            {
                commUdID = commUd.ID;
            }
            if (commUdID > 0)
            {
                commercialECIList = _db.Commercial_ECI.Where(x => x.Commercial_UDFk == commUdID && x.Active == true).ToList();
                commercialECIList.ForEach(x => x.IsClose = false);
                await _db.SaveChangesAsync();

                commercialBBLCList = _db.Commercial_BBLC.Where(x => x.Commercial_UDFk == commUdID && x.Active == true).ToList();
                commercialBBLCList.ForEach(x => x.IsClose = false);
                await _db.SaveChangesAsync();

            }

            return commUdID;
        }
        public async Task<VmECI> GetECI(int id)
        {
            return new VmECI { DataList = await Task.Run(() => GetEciData(id)) };
        }
        private IEnumerable<VmECI> GetEciData(int id)
        {
            var v = (from t1 in _db.Commercial_ECI
                     join t2 in _db.Common_BuyerNotifyParty on t1.Common_BuyerNotifyPartyFk equals t2.ID
                     join t3 in _db.Common_LcType on t1.Common_ECITypeFK equals t3.ID

                     join t4 in _db.Common_BuyerBank on t1.Common_BuyerBankFK equals t4.ID
                     join t4s in _db.Common_Bank on t4.Common_BankFK equals t4s.ID

                     join t5 in _db.Common_LienBank on t1.Common_LienBankFK equals t5.ID
                     join t5s in _db.Common_Bank on t5.Common_BankFK equals t5s.ID

                     join t6 in _db.Common_CompanyBank on t1.Common_CompanyBankFK equals t6.ID
                     join t6s in _db.Common_Bank on t6.Common_BankFK equals t6s.ID

                     join t7 in _db.Common_Buyer on t1.Common_BuyerFK equals t7.ID
                     where t1.Active == true
                     select new VmECI
                     {
                         CID = t1.CID,
                         ID = t1.ID,
                         ECINo = t1.ECINo,
                         ECIDate = t1.ECIDate,
                         Common_ECITypeFK = t1.Common_ECITypeFK,
                         Common_BuyerFK = t1.Common_BuyerFK,
                         Common_BuyerBankFK = t1.Common_BuyerBankFK,
                         Common_LienBankFK = t1.Common_LienBankFK,
                         Common_CompanyBankFK = t1.Common_CompanyBankFK,
                         Common_CurrencyFK = t1.Common_CurrencyFK,
                         VMECIValueAmendmentList = (from x in _db.Commercial_ECIValueAmendment
                                                    where x.Active == true && x.Commercial_ECIFk == t1.ID
                                                    select new VMECIValueAmendment
                                                    {
                                                        ID = x.ID,
                                                        ValueAmendment = x.ValueAmendment,
                                                        Commercial_ECIFk = x.Commercial_ECIFk,
                                                        Time = x.Time

                                                    }).OrderByDescending(o => o.ID).ToList(),
                         VMOrderList = (from x in _db.Merchandising_BuyerOrder
                                        where x.Active == true && x.Commercial_ECIFk == t1.ID && x.Common_BuyerFK == t7.ID
                                        select new VMOrder
                                        {
                                            ID = x.ID,
                                            CID = x.BuyerPO + "/" + x.CID,
                                            BuyerPOValue = x.BuyerOrderPOValue,
                                            TotalOrderPackQuntity = _db.Merchandising_Style.Where(xs => xs.Merchandising_BuyerOrderFK == x.ID && xs.Active == true).Select(xs => xs.PackQty).DefaultIfEmpty(0).Sum()
                                        }).OrderByDescending(o => o.ID).ToList(),
                         Common_UDNo = t1.Commercial_UDFk > 0 ? (from o in _db.Commercial_UD where o.Active == true && o.ID == t1.Commercial_UDFk select o.Ref).FirstOrDefault() : "Not set yet",
                         Common_ECIType = t3.Name,
                         Common_Applicant = t7.Name,
                         Common_BuyerBank = t4.Name + ", " + t4s.Name,
                         Common_LienBank = t5.Name + ", " + t5s.Name,
                         Common_CompanyBank = t6.Name + ", " + t6s.Name,
                         BuyerNotifyParty = t2.Name,
                         TotalValue = t1.TotalValue,
                         CurrentTotalValue = t1.TotalValue + (from xx in _db.Commercial_ECIValueAmendment where xx.Active && xx.Commercial_ECIFk == t1.ID select xx.ValueAmendment).DefaultIfEmpty(0).Sum(),
                         Tolerance = t1.Tolerance,
                         ShipmentDate = t1.ShipmentDate,
                         ExpiryDate = t1.ExpiryDate,
                         Destination = t1.Destination,
                         UserID = t1.UserID,
                         ScanedFile = t1.ScanedFile,
                         ValueAmendment = (from xx in _db.Commercial_ECIValueAmendment where xx.Active && xx.Commercial_ECIFk == t1.ID select xx.ValueAmendment).DefaultIfEmpty(0).Sum(),
                         Remarks = t1.Remarks,
                         Commercial_UDFk = t1.Commercial_UDFk,
                         Common_BuyerNotifyPartyFk = t1.Common_BuyerNotifyPartyFk,
                         IsClose = t1.IsClose

                     }).OrderByDescending(x => x.ID).AsEnumerable();
            if (id > 0)
            {
                v = v.Where(x => x.ID == id).AsEnumerable();
            }
            return v;
        }
        private IEnumerable<VmECI> EciDataByUDGet(int id)
        {
            var v = (from t1 in _db.Commercial_ECI
                     join t2 in _db.Common_BuyerNotifyParty on t1.Common_BuyerNotifyPartyFk equals t2.ID
                     join t3 in _db.Common_LcType on t1.Common_ECITypeFK equals t3.ID
                     join t4 in _db.Common_BuyerBank on t1.Common_BuyerBankFK equals t4.ID
                     join t5 in _db.Common_LienBank on t1.Common_LienBankFK equals t5.ID
                     join t6 in _db.Common_CompanyBank on t1.Common_CompanyBankFK equals t6.ID
                     join t7 in _db.Common_Buyer on t1.Common_BuyerFK equals t7.ID
                     where t1.Active == true && t1.Commercial_UDFk == id
                     select new VmECI
                     {
                         CID = t1.CID,
                         ID = t1.ID,
                         ECINo = t1.ECINo,
                         ECIDate = t1.ECIDate,
                         Common_ECITypeFK = t1.Common_ECITypeFK,
                         Common_BuyerFK = t1.Common_BuyerFK,
                         Common_BuyerBankFK = t1.Common_BuyerBankFK,
                         Common_LienBankFK = t1.Common_LienBankFK,
                         Common_CompanyBankFK = t1.Common_CompanyBankFK,
                         Common_CurrencyFK = t1.Common_CurrencyFK,
                         VMECIValueAmendmentList = (from x in _db.Commercial_ECIValueAmendment
                                                    where x.Active == true && x.Commercial_ECIFk == t1.ID
                                                    select new VMECIValueAmendment
                                                    {
                                                        ID = x.ID,
                                                        ValueAmendment = x.ValueAmendment,
                                                        Commercial_ECIFk = x.Commercial_ECIFk,
                                                        Time = x.Time

                                                    }).OrderByDescending(o => o.ID).ToList(),
                         VMOrderList = (from x in _db.Merchandising_BuyerOrder
                                        where x.Active == true && x.Commercial_ECIFk == t1.ID && x.Common_BuyerFK == t7.ID
                                        select new VMOrder
                                        {
                                            ID = x.ID,
                                            CID = x.BuyerPO + "/" + x.CID,
                                            BuyerPOValue = x.BuyerOrderPOValue,
                                            TotalOrderPackQuntity = _db.Merchandising_Style.Where(xs => xs.Merchandising_BuyerOrderFK == x.ID && xs.Active == true).Select(xs => xs.PackQty).DefaultIfEmpty(0).Sum()
                                        }).OrderByDescending(o => o.ID).ToList(),
                         Common_UDNo = t1.Commercial_UDFk > 0 ? (from o in _db.Commercial_UD where o.Active == true && o.ID == t1.Commercial_UDFk select o.Ref).FirstOrDefault() : "Not set yet",
                         TotalValuePO = _db.Merchandising_BuyerOrder.Where(x => x.Active == true && x.Commercial_ECIFk == t1.ID).Select(x => x.BuyerOrderPOValue).DefaultIfEmpty(0).Sum(),
                         Common_ECIType = t3.Name,
                         Common_Applicant = t7.Name,
                         Common_BuyerBank = t4.Name,
                         Common_LienBank = t5.Name,
                         Common_CompanyBank = t6.Name,
                         BuyerNotifyParty = t2.Name,
                         TotalValue = t1.TotalValue,
                         CurrentTotalValue = t1.TotalValue + (from xx in _db.Commercial_ECIValueAmendment where xx.Active && xx.Commercial_ECIFk == t1.ID select xx.ValueAmendment).DefaultIfEmpty(0).Sum(),
                         Tolerance = t1.Tolerance,
                         ShipmentDate = t1.ShipmentDate,
                         ExpiryDate = t1.ExpiryDate,
                         Destination = t1.Destination,
                         UserID = t1.UserID,
                         ScanedFile = t1.ScanedFile,
                         ValueAmendment = (from xx in _db.Commercial_ECIValueAmendment where xx.Active && xx.Commercial_ECIFk == t1.ID select xx.ValueAmendment).DefaultIfEmpty(0).Sum(),
                         Remarks = t1.Remarks,
                         Commercial_UDFk = t1.Commercial_UDFk,
                         Common_BuyerNotifyPartyFk = t1.Common_BuyerNotifyPartyFk,
                         IsClose = t1.IsClose
                     }).OrderByDescending(x => x.ID).AsEnumerable();
            return v;
        }

        public async Task<int> ECIAdd(VmECI vmECI)
        {
            #region Genarate CID
            int totalECI = _db.Commercial_ECI.Count();

            if (totalECI == 0)
            {
                totalECI = 1;
            }
            else
            {
                totalECI++;
            }
            var newString = totalECI.ToString().PadLeft(5, '0');
            string genaratedCID = "ECI" + DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + newString;
            #endregion
            vmECI.Common_Applicant = _db.Common_Buyer.Find(vmECI.Common_BuyerFK).Name;
            var result = -1;
            int baseHeadId = -1;

            var commEci = new Commercial_ECI
            {
                CID = genaratedCID,
                ECINo = vmECI.ECINo,
                ECIDate = vmECI.ECIDate,
                Common_ECITypeFK = vmECI.Common_ECITypeFK,
                Common_BuyerFK = vmECI.Common_BuyerFK,
                Common_BuyerBankFK = vmECI.Common_BuyerBankFK,
                Common_LienBankFK = vmECI.Common_LienBankFK,
                Common_CompanyBankFK = vmECI.Common_CompanyBankFK,
                Commercial_UDFk = vmECI.Commercial_UDFk,
                Common_CurrencyFK = vmECI.Common_CurrencyFK,
                TotalValue = vmECI.TotalValue,
                Tolerance = vmECI.Tolerance,
                ShipmentDate = vmECI.ShipmentDate,
                ExpiryDate = vmECI.ExpiryDate,
                Destination = vmECI.Destination,
                Common_BuyerNotifyPartyFk = vmECI.Common_BuyerNotifyPartyFk,
                Remarks = vmECI.Remarks,
                ScanedFile = vmECI.ScanedFile,
                UserID = vmECI.UserID,
                User = vmECI.UserID.ToString(),

            };
            _db.Commercial_ECI.Add(commEci);
            if (await _db.SaveChangesAsync() > 0)
            {
                //Accounting Head Adjust
                baseHeadId = await integrationService.AccountingHeadCreate("ECI No: " + commEci.ECINo + ". Date: " + commEci.ECIDate.ToShortDateString(), 1306);
                //var eeiresult = await integrationService.AccountingHeadCreateandAdjust(commEci.ECINo, commEci.ID, (int)IntegratedAccountHeadTypeEnum.ExportLc, (int)IntegratedAccountHeadEnum.ExportLc);
                //result = commEci.ID;
                result = commEci.ID;
            }
            if (commEci.ID > 0 && vmECI.ValueAmendment > 0)
            {
                Commercial_ECIValueAmendment commercialECIValueAmendment = new Commercial_ECIValueAmendment
                {
                    Commercial_ECIFk = commEci.ID,
                    ValueAmendment = vmECI.ValueAmendment,
                    UserID = vmECI.UserID

                };
                _db.Commercial_ECIValueAmendment.Add(commercialECIValueAmendment);
                await _db.SaveChangesAsync();
            }
            //Accounting Head Adjust to ECI
            if (baseHeadId > 0)
            {

                vmECI.ID = result;
                vmECI.BaseHeadId = baseHeadId;
                await ECIEdit(vmECI);
            }

            return result;
        }
        public async Task<int> ECIEdit(VmECI vmECI)
        {
            var result = -1;
            var commEci = await _db.Commercial_ECI.FirstOrDefaultAsync(x => x.ID == vmECI.ID);
            commEci.ECINo = vmECI.ECINo;
            commEci.ECIDate = vmECI.ECIDate;
            commEci.Common_ECITypeFK = vmECI.Common_ECITypeFK;
            commEci.Common_BuyerFK = vmECI.Common_BuyerFK;
            commEci.Common_BuyerBankFK = vmECI.Common_BuyerBankFK;
            commEci.Common_LienBankFK = vmECI.Common_LienBankFK;
            commEci.Common_CompanyBankFK = vmECI.Common_CompanyBankFK;
            commEci.Commercial_UDFk = vmECI.Commercial_UDFk;
            commEci.Common_CurrencyFK = vmECI.Common_CurrencyFK;
            commEci.TotalValue = vmECI.TotalValue;
            commEci.Tolerance = vmECI.Tolerance;
            commEci.ShipmentDate = vmECI.ShipmentDate;
            commEci.ExpiryDate = vmECI.ExpiryDate;
            commEci.Destination = vmECI.Destination;
            commEci.Common_BuyerNotifyPartyFk = vmECI.Common_BuyerNotifyPartyFk;
            commEci.Remarks = vmECI.Remarks;
            commEci.ScanedFile = vmECI.ScanedFile;
            commEci.UserID = vmECI.UserID;
            commEci.User = vmECI.UserID.ToString();
            commEci.BaseHeadId = vmECI.BaseHeadId;
            if (await _db.SaveChangesAsync() > 0)
            {
                result = commEci.ID;
            }
            if (commEci.BaseHeadId >0)
            {
                var eciresult = await integrationService.AccountingHeadEdit(commEci.BaseHeadId, "ECI No: " + commEci.ECINo + ". Date: " + commEci.ECIDate.ToShortDateString());

            }

            if (commEci.ID > 0 && vmECI.ValueAmendment > 0)
            {
                Commercial_ECIValueAmendment commercialECIValueAmendment = new Commercial_ECIValueAmendment
                {
                    Commercial_ECIFk = commEci.ID,
                    ValueAmendment = vmECI.ValueAmendment,
                    UserID = vmECI.UserID

                };
                _db.Commercial_ECIValueAmendment.Add(commercialECIValueAmendment);
                await _db.SaveChangesAsync();
            }
            return result;
        }
        public async Task<int> ECIDelete(int id)
        {
            var result = -1;
            if (id == 0) return result;
            var commEci = _db.Commercial_ECI.Find(id);
            if (commEci == null) return result;
            commEci.Active = false;

            if (await _db.SaveChangesAsync() > 0)
            {
                result = commEci.ID;
            }
            return result;
        }

        public async Task<VmBBLC> GetBBLC(int id)
        {
            var vmBBLC = new VmBBLC
            {
                DataList = await Task.Run(() => GetBBLCData(id))
            };
            return vmBBLC;
        }
        private IEnumerable<VmBBLC> GetBBLCData(int id)
        {
            var v = (from t1 in _db.Commercial_BBLC
                     join t2 in _db.Common_Supplier on t1.Common_SupplierFK equals t2.ID
                     join t3 in _db.Common_LcType on t1.Common_LcTypeFK equals t3.ID
                     join t4 in _db.Common_Currency on t1.Common_CurrencyFK equals t4.ID
                     where t1.Active == true
                     select new VmBBLC
                     {
                         CID = t1.CID,
                         ID = t1.ID,
                         BBNo = t1.BBNo,
                         BBValue = t1.BBValue,
                         Common_LcTypeFK = t1.Common_LcTypeFK,
                         Common_SupplierFK = t1.Common_SupplierFK,
                         Common_CurrencyFK = t1.Common_CurrencyFK,
                         IsClose = t1.IsClose,
                         VMBBLCValueAmendmentList = (from x in _db.Commercial_BBLCValueAmendment
                                                     where x.Active == true && x.Commercial_BBLCFk == t1.ID
                                                     select new VMBBLCValueAmendment
                                                     {
                                                         ID = x.ID,
                                                         ValueAmendment = x.ValueAmendment,
                                                         Commercial_BBLCFk = x.Commercial_BBLCFk,
                                                         Time = x.Time

                                                     }).OrderByDescending(o => o.ID).ToList(),
                         VMPurchaseOrderList = (from x in _db.Procurement_PurchaseOrder
                                                where x.Active == true && x.Commercial_BBLCFk == t1.ID && x.Common_SupplierFK == t2.ID
                                                select new VMPurchaseOrder
                                                {
                                                    ID = x.ID,
                                                    CID = x.CID + "/" + x.OrderDate.ToShortDateString(),
                                                    TotalPOValue = x.TotalPOValue
                                                }).OrderByDescending(o => o.ID).ToList(),
                         Common_UDNo = t1.Commercial_UDFk > 0 ? (from o in _db.Commercial_UD where o.Active == true && o.ID == t1.Commercial_UDFk select o.Ref).FirstOrDefault() : "Not set yet",
                         Common_Supplier = t2.Name,
                         Common_LcType = t3.Name,
                         Common_Currency = t4.Name,
                         BaseHeadId = t1.BaseHeadId,                           
                         ScanedFile = t1.ScanedFile,
                         Status = t1.Status,
                         IsApproved = t1.IsApproved,
                         LCDate = t1.LCDate,
                         Commercial_LCOreginFK = t1.Commercial_LCOreginFK,
                         ValueAmendment = (from xx in _db.Commercial_BBLCValueAmendment where xx.Active && xx.Commercial_BBLCFk == t1.ID select xx.ValueAmendment).DefaultIfEmpty(0).Sum(),
                         UserID = t1.UserID,
                         CurrentTotalValue = t1.BBValue + (from xx in _db.Commercial_BBLCValueAmendment where xx.Active && xx.Commercial_BBLCFk == t1.ID select xx.ValueAmendment).DefaultIfEmpty(0).Sum(),
                         Tolerance = t1.Tolerance,
                         Remarks = t1.Remarks,
                         Commercial_UDFk = t1.Commercial_UDFk
                     }).OrderByDescending(x => x.ID).AsEnumerable();
            if (id > 0)
            {
                v = v.Where(x => x.ID == id);
            }
            return v;
        }
        public async Task<VmBBLC> GetBBLCSupplierWise(int id)
        {
            var vmBBLC = new VmBBLC
            {
                VMSupplier = (from t1 in _db.Common_Supplier
                              where t1.ID == id
                              select new VMSupplier
                              {
                                  ID = t1.ID,
                                  Code = t1.Code,
                                  Name = t1.Name,
                                  VMCommon_CountryID = t1.Common_CountryFK,
                                  VMCommon_CountryName = t1.Common_Country.Name,
                                  IsForeignSupplier = t1.IsForeignSupplier,
                                  SupplierType = (EnumSupplierType)t1.SupplierType,
                                  Email = t1.Email,
                                  Phone = t1.Phone,
                                  Address = t1.Address,
                                  ContactPerson = t1.ContactPerson
                              }).FirstOrDefault(),

                DataList = await Task.Run(() => GetBBLCListSupplierWise(id))
            };
            return vmBBLC;
        }
        private IEnumerable<VmBBLC> GetBBLCListSupplierWise(int id)
        {
            var v = (from t1 in _db.Commercial_BBLC
                     join t2 in _db.Common_Supplier on t1.Common_SupplierFK equals t2.ID
                     join t3 in _db.Common_LcType on t1.Common_LcTypeFK equals t3.ID
                     join t4 in _db.Common_Currency on t1.Common_CurrencyFK equals t4.ID
                     where t1.Active == true && t2.Active == true && t1.Common_SupplierFK == id
                     select new VmBBLC
                     {
                         TotalPaymentValue = _db.Commercial_BBLCPaymentInformation.Where(x => x.Active && x.Commercial_BBLCFk == t1.ID).Select(x => x.B2bLCPayment).DefaultIfEmpty(0).Sum(),
                         CID = t1.CID,
                         ID = t1.ID,
                         BBNo = t1.BBNo,
                         BBValue = t1.BBValue,
                         Common_LcTypeFK = t1.Common_LcTypeFK,
                         Common_SupplierFK = t1.Common_SupplierFK,
                         Common_CurrencyFK = t1.Common_CurrencyFK,
                         IsClose = t1.IsClose,
                         VMBBLCValueAmendmentList = (from x in _db.Commercial_BBLCValueAmendment
                                                     where x.Active == true && x.Commercial_BBLCFk == t1.ID
                                                     select new VMBBLCValueAmendment
                                                     {
                                                         ID = x.ID,
                                                         ValueAmendment = x.ValueAmendment,
                                                         Commercial_BBLCFk = x.Commercial_BBLCFk,
                                                         Time = x.Time

                                                     }).OrderByDescending(o => o.ID).ToList(),
                         VMPurchaseOrderList = (from x in _db.Procurement_PurchaseOrder
                                                where x.Active == true && x.Commercial_BBLCFk == t1.ID && x.Common_SupplierFK == t2.ID
                                                select new VMPurchaseOrder
                                                {
                                                    ID = x.ID,
                                                    CID = x.CID + "/" + x.OrderDate.ToShortDateString(),
                                                    TotalPOValue = x.TotalPOValue
                                                }).OrderByDescending(o => o.ID).ToList(),
                         Common_UDNo = t1.Commercial_UDFk > 0 ? (from o in _db.Commercial_UD where o.Active == true && o.ID == t1.Commercial_UDFk select o.Ref).FirstOrDefault() : "Not set yet",
                         Common_Supplier = t2.Name,
                         Common_LcType = t3.Name,
                         Common_Currency = t4.Name,
                         ScanedFile = t1.ScanedFile,
                         Status = t1.Status,
                         IsApproved = t1.IsApproved,
                         LCDate = t1.LCDate,
                         Commercial_LCOreginFK = t1.Commercial_LCOreginFK,
                         ValueAmendment = (from xx in _db.Commercial_BBLCValueAmendment where xx.Active && xx.Commercial_BBLCFk == t1.ID select xx.ValueAmendment).DefaultIfEmpty(0).Sum(),
                         UserID = t1.UserID,
                         CurrentTotalValue = t1.BBValue + (from xx in _db.Commercial_BBLCValueAmendment where xx.Active && xx.Commercial_BBLCFk == t1.ID select xx.ValueAmendment).DefaultIfEmpty(0).Sum(),
                         Tolerance = t1.Tolerance,
                         Remarks = t1.Remarks,
                         Commercial_UDFk = t1.Commercial_UDFk
                     }).OrderByDescending(x => x.ID).AsEnumerable();

            return v;
        }

        private IEnumerable<VMCommercial_BBLCPaymentInformation> GetBBLCPaymentInformation(int id)
        {
            var v = (from t1 in _db.Commercial_BBLCPaymentInformation
                     where t1.Active == true && t1.Commercial_BBLCFk == id
                     select new VMCommercial_BBLCPaymentInformation
                     {
                         Commercial_BBLCFk = t1.Commercial_BBLCFk,
                         B2bLCPayment = t1.B2bLCPayment,
                         ID = t1.ID,
                         UserID = t1.UserID,
                         User = t1.User,
                         Time = t1.Time,
                         Date = t1.Date
                     }).OrderByDescending(x => x.ID).AsEnumerable();

            return v;
        }

        public async Task<VMCommercial_BBLCPaymentInformation> BBLCPaymentInformationGet(int id)
        {
            var vmBBLC = new VMCommercial_BBLCPaymentInformation
            {
                VmBBLC = await Task.Run(() => GetSingleBBLC(id)),

                DataList = await Task.Run(() => GetBBLCPaymentInformation(id))
            };

            return vmBBLC;
        }

        public async Task<VmBBLC> GetSingleBBLC(int id)
        {
            var v = await Task.Run(() => (from t1 in _db.Commercial_BBLC
                                          join t2 in _db.Common_Supplier on t1.Common_SupplierFK equals t2.ID
                                          join t3 in _db.Common_LcType on t1.Common_LcTypeFK equals t3.ID
                                          join t4 in _db.Common_Currency on t1.Common_CurrencyFK equals t4.ID
                                          where t1.Active == true && t2.Active == true && t1.ID == id
                                          select new VmBBLC
                                          {
                                              TotalPaymentValue = _db.Commercial_BBLCPaymentInformation.Where(x => x.Active && x.Commercial_BBLCFk == t1.ID).Select(x => x.B2bLCPayment).DefaultIfEmpty(0).Sum(),
                                              CID = t1.CID,
                                              ID = t1.ID,
                                              BBNo = t1.BBNo,
                                              BBValue = t1.BBValue,
                                              Common_LcTypeFK = t1.Common_LcTypeFK,
                                              Common_SupplierFK = t1.Common_SupplierFK,
                                              Common_CurrencyFK = t1.Common_CurrencyFK,
                                              IsClose = t1.IsClose,
                                              VMBBLCValueAmendmentList = (from x in _db.Commercial_BBLCValueAmendment
                                                                          where x.Active == true && x.Commercial_BBLCFk == t1.ID
                                                                          select new VMBBLCValueAmendment
                                                                          {
                                                                              ID = x.ID,
                                                                              ValueAmendment = x.ValueAmendment,
                                                                              Commercial_BBLCFk = x.Commercial_BBLCFk,
                                                                              Time = x.Time

                                                                          }).OrderByDescending(o => o.ID).ToList(),
                                              VMPurchaseOrderList = (from x in _db.Procurement_PurchaseOrder
                                                                     where x.Active == true && x.Commercial_BBLCFk == t1.ID && x.Common_SupplierFK == t2.ID
                                                                     select new VMPurchaseOrder
                                                                     {
                                                                         ID = x.ID,
                                                                         CID = x.CID + "/" + x.OrderDate.ToShortDateString(),
                                                                         TotalPOValue = x.TotalPOValue
                                                                     }).OrderByDescending(o => o.ID).ToList(),
                                              Common_UDNo = t1.Commercial_UDFk > 0 ? (from o in _db.Commercial_UD where o.Active == true && o.ID == t1.Commercial_UDFk select o.Ref).FirstOrDefault() : "Not set yet",
                                              Common_Supplier = t2.Name,
                                              Common_LcType = t3.Name,
                                              Common_Currency = t4.Name,
                                              ScanedFile = t1.ScanedFile,
                                              Status = t1.Status,
                                              IsApproved = t1.IsApproved,
                                              LCDate = t1.LCDate,
                                              Commercial_LCOreginFK = t1.Commercial_LCOreginFK,
                                              ValueAmendment = (from xx in _db.Commercial_BBLCValueAmendment where xx.Active && xx.Commercial_BBLCFk == t1.ID select xx.ValueAmendment).DefaultIfEmpty(0).Sum(),
                                              UserID = t1.UserID,
                                              CurrentTotalValue = t1.BBValue + (from xx in _db.Commercial_BBLCValueAmendment where xx.Active && xx.Commercial_BBLCFk == t1.ID select xx.ValueAmendment).DefaultIfEmpty(0).Sum(),
                                              Remaining = (t1.BBValue + (from xx in _db.Commercial_BBLCValueAmendment where xx.Active && xx.Commercial_BBLCFk == t1.ID select xx.ValueAmendment).DefaultIfEmpty(0).Sum())
                              - (from stpay in _db.Commercial_BBLCPaymentInformation where stpay.Active && stpay.Commercial_BBLCFk == t1.ID select stpay.B2bLCPayment).DefaultIfEmpty(0).Sum()
                              ,
                                              Tolerance = t1.Tolerance,
                                              Remarks = t1.Remarks,
                                              Commercial_UDFk = t1.Commercial_UDFk
                                          }).OrderByDescending(x => x.ID).FirstOrDefault());

            return v;
        }


        private IEnumerable<VmBBLC> GetBBLCDataByUD(int id)
        {
            var bbData = (from t1 in _db.Commercial_BBLC
                          join t2 in _db.Common_Supplier on t1.Common_SupplierFK equals t2.ID
                          join t3 in _db.Common_LcType on t1.Common_LcTypeFK equals t3.ID
                          join t4 in _db.Common_Currency on t1.Common_CurrencyFK equals t4.ID
                          where t1.Active == true && t1.Commercial_UDFk == id
                          select new VmBBLC
                          {
                              CID = t1.CID,
                              ID = t1.ID,
                              BBNo = t1.BBNo,
                              BBValue = t1.BBValue,
                              Common_LcTypeFK = t1.Common_LcTypeFK,
                              Common_SupplierFK = t1.Common_SupplierFK,
                              Common_CurrencyFK = t1.Common_CurrencyFK,
                              IsClose = t1.IsClose,
                              VMBBLCValueAmendmentList = (from x in _db.Commercial_BBLCValueAmendment
                                                          where x.Active == true && x.Commercial_BBLCFk == t1.ID
                                                          select new VMBBLCValueAmendment
                                                          {
                                                              ID = x.ID,
                                                              ValueAmendment = x.ValueAmendment,
                                                              Commercial_BBLCFk = x.Commercial_BBLCFk,
                                                              Time = x.Time

                                                          }).OrderByDescending(o => o.ID).ToList(),
                              VMPurchaseOrderList = (from x in _db.Procurement_PurchaseOrder
                                                     where x.Active == true && x.Commercial_BBLCFk == t1.ID && x.Common_SupplierFK == t2.ID
                                                     select new VMPurchaseOrder
                                                     {
                                                         ID = x.ID,
                                                         CID = x.CID + "/" + x.OrderDate.ToShortDateString(),
                                                         TotalPOValue = x.TotalPOValue
                                                     }).OrderByDescending(o => o.ID).ToList(),
                              TotalPurchaseOrderalue = _db.Procurement_PurchaseOrder.Where(x => x.Commercial_BBLCFk == t1.ID).Select(x => x.TotalPOValue).DefaultIfEmpty(0).Sum(),
                              Common_UDNo = t1.Commercial_UDFk > 0 ? (from o in _db.Commercial_UD where o.Active == true && o.ID == t1.Commercial_UDFk select o.Ref).FirstOrDefault() : "Not set yet",
                              Common_Supplier = t2.Name,
                              Common_LcType = t3.Name,
                              Common_Currency = t4.Name,
                              ValueAmendment = (from xx in _db.Commercial_BBLCValueAmendment where xx.Active && xx.Commercial_BBLCFk == t1.ID select xx.ValueAmendment).DefaultIfEmpty(0).Sum(),
                              ScanedFile = t1.ScanedFile,
                              Status = t1.Status,
                              IsApproved = t1.IsApproved,
                              LCDate = t1.LCDate,
                              Commercial_LCOreginFK = t1.Commercial_LCOreginFK,
                              UserID = t1.UserID,
                              CurrentTotalValue = t1.BBValue + (from xx in _db.Commercial_BBLCValueAmendment where xx.Active && xx.Commercial_BBLCFk == t1.ID select xx.ValueAmendment).DefaultIfEmpty(0).Sum(),

                              Remaining = (t1.BBValue + (from xx in _db.Commercial_BBLCValueAmendment where xx.Active && xx.Commercial_BBLCFk == t1.ID select xx.ValueAmendment).DefaultIfEmpty(0).Sum())
                              - (from stpay in _db.Commercial_BBLCPaymentInformation where stpay.Active && stpay.Commercial_BBLCFk == t1.ID select stpay.B2bLCPayment).DefaultIfEmpty(0).Sum()
                              ,

                              Tolerance = t1.Tolerance,
                              Remarks = t1.Remarks,
                              Commercial_UDFk = t1.Commercial_UDFk

                          }).OrderByDescending(x => x.ID).ToList();
            return bbData;
        }

        public async Task<int> BBLCAdd(VmBBLC vmBBLC)
        {
            #region Genarate CID
            int totalBBLC = _db.Commercial_BBLC.Count();

            if (totalBBLC == 0)
            {
                totalBBLC = 1;
            }
            else
            {
                totalBBLC++;
            }
            var newString = totalBBLC.ToString().PadLeft(5, '0');
            string genaratedBBLC = "BBLC" + DateTime.Now.Year.ToString() + DateTime.Now.Month.ToString() + newString;
            #endregion
            var result = -1;
            int baseHeadId = 0;
            var commercialBBLC = new Commercial_BBLC
            {
                CID = genaratedBBLC,
                ID = vmBBLC.ID,
                BBNo = vmBBLC.BBNo,
                LCDate = vmBBLC.LCDate,
                Commercial_LCOreginFK = vmBBLC.Commercial_LCOreginFK,
                Commercial_UDFk = vmBBLC.Commercial_UDFk,
                Common_LcTypeFK = vmBBLC.Common_LcTypeFK,
                Common_SupplierFK = vmBBLC.Common_SupplierFK,
                Common_CurrencyFK = vmBBLC.Common_CurrencyFK,
                Tolerance = vmBBLC.Tolerance,
                BBValue = vmBBLC.BBValue,
                Remarks = vmBBLC.Remarks,
                UserID = vmBBLC.UserID,
                User = vmBBLC.User,
                ScanedFile = vmBBLC.ScanedFile,
                IsApproved = vmBBLC.IsApproved,
                Status = vmBBLC.Status
            };

            _db.Commercial_BBLC.Add(commercialBBLC);
            if (await _db.SaveChangesAsync() > 0)
            {
                baseHeadId = await integrationService.AccountingHeadCreate("BBLC No: " + vmBBLC.BBNo + ". Date: " + vmBBLC.LCDate.ToShortDateString(), 1424);

                result = commercialBBLC.ID;
                ///// Account Pay Integration /////

            }
            if (commercialBBLC.ID > 0 && vmBBLC.ValueAmendment > 0)
            {
                Commercial_BBLCValueAmendment commercialBBLCValueAmendment = new Commercial_BBLCValueAmendment
                {
                    Commercial_BBLCFk = commercialBBLC.ID,
                    ValueAmendment = vmBBLC.ValueAmendment,
                    UserID = vmBBLC.UserID

                };
                _db.Commercial_BBLCValueAmendment.Add(commercialBBLCValueAmendment);
                await _db.SaveChangesAsync();
            }
            if (baseHeadId > 0)
            {
                VmBBLC vmBBLCEdit = new VmBBLC { ID = result, BaseHeadId = baseHeadId };

            }
            return result;
        }
        public async Task<int> BBLCEdit(VmBBLC vmBBLC)
        {
            var result = -1;
            var commercialBBLC = _db.Commercial_BBLC.Find(vmBBLC.ID);

            commercialBBLC.BBNo = vmBBLC.BBNo;
            commercialBBLC.LCDate = vmBBLC.LCDate;
            commercialBBLC.Commercial_LCOreginFK = vmBBLC.Commercial_LCOreginFK;
            commercialBBLC.Commercial_UDFk = vmBBLC.Commercial_UDFk;
            commercialBBLC.Common_LcTypeFK = vmBBLC.Common_LcTypeFK;
            commercialBBLC.Common_SupplierFK = vmBBLC.Common_SupplierFK;
            commercialBBLC.Common_CurrencyFK = vmBBLC.Common_CurrencyFK;
            commercialBBLC.Tolerance = vmBBLC.Tolerance;
            commercialBBLC.BBValue = vmBBLC.BBValue;
            commercialBBLC.Remarks = vmBBLC.Remarks;
            commercialBBLC.UserID = vmBBLC.UserID;
            commercialBBLC.User = vmBBLC.User;
            commercialBBLC.ScanedFile = vmBBLC.ScanedFile;
            commercialBBLC.IsApproved = vmBBLC.IsApproved;
            commercialBBLC.Status = vmBBLC.Status;
            commercialBBLC.BaseHeadId = vmBBLC.BaseHeadId;
            if (await _db.SaveChangesAsync() > 0)
            {
                if (vmBBLC.BaseHeadId > 0)
                {
                    var eciresult = await integrationService.AccountingHeadEdit(vmBBLC.BaseHeadId, "BBLC No: " + vmBBLC.BBNo + ". Date: " + vmBBLC.LCDate.ToShortDateString());

                }

                result = commercialBBLC.ID;
            }
            if (commercialBBLC.ID > 0 && vmBBLC.ValueAmendment > 0)
            {
                Commercial_BBLCValueAmendment commercialBBLCValueAmendment = new Commercial_BBLCValueAmendment
                {
                    Commercial_BBLCFk = commercialBBLC.ID,
                    ValueAmendment = vmBBLC.ValueAmendment,
                    UserID = vmBBLC.UserID

                };
                _db.Commercial_BBLCValueAmendment.Add(commercialBBLCValueAmendment);
                await _db.SaveChangesAsync();
            }
            return result;
        }
        public async Task<int> BBLCDelete(int id)
        {
            var result = -1;
            if (id == 0) return result;
            var commEci = _db.Commercial_BBLC.Find(id);
            commEci.Active = false;

            if (await _db.SaveChangesAsync() > 0)
            {
                result = commEci.ID;
            }
            return result;
        }

        public async Task<int> ECIBuyerOrderMapping(VmECI comm_ECI)
        {
            var result = -1;
            List<Merchandising_BuyerOrder> merchandisingBuyerOrdeList = new List<Merchandising_BuyerOrder>();
            if (comm_ECI.Merchandising_BuyerOrderListFk.Any())
            {
                foreach (var orderId in comm_ECI.Merchandising_BuyerOrderListFk)
                {
                    Merchandising_BuyerOrder merchandising_BuyerOrder = _db.Merchandising_BuyerOrder.Where(x => x.ID.ToString() == orderId).FirstOrDefault();
                    merchandisingBuyerOrdeList.Add(merchandising_BuyerOrder);
                }
            }
            merchandisingBuyerOrdeList.ForEach(x => x.Commercial_ECIFk = comm_ECI.ID);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = comm_ECI.ID;
            }
            return result;
        }
        public async Task<int> BBSupplierOrderMapping(VmBBLC vmBBLC)
        {
            var result = -1;
            List<Procurement_PurchaseOrder> merchandisingBuyerOrdeList = new List<Procurement_PurchaseOrder>();
            if (vmBBLC.Procurement_PurchaseOrderListFk.Any())
            {
                foreach (var orderId in vmBBLC.Procurement_PurchaseOrderListFk)
                {
                    Procurement_PurchaseOrder procurement_PurchaseOrder = _db.Procurement_PurchaseOrder.Where(x => x.ID.ToString() == orderId).FirstOrDefault();
                    merchandisingBuyerOrdeList.Add(procurement_PurchaseOrder);
                }
            }
            merchandisingBuyerOrdeList.ForEach(x => x.Commercial_BBLCFk = vmBBLC.ID);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = vmBBLC.ID;
            }
            return result;
        }
        public async Task<int> MappingUDInECIPost(VmECI comm_ECI)
        {
            var result = -1;
            Commercial_ECI commercialECI = _db.Commercial_ECI.Where(x => x.ID == comm_ECI.ID).FirstOrDefault();
            commercialECI.Commercial_UDFk = comm_ECI.Commercial_UDFk;
            if (await _db.SaveChangesAsync() > 0)
            {
                result = comm_ECI.ID;
            }
            return result;
        }
        public async Task<int> MappingECIInUDPost(VmECI comm_ECI)
        {
            var result = -1;
            if (comm_ECI.Commercial_ECIListFk.Any())
            {
                List<Commercial_ECI> commercialECIList = new List<Commercial_ECI>();
                foreach (var eciId in comm_ECI.Commercial_ECIListFk)
                {
                    Commercial_ECI commercialECI = _db.Commercial_ECI.Where(x => x.ID == Convert.ToInt32(eciId)).FirstOrDefault();
                    commercialECIList.Add(commercialECI);
                }
                commercialECIList.ForEach(x => x.Commercial_UDFk = comm_ECI.Commercial_UDFk);
            }
            if (await _db.SaveChangesAsync() > 0)
            {
                result = comm_ECI.ID;
            }
            return result;
        }
        public async Task<int> MappingBBInUDPost(VmBBLC vmBBLC)
        {
            var result = -1;
            if (vmBBLC.Commercial_BBListFk.Any())
            {
                List<Commercial_BBLC> CommercialBBLCList = new List<Commercial_BBLC>();
                foreach (var bbId in vmBBLC.Commercial_BBListFk)
                {
                    Commercial_BBLC commercialECI = _db.Commercial_BBLC.Where(x => x.ID == Convert.ToInt32(bbId)).FirstOrDefault();
                    CommercialBBLCList.Add(commercialECI);
                }
                CommercialBBLCList.ForEach(x => x.Commercial_UDFk = vmBBLC.Commercial_UDFk);
            }
            if (await _db.SaveChangesAsync() > 0)
            {
                result = vmBBLC.ID;
            }
            return result;
        }
        public async Task<int> DetachECIFromUDPost(VmECI comm_ECI)
        {
            var result = -1;
            Commercial_ECI commercialECI = _db.Commercial_ECI.Where(x => x.ID == comm_ECI.ID).FirstOrDefault();
            commercialECI.Commercial_UDFk = 0;
            if (await _db.SaveChangesAsync() > 0)
            {
                result = comm_ECI.ID;
            }
            return result;
        }
        public async Task<int> DetachBBFromUDPost(VmBBLC vmBBLC)
        {
            var result = -1;
            Commercial_BBLC commercialBBLC = _db.Commercial_BBLC.Where(x => x.ID == vmBBLC.ID).FirstOrDefault();
            commercialBBLC.Commercial_UDFk = 0;
            if (await _db.SaveChangesAsync() > 0)
            {
                result = commercialBBLC.ID;
            }
            return result;
        }
        public async Task<int> MappingUDInBBPost(VmBBLC vmBBLC)
        {
            var result = -1;
            Commercial_BBLC commercialBBLC = _db.Commercial_BBLC.Where(x => x.ID == vmBBLC.ID).FirstOrDefault();
            commercialBBLC.Commercial_UDFk = vmBBLC.Commercial_UDFk;
            if (await _db.SaveChangesAsync() > 0)
            {
                result = vmBBLC.ID;
            }
            return result;
        }
        public async Task<int> DeleteECIPost(VmECI comm_ECI)
        {
            var result = -1;
            Commercial_ECI commercialECI = _db.Commercial_ECI.Where(x => x.ID == comm_ECI.ID).FirstOrDefault();
            commercialECI.Active = false;
            if (await _db.SaveChangesAsync() > 0)
            {
                result = comm_ECI.ID;
            }
            return result;
        }

        public async Task<int> DeleteBacktoBackPost(VmBBLC vmBBLC)
        {
            var result = -1;
            Commercial_BBLC commercialBBLC = _db.Commercial_BBLC.Where(x => x.ID == vmBBLC.ID).FirstOrDefault();
            commercialBBLC.Active = false;
            if (await _db.SaveChangesAsync() > 0)
            {
                result = commercialBBLC.ID;
            }
            return result;
        }
        public async Task<int> DetachBuyerPoFromECIPost(VmECI comm_ECI)
        {
            var result = -1;
            Merchandising_BuyerOrder merchandising_BuyerOrder = _db.Merchandising_BuyerOrder.Where(x => x.ID == comm_ECI.Merchandising_BuyerOrderFk).FirstOrDefault();
            merchandising_BuyerOrder.Commercial_ECIFk = 0; // 0 means detach from  eci.
            if (await _db.SaveChangesAsync() > 0)
            {
                result = comm_ECI.ID;
            }
            return result;
        }
        public async Task<int> DetachSupplierPoFromBBPost(VmBBLC vmBBLC)
        {
            var result = -1;
            Procurement_PurchaseOrder procurementPurchaseOrder = _db.Procurement_PurchaseOrder.Where(x => x.ID == vmBBLC.Procurement_PurchaseOrderFk).FirstOrDefault();
            procurementPurchaseOrder.Commercial_BBLCFk = 0; // 0 means detach from  eci.
            if (await _db.SaveChangesAsync() > 0)
            {
                result = procurementPurchaseOrder.ID;
            }
            return result;
        }
        public List<VMOrder> MerchandisingBuyerOrderByBuyerIdGet(int id)
        {
            var v = (from t1 in _db.Merchandising_BuyerOrder
                     join t2 in _db.Common_Buyer on t1.Common_BuyerFK equals t2.ID
                     where t1.Common_BuyerFK == id && t1.Active && !_db.Commercial_ECI.Any(c => c.ID == t1.Commercial_ECIFk)
                     select new VMOrder()
                     {
                         ID = t1.ID,
                         CID = t2.Name + "/" + t1.BuyerPO + "/" + t1.CID + " Value: " + t1.BuyerOrderPOValue,
                     }).OrderByDescending(a => a.ID).ToList();
            return v;
        }
        public List<VMOrder> ProcurementPurchaseOrderBySypplierIdGet(int id)
        {
            var v = (from t1 in _db.Procurement_PurchaseOrder
                     join t2 in _db.Common_Supplier on t1.Common_SupplierFK equals t2.ID
                     where t1.Common_SupplierFK == id && t1.Active && !_db.Commercial_BBLC.Any(c => c.ID == t1.Commercial_BBLCFk)
                     select new VMOrder()
                     {
                         ID = t1.ID,
                         CID = t1.CID + " Value: " + t1.TotalPOValue,
                     }).OrderByDescending(a => a.ID).ToList();
            return v;
        }
        public List<VMCommercialUD> MappingUDByBuyerIdWithECIGet(int id)
        {
            var v = (from t1 in _db.Commercial_UD
                     join t2 in _db.Common_Buyer on t1.Common_BuyerFk equals t2.ID
                     where t1.Common_BuyerFk == id && t1.Active && !t1.IsClose
                     select new VMCommercialUD()
                     {
                         ID = t1.ID,
                         Ref = t1.Ref,
                     }).OrderByDescending(a => a.ID).ToList();
            return v;
        }
        public List<VMCommercialUD> MappingUDByIdWithBBGet()
        {
            var v = (from t1 in _db.Commercial_UD
                     where t1.Active && !t1.IsClose
                     select new VMCommercialUD()
                     {
                         ID = t1.ID,
                         Ref = t1.Ref,
                     }).OrderByDescending(a => a.ID).ToList();
            return v;
        }
        public List<VMCommercialUD> MappingBBWithUDGet()
        {
            var v = (from t1 in _db.Commercial_BBLC
                     where t1.Active && t1.Commercial_UDFk == 0 && !t1.IsClose
                     select new VMCommercialUD()
                     {
                         ID = t1.ID,
                         Ref = t1.CID + "-" + t1.BBNo + ". Value: " + decimal.Add(t1.BBValue, (from xx in _db.Commercial_BBLCValueAmendment where xx.Active && xx.Commercial_BBLCFk == t1.ID select xx.ValueAmendment).DefaultIfEmpty(0).Sum()),
                     }).OrderByDescending(a => a.ID).ToList();
            return v;
        }
        public List<VMCommercialUD> MappingECIByIdWithUDGet(int id)
        {
            var v = (from t1 in _db.Commercial_ECI
                     where t1.Active && t1.Common_BuyerFK == id && t1.Commercial_UDFk == 0 && !t1.IsClose
                     select new VMCommercialUD()
                     {
                         ID = t1.ID,
                         Ref = t1.CID + "-" + t1.ECINo + ". Value: " + decimal.Add(t1.TotalValue, (from xx in _db.Commercial_ECIValueAmendment where xx.Active && xx.Commercial_ECIFk == t1.ID select xx.ValueAmendment).DefaultIfEmpty(0).Sum()),
                     }).OrderByDescending(a => a.ID).ToList();
            return v;
        }
        public Commercial_ECI SingleECIJsonGet(int id)
        {
            var v = _db.Commercial_ECI.Where(x => x.ID == id).FirstOrDefault();
            return v;
        }
        public Commercial_UD SingleUDJsonGet(int id)
        {
            var v = _db.Commercial_UD.Where(x => x.ID == id).FirstOrDefault();
            return v;
        }
        public Commercial_BBLC SingleBBLCJsonGet(int id)
        {
            var v = _db.Commercial_BBLC.Where(x => x.ID == id).FirstOrDefault();
            return v;
        }
        public List<object> CurrencyDropDownList()
        {
            var currencyList = new List<object>();
            foreach (var currency in _db.Common_Currency.Where(x => x.Active).ToList())
            {
                currencyList.Add(new { Text = currency.Name, Value = currency.ID });
            }
            return currencyList;
        }
        public List<object> UnitDropDownList()
        {
            var unitList = new List<object>();
            foreach (var unit in _db.Common_Unit.Where(x => x.Active).ToList())
            {
                unitList.Add(new { Text = unit.Name, Value = unit.ID });
            }
            return unitList;
        }
        public List<object> UDDropDownList()
        {
            var commercialUDList = new List<object>();
            foreach (var commercialUd in _db.Commercial_UD.Where(x => x.Active).ToList())
            {
                commercialUDList.Add(new { Text = commercialUd.Ref +". Date: "+ commercialUd.Date.ToShortDateString(), Value = commercialUd.ID });
            }
            return commercialUDList;
        }
        public List<object> OriginDropDownList()
        {
            var unitList = new List<object>();
            unitList.Add(new { Text = "Local", Value = 1 });
            unitList.Add(new { Text = "Foreign", Value = 2 });

            return unitList;
        }
        public List<object> SupplierDropDownList()
        {
            var supplierList = new List<object>();
            foreach (var supplier in _db.Common_Supplier.Where(x => x.Active).ToList())
            {
                supplierList.Add(new { Text = supplier.Name, Value = supplier.ID });
            }
            return supplierList;
        }
        public List<object> BuyerDropDownList()
        {
            var buyerList = new List<object>();
            foreach (var buyer in _db.Common_Buyer.Where(x => x.Active).ToList())
            {
                buyerList.Add(new { Text = buyer.Name, Value = buyer.ID });
            }
            return buyerList;
        }
        public List<object> LcTypeForECIDropDownList()
        {
            var lcType = new List<object>();
            //foreach (var lc in _db.Common_LcType.Where(x => x.IsECIOrBB == 1).ToList())
            //{
            //    lcType.Add(new { Text = lc.Name, Value = lc.ID });
            //}
            foreach (var lc in _db.Common_LcType.Where(x => x.IsECIOrBB == 1 && x.Active == true).ToList())
            {
                lcType.Add(new { Text = lc.Name, Value = lc.ID });
            }
            return lcType;
        }
        public List<object> LcTypeForBBDropDownList()
        {
            var lcType = new List<object>();
            foreach (var lc in _db.Common_LcType.Where(x => x.IsECIOrBB == 2 && x.Active == true).ToList())
            {
                lcType.Add(new { Text = lc.Name, Value = lc.ID });
            }
            return lcType;
        }
        public List<object> BuyerBankDropDownList()
        {
            var buyerBank = new List<object>();
            var bankes = (from t1 in _db.Common_BuyerBank
                          join t2 in _db.Common_Bank on t1.Common_BankFK equals t2.ID
                          where t1.Active && t2.Active
                          select new
                          {
                              BankID = t1.ID,
                              BankName = t2.Name + ", " + t1.Name
                          }).ToList();


            foreach (var bank in bankes)
            {
                buyerBank.Add(new { Text = bank.BankName, Value = bank.BankID });
            }
            return buyerBank;

        }

        public List<object> BuyerBankByBuyerIdDropDownList(int id)
        {
            var buyerBank = new List<object>();
            var bankes = (from t1 in _db.Common_BuyerBank
                          join t2 in _db.Common_Bank on t1.Common_BankFK equals t2.ID
                          where t1.Active && t2.Active && t1.Common_BuyerFK == id
                          select new VMBank()
                          {
                              ID = t1.ID,
                              Name = t2.Name + ", " + t1.Name
                          }).ToList();
            foreach (var bank in bankes)
            {
                buyerBank.Add(new { Text = bank.Name, Value = bank.ID });
            }
            return buyerBank;

        }
        public List<object> LienBankDropDownList()
        {
            var lienBank = new List<object>();
            var bankes = (from t1 in _db.Common_LienBank
                          join t2 in _db.Common_Bank on t1.Common_BankFK equals t2.ID
                          where t1.Active && t2.Active
                          select new
                          {
                              BankID = t1.ID,
                              BankName = t2.Name + ", " + t1.Name
                          }).ToList();
            foreach (var bank in bankes)
            {
                lienBank.Add(new { Text = bank.BankName, Value = bank.BankID });
            }
            return lienBank;

        }
        public List<object> BuyerNotifyPartyDropDownList()
        {
            var buyer = new List<object>();
            foreach (var bb in _db.Common_BuyerNotifyParty.Where(x => x.Active).ToList())
            {
                buyer.Add(new { Text = bb.Name, Value = bb.ID });
            }
            return buyer;

        }
        public List<object> CompanyBankDropDownList()
        {
            var companyBank = new List<object>();
            var bankes = (from t1 in _db.Common_CompanyBank
                          join t2 in _db.Common_Bank on t1.Common_BankFK equals t2.ID
                          where t1.Active && t2.Active
                          select new
                          {
                              BankID = t1.ID,
                              BankName = t2.Name + ", " + t1.Name
                          }).ToList();
            foreach (var bank in bankes)
            {
                companyBank.Add(new { Text = bank.BankName, Value = bank.BankID });
            }
            return companyBank;

        }

        public List<object> CommercialECIDropDownList()
        {
            var commercialECI = new List<object>();
            foreach (var eci in _db.Commercial_ECI.ToList())
            {
                commercialECI.Add(new { Text = eci.CID + "-" + eci.ECINo + ". Value = " + eci.TotalValue + ")", Value = eci.ID });
            }
            return commercialECI;
        }

        public List<object> BuyerPODropDownList()
        {
            var eciList = new List<object>();
            var vData = (from t1 in _db.Merchandising_BuyerOrder
                         join t2 in _db.Common_Buyer on t1.Common_BuyerFK equals t2.ID
                         where t1.Active == true && !_db.Commercial_ECI.Any(c => c.ID == t1.Commercial_ECIFk && c.Active == true)
                         select new
                         {
                             ID = t1.ID,
                             Name = t2.BuyerID + "/" + t1.BuyerPO + "/" + t1.CID + ". Value: " + t1.BuyerOrderPOValue
                         }).ToList();


            foreach (var order in vData)
            {
                eciList.Add(new { Text = order.Name, Value = order.ID });
            }
            return eciList;
        }
        public List<object> PODropDownList()
        {
            var eciList = new List<object>();
            var filterdata = _db.Procurement_PurchaseOrder.Where(x => !_db.Commercial_BBLC.Any(c => c.ID == x.Commercial_BBLCFk && c.Active) && x.Status == (int)POStatusEnum.FinalApproved).AsEnumerable();
            foreach (var currency in filterdata.ToList())
            {
                eciList.Add(new { Text = currency.CID + " (PO Value =" + currency.TotalPOValue + ")", Value = currency.ID });
            }
            return eciList;
        }

        public async Task<VMSupplier> GetBBLCSuppliers()
        {
            VMSupplier vMSupplier = new VMSupplier();
            vMSupplier.DataList = await Task.Run(() => (from t0 in _db.Commercial_BBLC
                                                        join t1 in _db.Common_Supplier on t0.Common_SupplierFK equals t1.ID into t1_Join
                                                        from t1 in t1_Join.DefaultIfEmpty()
                                                        where t0.Active
                                                        select new VMSupplier
                                                        {
                                                            ID = t1.ID,
                                                            Code = t1.Code,
                                                            Name = t1.Name,
                                                            VMCommon_CountryID = t1.Common_CountryFK,
                                                            VMCommon_CountryName = t1.Common_Country.Name,
                                                            IsForeignSupplier = t1.IsForeignSupplier,
                                                            SupplierType = (EnumSupplierType)t1.SupplierType,
                                                            Email = t1.Email,
                                                            Phone = t1.Phone,
                                                            Address = t1.Address,
                                                            ContactPerson = t1.ContactPerson
                                                        }).ToList());
            return vMSupplier;
        }
    }
}