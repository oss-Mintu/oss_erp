﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Erp.Core.Services.Home;
using Erp.Core.Services.Merchandising;
using Erp.Core.Services.Procurement;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Erp.Core.Services.Commercial
{
    //public class VmUDDetailBuyerOrder : VmUDBuyerOrder
    //{
    //    public VmUDBuyerOrder UDObj { get; set; }
    //    public VmECI ECIObj { get; set; }
    //    public VmBBLC BBObj { get; set; }
    //}
    public class VMCommercialUD : BaseVM
    {
        public bool IsClose { get; set; }

        public string CID { set; get; }
        public string Ref { set; get; }
        public DateTime Date { set; get; }
        public string ScanedFile { get; set; }
        public string Remarks { get; set; }
        public decimal LimitDeclaration { set; get; }
        public int Common_BuyerFk { set; get; }
        public string CommonBuyerName { set; get; }
        public IEnumerable<VMCommercialUD> DataList { get; set; }
        public List<VmECI> VmECIList { get; set; }
        public decimal TotalECIValue { get; set; }
        public decimal TotalBPOValue { get; set; }

        public decimal BTBDocumentLimitValue { get; set; }
        public decimal BTBDocumentValue { get; internal set; }
        public double UtilizedValue { get; set; }
        //public decimal TotalValue { set; get; }
        //public string Common_ECINo { set; get; }

        //public decimal TotalValueECI { set; get; }
        //public decimal TotalValueBBLC { set; get; }        
        //public decimal TotalValueBPO { set; get; }
        //public decimal TotalValueBBPer { set; get; }
        //public decimal TotalValueUD { set; get; }
        //public decimal TotalValueUDPer { set; get; }
        //public decimal TotalValueRem { set; get; }
        //public decimal TotalValueRemPer { set; get; }         

    }
    public class VMCommercialUDSlave : BaseVM
    {
        public VMCommercialUD VMCommercialUD { get; set; }
        public VmECI VmECI { get; set; }
        public VmBBLC VmBBLC { get; set; }         

    }
    public class VmECI : BaseVM
    {
        public bool IsClose { get; set; }
        public List<VMECIValueAmendment> VMECIValueAmendmentList { get; set; }
        public List<string> Commercial_ECIListFk { get; set; }
        public SelectList Commercial_ECIList { get; set; } = new SelectList(new List<object>());
        //public int[] ECIID { get; set; }
        public string ECINo { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime ECIDate { get; set; }
       
        public int Commercial_UDFk { get; set; }
        public int BaseHeadId { get; set; }

        public int Common_ECITypeFK { get; set; }
        public int Common_BuyerFK { get; set; }
        public decimal ValueAmendment { get; set; }
        public int Common_BuyerNotifyPartyFk { get; set; }
        public string Remarks { get; set; }
        public int Common_BuyerBankFK { get; set; }
        public int Common_LienBankFK { get; set; }
        public int Common_CompanyBankFK { get; set; }
        public int Common_CurrencyFK { get; set; }

        [Required(ErrorMessage = "Category TotalValue should not be empty")]
        [RegularExpression(@"^[a-zA-Z0-9]+$", ErrorMessage = "Special character should not be entered")]
        public decimal TotalValue { get; set; }
        public decimal CurrentTotalValue { get; set; }


        [Required(ErrorMessage = "Category Tolerance should not be empty")]
        [RegularExpression(@"^[a-zA-Z0-9]+$", ErrorMessage = "Special character should not be entered")]
        public decimal Tolerance { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime ShipmentDate { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public DateTime ExpiryDate { get; set; }
        public string Destination { get; set; }
        public string ScanedFile { get; set; }

        public string Common_UDNo { get; set; }
        public string Common_ECIType { get; set; }
        public string Common_Applicant { get; set; }
        public string Common_BuyerBank { get; set; }
        public string Common_LienBank { get; set; }
        public string Common_CompanyBank { get; set; }
        public string BuyerNotifyParty { get; set; }
        public List<string> Merchandising_BuyerOrderListFk { get; set; }
        public SelectList Merchandising_BuyerOrderList { get; set; } = new SelectList(new List<object>());
        public SelectList Commercial_UDList { get; set; } = new SelectList(new List<object>());


        public int Merchandising_BuyerOrderFk { get; set; }
        public string RefNoBuyerPO { get; set; }
        public decimal TotalValuePO { get; set; }

        public IEnumerable<VmECI> DataList { get; set; }
        public List<VmECI> DataLists { get; set; }
        public List<VMOrder> VMOrderList { get; set; }
        public string CID { get; set; }
    }
    public class VMECIValueAmendment : BaseVM
    {
        public decimal ValueAmendment { get; set; }
        public int Commercial_ECIFk { get; set; }
      

    }
    public class VMBBLCValueAmendment : BaseVM 
    {
        public decimal ValueAmendment { get; set; }
        public int Commercial_BBLCFk { get; set; }
       
    }

    public class VMCommercial_BBLCPaymentInformation : BaseVM
    {
        public VmBBLC VmBBLC { get; set; }
        [DisplayName("B2B L/C No")]
        public int Commercial_BBLCFk { get; set; }

        public int Accounting_HeadFK { get; set; }

        [DisplayName("Document Payment")]
        public decimal B2bLCPayment { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Payment Date")]
        public DateTime Date { get; set; }

        public bool IsApproved { get; set; }
        public IEnumerable<VMCommercial_BBLCPaymentInformation> DataList { get; set; }

    }
    public class VmBBLC : BaseVM
    {
        public VMSupplier VMSupplier { get; set; }
        public List<VMBBLCValueAmendment> VMBBLCValueAmendmentList { get; set; }
        public int BaseHeadId { get; set; }
        public int Commercial_BBFk { get; set; }
        public bool IsClose { get; set; }
        public decimal TotalPaymentValue { get; set; }

        public List<string> Commercial_BBListFk { get; set; }
        public SelectList Commercial_BBList { get; set; } = new SelectList(new List<object>());
        public string BBNo { get; set; }
        public DateTime LCDate { get; set; }
        public decimal CurrentTotalValue { get; set; }
        public decimal BBValue { get; set; }
        public int Commercial_UDFk { get; set; } = 0;

        public int Common_SupplierFK { get; set; }
        public int Common_LcTypeFK { get; set; }
        //public int Common_UnitFK { get; set; }

        public int Common_CurrencyFK { get; set; }
        public decimal Tolerance { get; set; }

        // Need those column 
        public decimal ValueAmendment { get; set; }
        public double TotalPurchaseOrderalue { get; set; }
        public int Commercial_LCOreginFK { get; set; }
        public string Status { get; set; } //Add due to requirement--FD 
        public bool IsApproved { get; set; } //Add due to requirement--FD 
        public EnumSupplierType SupplierType { get; set; }

        public SelectList SupplierTypeList
        {
            get
            {
                return new SelectList(BaseFunctionalities.GetEnumList<EnumSupplierType>(), "Value", "Text");
            }
        }

        public string Payable { get; set; }
        //public string ReqQty { get; set; }


        public string Remarks { get; set; }

        public string ScanedFile { get; set; }

        public IEnumerable<VmBBLC> DataList { get; set; }
        public List<VMPurchaseOrder> VMPurchaseOrderList { get; set; }
        public int Procurement_PurchaseOrderFk { get; set; }
        public List<string> Procurement_PurchaseOrderListFk { get; set; }
        public SelectList Procurement_PurchaseOrderList { get; set; } = new SelectList(new List<object>());
        public SelectList Commercial_UDList { get; set; } = new SelectList(new List<object>());



        public string Common_UDNo { get; set; }
        public string Common_LcType { get; set; }

        public string Common_Supplier { get; set; }
        public string Common_Currency { get; set; }
        public string CID { get; set; }
        public decimal Remaining { get; set; }

        //public int Common_POFK { get; set; }
        //public string RefNoPO { get; set; }
        //public decimal TotalValuePO { get; set; }
    }
}
