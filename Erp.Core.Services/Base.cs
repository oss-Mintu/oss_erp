﻿using Erp.Core.Services.Home;
using Erp.Core.Services.Integration;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Erp.Core.Services
{
    public static class SessionExtensionService
    {
        public static void SetObjectAsJson(this ISession session, string key, object value)
        {
            session.SetString(key, JsonConvert.SerializeObject(value));
        }

        public static T GetObjectFromJson<T>(this ISession session, string key)
        {
            var value = session.GetString(key);

            return value == null ? default(T) : JsonConvert.DeserializeObject<T>(value);
        }
    }
    public static class Environment
    {
        public static bool IsDevelopment = false;
    }
    public enum UserAccessLevel
    {
        Basic=1,
        Ultimate
    }
    public enum InventoryTypeEnum
    {
        Raw = 1,
        Wip,
        Finished,
        Procurement



    }

    public enum RoleTypeEnum
    {
        LineCheif = 1,
        LineSupervisor,
        LineOperator,
        LineHelper

    }
    public enum AccountTypeEnum
    {
        Type = 1,
        C1,
        C2,
        Head
    }
    public enum ActionEnum
    {
        Add = 1,
        Edit,
        Delete,
        Detech,
        Attech,
        Approve,
        Close,
        UnApprove,
        ReOpen,
        Finalize
    }
    public enum StatusEnum
    {
        Draft = 1,
        Approved,
        Closed
    }
    public enum PRActionEnum
    {
        Add = 1,
        Edit,
        Delete,
        Submit,
        [Description("Primary Approve")]
        PrimaryApprove,
        [Description("Final Approve")]
        FinalApprove,
        [Description("Procurement Approve")]
        ProcurementApprove,
        Close,
        [Description("Undo Submit")]
        UndoSubmit,
        [Description("Undo Primary Approval")]
        UndoPrimaryApprove,
        [Description("Undo Final Approval")]
        UndoFinalApprove,
        [Description("Undo Procurement Approval")]
        UndoProcurementApprove,
        [Description("Re-Open")]
        ReOpen,
        Finalize,
        [Description("Undo Finalize")]
        UndoFinalize,
        Hold,
        Cancel
    }
    public enum PRStatusEnum
    {
        Draft = 1,

        Submitted,
        [Description("Primary Approved")]
        PrimaryApproved,
        [Description("Final Approved")]
        FinalApproved,
        [Description("Procurement Approved")]
        ProcurementApproved,
        [Description("Partial PO Created")]
        PartialPOCreated,
        [Description("Full PO Created")]
        FullPOCreated,
        [Description("Inspection Done")]
        InspectionDone,
        [Description("Goods Received")]
        GoodsReceived,
        Closed,
        Hold,
        [Description("Cancelled")]
        Cancel
    }
    public enum PRTypeEnum { External = 0, Internal }
    public enum ProcurementOriginTypeEnum { Regular = 0, [Description("BOM/BOF")] Merchandising, BOM, BOF }
    public enum SupplierPaymentMethodEnum
    {
        Cash = 1,
        Cheque,
        TT,
        BBLC
    }
    public enum POTypeEnum
    {
        Procurement = 1,

        Service
    }
    public enum SRTypeEnum
    {
        Challan = 1,
        ChallanInternal = 2,
    }
    public enum POActionEnum
    {
        Add = 1,
        Edit,
        Delete,
        Submit,
        [Description("Primary Approve")]
        PrimaryApprove,
        [Description("Secondary Approve")]
        SecondaryApprove,
        [Description("Final Approve")]
        FinalApprove,
        Close,
        [Description("Undo Submit")]
        UndoSubmit,
        [Description("Undo Primary Approve")]
        UndoPrimaryApprove,
        [Description("Undo Secondary Approve")]
        UndoSecondaryApprove,
        [Description("Undo Final Approve")]
        UndoFinalApprove,
        [Description("Re-Open")]
        ReOpen,
        Finalize,
        [Description("Undo Finalize")]
        UndoFinalize,
        Hold,
        Cancel,
        PartialGoodsReceived,
        GoodsReceived
    }
    public enum POStatusEnum
    {
        Draft = 1,
        Submitted,
        [Description("Primary Approved")]
        PrimaryApproved,
        [Description("Secondary Approved")]
        SecondaryApproved,
        [Description("Final Approved")]
        FinalApproved,

        [Description("Inspection Done")]
        InspectionDone,
        [Description("Goods Received")]
        GoodsReceived,
        Closed,
        Hold,
        [Description("Cancelled")]
        Cancel,
        [Description("Partial Goods Received")]
        PartialGoodsReceived
    }

    public enum PurchaseInvoiceActionEnum
    {
        Add = 1,
        Edit,
        Delete,
        Submit,
        [Description("Primary Approve")]
        PrimaryApprove,
        [Description("Final Approve")]
        FinalApprove,
        [Description("Accounts Approve")]
        AccountsApprove,
        Close,
        [Description("Undo Submit")]
        UndoSubmit,
        [Description("Undo Primary Approve")]
        UndoPrimaryApprove,
        [Description("Undo Final Approve")]
        UndoFinalApprove,
        [Description("Undo Accounts Approve")]
        UndoAccountsApprove,
        [Description("Re-Open")]
        ReOpen,
        Finalize,
        [Description("Undo Finalize")]
        UndoFinalize,
        Hold,
        Cancel
    }
    public enum PurchaseInvoiceStatusEnum
    {
        Draft = 1,

        Submitted,
        [Description("Primary Approved")]
        PrimaryApproved,
        [Description("Final Approved")]
        FinalApproved,
        [Description("Accounts Approved")]
        AccountsApproved,
        [Description("Partial Paid")]
        PartialPaid,
        [Description("Full Paid")]
        FullPaid,
        Closed,
        Hold,
        [Description("Cancelled")]
        Cancel
    }
    public enum EnumBloodGroup
    {
        [Description("A+(ve)")]
        APositive = 1,
        [Description("A-(ve)")]
        ANegative,
        [Description("B+(ve)")]
        BPositive,
        [Description("B-(ve)")]
        BNegative,
        [Description("AB+(ve)")]
        ABPositive,
        [Description("AB-(ve)")]
        ABNegative,
        [Description("O+(ve)")]
        OPositive,
        [Description("O-(ve)")]
        ONegative,
    }
    public enum EnumEmploymentType
    {
        Permanent = 1,
        Contractual,
        [Description("Part Time")]
        PartTime
    }

    public enum EnumEmployeeType
    {
        Executive = 1,
        Labour
    }
    public enum EnumStaffType
    {
        Staff = 1,
        Worker
    }

    public enum EnumLeaveType
    {
        Casual = 1,
        Annual = 2,
        Sick = 3,
        Maternity = 4,
        Earn = 5
    }
    public enum EnumLeavePaidType
    {
        Paid = 1,
        UnPaid
    }
    public enum EnumLeaveStatus
    {
        Pending = 1,
        Approved = 2,
        Rejected = 3
    }

    public enum EnumLeaveDeptStatus
    {
        FirstLayerApproved = 1,
        SecondLayerApproved = 2,
        ThirdLayerApproved = 3,
        FourthLayerApproved = 4,
        FifthLayerApproved = 5,
        Rejected = 6
    }

    public enum EnumWeekendDay
    {
        Sunday = 1,
        Monday = 2,
        Tuesday = 3,
        Wednesday = 4,
        Thursday = 5,
        Friday = 6,
        Saturday = 7
    }
    public enum EnumSalaryBenefitType
    {
        [Description("OT")]
        OnlyOT = 2,
        [Description("Attendance")]
        OnlyAttendance
    }
    public enum EnumHolidayAndNightBillType
    {
        [Description("Holiday Bill")]
        HolidayBill = 1,
        [Description("Night Bill")]
        NightBill
    }
    public enum EnumSalaryType
    {
        [Description("Gross Salary")]
        GrossSalary = 1,
        [Description("Basic Salary")]
        BasicSalary
    }
    public enum EnumSupplierType
    {
        [Description("Regular (Trade)")]
        Regular = 1,
        [Description("Transport (Trade)")]
        Transport,
        [Description("C&F Agent (Trade)")]
        CandF,
        [Description("Construction (Trade)")]
        Construction,
        [Description("Other (Trade)")]
        Other

    }
    public static class BaseFunctionalities
    {
        public static string GetEnumDescription(Enum en)
        {
            Type type = en.GetType();
            MemberInfo[] memInfo = type.GetMember(en.ToString());

            if (memInfo != null && memInfo.Length > 0)
            {
                object[] attrs = memInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);
                if (attrs != null && attrs.Length > 0)
                    return ((DescriptionAttribute)attrs[0]).Description;
            }
            return en.ToString();
        }

        public static List<object> GetEnumList<T>()
        {
            var list = new List<object>();

            foreach (var item in Enum.GetValues(typeof(T)))
            {
                list.Add(new { Value = (int)item, Text = GetEnumDescription((Enum)Enum.Parse(typeof(T), item.ToString())) });
            }

            return list;
        }
    }
    
    public abstract class BaseService
    {
        public IErpDbContext _db { get; set; }
        public CidServices CidServices { get; set; }
        public MessageService _messageService { get; set; }
    }
    public abstract class BaseVM
    {
        public int ID { get; set; }
        public string User { get; set; }
        public int UserID { get; set; }
        public DateTime Time { get; set; }
        public ActionEnum ActionEum { get { return (ActionEnum)this.ActionId; } }
        public int ActionId { get; set; } = 1;
        public JournalEnum JournalEnum { get { return (JournalEnum)this.JournalType; } }
        public int JournalType { get; set; } = 1;
        public bool IsActive { get; set; }
        public string Code { get; set; }
        public string error { get; set; } = "";
        public string DepartmentId { get; set; }
    }

    public abstract class BaseReport
    {
        
        public string ReportTypeName { get; set; } 
        [DisplayName("Report Type")]
        public int ReportID { get; set; }
        [DataType(DataType.Date)]
        [DisplayName("From Date")]
        [Required(ErrorMessage = "Date is Required")]
        public DateTime FromDate { get; set; } = DateTime.Now;
        [DataType(DataType.Date)]
        [DisplayName("To Date")]
        [Required(ErrorMessage = "Date is Required")]
        public DateTime ToDate { get; set; } = DateTime.Now;
        public string User { get; set; }
        public int UserID { get; set; }
        public DateTime Time { get; set; }
        public ActionEnum ActionEum { get { return (ActionEnum)this.ActionId; } }
        public int ActionId { get; set; } = 1;
        public JournalEnum JournalEnum { get { return (JournalEnum)this.JournalType; } }
        public int JournalType { get; set; } = 1;
        public bool IsActive { get; set; }

        public string Error { get; set; } = "";
    }

    public class SelectListItem
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }
    public enum JournalEnum
    {
        JournalVoucher = 1,
        DebitVoucher,
        CreditVoucher,
        ContraVoucher,
        PurchaseVoucher,
        SellsVoucher,
        IntegrationVoucher,
        QuickJournalVoucher,
    }
    public enum StoreTransactionTypeEnum
    {
        In = 1,
        Out = 2
    }
    public enum InventoryTransactionTriggerTypeEnum
    {
        PurchaseInvoice = 1,
        StoreRequisition,
        StoreStockIn
        //StoreStockOut = 3
    }

    public enum IntegratedAccountHeadEnum
    {
        ExportLc = 1306,
        BbLc = 1302,
    }
    public enum IntegratedAccountHeadTypeEnum
    {
        Buyer = 1,
        Supplier = 2,
        ExportLc = 3,
        BbLc = 4,
    }
    public enum IntegratedJournalTypeEnum
    {
        SupplierPayment = 1,
        BuyerCollection,
        StoreOutInternal,
        StoreOutSaleReturn,
        StoreOutDisposal
    }
    public enum ECIOrBB
    {
        ECI = 1,
        BackToBack
    }
    public class Message
    {
        public const string TempDataKey = "TempDataKey";
        public string MessageStyle { get; set; }
        public string Messages { get; set; }
        public bool Dismissable { get; set; }
    }
    public static class MessageStyle
    {
        public const string Information = "information";
        public const string Success = "success";
        public const string Warning = "warning";
        public const string Danger = "danger";
    }

    public enum SRActionEnum
    {
        Add = 1,
        Edit,
        Delete,
        Submit,
        [Description("Primary Approve")]
        PrimaryApprove,
        [Description("Final Approve")]
        FinalApprove,
        [Description("Procurement Approve")]
        ProcurementApprove,
        Close,
        [Description("Undo Submit")]
        UndoSubmit,
        [Description("Undo Primary Approval")]
        UndoPrimaryApprove,
        [Description("Undo Final Approval")]
        UndoFinalApprove,
        [Description("Undo Procurement Approval")]
        UndoProcurementApprove,
        [Description("Re-Open")]
        ReOpen,
        Finalize,
        [Description("Undo Finalize")]
        UndoFinalize,
        Hold,
        Cancel
    }
    public enum SRStatusEnum
    {
        Draft = 1,

        Submitted,
        [Description("Primary Approved")]
        PrimaryApproved,
        [Description("Final Approved")]
        FinalApproved,
        [Description("Procurement Approved")]
        ProcurementApproved,
        [Description("Partial PO Created")]
        PartialPOCreated,
        [Description("Full PO Created")]
        FullPOCreated,
        [Description("Inspection Done")]
        InspectionDone,
        [Description("Goods Received")]
        GoodsReceived,
        Closed,
        Hold,
        [Description("Cancelled")]
        Cancel
    }
    public enum ItemStatusEnum
    {
        Working = 1,
        ParcialWorking,
        NotWorking,
        Damage,
        ParcialDamage,
        Return,
        Lost
    }
    public enum EnumAttendanceStatus
    {
        [Description("OK")]
        Present = 1,
        Late = 2,
        Absent = 3,
        Leave = 4,
        Holiday = 5,
        OffDay = 6,
        Weekend = 7,
        [Description("Holiday + OK")]
        HolidayPresent = 8,
        [Description("Weekend + OK")]
        WeekendPresent = 9,
        [Description("Off Day + OK")]
        OffDayPresent = 10,
        [Description("Leave + OK")]
        LeavePresent = 11,
        [Description("Holiday + Late")]
        HolidayLate = 12,
        [Description("Weekend + Late")]
        WeekendLate = 13,
        [Description("Off Day + Late")]
        OffDayLate = 14,
        [Description("Leave + Late")]
        LeaveLate = 15,
        Invalid = 20
    }
    public enum EnumEmployeeAction
    {
        Add = 1,
        Edit,
        Delete,
        Resign,
        [Description("Undo Resign")]
        UndoResign,
        Terminate,
        [Description("Undo Terminate")]
        UndoTerminate
    }
    public enum EnumEmployeeStatus
    {
        Present = 1,
        Resigned = 2,
        Terminated = 3
    }
    public enum EnumRequisitionType
    {
        NonProduction = 0,
        Production = 1
    }


    public enum CreditEnum
    {
        Rawinventory = 74,
        Wip = 75,
        Finish = 76,
        DebitVoucher,
        CreditVoucher,
        ContraVoucher,
        PurchaseVoucher,
        SellsVoucher,
        IntegrationVoucher,
        QuickJournalVoucher,
    }
    public enum DebitEnum
    {
        Rawinventory = 74,
        Wip = 75,
        Finish = 76,
        Unknown = 698,
        ContraVoucher,
        PurchaseVoucher,
        SellsVoucher,
        IntegrationVoucher,
        QuickJournalVoucher,
    }

    public enum CostCenterEnum
    {
        RomoOne = 1,
        HeadOffice,
        RomoTwo,
        Construction,
        Factory,
        RomoSub,
        General,
        //IntegrationVoucher,
        //QuickJournalVoucher,
    }
    public enum EnumProcess
    {
        Cutting = 1,
        Sewing,
        Ironning,
        Packing
    }

    public enum EnumDepartment
    {
        Admin=1,
        Merchandising,
        Procurement,
        Store,
        Production,
        Planning,
        Commercial,
        Accounts,
        HRAdmin

    }
    public enum EnumCategoryType 
    {
        RawItem = 1,
        FixedAsset,
        Stationeries
    }


}
