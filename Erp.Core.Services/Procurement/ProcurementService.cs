﻿using Erp.Core.Entity.Common;
using Erp.Core.Entity.Procurement;
using Erp.Core.Services.Home;
using Erp.Core.Services.Integration;
using Erp.Core.Services.Inventory;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Erp.Core.Services.Procurement
{
    public class ProcurementService : BaseService
    {
        private readonly IntegrationService integrationService;
        private readonly HttpContext _httpContext;
        private readonly VMLogin _vmLogin;

        public ProcurementService(IErpDbContext db, HttpContext httpContext)
        {
            _db = db; integrationService = new IntegrationService(_db, httpContext);
            _httpContext = httpContext;
            _vmLogin = httpContext.Session.GetObjectFromJson<VMLogin>("LoginUser");
        }

        #region Common
        public List<object> SupplierDropDownList()
        {
            var SupplierList = new List<object>();
            _db.Common_Supplier.Where(x => x.Active).Select(x => x).ToList().ForEach(x => SupplierList.Add(new
            {
                Value = x.ID.ToString(),
                Text = x.Name
            }));
            return SupplierList;
        }
        public List<object> RawCategoryDropDownList()
        {
            var List = new List<object>();
            _db.Common_RawCategory
        .Where(x => x.Active).Select(x => x).ToList()
        .ForEach(x => List.Add(new
        {
            Value = x.ID,
            Text = x.Name
        }));
            return List;

        }
        public List<object> RawSubCategoryDropDownList(int id = 0)
        {
            var List = new List<object>();
            _db.Common_RawSubCategory
        .Where(x => x.Active).Where(x => x.Common_RawCategoryFK == id || id <= 0).Select(x => x).ToList()
        .ForEach(x => List.Add(new
        {
            Value = x.ID,
            Text = x.Name
        }));
            return List;

        }
        public List<object> RawItemDropDownList(int id = 0)
        {
            var List = new List<object>();
            _db.Common_RawItem
        .Where(x => x.Active).Where(x => x.Common_RawSubCategoryFK == id || id <= 0).Select(x => x).ToList()
        .ForEach(x => List.Add(new
        {
            Value = x.ID,
            Text = x.Name
        }));
            return List;

        }
        public List<object> EmployeeDropDownList()
        {
            var List = new List<object>();
            _db.HRMS_Employee
        .Where(x => x.Active).Select(x => x).ToList()
        .ForEach(x => List.Add(new
        {
            Value = x.ID,
            Text = x.Name
        }));
            return List;
        }

        public List<object> UserDropDownList()
        {
            var List = new List<object>();
            _db.User_User
        .Where(x => x.Active).Select(x => x).ToList()
        .ForEach(x => List.Add(new
        {
            Value = x.ID,
            Text = x.Name
        }));
            return List;
        }
        public List<object> UnitDropDownList()
        {
            var List = new List<object>();
            _db.Common_Unit
        .Where(x => x.Active).Select(x => x).ToList()
        .ForEach(x => List.Add(new
        {
            Value = x.ID,
            Text = x.Name
        }));
            return List;

        }
        public List<object> CurrencyDropDownList()
        {
            var List = new List<object>();
            _db.Common_Currency
        .Where(x => x.Active).Select(x => x).ToList()
        .ForEach(x => List.Add(new
        {
            Value = x.ID.ToString(),
            Text = x.Name
        }));
            return List;

        }

        public List<object> PurchaseOrderDropDownList()
        {
            var List = new List<object>();
            var data = (from t1 in _db.Procurement_PurchaseInvoice
                        join t2 in _db.Procurement_PurchaseInvoiceSlave on t1.ID equals t2.Procurement_PurchaseInvoiceFK
                        join t3 in _db.Common_Supplier on t1.Common_SupplierFK equals t3.ID
                        select new VMPurchaseInvoiceSlave
                        {
                            ID = t1.ID,
                            CID = t1.CID + "/" + t3.Name
                        }).Distinct().ToList();
            foreach (var item in data)
            {
                List.Add(new { Text = item.CID, Value = item.ID });
            }
            return List;

        }
        #endregion

        #region PurchaseRequisition
        public async Task<VMPurchaseRequisition> PurchaseRequisitionGet(VMPurchaseRequisition model)
        {
            VMPurchaseRequisition vMPurchaseRequisition = new VMPurchaseRequisition();


            vMPurchaseRequisition.DataList = new List<VMPurchaseRequisition>();

            vMPurchaseRequisition.Status = model.Status;

            if (model.Status != PRStatusEnum.Closed)
            {
                await Task.Run(() => (from REQ in _db.Procurement_PurchaseRequisition
                                      where REQ.Active && !string.IsNullOrEmpty(REQ.CID) && (REQ.Status != (int)PRStatusEnum.Closed && REQ.Status != (int)PRStatusEnum.Cancel)
                                      group REQ by REQ.Status into g
                                      select new { Status = (PRStatusEnum)g.Key, Count = g.Count().ToString() }).ForEachAsync(x =>
                                           {
                                               if (x.Status == PRStatusEnum.Draft)
                                               {
                                                   vMPurchaseRequisition.DraftStatusCount = x.Count;
                                               }
                                               if (x.Status == PRStatusEnum.Submitted)
                                               {
                                                   vMPurchaseRequisition.SubmittedStatusCount = x.Count;
                                               }
                                               if (x.Status == PRStatusEnum.PrimaryApproved)
                                               {
                                                   vMPurchaseRequisition.PrimaryStatusCount = x.Count;
                                               }
                                               if (x.Status == PRStatusEnum.FinalApproved)
                                               {
                                                   vMPurchaseRequisition.FinalStatusCount = x.Count;
                                               }
                                               if (x.Status == PRStatusEnum.ProcurementApproved)
                                               {
                                                   vMPurchaseRequisition.ProcurementStatusCount = x.Count;
                                               }
                                               if (x.Status == PRStatusEnum.Hold)
                                               {
                                                   vMPurchaseRequisition.HoldStatusCount = x.Count;
                                               }
                                               if (x.Status == PRStatusEnum.PartialPOCreated)
                                               {
                                                   vMPurchaseRequisition.PartialPOStatusCount = x.Count;
                                               }
                                               if (x.Status == PRStatusEnum.FullPOCreated)
                                               {
                                                   vMPurchaseRequisition.FullPOStatusCount = x.Count;
                                               }

                                           }));

                //vMPurchaseRequisition.DraftStatusCount = await Task.Run(() => (_db.Procurement_PurchaseRequisition.Where(x => x.Status == (int)PRStatusEnum.Draft && !string.IsNullOrEmpty(x.CID) && x.Active).CountAsync().Result.ToString()));
                //vMPurchaseRequisition.SubmittedStatusCount = await Task.Run(() => (_db.Procurement_PurchaseRequisition.Where(x => x.Status == (int)PRStatusEnum.Submitted && x.Active).CountAsync().Result.ToString()));
                //vMPurchaseRequisition.PrimaryStatusCount = await Task.Run(() => (_db.Procurement_PurchaseRequisition.Where(x => x.Status == (int)PRStatusEnum.PrimaryApproved && x.Active).CountAsync().Result.ToString()));
                //vMPurchaseRequisition.FinalStatusCount = await Task.Run(() => (_db.Procurement_PurchaseRequisition.Where(x => x.Status == (int)PRStatusEnum.FinalApproved && x.Active).CountAsync().Result.ToString()));
                //vMPurchaseRequisition.ProcurementStatusCount = await Task.Run(() => (_db.Procurement_PurchaseRequisition.Where(x => x.Status == (int)PRStatusEnum.ProcurementApproved && x.Active).CountAsync().Result.ToString()));
                //vMPurchaseRequisition.PartialPOStatusCount = await Task.Run(() => (_db.Procurement_PurchaseRequisition.Where(x => x.Status == (int)PRStatusEnum.PartialPOCreated && x.Active).CountAsync().Result.ToString()));
                //vMPurchaseRequisition.FullPOStatusCount = await Task.Run(() => (_db.Procurement_PurchaseRequisition.Where(x => x.Status == (int)PRStatusEnum.FullPOCreated && x.Active).CountAsync().Result.ToString()));
                //vMPurchaseRequisition.InspectionStatusCount = await Task.Run(() => (_db.Procurement_PurchaseRequisition.Where(x => x.Status == (int)PRStatusEnum.InspectionDone && x.Active).CountAsync().Result.ToString()));
                //vMPurchaseRequisition.GoodsReceivedStatusCount = await Task.Run(() => (_db.Procurement_PurchaseRequisition.Where(x => x.Status == (int)PRStatusEnum.GoodsReceived && x.Active).CountAsync().Result.ToString()));
                //vMPurchaseRequisition.HoldStatusCount = await Task.Run(() => (_db.Procurement_PurchaseRequisition.Where(x => x.Status == (int)PRStatusEnum.Hold && x.Active).CountAsync().Result.ToString()));
            }

            //get statuswise data
            if (model.Status == PRStatusEnum.Closed)
            {


                vMPurchaseRequisition.DataList = await Task.Run(() => (_db.Procurement_PurchaseRequisition.Where(x => x.Active && !string.IsNullOrEmpty(x.CID))
                                                                    .Where(x => x.Status == (int)PRStatusEnum.Closed || x.Status == (int)PRStatusEnum.Cancel)
                                                                    .Select(x =>
                                                                         new VMPurchaseRequisition
                                                                         {
                                                                             Status = (PRStatusEnum)x.Status,
                                                                             ID = x.ID,
                                                                             CID = x.CID,
                                                                             PRType = (PRTypeEnum)x.PRType,
                                                                             OriginType = (ProcurementOriginTypeEnum)x.OriginType,
                                                                             Description = x.Description,
                                                                             VMClosedByUserFK = x.ClosedByUserFK.HasValue ? (int)x.ClosedByUserFK : 0,
                                                                             VMClosedByUserName = x.ClosedByUserFK.HasValue ? x.User_UserClosedBy.Name : "",
                                                                             CancellationRemaks = x.CancellationRemaks,
                                                                             CreationDate = x.CreationDate,
                                                                             CreatedBy = x.User,
                                                                             //ItemNames = string.Join(", ", _db.Procurement_PurchaseRequisitionSlave.Include(y => y.Common_RawItem).Where(y => y.Procurement_PurchaseRequisitionFK == x.ID).Select(y => y.Common_RawItem.Name).AsEnumerable()),
                                                                             HasSlave = _db.Procurement_PurchaseRequisitionSlave.Any(y => y.Active && y.Procurement_PurchaseRequisitionFK == x.ID),

                                                                         }).OrderByDescending(x => x.CID).ToListAsync()));
            }
            else
            {


                vMPurchaseRequisition.DataList = await Task.Run(() => (_db.Procurement_PurchaseRequisition.Include(x => x.User_UserClosedBy).Where(x => x.Active && !string.IsNullOrEmpty(x.CID) && x.Status != (int)PRStatusEnum.Closed && x.Status != (int)PRStatusEnum.Cancel)
                                                                    .Where(x => x.Status == (int)model.Status || (int)model.Status <= 0)
                                                                    .Select(x =>
                                                                         new VMPurchaseRequisition
                                                                         {
                                                                             Status = (PRStatusEnum)x.Status,
                                                                             ID = x.ID,
                                                                             CID = x.CID,
                                                                             PRType = (PRTypeEnum)x.PRType,
                                                                             OriginType = (ProcurementOriginTypeEnum)x.OriginType,
                                                                             Description = x.Description,
                                                                             VMClosedByUserFK = x.ClosedByUserFK.HasValue ? (int)x.ClosedByUserFK : 0,
                                                                             VMClosedByUserName = x.ClosedByUserFK.HasValue ? x.User_UserClosedBy.Name : "",
                                                                             CancellationRemaks = x.CancellationRemaks,
                                                                             CreationDate = x.CreationDate,
                                                                             CreatedBy = x.User,
                                                                             //ItemNames = string.Join(", ", _db.Procurement_PurchaseRequisitionSlave.Include(y => y.Common_RawItem).Where(y => y.Procurement_PurchaseRequisitionFK == x.ID).Select(y => y.Common_RawItem.Name).AsEnumerable()),
                                                                             HasSlave = _db.Procurement_PurchaseRequisitionSlave.Any(y => y.Active && y.Procurement_PurchaseRequisitionFK == x.ID),

                                                                         }).OrderByDescending(x => x.CID).ToListAsync()));
            }

            return vMPurchaseRequisition;
        }

        public async Task<VMPurchaseRequisition> PurchaseRequisitionGetSpecific(VMPurchaseRequisition model)
        {
            VMPurchaseRequisition vMPurchaseRequisition = await Task.Run(() => (from tt in _db.Procurement_PurchaseRequisition
                                                                                where tt.ID == model.ID

                                                                                select new VMPurchaseRequisition
                                                                                {
                                                                                    Status = (PRStatusEnum)tt.Status,
                                                                                    ID = tt.ID,
                                                                                    CID = tt.CID,

                                                                                    Description = tt.Description
                                                                                }).FirstOrDefaultAsync());
            return vMPurchaseRequisition;
        }

        public async Task<VMPurchaseRequisitionSlave> PRWithSlaveListGet(VMPurchaseRequisitionSlave model)
        {
            VMPurchaseRequisitionSlave vM = new VMPurchaseRequisitionSlave();
            vM.DataListSlave = new List<VMPurchaseRequisitionSlave>();

            vM.DataListSlave = await Task.Run(() => (from PR in _db.Procurement_PurchaseRequisition
                                                     .Where(x => x.Active)
                                                    .Where(x => x.ID == (int)model.Procurement_PurchaseRequisitionFK)
                                                     join PRS in _db.Procurement_PurchaseRequisitionSlave.Where(x => x.Active) on PR.ID equals PRS.Procurement_PurchaseRequisitionFK into PRS_Join
                                                     from PRS in PRS_Join.DefaultIfEmpty()
                                                     join RI in _db.Common_RawItem on PRS.Common_RawItemFK equals RI.ID into RI_Join
                                                     from RI in RI_Join.DefaultIfEmpty()
                                                     join U in _db.Common_Unit on RI.Common_UnitFK equals U.ID into U_Join
                                                     from U in U_Join.DefaultIfEmpty()
                                                     join RSC in _db.Common_RawSubCategory on RI.Common_RawSubCategoryFK equals RSC.ID into RSC_Join
                                                     from RSC in RSC_Join.DefaultIfEmpty()
                                                     join RC in _db.Common_RawCategory on RSC.Common_RawCategoryFK equals RC.ID into RC_Join
                                                     from RC in RC_Join.DefaultIfEmpty()
                                                     join E in _db.User_User on PRS.UserID equals E.ID into E_Join
                                                     from E in E_Join.DefaultIfEmpty()
                                                     join C_U in _db.User_User on PR.ClosedByUserFK equals C_U.ID into C_U_Join
                                                     from C_U in C_U_Join.DefaultIfEmpty()
                                                     join MSS in _db.Merchandising_StyleSlave on PRS.Merchandising_StyleSlaveFK equals MSS.ID into MSS_Join
                                                     from MSS in MSS_Join.DefaultIfEmpty()
                                                     join BOF in _db.Merchandising_BOF on PRS.Merchandising_BOFFK equals BOF.ID into BOF_Join
                                                     from BOF in BOF_Join.DefaultIfEmpty()
                                                     join MS in _db.Merchandising_Style on PRS.Merchandising_StyleID equals MS.ID into MS_Join
                                                     from MS in MS_Join.DefaultIfEmpty()
                                                     join MBO in _db.Merchandising_BuyerOrder on MS.Merchandising_BuyerOrderFK equals MBO.ID into MBO_Join
                                                     from MBO in MBO_Join.DefaultIfEmpty()
                                                     join C in _db.Common_Color on MSS.Common_ColorFK equals C.ID into C_Join
                                                     from C in C_Join.DefaultIfEmpty()
                                                     select new VMPurchaseRequisitionSlave
                                                     {
                                                         CID = PR.CID,

                                                         PRType = (PRTypeEnum)PR.PRType,
                                                         OriginType = (ProcurementOriginTypeEnum)PR.OriginType,

                                                         CreationDate = PR.CreationDate,
                                                         Description = PR.Description,
                                                         Status = (PRStatusEnum)PR.Status,
                                                         VMClosedByUserFK = PR.ClosedByUserFK.HasValue ? (int)PR.ClosedByUserFK : 0,
                                                         VMClosedByUserName = C_U != null ? C_U.Name : "",
                                                         CancellationRemaks = PR.CancellationRemaks,

                                                         VMMerchandising_StyleSlaveID = PRS != null ? PRS.Merchandising_StyleSlaveFK.HasValue ? PRS.Merchandising_StyleSlaveFK.Value : 0 : 0,
                                                         BONoStyleName = MS != null && MBO != null ? MBO.BuyerPO + "/" + MS.StyleName : "",
                                                         UserID = E != null ? E.ID : 0,
                                                         VMEmployeeName = E != null ? E.Name : "",
                                                         Procurement_PurchaseRequisitionFK = PRS != null ? PRS.Procurement_PurchaseRequisitionFK : PR.ID,
                                                         ID = PRS != null ? PRS.ID : 0,
                                                         DescriptionSlave = PRS != null ? PRS.Description : "",
                                                         RequisitionQuantity = PRS != null ? PRS.RequisitionQuantity : 0,
                                                         StyleQuantity = MSS != null ? (double)MSS.RequiredQty : BOF != null ? (double)(BOF.RequiredQuantity.HasValue ? BOF.RequiredQuantity.Value : 0) : 0,
                                                         /*PRRaisedQuantity = (PRS != null ? PRS.RequisitionQuantity : 0) -
                                                         (from PRS in _db.Procurement_PurchaseRequisitionSlave.Include(x => x.Procurement_PurchaseRequisition)
                                                          .Where(x => x.Common_RawItemFK == PRS.Common_RawItemFK
                                                          && x.Procurement_PurchaseRequisition.Active
                                                          && !string.IsNullOrEmpty(x.Procurement_PurchaseRequisition.CID)
                                                          && x.Active
                                                          && (!x.Merchandising_StyleID.HasValue || x.Merchandising_StyleID == (MS != null ? MS.ID : (int?)null))
                                                          && (!x.Merchandising_StyleSlaveFK.HasValue || x.Merchandising_StyleSlaveFK == (MSS != null ? MSS.ID : (int?)null))
                                                          && (!x.Merchandising_BOFFK.HasValue || x.Merchandising_BOFFK == (BOF != null ? BOF.ID : (int?)null)))
                                                          select new { Quantity = PRS.RequisitionQuantity })
                                                          .Sum(x => x.Quantity),*/
                                                         StyleColorName = MSS != null && C != null ? C.Name : "",
                                                         StyleGSM = MSS != null ? (double)MSS.GSM : 0,
                                                         InspectionRequired = PRS != null ? PRS.InspectionRequired : false,
                                                         RequiredDate = PRS != null ? PRS.RequiredDate : DateTime.MinValue,
                                                         SupplierNames = PRS != null ? PRS.SupplierNames : "",
                                                         VMRawItemFK = RI != null ? RI.ID : 0,
                                                         VMRawItemName = RI != null ? RI.Name : "",
                                                         VMRawCategoryFK = RC != null ? RC.ID : 0,
                                                         VMRawCategoryName = RC != null ? RC.Name : "",
                                                         VMRawSubCategoryFK = RSC != null ? RSC.ID : 0,
                                                         VMRawSubCategoryName = RSC != null ? RSC.Name : "",
                                                         VMUnitName = U != null ? U.Name : "",
                                                         //AvailableQuantity = RI != null ? RI.Quantity : 0 //AvailableQuantity has to come from Store
                                                     }).OrderByDescending(x => x.ID).ToListAsync());

            VMPurchaseRequisitionSlave vMSlave = new VMPurchaseRequisitionSlave();
            if (vM.DataListSlave != null && vM.DataListSlave.Count > 0)
            {
                vMSlave.Procurement_PurchaseRequisitionFK = vM.DataListSlave[0].Procurement_PurchaseRequisitionFK;
                vMSlave.CID = vM.DataListSlave[0].CID;
                vMSlave.PRType = vM.DataListSlave[0].PRType;
                vMSlave.OriginType = vM.DataListSlave[0].OriginType;
                vMSlave.CreatedBy = vM.DataListSlave[0].CreatedBy;
                vMSlave.CreationDate = vM.DataListSlave[0].CreationDate;
                vMSlave.Description = vM.DataListSlave[0].Description;
                vMSlave.Status = (PRStatusEnum)vM.DataListSlave[0].Status;
                vMSlave.VMDepartmentName = vM.DataListSlave[0].VMDepartmentName;
                vMSlave.VMClosedByUserFK = vM.DataListSlave[0].VMClosedByUserFK;
                vMSlave.VMClosedByUserName = vM.DataListSlave[0].VMClosedByUserName;
                vMSlave.CancellationRemaks = vM.DataListSlave[0].CancellationRemaks;
            }
            else
            {
                vM.DataListSlave = new List<VMPurchaseRequisitionSlave>();
            }


            vMSlave.DataListSlave = new List<VMPurchaseRequisitionSlave>();

            vM.DataListSlave.ForEach(PRD =>
            {
                PRD.PRRaisedQuantity = PRD.RequisitionQuantity -
                             (from PRS in _db.Procurement_PurchaseRequisitionSlave.Include(x => x.Procurement_PurchaseRequisition)
                              .Where(x => x.Common_RawItemFK == PRD.VMRawItemFK
                              && x.Procurement_PurchaseRequisition.Active
                              && !string.IsNullOrEmpty(x.Procurement_PurchaseRequisition.CID)
                              && x.Active
                              && (x.Merchandising_StyleID == PRD.VMMerchandising_StyleID)
                              && (x.Merchandising_StyleSlaveFK == PRD.VMMerchandising_StyleSlaveID)
                              && (x.Merchandising_BOFFK == PRD.VMMerchandising_BOFID))
                              select new { Quantity = PRS.RequisitionQuantity })
                              .Sum(x => x.Quantity);
            });

            if (vM.DataListSlave.Count > 0 && vM.DataListSlave[0].ID > 0)
            {
                vMSlave.HasSlave = true;
                vMSlave.DataListSlave = vM.DataListSlave;
            }

            return vMSlave;
        }

        public async Task<int> PurchaseRequisitionAdd(VMPurchaseRequisition model)
        {
            var result = -1;
            Procurement_PurchaseRequisition enProcurement_PurchaseRequisition = new Procurement_PurchaseRequisition
            {
                //CID = await Task.Run(() => (((_db.Procurement_PurchaseRequisition.AnyAsync().Result ? _db.Procurement_PurchaseRequisition.MaxAsync(x => x.ID).Result : 0) + 1).ToString().PadLeft(5, '0'))),
                Status = (int)model.Status,
                CreationDate = model.CreationDate,
                Description = model.Description,
                User = model.User,
                UserID = model.UserID,
                PRType = (int)model.PRType,
                OriginType = (int)model.OriginType
            };

            try
            {
                //using (var trans = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted }, EnterpriseServicesInteropOption.Automatic))
                //{
                // Perform operations using your DC, including submitting changes

                _db.Procurement_PurchaseRequisition.Add(enProcurement_PurchaseRequisition);
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = enProcurement_PurchaseRequisition.ID;
                }

                //    if (result > 0)
                //    {
                //        trans.Complete();
                //    }
                //}

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }
        public async Task<int> PurchaseRequisitionEdit(VMPurchaseRequisition model)
        {
            var result = -1;
            Procurement_PurchaseRequisition enPR = await Task.Run(() => (_db.Procurement_PurchaseRequisition.FindAsync(model.ID)));

            if (!string.IsNullOrEmpty(model.Description))
            {
                enPR.Description = model.Description;
            }

            if (await _db.SaveChangesAsync() > 0)
            {
                result = enPR.ID;
            }
            return result;
        }
        public async Task<int> PurchaseRequisitionStatusUpdate(VMPurchaseRequisition model)
        {
            var result = -1;
            Procurement_PurchaseRequisition enpr = await _db.Procurement_PurchaseRequisition.FindAsync(model.ID);
            if (model.ActionId > 3)
            {
                if (model.PRActionEnum == PRActionEnum.Submit)
                {
                    enpr.Status = (int)PRStatusEnum.Submitted;
                }
                if (model.PRActionEnum == PRActionEnum.UndoSubmit)
                {
                    enpr.Status = (int)PRStatusEnum.Draft;
                }

                if (model.PRActionEnum == PRActionEnum.PrimaryApprove)
                {
                    enpr.Status = (int)PRStatusEnum.PrimaryApproved;
                }
                if (model.PRActionEnum == PRActionEnum.UndoPrimaryApprove)
                {
                    enpr.Status = (int)PRStatusEnum.Submitted;
                }

                if (model.PRActionEnum == PRActionEnum.FinalApprove)
                {
                    enpr.Status = (int)PRStatusEnum.FinalApproved;
                }
                if (model.PRActionEnum == PRActionEnum.UndoFinalApprove)
                {
                    enpr.Status = (int)PRStatusEnum.PrimaryApproved;
                }

                if (model.PRActionEnum == PRActionEnum.ProcurementApprove)
                {
                    enpr.Status = (int)PRStatusEnum.ProcurementApproved;
                }
                if (model.PRActionEnum == PRActionEnum.UndoProcurementApprove)
                {
                    enpr.Status = (int)PRStatusEnum.FinalApproved;
                }

                if (model.PRActionEnum == PRActionEnum.Cancel)
                {
                    enpr.Status = (int)PRStatusEnum.Cancel;
                    //assign closed by User
                    enpr.ClosedByUserFK = 1;
                }

                if (model.PRActionEnum == PRActionEnum.Hold)
                {
                    enpr.Status = (int)PRStatusEnum.Hold;
                }

                if (model.PRActionEnum == PRActionEnum.Close)
                {
                    enpr.Status = (int)PRStatusEnum.Closed;
                    //assign closed by User
                    enpr.ClosedByUserFK = 1;
                }
                if (model.PRActionEnum == PRActionEnum.ReOpen)
                {
                    enpr.Status = (int)PRStatusEnum.Draft;
                    enpr.ClosedByUserFK = null;
                }

                if (model.PRActionEnum == PRActionEnum.Finalize)
                {
                    if (string.IsNullOrEmpty(enpr.CID))
                    {
                        string prid = @"PR" +
                            DateTime.Now.ToString("yy") +
                            DateTime.Now.ToString("MM") + @"-" +
                            (((ProcurementOriginTypeEnum)enpr.OriginType == ProcurementOriginTypeEnum.Regular) ? "G" : "O") +
                            @"-P";
                        enpr.CID = prid + await Task.Run(() => (((_db.Procurement_PurchaseRequisition.AnyAsync(x => x.CID.ToUpper().Contains(prid.ToUpper())).Result ? _db.Procurement_PurchaseRequisition.Where(x => x.CID.ToUpper().Contains(prid.ToUpper())).MaxAsync(x => Int32.Parse(!string.IsNullOrEmpty(x.CID.Replace(prid, "")) ? x.CID.Replace(prid, "") : "0")).Result : 0) + 1).ToString().PadLeft(6, '0')));
                    }


                    if (!string.IsNullOrEmpty(model.Description))
                    {
                        enpr.Description = model.Description;
                    }
                }
                if (model.PRActionEnum == PRActionEnum.UndoFinalize)
                {
                    enpr.CID = "";
                }
            }

            //else if (model.ActionEum == PRActionEnum.Close)
            //{
            //    enpr.Status = (int)PRStatusEnum.Closed;
            //}
            //else if (model.ActionEum == PRActionEnum.UnApprove)
            //{
            //    enpr.Status = (int)PRStatusEnum.Draft;
            //}
            //else if (model.ActionEum == PRActionEnum.ReOpen)
            //{
            //    enpr.Status = (int)PRStatusEnum.Draft;
            //}


            if (await _db.SaveChangesAsync() > 0)
            {
                result = enpr.ID;
            }
            return result;
        }
        public async Task<int> PurchaseRequisitionDelete(int id)
        {
            var result = -1;
            Procurement_PurchaseRequisition selectSingle = await _db.Procurement_PurchaseRequisition.FindAsync(id);
            if (selectSingle != null)
            {
                selectSingle.Active = false;
                if (await _db.SaveChangesAsync() > 0)
                {
                    await _db.Procurement_PurchaseRequisitionSlave.Where(y => y.Procurement_PurchaseRequisitionFK == selectSingle.ID).ForEachAsync(x => x.Active = false);

                    await _db.SaveChangesAsync();

                    result = selectSingle.ID;
                }
            }
            return result;
        }

        public List<object> ApprovedStyleList(int prID = 0)
        {
            return integrationService.DDLBOMStyleList().Where(x => ((double)x.GetType().GetProperty("TotalQty").GetValue(x, null)) >
            (from PRS in _db.Procurement_PurchaseRequisitionSlave.Include(y => y.Procurement_PurchaseRequisition)
                                                          .Where(y => y.Procurement_PurchaseRequisition.Active && (y.Procurement_PurchaseRequisition.ID != prID)
                                                          && (!String.IsNullOrEmpty(y.Procurement_PurchaseRequisition.CID))
                                                          && y.Active && (y.Merchandising_StyleID == (int)x.GetType().GetProperty("Value").GetValue(x, null)))
             select new { Quantity = PRS.RequisitionQuantity }).Sum(y => y.Quantity)
            ).ToList();

        }
        public VMPurchaseRequisitionSlave PRSlaveFromStyleSlaveGet(int id, int PRID, int typeID, int orgID)
        {
            //////send orgID to BOMGetByStyleID for DataListSlave (1=Combined,2=BOM,3=BOF)
            VMPurchaseRequisitionSlave vM = new VMPurchaseRequisitionSlave();
            vM.Procurement_PurchaseRequisitionFK = PRID;
            vM.PRType = (PRTypeEnum)typeID;
            vM.OriginType = (ProcurementOriginTypeEnum)orgID;
            vM.DataListSlave = new List<VMPurchaseRequisitionSlave>();
            vM.DataListSlave = integrationService.BOMGetByStyleID(id, orgID).Select(x => new VMPurchaseRequisitionSlave()
            {
                PRType = (PRTypeEnum)typeID,
                OriginType = (ProcurementOriginTypeEnum)orgID,
                Procurement_PurchaseRequisitionFK = PRID,
                VMMerchandising_StyleID = x.Merchandising_StyleFK,
                VMMerchandising_StyleSlaveID = x.IsBOM ? x.ID : (int?)null,
                VMMerchandising_BOFID = !x.IsBOM ? x.ID : (int?)null,
                VMRawItemFK = x.Common_RawItemFK,

                VMRawItemName = string.IsNullOrEmpty(x.Size) ? x.RawItem : x.RawItem + " ( " + x.Size + " )",
                DescriptionSlave = x.Description,
                UserList = new SelectList(UserDropDownList(), "Value", "Text"),
                StyleQuantity = (double)x.RequiredQty,
                PRRaisedQuantity = (from PRS in _db.Procurement_PurchaseRequisitionSlave.Include(y => y.Procurement_PurchaseRequisition)
                                                          .Where(y => y.Procurement_PurchaseRequisition.Active
                                                          && (!String.IsNullOrEmpty(y.Procurement_PurchaseRequisition.CID) && (PRID <= 0 || y.Procurement_PurchaseRequisition.ID != PRID))
                                                          && y.Active
                                                          && (y.Common_RawItemFK == x.Common_RawItemFK)
                                                          && (y.Merchandising_StyleID == x.Merchandising_StyleFK)
                                                          && ((y.Merchandising_StyleSlaveFK == x.ID && x.IsBOM) || (!y.Merchandising_StyleSlaveFK.HasValue))
                                                          && ((y.Merchandising_BOFFK == x.ID && !x.IsBOM) || (!y.Merchandising_BOFFK.HasValue)))
                                    select new { Quantity = PRS.RequisitionQuantity }).Sum(y => y.Quantity),
                VMUnitName = x.UnitName,
            }).ToList();

            vM.DataListSlave = vM.DataListSlave.Where(x => x.RestQuantity > 0).ToList();

            return vM;
        }

        public async Task<int> PRSlaveAdd(VMPurchaseRequisitionSlave vM)
        {
            var result = -1;

            Procurement_PurchaseRequisitionSlave pRSlave = await Task.Run(() => (_db.Procurement_PurchaseRequisitionSlave.Where(x => x.Procurement_PurchaseRequisitionFK == vM.Procurement_PurchaseRequisitionFK && x.Common_RawItemFK == vM.VMRawItemFK && x.Merchandising_StyleID == vM.VMMerchandising_StyleID && x.Merchandising_StyleSlaveFK == vM.VMMerchandising_StyleSlaveID && x.Merchandising_BOFFK == vM.VMMerchandising_BOFID && x.Active).FirstOrDefaultAsync()));
            if (pRSlave == null)
            {
                pRSlave = new Procurement_PurchaseRequisitionSlave
                {
                    Procurement_PurchaseRequisitionFK = vM.Procurement_PurchaseRequisitionFK,
                    RequisitionQuantity = vM.RequisitionQuantity,
                    Description = vM.DescriptionSlave,
                    Common_RawItemFK = vM.VMRawItemFK,
                    SupplierNames = vM.SupplierNames,
                    RequiredDate = vM.RequiredDate,
                    InspectionRequired = vM.InspectionRequired,
                    User = vM.User,
                    UserID = vM.UserID,

                };
                _db.Procurement_PurchaseRequisitionSlave.Add(pRSlave);
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = pRSlave.ID;
                }
            }

            return result;
        }
        public async Task<int> PRSlaveListAdd(List<VMPurchaseRequisitionSlave> vM)
        {
            var result = -1;
            if (vM == null || vM.Count <= 0) { return result; }
            VMPurchaseRequisitionSlave vMPR = vM.FirstOrDefault();
            int nPRID = vMPR.Procurement_PurchaseRequisitionFK;
            int prType = (int)vMPR.PRType;
            int orgType = (int)vMPR.OriginType;
            string sUser = vMPR.User;
            int nUserId = vMPR.UserID;

            Procurement_PurchaseRequisition enPR = new Procurement_PurchaseRequisition() { Status = (int)PRStatusEnum.Draft, PRType = prType, OriginType = orgType };
            if (nPRID > 0)
            {
                enPR = _db.Procurement_PurchaseRequisition.Find(nPRID);
                if (enPR.Active)
                {
                    vM = vM.Where(x => !_db.Procurement_PurchaseRequisitionSlave
                    .Any(y => y.Active
                    && y.Procurement_PurchaseRequisitionFK == enPR.ID
                    && y.Common_RawItemFK == x.VMRawItemFK
                    && x.VMMerchandising_StyleID == y.Merchandising_StyleID
                    && x.VMMerchandising_StyleSlaveID == y.Merchandising_StyleSlaveFK
                    && x.VMMerchandising_BOFID == y.Merchandising_BOFFK)).ToList();
                }
                else
                {
                    enPR = new Procurement_PurchaseRequisition() { Status = (int)PRStatusEnum.Draft, PRType = prType, OriginType = orgType };
                }

            }
            enPR.User = sUser;
            enPR.UserID = nUserId;

            enPR.Procurement_PurchaseRequisitionSlave = new List<Procurement_PurchaseRequisitionSlave>();
            enPR.Procurement_PurchaseRequisitionSlave = vM.Select(x => new Procurement_PurchaseRequisitionSlave
            {
                Merchandising_StyleID = x.VMMerchandising_StyleID,
                Merchandising_StyleSlaveFK = x.VMMerchandising_StyleSlaveID,
                Merchandising_BOFFK = x.VMMerchandising_BOFID,
                RequisitionQuantity = x.RequisitionQuantity,
                Description = x.DescriptionSlave,
                Common_RawItemFK = x.VMRawItemFK,
                SupplierNames = x.SupplierNames,
                RequiredDate = x.RequiredDate,
                InspectionRequired = x.InspectionRequired,
                User = sUser,
                UserID = nUserId,

            }).ToList();
            if (enPR.ID <= 0)
            {
                await _db.Procurement_PurchaseRequisition.AddRangeAsync(enPR);
            }

            if (await _db.SaveChangesAsync() > 0)
            {
                result = enPR.ID;
            }
            return result;
        }
        public async Task<int> PRSlaveEdit(VMPurchaseRequisitionSlave vM)
        {
            var result = -1;
            Procurement_PurchaseRequisitionSlave pRSlave = await _db.Procurement_PurchaseRequisitionSlave.FindAsync(vM.ID);
            if (vM.RequisitionQuantity > 0)
            {
                pRSlave.RequisitionQuantity = vM.RequisitionQuantity;
            }
            if (!string.IsNullOrEmpty(vM.DescriptionSlave))
            {
                pRSlave.Description = vM.DescriptionSlave;
            }
            if (vM.VMRawItemFK > 0)
            {
                pRSlave.Common_RawItemFK = vM.VMRawItemFK;
            }
            if (vM.UserID > 0)
            {
                pRSlave.UserID = vM.UserID;
            }
            if (!string.IsNullOrEmpty(vM.SupplierNames))
            {
                pRSlave.SupplierNames = vM.SupplierNames;
            }
            if (vM.RequiredDate != DateTime.MinValue)
            {
                pRSlave.RequiredDate = vM.RequiredDate;
            }
            pRSlave.InspectionRequired = vM.InspectionRequired;

            if (await _db.SaveChangesAsync() > 0)
            {
                result = pRSlave.ID;
            }
            return result;
        }
        public async Task<int> PRSlaveDelete(int id)
        {
            var result = -1;
            Procurement_PurchaseRequisitionSlave selectSingle = await _db.Procurement_PurchaseRequisitionSlave.FindAsync(id);
            if (selectSingle != null)
            {
                selectSingle.Active = false;
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = selectSingle.ID;
                }
            }
            return result;
        }

        public async Task<VMRawItem> RawItemInfoGet(int id)
        {
            VMRawItem vM = new VMRawItem();

            Common_RawItem enRI = await Task.Run(() => (from ri in _db.Common_RawItem.Include(x => x.Common_Unit)
                                                        join rsc in _db.Common_RawSubCategory.Include(x => x.Common_RawCategory) on ri.Common_RawSubCategoryFK equals rsc.ID

                                                        select new Common_RawItem
                                                        {
                                                            ID = ri.ID,
                                                            Name = ri.Name,
                                                            Common_Unit = ri.Common_Unit,
                                                            Common_RawSubCategory = rsc,
                                                            Quantity = ri.Quantity
                                                        }
                                                        ).FirstOrDefaultAsync(x => x.ID == id));

            if (enRI != null)
            {
                vM.ID = enRI.ID;
                vM.Name = enRI.Name;
                vM.Unit = enRI.Common_Unit.Name;
                vM.Quantity = enRI.Quantity;
                vM.RawCategoryFK = enRI.Common_RawSubCategory.Common_RawCategory.ID;
                vM.RawSubCategoryFK = enRI.Common_RawSubCategory.ID;
            }
            return vM;
        }
        public async Task<List<VMRawSubCategory>> RawSubCategoryGet(int id)
        {

            List<VMRawSubCategory> vMRSC = await Task.Run(() => (_db.Common_RawSubCategory.Where(x => id <= 0 || x.Common_RawCategoryFK == id)).Select(x => new VMRawSubCategory() { ID = x.ID, Name = x.Name }).ToListAsync());


            return vMRSC;
        }
        public async Task<List<VMRawItem>> RawItemGet(int id)
        {
            List<VMRawItem> vMRI = await Task.Run(() => (_db.Common_RawItem.Where(x => id <= 0 || x.Common_RawSubCategoryFK == id).Select(x => new VMRawItem() { ID = x.ID, Name = x.Name })).ToListAsync());

            return vMRI;
        }

        #endregion

        #region PurchaseOrder
        public async Task<List<VMPurchaseOrder>> POListGetForAccounts(DateTime FromDate, DateTime ToDate, int nSupplierId = 0)
        {
            List<VMPurchaseOrder> vMPurchaseOrder = new List<VMPurchaseOrder>();
            vMPurchaseOrder = await Task.Run(() => (from PO in _db.Procurement_PurchaseOrder
                                                    join C in _db.Common_Currency on PO.Common_CurrencyFK equals C.ID into C_Join
                                                    from C in C_Join.DefaultIfEmpty()
                                                    join E in _db.User_User on PO.UserID equals E.ID into E_Join
                                                    from E in E_Join.DefaultIfEmpty()
                                                    join S in _db.Common_Supplier on PO.Common_SupplierFK equals S.ID into S_Join
                                                    from S in S_Join.DefaultIfEmpty()
                                                    where PO.Active && PO.Status == (int)POStatusEnum.FinalApproved
                                                    && (PO.OrderDate >= FromDate && PO.OrderDate <= ToDate)
                                                    && (nSupplierId == 0 || PO.Common_SupplierFK == nSupplierId)
                                                    select new VMPurchaseOrder
                                                    {
                                                        ID = PO.ID,
                                                        CID = PO.CID,
                                                        OriginType = (ProcurementOriginTypeEnum)PO.OriginType,
                                                        OrderDate = PO.OrderDate,
                                                        POPaymentMethod = (SupplierPaymentMethodEnum)PO.POPaymentMethod,
                                                        POType = (POTypeEnum)PO.POType,
                                                        CreationDate = PO.CreationDate,
                                                        CreatedBy = PO.CreatedBy,
                                                        ApprovedDate = PO.ApprovedDate.HasValue ? PO.ApprovedDate.Value : DateTime.MinValue,
                                                        RevisionNo = PO.RevisionNo,
                                                        Description = PO.Description,
                                                        Status = (POStatusEnum)PO.Status,
                                                        VMEmployeeFK = E != null ? E.ID : 0,
                                                        VMEmployeeName = E != null ? E.Name : "",
                                                        VMSupplierFK = PO.Common_SupplierFK,
                                                        VMSupplierName = S.Name,
                                                        VMCurrencyFK = PO.Common_CurrencyFK,
                                                        VMCurrencyName = C.Name,
                                                        TermsAndCondition = PO.TermsAndCondition,
                                                        TotalPOValue = PO.TotalPOValue
                                                    }).ToListAsync());

            return vMPurchaseOrder;
        }
        public async Task<VMPurchaseOrder> PurchaseOrderGet(VMPurchaseOrder model)
        {
            VMPurchaseOrder vMPurchaseOrder = new VMPurchaseOrder();

            vMPurchaseOrder.DataList = new List<VMPurchaseOrder>();
            vMPurchaseOrder.Status = model.Status;


            if (model.Status != POStatusEnum.Closed)
            {
                await Task.Run(() => (from ORD in _db.Procurement_PurchaseOrder
                                      where ORD.Active && (ORD.Status != (int)POStatusEnum.Closed && ORD.Status != (int)POStatusEnum.Cancel)
                                      group ORD by ORD.Status into g
                                      select new { Status = (POStatusEnum)g.Key, Count = g.Count().ToString() }).ForEachAsync(x =>
                                           {
                                               if (x.Status == POStatusEnum.Draft)
                                               {
                                                   vMPurchaseOrder.DraftStatusCount = x.Count;
                                               }
                                               if (x.Status == POStatusEnum.Submitted)
                                               {
                                                   vMPurchaseOrder.SubmittedStatusCount = x.Count;
                                               }
                                               if (x.Status == POStatusEnum.PrimaryApproved)
                                               {
                                                   vMPurchaseOrder.PrimaryStatusCount = x.Count;
                                               }
                                               if (x.Status == POStatusEnum.FinalApproved)
                                               {
                                                   vMPurchaseOrder.FinalStatusCount = x.Count;
                                               }
                                               if (x.Status == POStatusEnum.SecondaryApproved)
                                               {
                                                   vMPurchaseOrder.SecondaryStatusCount = x.Count;
                                               }
                                               if (x.Status == POStatusEnum.Hold)
                                               {
                                                   vMPurchaseOrder.HoldStatusCount = x.Count;
                                               }
                                               if (x.Status == POStatusEnum.PartialGoodsReceived)
                                               {
                                                   vMPurchaseOrder.PartialGoodsReceivedStatusCount = x.Count;
                                               }
                                               if (x.Status == POStatusEnum.GoodsReceived)
                                               {
                                                   vMPurchaseOrder.GoodsReceivedStatusCount = x.Count;
                                               }

                                           }));



                //vMPurchaseOrder.DraftStatusCount = await Task.Run(() => (_db.Procurement_PurchaseOrder.Where(x => x.Status == (int)POStatusEnum.Draft && x.Active).CountAsync().Result.ToString()));
                //vMPurchaseOrder.SubmittedStatusCount = await Task.Run(() => (_db.Procurement_PurchaseOrder.Where(x => x.Status == (int)POStatusEnum.Submitted && x.Active).CountAsync().Result.ToString()));
                //vMPurchaseOrder.PrimaryStatusCount = await Task.Run(() => (_db.Procurement_PurchaseOrder.Where(x => x.Status == (int)POStatusEnum.PrimaryApproved && x.Active).CountAsync().Result.ToString()));
                //vMPurchaseOrder.FinalStatusCount = await Task.Run(() => (_db.Procurement_PurchaseOrder.Where(x => x.Status == (int)POStatusEnum.FinalApproved && x.Active).CountAsync().Result.ToString()));
                //vMPurchaseOrder.SecondaryStatusCount = await Task.Run(() => (_db.Procurement_PurchaseOrder.Where(x => x.Status == (int)POStatusEnum.SecondaryApproved && x.Active).CountAsync().Result.ToString()));

                //vMPurchaseOrder.InspectionStatusCount = await Task.Run(() => (_db.Procurement_PurchaseOrder.Where(x => x.Status == (int)POStatusEnum.InspectionDone && x.Active).CountAsync().Result.ToString()));
                //vMPurchaseOrder.GoodsReceivedStatusCount = await Task.Run(() => (_db.Procurement_PurchaseOrder.Where(x => x.Status == (int)POStatusEnum.GoodsReceived && x.Active).CountAsync().Result.ToString()));
                //vMPurchaseOrder.PartialGoodsReceivedStatusCount = await Task.Run(() => (_db.Procurement_PurchaseOrder.Where(x => x.Status == (int)POStatusEnum.PartialGoodsReceived && x.Active).CountAsync().Result.ToString()));
                //vMPurchaseOrder.HoldStatusCount = await Task.Run(() => (_db.Procurement_PurchaseOrder.Where(x => x.Status == (int)POStatusEnum.Hold && x.Active).CountAsync().Result.ToString()));
            }


            if (model.Status == POStatusEnum.Closed)
            {
                vMPurchaseOrder.DataList = await Task.Run(() => (from PO in _db.Procurement_PurchaseOrder.Where(x => x.Active).Where(x => x.Status == (int)POStatusEnum.Closed || x.Status == (int)POStatusEnum.Cancel)
                                                                 join C in _db.Common_Currency on PO.Common_CurrencyFK equals C.ID into C_Join
                                                                 from C in C_Join.DefaultIfEmpty()
                                                                 join E in _db.User_User on PO.UserID equals E.ID into E_Join
                                                                 from E in E_Join.DefaultIfEmpty()
                                                                 join S in _db.Common_Supplier on PO.Common_SupplierFK equals S.ID into S_Join
                                                                 from S in S_Join.DefaultIfEmpty()
                                                                 where PO.Active == true
                                                                 select new VMPurchaseOrder
                                                                 {
                                                                     ID = PO.ID,
                                                                     CID = PO.CID,
                                                                     OriginType = (ProcurementOriginTypeEnum)PO.OriginType,
                                                                     OrderDate = PO.OrderDate,
                                                                     POPaymentMethod = (SupplierPaymentMethodEnum)PO.POPaymentMethod,
                                                                     POType = (POTypeEnum)PO.POType,
                                                                     CreationDate = PO.CreationDate,
                                                                     CreatedBy = PO.CreatedBy,
                                                                     ApprovedDate = PO.ApprovedDate.HasValue ? PO.ApprovedDate.Value : DateTime.MinValue,
                                                                     RevisionNo = PO.RevisionNo,
                                                                     Description = PO.Description,
                                                                     Status = (POStatusEnum)PO.Status,
                                                                     VMEmployeeFK = E != null ? E.ID : 0,
                                                                     VMEmployeeName = E != null ? E.Name : "",
                                                                     VMSupplierFK = PO.Common_SupplierFK,
                                                                     VMSupplierName = S.Name,
                                                                     VMCurrencyFK = PO.Common_CurrencyFK,
                                                                     VMCurrencyName = C.Name,
                                                                     TermsAndCondition = PO.TermsAndCondition,
                                                                     HasSlave = _db.Procurement_PurchaseOrderSlave.Any(y => y.Active && y.Procurement_PurchaseOrderFK == PO.ID)
                                                                 }).OrderByDescending(x => x.ID).ToListAsync());
            }
            else
            {
                vMPurchaseOrder.DataList = await Task.Run(() => (from PO in _db.Procurement_PurchaseOrder.Where(x => x.Active && x.Status != (int)POStatusEnum.Closed && x.Status != (int)POStatusEnum.Cancel).Where(x => x.Status == (int)model.Status || (int)model.Status <= 0)
                                                                 join C in _db.Common_Currency on PO.Common_CurrencyFK equals C.ID into C_Join
                                                                 from C in C_Join.DefaultIfEmpty()
                                                                 join E in _db.User_User on PO.UserID equals E.ID into E_Join
                                                                 from E in E_Join.DefaultIfEmpty()
                                                                 join S in _db.Common_Supplier on PO.Common_SupplierFK equals S.ID into S_Join
                                                                 from S in S_Join.DefaultIfEmpty()
                                                                 where PO.Active == true
                                                                 select new VMPurchaseOrder
                                                                 {
                                                                     ID = PO.ID,
                                                                     CID = PO.CID,
                                                                     OriginType = (ProcurementOriginTypeEnum)PO.OriginType,
                                                                     OrderDate = PO.OrderDate,
                                                                     POPaymentMethod = (SupplierPaymentMethodEnum)PO.POPaymentMethod,
                                                                     POType = (POTypeEnum)PO.POType,
                                                                     CreationDate = PO.CreationDate,
                                                                     CreatedBy = PO.CreatedBy,
                                                                     ApprovedDate = PO.ApprovedDate.HasValue ? PO.ApprovedDate.Value : DateTime.MinValue,
                                                                     RevisionNo = PO.RevisionNo,
                                                                     Description = PO.Description,
                                                                     Status = (POStatusEnum)PO.Status,
                                                                     VMEmployeeFK = E != null ? E.ID : 0,
                                                                     VMEmployeeName = E != null ? E.Name : "",
                                                                     VMSupplierFK = PO.Common_SupplierFK,
                                                                     VMSupplierName = S.Name,
                                                                     VMCurrencyFK = PO.Common_CurrencyFK,
                                                                     VMCurrencyName = C.Name,
                                                                     TermsAndCondition = PO.TermsAndCondition,
                                                                     HasSlave = _db.Procurement_PurchaseOrderSlave.Any(y => y.Active && y.Procurement_PurchaseOrderFK == PO.ID)
                                                                 }).OrderByDescending(x => x.ID).ToListAsync());
            }



            return vMPurchaseOrder;
        }



        public async Task<VMPurchaseOrder> PurchaseOrderGetSpecific(VMPurchaseOrder model)
        {
            VMPurchaseOrder vMPurchaseOrder = new VMPurchaseOrder();
            vMPurchaseOrder = await Task.Run(() => (from PO in _db.Procurement_PurchaseOrder
                                                    join C in _db.Common_Currency on PO.Common_CurrencyFK equals C.ID into C_Join
                                                    from C in C_Join.DefaultIfEmpty()
                                                    join E in _db.HRMS_Employee on PO.UserID equals E.ID into E_Join
                                                    from E in E_Join.DefaultIfEmpty()
                                                    join S in _db.Common_Supplier on PO.Common_SupplierFK equals S.ID into S_Join
                                                    from S in S_Join.DefaultIfEmpty()
                                                    where PO.ID == model.ID && PO.Active == true
                                                    select new VMPurchaseOrder
                                                    {
                                                        ID = PO.ID,
                                                        CID = PO.CID,
                                                        OriginType = (ProcurementOriginTypeEnum)PO.OriginType,
                                                        OrderDate = PO.OrderDate,
                                                        POPaymentMethod = (SupplierPaymentMethodEnum)PO.POPaymentMethod,
                                                        POType = (POTypeEnum)PO.POType,
                                                        CreationDate = PO.CreationDate,
                                                        CreatedBy = PO.CreatedBy,
                                                        ApprovedDate = PO.ApprovedDate.HasValue ? PO.ApprovedDate.Value : DateTime.MinValue,
                                                        RevisionNo = PO.RevisionNo,
                                                        Description = PO.Description,
                                                        Status = (POStatusEnum)PO.Status,
                                                        VMEmployeeFK = E != null ? E.ID : 0,
                                                        VMEmployeeName = E != null ? E.Name : "",
                                                        VMSupplierFK = PO.Common_SupplierFK,
                                                        VMSupplierName = S.Name,
                                                        VMCurrencyFK = PO.Common_CurrencyFK,
                                                        VMCurrencyName = C.Name,
                                                        TermsAndCondition = PO.TermsAndCondition,
                                                        HasSlave = _db.Procurement_PurchaseOrderSlave.Any(y => y.Active && y.Procurement_PurchaseOrderFK == PO.ID)
                                                    }).FirstOrDefaultAsync());

            return vMPurchaseOrder;
        }

        public async Task<VMPurchaseOrderSlave> POWithSlaveListGet(VMPurchaseOrderSlave model)
        {

            VMPurchaseOrderSlave vM = new VMPurchaseOrderSlave();
            vM.DataListSlave = new List<VMPurchaseOrderSlave>();

            vM = await Task.Run(() => (from PO in _db.Procurement_PurchaseOrder.Include(x => x.Procurement_PurchaseOrderSlave)
                                                     .Where(x => x.Active)
                                                    .Where(x => x.ID == (int)model.Procurement_PurchaseOrderFK)

                                       join C in _db.Common_Currency on PO.Common_CurrencyFK equals C.ID into C_Join
                                       from C in C_Join.DefaultIfEmpty()
                                       join E in _db.User_User on PO.UserID equals E.ID into E_Join
                                       from E in E_Join.DefaultIfEmpty()
                                       join S in _db.Common_Supplier on PO.Common_SupplierFK equals S.ID into S_Join
                                       from S in S_Join.DefaultIfEmpty()



                                       select new VMPurchaseOrderSlave
                                       {
                                           Procurement_PurchaseOrderFK = PO.ID,
                                           CID = PO.CID,
                                           OriginType = (ProcurementOriginTypeEnum)PO.OriginType,
                                           OrderDate = PO.OrderDate,
                                           POPaymentMethod = (SupplierPaymentMethodEnum)PO.POPaymentMethod,
                                           POType = (POTypeEnum)PO.POType,
                                           CreationDate = PO.CreationDate,
                                           CreatedBy = PO.CreatedBy,
                                           ApprovedDate = PO.ApprovedDate.HasValue ? PO.ApprovedDate.Value : DateTime.MinValue,
                                           RevisionNo = PO.RevisionNo,
                                           Description = PO.Description,
                                           Status = (POStatusEnum)PO.Status,
                                           VMEmployeeFK = E != null ? E.ID : 0,
                                           VMEmployeeName = E != null ? E.Name : "",
                                           VMSupplierFK = PO.Common_SupplierFK,
                                           VMSupplierName = S != null ? S.Name : "",
                                           VMCurrencyFK = PO.Common_CurrencyFK,
                                           VMCurrencyName = C != null ? C.Name : "",
                                           TermsAndCondition = PO.TermsAndCondition

                                       }).OrderByDescending(x => x.ID).FirstOrDefaultAsync());



            vM.DataListSlave = await Task.Run(() => (from POS in _db.Procurement_PurchaseOrderSlave.Where(x => x.Active && x.Procurement_PurchaseOrderFK == vM.Procurement_PurchaseOrderFK)
                                                     join PRS in _db.Procurement_PurchaseRequisitionSlave on POS.Procurement_PurchaseRequisitionSlaveFK equals PRS.ID into PRS_Join
                                                     from PRS in PRS_Join.DefaultIfEmpty()
                                                     join RI in _db.Common_RawItem on POS.Common_RawItemFK equals RI.ID into RI_Join
                                                     from RI in RI_Join.DefaultIfEmpty()
                                                     join RSC in _db.Common_RawSubCategory on RI.Common_RawSubCategoryFK equals RSC.ID into RSC_Join
                                                     from RSC in RSC_Join.DefaultIfEmpty()
                                                     join RC in _db.Common_RawCategory on RSC.Common_RawCategoryFK equals RC.ID into RC_Join
                                                     from RC in RC_Join.DefaultIfEmpty()
                                                     join U in _db.Common_Unit on RI.Common_UnitFK equals U.ID into U_Join
                                                     from U in U_Join.DefaultIfEmpty()
                                                     join MS in _db.Merchandising_Style on POS.Merchandising_StyleID equals MS.ID into MS_Join
                                                     from MS in MS_Join.DefaultIfEmpty()
                                                     join MBO in _db.Merchandising_BuyerOrder on MS.Merchandising_BuyerOrderFK equals MBO.ID into MBO_Join
                                                     from MBO in MBO_Join.DefaultIfEmpty()
                                                     join MSS in _db.Merchandising_StyleSlave on POS.Merchandising_StyleSlaveFK equals MSS.ID into MSS_Join
                                                     from MSS in MSS_Join.DefaultIfEmpty()
                                                     join C in _db.Common_Color on MSS.Common_ColorFK equals C.ID into C_Join
                                                     from C in C_Join.DefaultIfEmpty()
                                                     select new VMPurchaseOrderSlave
                                                     {
                                                         BONoStyleName = MS != null && MBO != null ? MBO.BuyerPO + "/" + MS.StyleName : "",
                                                         StyleColorName = MSS != null && C != null ? C.Name : "",
                                                         StyleGSM = MSS != null ? (double)MSS.GSM : 0,
                                                         Procurement_PurchaseOrderFK = POS != null ? POS.Procurement_PurchaseOrderFK : 0,
                                                         Procurement_PurchaseRequisitionSlaveFK = POS != null ? POS.Procurement_PurchaseRequisitionSlaveFK.Value : 0,
                                                         ID = POS != null ? POS.ID : 0,
                                                         PurchaseQuantity = POS != null ? POS.PurchaseQuantity : 0,
                                                         PurchasingPrice = POS != null ? POS.PurchasingPrice : 0,
                                                         DescriptionSlave = POS != null ? POS.Description : "",
                                                         RequisitionQuantity = POS != null ? PRS != null ? PRS.RequisitionQuantity : 0 : 0,
                                                         ProcuredQuantity = (POS != null ? POS.PurchaseQuantity : 0) - (from POS in _db.Procurement_PurchaseOrderSlave.Include(x => x.Procurement_PurchaseOrder).Where(x => x.Procurement_PurchaseOrder.Active && x.Active && x.Procurement_PurchaseRequisitionSlaveFK == PRS.ID) select new { PurchasingQuantity = POS.PurchaseQuantity }).Sum(x => x.PurchasingQuantity),
                                                         InspectionRequired = POS != null ? POS.InspectionRequired : false,
                                                         InspectionRequiredInPR = POS != null ? PRS != null ? PRS.InspectionRequired : false : false,
                                                         VMRawCategoryFK = RC != null ? RC.ID : 0,
                                                         VMRawSubCategoryFK = RSC != null ? RSC.ID : 0,
                                                         VMRawItemFK = RI != null ? RI.ID : 0,
                                                         VMRawItemName = RI != null ? RI.Name : "",
                                                         VMUnitName = U != null ? U.Name : "",
                                                         //AvailableQuantity = RI != null ? RI.Quantity : 0 //AvailableQuantity has to come from Store
                                                     }).OrderByDescending(x => x.ID).ToListAsync());




            return vM;
        }

        public async Task<VMPurchaseOrderSlave> POSlaveGetSpecific(VMPurchaseOrderSlave model)
        {

            VMPurchaseOrderSlave vM = new VMPurchaseOrderSlave();

            vM = await Task.Run(() => (from POS in _db.Procurement_PurchaseOrderSlave.Where(x => x.Active && x.ID == model.ID)
                                       join PRS in _db.Procurement_PurchaseRequisitionSlave on POS.Procurement_PurchaseRequisitionSlaveFK equals PRS.ID into PRS_Join
                                       from PRS in PRS_Join.DefaultIfEmpty()
                                       join RI in _db.Common_RawItem on POS.Common_RawItemFK equals RI.ID into RI_Join
                                       from RI in RI_Join.DefaultIfEmpty()
                                       join RSC in _db.Common_RawSubCategory on RI.Common_RawSubCategoryFK equals RSC.ID into RSC_Join
                                       from RSC in RSC_Join.DefaultIfEmpty()
                                       join RC in _db.Common_RawCategory on RSC.Common_RawCategoryFK equals RC.ID into RC_Join
                                       from RC in RC_Join.DefaultIfEmpty()
                                       join U in _db.Common_Unit on RI.Common_UnitFK equals U.ID into U_Join
                                       from U in U_Join.DefaultIfEmpty()
                                       select new VMPurchaseOrderSlave
                                       {

                                           Procurement_PurchaseOrderFK = POS.Procurement_PurchaseOrderFK,
                                           Procurement_PurchaseRequisitionSlaveFK = POS != null ? POS.Procurement_PurchaseRequisitionSlaveFK.Value : 0,
                                           ID = POS != null ? POS.ID : 0,
                                           PurchaseQuantity = POS != null ? POS.PurchaseQuantity : 0,
                                           PurchasingPrice = POS != null ? POS.PurchasingPrice : 0,
                                           DescriptionSlave = POS != null ? POS.Description : "",
                                           RequisitionQuantity = POS != null ? PRS != null ? PRS.RequisitionQuantity : 0 : 0,
                                           ProcuredQuantity = (POS != null ? POS.PurchaseQuantity : 0) - (from POS in _db.Procurement_PurchaseOrderSlave.Include(x => x.Procurement_PurchaseOrder).Where(x => x.Procurement_PurchaseOrder.Active && x.Active && x.Procurement_PurchaseRequisitionSlaveFK == PRS.ID) select new { PurchasingQuantity = POS.PurchaseQuantity }).Sum(x => x.PurchasingQuantity),
                                           InspectionRequired = POS != null ? POS.InspectionRequired : false,
                                           InspectionRequiredInPR = POS != null ? PRS != null ? PRS.InspectionRequired : false : false,
                                           VMRawCategoryFK = RC != null ? RC.ID : 0,
                                           VMRawCategoryName = RC != null ? RC.Name : "",
                                           VMRawSubCategoryFK = RSC != null ? RSC.ID : 0,
                                           VMRawSubCategoryName = RSC != null ? RSC.Name : "",
                                           VMRawItemFK = RI != null ? RI.ID : 0,
                                           VMRawItemName = RI != null ? RI.Name : "",
                                           VMUnitName = U != null ? U.Name : "",
                                           //AvailableQuantity = RI != null ? RI.Quantity : 0 //AvailableQuantity has to come from Store
                                       }).OrderByDescending(x => x.ID).FirstOrDefaultAsync());



            vM.RawCategoryList = new Microsoft.AspNetCore.Mvc.Rendering.SelectList(RawCategoryDropDownList(), "Value", "Text");
            if (model.ID > 0)
            {
                vM.RawSubCategoryList = new Microsoft.AspNetCore.Mvc.Rendering.SelectList(RawSubCategoryDropDownList(vM.VMRawCategoryFK), "Value", "Text");
                vM.RawItemList = new Microsoft.AspNetCore.Mvc.Rendering.SelectList(RawItemDropDownList(vM.VMRawSubCategoryFK), "Value", "Text");
            }
            return vM;
        }

        public async Task<decimal> POSlaveGetSpecificPrice(VMPurchaseOrderSlave model)
        {
            var vM = new VMPurchaseOrderSlave();
            vM = await Task.Run(() => (from POS in _db.Procurement_PurchaseOrderSlave.Where(x => x.Active && x.ID == model.ID)
                                       join RI in _db.Common_RawItem on POS.Common_RawItemFK equals RI.ID into RI_Join
                                       from RI in RI_Join.DefaultIfEmpty()
                                       select new VMPurchaseOrderSlave
                                       {

                                           Procurement_PurchaseOrderFK = POS.Procurement_PurchaseOrderFK,
                                           Procurement_PurchaseRequisitionSlaveFK = POS != null ? POS.Procurement_PurchaseRequisitionSlaveFK.Value : 0,
                                           ID = POS != null ? POS.ID : 0,
                                           PurchaseQuantity = POS != null ? POS.PurchaseQuantity : 0,
                                           PurchasingPrice = POS != null ? POS.PurchasingPrice : 0,
                                           DescriptionSlave = POS != null ? POS.Description : "",
                                           InspectionRequired = POS != null ? POS.InspectionRequired : false,
                                           VMRawItemFK = POS != null ? POS.Common_RawItemFK : 0,
                                           VMRawItemName = RI != null ? RI.Name : ""
                                       }).OrderByDescending(x => x.ID).FirstOrDefaultAsync());
            return (decimal)(vM == null ? 0 : vM.PurchasingPrice);
        }

        public async Task<int> PurchaseOrderAdd(VMPurchaseOrder vMPurchaseOrder)
        {
            var result = -1;
            string poidPRef = @"PO" +
                            DateTime.Now.ToString("yy") +
                            DateTime.Now.ToString("MM") + @"-" +
                            ((vMPurchaseOrder.OriginType == ProcurementOriginTypeEnum.Regular) ? "G" : "O") +
                            @"-P";
            Procurement_PurchaseOrder Procurement_PurchaseOrder = new Procurement_PurchaseOrder
            {
                CID = poidPRef + await Task.Run(() => (((_db.Procurement_PurchaseOrder.AnyAsync(x => x.CID.ToUpper().Contains(poidPRef.ToUpper())).Result ? _db.Procurement_PurchaseOrder.Where(x => x.CID.ToUpper().Contains(poidPRef.ToUpper())).MaxAsync(x => Int32.Parse(!string.IsNullOrEmpty(x.CID.Replace(poidPRef, "")) ? x.CID.Replace(poidPRef, "") : "0")).Result : 0) + 1).ToString().PadLeft(6, '0'))),
                OrderDate = vMPurchaseOrder.OrderDate,
                Common_SupplierFK = vMPurchaseOrder.VMSupplierFK,
                ClosedByUserFK = vMPurchaseOrder.VMEmployeeFK,
                Common_CurrencyFK = vMPurchaseOrder.VMCurrencyFK,
                TermsAndCondition = vMPurchaseOrder.TermsAndCondition,
                Description = vMPurchaseOrder.Description,
                POPaymentMethod = (int)vMPurchaseOrder.POPaymentMethod,
                POType = (int)vMPurchaseOrder.POType,
                User = vMPurchaseOrder.User,
                UserID = vMPurchaseOrder.UserID,
                CreatedBy = vMPurchaseOrder.User,
                Status = (int)POStatusEnum.Draft,
                OriginType = (int)vMPurchaseOrder.OriginType,
            };
            _db.Procurement_PurchaseOrder.Add(Procurement_PurchaseOrder);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = Procurement_PurchaseOrder.ID;
            }
            return result;
        }
        public async Task<int> PurchaseOrderEdit(VMPurchaseOrder vMPurchaseOrder)
        {
            var result = -1;
            Procurement_PurchaseOrder Procurement_PurchaseOrder = await _db.Procurement_PurchaseOrder.FindAsync(vMPurchaseOrder.ID);
            Procurement_PurchaseOrder.OrderDate = vMPurchaseOrder.OrderDate;
            Procurement_PurchaseOrder.Common_SupplierFK = vMPurchaseOrder.VMSupplierFK;
            Procurement_PurchaseOrder.UserID = vMPurchaseOrder.VMEmployeeFK;
            Procurement_PurchaseOrder.Common_CurrencyFK = vMPurchaseOrder.VMCurrencyFK;
            Procurement_PurchaseOrder.TermsAndCondition = vMPurchaseOrder.TermsAndCondition;
            Procurement_PurchaseOrder.Description = vMPurchaseOrder.Description;
            Procurement_PurchaseOrder.POPaymentMethod = (int)vMPurchaseOrder.POPaymentMethod;
            Procurement_PurchaseOrder.POType = (int)vMPurchaseOrder.POType;
            Procurement_PurchaseOrder.OriginType = (int)vMPurchaseOrder.OriginType;
            Procurement_PurchaseOrder.RevisionNo = await Task.Run(() => (((_db.Procurement_PurchaseOrder.FirstOrDefaultAsync(x => x.ID == vMPurchaseOrder.ID).Result != null ? _db.Procurement_PurchaseOrder.FirstOrDefaultAsync(x => x.ID == vMPurchaseOrder.ID).Result.RevisionNo : 0) + 1)));
            if (await _db.SaveChangesAsync() > 0)
            {
                result = Procurement_PurchaseOrder.ID;
            }
            return result;
        }
        public async Task<int> POGoodsReceivedStatusUpdateFromSIn(int POId, int id = 0)
        {
            var result = -1;
            Procurement_PurchaseOrder enpr = await _db.Procurement_PurchaseOrder.FindAsync(POId);
            if (enpr != null && enpr.ID > 0)
            {
                if ((POStatusEnum)id == POStatusEnum.PartialGoodsReceived)
                {
                    enpr.Status = (int)POStatusEnum.PartialGoodsReceived;
                }
                else if ((POStatusEnum)id == POStatusEnum.GoodsReceived)
                {
                    enpr.Status = (int)POStatusEnum.GoodsReceived;
                }
            }


            if (await _db.SaveChangesAsync() > 0)
            {
                result = enpr.ID;
            }
            return result;

        }
        public async Task<int> PurchaseOrderStatusUpdate(VMPurchaseOrder model)
        {
            var result = -1;
            Procurement_PurchaseOrder enpr = await _db.Procurement_PurchaseOrder.FindAsync(model.ID);
            if (model.ActionId > 3)
            {
                if (model.POActionEnum == POActionEnum.Submit)
                {
                    enpr.Status = (int)POStatusEnum.Submitted;
                }
                if (model.POActionEnum == POActionEnum.UndoSubmit)
                {
                    enpr.Status = (int)POStatusEnum.Draft;
                }

                if (model.POActionEnum == POActionEnum.PrimaryApprove)
                {
                    enpr.Status = (int)POStatusEnum.PrimaryApproved;
                }
                if (model.POActionEnum == POActionEnum.UndoPrimaryApprove)
                {
                    enpr.Status = (int)POStatusEnum.Submitted;
                }

                if (model.POActionEnum == POActionEnum.SecondaryApprove)
                {
                    enpr.Status = (int)POStatusEnum.SecondaryApproved;
                }
                if (model.POActionEnum == POActionEnum.UndoSecondaryApprove)
                {
                    enpr.Status = (int)POStatusEnum.PrimaryApproved;
                }

                if (model.POActionEnum == POActionEnum.FinalApprove)
                {
                    enpr.Status = (int)POStatusEnum.FinalApproved;
                    enpr.ApprovedDate = DateTime.Now;
                }
                if (model.POActionEnum == POActionEnum.UndoFinalApprove)
                {
                    enpr.Status = (int)POStatusEnum.SecondaryApproved;
                    enpr.ApprovedDate = null;
                }

                if (model.POActionEnum == POActionEnum.Cancel)
                {
                    enpr.Status = (int)POStatusEnum.Cancel;
                    //assign closed by User
                    //enpr.ClosedByUserFK = 1;
                }

                if (model.POActionEnum == POActionEnum.Hold)
                {
                    enpr.Status = (int)POStatusEnum.Hold;
                }

                if (model.POActionEnum == POActionEnum.Close)
                {
                    enpr.Status = (int)POStatusEnum.Closed;
                    //assign closed by User
                    //enpr.ClosedByUserFK = 1;
                }
                if (model.POActionEnum == POActionEnum.ReOpen)
                {
                    enpr.Status = (int)POStatusEnum.Draft;
                    //enpr.ClosedByUserFK = null;
                }


            }

            //else if (model.ActionEum == PRActionEnum.Close)
            //{
            //    enpr.Status = (int)PRStatusEnum.Closed;
            //}
            //else if (model.ActionEum == PRActionEnum.UnApprove)
            //{
            //    enpr.Status = (int)PRStatusEnum.Draft;
            //}
            //else if (model.ActionEum == PRActionEnum.ReOpen)
            //{
            //    enpr.Status = (int)PRStatusEnum.Draft;
            //}


            if (await _db.SaveChangesAsync() > 0)
            {
                result = enpr.ID;
            }
            return result;
        }
        public async Task<int> PurchaseOrderDelete(int id)
        {
            var result = -1;
            Procurement_PurchaseOrder selectSingle = await _db.Procurement_PurchaseOrder.FindAsync(id);
            if (selectSingle != null)
            {
                selectSingle.Active = false;
                if (await _db.SaveChangesAsync() > 0)
                {
                    await _db.Procurement_PurchaseOrderSlave.Where(y => y.Procurement_PurchaseOrderFK == selectSingle.ID).ForEachAsync(x => x.Active = false);

                    await _db.SaveChangesAsync();

                    List<VMPurchaseRequisition> vMs = await Task.Run(() => (_db.Procurement_PurchaseRequisitionSlave.Include(x => x.Procurement_PurchaseRequisition).Where(x => _db.Procurement_PurchaseOrderSlave.Where(y => y.Procurement_PurchaseOrderFK == selectSingle.ID).Any(y => x.ID == y.Procurement_PurchaseRequisitionSlaveFK)).Select(x => new VMPurchaseRequisition { ID = x.Procurement_PurchaseRequisition.ID }).ToListAsync()));

                    await PRStatusUpdateFromPO(vMs);

                    result = selectSingle.ID;
                }
            }
            return result;
        }



        public async Task<List<VMPurchaseRequisitionSlave>> ApprovedPRItemList(int poID = 0, int orgID = 0)
        {
            //populate RawItem DDL from PRSLave
            return

               await Task.Run(() => (from PRS in _db.Procurement_PurchaseRequisitionSlave
                                     join PR in _db.Procurement_PurchaseRequisition
                                          .Where(x => x.OriginType == orgID && x.Active && !string.IsNullOrEmpty(x.CID) &&
                                                      (x.Status == (int)PRStatusEnum.ProcurementApproved ||
                                                       x.Status == (int)PRStatusEnum.PartialPOCreated ||
                                                       (x.Status == (int)PRStatusEnum.FullPOCreated &&
                                                       x.ID == _db.Procurement_PurchaseOrderSlave
                                                       .Include(y => y.Procurement_PurchaseRequisitionSlave)
                                                       .Where(y => y.Procurement_PurchaseOrderFK == poID)
                                                       .Select(y => y.Procurement_PurchaseRequisitionSlave.Procurement_PurchaseRequisitionFK)
                                                       .DefaultIfEmpty(0).FirstOrDefault())))
                                      on PRS.Procurement_PurchaseRequisitionFK equals PR.ID

                                     join RI in _db.Common_RawItem on PRS.Common_RawItemFK equals RI.ID
                                     where PRS.Active &&
                                     PRS.RequisitionQuantity >
                                     (_db.Procurement_PurchaseOrderSlave.Include(x => x.Procurement_PurchaseOrder)
                                      .Where(x => x.Procurement_PurchaseOrder.Active && x.Active &&
                                     x.Procurement_PurchaseRequisitionSlaveFK == PRS.ID &&
                                     (x.Procurement_PurchaseOrder.ID != poID))
                                     .Select(x => x.PurchaseQuantity)
                                     .DefaultIfEmpty(0)
                                     .Sum())

                                     select new VMPurchaseRequisitionSlave
                                     {
                                         ID = PRS.ID,
                                         Procurement_PurchaseRequisitionFK = PR.ID,
                                         CID = PR.CID,
                                         VMRawItemFK = RI.ID,
                                         VMRawItemName = RI.Name,

                                     }).ToListAsync());
        }
        public async Task<VMPurchaseOrderSlave> POSlaveFromPRSlaveGet(int PRID, int POID)
        {

            VMPurchaseOrderSlave vM = new VMPurchaseOrderSlave();
            vM.Procurement_PurchaseOrderFK = POID;
            vM.OriginType = await Task.Run(() => _db.Procurement_PurchaseOrder.Where(x => x.ID == POID).Select(x => (ProcurementOriginTypeEnum)x.OriginType).DefaultIfEmpty(ProcurementOriginTypeEnum.Regular).FirstOrDefaultAsync());
            vM.PRIDVisible = false;
            vM.DataListSlave = new List<VMPurchaseOrderSlave>();
            vM.DataListSlave = await Task.Run(() => (from PRS in _db.Procurement_PurchaseRequisitionSlave
                                                     join PR in _db.Procurement_PurchaseRequisition on PRS.Procurement_PurchaseRequisitionFK equals PR.ID
                                                     join RI in _db.Common_RawItem on PRS.Common_RawItemFK equals RI.ID
                                                     join U in _db.Common_Unit on RI.Common_UnitFK equals U.ID
                                                     join MS in _db.Merchandising_Style on PRS.Merchandising_StyleID equals MS.ID into MS_Join
                                                     from MS in MS_Join.DefaultIfEmpty()
                                                     join MBO in _db.Merchandising_BuyerOrder on MS.Merchandising_BuyerOrderFK equals MBO.ID into MBO_Join
                                                     from MBO in MBO_Join.DefaultIfEmpty()
                                                     join MSS in _db.Merchandising_StyleSlave on PRS.Merchandising_StyleSlaveFK equals MSS.ID into MSS_Join
                                                     from MSS in MSS_Join.DefaultIfEmpty()
                                                     join C in _db.Common_Color on MSS.Common_ColorFK equals C.ID into C_Join
                                                     from C in C_Join.DefaultIfEmpty()
                                                     where PRS.Active && PRS.Procurement_PurchaseRequisitionFK == PRID
                                                     select new VMPurchaseOrderSlave
                                                     {
                                                         BONoStyleName = MS != null && MBO != null ? MBO.BuyerPO + "/" + MS.StyleName : "",
                                                         StyleColorName = MSS != null && C != null ? C.Name : "",
                                                         StyleGSM = MSS != null ? (double)MSS.GSM : 0,
                                                         Procurement_PurchaseOrderFK = POID,
                                                         Procurement_PurchaseRequisitionSlaveFK = PRS.ID,
                                                         VMRawItemFK = PRS.Common_RawItemFK,
                                                         VMRawItemName = RI.Name,
                                                         //AvailableQuantity = RI != null ? RI.Quantity : 0 //AvailableQuantity has to come from Store
                                                         RequisitionQuantity = PRS.RequisitionQuantity,
                                                         ProcuredQuantity = (from POS in _db.Procurement_PurchaseOrderSlave
                                                                             join PO in _db.Procurement_PurchaseOrder on POS.Procurement_PurchaseOrderFK equals PO.ID
                                                                             where PO.Active && PO.ID != POID && POS.Active && POS.Procurement_PurchaseRequisitionSlaveFK == PRS.ID
                                                                             select POS.PurchaseQuantity).DefaultIfEmpty(0).Sum(),
                                                         VMUnitName = U.Name,
                                                         InspectionRequired = PRS.InspectionRequired,
                                                         InspectionRequiredInPR = PRS.InspectionRequired,
                                                         DescriptionSlave= PRS.Description,
                                                         PRID = PR.CID,
                                                         VMMerchandising_StyleSlaveID = PRS.Merchandising_StyleSlaveFK,
                                                         VMMerchandising_StyleID = PRS.Merchandising_StyleID,
                                                         VMMerchandising_BOFID = PRS.Merchandising_BOFFK
                                                     }).ToListAsync());

            vM.DataListSlave = vM.DataListSlave.Where(x => x.RestQuantity > 0).ToList();
            vM.DataListSlave.ForEach(x =>
            {
                x.SuggestedPrice = integrationService.ItemPoValueListStyleWise(
                    x.VMMerchandising_StyleID.HasValue ? x.VMMerchandising_StyleID.Value : 0, x.VMRawItemFK);
            });
            return vM;
        }
        public async Task<VMPurchaseOrderSlave> POSlaveFromPRSlaveItemGet(int ItemID, int POID)
        {

            VMPurchaseOrderSlave vM = new VMPurchaseOrderSlave();
            vM.Procurement_PurchaseOrderFK = POID;
            vM.OriginType = await Task.Run(() => _db.Procurement_PurchaseOrder.Where(x => x.ID == POID).Select(x => (ProcurementOriginTypeEnum)x.OriginType).DefaultIfEmpty(ProcurementOriginTypeEnum.Regular).FirstOrDefaultAsync());
            vM.PRIDVisible = true;
            vM.DataListSlave = new List<VMPurchaseOrderSlave>();
            vM.DataListSlave = await Task.Run(() => (from PRS in _db.Procurement_PurchaseRequisitionSlave
                                                     join PR in _db.Procurement_PurchaseRequisition
                                                     .Where(x => x.Active && !string.IsNullOrEmpty(x.CID) &&
                                  (x.Status == (int)PRStatusEnum.ProcurementApproved ||
                                  x.Status == (int)PRStatusEnum.PartialPOCreated) ||
                                  (x.Status == (int)PRStatusEnum.FullPOCreated &&
                                  x.ID == (from POS in _db.Procurement_PurchaseOrderSlave
                                           join PRS in _db.Procurement_PurchaseRequisitionSlave on POS.Procurement_PurchaseRequisitionSlaveFK equals PRS.ID

                                           where POS.Procurement_PurchaseOrderFK == POID
                                           select PRS.Procurement_PurchaseRequisitionFK)
                                                       .DefaultIfEmpty(0).FirstOrDefault())) on PRS.Procurement_PurchaseRequisitionFK equals PR.ID
                                                     join RI in _db.Common_RawItem on PRS.Common_RawItemFK equals RI.ID
                                                     join U in _db.Common_Unit on RI.Common_UnitFK equals U.ID
                                                     join MS in _db.Merchandising_Style on PRS.Merchandising_StyleID equals MS.ID into MS_Join
                                                     from MS in MS_Join.DefaultIfEmpty()
                                                     join MBO in _db.Merchandising_BuyerOrder on MS.Merchandising_BuyerOrderFK equals MBO.ID into MBO_Join
                                                     from MBO in MBO_Join.DefaultIfEmpty()
                                                     join MSS in _db.Merchandising_StyleSlave on PRS.Merchandising_StyleSlaveFK equals MSS.ID into MSS_Join
                                                     from MSS in MSS_Join.DefaultIfEmpty()
                                                     join C in _db.Common_Color on MSS.Common_ColorFK equals C.ID into C_Join
                                                     from C in C_Join.DefaultIfEmpty()
                                                     where PRS.Active && PRS.Common_RawItemFK == ItemID
                                                     select new VMPurchaseOrderSlave
                                                     {
                                                         BONoStyleName = MS != null && MBO != null ? MBO.BuyerPO + "/" + MS.StyleName : "",
                                                         StyleColorName = MSS != null && C != null ? C.Name : "",
                                                         StyleGSM = MSS != null ? (double)MSS.GSM : 0,
                                                         Procurement_PurchaseOrderFK = POID,
                                                         Procurement_PurchaseRequisitionSlaveFK = PRS.ID,
                                                         VMRawItemFK = PRS.Common_RawItemFK,
                                                         VMRawItemName = RI.Name,
                                                         //AvailableQuantity = RI != null ? RI.Quantity : 0 //AvailableQuantity has to come from Store
                                                         RequisitionQuantity = PRS.RequisitionQuantity,
                                                         ProcuredQuantity = (from POS in _db.Procurement_PurchaseOrderSlave.Include(x => x.Procurement_PurchaseOrder)
                                                                             .Where(x => x.Procurement_PurchaseOrder.Active
                                                                             && x.Procurement_PurchaseOrder.ID != POID
                                                                             && x.Active
                                                                             && x.Procurement_PurchaseRequisitionSlaveFK == PRS.ID)
                                                                             select new { PurchasingQuantity = POS.PurchaseQuantity })
                                                                             .Sum(x => x.PurchasingQuantity),
                                                         VMUnitName = U.Name,
                                                         InspectionRequired = PRS.InspectionRequired,
                                                         InspectionRequiredInPR = PRS.InspectionRequired,
                                                         PRID = PR.CID,
                                                         VMMerchandising_StyleSlaveID = PRS.Merchandising_StyleSlaveFK,
                                                         VMMerchandising_StyleID = PRS.Merchandising_StyleID,
                                                         VMMerchandising_BOFID = PRS.Merchandising_BOFFK
                                                     }).ToListAsync());
            vM.DataListSlave = vM.DataListSlave.Where(x => x.RestQuantity > 0).ToList();
            vM.DataListSlave.ForEach(x => { x.SuggestedPrice = integrationService.ItemPoValueListStyleWise(x.VMMerchandising_StyleID.HasValue ? x.VMMerchandising_StyleID.Value : 0, x.VMRawItemFK); });
            return vM;
        }
        public async Task<int> POSlaveAdd(VMPurchaseOrderSlave vM)
        {
            var result = -1;
            Procurement_PurchaseOrderSlave enPOS = await Task.Run(() => (_db.Procurement_PurchaseOrderSlave.Where(x => x.Procurement_PurchaseOrderFK == vM.Procurement_PurchaseOrderFK && x.Common_RawItemFK == vM.VMRawItemFK && vM.Procurement_PurchaseRequisitionSlaveFK == x.Procurement_PurchaseRequisitionSlaveFK && x.Active).FirstOrDefaultAsync()));

            if (enPOS == null)
            {
                enPOS = new Procurement_PurchaseOrderSlave
                {
                    Procurement_PurchaseOrderFK = vM.Procurement_PurchaseOrderFK,
                    Procurement_PurchaseRequisitionSlaveFK = vM.Procurement_PurchaseRequisitionSlaveFK,
                    Common_RawItemFK = vM.VMRawItemFK,
                    PurchaseQuantity = vM.PurchaseQuantity,
                    PurchasingPrice = vM.PurchasingPrice,
                    InspectionRequired = vM.InspectionRequired,
                    Description = vM.DescriptionSlave,
                    User = vM.User,
                    UserID = vM.UserID,
                    Merchandising_StyleSlaveFK = vM.VMMerchandising_StyleSlaveID,
                    Merchandising_StyleID = vM.VMMerchandising_StyleID,
                    Merchandising_BOFFK = vM.VMMerchandising_BOFID
                };
                _db.Procurement_PurchaseOrderSlave.Add(enPOS);



                if (await _db.SaveChangesAsync() > 0)
                {
                    Procurement_PurchaseOrder enPO = await _db.Procurement_PurchaseOrder.FindAsync(vM.Procurement_PurchaseOrderFK);
                    enPO.TotalPOValue = await _db.Procurement_PurchaseOrderSlave.Where(x => x.Procurement_PurchaseOrderFK == vM.Procurement_PurchaseOrderFK && x.Active).SumAsync(x => x.PurchasingPrice * x.PurchaseQuantity);

                    await _db.SaveChangesAsync();

                    List<VMPurchaseRequisition> vMs = await Task.Run(() => (_db.Procurement_PurchaseRequisitionSlave.Include(x => x.Procurement_PurchaseRequisition).Where(x => x.ID == enPOS.Procurement_PurchaseRequisitionSlaveFK).Select(x => new VMPurchaseRequisition { ID = x.Procurement_PurchaseRequisition.ID }).ToListAsync()));

                    await PRStatusUpdateFromPO(vMs);

                    result = enPOS.ID;
                }
            }

            return result;
        }
        public async Task<int> POSlaveListAdd(List<VMPurchaseOrderSlave> vM)
        {
            var result = -1;

            List<Procurement_PurchaseOrderSlave> enPOSExisting = await Task.Run(() => (_db.Procurement_PurchaseOrderSlave.Where(x => vM.Any(y => x.Procurement_PurchaseOrderFK == y.Procurement_PurchaseOrderFK && x.Common_RawItemFK == y.VMRawItemFK && x.Procurement_PurchaseRequisitionSlaveFK == y.Procurement_PurchaseRequisitionSlaveFK) && x.Active).ToListAsync()));

            List<Procurement_PurchaseOrderSlave> enPOS = vM.Where(x => !enPOSExisting.Any(y => x.VMRawItemFK == x.VMRawItemFK))
                                                            .Select(x => new Procurement_PurchaseOrderSlave
                                                            {
                                                                Procurement_PurchaseOrderFK = x.Procurement_PurchaseOrderFK,
                                                                Procurement_PurchaseRequisitionSlaveFK = x.Procurement_PurchaseRequisitionSlaveFK,
                                                                Common_RawItemFK = x.VMRawItemFK,
                                                                PurchaseQuantity = x.PurchaseQuantity,
                                                                PurchasingPrice = x.PurchasingPrice,
                                                                InspectionRequired = x.InspectionRequired,
                                                                Description = x.DescriptionSlave,
                                                                User = x.User,
                                                                UserID = x.UserID,
                                                                Merchandising_StyleSlaveFK = x.VMMerchandising_StyleSlaveID,
                                                                Merchandising_StyleID = x.VMMerchandising_StyleID,
                                                                Merchandising_BOFFK = x.VMMerchandising_BOFID
                                                            }).ToList();
            if (enPOS != null && enPOS.Count() > 0)
            {
                await _db.Procurement_PurchaseOrderSlave.AddRangeAsync(enPOS);



                if (await _db.SaveChangesAsync() > 0)
                {
                    Procurement_PurchaseOrder enPO = await _db.Procurement_PurchaseOrder.FindAsync(enPOS.First().Procurement_PurchaseOrderFK);
                    enPO.TotalPOValue = await _db.Procurement_PurchaseOrderSlave.Where(x => x.Procurement_PurchaseOrderFK == enPOS.First().Procurement_PurchaseOrderFK && x.Active).SumAsync(x => x.PurchasingPrice * x.PurchaseQuantity);


                    result = await _db.SaveChangesAsync();

                    List<VMPurchaseRequisition> vMs = await Task.Run(() => (_db.Procurement_PurchaseRequisitionSlave.Include(x => x.Procurement_PurchaseRequisition).Where(x => enPOS.Any(y => x.ID == y.Procurement_PurchaseRequisitionSlaveFK)).Select(x => new VMPurchaseRequisition { ID = x.Procurement_PurchaseRequisition.ID }).ToListAsync()));

                    await PRStatusUpdateFromPO(vMs);
                }
            }

            return result;
        }
        public async Task<int> POSlaveEdit(VMPurchaseOrderSlave vM)
        {
            var result = -1;
            Procurement_PurchaseOrderSlave enPOS = await _db.Procurement_PurchaseOrderSlave.FindAsync(vM.ID);
            enPOS.PurchaseQuantity = vM.PurchaseQuantity;
            enPOS.PurchasingPrice = vM.PurchasingPrice;
            enPOS.InspectionRequired = vM.InspectionRequired;
            enPOS.Description = vM.DescriptionSlave;


            if (await _db.SaveChangesAsync() > 0)
            {
                Procurement_PurchaseOrder enPO = await _db.Procurement_PurchaseOrder.FindAsync(vM.Procurement_PurchaseOrderFK);
                enPO.TotalPOValue = await _db.Procurement_PurchaseOrderSlave.Where(x => x.Procurement_PurchaseOrderFK == vM.Procurement_PurchaseOrderFK && x.Active).SumAsync(x => x.PurchasingPrice * x.PurchaseQuantity);

                await _db.SaveChangesAsync();

                List<VMPurchaseRequisition> vMs = await Task.Run(() => (_db.Procurement_PurchaseRequisitionSlave.Include(x => x.Procurement_PurchaseRequisition).Where(x => x.ID == enPOS.Procurement_PurchaseRequisitionSlaveFK).Select(x => new VMPurchaseRequisition { ID = x.Procurement_PurchaseRequisition.ID }).ToListAsync()));

                await PRStatusUpdateFromPO(vMs);

                result = enPOS.ID;
            }
            return result;
        }
        public async Task<int> POSlaveDelete(int id)
        {
            var result = -1;
            Procurement_PurchaseOrderSlave selectSingle = await _db.Procurement_PurchaseOrderSlave.FindAsync(id);
            if (selectSingle != null)
            {
                selectSingle.Active = false;



                if (await _db.SaveChangesAsync() > 0)
                {

                    Procurement_PurchaseOrder enPO = await _db.Procurement_PurchaseOrder.FindAsync(selectSingle.Procurement_PurchaseOrderFK);
                    enPO.TotalPOValue = await _db.Procurement_PurchaseOrderSlave.Where(x => x.Procurement_PurchaseOrderFK == selectSingle.Procurement_PurchaseOrderFK && x.Active).SumAsync(x => x.PurchasingPrice * x.PurchaseQuantity);

                    await _db.SaveChangesAsync();

                    List<VMPurchaseRequisition> vMs = await Task.Run(() => (_db.Procurement_PurchaseRequisitionSlave.Include(x => x.Procurement_PurchaseRequisition).Where(x => x.ID == selectSingle.Procurement_PurchaseRequisitionSlaveFK).Select(x => new VMPurchaseRequisition { ID = x.Procurement_PurchaseRequisition.ID }).ToListAsync()));

                    await PRStatusUpdateFromPO(vMs);

                    result = selectSingle.ID;
                }
            }
            return result;
        }

        private async Task<int> PRStatusUpdateFromPO(List<VMPurchaseRequisition> model)
        {
            List<Procurement_PurchaseRequisition> enPRs = new List<Procurement_PurchaseRequisition>();
            int result = -1;


            double poQty = 0, PRqty = 0;

            if (model == null || model.Count <= 0) { return result; }

            enPRs = await Task.Run(() => (_db.Procurement_PurchaseRequisition.Where(x => x.Active && model.Any(y => x.ID == y.ID)).ToListAsync()));

            foreach (Procurement_PurchaseRequisition oItem in enPRs)
            {
                poQty = 0;
                PRqty = 0;
                PRqty = await Task.Run(() => (_db.Procurement_PurchaseRequisitionSlave.Where(x => x.Active && oItem.ID == x.Procurement_PurchaseRequisitionFK).SumAsync(x => x.RequisitionQuantity)));
                poQty = await Task.Run(() => (_db.Procurement_PurchaseOrderSlave.Where(z => z.Active && _db.Procurement_PurchaseRequisitionSlave.Where(x => x.Active && oItem.ID == x.Procurement_PurchaseRequisitionFK).Any(w => w.ID == z.Procurement_PurchaseRequisitionSlaveFK)).SumAsync(x => x.PurchaseQuantity)));

                if (PRqty <= poQty)
                {
                    oItem.Status = (int)PRStatusEnum.FullPOCreated;
                }
                else if (PRqty > poQty)
                {
                    oItem.Status = (int)PRStatusEnum.PartialPOCreated;
                }
            }

            result = await _db.SaveChangesAsync();

            return result;
        }

        #endregion

        #region PurchaseInvoice

        public async Task<VMPurchaseInvoice> PurchaseInvoiceGet(VMPurchaseInvoice model)
        {
            VMPurchaseInvoice vMPurchaseInvoice = new VMPurchaseInvoice();

            vMPurchaseInvoice.DataList = new List<VMPurchaseInvoice>();
            vMPurchaseInvoice.Status = model.Status;
            vMPurchaseInvoice.CreatedBy = "Admin";


            if (model.Status != PurchaseInvoiceStatusEnum.Closed)
            {
                await Task.Run(() => (from INV in _db.Procurement_PurchaseInvoice
                                      where INV.Active && (INV.Status != (int)PurchaseInvoiceStatusEnum.Closed && INV.Status != (int)PurchaseInvoiceStatusEnum.Cancel)
                                      group INV by INV.Status into g
                                      select new { Status = (PurchaseInvoiceStatusEnum)g.Key, Count = g.Count().ToString() }).ForEachAsync(x =>
                                                                 {
                                                                     if (x.Status == PurchaseInvoiceStatusEnum.Draft)
                                                                     {
                                                                         vMPurchaseInvoice.DraftStatusCount = x.Count;
                                                                     }
                                                                     if (x.Status == PurchaseInvoiceStatusEnum.Submitted)
                                                                     {
                                                                         vMPurchaseInvoice.SubmittedStatusCount = x.Count;
                                                                     }
                                                                     if (x.Status == PurchaseInvoiceStatusEnum.PrimaryApproved)
                                                                     {
                                                                         vMPurchaseInvoice.PrimaryStatusCount = x.Count;
                                                                     }
                                                                     if (x.Status == PurchaseInvoiceStatusEnum.FinalApproved)
                                                                     {
                                                                         vMPurchaseInvoice.FinalStatusCount = x.Count;
                                                                     }
                                                                     if (x.Status == PurchaseInvoiceStatusEnum.AccountsApproved)
                                                                     {
                                                                         vMPurchaseInvoice.AccountsStatusCount = x.Count;
                                                                     }
                                                                     if (x.Status == PurchaseInvoiceStatusEnum.Hold)
                                                                     {
                                                                         vMPurchaseInvoice.HoldStatusCount = x.Count;
                                                                     }

                                                                 }));
                //vMPurchaseInvoice.DraftStatusCount = await Task.Run(() => (_db.Procurement_PurchaseInvoice.Where(x => x.Status == (int)PurchaseInvoiceStatusEnum.Draft && x.Active).CountAsync().Result.ToString()));
                //vMPurchaseInvoice.SubmittedStatusCount = await Task.Run(() => (_db.Procurement_PurchaseInvoice.Where(x => x.Status == (int)PurchaseInvoiceStatusEnum.Submitted && x.Active).CountAsync().Result.ToString()));
                //vMPurchaseInvoice.PrimaryStatusCount = await Task.Run(() => (_db.Procurement_PurchaseInvoice.Where(x => x.Status == (int)PurchaseInvoiceStatusEnum.PrimaryApproved && x.Active).CountAsync().Result.ToString()));
                //vMPurchaseInvoice.AccountsStatusCount = await Task.Run(() => (_db.Procurement_PurchaseInvoice.Where(x => x.Status == (int)PurchaseInvoiceStatusEnum.AccountsApproved && x.Active).CountAsync().Result.ToString()));
                //vMPurchaseInvoice.FinalStatusCount = await Task.Run(() => (_db.Procurement_PurchaseInvoice.Where(x => x.Status == (int)PurchaseInvoiceStatusEnum.FinalApproved && x.Active).CountAsync().Result.ToString()));

                //vMPurchaseInvoice.PartialPaidStatusCount = await Task.Run(() => (_db.Procurement_PurchaseInvoice.Where(x => x.Status == (int)PurchaseInvoiceStatusEnum.PartialPaid && x.Active).CountAsync().Result.ToString()));
                //vMPurchaseInvoice.FullPaidStatusCount = await Task.Run(() => (_db.Procurement_PurchaseInvoice.Where(x => x.Status == (int)PurchaseInvoiceStatusEnum.FullPaid && x.Active).CountAsync().Result.ToString()));
                //vMPurchaseInvoice.HoldStatusCount = await Task.Run(() => (_db.Procurement_PurchaseInvoice.Where(x => x.Status == (int)PurchaseInvoiceStatusEnum.Hold && x.Active).CountAsync().Result.ToString()));
            }


            if (model.Status == PurchaseInvoiceStatusEnum.Closed)
            {
                vMPurchaseInvoice.DataList = await Task.Run(() => (from PI in _db.Procurement_PurchaseInvoice.Where(x => x.Active).Where(x => x.Status == (int)PurchaseInvoiceStatusEnum.Closed || x.Status == (int)PurchaseInvoiceStatusEnum.Cancel)
                                                                   join S in _db.Common_Supplier on PI.Common_SupplierFK equals S.ID into S_Join
                                                                   from S in S_Join.DefaultIfEmpty()
                                                                   where PI.Active == true
                                                                   select new VMPurchaseInvoice
                                                                   {
                                                                       ID = PI.ID,
                                                                       CID = PI.CID,
                                                                       InvoiceDate = PI.InvoiceDate,
                                                                       CreationDate = PI.CreationDate,
                                                                       CreatedBy = PI.CreatedBy,
                                                                       ApprovedDate = PI.ApprovedDate,
                                                                       PaymentMethod = (SupplierPaymentMethodEnum)PI.PaymentMethod,
                                                                       Description = PI.Description,
                                                                       Status = (PurchaseInvoiceStatusEnum)PI.Status,
                                                                       VMSupplierFK = PI.Common_SupplierFK.HasValue ? PI.Common_SupplierFK.Value : 0,
                                                                       VMSupplierName = S != null ? S.Name : "",
                                                                       OriginType = (ProcurementOriginTypeEnum)PI.OriginType,
                                                                       HasSlave = _db.Procurement_PurchaseInvoiceSlave.Any(y => y.Active && y.Procurement_PurchaseInvoiceFK == PI.ID)
                                                                   }).OrderByDescending(x => x.ID).ToListAsync());
            }
            else
            {
                vMPurchaseInvoice.DataList = await Task.Run(() => (from PI in _db.Procurement_PurchaseInvoice.Where(x => x.Active && x.Status != (int)PurchaseInvoiceStatusEnum.Closed && x.Status != (int)PurchaseInvoiceStatusEnum.Cancel).Where(x => x.Status == (int)model.Status || (int)model.Status <= 0)
                                                                   join S in _db.Common_Supplier on PI.Common_SupplierFK equals S.ID into S_Join
                                                                   from S in S_Join.DefaultIfEmpty()
                                                                   where PI.Active == true
                                                                   select new VMPurchaseInvoice
                                                                   {
                                                                       ID = PI.ID,
                                                                       CID = PI.CID,
                                                                       InvoiceDate = PI.InvoiceDate,
                                                                       CreationDate = PI.CreationDate,
                                                                       CreatedBy = PI.CreatedBy,
                                                                       ApprovedDate = PI.ApprovedDate,
                                                                       PaymentMethod = (SupplierPaymentMethodEnum)PI.PaymentMethod,
                                                                       Description = PI.Description,
                                                                       Status = (PurchaseInvoiceStatusEnum)PI.Status,
                                                                       VMSupplierFK = PI.Common_SupplierFK.HasValue ? PI.Common_SupplierFK.Value : 0,
                                                                       VMSupplierName = S != null ? S.Name : "",
                                                                       OriginType = (ProcurementOriginTypeEnum)PI.OriginType,
                                                                       HasSlave = _db.Procurement_PurchaseInvoiceSlave.Any(y => y.Active && y.Procurement_PurchaseInvoiceFK == PI.ID)
                                                                       
                                                                   }).OrderByDescending(x => x.ID).ToListAsync());
            }

            if (vMPurchaseInvoice.DataList.Any())
            {
                vMPurchaseInvoice.DataList.ForEach(a => {

                    var POCID = (from t1 in _db.Procurement_PurchaseInvoiceSlave
                                join t2 in _db.Procurement_PurchaseOrderSlave on t1.Procurement_PurchaseOrderSlaveID equals t2.ID
                                join t3 in _db.Procurement_PurchaseOrder on t2.Procurement_PurchaseOrderFK equals t3.ID
                                where t1.Procurement_PurchaseInvoiceFK==a.ID && t1.Active==true
                                select new { ID=t3.ID,CID=t3.CID }).ToList();
                    a.Procurement_PurchaseOrderID = POCID.Any() == true ? POCID.FirstOrDefault().ID : 0;
                    a.POCID = POCID.Any()==true ? POCID.FirstOrDefault().CID : "";
                });
            }

            return vMPurchaseInvoice;
        }

        public async Task<VMPurchaseInvoice> PurchaseInvoiceGetSpecific(VMPurchaseInvoice model)
        {
            VMPurchaseInvoice vMPurchaseInvoice = new VMPurchaseInvoice();
            vMPurchaseInvoice = await Task.Run(() => (from PI in _db.Procurement_PurchaseInvoice
                                                      join S in _db.Common_Supplier on PI.Common_SupplierFK equals S.ID into S_Join
                                                      from S in S_Join.DefaultIfEmpty()
                                                      where PI.ID == model.ID && PI.Active == true
                                                      select new VMPurchaseInvoice
                                                      {
                                                          ID = PI.ID,
                                                          CID = PI.CID,
                                                          InvoiceDate = PI.InvoiceDate,
                                                          PaymentMethod = (SupplierPaymentMethodEnum)PI.PaymentMethod,
                                                          CreationDate = PI.CreationDate,
                                                          CreatedBy = PI.CreatedBy,
                                                          ApprovedDate = PI.ApprovedDate,
                                                          Description = PI.Description,
                                                          Status = (PurchaseInvoiceStatusEnum)PI.Status,
                                                          VMSupplierFK = PI.Common_SupplierFK.HasValue ? PI.Common_SupplierFK.Value : 0,
                                                          VMSupplierName = S != null ? S.Name : "",
                                                          OriginType = (ProcurementOriginTypeEnum)PI.OriginType,
                                                      }).FirstOrDefaultAsync());
            return vMPurchaseInvoice;
        }

        public async Task<VMPurchaseInvoiceSlave> PurchaseInvoiceWithSlaveListGet(VMPurchaseInvoiceSlave model)
        {

            VMPurchaseInvoiceSlave vM = new VMPurchaseInvoiceSlave();
            vM.DataListSlave = new List<VMPurchaseInvoiceSlave>();
            vM = await Task.Run(() => (from PI in _db.Procurement_PurchaseInvoice.Include(x => x.Procurement_PurchaseInvoiceSlave)
                                                     .Where(x => x.Active)
                                                    .Where(x => x.ID == (int)model.Procurement_PurchaseInvoiceFK)
                                       join S in _db.Common_Supplier on PI.Common_SupplierFK equals S.ID into S_Join
                                       from S in S_Join.DefaultIfEmpty()
                                       select new VMPurchaseInvoiceSlave
                                       {
                                           Procurement_PurchaseInvoiceFK = PI.ID,
                                           CID = PI.CID,
                                           InvoiceDate = PI.InvoiceDate,
                                           PaymentMethod = (SupplierPaymentMethodEnum)PI.PaymentMethod,
                                           CreationDate = PI.CreationDate,
                                           CreatedBy = PI.CreatedBy,
                                           ApprovedDate = PI.ApprovedDate,
                                           Description = PI.Description,
                                           Status = (PurchaseInvoiceStatusEnum)PI.Status,
                                           VMSupplierFK = PI.Common_SupplierFK.HasValue ? PI.Common_SupplierFK.Value : 0,
                                           VMSupplierName = S != null ? S.Name : "",
                                           OriginType = (ProcurementOriginTypeEnum)PI.OriginType,

                                       }).OrderByDescending(x => x.ID).FirstOrDefaultAsync());



            vM.DataListSlave = await Task.Run(() => (from PIS in _db.Procurement_PurchaseInvoiceSlave.Where(x => x.Active && x.Procurement_PurchaseInvoiceFK == vM.Procurement_PurchaseInvoiceFK)
                                                     join POS in _db.Procurement_PurchaseOrderSlave on PIS.Procurement_PurchaseOrderSlaveID equals POS.ID into POS_Join
                                                     from POs in POS_Join.DefaultIfEmpty()
                                                     join PO in _db.Procurement_PurchaseOrder on POs.Procurement_PurchaseOrderFK equals PO.ID
                                                     join SIn in _db.Store_StockIn on PIS.Store_StockInFK equals SIn.ID into SIn_Join
                                                     from SIn in SIn_Join.DefaultIfEmpty()
                                                     join RI in _db.Common_RawItem on PIS.Common_RawItemFK equals RI.ID into RI_Join
                                                     from RI in RI_Join.DefaultIfEmpty()
                                                     join U in _db.Common_Unit on RI.Common_UnitFK equals U.ID into U_Join
                                                     from U in U_Join.DefaultIfEmpty()
                                                     join MS in _db.Merchandising_Style on PIS.Merchandising_StyleID equals MS.ID into MS_Join
                                                     from MS in MS_Join.DefaultIfEmpty()
                                                     join MBO in _db.Merchandising_BuyerOrder on MS.Merchandising_BuyerOrderFK equals MBO.ID into MBO_Join
                                                     from MBO in MBO_Join.DefaultIfEmpty()
                                                     join MSS in _db.Merchandising_StyleSlave on PIS.Merchandising_StyleSlaveID equals MSS.ID into MSS_Join
                                                     from MSS in MSS_Join.DefaultIfEmpty()
                                                     join C in _db.Common_Color on MSS.Common_ColorFK equals C.ID into C_Join
                                                     from C in C_Join.DefaultIfEmpty()
                                                     select new VMPurchaseInvoiceSlave
                                                     {
                                                         Procurement_PurchaseInvoiceFK = PIS != null ? PIS.Procurement_PurchaseInvoiceFK : 0,
                                                         Procurement_PurchaseOrderSlaveID = PIS != null && PIS.Procurement_PurchaseOrderSlaveID.HasValue ? PIS.Procurement_PurchaseOrderSlaveID.Value : 0,
                                                         Procurement_PurchaseOrderID= PO.ID,
                                                         POID= PO.CID,
                                                         Store_StockInFK = PIS != null && PIS.Store_StockInFK.HasValue ? PIS.Store_StockInFK.Value : 0,
                                                         ID = PIS != null ? PIS.ID : 0,
                                                         InvoiceQuantity = PIS != null ? PIS.InvoiceQuantity : 0,
                                                         InvoicePrice = PIS != null ? PIS.InvoicePrice : 0,
                                                         DescriptionSlave = PIS != null ? PIS.Description : "",
                                                         StockInQuantity = PIS != null ? SIn != null ? SIn.ReceivedQty : 0 : 0,
                                                         PreviousInvoiceQuantity = (PIS != null ? PIS.InvoiceQuantity : 0) - (from PIS in _db.Procurement_PurchaseInvoiceSlave.Include(x => x.Procurement_PurchaseInvoice).Where(x => x.Procurement_PurchaseInvoice.Active && x.Active && x.Store_StockInFK == SIn.ID) select new { Quantity = PIS.InvoiceQuantity }).Sum(x => x.Quantity),
                                                         VMRawItemFK = RI != null ? RI.ID : 0,
                                                         VMRawItemName = RI != null ? RI.Name : "",
                                                         VMUnitName = U != null ? U.Name : "",
                                                         SuggestedPrice = PIS != null ? SIn != null ? SIn.UnitPrice : 0 : 0,
                                                         VMMerchandising_BOFID = PIS.Merchandising_BOFID,
                                                         BONoStyleName = MS != null && MBO != null ? MBO.BuyerPO + "/" + MS.StyleName : "",
                                                         StyleColorName = MSS != null && C != null ? C.Name : "",
                                                         StyleGSM = MSS != null ? (double)MSS.GSM : 0,
                                                     }).OrderByDescending(x => x.ID).ToList());

            if (vM.DataListSlave.Any())
            {
                vM.Procurement_PurchaseOrderID = vM.DataListSlave.FirstOrDefault().Procurement_PurchaseOrderID;
                vM.POID = vM.DataListSlave.FirstOrDefault().POID;
            }

            return vM;
        }

        public async Task<VMPurchaseInvoiceSlave> PurchaseInvoiceSlaveGetSpecific(VMPurchaseInvoiceSlave model)
        {

            VMPurchaseInvoiceSlave vM = new VMPurchaseInvoiceSlave();

            vM = await Task.Run(() => (from PIS in _db.Procurement_PurchaseInvoiceSlave.Where(x => x.Active && x.ID == model.ID)
                                       join PO in _db.Procurement_PurchaseOrder on PIS.Procurement_PurchaseOrderSlaveID equals PO.ID into PO_Join
                                       from PO in PO_Join.DefaultIfEmpty()
                                       join SIn in _db.Store_StockIn on PIS.Store_StockInFK equals SIn.ID into SIn_Join
                                       from SIn in SIn_Join.DefaultIfEmpty()
                                       join RI in _db.Common_RawItem on PIS.Common_RawItemFK equals RI.ID into RI_Join
                                       from RI in RI_Join.DefaultIfEmpty()
                                       join U in _db.Common_Unit on RI.Common_UnitFK equals U.ID into U_Join
                                       from U in U_Join.DefaultIfEmpty()
                                       select new VMPurchaseInvoiceSlave
                                       {
                                           Procurement_PurchaseInvoiceFK = PIS != null ? PIS.Procurement_PurchaseInvoiceFK : 0,
                                           Procurement_PurchaseOrderSlaveID = PIS != null && PIS.Procurement_PurchaseOrderSlaveID.HasValue ? PIS.Procurement_PurchaseOrderSlaveID.Value : 0,
                                           Store_StockInFK = PIS != null && PIS.Store_StockInFK.HasValue ? PIS.Store_StockInFK.Value : 0,
                                           ID = PIS != null ? PIS.ID : 0,
                                           InvoiceQuantity = PIS != null ? PIS.InvoiceQuantity : 0,
                                           InvoicePrice = PIS != null ? PIS.InvoicePrice : 0,
                                           DescriptionSlave = PIS != null ? PIS.Description : "",
                                           StockInQuantity = PIS != null ? SIn != null ? SIn.ReceivedQty : 0 : 0,
                                           PreviousInvoiceQuantity = (PIS != null ? PIS.InvoiceQuantity : 0) - (from PIS in _db.Procurement_PurchaseInvoiceSlave.Include(x => x.Procurement_PurchaseInvoice).Where(x => x.Procurement_PurchaseInvoice.Active && x.Active && x.Store_StockInFK == SIn.ID) select new { Quantity = PIS.InvoiceQuantity }).Sum(x => x.Quantity),
                                           VMRawItemFK = RI != null ? RI.ID : 0,
                                           VMRawItemName = RI != null ? RI.Name : "",
                                           VMUnitName = U != null ? U.Name : "",
                                       }).OrderByDescending(x => x.ID).FirstOrDefaultAsync());




            return vM;
        }

        public async Task<int> PurchaseInvoiceAdd(VMPurchaseInvoice vMPurchaseInvoice)
        {
            var result = -1;
            string poidPRef = @"IN" +
                            DateTime.Now.ToString("yy") +
                            DateTime.Now.ToString("MM") + @"-" +
                            ((vMPurchaseInvoice.OriginType == ProcurementOriginTypeEnum.Regular) ? "G" : "O") +
                            @"-P";
            Procurement_PurchaseInvoice Procurement_PurchaseInvoice = new Procurement_PurchaseInvoice
            {
                CID = poidPRef + await Task.Run(() => (((_db.Procurement_PurchaseInvoice.AnyAsync(x => x.CID.ToUpper().Contains(poidPRef.ToUpper())).Result ? _db.Procurement_PurchaseInvoice.Where(x => x.CID.ToUpper().Contains(poidPRef.ToUpper())).MaxAsync(x => Int32.Parse(!string.IsNullOrEmpty(x.CID.Replace(poidPRef, "")) ? x.CID.Replace(poidPRef, "") : "0")).Result : 0) + 1).ToString().PadLeft(6, '0'))),
                InvoiceDate = vMPurchaseInvoice.InvoiceDate,
                Common_SupplierFK = vMPurchaseInvoice.VMSupplierFK,
                Description = vMPurchaseInvoice.Description,
                PaymentMethod = (int)vMPurchaseInvoice.PaymentMethod,
                User = vMPurchaseInvoice.User,
                UserID = vMPurchaseInvoice.UserID,
                CreatedBy = vMPurchaseInvoice.User,
                Status = (int)POStatusEnum.Draft,
                OriginType = (int)vMPurchaseInvoice.OriginType,
            };
            _db.Procurement_PurchaseInvoice.Add(Procurement_PurchaseInvoice);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = Procurement_PurchaseInvoice.ID;
            }
            return result;
        }
        public async Task<int> PurchaseInvoiceEdit(VMPurchaseInvoice vMPurchaseInvoice)
        {
            var result = -1;
            Procurement_PurchaseInvoice Procurement_PurchaseInvoice = await _db.Procurement_PurchaseInvoice.FindAsync(vMPurchaseInvoice.ID);
            Procurement_PurchaseInvoice.InvoiceDate = vMPurchaseInvoice.InvoiceDate;
            Procurement_PurchaseInvoice.Common_SupplierFK = vMPurchaseInvoice.VMSupplierFK;
            Procurement_PurchaseInvoice.Description = vMPurchaseInvoice.Description;
            Procurement_PurchaseInvoice.PaymentMethod = (int)vMPurchaseInvoice.PaymentMethod;
            Procurement_PurchaseInvoice.OriginType = (int)vMPurchaseInvoice.OriginType;
            if (await _db.SaveChangesAsync() > 0)
            {
                result = Procurement_PurchaseInvoice.ID;
            }
            return result;
        }
        public async Task<int> PurchaseInvoicePaidStatusUpdateFromAcc(int PIId, int id = 0)
        {
            var result = -1;
            Procurement_PurchaseInvoice enpr = await _db.Procurement_PurchaseInvoice.FindAsync(PIId);
            if ((PurchaseInvoiceStatusEnum)id == PurchaseInvoiceStatusEnum.PartialPaid)
            {
                enpr.Status = (int)PurchaseInvoiceStatusEnum.PartialPaid;
            }
            else if ((PurchaseInvoiceStatusEnum)id == PurchaseInvoiceStatusEnum.FullPaid)
            {
                enpr.Status = (int)PurchaseInvoiceStatusEnum.FullPaid;
            }

            if (await _db.SaveChangesAsync() > 0)
            {
                result = enpr.ID;
            }
            return result;

        }
        public async Task<int> PurchaseInvoiceStatusUpdate(VMPurchaseInvoice model)
        {
            var result = -1;
            Procurement_PurchaseInvoice enpi = await _db.Procurement_PurchaseInvoice.FindAsync(model.ID);
            Procurement_PurchaseOrder enpo = await _db.Procurement_PurchaseOrder.Include(po=>po.Common_Currency).Where(po=>po.ID == model.Procurement_PurchaseOrderID).FirstOrDefaultAsync();
            if (model.ActionId > 3)
            {
                if (model.InvoiceActionEnum == PurchaseInvoiceActionEnum.Submit)
                {
                    enpi.Status = (int)PurchaseInvoiceStatusEnum.Submitted;
                }
                if (model.InvoiceActionEnum == PurchaseInvoiceActionEnum.UndoSubmit)
                {
                    enpi.Status = (int)PurchaseInvoiceStatusEnum.Draft;
                }

                if (model.InvoiceActionEnum == PurchaseInvoiceActionEnum.PrimaryApprove)
                {
                    enpi.Status = (int)PurchaseInvoiceStatusEnum.PrimaryApproved;
                }
                if (model.InvoiceActionEnum == PurchaseInvoiceActionEnum.UndoPrimaryApprove)
                {
                    enpi.Status = (int)PurchaseInvoiceStatusEnum.Submitted;
                }

                if (model.InvoiceActionEnum == PurchaseInvoiceActionEnum.FinalApprove)
                {
                    enpi.Status = (int)PurchaseInvoiceStatusEnum.FinalApproved;
                }
                if (model.InvoiceActionEnum == PurchaseInvoiceActionEnum.UndoFinalApprove)
                {
                    enpi.Status = (int)PurchaseInvoiceStatusEnum.PrimaryApproved;
                }

                if (model.InvoiceActionEnum == PurchaseInvoiceActionEnum.AccountsApprove)
                {
                    //invetorylist will be here
                    VmInventory inventory = new VmInventory();
                    inventory.DataList = (from PPI in _db.Procurement_PurchaseInvoice
                                          join PPIS in _db.Procurement_PurchaseInvoiceSlave on PPI.ID equals PPIS.Procurement_PurchaseInvoiceFK
                                          where PPI.Active && PPIS.Active && PPI.ID == model.ID
                                          select new VmInventory
                                          {
                                              StoreTypeFk = (int)InventoryTypeEnum.Raw,
                                              TransactionType = (int)StoreTransactionTypeEnum.In,
                                              Store_StockOutFK = (int)InventoryTypeEnum.Procurement,
                                              Store_StockInFK = PPIS.Store_StockInFK,
                                              Common_RawItemFK = PPIS.Common_RawItemFK,
                                              Amount = (decimal)PPIS.InvoiceQuantity,
                                              Price = (decimal)PPIS.InvoicePrice,
                                              Procurement_PurchaseInvoiceFK = PPI.ID,
                                              Procurement_PurchaseOrderSlaveID = PPIS.Procurement_PurchaseOrderSlaveID,
                                              Merchandising_StyleID = PPIS.Merchandising_StyleID,
                                              Merchandising_StyleSlaveID = PPIS.Merchandising_StyleSlaveID,
                                              Merchandising_BOFID = PPIS.Merchandising_BOFID,
                                              Time = DateTime.Now,
                                              User = PPI.User,
                                              UserID = PPI.UserID,
                                              Active = true
                                          }).ToList();
                    enpi = await _db.Procurement_PurchaseInvoice.Include(x => x.Common_Supplier).Where(x => x.ID == model.ID).FirstOrDefaultAsync();
                    decimal convertedAmount = (decimal)enpi.TotalInvoiceValue * enpo.Common_Currency.ConversionRateToBDT;

                    string ProcRef = "Procurement- Invoice No: " + enpi.CID + ". PO No: " + model.POCID;
                    if (enpi != null && enpi.Common_Supplier != null)
                    {
                        // await integrationService.AccountingJournalPush((int)CreditEnum.Rawinventory, enpi.Common_Supplier.AccHeadID, (decimal)enpi.TotalInvoiceValue, "Procurement", (int)CostCenterEnum.General);
                        await integrationService.InventroyPush(inventory, InventoryTransactionTriggerTypeEnum.PurchaseInvoice, (int)CreditEnum.Rawinventory, enpi.Common_Supplier.AccHeadID, convertedAmount, (decimal)enpi.TotalInvoiceValue, enpo.Common_Currency.ConversionRateToBDT, ProcRef, (int)CostCenterEnum.General);
                        enpi.Status = (int)PurchaseInvoiceStatusEnum.AccountsApproved;
                    }
                }
                if (model.InvoiceActionEnum == PurchaseInvoiceActionEnum.UndoAccountsApprove)
                {
                    enpi.Status = (int)PurchaseInvoiceStatusEnum.FinalApproved;
                }

                if (model.InvoiceActionEnum == PurchaseInvoiceActionEnum.Cancel)
                {
                    enpi.Status = (int)PurchaseInvoiceStatusEnum.Cancel;
                    //assign closed by User
                    enpi.ClosedByUserFK = model.UserID;
                }

                if (model.InvoiceActionEnum == PurchaseInvoiceActionEnum.Hold)
                {
                    enpi.Status = (int)PurchaseInvoiceStatusEnum.Hold;
                }

                if (model.InvoiceActionEnum == PurchaseInvoiceActionEnum.Close)
                {
                    enpi.Status = (int)PurchaseInvoiceStatusEnum.Closed;
                    //assign closed by User
                    enpi.ClosedByUserFK = model.UserID;
                }
                if (model.InvoiceActionEnum == PurchaseInvoiceActionEnum.ReOpen)
                {
                    enpi.Status = (int)PurchaseInvoiceStatusEnum.Draft;
                    enpi.ClosedByUserFK = (int?)null;
                }

            }


            if (await _db.SaveChangesAsync() > 0)
            {
                result = enpi.ID;
            }
            return result;
        }
        public async Task<int> PurchaseInvoiceDelete(int id)
        {
            var result = -1;
            Procurement_PurchaseInvoice selectSingle = await _db.Procurement_PurchaseInvoice.FindAsync(id);
            if (selectSingle != null)
            {
                selectSingle.Active = false;
                if (await _db.SaveChangesAsync() > 0)
                {
                    await _db.Procurement_PurchaseInvoiceSlave.Where(y => y.Procurement_PurchaseInvoiceFK == selectSingle.ID).ForEachAsync(x => x.Active = false);

                    await _db.SaveChangesAsync();

                    //List<VMPurchaseRequisition> vMs = await Task.Run(() => (_db.Procurement_PurchaseRequisitionSlave.Include(x => x.Procurement_PurchaseRequisition).Where(x => _db.Procurement_PurchaseOrderSlave.Where(y => y.Procurement_PurchaseOrderFK == selectSingle.ID).Any(y => x.ID == y.Procurement_PurchaseRequisitionSlaveFK)).Select(x => new VMPurchaseRequisition { ID = x.Procurement_PurchaseRequisition.ID }).ToListAsync()));

                    //await PRStatusUpdateFromPO(vMs);

                    result = selectSingle.ID;
                }
            }
            return result;
        }

        public async Task<List<VMPurchaseInvoiceSlave>> ApprovedSInItemList(int nSupplierId = 0, int PIID = 0, int orgID = 0)
        {
            //populate RawItem DDL from PRSLave
            return await Task.Run(() => (from SIn in _db.Store_StockIn.Where(x => x.Active && x.IsLocked)
                                  join PO in _db.Procurement_PurchaseOrder
                                  .Where(x => x.Active &&
                                  x.OriginType == orgID &&
                                  x.Common_SupplierFK == nSupplierId &&
                                  (x.Status == (int)POStatusEnum.FinalApproved ||
                                  x.Status == (int)POStatusEnum.PartialGoodsReceived ||
                                  (x.Status == (int)POStatusEnum.GoodsReceived &&
                                  x.ID == (from PIS in _db.Procurement_PurchaseInvoiceSlave
                                           join SIn in _db.Store_StockIn on PIS.Store_StockInFK equals SIn.ID into SIn_Join
                                           from SIn in SIn_Join.DefaultIfEmpty()
                                           where PIS.Procurement_PurchaseInvoiceFK == PIID
                                           select SIn != null ? SIn.IPOFK : 0)
                                                       .DefaultIfEmpty(0).FirstOrDefault()))) on SIn.IPOFK equals PO.ID
                                  join RI in _db.Common_RawItem on SIn.ItemIDFK equals RI.ID into RI_Join
                                  from RI in RI_Join.DefaultIfEmpty()
                                  where SIn.Active &&
                                  SIn.ReceivedQty >
                                  _db.Procurement_PurchaseInvoiceSlave.Include(x => x.Procurement_PurchaseInvoice)
                                  .Where(x => x.Procurement_PurchaseInvoice.Active &&
                                  x.Active &&
                                  x.Store_StockInFK == SIn.ID &&
                                  x.Procurement_PurchaseInvoice.ID != PIID)
                                  .Select(x => x.InvoiceQuantity)
                                  .DefaultIfEmpty(0)
                                  .Sum()
                                  select new VMPurchaseInvoiceSlave
                                  {
                                      POID = PO.CID,
                                      Procurement_PurchaseOrderID = PO.ID,
                                      VMRawItemFK = RI.ID,
                                      VMRawItemName = RI.Name
                                  }).ToListAsync());


        }

        public async Task<int> PurchaseInvoiceSlaveAdd(VMPurchaseInvoiceSlave vM)
        {
            var result = -1;
            Procurement_PurchaseInvoiceSlave enPIS = await Task.Run(() => (_db.Procurement_PurchaseInvoiceSlave.Where(x => x.Procurement_PurchaseInvoiceFK == vM.Procurement_PurchaseInvoiceFK && x.Common_RawItemFK == vM.VMRawItemFK && x.Store_StockInFK == vM.Store_StockInFK && x.Active).FirstOrDefaultAsync()));

            if (enPIS == null)
            {
                enPIS = new Procurement_PurchaseInvoiceSlave
                {
                    Procurement_PurchaseInvoiceFK = vM.Procurement_PurchaseInvoiceFK,
                    Procurement_PurchaseOrderSlaveID = vM.Procurement_PurchaseOrderSlaveID,
                    Store_StockInFK = vM.Store_StockInFK,
                    Common_RawItemFK = vM.VMRawItemFK,
                    InvoiceQuantity = vM.InvoiceQuantity,
                    InvoicePrice = vM.InvoicePrice,
                    Description = vM.DescriptionSlave,
                    User = vM.User,
                    UserID = vM.UserID,

                };
                _db.Procurement_PurchaseInvoiceSlave.Add(enPIS);

                if (await _db.SaveChangesAsync() > 0)
                {
                    Procurement_PurchaseInvoice enPI = await _db.Procurement_PurchaseInvoice.FindAsync(vM.Procurement_PurchaseInvoiceFK);
                    enPI.TotalInvoiceValue = await _db.Procurement_PurchaseInvoiceSlave.Where(x => x.Procurement_PurchaseInvoiceFK == vM.Procurement_PurchaseInvoiceFK && x.Active).SumAsync(x => x.InvoicePrice * x.InvoiceQuantity);

                    await _db.SaveChangesAsync();

                    //List<VMPurchaseRequisition> vMs = await Task.Run(() => (_db.Procurement_PurchaseRequisitionSlave.Include(x => x.Procurement_PurchaseRequisition).Where(x => x.ID == enPIS.Procurement_PurchaseRequisitionSlaveFK).Select(x => new VMPurchaseRequisition { ID = x.Procurement_PurchaseRequisition.ID }).ToListAsync()));

                    //await PRStatusUpdateFromPO(vMs);

                    result = enPIS.ID;
                }
            }

            return result;
        }
        public async Task<int> PurchaseInvoiceSlaveListAdd(List<VMPurchaseInvoiceSlave> vM)
        {
            var result = -1;
            List<Procurement_PurchaseInvoiceSlave> enPISExisting = await Task.Run(() => (_db.Procurement_PurchaseInvoiceSlave.Where(x => vM.Any(y => x.Procurement_PurchaseInvoiceFK == y.Procurement_PurchaseInvoiceFK && x.Common_RawItemFK == y.VMRawItemFK && x.Store_StockInFK == y.Store_StockInFK) && x.Active).ToListAsync()));

            List<Procurement_PurchaseInvoiceSlave> enPIS = vM.Where(x => !enPISExisting.Any(y => x.VMRawItemFK == x.VMRawItemFK))
                                                            .Select(x => new Procurement_PurchaseInvoiceSlave
                                                            {
                                                                Procurement_PurchaseInvoiceFK = x.Procurement_PurchaseInvoiceFK,
                                                                Procurement_PurchaseOrderSlaveID = x.Procurement_PurchaseOrderSlaveID,
                                                                Merchandising_StyleID = x.VMMerchandising_StyleID,
                                                                Merchandising_StyleSlaveID = x.VMMerchandising_StyleSlaveID,
                                                                Merchandising_BOFID = x.VMMerchandising_BOFID,
                                                                Store_StockInFK = x.Store_StockInFK,
                                                                Common_RawItemFK = x.VMRawItemFK,
                                                                InvoiceQuantity = x.InvoiceQuantity,
                                                                InvoicePrice = x.InvoicePrice,
                                                                Description = x.DescriptionSlave,
                                                                User = x.User,
                                                                UserID = x.UserID,

                                                            }).ToList();

            if (enPIS != null && enPIS.Count() > 0)
            {
                await _db.Procurement_PurchaseInvoiceSlave.AddRangeAsync(enPIS);

                if (await _db.SaveChangesAsync() > 0)
                {
                    Procurement_PurchaseInvoice enPI = await _db.Procurement_PurchaseInvoice.FindAsync(enPIS.First().Procurement_PurchaseInvoiceFK);
                    enPI.TotalInvoiceValue = await _db.Procurement_PurchaseInvoiceSlave.Where(x => x.Procurement_PurchaseInvoiceFK == enPIS.First().Procurement_PurchaseInvoiceFK && x.Active).SumAsync(x => x.InvoicePrice * x.InvoiceQuantity);

                    result = await _db.SaveChangesAsync();

                    //List<VMPurchaseRequisition> vMs = await Task.Run(() => (_db.Procurement_PurchaseRequisitionSlave.Include(x => x.Procurement_PurchaseRequisition).Where(x => enPIS.Any(y => x.ID == y.Procurement_PurchaseRequisitionSlaveFK)).Select(x => new VMPurchaseRequisition { ID = x.Procurement_PurchaseRequisition.ID }).ToListAsync()));

                    //await PRStatusUpdateFromPO(vMs);
                }
            }

            return result;
        }
        public async Task<int> PurchaseInvoiceSlaveEdit(VMPurchaseInvoiceSlave vM)
        {
            var result = -1;
            Procurement_PurchaseInvoiceSlave enPIS = await _db.Procurement_PurchaseInvoiceSlave.FindAsync(vM.ID);
            enPIS.InvoiceQuantity = vM.InvoiceQuantity;
            enPIS.InvoicePrice = vM.InvoicePrice;
            enPIS.Description = vM.DescriptionSlave;


            if (await _db.SaveChangesAsync() > 0)
            {
                Procurement_PurchaseInvoice enPI = await _db.Procurement_PurchaseInvoice.FindAsync(vM.Procurement_PurchaseInvoiceFK);
                enPI.TotalInvoiceValue = await _db.Procurement_PurchaseInvoiceSlave.Where(x => x.Procurement_PurchaseInvoiceFK == vM.Procurement_PurchaseInvoiceFK && x.Active).SumAsync(x => x.InvoicePrice * x.InvoiceQuantity);

                await _db.SaveChangesAsync();

                //List<VMPurchaseRequisition> vMs = await Task.Run(() => (_db.Procurement_PurchaseRequisitionSlave.Include(x => x.Procurement_PurchaseRequisition).Where(x => x.ID == enPIS.Procurement_PurchaseRequisitionSlaveFK).Select(x => new VMPurchaseRequisition { ID = x.Procurement_PurchaseRequisition.ID }).ToListAsync()));

                //await PRStatusUpdateFromPO(vMs);

                result = enPIS.ID;
            }
            return result;
        }
        public async Task<int> PurchaseInvoiceSlaveDelete(int id)
        {
            var result = -1;
            Procurement_PurchaseInvoiceSlave selectSingle = await _db.Procurement_PurchaseInvoiceSlave.FindAsync(id);
            if (selectSingle != null)
            {
                selectSingle.Active = false;



                if (await _db.SaveChangesAsync() > 0)
                {

                    Procurement_PurchaseInvoice enPI = await _db.Procurement_PurchaseInvoice.FindAsync(selectSingle.Procurement_PurchaseInvoiceFK);
                    enPI.TotalInvoiceValue = await _db.Procurement_PurchaseInvoiceSlave.Where(x => x.Procurement_PurchaseInvoiceFK == selectSingle.Procurement_PurchaseInvoiceFK && x.Active).SumAsync(x => x.InvoicePrice * x.InvoiceQuantity);

                    await _db.SaveChangesAsync();

                    //List<VMPurchaseRequisition> vMs = await Task.Run(() => (_db.Procurement_PurchaseRequisitionSlave.Include(x => x.Procurement_PurchaseRequisition).Where(x => x.ID == selectSingle.Procurement_PurchaseRequisitionSlaveFK).Select(x => new VMPurchaseRequisition { ID = x.Procurement_PurchaseRequisition.ID }).ToListAsync()));

                    //await PRStatusUpdateFromPO(vMs);

                    result = selectSingle.ID;
                }
            }
            return result;
        }

        //private async Task<int> PRStatusUpdateFromPO(List<VMPurchaseRequisition> model)
        //{
        //    List<Procurement_PurchaseRequisition> enPRs = new List<Procurement_PurchaseRequisition>();
        //    int result = -1;


        //    double poQty = 0, PRqty = 0;

        //    if (model == null || model.Count <= 0) { return result; }

        //    enPRs = await Task.Run(() => (_db.Procurement_PurchaseRequisition.Where(x => x.Active && model.Any(y => x.ID == y.ID)).ToListAsync()));

        //    foreach (Procurement_PurchaseRequisition oItem in enPRs)
        //    {
        //        poQty = 0;
        //        PRqty = 0;
        //        PRqty = await Task.Run(() => (_db.Procurement_PurchaseRequisitionSlave.Where(x => x.Active && oItem.ID == x.Procurement_PurchaseRequisitionFK).SumAsync(x => x.RequisitionQuantity)));
        //        poQty = await Task.Run(() => (_db.Procurement_PurchaseOrderSlave.Where(z => z.Active && _db.Procurement_PurchaseRequisitionSlave.Where(x => x.Active && oItem.ID == x.Procurement_PurchaseRequisitionFK).Any(w => w.ID == z.Procurement_PurchaseRequisitionSlaveFK)).SumAsync(x => x.PurchaseQuantity)));

        //        if (PRqty <= poQty)
        //        {
        //            oItem.Status = (int)PRStatusEnum.FullPOCreated;
        //        }
        //        else if (PRqty > poQty)
        //        {
        //            oItem.Status = (int)PRStatusEnum.PartialPOCreated;
        //        }
        //    }

        //    result = await _db.SaveChangesAsync();

        //    return result;
        //}
        public async Task<VMPurchaseInvoiceSlave> PurchaseInvoiceSlaveFromSInGet(int POID = 0, int PurchaseInvoiceID = 0)
        {

            VMPurchaseInvoiceSlave vM = new VMPurchaseInvoiceSlave();
            vM.Procurement_PurchaseInvoiceFK = PurchaseInvoiceID;
            vM.OriginType = await Task.Run(() => _db.Procurement_PurchaseInvoice.Where(x => x.ID == PurchaseInvoiceID).Select(x => (ProcurementOriginTypeEnum)x.OriginType).DefaultIfEmpty(ProcurementOriginTypeEnum.Regular).FirstOrDefaultAsync());
            vM.POIDVisible = false;
            vM.DataListSlave = new List<VMPurchaseInvoiceSlave>();
            vM.DataListSlave = await Task.Run(() => (from SIn in _db.Store_StockIn
                                                     join POS in _db.Procurement_PurchaseOrderSlave on SIn.Procurement_PurchaseOrderSlaveFK equals POS.ID
                                                     join PO in _db.Procurement_PurchaseOrder on SIn.IPOFK equals PO.ID into PO_Join
                                                     from PO in PO_Join.DefaultIfEmpty()
                                                     join RI in _db.Common_RawItem on SIn.ItemIDFK equals RI.ID
                                                     join U in _db.Common_Unit on RI.Common_UnitFK equals U.ID
                                                     join MS in _db.Merchandising_Style on SIn.Merchandising_StyleSlaveFK equals MS.ID into MS_Join
                                                     from MS in MS_Join.DefaultIfEmpty()
                                                     join MBO in _db.Merchandising_BuyerOrder on MS.Merchandising_BuyerOrderFK equals MBO.ID into MBO_Join
                                                     from MBO in MBO_Join.DefaultIfEmpty()
                                                     join MSS in _db.Merchandising_StyleSlave on SIn.Merchandising_StyleSlaveFK equals MSS.ID into MSS_Join
                                                     from MSS in MSS_Join.DefaultIfEmpty()
                                                     join C in _db.Common_Color on MSS.Common_ColorFK equals C.ID into C_Join
                                                     from C in C_Join.DefaultIfEmpty()
                                                     where SIn.Active && SIn.IPOFK == POID
                                                     select new VMPurchaseInvoiceSlave
                                                     {
                                                         Procurement_PurchaseInvoiceFK = PurchaseInvoiceID,
                                                         Procurement_PurchaseOrderSlaveID = SIn.Procurement_PurchaseOrderSlaveFK,
                                                         VMMerchandising_StyleID = SIn.Merchandising_StyleFK,
                                                         VMMerchandising_StyleSlaveID = POS.Merchandising_StyleSlaveFK,
                                                         VMMerchandising_BOFID = POS.Merchandising_BOFFK,

                                                         BONoStyleName = MS != null && MBO != null ? MBO.BuyerPO + "/" + MS.StyleName : "",
                                                         StyleColorName = MSS != null && C != null ? C.Name : "",
                                                         StyleGSM = MSS != null ? (double)MSS.GSM : 0,
                                                         POID = PO.CID,
                                                         Store_StockInFK = SIn.ID,
                                                         VMRawItemFK = SIn.ItemIDFK,
                                                         VMRawItemName = RI.Name,
                                                         StockInQuantity = SIn.ReceivedQty,
                                                         DescriptionSlave= POS.Description,
                                                         PreviousInvoiceQuantity = (from PIS in _db.Procurement_PurchaseInvoiceSlave
                                                                                    join PI in _db.Procurement_PurchaseInvoice on PIS.Procurement_PurchaseInvoiceFK equals PI.ID
                                                                                    where PI.Active && PIS.Active && PIS.Store_StockInFK == SIn.ID && PI.ID != PurchaseInvoiceID
                                                                                    select PIS.InvoiceQuantity).DefaultIfEmpty(0).Sum(),
                                                         VMUnitName = U.Name,
                                                         SuggestedPrice = SIn.UnitPrice,
                                                         InvoicePrice= SIn.UnitPrice
                                                     }).ToListAsync());

            vM.DataListSlave = vM.DataListSlave.Where(x => x.RestQuantity > 0).ToList();
            return vM;
        }
        public async Task<VMPurchaseInvoiceSlave> PurchaseInvoiceSlaveFromSInItemGet(int ItemID = 0, int PurchaseInvoiceID = 0)
        {

            VMPurchaseInvoiceSlave vM = new VMPurchaseInvoiceSlave();
            vM.Procurement_PurchaseInvoiceFK = PurchaseInvoiceID;
            vM.OriginType = await Task.Run(() => _db.Procurement_PurchaseInvoice.Where(x => x.ID == PurchaseInvoiceID).Select(x => (ProcurementOriginTypeEnum)x.OriginType).DefaultIfEmpty(ProcurementOriginTypeEnum.Regular).FirstOrDefaultAsync());
            vM.POIDVisible = true;
            vM.DataListSlave = new List<VMPurchaseInvoiceSlave>();
            vM.DataListSlave = await Task.Run(() => (from SIn in _db.Store_StockIn
                                                     join PO in _db.Procurement_PurchaseOrder
                                                     .Where(x => x.Active &&
                                                     x.Status == (int)POStatusEnum.FinalApproved || x.Status == (int)POStatusEnum.PartialGoodsReceived //||
                                                                                     //(x.Status == (int)POStatusEnum.GoodsReceived &&
                                                                                     //x.ID == (from PIS in _db.Procurement_PurchaseInvoiceSlave
                                                                                     //         join SIn in _db.Store_StockIn on PIS.Store_StockInFK equals SIn.ID into SIn_Join
                                                                                     //         from SIn in SIn_Join.DefaultIfEmpty()
                                                                                     //         where PIS.Procurement_PurchaseInvoiceFK == PurchaseInvoiceID
                                                                                     //         select SIn != null ? SIn.IPOFK : 0).DefaultIfEmpty(0).FirstOrDefault())
                                           ) on SIn.IPOFK equals PO.ID into PO_Join
                                                     from PO in PO_Join.DefaultIfEmpty()
                                                     join RI in _db.Common_RawItem on SIn.ItemIDFK equals RI.ID
                                                     join U in _db.Common_Unit on RI.Common_UnitFK equals U.ID
                                                     join MS in _db.Merchandising_Style on SIn.Merchandising_StyleFK equals MS.ID into MS_Join
                                                     from MS in MS_Join.DefaultIfEmpty()
                                                     join MBO in _db.Merchandising_BuyerOrder on MS.Merchandising_BuyerOrderFK equals MBO.ID into MBO_Join
                                                     from MBO in MBO_Join.DefaultIfEmpty()
                                                     join MSS in _db.Merchandising_StyleSlave on SIn.Merchandising_StyleSlaveFK equals MSS.ID into MSS_Join
                                                     from MSS in MSS_Join.DefaultIfEmpty()
                                                     join C in _db.Common_Color on MSS.Common_ColorFK equals C.ID into C_Join
                                                     from C in C_Join.DefaultIfEmpty()

                                                     where SIn.Active && SIn.ItemIDFK == ItemID

                                                     select new VMPurchaseInvoiceSlave
                                                     {
                                                         Procurement_PurchaseInvoiceFK = PurchaseInvoiceID,
                                                         Procurement_PurchaseOrderSlaveID = SIn.Procurement_PurchaseOrderSlaveFK,
                                                         VMMerchandising_StyleID = SIn.Merchandising_StyleFK,
                                                         VMMerchandising_StyleSlaveID = SIn.Merchandising_StyleSlaveFK,
                                                         //VMMerchandising_BOFID=SIn.bof
                                                         BONoStyleName = MS != null && MBO != null ? MBO.BuyerPO + "/" + MS.StyleName : "",
                                                         StyleColorName = MSS != null && C != null ? C.Name : "",
                                                         StyleGSM = MSS != null ? (double)MSS.GSM : 0,
                                                         POID = PO.CID,
                                                         Store_StockInFK = SIn.ID,
                                                         VMRawItemFK = SIn.ItemIDFK,
                                                         VMRawItemName = RI.Name,
                                                         StockInQuantity = SIn.ReceivedQty,
                                                         PreviousInvoiceQuantity = _db.Procurement_PurchaseInvoiceSlave.Include(x => x.Procurement_PurchaseInvoice)
                                                         .Where(x => x.Procurement_PurchaseInvoice.Active && x.Active && x.Store_StockInFK == SIn.ID && x.Procurement_PurchaseInvoice.ID != PurchaseInvoiceID)
                                                         .Select(x => x.InvoiceQuantity).DefaultIfEmpty(0).Sum(),
                                                         VMUnitName = U.Name,
                                                         SuggestedPrice = SIn.UnitPrice,
                                                     }).ToListAsync());
            vM.DataListSlave = vM.DataListSlave.Where(x => x.RestQuantity > 0).ToList();

            return vM;
        }
        #endregion

        /// Update by Mintu
        /// 
        public async Task<VMPurchaseOrder> PurchaseOrderGet()
        {
            VMPurchaseOrder vMPurchaseOrder = new VMPurchaseOrder();

            vMPurchaseOrder.DataListforCommercial = new List<VMPurchaseOrder>();
            //vMPurchaseOrder.Status = model.Status;
            vMPurchaseOrder.CreatedBy = "Admin";

            vMPurchaseOrder.DataListforCommercial = await Task.Run(() => (from PO in _db.Procurement_PurchaseOrder
                                                                          join C in _db.Common_Currency on PO.Common_CurrencyFK equals C.ID into C_Join
                                                                          from C in C_Join.DefaultIfEmpty()
                                                                          join E in _db.User_User on PO.UserID equals E.ID into E_Join
                                                                          from E in E_Join.DefaultIfEmpty()
                                                                          join S in _db.Common_Supplier on PO.Common_SupplierFK equals S.ID into S_Join
                                                                          from S in S_Join.DefaultIfEmpty()
                                                                          where PO.Active == true
                                                                          select new VMPurchaseOrder
                                                                          {
                                                                              ID = PO.ID,
                                                                              CID = PO.CID,
                                                                              OrderDate = PO.OrderDate,
                                                                              POPaymentMethod = (SupplierPaymentMethodEnum)PO.POPaymentMethod,
                                                                              POType = (POTypeEnum)PO.POType,
                                                                              CreationDate = PO.CreationDate,
                                                                              CreatedBy = PO.CreatedBy,
                                                                              ApprovedDate = PO.ApprovedDate.HasValue ? PO.ApprovedDate.Value : DateTime.MinValue,
                                                                              RevisionNo = PO.RevisionNo,
                                                                              Description = PO.Description,
                                                                              Status = (POStatusEnum)PO.Status,
                                                                              VMEmployeeFK = E != null ? E.ID : 0,
                                                                              VMEmployeeName = E != null ? E.Name : "",
                                                                              VMSupplierFK = PO.Common_SupplierFK,
                                                                              VMSupplierName = S.Name,
                                                                              VMCurrencyFK = PO.Common_CurrencyFK,
                                                                              VMCurrencyName = C.Name,
                                                                              TermsAndCondition = PO.TermsAndCondition,
                                                                              TotalPOValue = PO.TotalPOValue
                                                                          }).OrderByDescending(x => x.ID).AsEnumerable());

            vMPurchaseOrder.CurrencyList = new Microsoft.AspNetCore.Mvc.Rendering.SelectList(CurrencyDropDownList(), "Value", "Text");
            vMPurchaseOrder.SupplierList = new Microsoft.AspNetCore.Mvc.Rendering.SelectList(SupplierDropDownList(), "Value", "Text");
            vMPurchaseOrder.UserList = new Microsoft.AspNetCore.Mvc.Rendering.SelectList(UserDropDownList(), "Value", "Text");
            return vMPurchaseOrder;
        }

        #region Procurment new code by Anis
        // Procurement_PurchaseInvoice
        public async Task<VMPurchaseInvoiceSlave> PurchaseInvoiceSlaveFromSInGet(int POID = 0)
        {

            VMPurchaseInvoiceSlave vM = new VMPurchaseInvoiceSlave();
            vM = await Task.Run(() => (from PI in _db.Procurement_PurchaseOrder
                                       join t2 in _db.Procurement_PurchaseOrderSlave on PI.ID equals t2.Procurement_PurchaseOrderFK
                                       join S in _db.Common_Supplier on PI.Common_SupplierFK equals S.ID
                                       where PI.ID == POID
                                       select new VMPurchaseInvoiceSlave
                                       {
                                           Procurement_PurchaseInvoiceFK = PI.ID,
                                           CID = PI.CID,
                                           // InvoiceDate = PI.InvoiceDate,
                                           PaymentMethod = (SupplierPaymentMethodEnum)PI.POPaymentMethod,
                                           // CreationDate = PI.CreationDate,
                                           // CreatedBy = PI.CreatedBy,
                                           // ApprovedDate = PI.ApprovedDate,
                                           //  Description = PI.Description,
                                           Status = (PurchaseInvoiceStatusEnum)PI.Status,
                                           VMSupplierFK = PI.Common_SupplierFK,
                                           VMSupplierName = S != null ? S.Name : "",
                                           OriginType = (ProcurementOriginTypeEnum)PI.OriginType,
                                       }).OrderByDescending(x => x.ID).FirstOrDefaultAsync());
            vM.POIDVisible = false;
            vM.DataListSlave = new List<VMPurchaseInvoiceSlave>();
            vM.DataListSlave = await Task.Run(() => (from SIn in _db.Store_StockIn
                                                     join SInMaster in _db.Store_StockInMaster on SIn.Store_StockInMasterFK equals SInMaster.ID
                                                     join PO in _db.Procurement_PurchaseOrder on SIn.IPOFK equals PO.ID into PO_Join
                                                     from PO in PO_Join.DefaultIfEmpty()
                                                     join RI in _db.Common_RawItem on SIn.ItemIDFK equals RI.ID
                                                     join U in _db.Common_Unit on RI.Common_UnitFK equals U.ID
                                                     join MS in _db.Merchandising_Style on SIn.Merchandising_StyleSlaveFK equals MS.ID into MS_Join
                                                     from MS in MS_Join.DefaultIfEmpty()
                                                     join MBO in _db.Merchandising_BuyerOrder on MS.Merchandising_BuyerOrderFK equals MBO.ID into MBO_Join
                                                     from MBO in MBO_Join.DefaultIfEmpty()
                                                     join MSS in _db.Merchandising_StyleSlave on SIn.Merchandising_StyleSlaveFK equals MSS.ID into MSS_Join
                                                     from MSS in MSS_Join.DefaultIfEmpty()
                                                     join C in _db.Common_Color on MSS.Common_ColorFK equals C.ID into C_Join
                                                     from C in C_Join.DefaultIfEmpty()
                                                     where SIn.Active && SIn.IPOFK == POID
                                                     select new VMPurchaseInvoiceSlave
                                                     {
                                                         Procurement_PurchaseOrderSlaveID = SIn.Procurement_PurchaseOrderSlaveFK,
                                                         VMMerchandising_StyleID = SIn.Merchandising_StyleFK,
                                                         VMMerchandising_StyleSlaveID = SIn.Merchandising_StyleSlaveFK,
                                                         BONoStyleName = MS != null && MBO != null ? MBO.BuyerPO + "/" + MS.StyleName : "",
                                                         StyleColorName = MSS != null && C != null ? C.Name : "",
                                                         StyleGSM = MSS != null ? (double)MSS.GSM : 0,
                                                         POID = PO.CID,
                                                         StrChallanNo = SInMaster.ChallanNo,
                                                         Store_StockInFK = SIn.ID,
                                                         VMRawItemFK = SIn.ItemIDFK,
                                                         VMRawItemName = RI.Name,
                                                         POQuantity = (from POS in _db.Procurement_PurchaseOrderSlave
                                                                       where POS.ID == SIn.Procurement_PurchaseOrderSlaveFK
                                                                       && POS.Procurement_PurchaseOrderFK == POID
                                                                       && POS.Common_RawItemFK == SIn.Common_RawItemFK
                                                                       select POS.PurchaseQuantity).DefaultIfEmpty(0).Sum(),
                                                         StockInQuantity = SIn.ReceivedQty,
                                                         PreviousInvoiceQuantity =
                                                         (from PIS in _db.Procurement_PurchaseInvoiceSlave
                                                          join PI in _db.Procurement_PurchaseInvoice on PIS.Procurement_PurchaseInvoiceFK equals PI.ID
                                                          where PIS.Active && PIS.Store_StockInFK == SIn.ID && PI.Active
                                                          && PIS.Common_RawItemFK == SIn.Common_RawItemFK
                                                          group PIS.InvoiceQuantity by new { PIS.Common_RawItemFK } into all
                                                          select all.Sum()).DefaultIfEmpty(0).Sum(),
                                                         VMUnitName = U.Name,
                                                         SuggestedPrice = SIn.UnitPrice,
                                                     }).Distinct().ToListAsync());

            vM.DataListSlave = vM.DataListSlave.Where(x => x.RestQuantity > 0).ToList();
            return vM;
        }


        public async Task<int> PurchaseInvoiceAddnew(VMPurchaseInvoice vMPurchaseInvoice)
        {
            var result = -1;
            string poidPRef = @"IN" +
                            DateTime.Now.ToString("yy") +
                            DateTime.Now.ToString("MM") + @"-" +
                            ((vMPurchaseInvoice.OriginType == ProcurementOriginTypeEnum.Regular) ? "G" : "O") +
                            @"-P";
            Procurement_PurchaseInvoice Procurement_PurchaseInvoice = new Procurement_PurchaseInvoice
            {
                CID = poidPRef + await Task.Run(() => (((_db.Procurement_PurchaseInvoice.AnyAsync(x => x.CID.ToUpper().Contains(poidPRef.ToUpper())).Result ? _db.Procurement_PurchaseInvoice.Where(x => x.CID.ToUpper().Contains(poidPRef.ToUpper())).MaxAsync(x => Int32.Parse(!string.IsNullOrEmpty(x.CID.Replace(poidPRef, "")) ? x.CID.Replace(poidPRef, "") : "0")).Result : 0) + 1).ToString().PadLeft(6, '0'))),
                InvoiceDate = vMPurchaseInvoice.InvoiceDate,
                Common_SupplierFK = vMPurchaseInvoice.VMSupplierFK,
                Description = vMPurchaseInvoice.Description,
                PaymentMethod = (int)vMPurchaseInvoice.PaymentMethod,
                User = vMPurchaseInvoice.User,
                UserID = vMPurchaseInvoice.UserID,
                CreatedBy = vMPurchaseInvoice.User,
                Status = (int)POStatusEnum.Draft,
                OriginType = (int)vMPurchaseInvoice.OriginType,
            };
            _db.Procurement_PurchaseInvoice.Add(Procurement_PurchaseInvoice);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = Procurement_PurchaseInvoice.ID;
            }
            return result;
        }

        public async Task<int> PurchaseInvoiceSlaveListAddNew(List<VMPurchaseInvoiceSlave> vM)
        {
            var result = -1;
            List<Procurement_PurchaseInvoiceSlave> enPISExisting = await Task.Run(() => (_db.Procurement_PurchaseInvoiceSlave.Where(x => vM.Any(y => x.Procurement_PurchaseInvoiceFK == y.Procurement_PurchaseInvoiceFK && x.Common_RawItemFK == y.VMRawItemFK && x.Store_StockInFK == y.Store_StockInFK) && x.Active).ToListAsync()));
            List<Procurement_PurchaseInvoiceSlave> enPIS = vM.Where(x => !enPISExisting.Any(y => x.VMRawItemFK == x.VMRawItemFK))
                                                            .Select(x => new Procurement_PurchaseInvoiceSlave
                                                            {
                                                                Procurement_PurchaseInvoiceFK = x.Procurement_PurchaseInvoiceFK,
                                                                Procurement_PurchaseOrderSlaveID = x.Procurement_PurchaseOrderSlaveID,
                                                                Merchandising_StyleID = x.VMMerchandising_StyleID,
                                                                Merchandising_StyleSlaveID = x.VMMerchandising_StyleSlaveID,
                                                                Merchandising_BOFID = x.VMMerchandising_BOFID,
                                                                Store_StockInFK = x.Store_StockInFK,
                                                                Common_RawItemFK = x.VMRawItemFK,
                                                                InvoiceQuantity = x.InvoiceQuantity,
                                                                InvoicePrice = x.InvoicePrice,
                                                                Description = x.DescriptionSlave,
                                                                User = x.User,
                                                                UserID = x.UserID,
                                                            }).ToList();
            if (enPIS != null && enPIS.Count() > 0)
            {
                await _db.Procurement_PurchaseInvoiceSlave.AddRangeAsync(enPIS);
                if (await _db.SaveChangesAsync() > 0)
                {
                    Procurement_PurchaseInvoice enPI = await _db.Procurement_PurchaseInvoice.FindAsync(enPIS.First().Procurement_PurchaseInvoiceFK);
                    enPI.TotalInvoiceValue = await _db.Procurement_PurchaseInvoiceSlave.Where(x => x.Procurement_PurchaseInvoiceFK == enPIS.First().Procurement_PurchaseInvoiceFK && x.Active).SumAsync(x => x.InvoicePrice * x.InvoiceQuantity);
                    result = await _db.SaveChangesAsync();
                }
            }

            return result;
        }

        public async Task<int> ProcurementMasterInvoicePush(VMPurchaseInvoiceSlave vMPurchaseInvoiceSlave)
        {
            int result = -1;
            //GET Account Journal and Update Status.....
            string poidPRef = @"IN" +
                            DateTime.Now.ToString("yy") +
                            DateTime.Now.ToString("MM") + @"-" +
                            ((vMPurchaseInvoiceSlave.OriginType == ProcurementOriginTypeEnum.Regular) ? "G" : "O") +
                            @"-P";
            Procurement_PurchaseInvoice procurement_PurchaseInvoice = new Procurement_PurchaseInvoice();
            procurement_PurchaseInvoice.CID = poidPRef + await Task.Run(() => (((_db.Procurement_PurchaseInvoice.AnyAsync(x => x.CID.ToUpper().Contains(poidPRef.ToUpper())).Result ? _db.Procurement_PurchaseInvoice.Where(x => x.CID.ToUpper().Contains(poidPRef.ToUpper())).MaxAsync(x => Int32.Parse(!string.IsNullOrEmpty(x.CID.Replace(poidPRef, "")) ? x.CID.Replace(poidPRef, "") : "0")).Result : 0) + 1).ToString().PadLeft(6, '0')));
            procurement_PurchaseInvoice.InvoiceDate = vMPurchaseInvoiceSlave.InvoiceDate;
            procurement_PurchaseInvoice.Common_SupplierFK = vMPurchaseInvoiceSlave.VMSupplierFK;
            procurement_PurchaseInvoice.Description = vMPurchaseInvoiceSlave.Description;
            procurement_PurchaseInvoice.PaymentMethod = (int)vMPurchaseInvoiceSlave.PaymentMethod;
            procurement_PurchaseInvoice.User = vMPurchaseInvoiceSlave.User;
            procurement_PurchaseInvoice.UserID = vMPurchaseInvoiceSlave.UserID;
            procurement_PurchaseInvoice.CreatedBy = vMPurchaseInvoiceSlave.User;
            procurement_PurchaseInvoice.Status = (int)PurchaseInvoiceStatusEnum.Draft;
            procurement_PurchaseInvoice.OriginType = (int)vMPurchaseInvoiceSlave.OriginType;
            procurement_PurchaseInvoice.InvoiceMatureDate = vMPurchaseInvoiceSlave.InvoiceMatureDate;
            procurement_PurchaseInvoice.SupplierInvoiceNo = vMPurchaseInvoiceSlave.SupplierInvoiceNo;
            procurement_PurchaseInvoice.TotalInvoiceValue = vMPurchaseInvoiceSlave.TotalInvoiceValue;

            procurement_PurchaseInvoice.Procurement_PurchaseInvoiceSlave = new List<Procurement_PurchaseInvoiceSlave>();
            //  List<Procurement_PurchaseInvoiceSlave> enPISExisting = await Task.Run(() => (_db.Procurement_PurchaseInvoiceSlave.Where(x => vM.Any(y => x.Procurement_PurchaseInvoiceFK == y.Procurement_PurchaseInvoiceFK && x.Common_RawItemFK == y.VMRawItemFK && x.Store_StockInFK == y.Store_StockInFK) && x.Active).ToListAsync()));
            procurement_PurchaseInvoice.Procurement_PurchaseInvoiceSlave = vMPurchaseInvoiceSlave.DataListSlave.Select(x => new Procurement_PurchaseInvoiceSlave
            {
                Procurement_PurchaseInvoiceFK = x.Procurement_PurchaseInvoiceFK,
                Procurement_PurchaseOrderSlaveID = x.Procurement_PurchaseOrderSlaveID,
                Merchandising_StyleID = x.VMMerchandising_StyleID,
                Merchandising_StyleSlaveID = x.VMMerchandising_StyleSlaveID,
                Merchandising_BOFID = x.VMMerchandising_BOFID,
                Store_StockInFK = x.Store_StockInFK,
                Common_RawItemFK = x.VMRawItemFK,
                InvoiceQuantity = x.InvoiceQuantity,
                InvoicePrice = x.InvoicePrice,
                Description = x.DescriptionSlave,
                User = x.User,
                UserID = x.UserID,
                StrChallanNo = x.StrChallanNo
            }).ToList();

            await _db.Procurement_PurchaseInvoice.AddRangeAsync(procurement_PurchaseInvoice);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = procurement_PurchaseInvoice.ID;
            }
            return result;
        }

        public async Task<List<VMPurchaseInvoiceSlave>> ApprovedSInItemListNew()
        {
            //int nSupplierId = 0, int PIID = 0, int orgID = 0
            //populate RawItem DDL from PRSLave
            return
           await Task.Run(() => (from SIn in _db.Store_StockIn.Where(x => x.Active && x.IsLocked)
                                 join PO in _db.Procurement_PurchaseOrder
                                 .Where(x => x.Active &&
                                 //  x.OriginType == orgID &&
                                 //  x.Common_SupplierFK == nSupplierId &&
                                 (x.Status == (int)POStatusEnum.FinalApproved ||
                                 x.Status == (int)POStatusEnum.PartialGoodsReceived ||
                                 (x.Status == (int)POStatusEnum.GoodsReceived &&
                                 x.ID == (from PIS in _db.Procurement_PurchaseInvoiceSlave
                                          join SIn in _db.Store_StockIn on PIS.Store_StockInFK equals SIn.ID into SIn_Join
                                          from SIn in SIn_Join.DefaultIfEmpty()
                                              // where PIS.Procurement_PurchaseInvoiceFK == PIID
                                          select SIn != null ? SIn.IPOFK : 0)
                                                      .DefaultIfEmpty(0).FirstOrDefault()))) on SIn.IPOFK equals PO.ID
                                 join RI in _db.Common_RawItem on SIn.ItemIDFK equals RI.ID into RI_Join
                                 from RI in RI_Join.DefaultIfEmpty()
                                 where SIn.Active &&
                                 SIn.ReceivedQty >
                                 _db.Procurement_PurchaseInvoiceSlave.Include(x => x.Procurement_PurchaseInvoice)
                                 .Where(x => x.Procurement_PurchaseInvoice.Active &&
                                 x.Active &&
                                 x.Store_StockInFK == SIn.ID //&& x.Procurement_PurchaseInvoice.ID != PIID
                                 )
                                 .Select(x => x.InvoiceQuantity)
                                 .DefaultIfEmpty(0)
                                 .Sum()
                                 select new VMPurchaseInvoiceSlave
                                 {
                                     POID = PO.CID,
                                     Procurement_PurchaseOrderID = PO.ID,
                                     VMRawItemFK = RI.ID,
                                     VMRawItemName = RI.Name
                                 }).ToListAsync());
        }

        #endregion
    }
}