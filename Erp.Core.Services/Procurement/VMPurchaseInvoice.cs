﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
//using System.Text;

namespace Erp.Core.Services.Procurement
{
    public class VMPurchaseInvoice : BaseVM
    {
        public string CID { get; set; }
        public ProcurementOriginTypeEnum OriginType { get; set; }
        public string OriginTypeName { get { return BaseFunctionalities.GetEnumDescription(this.OriginType); } }
        public SelectList OriginTypeList { get { return new SelectList(BaseFunctionalities.GetEnumList<ProcurementOriginTypeEnum>(), "Value", "Text"); } }
        public string Description { get; set; }
        public DateTime CreationDate { get; set; } = DateTime.Now;
        public string CreationDateControlString { get { return this.CreationDate != DateTime.MinValue ? this.CreationDate.ToString("yyyy-MM-dd") : ""; } }
        public string CreationDateTableString { get { return this.CreationDate != DateTime.MinValue ? this.CreationDate.ToString("MMM dd, yyyy") : ""; } }
        public string CreatedBy { get; set; }
        
        public DateTime ApprovedDate { get; set; }
        
        public string CancellationRemaks { get; set; }
        public double TotalInvoiceValue { get; set; }

        [Required(ErrorMessage = "Invoice Date is Required")]
        public DateTime InvoiceDate { get; set; }
        public string InvoiceDateControlString { get { return this.InvoiceDate != DateTime.MinValue ? this.InvoiceDate.ToString("yyyy-MM-dd") : ""; } }
        public string InvoiceDateTableString { get { return this.InvoiceDate != DateTime.MinValue ? this.InvoiceDate.ToString("MMM dd, yyyy") : ""; } }
        public string VMSupplierName { get; set; }

        [Required(ErrorMessage = "Supplier is Required")]
        public int VMSupplierFK { get; set; }
        public SelectList VMSupplierList { get; set; } = new SelectList(new List<object>(), "Value", "Text");


        public int InvoiceActionId { get; set; }
        public PurchaseInvoiceActionEnum InvoiceActionEnum { get { return (PurchaseInvoiceActionEnum)this.InvoiceActionId; } }
        public PurchaseInvoiceStatusEnum Status { get; set; } = PurchaseInvoiceStatusEnum.Draft;
        public string AuthorizationStatus { get { return BaseFunctionalities.GetEnumDescription(Status); } }
        public List<VMPurchaseInvoice> DataList { get; set; }
        public SupplierPaymentMethodEnum PaymentMethod { get; set; } = SupplierPaymentMethodEnum.Cash;
        public string PaymentMethodName { get { return BaseFunctionalities.GetEnumDescription(PaymentMethod); } }
        public SelectList PaymentMethodList { get { return new SelectList(BaseFunctionalities.GetEnumList<SupplierPaymentMethodEnum>(), "Value", "Text"); } }

        public string POCID { get; set; }
        public int Procurement_PurchaseOrderID { get; set; }

        public string DraftStatusCount { get; set; } = "0";
        public string SubmittedStatusCount { get; set; } = "0";
        public string PrimaryStatusCount { get; set; } = "0";
        public string AccountsStatusCount { get; set; } = "0";
        public string FinalStatusCount { get; set; } = "0";
        public string PartialPaidStatusCount { get; set; } = "0";
        public string FullPaidStatusCount { get; set; } = "0";
        public string ClosedStatusCount { get; set; } = "0";
        public string HoldStatusCount { get; set; } = "0";
        public bool HasSlave { get; set; }

        //new Property by Anis
        public DateTime InvoiceMatureDate { get; set; }
        public string SupplierInvoiceNo { get; set; }
    }




    public class VMPurchaseInvoiceSlave : VMPurchaseInvoice
    {
        public bool IsForSave { get; set; }
        public bool POIDVisible { get; set; }
        public int Procurement_PurchaseInvoiceFK { get; set; }
        public int Procurement_PurchaseOrderSlaveID { get; set; }
        public int Procurement_PurchaseOrderID { get; set; }
        public string POID { get; set; }
        public int Store_StockInFK { get; set; }
        private double _InvoiceQuantity = 0;

        public double PreviousInvoiceQuantity { get; set; }


        public double SuggestedPrice { get; set; }
        [Required(ErrorMessage = "Invoice Price is Required")]
        public double InvoicePrice { get; set; }
        
        public int VMRawItemFK { get; set; }
        public string VMRawItemName { get; set; }
        

        public string DescriptionSlave { get; set; }
        public double StockInQuantity { get; set; }

        public double AvailableQuantity { get; set; }
       
        public double RestQuantity
        {
            get
            {
                return StockInQuantity - (PreviousInvoiceQuantity);
            }
        }

        [Required(ErrorMessage = "Purchase Quantity is Required")]
        public double InvoiceQuantity
        {
            get
            {
                if (_InvoiceQuantity <= 0)
                {
                    if (StockInQuantity > (PreviousInvoiceQuantity))
                    { _InvoiceQuantity = StockInQuantity - (PreviousInvoiceQuantity); }
                }
                return _InvoiceQuantity;
            }

            set { _InvoiceQuantity = value; }
        }

        [Range(typeof(bool), "true", "true", ErrorMessage = "Purchase Quantity must be smaller than or equal to Rest Quantity")]
        public bool IsValidQuantity { get { return Store_StockInFK > 0 && RestQuantity >= InvoiceQuantity; } }
        [Range(typeof(bool), "true", "true", ErrorMessage = "Minimum Purchasing is 1")]
        public bool IsValidPrice { get { return InvoicePrice >= 1; } }

        public double TotalPrice { get { return InvoiceQuantity * InvoicePrice; } }
        public string VMUnitName { get; set; }
        public int? VMMerchandising_StyleID { get; set; }
        public int? VMMerchandising_StyleSlaveID { get; set; }
        public int? VMMerchandising_BOFID { get; set; }
        public string BONoStyleName { get; set; }
        public string StyleColorName { get; set; }
        public double StyleGSM { get; set; }

        public string StyleColorGSM { get { return !string.IsNullOrEmpty(StyleColorName) && StyleGSM > 0 ? StyleColorName + "/" + StyleGSM.ToString() : !string.IsNullOrEmpty(StyleColorName) ? StyleGSM.ToString() : ""; } }
        public List<VMPurchaseInvoiceSlave> DataListSlave { get; set; }
        public SelectList ApprovedSInList { get; set; } = new SelectList(new List<object>());
        public SelectList ApprovedSInItemList { get; set; } = new SelectList(new List<object>());


        //new Property by Anis
        public string StrChallanNo { get; set; }
        public double POQuantity { get; set; }
        public int Procurement_PurchaseOrderSlaveFk { get;  set; }
    }
}