﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
//using System.Text;

namespace Erp.Core.Services.Procurement
{
    public class VMPurchaseOrder : BaseVM
    {

        public DateTime CreationDate { get; set; } = DateTime.Now;
        public string CreationDateControlString { get { return this.CreationDate != DateTime.MinValue ? this.CreationDate.ToString("yyyy-MM-dd") : ""; } }
        public string CreationDateTableString { get { return this.CreationDate != DateTime.MinValue ? this.CreationDate.ToString("MMM dd, yyyy") : ""; } }
        public ProcurementOriginTypeEnum OriginType { get; set; }
        public string OriginTypeName {
            get {
                return BaseFunctionalities.GetEnumDescription(this.OriginType);
            }
        }
        public SelectList OriginTypeList { get { return new SelectList(BaseFunctionalities.GetEnumList<ProcurementOriginTypeEnum>(), "Value", "Text"); } }

        public string CreatedBy { get; set; }
        [Required(ErrorMessage = "Order Date is Required")]
        public DateTime OrderDate { get; set; }
        public string OrderDateControlString { get { return this.OrderDate != DateTime.MinValue ? this.OrderDate.ToString("yyyy-MM-dd") : ""; } }
        public string OrderDateTableString { get { return this.OrderDate != DateTime.MinValue ? this.OrderDate.ToString("MMM dd, yyyy") : ""; } }

        public DateTime ApprovedDate { get; set; }
        public string ApprovedDateControlString { get { return this.ApprovedDate != DateTime.MinValue ? this.ApprovedDate.ToString("yyyy-MM-dd") : ""; } }
        public string ApprovedDateTableString { get { return this.ApprovedDate != DateTime.MinValue ? this.ApprovedDate.ToString("MMM dd, yyyy") : ""; } }

        public string VMCurrencyName { get; set; }
        [Required(ErrorMessage = "Currency is Required")]
        public int VMCurrencyFK { get; set; }
        [Required(ErrorMessage = "Terms & Conditions is Required")]
        public string TermsAndCondition { get; set; }




        public string ProtectedPOID { get; set; }
        public string CID { get; set; }
        public string VMSupplierName { get; set; }
        
        [Required(ErrorMessage = "Supplier is Required")]
        public int VMSupplierFK { get; set; }

        public string Description { get; set; }

        public int RevisionNo { get; set; }

        [Required(ErrorMessage = "Requestor is Required")]
        public int VMEmployeeFK { get; set; }
        public string VMEmployeeName { get; set; }

        public int POActionId { get; set; }
        public POActionEnum POActionEnum { get { return (POActionEnum)this.POActionId; } }
        public POStatusEnum Status { get; set; } = POStatusEnum.Draft;
        public string AuthorizationStatus { get { return BaseFunctionalities.GetEnumDescription(Status); } }
        public List<VMPurchaseOrder> DataList { get; set; }
        /// <summary>
        /// Update by Mintu
        /// </summary>
        public IEnumerable<VMPurchaseOrder> DataListforCommercial { get; set; }
        public SelectList CurrencyList { get; set; } = new SelectList(new List<object>());
        public SelectList SupplierList { get; set; } = new SelectList(new List<object>());
        public SelectList UserList { get; set; } = new SelectList(new List<object>());
        public SupplierPaymentMethodEnum POPaymentMethod { get; set; } = SupplierPaymentMethodEnum.Cash;
        public string POPaymentMethodName { get { return BaseFunctionalities.GetEnumDescription(POPaymentMethod); } }
        public SelectList POPaymentMethodList { get { return new SelectList(BaseFunctionalities.GetEnumList<SupplierPaymentMethodEnum>(), "Value", "Text"); } }
        public POTypeEnum POType { get; set; } = POTypeEnum.Procurement;
        public string POTypeName { get { return BaseFunctionalities.GetEnumDescription(POType); } }
        public SelectList POTypeList { get { return new SelectList(BaseFunctionalities.GetEnumList<POTypeEnum>(), "Value", "Text"); } }



        public string DraftStatusCount { get; set; } = "0";
        public string SubmittedStatusCount { get; set; } = "0";
        public string PrimaryStatusCount { get; set; } = "0";
        public string FinalStatusCount { get; set; } = "0";
        public string SecondaryStatusCount { get; set; } = "0";
        public string InspectionStatusCount { get; set; }
        public string GoodsReceivedStatusCount { get; set; } = "0";
        public string PartialGoodsReceivedStatusCount { get; set; } = "0";
        public string ClosedStatusCount { get; set; }
        public string HoldStatusCount { get; set; } = "0";
        public double TotalPOValue { get; set; }
        public bool HasSlave { get; set; }
    }




    public class VMPurchaseOrderSlave : VMPurchaseOrder
    {
        public bool IsForSave { get; set; }
        private double _PurchaseQuantity = 0;

        public double ProcuredQuantity { get; set; }


        public double SuggestedPrice { get; set; }
        [Required(ErrorMessage = "Purchase Price is Required")]
        
        public double PurchasingPrice { get; set; }
        [Required(ErrorMessage = "Raw Item is Required")]
        public int Procurement_PurchaseRequisitionSlaveFK { get; set; }
        public int Procurement_PurchaseOrderFK { get; set; }
        public int VMRawCategoryFK { get; set; }
        public string VMRawCategoryName { get; set; }
        public int VMRawSubCategoryFK { get; set; }
        public string VMRawSubCategoryName { get; set; }
        public int VMRawItemFK { get; set; }
        public string VMRawItemName { get; set; }
        
        public string PRID { get; set; }
        public bool PRIDVisible { get; set; }

        public string DescriptionSlave { get; set; }

        public double RequisitionQuantity { get; set; }
        
        public double AvailableQuantity { get; set; }

        public double RestQuantity
        {
            get
            {
                return RequisitionQuantity - (ProcuredQuantity);
            }
        }

        
        public double PurchaseQuantity
        {
            get
            {
                if (_PurchaseQuantity <= 0)
                {
                    if (RequisitionQuantity > (ProcuredQuantity + AvailableQuantity))
                    { _PurchaseQuantity = RequisitionQuantity - (ProcuredQuantity + AvailableQuantity); }
                }
                return _PurchaseQuantity;
            }

            set { _PurchaseQuantity = value; }
        }

        [Range(typeof(bool), "true", "true", ErrorMessage = "Purchase Quantity must be smaller than or equal to Rest Quantity")]
        public bool IsValidQuantity { get { return Procurement_PurchaseRequisitionSlaveFK > 0 && RestQuantity >= PurchaseQuantity; } }
        [Range(typeof(bool), "true", "true", ErrorMessage = "Minimum Purchasing is 1")]
        public bool IsValidPrice { get { return PurchasingPrice >= 1; } }

        public double TotalPrice { get { return PurchaseQuantity * PurchasingPrice; } }
        public string VMUnitName { get; set; }
        public string StyleColorName { get; set; }
        public double StyleGSM { get; set; }

        public string StyleColorGSM { get { return !string.IsNullOrEmpty(StyleColorName) && StyleGSM > 0 ? StyleColorName + "/" + StyleGSM.ToString() : !string.IsNullOrEmpty(StyleColorName) && StyleGSM <= 0 ? StyleColorName : string.IsNullOrEmpty(StyleColorName) && StyleGSM > 0? StyleGSM.ToString():""; } }

        public bool InspectionRequiredInPR { get; set; }
        public bool InspectionRequired { get; set; }
        public string Inspection { get { return this.InspectionRequired ? "Required" : "Not Required"; } }
        public int? VMMerchandising_StyleID { get; set; }
        public int? VMMerchandising_StyleSlaveID { get; set; }
        public int? VMMerchandising_BOFID { get; set; }
        public string BONoStyleName { get; set; }
        public List<VMPurchaseOrderSlave> DataListSlave { get; set; }
        public SelectList RawCategoryList { get; set; } = new SelectList(new List<object>());
        public SelectList RawSubCategoryList { get; set; } = new SelectList(new List<object>());
        public SelectList RawItemList { get; set; } = new SelectList(new List<object>());
        public SelectList ApprovedPRList { get; set; } = new SelectList(new List<object>());
        public SelectList ApprovedPRItemList { get; set; } = new SelectList(new List<object>());
    }
}