﻿using Erp.Core.Entity.Common;
using Erp.Core.Services.Home;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
//using System.Text;

namespace Erp.Core.Services.Procurement
{
    public class VMPurchaseRequisition : BaseVM
    {
        public string CID { get; set; }
        public int PRActionId { get; set; }
        public PRActionEnum PRActionEnum { get { return (PRActionEnum)this.PRActionId; } }
        public PRTypeEnum PRType { get; set; }
        public string PRTypeName { get { return BaseFunctionalities.GetEnumDescription(this.PRType); } }
        public SelectList PRTypeList { get { return new SelectList(BaseFunctionalities.GetEnumList<PRTypeEnum>(), "Value", "Text"); } }
        public ProcurementOriginTypeEnum OriginType { get; set; }
        public string OriginTypeName { get { return BaseFunctionalities.GetEnumDescription(this.OriginType); } }
        public SelectList OriginTypeList { get { return new SelectList(BaseFunctionalities.GetEnumList<ProcurementOriginTypeEnum>(), "Value", "Text"); } }

        public string Description { get; set; }
        public PRStatusEnum Status { get; set; }
        public string AuthorizationStatus { get { return BaseFunctionalities.GetEnumDescription(Status); } }
        public DateTime CreationDate { get; set; } = DateTime.Now;
        public string CreationDateControlString { get { return this.CreationDate != DateTime.MinValue ? this.CreationDate.ToString("yyyy-MM-dd") : ""; } }
        public string CreationDateTableString { get { return this.CreationDate != DateTime.MinValue ? this.CreationDate.ToString("MMM dd, yyyy") : ""; } }

        public string CancellationRemaks { get; set; }
        public int VMClosedByUserFK { get; set; }
        public string VMClosedByUserName { get; set; }

        public string CreatedBy { get; set; } = "UserOne";
        public string VMDepartmentName { get; set; } = "Accounts";

        public string ItemNames { get; set; }

        public List<VMPurchaseRequisition> DataList { get; set; }

        public string DraftStatusCount { get; set; } = "0";
        public string SubmittedStatusCount { get; set; } = "0";
        public string PrimaryStatusCount { get; set; } = "0";
        public string FinalStatusCount { get; set; } = "0";
        public string ProcurementStatusCount { get; set; } = "0";
        public string PartialPOStatusCount { get; set; } = "0";
        public string FullPOStatusCount { get; set; } = "0";
        public string InspectionStatusCount { get; set; }
        public string GoodsReceivedStatusCount { get; set; }
        public string ClosedStatusCount { get; set; }
        public string HoldStatusCount { get; set; } = "0";
        public bool HasSlave { get; set; } //= false;
    }

    public class VMPurchaseRequisitionSlave : VMPurchaseRequisition
    {
        //private double _RequisitionQuantity = 0;
        public double PRRaisedQuantity { get; set; }
        public double StyleQuantity { get; set; }
        public bool IsForSave { get; set; }
        public int Procurement_PurchaseRequisitionFK { get; set; }
        public string VMRawItemName { get; set; }
        [Required(ErrorMessage = "Raw Item is Required")]
        public int VMRawItemFK { get; set; }
        public int VMRawCategoryFK { get; set; }
        public string VMRawCategoryName { get; set; }
        public int VMRawSubCategoryFK { get; set; }
        public string VMRawSubCategoryName { get; set; }

        public string DescriptionSlave { get; set; }

        public double SuggestedPrice { get; set; }
        public double RestQuantity
        {
            get
            {
                return StyleQuantity - (PRRaisedQuantity);
            }
        }
        public double RequisitionQuantity { get; set; }

        //[RegularExpression(@"^(0*[1-9][0-9]*(\.[0-9]+)?|0+\.[0-9]*[1-9][0-9]*)$", ErrorMessage = "Can't be zero")]
        //[Range(0.0, Double.MaxValue)]
        //[DisplayFormat(DataFormatString = "{0:n5}", ApplyFormatInEditMode = true)]
        //public double RequisitionQuantity
        //{
        //    get
        //    {
        //        if (_RequisitionQuantity <= 0)
        //        {
        //            if (StyleQuantity > (PRRaisedQuantity))
        //            { _RequisitionQuantity = StyleQuantity - (PRRaisedQuantity); }
        //        }
        //        return _RequisitionQuantity;
        //    }
        //    set { _RequisitionQuantity = value; }
        //}
        [Range(typeof(bool), "true", "true", ErrorMessage = "Requisition Quantity must be smaller than or equal to Rest Quantity")]
        //public bool IsValidQuantity { get { return VMMerchandising_StyleID > 0 && RestQuantity >= RequisitionQuantity; } }
        public bool IsValidQuantity { get { return VMMerchandising_StyleID > 0; } }
        public double AvailableQuantity { get; set; }

        public string VMUnitName { get; set; }
        [Required(ErrorMessage = "Required Date is Required")]
        public DateTime RequiredDate { get; set; } 
        public string RequiredDateString { get { return this.RequiredDate != DateTime.MinValue ? this.RequiredDate.ToString("yyyy-MM-dd") : ""; } }

        public string SupplierNames { get; set; }

        public bool InspectionRequired { get; set; }
        public string Inspection { get { return this.InspectionRequired ? "Required" : "Not Required"; } }

        //public int? UserId { get; set; }
        public string VMEmployeeName { get; set; }
        public int? VMMerchandising_StyleID { get; set; }
        public int? VMMerchandising_StyleSlaveID { get; set; }
        public int? VMMerchandising_BOFID { get; set; }
        public string BONoStyleName { get; set; }

        public string StyleColorName { get; set; }
        public double StyleGSM { get; set; }

        public string StyleColorGSM { get { return !string.IsNullOrEmpty(StyleColorName) && StyleGSM > 0 ? StyleColorName + "/" + StyleGSM.ToString() : !string.IsNullOrEmpty(StyleColorName) ? StyleGSM.ToString() : ""; } }

        public List<VMPurchaseRequisitionSlave> DataListSlave { get; set; }
        public SelectList RawItemList { get; set; } = new SelectList(new List<object>());
        public SelectList RawCategoryList { get; set; } = new SelectList(new List<object>());
        public SelectList RawSubCategoryList { get; set; } = new SelectList(new List<object>());
        public SelectList UserList { get; set; } = new SelectList(new List<object>());
        [Display(Name ="By Style")]
        public SelectList ApprovedBOMStyleList { get; set; } = new SelectList(new List<object>());
        [Display(Name = "By Style")]
        public SelectList ApprovedBOFStyleList { get; set; } = new SelectList(new List<object>());
    }
}