﻿using Erp.Core.Entity.Accounting;
using Erp.Core.Entity.MIS;
using System;
using System.Collections;
using System.Collections.Generic;

using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Erp.Core.Services.MIS
{
    public class MisService : BaseService
    {
        public MisService(IErpDbContext db) => _db = db;

        public async Task<VMMis> MisGet()
        {
            VMMis vmMis = new VMMis();
            vmMis = await Task.Run(() => MisGetDataLoad());
            return vmMis;
        }

        private VMMis MisGetDataLoad()
        {
            VMMis vm = new VMMis();
            vm.VMMis_BTBLCStatus = new VMMis_BTBLCStatus();
            vm.VMMis_DailyAttendance = new VMMis_DailyAttendance();
            vm.VMMis_DailyProduction = new VMMis_DailyProduction();
            vm.VMMis_OrderConfirm = new VMMis_OrderConfirm();
            vm.VMMis_ShipmentStatus = new VMMis_ShipmentStatus();
            vm.VMMis_FinancialStatus = new VMMis_FinancialStatus();


            vm.CashAtBank = _db.Mis_CashAtBank.Where(x => x.Active == true).Select(x => x.Value).DefaultIfEmpty(0).Sum();
            vm.CashInHand = _db.Mis_CashInHand.Where(x => x.Active == true).Select(x => x.Value).DefaultIfEmpty(0).Sum();
            // var cm = _db.Mis_CMEarned.Where(x => x.Active == true).FirstOrDefault();
            vm.CMEarned = _db.Mis_CMEarned.Where(x => x.Active == true).Select(x => x.Value).DefaultIfEmpty(0).Sum();
            vm.CMEarnedQty = _db.Mis_CMEarned.Where(x => x.Active == true).Select(x => x.Quantity).DefaultIfEmpty(0).Sum();
            vm.WIPInventory = _db.Mis_RawMaterials.Where(x => x.Active == true).Select(x => x.WIPValue).DefaultIfEmpty(0).Sum();
            vm.Payable = _db.Mis_AccountsPayable.Where(x => x.Active == true).Select(x => x.Value).DefaultIfEmpty(0).Sum();
            vm.RawInventory = _db.Mis_RawMaterials.Where(x => x.Active == true).Select(x => x.RawValue).DefaultIfEmpty(0).Sum();
            vm.Receivable = _db.Mis_AccountsReceivable.Where(x => x.Active == true).Select(x => x.Value).DefaultIfEmpty(0).Sum();

            vm.BTBLimit = _db.Mis_BTBLCStatus.Where(x => x.Active == true).Select(x => x.BTBLimit).DefaultIfEmpty(0).Sum();
            vm.BTBIssued = _db.Mis_BTBLCStatus.Where(x => x.Active == true).Select(x => x.BTBIssued).DefaultIfEmpty(0).Sum();
            vm.BTBLiabilites = _db.Mis_BTBLCStatus.Where(x => x.Active == true).Select(x => x.BTBLiabilites).DefaultIfEmpty(0).Sum();

            vm.VMMis_BTBLCStatus.DataList = BTBLCStatusDataLoad();
            vm.VMMis_DailyAttendance.DataList = MisDailyAttendanceDataLoad();
            vm.VMMis_DailyProduction.DataList = MisDailyProductionDataLoad();
            vm.VMMis_OrderConfirm.DataList = MisOrderConfirmDataLoad();
            vm.VMMis_ShipmentStatus.DataList = MisShipmentStatusDataLoad();
            vm.VMMis_FinancialStatus.DataList = MisFinancialStatusDataLoad();

            return vm;
        }

        #region CM Earned 
        public async Task<VMMis_CMEarned> CMEarnedGet()
        {
            VMMis_CMEarned vmMisCMEarned = new VMMis_CMEarned();
            var currentData = _db.Mis_CMEarned.Where(x => x.Time.Month == DateTime.Today.Month).FirstOrDefault();
            if (currentData == null)
            {
                await CMEarnedAdd(new VMMis_CMEarned { Month = DateTime.Today.Month.ToString().PadLeft(2, '0'), Year = DateTime.Today.Year.ToString(), Quantity = 0, Value = 0 });
            }

            vmMisCMEarned.DataList = await Task.Run(() => CMEarnedDataLoad());
            return vmMisCMEarned;
        }

        public IEnumerable<VMMis_CMEarned> CMEarnedDataLoad()
        {
            IEnumerable<VMMis_CMEarned> misCMEarneds = null;
            var now = DateTime.Now;
            var sevenOfMonth = new DateTime(now.Year, now.Month, 1).AddDays(7);
            if (now < sevenOfMonth)
            {
                misCMEarneds = (from t1 in _db.Mis_CMEarned
                                where t1.Active == true
                                select new VMMis_CMEarned
                                {
                                    ID = t1.ID,
                                    Month = t1.Month,
                                    Quantity = t1.Quantity,
                                    Value = t1.Value,
                                    Year = t1.Year,
                                    Time = t1.Time,
                                    UserID = t1.UserID
                                }).Where(x => x.Time.Year == DateTime.Today.AddDays(-10).Year && x.Time.Month == DateTime.Today.AddDays(-10).Month
                    ).OrderByDescending(x => x.ID).AsEnumerable();

            }
            var currentData = (from t1 in _db.Mis_CMEarned
                               where t1.Active == true
                               select new VMMis_CMEarned
                               {
                                   ID = t1.ID,
                                   Month = t1.Month,
                                   Quantity = t1.Quantity,
                                   Value = t1.Value,
                                   Year = t1.Year,
                                   Time = t1.Time,
                                   UserID = t1.UserID
                               }).Where(x => x.Time.Year == DateTime.Today.Year && x.Time.Month == DateTime.Today.Month
                     ).OrderByDescending(x => x.ID).AsEnumerable();
            if (misCMEarneds != null)
            {
                return misCMEarneds.Union(currentData);
            }
            else
            {
                return currentData;
            }

        }


        public async Task<int> CMEarnedAdd(VMMis_CMEarned vmMis_CMEarned)
        {
            var result = -1;
            Mis_CMEarned misCMEarned = new Mis_CMEarned
            {
                Month = vmMis_CMEarned.Month,
                Quantity = vmMis_CMEarned.Quantity,
                Value = vmMis_CMEarned.Value,
                Year = vmMis_CMEarned.Year,
                Time = Convert.ToDateTime(vmMis_CMEarned.Year + "-" + vmMis_CMEarned.Month + "-01"),
                User = "UserOne",
            };
            _db.Mis_CMEarned.Add(misCMEarned);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = misCMEarned.ID;
            }
            return result;
        }
        public async Task<int> CMEarnedEdit(VMMis_CMEarned vMMisCMEarned)
        {
            var result = -1;
            //to select Accountining_Chart_Two data.....
            Mis_CMEarned misCMEarned = _db.Mis_CMEarned.Find(vMMisCMEarned.ID);
            misCMEarned.Month = vMMisCMEarned.Month;
            misCMEarned.Year = vMMisCMEarned.Year;
            misCMEarned.Value = vMMisCMEarned.Value;
            misCMEarned.Quantity = vMMisCMEarned.Quantity;
            misCMEarned.Time = Convert.ToDateTime(vMMisCMEarned.Year + "-" + vMMisCMEarned.Month + "-01");

            if (await _db.SaveChangesAsync() > 0)
            {
                result = misCMEarned.ID;
            }
            return result;
        }
        public async Task<int> CMEarnedDelete(int id)
        {
            int result = -1;

            if (id != 0)
            {
                Mis_CMEarned misCMEarned = _db.Mis_CMEarned.Find(id);
                misCMEarned.Active = false;

                if (await _db.SaveChangesAsync() > 0)
                {
                    result = misCMEarned.ID;
                }

            }
            return result;
        }
        #endregion

        #region Cash In Hand
        public async Task<VMMis_CashInHand> CashInHandGet()
        {
            VMMis_CashInHand vmMisCashInHand = new VMMis_CashInHand();
            vmMisCashInHand.DataList = await Task.Run(() => CashInHandDataLoad());
            return vmMisCashInHand;
        }

        public IEnumerable<VMMis_CashInHand> CashInHandDataLoad()
        {
            var v = (from t1 in _db.Mis_CashInHand
                         //join t2 in _db.Accounting_Head on t1.Accounting_HeadFk equals t2.ID
                     where t1.Active == true
                     select new VMMis_CashInHand
                     {
                         ID = t1.ID,
                         //Accounting_HeadFk = t1.Accounting_HeadFk,
                         Accounting_HeadName = t1.Accounting_Head,
                         Date = t1.Date,
                         Value = t1.Value
                     }).OrderByDescending(x => x.ID).AsEnumerable();
            return v;
        }


        public async Task<int> CashInHandAdd(VMMis_CashInHand vmMisCashInHand)
        {
            var result = -1;
            Mis_CashInHand misCashInHand = new Mis_CashInHand
            {
                Accounting_Head = vmMisCashInHand.Accounting_HeadName,
                Date = vmMisCashInHand.Date,
                Value = vmMisCashInHand.Value,
                User = "UserOne",
            };
            _db.Mis_CashInHand.Add(misCashInHand);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = misCashInHand.ID;
            }
            return result;
        }
        public async Task<int> CashInHandEdit(VMMis_CashInHand vmMisCashInHand)
        {
            var result = -1;
            //to select Accountining_Chart_Two data.....
            Mis_CashInHand misCashInHand = _db.Mis_CashInHand.Find(vmMisCashInHand.ID);
            misCashInHand.Accounting_Head = vmMisCashInHand.Accounting_HeadName;
            misCashInHand.Date = vmMisCashInHand.Date;
            misCashInHand.Value = vmMisCashInHand.Value;

            if (await _db.SaveChangesAsync() > 0)
            {
                result = misCashInHand.ID;
            }
            return result;
        }
        public async Task<int> CashInHandDelete(int id)
        {
            var result = -1;

            if (id != 0)
            {
                Mis_CashInHand misCashInHand = _db.Mis_CashInHand.Find(id);
                misCashInHand.Active = false;
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = misCashInHand.ID;
                }
            }
            return result;
        }
        #endregion

        #region Cash At Bank
        public async Task<VMMis_CashAtBank> CashAtBankGet()
        {
            VMMis_CashAtBank vmMisCashAtBank = new VMMis_CashAtBank();
            vmMisCashAtBank.DataList = await Task.Run(() => CashAtBankDataLoad());
            return vmMisCashAtBank;
        }

        public IEnumerable<VMMis_CashAtBank> CashAtBankDataLoad()
        {
            var v = (from t1 in _db.Mis_CashAtBank
                         //join t2 in _db.Accounting_Head on t1.Accounting_HeadFk equals t2.ID
                     where t1.Active == true
                     select new VMMis_CashAtBank
                     {
                         ID = t1.ID,
                         Date = t1.Date,
                         Accounting_HeadName = t1.Accounting_Head,
                         // Accounting_HeadName = t2.Name,
                         Value = t1.Value

                     }).OrderByDescending(x => x.ID).AsEnumerable();
            return v;
        }

        public async Task<int> CashAtBankAdd(VMMis_CashAtBank vmMisCashAtBank)
        {
            var result = -1;
            Mis_CashAtBank misCashAtBank = new Mis_CashAtBank
            {
                Accounting_Head = vmMisCashAtBank.Accounting_HeadName,
                Date = vmMisCashAtBank.Date,
                Value = vmMisCashAtBank.Value,
                User = "UserOne",
            };
            _db.Mis_CashAtBank.Add(misCashAtBank);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = misCashAtBank.ID;
            }
            return result;
        }
        public async Task<int> CashAtBankEdit(VMMis_CashAtBank vMMisCashAtBank)
        {
            var result = -1;
            //to select Accountining_Chart_Two data.....
            Mis_CashAtBank misCashAtBank = _db.Mis_CashAtBank.Find(vMMisCashAtBank.ID);
            misCashAtBank.Accounting_Head = vMMisCashAtBank.Accounting_HeadName;
            misCashAtBank.Date = vMMisCashAtBank.Date;
            misCashAtBank.Value = vMMisCashAtBank.Value;

            if (await _db.SaveChangesAsync() > 0)
            {
                result = misCashAtBank.ID;
            }
            return result;
        }
        public async Task<int> CashAtBankDelete(int id)
        {
            var result = -1;

            if (id != 0)
            {
                Mis_CashAtBank misCashAtBank = _db.Mis_CashAtBank.Find(id);
                misCashAtBank.Active = false;

                if (await _db.SaveChangesAsync() > 0)
                {
                    result = misCashAtBank.ID;
                }
                return result;
            }
            return result;
        }
        #endregion

        #region Income Statement
        public async Task<VMMis_IncomeStatement> IncomeStatementGet()
        {
            VMMis_IncomeStatement vmMisIncomeStatement = new VMMis_IncomeStatement();
            vmMisIncomeStatement.DataList = await Task.Run(() => IncomeStatementDataLoad());
            return vmMisIncomeStatement;
        }

        public IEnumerable<VMMis_IncomeStatement> IncomeStatementDataLoad()
        {
            var v = (from t1 in _db.Mis_IncomeStatement
                         //join t2 in _db.Accounting_Head on t1.Accounting_HeadFK equals t2.ID
                     where t1.Active == true
                     select new VMMis_IncomeStatement
                     {
                         ChartTableId = t1.ChartTableId,
                         BaseHeadId = t1.BaseHeadId,
                         ID = t1.ID,
                         Accounting_HeadName = t1.Accounting_Head,

                         Value = t1.Value,
                         FromDate = t1.FromDate,
                         ToDate = t1.ToDate
                     }).OrderByDescending(x => x.ID).ToList();
            return v;
        }


        public async Task<int> IncomeStatementAdd(VMMis_IncomeStatement vmMis_IncomeStatement)
        {
            var result = -1;
            Mis_IncomeStatement misIncomeStatement = new Mis_IncomeStatement
            {
                FromDate = vmMis_IncomeStatement.FromDate,
                ToDate = vmMis_IncomeStatement.ToDate,

                Accounting_Head = vmMis_IncomeStatement.Accounting_HeadName,
                Value = vmMis_IncomeStatement.Value,

                User = "UserOne",
            };
            _db.Mis_IncomeStatement.Add(misIncomeStatement);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = misIncomeStatement.ID;
            }
            return result;
        }
        public async Task<int> IncomeStatementEdit(VMMis_IncomeStatement vMMisIncomeStatement)
        {
            var result = -1;
            //to select Accountining_Chart_Two data.....
            Mis_IncomeStatement misIncomeStatement = _db.Mis_IncomeStatement.Find(vMMisIncomeStatement.ID);
            misIncomeStatement.Accounting_Head = vMMisIncomeStatement.Accounting_HeadName;
            misIncomeStatement.FromDate = vMMisIncomeStatement.FromDate;
            misIncomeStatement.ToDate = vMMisIncomeStatement.ToDate;
            misIncomeStatement.Value = vMMisIncomeStatement.Value;
            if (await _db.SaveChangesAsync() > 0)
            {
                result = misIncomeStatement.ID;
            }
            return result;
        }
        public async Task<int> IncomeStatementDelete(int id)
        {
            var result = -1;

            if (id != 0)
            {
                Mis_IncomeStatement misIncomeStatement = _db.Mis_IncomeStatement.Find(id);
                misIncomeStatement.Active = false;
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = misIncomeStatement.ID;
                }
                return result;

            }
            return result;
        }
        #endregion

        #region Balance Sheet
        public async Task<VMMis_BalanceSheet> BalanceSheetGet()
        {
            VMMis_BalanceSheet vmMisBalanceSheet = new VMMis_BalanceSheet();
            vmMisBalanceSheet.DataList = await Task.Run(() => BalanceSheetDataLoad());
            return vmMisBalanceSheet;
        }



        public IEnumerable<VMMis_BalanceSheet> BalanceSheetDataLoad()
        {
            var v = (from t1 in _db.Mis_BalanceSheet
                         // join t2 in _db.Accounting_Head on t1.Accounting_HeadFK equals t2.ID
                     where t1.Active == true
                     select new VMMis_BalanceSheet
                     {
                         ID = t1.ID,
                         ChartTableId = t1.ChartTableId,
                         BaseHeadId = t1.BaseHeadId,
                         Accounting_HeadName = t1.Accounting_Head,
                         Value = t1.Value,
                          ToDate = t1.ToDate,
                         FromDate = t1.FromDate

                     }).OrderByDescending(x => x.ID).AsEnumerable();
            return v;
        }


        public async Task<int> MisBalanceSheetAdd(VMMis_BalanceSheet vmMis_BalanceSheet)
        {
            var result = -1;
            Mis_BalanceSheet misBalanceSheet = new Mis_BalanceSheet
            {
                Accounting_Head = vmMis_BalanceSheet.Accounting_HeadName,
                Value = vmMis_BalanceSheet.Value,
                FromDate = vmMis_BalanceSheet.FromDate,
                ToDate = vmMis_BalanceSheet.ToDate,

                User = "UserOne",
            };
            _db.Mis_BalanceSheet.Add(misBalanceSheet);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = misBalanceSheet.ID;
            }
            return result;
        }
        public async Task<int> BalanceSheetEdit(VMMis_BalanceSheet vMMisBalanceSheet)
        {
            var result = -1;
            //to select Accountining_Chart_Two data.....
            Mis_BalanceSheet misBalanceSheet = _db.Mis_BalanceSheet.Find(vMMisBalanceSheet.ID);
            misBalanceSheet.Accounting_Head = vMMisBalanceSheet.Accounting_HeadName;
            misBalanceSheet.FromDate = vMMisBalanceSheet.FromDate;
            misBalanceSheet.ToDate = vMMisBalanceSheet.ToDate;
            misBalanceSheet.Value = vMMisBalanceSheet.Value;

            if (await _db.SaveChangesAsync() > 0)
            {
                result = misBalanceSheet.ID;
            }
            return result;
        }
        public async Task<int> BalanceSheetDelete(int id)
        {
            var result = -1;

            if (id != 0)
            {
                Mis_BalanceSheet misBalanceSheet = _db.Mis_BalanceSheet.Find(id);
                misBalanceSheet.Active = false;

                if (await _db.SaveChangesAsync() > 0)
                {
                    result = misBalanceSheet.ID;
                }
            }
            return result;
        }
        #endregion

        #region Accounts Payable
        public async Task<VMMis_AccountsPayable> AccountsPayableGet()
        {
            VMMis_AccountsPayable vmMisAccountsPayable = new VMMis_AccountsPayable();
            vmMisAccountsPayable.DataList = await Task.Run(() => AccountsPayableDataLoad());
            return vmMisAccountsPayable;
        }
        public async Task<VMMis_AccountsPayable> AccountsPayableInputGet()
        {
            VMMis_AccountsPayable vmMisAccountsPayable = new VMMis_AccountsPayable();
            vmMisAccountsPayable.DataList = await Task.Run(() => AccountsPayableInputDataLoad());
            return vmMisAccountsPayable;
        }
        public IEnumerable<VMMis_AccountsPayable> AccountsPayableInputDataLoad()
        {
            var v = (from t1 in _db.Mis_AccountsPayable
                     join t2 in _db.Common_Supplier on t1.Common_SupplierFk equals t2.ID
                     where t1.Active == true
                     //group new { t1, t2 } by new { t2.Name }
                     // into Group
                     select new VMMis_AccountsPayable
                     {
                         ID = t1.ID,
                         Common_SupplierFk = t2.ID,
                         Common_SupplierName = t2.Name,
                         Value = t1.Value,
                         Date = t1.Date
                     }).OrderByDescending(x => x.Date).AsEnumerable();
            return v;
        }
        public IEnumerable<VMMis_AccountsPayable> AccountsPayableDataLoad()
        {
            var v = (from t1 in _db.Mis_AccountsPayable
                     join t2 in _db.Common_Supplier on t1.Common_SupplierFk equals t2.ID
                     where t1.Active == true
                     group new { t1, t2 } by new { t2.Name }
                      into Group
                     select new VMMis_AccountsPayable
                     {
                         ID = Group.First().t1.ID,
                         Common_SupplierFk = Group.First().t2.ID,
                         Common_SupplierName = Group.First().t2.Name,
                         Value = Group.Sum(x => x.t1.Value),
                         Date = Group.Max(x => x.t1.Date)
                     }).OrderByDescending(x => x.Date).AsEnumerable();
            return v;
        }


        public async Task<int> MisAccountsPayableAdd(VMMis_AccountsPayable vmMis_AccountsPayable)
        {
            var result = -1;
            Mis_AccountsPayable misAccountsPayable = new Mis_AccountsPayable
            {
                Date = vmMis_AccountsPayable.Date,
                Common_SupplierFk = vmMis_AccountsPayable.Common_SupplierFk,
                Value = vmMis_AccountsPayable.Value,
                User = "UserOne",
            };
            _db.Mis_AccountsPayable.Add(misAccountsPayable);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = misAccountsPayable.ID;
            }
            return result;
        }
        public async Task<int> AccountsPayableEdit(VMMis_AccountsPayable vMMisAccountsPayable)
        {
            var result = -1;
            //to select Accountining_Chart_Two data.....
            Mis_AccountsPayable misAccountsPayable = _db.Mis_AccountsPayable.Find(vMMisAccountsPayable.ID);
            misAccountsPayable.Common_SupplierFk = vMMisAccountsPayable.Common_SupplierFk;
            misAccountsPayable.Date = vMMisAccountsPayable.Date;
            misAccountsPayable.Value = vMMisAccountsPayable.Value;
            if (await _db.SaveChangesAsync() > 0)
            {
                result = misAccountsPayable.ID;
            }
            return result;
        }
        public async Task<int> AccountsPayableDelete(int id)
        {
            var result = -1;

            if (id != 0)
            {
                Mis_AccountsPayable misAccountsPayable = _db.Mis_AccountsPayable.Find(id);
                misAccountsPayable.Active = false;
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = misAccountsPayable.ID;
                }
            }
            return result;
        }
        #endregion

        #region Accounts Receivable
        public async Task<VMMis_AccountsReceivable> AccountsReceivableInputGet()
        {
            VMMis_AccountsReceivable vmMisAccountsReceivable = new VMMis_AccountsReceivable();
            vmMisAccountsReceivable.DataList = await Task.Run(() => AccountsReceivableInputDataLoad());
            return vmMisAccountsReceivable;
        }
        public IEnumerable<VMMis_AccountsReceivable> AccountsReceivableInputDataLoad()
        {


            var v = (from t1 in _db.Mis_AccountsReceivable
                     join t2 in _db.Common_Buyer on t1.Common_BuyerFk equals t2.ID
                     where t1.Active == true
                    // group new { t1, t2 } by new { t2.Name }
                    //into Group
                     select new VMMis_AccountsReceivable
                     {
                         ID = t1.ID,
                         Common_BuyerFk = t2.ID,
                         Common_BuyerName = t2.Name,
                         Value = t1.Value,
                         Date = t1.Date
                     }).OrderByDescending(x => x.Date).AsEnumerable();
            return v;
        }
        public async Task<VMMis_AccountsReceivable> AccountsReceivableGet()
        {
            VMMis_AccountsReceivable vmMisAccountsReceivable = new VMMis_AccountsReceivable();
            vmMisAccountsReceivable.DataList = await Task.Run(() => AccountsReceivableDataLoad());
            return vmMisAccountsReceivable;
        }

        public IEnumerable<VMMis_AccountsReceivable> AccountsReceivableDataLoad()
        {


            var v = (from t1 in _db.Mis_AccountsReceivable
                     join t2 in _db.Common_Buyer on t1.Common_BuyerFk equals t2.ID
                     where t1.Active == true
                     group new { t1, t2 } by new { t2.Name }
                    into Group
                     select new VMMis_AccountsReceivable
                     {
                         ID = Group.First().t1.ID,
                         Common_BuyerFk = Group.First().t2.ID,
                         Common_BuyerName = Group.First().t2.Name,
                         Value = Group.Sum(x => x.t1.Value),
                         Date = Group.Max(x => x.t1.Date)
                     }).OrderByDescending(x => x.Date).AsEnumerable();
            return v;
        }


        public async Task<int> MisAccountsReceivableAdd(VMMis_AccountsReceivable vmMis_AccountsReceivable)
        {
            var result = -1;
            Mis_AccountsReceivable misAccountsReceivable = new Mis_AccountsReceivable
            {
                Common_BuyerFk = vmMis_AccountsReceivable.Common_BuyerFk,
                Date = vmMis_AccountsReceivable.Date,
                Value = vmMis_AccountsReceivable.Value,
                User = "UserOne",
            };
            _db.Mis_AccountsReceivable.Add(misAccountsReceivable);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = misAccountsReceivable.ID;
            }
            return result;
        }
        public async Task<int> AccountsReceivableEdit(VMMis_AccountsReceivable vMMisAccountsReceivable)
        {
            var result = -1;
            //to select Accountining_Chart_Two data.....
            Mis_AccountsReceivable misAccountsReceivable = _db.Mis_AccountsReceivable.Find(vMMisAccountsReceivable.ID);
            misAccountsReceivable.Common_BuyerFk = vMMisAccountsReceivable.Common_BuyerFk;
            misAccountsReceivable.Date = vMMisAccountsReceivable.Date;
            misAccountsReceivable.Value = vMMisAccountsReceivable.Value;
            if (await _db.SaveChangesAsync() > 0)
            {
                result = misAccountsReceivable.ID;
            }
            return result;
        }
        public async Task<int> AccountsReceivableDelete(int id)
        {
            var result = -1;

            if (id != 0)
            {
                Mis_AccountsReceivable misAccountsReceivable = _db.Mis_AccountsReceivable.Find(id);
                misAccountsReceivable.Active = false;
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = misAccountsReceivable.ID;
                }
            }
            return result;
        }
        #endregion

        #region Order Confirm
        public async Task<VMMis_OrderConfirm> OrderConfirmGet()
        {
            VMMis_OrderConfirm vmMisOrderConfirm = new VMMis_OrderConfirm();
            vmMisOrderConfirm.DataList = await Task.Run(() => OrderConfirmDataLoad());
            return vmMisOrderConfirm;
        }

        public IEnumerable<VMMis_OrderConfirm> OrderConfirmDataLoad()
        {
            var v = (from t1 in _db.Mis_OrderConfirm
                         //join t2 in _db.Common_Order on t1.Common_OrderFk equals t2.ID
                     join t3 in _db.Common_Buyer on t1.Common_BuyerFk equals t3.ID
                     where t1.Active == true
                     select new VMMis_OrderConfirm
                     {
                         ID = t1.ID,
                         Common_OrderNo = t1.OrderNo,
                         Common_BuyerName = t3.Name,
                         OrderValue = t1.OrderValue,
                         FinalDeliveryDate = t1.FinalDeliveryDate,
                         CmValue = t1.CmValue,
                         Common_BuyerFk = t1.Common_BuyerFk,

                         OrderQty = t1.OrderQty
                     }).OrderBy(x => x.Common_BuyerName).AsEnumerable();
            return v;
        }

        public async Task<VMMis_OrderConfirm> OrderConfirmByBuyerGet(int id)
        {
            VMMis_OrderConfirm vmMisOrderConfirm = new VMMis_OrderConfirm();
            vmMisOrderConfirm = OrderConfirmGetById(id);

            vmMisOrderConfirm.DataList = await Task.Run(() => OrderConfirmByBuyerDataLoad(id));
            return vmMisOrderConfirm;
        }

        private VMMis_OrderConfirm OrderConfirmGetById(int id)
        {
            var v = (from t1 in _db.Mis_OrderConfirm
                         //join t2 in _db.Common_Order on t1.Common_OrderFk equals t2.ID
                     join t3 in _db.Common_Buyer on t1.Common_BuyerFk equals t3.ID
                     where t1.Active == true && t1.Common_BuyerFk == id
                     select new VMMis_OrderConfirm
                     {
                         ID = t1.ID,
                         Common_OrderNo = t1.OrderNo,
                         Common_BuyerName = t3.Name,
                         OrderValue = t1.OrderValue,
                         FinalDeliveryDate = t1.FinalDeliveryDate,
                         CmValue = t1.CmValue,
                         Common_BuyerFk = t1.Common_BuyerFk,
                         OrderQty = t1.OrderQty,

                     }).FirstOrDefault();
            return v;
        }

        public IEnumerable<VMMis_OrderConfirm> OrderConfirmByBuyerDataLoad(int id)
        {
            var v = (from t1 in _db.Mis_OrderConfirm
                         //join t2 in _db.Common_Order on t1.Common_OrderFk equals t2.ID
                     join t3 in _db.Common_Buyer on t1.Common_BuyerFk equals t3.ID
                     where t1.Active == true && t1.Common_BuyerFk == id
                     select new VMMis_OrderConfirm
                     {
                         ID = t1.ID,
                         Common_OrderNo = t1.OrderNo,
                         Common_BuyerName = t3.Name,
                         OrderValue = t1.OrderValue,
                         FinalDeliveryDate = t1.FinalDeliveryDate,
                         CmValue = t1.CmValue,
                         Common_BuyerFk = t1.Common_BuyerFk,
                         OrderQty = t1.OrderQty,

                     }).OrderBy(x => x.Common_BuyerName).AsEnumerable();
            return v;
        }
        public async Task<VMMis_OrderConfirm> MisOrderConfirmOrder()
        {
            VMMis_OrderConfirm vmMisOrderConfirm = new VMMis_OrderConfirm();
            vmMisOrderConfirm.DataList = await Task.Run(() => MisOrderConfirmDataLoad());
            return vmMisOrderConfirm;
        }
        public async Task<VMMis_OrderConfirm> MisOrderConfirmOrderAll()
        {
            VMMis_OrderConfirm vmMisOrderConfirm = new VMMis_OrderConfirm();
            vmMisOrderConfirm.DataList = await Task.Run(() => MisOrderConfirmDataLoadAll());
            return vmMisOrderConfirm;
        }
        private IEnumerable<VMMis_OrderConfirm> MisOrderConfirmDataLoadAll()
        {
            var v = (from t1 in _db.Mis_OrderConfirm
                         //join t2 in _db.Common_Order on t1.Common_OrderFk equals t2.ID
                     join t3 in _db.Common_Buyer on t1.Common_BuyerFk equals t3.ID
                     where t1.Active == true
                     group new { t1, t3 } by new { t3.Name }
                     into Group
                     where Group.Count() > 0
                     select new VMMis_OrderConfirm
                     {
                         Common_BuyerName = Group.Key.Name,
                         ID = Group.First().t1.ID,
                         Common_OrderNo = Group.First().t1.OrderNo,
                         Common_BuyerFk = Group.First().t3.ID,
                         OrderValue = Group.Sum(x => x.t1.OrderValue),
                         FinalDeliveryDate = Group.First().t1.FinalDeliveryDate,
                         CmValue = Group.Sum(x => x.t1.CmValue),
                         // Common_OrderFk = Group.First().t2.ID,
                         OrderQty = Group.Sum(x => x.t1.OrderQty)
                     }).OrderBy(x => x.Common_BuyerName).AsEnumerable();
            return v;
        }
        private IEnumerable<VMMis_OrderConfirm> MisOrderConfirmDataLoad()
        {
            var v = (from t1 in _db.Mis_OrderConfirm
                         //join t2 in _db.Common_Order on t1.Common_OrderFk equals t2.ID
                     join t3 in _db.Common_Buyer on t1.Common_BuyerFk equals t3.ID
                     where t1.Active == true
                     group new { t1, t3 } by new { t3.Name }
                     into Group
                     where Group.Count() > 0
                     select new VMMis_OrderConfirm
                     {
                         Common_BuyerName = Group.Key.Name,
                         ID = Group.First().t1.ID,
                         Common_OrderNo = Group.First().t1.OrderNo,
                         Common_BuyerFk = Group.First().t3.ID,
                         OrderValue = Group.Sum(x => x.t1.OrderValue),
                         FinalDeliveryDate = Group.First().t1.FinalDeliveryDate,
                         CmValue = Group.Sum(x => x.t1.CmValue),
                         // Common_OrderFk = Group.First().t2.ID,
                         OrderQty = Group.Sum(x => x.t1.OrderQty)
                     }).OrderBy(x => x.Common_BuyerName).Take(5).AsEnumerable();
            return v;
        }

        public async Task<int> MisOrderConfirmAdd(VMMis_OrderConfirm vmMis_OrderConfirm)
        {
            var result = -1;
            Mis_OrderConfirm misOrderConfirm = new Mis_OrderConfirm
            {
                CmValue = vmMis_OrderConfirm.CmValue,
                Common_BuyerFk = vmMis_OrderConfirm.Common_BuyerFk,
                OrderNo = vmMis_OrderConfirm.Common_OrderNo,
                FinalDeliveryDate = vmMis_OrderConfirm.FinalDeliveryDate,
                OrderValue = vmMis_OrderConfirm.OrderValue,
                OrderQty = vmMis_OrderConfirm.OrderQty,
                User = "UserOne",
            };
            _db.Mis_OrderConfirm.Add(misOrderConfirm);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = misOrderConfirm.ID;
            }
            return result;
        }
        public async Task<int> OrderConfirmEdit(VMMis_OrderConfirm vMMisOrderConfirm)
        {
            var result = -1;
            //to select Accountining_Chart_Two data.....
            Mis_OrderConfirm misOrderConfirm = _db.Mis_OrderConfirm.Find(vMMisOrderConfirm.ID);
            misOrderConfirm.OrderNo = vMMisOrderConfirm.Common_OrderNo;
            misOrderConfirm.CmValue = vMMisOrderConfirm.CmValue;
            misOrderConfirm.OrderValue = vMMisOrderConfirm.OrderValue;
            misOrderConfirm.FinalDeliveryDate = vMMisOrderConfirm.FinalDeliveryDate;
            misOrderConfirm.Common_BuyerFk = vMMisOrderConfirm.Common_BuyerFk;
            misOrderConfirm.OrderQty = vMMisOrderConfirm.OrderQty;
            if (await _db.SaveChangesAsync() > 0)
            {
                result = misOrderConfirm.ID;
            }
            return result;
        }
        public async Task<int> OrderConfirmDelete(int id)
        {
            var result = -1;

            if (id != 0)
            {
                Mis_OrderConfirm misOrderConfirm = _db.Mis_OrderConfirm.Find(id);
                misOrderConfirm.Active = false;

                if (await _db.SaveChangesAsync() > 0)
                {
                    result = misOrderConfirm.ID;
                }
            }
            return result;
        }
        #endregion

        #region BTB L/C Status
        public async Task<VMMis_BTBLCStatus> BTBLCStatusGet()
        {
            VMMis_BTBLCStatus vmMisBTBLCStatus = new VMMis_BTBLCStatus();
            vmMisBTBLCStatus.DataList = await Task.Run(() => BTBLCStatusDataLoad());
            return vmMisBTBLCStatus;
        }

        public IEnumerable<VMMis_BTBLCStatus> BTBLCStatusDataLoad()
        {
            var v = (from t1 in _db.Mis_BTBLCStatus
                     where t1.Active == true
                     select new VMMis_BTBLCStatus
                     {
                         ID = t1.ID,
                         BTBAcceptedValue = t1.BTBAcceptedValue,
                         BTBIssued = t1.BTBIssued,
                         BTBLiabilites = t1.BTBLiabilites,
                         BTBLimit = t1.BTBLimit,
                         UdNo = t1.UDNo,
                         MasterLCTotalValue = t1.MasterLCTotalValue,
                         UdID = t1.UdID,

                     }).OrderByDescending(x => x.ID).AsEnumerable();
            return v;
        }

        public async Task<int> MisBTBLCStatusAdd(VMMis_BTBLCStatus vmMis_BTBLCStatus)
        {
            var result = -1;
            Mis_BTBLCStatus misBTBLCStatus = new Mis_BTBLCStatus
            {
                MasterLCTotalValue = vmMis_BTBLCStatus.MasterLCTotalValue,
                UDNo = vmMis_BTBLCStatus.UdNo,
                UdID = vmMis_BTBLCStatus.UdID,
                BTBLimit = vmMis_BTBLCStatus.BTBLimit,
                BTBAcceptedValue = vmMis_BTBLCStatus.BTBAcceptedValue,
                BTBIssued = vmMis_BTBLCStatus.BTBIssued,
                BTBLiabilites = vmMis_BTBLCStatus.BTBIssued - vmMis_BTBLCStatus.BTBAcceptedValue,


                User = "UserOne",
            };
            _db.Mis_BTBLCStatus.Add(misBTBLCStatus);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = misBTBLCStatus.ID;
            }
            return result;
        }
        public async Task<int> BTBLCStatusEdit(VMMis_BTBLCStatus vMMisBTBLCStatus)
        {
            var result = -1;
            //to select Accountining_Chart_Two data.....
            Mis_BTBLCStatus misBTBLCStatus = _db.Mis_BTBLCStatus.Find(vMMisBTBLCStatus.ID);
            misBTBLCStatus.UDNo = vMMisBTBLCStatus.UdNo;
            misBTBLCStatus.UdID = vMMisBTBLCStatus.UdID;
            misBTBLCStatus.MasterLCTotalValue = vMMisBTBLCStatus.MasterLCTotalValue;

            misBTBLCStatus.BTBLimit = vMMisBTBLCStatus.BTBLimit;
            misBTBLCStatus.BTBIssued = vMMisBTBLCStatus.BTBIssued;
            misBTBLCStatus.BTBAcceptedValue = vMMisBTBLCStatus.BTBAcceptedValue;
            misBTBLCStatus.BTBLiabilites = vMMisBTBLCStatus.BTBIssued - vMMisBTBLCStatus.BTBAcceptedValue;

            if (await _db.SaveChangesAsync() > 0)
            {
                result = misBTBLCStatus.ID;
            }
            return result;
        }
        public async Task<int> BTBLCStatusDelete(int id)
        {
            var result = -1;

            if (id != 0)
            {
                Mis_BTBLCStatus misBTBLCStatus = _db.Mis_BTBLCStatus.Find(id);
                misBTBLCStatus.Active = false;

                if (await _db.SaveChangesAsync() > 0)
                {
                    result = misBTBLCStatus.ID;
                }
            }
            return result;
        }
        #endregion

        #region Daily Attendance
        public async Task<VMMis_DailyAttendance> DailyAttendanceGet()
        {
            VMMis_DailyAttendance vmMisDailyAttendance = new VMMis_DailyAttendance();
            vmMisDailyAttendance.DataList = await Task.Run(() => DailyAttendanceDataLoad());
            return vmMisDailyAttendance;
        }

        public IEnumerable<VMMis_DailyAttendance> DailyAttendanceDataLoad()
        {
            var v = (from t1 in _db.Mis_DailyAttendance
                     join t2 in _db.User_Department on t1.User_DepartmentFk equals t2.ID
                     where t1.Active == true
                     select new VMMis_DailyAttendance
                     {
                         ID = t1.ID,
                         Absent = t1.Absent,
                         Date = t1.AttendanceDate,
                         Off = t1.Off,
                         OnLeave = t1.OnLeave,
                         Present = t1.Present,
                         TotalEmployee = t1.TotalEmployee,
                         WorkingHour = t1.WorkingHour * t1.TotalEmployee,
                         User_DepartmentFk = t1.User_DepartmentFk,
                         Common_DepartmenName = t2.Name
                     }).OrderByDescending(x => x.ID).AsEnumerable();
            return v;
        }

        public async Task<VMMis_DailyAttendance> DepartmentWiseAttendanceGet(string dateTime)
        {
            VMMis_DailyAttendance vmMisDailyAttendance = new VMMis_DailyAttendance();
            vmMisDailyAttendance.DataList = await Task.Run(() => DepartmentWiseAttendanceDataLoad(dateTime));
            return vmMisDailyAttendance;
        }

        public IEnumerable<VMMis_DailyAttendance> DepartmentWiseAttendanceDataLoad(string dateTime)
        {
            //string[] dates = dateTime.Split("-");
            //string date = dates[0] + "/" + dates[1] + "/" + dates[2];
            var v = (from t1 in _db.Mis_DailyAttendance
                     join t2 in _db.User_Department on t1.User_DepartmentFk equals t2.ID
                     where t1.Active == true && t1.AttendanceDate.ToString("yyyy/MM/d") == dateTime
                     select new VMMis_DailyAttendance
                     {
                         ID = t1.ID,
                         Absent = t1.Absent,
                         Date = t1.AttendanceDate,
                         DateString = t1.AttendanceDate.ToShortDateString(),
                         Off = t1.Off,
                         OnLeave = t1.OnLeave,
                         Present = t1.Present,
                         TotalEmployee = t1.TotalEmployee,
                         WorkingHour = t1.WorkingHour * t1.TotalEmployee,
                         User_DepartmentFk = t1.User_DepartmentFk,
                         Common_DepartmenName = t2.Name
                     }).OrderByDescending(x => x.ID).AsEnumerable();
            return v;
        }

        public async Task<VMMis_DailyAttendance> MisDailyAttendanceGet()
        {
            VMMis_DailyAttendance vmMisDailyAttendance = new VMMis_DailyAttendance();
            vmMisDailyAttendance.DataList = await Task.Run(() => MisDailyAttendanceDataLoad());
            return vmMisDailyAttendance;
        }
        private IEnumerable<VMMis_DailyAttendance> MisDailyAttendanceDataLoad()
        {
            var v = (from t1 in _db.Mis_DailyAttendance
                     join t2 in _db.User_Department on t1.User_DepartmentFk equals t2.ID
                     where t1.Active == true
                     group new
                     {
                         t1,
                         t2
                     } by new { t1.AttendanceDate.Date }
                      into Group
                     where Group.Count() > 0

                     select new VMMis_DailyAttendance
                     {
                         Date = Group.Key.Date,
                         ID = Group.First().t1.ID,
                         Absent = Group.Sum(x => x.t1.Absent),
                         Off = Group.Sum(x => x.t1.Off),
                         OnLeave = Group.Sum(x => x.t1.OnLeave),
                         Present = Group.Sum(x => x.t1.Present),
                         TotalEmployee = Group.Sum(x => x.t1.TotalEmployee),
                         WorkingHour = Group.Sum(x => x.t1.WorkingHour * x.t1.TotalEmployee),
                         User_DepartmentFk = Group.First().t1.User_DepartmentFk,
                         Common_DepartmenName = Group.First().t2.Name
                     }).OrderByDescending(x => x.ID).Take(5).AsEnumerable();
            return v;
        }


        public async Task<int> MisDailyAttendanceAdd(VMMis_DailyAttendance vmMis_DailyAttendance)
        {
            var result = -1;
            Mis_DailyAttendance misDailyAttendance = new Mis_DailyAttendance
            {
                Absent = vmMis_DailyAttendance.Absent,
                AttendanceDate = vmMis_DailyAttendance.Date,
                Off = vmMis_DailyAttendance.Off,
                OnLeave = vmMis_DailyAttendance.OnLeave,
                Present = vmMis_DailyAttendance.Present,
                TotalEmployee = vmMis_DailyAttendance.Present + vmMis_DailyAttendance.OnLeave + vmMis_DailyAttendance.Off,
                WorkingHour = vmMis_DailyAttendance.WorkingHour,
                User_DepartmentFk = vmMis_DailyAttendance.User_DepartmentFk,

                User = "UserOne",
            };
            _db.Mis_DailyAttendance.Add(misDailyAttendance);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = misDailyAttendance.ID;
            }
            return result;
        }
        public async Task<int> DailyAttendanceEdit(VMMis_DailyAttendance vMMisDailyAttendance)
        {
            var result = -1;
            //to select Accountining_Chart_Two data.....
            Mis_DailyAttendance misDailyAttendance = _db.Mis_DailyAttendance.Find(vMMisDailyAttendance.ID);
            misDailyAttendance.Absent = vMMisDailyAttendance.Absent;
            misDailyAttendance.AttendanceDate = vMMisDailyAttendance.Date;
            misDailyAttendance.Off = vMMisDailyAttendance.Off;
            misDailyAttendance.OnLeave = vMMisDailyAttendance.OnLeave;
            misDailyAttendance.Present = vMMisDailyAttendance.Present;
            misDailyAttendance.TotalEmployee = vMMisDailyAttendance.Present + vMMisDailyAttendance.OnLeave + vMMisDailyAttendance.Off;
            misDailyAttendance.WorkingHour = vMMisDailyAttendance.WorkingHour;
            misDailyAttendance.User_DepartmentFk = vMMisDailyAttendance.User_DepartmentFk;
            if (await _db.SaveChangesAsync() > 0)
            {
                result = misDailyAttendance.ID;
            }
            return result;
        }
        public async Task<int> DailyAttendanceDelete(int id)
        {
            var result = -1;

            if (id != 0)
            {
                Mis_DailyAttendance misDailyAttendance = _db.Mis_DailyAttendance.Find(id);
                misDailyAttendance.Active = false;

                if (await _db.SaveChangesAsync() > 0)
                {
                    result = misDailyAttendance.ID;
                }
            }
            return result;
        }
        #endregion

        #region Raw Materials
        public async Task<VMMis_RawMaterials> RawMaterialsGet()
        {
            VMMis_RawMaterials vmMisRawMaterials = new VMMis_RawMaterials();
            vmMisRawMaterials.DataList = await Task.Run(() => RawMaterialsDataLoad());
            return vmMisRawMaterials;
        }

        public IEnumerable<VMMis_RawMaterials> RawMaterialsDataLoad()
        {
            var v = (from t1 in _db.Mis_RawMaterials
                     join t2 in _db.Common_RawCategory on t1.Common_RawCategoryFk equals t2.ID
                     where t1.Active == true
                     select new VMMis_RawMaterials
                     {
                         ID = t1.ID,
                         Common_RawCategoryFk = t1.Common_RawCategoryFk,
                         Common_RawCategoryName = t2.Name,
                         RawValue = t1.RawValue,
                         WIPValue = t1.WIPValue
                     }).OrderByDescending(x => x.ID).AsEnumerable();
            return v;
        }


        public async Task<int> MisRawMaterialsAdd(VMMis_RawMaterials vmMis_RawMaterials)
        {
            var result = -1;
            Mis_RawMaterials misRawMaterials = new Mis_RawMaterials
            {
                Common_RawCategoryFk = vmMis_RawMaterials.Common_RawCategoryFk,
                RawValue = vmMis_RawMaterials.RawValue,
                WIPValue = vmMis_RawMaterials.WIPValue,

                User = "UserOne",
            };
            _db.Mis_RawMaterials.Add(misRawMaterials);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = misRawMaterials.ID;
            }
            return result;
        }
        public async Task<int> RawMaterialsEdit(VMMis_RawMaterials vMMisRawMaterials)
        {
            var result = -1;
            //to select Accountining_Chart_Two data.....
            Mis_RawMaterials misRawMaterials = _db.Mis_RawMaterials.Find(vMMisRawMaterials.ID);
            misRawMaterials.Common_RawCategoryFk = vMMisRawMaterials.Common_RawCategoryFk;
            misRawMaterials.RawValue = vMMisRawMaterials.RawValue;
            misRawMaterials.WIPValue = vMMisRawMaterials.WIPValue;

            if (await _db.SaveChangesAsync() > 0)
            {
                result = misRawMaterials.ID;
            }
            return result;
        }
        public async Task<int> RawMaterialsDelete(int id)
        {
            var result = -1;

            if (id != 0)
            {
                Mis_RawMaterials misRawMaterials = _db.Mis_RawMaterials.Find(id);
                misRawMaterials.Active = false;

                if (await _db.SaveChangesAsync() > 0)
                {
                    result = misRawMaterials.ID;
                }
            }
            return result;
        }
        #endregion

        #region Shipment Status
        public async Task<VMMis_ShipmentStatus> ShipmentStatusGet()
        {
            VMMis_ShipmentStatus vmMisShipmentStatus = new VMMis_ShipmentStatus();
            vmMisShipmentStatus.DataList = await Task.Run(() => ShipmentStatusDataLoad());
            return vmMisShipmentStatus;
        }


        private IEnumerable<VMMis_ShipmentStatus> ShipmentStatusDataLoad()
        {
            var v = (from t1 in _db.Mis_ShipmentStatus
                         //join t2 in _db.Common_Order on t1.Common_OrderFk equals t2.ID
                     join t3 in _db.Common_Buyer on t1.Common_BuyerFk equals t3.ID
                     where t1.Active == true
                     select new VMMis_ShipmentStatus
                     {
                         ID = t1.ID,
                         Common_BuyerName = t3.Name,
                         Common_BuyerFk = t1.Common_BuyerFk,
                         Common_OrderNo = t1.OrderNo,
                         Date = t1.Date,
                         DeliveredQuantity = t1.DeliveredQuantity,
                         Exfactory = t1.Exfactory,
                         OrderQuantity = t1.OrderQuantity,
                         DueQuantity = t1.DueQty
                     }).OrderBy(x => x.Common_BuyerName).AsEnumerable();
            return v;
        }
        public async Task<VMMis_ShipmentStatus> MisShipmentStatusGet()
        {
            VMMis_ShipmentStatus vmMisShipmentStatus = new VMMis_ShipmentStatus();
            vmMisShipmentStatus.DataList = await Task.Run(() => MisShipmentStatusDataLoad());
            return vmMisShipmentStatus;
        }
        public async Task<VMMis_ShipmentStatus> MisShipmentStatusGetAll()
        {
            VMMis_ShipmentStatus vmMisShipmentStatus = new VMMis_ShipmentStatus();
            vmMisShipmentStatus.DataList = await Task.Run(() => MisShipmentStatusDataLoadAll());
            return vmMisShipmentStatus;
        }
        public IEnumerable<VMMis_ShipmentStatus> MisShipmentStatusDataLoad()
        {
            var v = (from t1 in _db.Mis_ShipmentStatus
                         //join t2 in _db.Common_Order on t1.Common_OrderFk equals t2.ID
                     join t3 in _db.Common_Buyer on t1.Common_BuyerFk equals t3.ID
                     where t1.Active == true
                     group new { t1, t3 } by new { t3.Name } into Group
                     select new VMMis_ShipmentStatus
                     {
                         ID = Group.First().t1.ID,
                         Common_BuyerName = Group.First().t3.Name,
                         Common_BuyerFk = Group.First().t1.Common_BuyerFk,
                         Common_OrderNo = Group.First().t1.OrderNo,
                         Date = Group.Max(x => x.t1.Date),
                         DeliveredQuantity = Group.Sum(x => x.t1.DeliveredQuantity),
                         Exfactory = Group.Sum(x => x.t1.Exfactory),
                         OrderQuantity = Group.Sum(x => x.t1.OrderQuantity),
                         DueQuantity = Group.Sum(x => x.t1.DueQty)
                     }).OrderBy(x => x.Common_BuyerName).Take(5).AsEnumerable();
            return v;
        }


        public IEnumerable<VMMis_ShipmentStatus> MisShipmentStatusDataLoadAll()
        {
            var v = (from t1 in _db.Mis_ShipmentStatus
                         //join t2 in _db.Common_Order on t1.Common_OrderFk equals t2.ID
                     join t3 in _db.Common_Buyer on t1.Common_BuyerFk equals t3.ID
                     where t1.Active == true
                     group new { t1, t3 } by new { t3.Name } into Group
                     select new VMMis_ShipmentStatus
                     {
                         ID = Group.First().t1.ID,
                         Common_BuyerName = Group.First().t3.Name,
                         Common_BuyerFk = Group.First().t1.Common_BuyerFk,
                         Common_OrderNo = Group.First().t1.OrderNo,
                         Date = Group.Max(x => x.t1.Date),
                         DeliveredQuantity = Group.Sum(x => x.t1.DeliveredQuantity),
                         Exfactory = Group.Sum(x => x.t1.Exfactory),
                         OrderQuantity = Group.Sum(x => x.t1.OrderQuantity),
                         DueQuantity = Group.Sum(x => x.t1.DueQty)
                     }).OrderBy(x => x.Common_BuyerName).AsEnumerable();
            return v;
        }
        public async Task<int> MisShipmentStatusAdd(VMMis_ShipmentStatus vmMis_ShipmentStatus)
        {
            var result = -1;
            Mis_ShipmentStatus misShipmentStatus = new Mis_ShipmentStatus
            {
                Common_BuyerFk = vmMis_ShipmentStatus.Common_BuyerFk,
                Date = vmMis_ShipmentStatus.Date,
                OrderNo = vmMis_ShipmentStatus.Common_OrderNo,
                OrderQuantity = vmMis_ShipmentStatus.OrderQuantity,
                DueQty = vmMis_ShipmentStatus.OrderQuantity - (vmMis_ShipmentStatus.Exfactory + vmMis_ShipmentStatus.DeliveredQuantity),
                DeliveredQuantity = vmMis_ShipmentStatus.DeliveredQuantity,
                Exfactory = vmMis_ShipmentStatus.Exfactory,

                User = "UserOne",
            };
            _db.Mis_ShipmentStatus.Add(misShipmentStatus);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = misShipmentStatus.ID;
            }
            return result;
        }
        public async Task<int> ShipmentStatusEdit(VMMis_ShipmentStatus vMMisShipmentStatus)
        {
            var result = -1;
            //to select Accountining_Chart_Two data.....
            Mis_ShipmentStatus misShipmentStatus = _db.Mis_ShipmentStatus.Find(vMMisShipmentStatus.ID);
            misShipmentStatus.Common_BuyerFk = vMMisShipmentStatus.Common_BuyerFk;
            misShipmentStatus.OrderNo = vMMisShipmentStatus.Common_OrderNo;
            misShipmentStatus.Date = vMMisShipmentStatus.Date;
            misShipmentStatus.DeliveredQuantity = vMMisShipmentStatus.DeliveredQuantity;
            misShipmentStatus.Exfactory = vMMisShipmentStatus.Exfactory;
            misShipmentStatus.OrderQuantity = vMMisShipmentStatus.OrderQuantity;
            misShipmentStatus.DueQty = vMMisShipmentStatus.OrderQuantity - (vMMisShipmentStatus.Exfactory + vMMisShipmentStatus.DeliveredQuantity);


            if (await _db.SaveChangesAsync() > 0)
            {
                result = misShipmentStatus.ID;
            }
            return result;
        }
        public async Task<int> ShipmentStatusDelete(int id)
        {
            var result = -1;

            if (id != 0)
            {
                Mis_ShipmentStatus misShipmentStatus = _db.Mis_ShipmentStatus.Find(id);
                misShipmentStatus.Active = false;


                if (await _db.SaveChangesAsync() > 0)
                {
                    result = misShipmentStatus.ID;
                }
            }
            return result;
        }

        public async Task<VMMis_ShipmentStatus> ShipmentStatusByBuyerGet(int id)
        {
            VMMis_ShipmentStatus vmMisShipmentStatus = new VMMis_ShipmentStatus();
            vmMisShipmentStatus = GetShipmentStatus(id);
            vmMisShipmentStatus.DataList = await Task.Run(() => ShipmentStatusByByerDataLoad(id));


            return vmMisShipmentStatus;
        }

        private VMMis_ShipmentStatus GetShipmentStatus(int id)
        {

            var v = (from t1 in _db.Mis_ShipmentStatus
                         //join t2 in _db.Common_Order on t1.Common_OrderFk equals t2.ID
                     join t3 in _db.Common_Buyer on t1.Common_BuyerFk equals t3.ID
                     where t1.Active == true
                     where t1.Common_BuyerFk == id
                     select new VMMis_ShipmentStatus
                     {
                         ID = t1.ID,
                         Common_BuyerFk = t3.ID,
                         Common_BuyerName = t3.Name,
                         Common_OrderNo = t1.OrderNo,
                         Date = t1.Date,
                         DeliveredQuantity = t1.DeliveredQuantity,
                         Exfactory = t1.Exfactory,
                         OrderQuantity = t1.OrderQuantity,
                         DueQuantity = t1.DueQty
                     }).OrderByDescending(x => x.ID).FirstOrDefault();
            return v;
        }

        public IEnumerable<VMMis_ShipmentStatus> ShipmentStatusByByerDataLoad(int id)
        {
            var v = (from t1 in _db.Mis_ShipmentStatus
                         //join t2 in _db.Common_Order on t1.Common_OrderFk equals t2.ID
                     join t3 in _db.Common_Buyer on t1.Common_BuyerFk equals t3.ID
                     where t1.Active == true
                     where t1.Common_BuyerFk == id
                     select new VMMis_ShipmentStatus
                     {
                         ID = t1.ID,
                         Common_BuyerFk = t3.ID,
                         Common_BuyerName = t3.Name,
                         Common_OrderNo = t1.OrderNo,
                         Date = t1.Date,
                         DeliveredQuantity = t1.DeliveredQuantity,
                         Exfactory = t1.Exfactory,
                         OrderQuantity = t1.OrderQuantity,
                         DueQuantity = t1.DueQty
                     }).OrderByDescending(x => x.ID).AsEnumerable();
            return v;
        }
        #endregion

        #region Daily Production
        public async Task<VMMis_DailyProduction> DailyProductionGet()
        {
            VMMis_DailyProduction vmMisDailyProduction = new VMMis_DailyProduction();
            vmMisDailyProduction.DataList = await Task.Run(() => DailyProductionDataLoad());
            return vmMisDailyProduction;
        }

        public IEnumerable<VMMis_DailyProduction> DailyProductionDataLoad()
        {
            var v = (from t1 in _db.Mis_DailyProduction
                         //join t2 in _db.Common_Order on t1.Common_OrderFk equals t2.ID
                     join t3 in _db.Common_Buyer on t1.Common_BuyerFk equals t3.ID
                     where t1.Active == true
                     select new VMMis_DailyProduction
                     {
                         ID = t1.ID,
                         Common_BuyerName = t3.Name,
                         Common_BuyerFk = t1.Common_BuyerFk,
                         Common_OrderNo = t1.OrderNo,
                         SMV = t1.SMV,
                         Date = t1.ProductionDate,
                         Dyeing = t1.Dyeing,
                         Finishing = t1.Finishing,
                         Knitting = t1.Knitting,
                         Sewing = t1.Sewing,
                         Cutting = t1.Cutting,
                         OrderQty = t1.OrderQty,
                         SewingDone = t1.SewingDone,
                         KnittingDone = t1.KnittingDone,
                         FinishingDone = t1.FinishingDone,
                         DyeingDone = t1.DyeingDone,
                         CuttingDone = t1.CuttingDone,
                         Ironing = t1.Ironing,
                         IroningDone = t1.IroningDone
                     }).OrderByDescending(x => x.Date).AsEnumerable();
            return v;
        }
        private IEnumerable<VMMis_DailyProduction> MisDailyProductionDataLoad()
        {
            var v = (from t1 in _db.Mis_DailyProduction
                         //join t2 in _db.Common_Order on t1.Common_OrderFk equals t2.ID
                     join t3 in _db.Common_Buyer on t1.Common_BuyerFk equals t3.ID
                     where t1.Active == true
                     select new VMMis_DailyProduction
                     {
                         ID = t1.ID,
                         Common_BuyerName = t3.Name,
                         Common_BuyerFk = t1.Common_BuyerFk,
                         Common_OrderNo = t1.OrderNo,
                         SMV = t1.SMV,
                         Date = t1.ProductionDate,
                         Dyeing = t1.Dyeing,
                         Finishing = t1.Finishing,
                         Knitting = t1.Knitting,
                         Sewing = t1.Sewing,
                         Cutting = t1.Cutting,
                         OrderQty = t1.OrderQty,
                         SewingDone = t1.SewingDone,
                         KnittingDone = t1.KnittingDone,
                         FinishingDone = t1.FinishingDone,
                         DyeingDone = t1.DyeingDone,
                         CuttingDone = t1.CuttingDone,
                          Ironing = t1.Ironing,
                           IroningDone = t1.IroningDone
                     }).OrderByDescending(x => x.Date).Take(5).AsEnumerable();
            return v;
        }

        public async Task<int> MisDailyProductionAdd(VMMis_DailyProduction vmMis_DailyProduction)
        {
            var result = -1;
            Mis_DailyProduction misDailyProduction = new Mis_DailyProduction
            {
                Common_BuyerFk = vmMis_DailyProduction.Common_BuyerFk,
                OrderNo = vmMis_DailyProduction.Common_OrderNo,
                SMV = vmMis_DailyProduction.SMV,
                Cutting = vmMis_DailyProduction.Cutting,
                ProductionDate = vmMis_DailyProduction.Date,
                Dyeing = vmMis_DailyProduction.Dyeing,
                Finishing = vmMis_DailyProduction.Finishing,
                Knitting = vmMis_DailyProduction.Knitting,
                Sewing = vmMis_DailyProduction.Sewing,
                CuttingDone = vmMis_DailyProduction.CuttingDone,
                DyeingDone = vmMis_DailyProduction.DyeingDone,
                FinishingDone = vmMis_DailyProduction.FinishingDone,
                KnittingDone = vmMis_DailyProduction.KnittingDone,
                SewingDone = vmMis_DailyProduction.SewingDone,
                OrderQty = vmMis_DailyProduction.OrderQty,
                 Ironing = vmMis_DailyProduction.Ironing,
                 IroningDone = vmMis_DailyProduction.IroningDone,
                User = "UserOne",
            };
            _db.Mis_DailyProduction.Add(misDailyProduction);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = misDailyProduction.ID;
            }
            return result;
        }
        public async Task<int> DailyProductionEdit(VMMis_DailyProduction vMMisDailyProduction)
        {
            var result = -1;
            //to select Accountining_Chart_Two data.....
            Mis_DailyProduction misDailyProduction = _db.Mis_DailyProduction.Find(vMMisDailyProduction.ID);
            misDailyProduction.Common_BuyerFk = vMMisDailyProduction.Common_BuyerFk;
            misDailyProduction.OrderNo = vMMisDailyProduction.Common_OrderNo;
            misDailyProduction.SMV = vMMisDailyProduction.SMV;
            misDailyProduction.Cutting = vMMisDailyProduction.Cutting;
            misDailyProduction.ProductionDate = vMMisDailyProduction.Date;
            misDailyProduction.Dyeing = vMMisDailyProduction.Dyeing;
            misDailyProduction.Finishing = vMMisDailyProduction.Finishing;
            misDailyProduction.Knitting = vMMisDailyProduction.Knitting;
            misDailyProduction.Sewing = vMMisDailyProduction.Sewing;
            misDailyProduction.CuttingDone = vMMisDailyProduction.CuttingDone;
            misDailyProduction.DyeingDone = vMMisDailyProduction.DyeingDone;
            misDailyProduction.FinishingDone = vMMisDailyProduction.FinishingDone;
            misDailyProduction.KnittingDone = vMMisDailyProduction.KnittingDone;
            misDailyProduction.SewingDone = vMMisDailyProduction.SewingDone;
            misDailyProduction.OrderQty = vMMisDailyProduction.OrderQty;
            misDailyProduction.Ironing = vMMisDailyProduction.Ironing;
            misDailyProduction.IroningDone = vMMisDailyProduction.IroningDone;
            if (await _db.SaveChangesAsync() > 0)
            {
                result = misDailyProduction.ID;
            }
            return result;
        }
        public async Task<int> DailyProductionDelete(int id)
        {
            var result = -1;

            if (id != 0)
            {
                Mis_DailyProduction misDailyProduction = _db.Mis_DailyProduction.Find(id);
                misDailyProduction.Active = false;

                if (await _db.SaveChangesAsync() > 0)
                {
                    result = misDailyProduction.ID;
                }
                return result;
            }
            return result;
        }
        #endregion
        public async Task<VMMis_FinancialStatus> FinancialStatusGet()
        {
            VMMis_FinancialStatus vmMisFinancialStatus = new VMMis_FinancialStatus();
            vmMisFinancialStatus.DataList = await Task.Run(() => FinancialStatusDataLoad());
            return vmMisFinancialStatus;
        }
        public IEnumerable<VMMis_FinancialStatus> FinancialStatusDataLoad()
        {
            var v = (from t1 in _db.Mis_FinancialStatus
                     where t1.Active == true
                     select new VMMis_FinancialStatus
                     {
                         ID = t1.ID,
                         Month = t1.Month,
                         Year = t1.Year,
                         Inflow = t1.Inflow,
                         Outflow = t1.Outflow,
                         RevenueProjection = t1.RevenueProjection,
                         ExpenditureProjection = t1.ExpenditureProjection
                     }).OrderByDescending(x => x.ID).AsEnumerable();
            return v;
        }


        public async Task<int> FinancialStatusAdd(VMMis_FinancialStatus vmMisFinancialStatus)
        {
            var result = -1;
            Mis_FinancialStatus misFinancialStatus = new Mis_FinancialStatus
            {
                Month = vmMisFinancialStatus.Month,
                Year = vmMisFinancialStatus.Year,
                Inflow = vmMisFinancialStatus.Inflow,
                Outflow = vmMisFinancialStatus.Outflow,
                RevenueProjection = vmMisFinancialStatus.RevenueProjection,
                ExpenditureProjection = vmMisFinancialStatus.ExpenditureProjection,
                User = "UserOne",
            };
            _db.Mis_FinancialStatus.Add(misFinancialStatus);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = misFinancialStatus.ID;
            }
            return result;
        }
        public async Task<int> FinancialStatusEdit(VMMis_FinancialStatus vmMisFinancialStatus)
        {
            var result = -1;
            //to select Accountining_Chart_Two data.....
            Mis_FinancialStatus misFinancialStatus = _db.Mis_FinancialStatus.Find(vmMisFinancialStatus.ID);
            misFinancialStatus.Month = vmMisFinancialStatus.Month;
            misFinancialStatus.Year = vmMisFinancialStatus.Year;
            misFinancialStatus.Inflow = vmMisFinancialStatus.Inflow;
            misFinancialStatus.Outflow = vmMisFinancialStatus.Outflow;
            misFinancialStatus.RevenueProjection = vmMisFinancialStatus.RevenueProjection;
            misFinancialStatus.ExpenditureProjection = vmMisFinancialStatus.ExpenditureProjection;

            if (await _db.SaveChangesAsync() > 0)
            {
                result = misFinancialStatus.ID;
            }
            return result;
        }
        public async Task<int> FinancialStatusDelete(int id)
        {
            var result = -1;

            if (id != 0)
            {
                Mis_FinancialStatus misFinancialStatus = _db.Mis_FinancialStatus.Find(id);
                misFinancialStatus.Active = false;

                if (await _db.SaveChangesAsync() > 0)
                {
                    result = misFinancialStatus.ID;
                }
            }
            return result;
        }



        private IEnumerable<VMMis_FinancialStatus> MisFinancialStatusDataLoad()
        {
            var v = (from t1 in _db.Mis_FinancialStatus
                     where t1.Active == true
                     select new VMMis_FinancialStatus
                     {
                         ID = t1.ID,
                         Month = t1.Month,
                         Year = t1.Year,
                         Inflow = t1.Inflow,
                         Outflow = t1.Outflow,
                         RevenueProjection = t1.RevenueProjection,
                         ExpenditureProjection = t1.ExpenditureProjection
                     }).OrderByDescending(x => x.ID).Take(5).AsEnumerable();
            return v;
        }








        public List<object> GetCommonDepartment()
        {
            var commonDepartmentList = new List<object>();
            foreach (var commonDepartment in _db.User_Department.Where(x => x.Active == true).ToList())
            {
                commonDepartmentList.Add(new { Text = commonDepartment.Name, Value = commonDepartment.ID });
            }
            return commonDepartmentList;
        }
        public List<object> GetYear()
        {
            var List = new List<object>();
            int currentYear = DateTime.Now.Year;
            for (int year = currentYear; year < currentYear + 10; year++)
            {
                List.Add(new SelectListItem { Value = year.ToString(), Text = year.ToString() });
            }
            return List;
        }

        public List<object> GetMonth()
        {
            var result = new List<object>();
            result.Add(new SelectListItem { Value = "01", Text = "January" });
            result.Add(new SelectListItem { Value = "02", Text = "February" });
            result.Add(new SelectListItem { Value = "03", Text = "March" });
            result.Add(new SelectListItem { Value = "04", Text = "April" });
            result.Add(new SelectListItem { Value = "05", Text = "May" });
            result.Add(new SelectListItem { Value = "06", Text = "June" });
            result.Add(new SelectListItem { Value = "07", Text = "July" });
            result.Add(new SelectListItem { Value = "08", Text = "August" });
            result.Add(new SelectListItem { Value = "09", Text = "September" });
            result.Add(new SelectListItem { Value = "10", Text = "October" });
            result.Add(new SelectListItem { Value = "11", Text = "November" });
            result.Add(new SelectListItem { Value = "12", Text = "December" });
            return result;
        }

        public List<object> AccountingHeadDropDownList()
        {
            var accountHeadList = new List<object>();
            foreach (var accountingHead in _db.Accounting_Head.Where(x => x.Active == true).ToList())
            {
                accountHeadList.Add(new { Text = accountingHead.Name, Value = accountingHead.ID });
            }
            return accountHeadList;
        }
        public List<object> CommonBuyerDropDownList()
        {
            var commonBuyerList = new List<object>();
            foreach (var commonBuyer in _db.Common_Buyer.Where(x => x.Active == true).ToList())
            {
                commonBuyerList.Add(new { Text = commonBuyer.Name, Value = commonBuyer.ID });
            }
            return commonBuyerList;
        }
        public List<object> CommonSupplierDropDownList()
        {
            var ommonSupplierList = new List<object>();
            foreach (var ommonSupplier in _db.Common_Supplier.Where(x => x.Active == true).ToList())
            {
                ommonSupplierList.Add(new { Text = ommonSupplier.Name, Value = ommonSupplier.ID });
            }
            return ommonSupplierList;
        }
        public List<object> CommonOrderDropDownList()
        {
            var commonOrderList = new List<object>();
            foreach (var commonOrder in _db.Merchandising_BuyerOrder.Where(x => x.Active == true).ToList())
            {
                commonOrderList.Add(new { Text = commonOrder.BuyerPO, Value = commonOrder.ID });
            }
            return commonOrderList;
        }
        public List<object> CommonRawCategoryDropDownList()
        {
            var commonRawCategoryList = new List<object>();
            foreach (var commonRawCategory in _db.Common_RawCategory.Where(x => x.Active == true).ToList())
            {
                commonRawCategoryList.Add(new { Text = commonRawCategory.Name, Value = commonRawCategory.ID });
            }
            return commonRawCategoryList;
        }
    }

}
