﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Erp.Core.Services.MIS
{
    
    public class VMMis
    {
        public decimal CMEarned { get; set; }
        public int CMEarnedQty { get; set; }
        public decimal CashInHand { get; set; }
        public decimal CashAtBank { get; set; }
        public decimal Receivable { get; set; }
        public decimal Payable { get; set; }
        public VMMis_OrderConfirm VMMis_OrderConfirm { get; set; }
        public VMMis_BTBLCStatus VMMis_BTBLCStatus { get; set; }
        public VMMis_FinancialStatus VMMis_FinancialStatus { get; set; }
        public int RawInventory { get; set; }
        public int WIPInventory { get; set; }
        public int FinishedInventory { get; set; }
        public VMMis_ShipmentStatus VMMis_ShipmentStatus { get; set; }
        public VMMis_DailyProduction VMMis_DailyProduction { get; set; }
        public VMMis_DailyAttendance VMMis_DailyAttendance { get; set; }
        public decimal BTBLimit { get; set; }
        public decimal BTBIssued { get; set; }
        public decimal BTBLiabilites { get; set; }
    }


    public class VMMis_CMEarned : BaseVM
    {
        public string Month { get; set; }
        public string Year { get; set; }
        public int Quantity { get; set; }
        public decimal Value { get; set; }
        public IEnumerable<VMMis_CMEarned> DataList { get; set; }
    }
    public class VMMis_CashInHand : BaseVM
    {

        [DisplayFormat(DataFormatString = "{0:yyyy/m/d}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; } = DateTime.Now;
        //public int Accounting_HeadFk { get; set; }
        
        public string Accounting_HeadName { get; set; }
        public decimal Value { get; set; }
        public IEnumerable<VMMis_CashInHand> DataList { get; set; }
    }
    public class VMMis_CashAtBank : BaseVM

    {
        [DisplayFormat(DataFormatString = "{0:yyyy/m/d}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }
        //public int Accounting_HeadFk { get; set; }

        public string Accounting_HeadName { get; set; }
        public decimal Value { get; set; }
        public IEnumerable<VMMis_CashAtBank> DataList { get; set; }
    }
    public class VMMis_IncomeStatement : BaseVM
    {
        public decimal NetProfit { get; set; }
        public decimal TotalExpense { get; set; }

        public decimal CostofGoodsSold { get; set; }
        public decimal TotalRevenueIncome { get; set; }

      
        [DisplayFormat(DataFormatString = "{0:yyyy/m/d}", ApplyFormatInEditMode = true)]
        public DateTime FromDate { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy/m/d}", ApplyFormatInEditMode = true)]
        public DateTime ToDate { get; set; }
        public int BaseHeadId { get; set; }
        public int ChartTableId { get; set; }
        public string Accounting_HeadName { get; set; }
        public decimal Value { get; set; }
        public IEnumerable<VMMis_IncomeStatement> DataList { get; set; }
    }
    public class VMMis_BalanceSheet : BaseVM

    {
        [DisplayFormat(DataFormatString = "{0:yyyy/m/d}", ApplyFormatInEditMode = true)]
        public DateTime FromDate { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy/m/d}", ApplyFormatInEditMode = true)]
        public DateTime ToDate { get; set; }
        public int BaseHeadId { get; set; }
        public int ChartTableId { get; set; }
        public string Accounting_HeadName { get; set; }
        public decimal Value { get; set; }

        public int TotalFixedAsset { get; set; }
        public int TotalCurrentAssets { get; set; }
        public int TotalAssets { get; set; }
        public int RetainedEarnings { get; set; }
        public int TotalEquity { get; set; }
        public int TotalLongTermLiabilities { get; set; }
        public int TotalCurrentLiabilities { get; set; }
        public int TotalEquityandLiabilities { get; set; }
        

        public IEnumerable<VMMis_BalanceSheet> DataList { get; set; }
    }
    public class VMMis_AccountsReceivable : BaseVM
    {
        public int Common_BuyerFk { get; set; }
        public string Common_BuyerName { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy/m/d}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }
        public decimal Value { get; set; }
        public IEnumerable<VMMis_AccountsReceivable> DataList { get; set; }
    }
    public class VMMis_AccountsPayable : BaseVM
    {
        public int Common_SupplierFk { get; set; }
        public string Common_SupplierName { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy/m/d}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }
        public decimal Value { get; set; }
        public IEnumerable<VMMis_AccountsPayable> DataList { get; set; }
    }
    public class VMMis_OrderConfirm : BaseVM
    {
        public int OrderQty { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy/m/d}", ApplyFormatInEditMode = true)]
        public DateTime? FinalDeliveryDate { get; set; } = DateTime.Now;
        public string Common_OrderNo { get; set; }
        //public int Common_OrderFk { get; set; }
        public string Common_BuyerName { get; set; }
        public int Common_BuyerFk { get; set; }
        public decimal OrderValue { get; set; }
        public decimal CmValue { get; set; }
        public IEnumerable<VMMis_OrderConfirm> DataList { get; set; }
    }

    public class VMMis_RawMaterials : BaseVM
    {
        public int Common_RawCategoryFk { get; set; }
        public string Common_RawCategoryName { get; set; }
        public int RawValue { get; set; }
        public int WIPValue { get; set; }
        public IEnumerable<VMMis_RawMaterials> DataList { get; set; }
    }
    public class VMMis_ShipmentStatus : BaseVM
    {
        public string Common_BuyerName { get; set; }
        //public int Common_BuyerID { get; set; }
        public int Common_BuyerFk { get; set; }
        public string Common_OrderNo { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy/m/d}", ApplyFormatInEditMode = true)]
        public DateTime? Date { get; set; }
        public int OrderQuantity { get; set; }
        public int DueQuantity { get; set; }
        public int Exfactory { get; set; }
        public int DeliveredQuantity { get; set; }
        public IEnumerable<VMMis_ShipmentStatus> DataList { get; set; }
    }

    public class VMMis_DailyProduction : BaseVM
    {
        [DisplayFormat(DataFormatString = "{0:yyyy/m/d}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }
        public int Common_BuyerFk { get; set; }
        public string Common_OrderNo { get; set; }
        public int OrderQty { get; set; }
        public string Common_BuyerName { get; set; }
        public int Knitting { get; set; }
        public decimal SMV { get; set; }
        public int Dyeing { get; set; }
        public int Cutting { get; set; }
        public int Sewing { get; set; }
        public int Finishing { get; set; }
        public int KnittingDone { get; set; }
        public int DyeingDone { get; set; }
        public int CuttingDone { get; set; }
        public int SewingDone { get; set; }
        public int FinishingDone { get; set; }
        public int Ironing { get; set; }

        public int IroningDone { get; set; }

        public IEnumerable<VMMis_DailyProduction> DataList { get; set; }
    }
    public class VMMis_DailyAttendance : BaseVM
    {
        [DisplayFormat(DataFormatString = "{0:yyyy/m/d}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }

        public string DateString { get; set; }
        public int WorkingHour { get; set; }
        public int TotalEmployee { get; set; }
        public int Present { get; set; }
        public int Absent { get; set; }
        public int OnLeave { get; set; }
        public int Off { get; set; }
        public IEnumerable<VMMis_DailyAttendance> DataList { get; set; }
        public int User_DepartmentFk { get;  set; }
        public string Common_DepartmenName { get; set; }
    }

    public class VMMis_BTBLCStatus : BaseVM
    {
        public string UdNo { get; set; }
        public int UdID { get; set; }

        public decimal MasterLCTotalValue { get; set; }
        public decimal BTBLimit { get; set; }
        public decimal BTBIssued { get; set; }
        public decimal BTBLiabilites { get; set; }
        public decimal BTBAcceptedValue { get; set; }
        public IEnumerable<VMMis_BTBLCStatus> DataList { get; set; }

    }
    public class VMMis_BTBLCStatusSlave : BaseVM
    {
        public string MasterLCNo { get; set; }
        public decimal MasterLCValue { get; set; }
        public decimal UdFk { get; set; }
       
        public IEnumerable<VMMis_BTBLCStatusSlave> DataList { get; set; }

    }

    public class VMMis_FinancialStatus : BaseVM
    {
       // public int ID { get; set; }
        public string Month { get; set; }
        public string Year { get; set; }
        public decimal Inflow { get; set; }
        public decimal Outflow { get; set; }
        public decimal RevenueProjection { get; set; }
        public decimal ExpenditureProjection { get; set; }
        public IEnumerable<VMMis_FinancialStatus> DataList { get; set; }
    }
}
