﻿using Erp.Core.Entity.Inventory;
using System;
using System.Collections.Generic;
///using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Threading.Tasks;

namespace Erp.Core.Services.Inventory
{
    public class InventoryService:BaseService
    {
        public InventoryService(IErpDbContext db)
        {
            _db = db;
        }
        public async Task<int> InventoryDataPush(VmInventory vm)
        {
            int result = -1;
           //Inventorydb inventory = new Inventorydb();
            var inventorydb = new List<Inventorydb>();
            inventorydb = vm.DataList.Select(x => new Inventorydb
            {
                StoreTypeFk = x.StoreTypeFk,
                TransactionType=x.TransactionType,
                Procurement_PurchaseInvoiceFK=x.Procurement_PurchaseInvoiceFK,
                Procurement_PurchaseOrderSlaveID = x.Procurement_PurchaseOrderSlaveID,
                Merchandising_StyleID =x.Merchandising_StyleID,
                Merchandising_StyleSlaveID =x.Merchandising_StyleSlaveID,
                Merchandising_BOFID=x.Merchandising_BOFID,
                Store_StockInFK =x.Store_StockInFK,
                Store_StockOutFK = x.Store_StockOutFK,
                Common_RawItemFK =x.Common_RawItemFK,
                Amount = x.Amount,
                Price =x.Price,
                Time = DateTime.Now,
                User =x.User,
                UserID = x.UserID,
                Active=true,
            }).ToList();
            await _db.Inventorydb.AddRangeAsync(inventorydb);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = 1;
            }
            return result;
        }

        public async Task<VmInventory> RawInventorySummeryListGet(DateTime fromdt, DateTime todt, int id=0,int storeTypeFk=0)
        {
            VmInventory vmInventory = new VmInventory();
            if(id>0)
            {
                vmInventory.DataList = await Task.Run(() => (from t1 in _db.Inventorydb
                                                             join t2 in _db.Common_RawItem on t1.Common_RawItemFK equals t2.ID
                                                             join t3 in _db.Common_Unit on t2.Common_UnitFK equals t3.ID
                                                             where t1.Active == true && (t1.Time >= fromdt && t1.Time <= todt)
                                                             && t1.Common_RawItemFK==id && t1.StoreTypeFk== storeTypeFk
                                                             group new { t1, t2, t3 } by new { t1.Common_RawItemFK } into all
                                                             select new VmInventory
                                                             {
                                                                 StoreTypeFk = all.First().t1.StoreTypeFk,
                                                                 ItemName = all.First().t2.Name,
                                                                 Common_RawItemFK = all.Key.Common_RawItemFK,
                                                                 Amount = all.Sum(x => x.t1.Amount),
                                                                 Price = decimal.Divide(all.Sum(x => x.t1.Price), all.Count()),
                                                                 TotalPrice = decimal.Multiply(all.Sum(x => x.t1.Amount), decimal.Divide(all.Sum(x => x.t1.Price), all.Count())),
                                                                 UnitName = all.First().t3.Name
                                                             }).OrderByDescending(x => x.ID).ToList());

            }
            else
            {
                vmInventory.DataList = await Task.Run(() => (from t1 in _db.Inventorydb
                                                             join t2 in _db.Common_RawItem on t1.Common_RawItemFK equals t2.ID
                                                             join t3 in _db.Common_Unit on t2.Common_UnitFK equals t3.ID
                                                             where t1.Active == true && (t1.Time >= fromdt && t1.Time <= todt)
                                                             && t1.StoreTypeFk == storeTypeFk
                                                             group new { t1, t2, t3 } by new { t1.Common_RawItemFK } into all
                                                             select new VmInventory
                                                             {
                                                                 StoreTypeFk = all.First().t1.StoreTypeFk,
                                                                 ItemName = all.First().t2.Name,
                                                                 Common_RawItemFK = all.Key.Common_RawItemFK,
                                                                 Amount = all.Sum(x => x.t1.Amount),
                                                                 Price = decimal.Divide(all.Sum(x => x.t1.Price), all.Count()),
                                                                 TotalPrice = decimal.Multiply(all.Sum(x => x.t1.Amount), decimal.Divide(all.Sum(x => x.t1.Price), all.Count())),
                                                                 UnitName = all.First().t3.Name
                                                             }).OrderByDescending(x => x.ID).ToList());
            }

            return vmInventory;
        }

        public async Task<VmInventory> RawInventoryItemWiseListGet(int id=0, int storeTypeFk = 0)
        {
            VmInventory vmInventory = new VmInventory
            {
                DataList = await Task.Run(() => (from t1 in _db.Inventorydb
                                                 join t2 in _db.Common_RawItem on t1.Common_RawItemFK equals t2.ID
                                                 join t3 in _db.Common_Unit on t2.Common_UnitFK equals t3.ID
                                                 join t4 in _db.Merchandising_Style on t1.Merchandising_StyleID equals t4.ID
                                                 where t1.Active == true && t1.Common_RawItemFK==id
                                                 && t1.StoreTypeFk == storeTypeFk
                                                 select new VmInventory
                                                 {
                                                     ID=t1.ID,
                                                     StoreTypeFk =t1.StoreTypeFk,
                                                     ItemName = t2.Name,
                                                     TransactionType = t1.TransactionType,
                                                     Procurement_PurchaseInvoiceFK = t1.Procurement_PurchaseInvoiceFK,
                                                     Procurement_PurchaseOrderSlaveID = t1.Procurement_PurchaseOrderSlaveID,
                                                     Merchandising_StyleID = t1.Merchandising_StyleID,
                                                     Merchandising_StyleSlaveID = t1.Merchandising_StyleSlaveID,
                                                     Merchandising_BOFID = t1.Merchandising_BOFID,
                                                     Store_StockInFK = t1.Store_StockInFK,
                                                     Common_RawItemFK = t1.Common_RawItemFK,
                                                     StyleNo=t4.CID +"/"+t4.StyleName,
                                                     Amount = t1.Amount,
                                                     Price = t1.Price,
                                                     TotalPrice= t1.Amount* t1.Price,
                                                     UnitName=t3.Name,
                                                     Time =t1.Time,
                                                     User = t1.User,
                                                     UserID = t1.UserID,
                                                     Active = true,
                                                 }).OrderByDescending(x => x.ID).ToList())
            };
            return vmInventory;
        }

        public async Task<VmInventory> FinishInventorySummeryListGet(DateTime fromdt, DateTime todt, int id = 0, int storeTypeFk = 0)
        {
            VmInventory vmInventory = new VmInventory();
            if (id > 0)
            {
                vmInventory.DataList = await Task.Run(() => (from t1 in _db.Inventorydb
                                                             join t2 in _db.Common_FinishItem on t1.Common_RawItemFK equals t2.ID
                                                             join t3 in _db.Common_Unit on t2.Common_UnitFK equals t3.ID
                                                             where t1.Active == true && (t1.Time >= fromdt && t1.Time <= todt)
                                                             && t1.Common_RawItemFK == id && t1.StoreTypeFk == storeTypeFk
                                                             group new { t1, t2, t3 } by new { t1.Common_RawItemFK } into all
                                                             select new VmInventory
                                                             {
                                                                 StoreTypeFk = all.First().t1.StoreTypeFk,
                                                                 ItemName = all.First().t2.Name,
                                                                 Common_RawItemFK = all.Key.Common_RawItemFK,
                                                                 Amount = all.Sum(x => x.t1.Amount),
                                                                 Price = decimal.Divide(all.Sum(x => x.t1.Price), all.Count()),
                                                                 TotalPrice = decimal.Multiply(all.Sum(x => x.t1.Amount), decimal.Divide(all.Sum(x => x.t1.Price), all.Count())),
                                                                 UnitName = all.First().t3.Name
                                                             }).OrderByDescending(x => x.ID).ToList());

            }
            else
            {
                vmInventory.DataList = await Task.Run(() => (from t1 in _db.Inventorydb
                                                             join t2 in _db.Common_FinishItem on t1.Common_RawItemFK equals t2.ID
                                                             join t3 in _db.Common_Unit on t2.Common_UnitFK equals t3.ID
                                                             where t1.Active == true && (t1.Time >= fromdt && t1.Time <= todt)
                                                             && t1.StoreTypeFk == storeTypeFk
                                                             group new { t1, t2, t3 } by new { t1.Common_RawItemFK } into all
                                                             select new VmInventory
                                                             {
                                                                 StoreTypeFk = all.First().t1.StoreTypeFk,
                                                                 ItemName = all.First().t2.Name,
                                                                 Common_RawItemFK = all.Key.Common_RawItemFK,
                                                                 Amount = all.Sum(x => x.t1.Amount),
                                                                 Price = decimal.Divide(all.Sum(x => x.t1.Price), all.Count()),
                                                                 TotalPrice = decimal.Multiply(all.Sum(x => x.t1.Amount), decimal.Divide(all.Sum(x => x.t1.Price), all.Count())),
                                                                 UnitName = all.First().t3.Name
                                                             }).OrderByDescending(x => x.ID).ToList());
            }

            return vmInventory;
        }

        public async Task<VmInventory> FinishInventoryItemWiseListGet(int id = 0, int storeTypeFk = 0)
        {
            VmInventory vmInventory = new VmInventory
            {
                DataList = await Task.Run(() => (from t1 in _db.Inventorydb
                                                 join t2 in _db.Common_FinishItem on t1.Common_RawItemFK equals t2.ID
                                                 join t3 in _db.Common_Unit on t2.Common_UnitFK equals t3.ID
                                                 join t4 in _db.Merchandising_Style on t1.Merchandising_StyleID equals t4.ID
                                                 where t1.Active == true && t1.Common_RawItemFK == id
                                                 && t1.StoreTypeFk == storeTypeFk
                                                 select new VmInventory
                                                 {
                                                     ID = t1.ID,
                                                     StoreTypeFk = t1.StoreTypeFk,
                                                     ItemName = t2.Name,
                                                     TransactionType = t1.TransactionType,
                                                     Procurement_PurchaseInvoiceFK = t1.Procurement_PurchaseInvoiceFK,
                                                     Procurement_PurchaseOrderSlaveID = t1.Procurement_PurchaseOrderSlaveID,
                                                     Merchandising_StyleID = t1.Merchandising_StyleID,
                                                     Merchandising_StyleSlaveID = t1.Merchandising_StyleSlaveID,
                                                     Merchandising_BOFID = t1.Merchandising_BOFID,
                                                     Store_StockInFK = t1.Store_StockInFK,
                                                     Common_RawItemFK = t1.Common_RawItemFK,
                                                     StyleNo = t4.CID + "/" + t4.StyleName,
                                                     Amount = t1.Amount,
                                                     Price = t1.Price,
                                                     TotalPrice = t1.Amount * t1.Price,
                                                     UnitName = t3.Name,
                                                     Time = t1.Time,
                                                     User = t1.User,
                                                     UserID = t1.UserID,
                                                     Active = true,
                                                 }).OrderByDescending(x => x.ID).ToList())
            };
            return vmInventory;
        }


        #region Store In out Transaction
        public async Task<int> StoreIn(int id)
        {
            int result = -1;
            var s = (from sim in _db.Store_StockInMaster
                     join si in _db.Store_StockIn on sim.ID equals si.Store_StockInMasterFK into si_Join
                     from si in si_Join.DefaultIfEmpty()
                     where sim.ID == id
                     select new
                     {
                         POFK = sim.PurchaseOrderFK,
                         StoreTypeFK = sim.StockType,
                         Store_StockInFK = sim.ID,
                         CommonRawItemFK = si.Common_RawItemFK,
                         TransactionType = 1,
                         ProcurementPOSlaveId = si.Procurement_PurchaseOrderSlaveFK
                     }).FirstOrDefault();

            Inventorydb idb = new Inventorydb();
            idb.Procurement_PurchaseOrderSlaveID = s.POFK;
            idb.StoreTypeFk = Convert.ToInt32(s.StoreTypeFK);
            idb.Store_StockInFK = Convert.ToInt32(s.Store_StockInFK);
            idb.TransactionType = s.TransactionType;
            idb.Procurement_PurchaseOrderSlaveID = s.ProcurementPOSlaveId;
            idb.Common_RawItemFK = s.CommonRawItemFK;
            _db.Inventorydb.Add(idb);

            if (await _db.SaveChangesAsync() > 0)
            {
                result = 1;
            }
            return result;
        }
        #endregion
    }
}
