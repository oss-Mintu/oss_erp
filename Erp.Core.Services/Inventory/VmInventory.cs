﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Erp.Core.Services.Inventory
{
    public class VmInventory :BaseVM
    {
        public int StoreTypeFk { get; set; }
        public int TransactionType { get; set; }
        //public InventoryTypeEnum StoreTypeFk { get; set; } = InventoryTypeEnum.Raw;
        //public StoreTransactionTypeEnum TransactionType { get; set; } = StoreTransactionTypeEnum.In;
        public int? Procurement_PurchaseInvoiceFK { get; set; }
        public int? Procurement_PurchaseOrderSlaveID { get; set; }
        public int? Merchandising_StyleID { get; set; }
        public int? Merchandising_StyleSlaveID { get; set; }
        public int? Merchandising_BOFID { get; set; }
        public int? Store_StockInFK { get; set; }
        public int? Store_StockOutFK { get; set; }
        public int Common_RawItemFK { get; set; }
        public decimal Amount { get; set; } = 0;
        public decimal Price { get; set; } = 0;
        public decimal TotalPrice { get; set; } = 0;
        public bool Active { get; set; } = true;
       // public DateTime Time { get; set; } = DateTime.Now;
        public string Remarks { get; set; }
        public IEnumerable<VmInventory> DataList { get; set; }
        public List<VmInventory> DataLists { get; set; }
        public int CategoryFK { get; set; }
        public int SubCategoryFK { get; set; }

        public string UnitName { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string ItemName { get; set; }
        public string StyleNo { get; set; }
    }
}
