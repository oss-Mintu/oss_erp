﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Text;

namespace Erp.Core.Services.Shipment
{

    public class VMStyleInvoiced : BaseVM
    {
        public bool Flag { get; set; }

        public int Shipment_InvoiceSlaveFk { get; set; }
        public int Shipment_InstructionSlaveFk { get; set; }
        public int Merchandising_StyleShipmentScheduleFk { get; set; }
        public int Merchandising_StyleFk { get; set; }
        public decimal DeliveryChallanQuantity { get; set; }

        public List<VMStyleInvoiced> vmStyleInvoicedList { get; set; }

        public string InvoiceID { get; set; }
        public string Destination { get; set; }
        public string PortNo { get; set; }
        public string BuyerPO { get; set; }
        public int PackQuantity { get; set; }
        public int PcsQuantity { get; set; }
        public DateTime ShipmentDate { get; set; }
    }

    public class VMShipmentDeliveryChallanSlave : VMShipmentDeliveryChallan
    {
        public int Shipment_InvoiceFk { get; set; }

        public string InvoiceID { get; set; }

        public int Shipment_DeliveryChallanFk { get; set; }
        public int Shipment_InvoiceSlaveFk { get; set; }
        public int Shipment_InstructionSlaveFk { get; set; }
        public int Merchandising_StyleShipmentScheduleFk { get; set; }
        public int Merchandising_StyleFk { get; set; }
        public int DeliveryChallanQuantity { get; set; }


        public IEnumerable<VMShipmentDeliveryChallanSlave> DataListSlave { get; set; }
        public string BuyerPO { get; internal set; }
       
        public string Destination { get; set; }
        public string PortNo { get; set; }
        public string CNFSupplierAddress { get; set; }
        public string TransporterSupplierAddress { get; set; }
    }
    public class VMShipmentDeliveryChallan : BaseVM
    {
        public DateTime Date { get; set; }

        public string ChallanNo { get; set; }
        
        public decimal CtnQty { get; set; }
        public decimal PackQuantity { get; set; }
        public decimal PcsQuantity { get; set; }

        public string CNFSupplierName { get; set; }
        public string TransporterSupplierName { get; set; }


        public int Common_CNFSupplierFk { get; set; }
        public string ContactPersonName { get; set; }
        public int Common_TransporterSupplierFk { get; set; }
        
        public string DriverName { get; set; }
        
        public string LockNo { get; set; }
       
        public string DLNo { get; set; }
       
        public string TruckNo { get; set; }
        
        public string DriverMobile { get; set; }
        
       
        
        public string ContactPersonMobile { get; set; }
        public string Remarks { get; set; }

        public string InTime { get; set; }
       
        public string OutTime { get; set; }
       
        public string GatePassNo { get; set; }
       
        public DateTime GatePassDate { get; set; }
        public SelectList ShipmentInvoiceList { get; set; } = new SelectList(new List<object>());
        public SelectList InvoicedStyleList { get; set; } = new SelectList(new List<object>());

        public SelectList TransportSupplierList { get; set; } = new SelectList(new List<object>());
        public SelectList CandFSupplierList { get; set; } = new SelectList(new List<object>());

        public SelectList ShipmentUnloadingPortList { get; set; } = new SelectList(new List<object>());

        public int ShipmentUnloadingPortFk { get; set; }
        public string UnloadingPortName { get; set; }

        public IEnumerable<VMShipmentDeliveryChallan> DataList { get; set; }
        public IEnumerable<TotalGenerateClass> TotalGenerateClass { get; set; }
    }
}
