﻿using Erp.Core.Entity.Common;
using Erp.Core.Entity.Merchandising;
using Erp.Core.Entity.Shipment;
using Erp.Core.Services.Integration;
using Erp.Core.Services.Merchandising;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Erp.Core.Services.Shipment
{
    public class ShipmentService : BaseService
    {
        //public ShipmentService(IErpDbContext db) => _db = db;
        private readonly IntegrationService integrationService;
        public ShipmentService(IErpDbContext db, HttpContext httpContext) { _db = db; integrationService = new IntegrationService(_db, httpContext); }

        public async Task<VMShipmentInstructionSlave> ShipmentInstructionGet(int id)
        {
            VMShipmentInstructionSlave instructionSlave = new VMShipmentInstructionSlave();
            instructionSlave = await Task.Run(() => (from t1 in _db.Shipment_ShipmentInstruction
                                                     where t1.Active && t1.ID == id
                                                     select new VMShipmentInstructionSlave
                                                     {
                                                         ID = t1.ID,
                                                         Shipment_ShipmentInstructionFk = t1.ID,
                                                         SICID = t1.SICID,
                                                         SIDate = t1.SIDate,
                                                         MerchandisingStyleList = new SelectList(StyleForShipmentInstruction(), "Value", "Text")
                                                         //ShipmentScheduleList = new SelectList(StyleShipmentScheduleDropDownList(), "Value", "Text"),
                                                     }).FirstOrDefault());
            instructionSlave.DataListSlave = await Task.Run(() => (from t1 in _db.Shipment_ShipmentInstruction
                                                                   join t2 in _db.Shipment_ShipmentInstructionSlave on t1.ID equals t2.Shipment_ShipmentInstructionFk
                                                                   join t3 in _db.Merchandising_StyleShipmentSchedule on t2.Merchandising_StyleShipmentScheduleFk equals t3.ID
                                                                   join t4 in _db.Merchandising_Style on t3.Merchandising_StyleFK equals t4.ID
                                                                   join t7 in _db.Merchandising_BuyerOrder on t4.Merchandising_BuyerOrderFK equals t7.ID

                                                                   join t5 in _db.Common_CountryPort on t3.Common_CountryPortFK equals t5.ID
                                                                   join t6 in _db.Common_Country on t3.Common_CountryFK equals t6.ID

                                                                   where t2.Active && t1.ID == id
                                                                   select new VMShipmentInstructionSlave
                                                                   {
                                                                       ID = t2.ID,
                                                                       Shipment_ShipmentInstructionFk = t2.Shipment_ShipmentInstructionFk.Value,
                                                                       SICID = t1.SICID,
                                                                       SIDate = t1.SIDate,
                                                                       StyleName = t7.BuyerPO + "/" + t4.CID + "/" + t4.StyleName,
                                                                       ScheduleQuantity = decimal.Divide(decimal.Divide(t3.Quantity, t4.SetQuantity), t4.PackPieceQty),
                                                                       InstructedQty = t2.InstructionQuantity,
                                                                       Destination = t6.Name,
                                                                       Port = t3.DestNo + " (" + t5.Name + ")",
                                                                       ShipmentDate = t3.ShipmentDate
                                                                   }).OrderByDescending(x => x.ID).AsEnumerable());



            return instructionSlave;
        }

        public async Task<VMCompletedShipmentStyle> GetCompletedShipmentStyle()
        {
            VMCompletedShipmentStyle vmCompletedShipmentStyle = new VMCompletedShipmentStyle
            {
                CompletedShipmentStyleList = await Task.Run(() => (from t1 in _db.Shipment_ShipmentInvoiceSlave
                                                                   join t2 in _db.Shipment_ShipmentInvoice on t1.Shipment_InvoiceFk equals t2.ID
                                                                   join t3 in _db.Merchandising_Style on t1.Merchandising_StyleFk equals t3.ID
                                                                   join t4 in _db.Merchandising_BuyerOrder on t3.Merchandising_BuyerOrderFK equals t4.ID
                                                                   join t5 in _db.Common_Buyer on t4.Common_BuyerFK equals t5.ID
                                                                   join t6 in _db.Common_FinishItem on t3.Common_FinishItemFK equals t6.ID
                                                                   where t1.Active && t2.Active && t3.Active && t4.Active && t6.Active
                                                                   group new { t1,t2,t3,t4, t5,t6 } by new { t3.ID } into Group
                                                                   select new VMCompletedShipmentStyle
                                                                   {
                                                                       BuyerPO = Group.First().t4.BuyerPO,
                                                                       CID = Group.First().t3.CID,
                                                                       Style = Group.First().t3.StyleName,
                                                                       BuyerName = Group.First().t5.Name,
                                                                       ID = Group.First().t3.ID,
                                                                       ItemName = Group.First().t6.Name,
                                                                       IsShipmentComplete = Group.First().t3.IsShipmentComplete,
                                                                       PackQty = Group.Sum( x => x.t3.PackQty),
                                                                       PieceQty = Group.Sum(x => x.t3.PieceQty * x.t3.PackPieceQty),
                                                                       InvoicedQuantity = Group.Sum(x => x.t1.InvoicedQuantity)
                                                                   }).AsEnumerable())
            };
            return vmCompletedShipmentStyle;
        }

        public async Task<VMShipmentSchedule> GetShipmentSchedule()
        {

            var v = _db.Shipment_ShipmentInstructionSlave.Where(x => x.Active).Select(x => x.Merchandising_StyleShipmentScheduleFk).ToList();
            VMShipmentSchedule vMShipment = new VMShipmentSchedule
            {
                DataList = await Task.Run(() => (from t1 in _db.Merchandising_StyleShipmentSchedule
                                                 join t2 in _db.Common_Country on t1.Common_CountryFK equals t2.ID
                                                 join t3 in _db.Common_CountryPort on t1.Common_CountryPortFK equals t3.ID
                                                 join t4 in _db.Merchandising_Style on t1.Merchandising_StyleFK equals t4.ID
                                                 join t5 in _db.Merchandising_BuyerOrder on t4.Merchandising_BuyerOrderFK equals t5.ID
                                                 where t1.Active && !v.Contains(t1.ID)
                                                 select new VMShipmentSchedule
                                                 {
                                                     ID = (int)t1.ID,
                                                     Merchandising_StyleFK = (int)t1.Merchandising_StyleFK,
                                                     Common_CountryFK = (int)t1.Common_CountryFK,
                                                     Common_CountryPortFK = (int)t1.Common_CountryPortFK,
                                                     Destination = t2.Name,
                                                     PortNo = t1.DestNo + " ("  +  t3.Name +")",
                                                     Quantity = t1.Quantity,
                                                     ColorSize = t1.Remarks,
                                                     Style = t5.BuyerPO + "/" + t4.CID + "/" + t4.StyleName,
                                                     Date = t1.ShipmentDate
                                                 }).OrderBy(b => b.Date).AsEnumerable())
            };
            return vMShipment;
        }
        public async Task<VMShipmentInstruction> GetSingleShipmentInstruction(int id)
        {

            var v = await Task.Run(() => (from t1 in _db.Shipment_ShipmentInstruction
                                          where t1.ID == id
                                          select new VMShipmentInstruction
                                          {

                                              SICID = t1.SICID,
                                              SIDate = t1.SIDate
                                          }).FirstOrDefault());
            return v;
        }
        public async Task<VMShipmentInstructionSlave> GetShipmentInstruction(int id)
        {

            var v = await Task.Run(() => (from t1 in _db.Shipment_ShipmentInstruction
                                          join t2 in _db.Shipment_ShipmentInstructionSlave on t1.ID equals t2.Shipment_ShipmentInstructionFk
                                          where t1.ID == id
                                          group new { t1, t2 } by new { t1.ID } into Group
                                          select new VMShipmentInstructionSlave
                                          {

                                              SICID = Group.First().t1.SICID,
                                              SIDate = Group.First().t1.SIDate,
                                              InstructedQty = Group.Sum(x => x.t2.InstructionQuantity),
                                              ///CtnQty = Group.Sum(x => x.t2.)
                                          }).FirstOrDefault());
            return v;
        }
        public async Task<VMShipmentBillOfExchange> GetSingleShipmentBillOfExchange(int id)
        {

            var v = await Task.Run(() => (from t1 in _db.Shipment_BillOfExchange
                                          join t2 in _db.Commercial_ECI on t1.Commercial_ECIFK equals t2.ID
                                          join t3 in _db.Common_Buyer on t2.Common_BuyerFK equals t3.ID
                                          where t1.ID == id
                                          select new VMShipmentBillOfExchange
                                          {
                                              BoECID = t1.BoECID,
                                              BoEDate = t1.BoEDate,
                                              //Commercial_ECIFK = t1.Commercial_ECIFK,
                                              CourierDate = t1.CourierDate,
                                              CourierNO = t1.CourierNO,
                                              Description = t1.Description,
                                              ECINO = t1.Description,
                                              FDBCDate = t1.FDBCDate,
                                              FDBCNO = t1.FDBCNO,
                                              ReceivedType = t1.ReceivedType,
                                              Remarks = t1.Remarks,
                                              User = t1.User,
                                              BuyerName = t3.Name,
                                              AccHead = t3.AccHeadID,
                                              BillValue = decimal.Round((from x in _db.Shipment_BillOfExchangeSlave
                                                                         join z in _db.Shipment_ShipmentInvoiceSlave on x.Shipment_ShipmentInvoiceFk equals z.Shipment_InvoiceFk
                                                                         join a in _db.Merchandising_Style on z.Merchandising_StyleFk equals a.ID
                                                                         where x.Active && x.Shipment_BillOfExchangeFk == t1.ID
                                                                         select z.InvoicedQuantity * a.PackPrice).DefaultIfEmpty(0).Sum(), 2)

                                          }).FirstOrDefault());
            return v;
        }
        public async Task<VMShipmentInvoice> GetSingleShipmentInvoice(int id)
        {

            var v = await Task.Run(() => (from t1 in _db.Shipment_ShipmentInvoice
                                          join t2 in _db.Shipment_TermsOfShipment on t1.Shipment_TermsOfShipmentFK equals t2.ID
                                          join t3 in _db.Common_CountryPort on t1.Shipment_PortOfLoadingFk equals t3.ID

                                          where t1.ID == id
                                          select new VMShipmentInvoice
                                          {

                                              BLDate = t1.BLDate,
                                              BLNo = t1.BLNo,
                                              //CBM = t1.CBM,
                                              ActionId = 2,

                                              ErcNo = t1.ErcNo,
                                              EstimatedReceivedDate = t1.EstimatedReceivedDate,
                                              ExpDate = t1.ExpDate,
                                              ExpNo = t1.ExpNo,
                                              Shipment_TermsOfShipmentFK = t1.Shipment_TermsOfShipmentFK,
                                              UserID = t1.UserID,
                                              IsActive = t1.Active,

                                              //GWT = t1.GWT,
                                              ID = t1.ID,
                                              AdjustedNote = t1.AdjustedNote,
                                              AdjustedValue = t1.AdjustedValue,
                                              InvoiceCID = t1.InvoiceCID,
                                              InvoiceDate = t1.InvoiceDate,
                                              InvoiceIdNo = t1.InvoiceIdNo,

                                              Shipment_PortOfLoadingFk = t1.Shipment_PortOfLoadingFk,
                                              ShippedBy = t1.ShippedBy,
                                              ShippingMarks = t1.ShippingMarks,
                                              TermsOfShipment = t2.Name,
                                              PortOfLoading = t3.Name,

                                              User = t1.User
                                          }).FirstOrDefault());
            return v;
        }

        public async Task<VMShipmentDeliveryChallan> GetSingleShipmentDeliveryChallan(int id)
        {

            var v = await Task.Run(() => (from t1 in _db.Shipment_DeliveryChallan
                                          where t1.ID == id
                                          select new VMShipmentDeliveryChallan
                                          {
                                              ChallanNo = t1.ChallanNo,
                                              ContactPersonMobile = t1.ContactPersonMobile,
                                              ContactPersonName = t1.ContactPersonName,
                                              Date = t1.Date,
                                              Common_CNFSupplierFk = t1.Common_CNFSupplierFk,
                                              Common_TransporterSupplierFk = t1.Common_TransporterSupplierFk,
                                              UserID = t1.UserID,
                                              DLNo = t1.DLNo,
                                              DriverMobile = t1.DriverMobile,
                                              DriverName = t1.DriverName,
                                              GatePassDate = t1.GatePassDate,
                                              GatePassNo = t1.GatePassNo,
                                              ID = t1.ID,
                                              InTime = t1.InTime,
                                              LockNo = t1.LockNo,
                                              OutTime = t1.OutTime,
                                              Remarks = t1.Remarks,
                                              ShipmentUnloadingPortFk = t1.ShipmentUnloadingPortFk,
                                              TruckNo = t1.TruckNo,
                                              User = t1.User,
                                              IsActive = t1.Active
                                          }).FirstOrDefault());
            return v;
        }
        public IEnumerable<VMShipmentInstruction> ShipmentInstructionesGet()
        {
            VMShipmentInstruction model = new VMShipmentInstruction();
            model.DataList = (from t1 in _db.Shipment_ShipmentInstruction
                              where t1.Active
                              select new VMShipmentInstruction
                              {
                                  ID = t1.ID,
                                  SICID = t1.SICID,
                                  SIDate = t1.SIDate,
                                  TotalInstructedQty = (from x in _db.Shipment_ShipmentInstructionSlave where x.Active && x.Shipment_ShipmentInstructionFk == t1.ID select x.InstructionQuantity).DefaultIfEmpty(0).Sum(),
                                  StyleList = (from x in _db.Shipment_ShipmentInstructionSlave
                                               join y in _db.Merchandising_Style on x.Merchandising_StyleFk equals y.ID
                                               join z in _db.Merchandising_BuyerOrder on y.Merchandising_BuyerOrderFK equals z.ID

                                               where x.Active == true && x.Shipment_ShipmentInstructionFk == t1.ID
                                               select z.BuyerPO + "/" + y.CID + "/" + y.StyleName).Distinct().ToList()
                              }).OrderByDescending(x => x.ID).AsEnumerable();

            return model.DataList;
        }
        public async Task<int> ShipmentInstructionAdd(VMShipmentInstruction vmShipmentInstruction)
        {
            var result = -1;
            #region Genarate Instruction No
            int totalInstruction = _db.Shipment_ShipmentInstruction.Count();

            if (totalInstruction == 0)
            {
                totalInstruction = 1;
            }
            else
            {
                totalInstruction++;
            }
            int year = DateTime.Now.Year;
            var newString = totalInstruction.ToString().PadLeft(5, '0');

            string instructionCID = "SI-" + year + "-" + newString;
            #endregion
            Shipment_ShipmentInstruction shipmentShipmentInstruction = new Shipment_ShipmentInstruction
            {
                SICID = instructionCID,
                SIDate = DateTime.Now,
                UserID = vmShipmentInstruction.UserID,
            };
            _db.Shipment_ShipmentInstruction.Add(shipmentShipmentInstruction);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = shipmentShipmentInstruction.ID;
            }
            return result;
        }
        public async Task<int> ShipmentInstructionEdit(VMShipmentInstruction vmShipmentInstruction)
        {
            var result = -1;
            //to select Accountining_Chart_Two data.....
            Shipment_ShipmentInstruction shipmentShipmentInstruction = _db.Shipment_ShipmentInstruction.Find(vmShipmentInstruction.ID);
            shipmentShipmentInstruction.SICID = vmShipmentInstruction.SICID;
            shipmentShipmentInstruction.SIDate = vmShipmentInstruction.SIDate;

            if (await _db.SaveChangesAsync() > 0)
            {
                result = shipmentShipmentInstruction.ID;
            }
            return result;
        }
        public async Task<int> ShipmentInstructionDelete(VMShipmentInstruction vmShipmentInstruction)
        {
            int result = -1;

            if (vmShipmentInstruction.ID != 0)
            {
                Shipment_ShipmentInstruction shipmentShipmentInstruction = _db.Shipment_ShipmentInstruction.Find(vmShipmentInstruction.ID);
                shipmentShipmentInstruction.Active = false;
                List<Shipment_ShipmentInstructionSlave> shipmentInstructionSlaveList = new List<Shipment_ShipmentInstructionSlave>();
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = shipmentShipmentInstruction.ID;
                    var slaves = _db.Shipment_ShipmentInstructionSlave.Where(x => x.Shipment_ShipmentInstructionFk == shipmentShipmentInstruction.ID && x.Active == true).ToList();

                    foreach (var shipmentInstructionSlave in slaves)
                    {
                        shipmentInstructionSlave.Active = false;
                        shipmentInstructionSlaveList.Add(shipmentInstructionSlave);
                    }

                }
                await _db.SaveChangesAsync();

            }
            return result;
        }

        public IEnumerable<VMShipmentInstructionSlave> ShipmentInstructionSlaveGet(VMShipmentInstructionSlave model)
        {
            model.DataListSlave = (from t1 in _db.Shipment_ShipmentInstructionSlave
                                   where t1.Active && t1.Shipment_ShipmentInstructionFk == model.Shipment_ShipmentInstructionFk
                                   select new VMShipmentInstructionSlave
                                   {
                                       ID = t1.ID,
                                       Shipment_ShipmentInstructionFk = t1.Shipment_ShipmentInstructionFk.Value,
                                       InstructedQty = t1.InstructionQuantity
                                   }).OrderByDescending(x => x.ID).AsEnumerable();

            return model.DataListSlave;
        }
        public async Task<int> ShipmentInstructionSlaveAdd(VMShipmentInstructionSlave vmShipmentInstructionSlave, VMStyleSchedule vmStyleSchedule)
        {
            var result = -1;
            var styleScheduleList = vmStyleSchedule.StyleScheduleList.Where(x => x.Flag == true && x.Quantity > 0).ToList();
            if (styleScheduleList.Any())
            {
                for (int i = 0; i < styleScheduleList.Count(); i++)
                {
                    Shipment_ShipmentInstructionSlave shipmentInstructionSlave = new Shipment_ShipmentInstructionSlave
                    {
                        InstructionQuantity = styleScheduleList[i].Quantity,
                        Merchandising_StyleShipmentScheduleFk = styleScheduleList[i].ID,
                        Shipment_ShipmentInstructionFk = vmShipmentInstructionSlave.Shipment_ShipmentInstructionFk,
                        Merchandising_StyleFk = styleScheduleList[i].Merchandising_StyleFK,

                        UserID = vmShipmentInstructionSlave.UserID,
                    };
                    _db.Shipment_ShipmentInstructionSlave.Add(shipmentInstructionSlave);
                    if (await _db.SaveChangesAsync() > 0)
                    {
                        result = shipmentInstructionSlave.ID;
                    }
                }
            }

            return result;
        }
        public async Task<int> ShipmentInstructionSlaveEdit(VMShipmentInstructionSlave vmShipmentInstructionSlave)
        {
            var result = -1;
            //to select Accountining_Chart_Two data.....
            Shipment_ShipmentInstructionSlave shipmentInstructionSlave = _db.Shipment_ShipmentInstructionSlave.Find(vmShipmentInstructionSlave.ID);
            shipmentInstructionSlave.Shipment_ShipmentInstructionFk = vmShipmentInstructionSlave.Shipment_ShipmentInstructionFk;
            shipmentInstructionSlave.Merchandising_StyleShipmentScheduleFk = vmShipmentInstructionSlave.Merchandising_StyleShipmentScheduleFk;
            shipmentInstructionSlave.InstructionQuantity = vmShipmentInstructionSlave.InstructedQty;
            shipmentInstructionSlave.UserID = vmShipmentInstructionSlave.UserID;

            if (await _db.SaveChangesAsync() > 0)
            {
                result = shipmentInstructionSlave.ID;
            }
            return result;
        }
        public async Task<int> ShipmentInstructionSlaveDelete(VMShipmentInstructionSlave vmShipmentInstructionSlave)
        {
            int? resualt = -1;

            if (vmShipmentInstructionSlave.ID != 0)
            {
                Shipment_ShipmentInstructionSlave shipmentInstructionSlave = _db.Shipment_ShipmentInstructionSlave.Find(vmShipmentInstructionSlave.ID);
                shipmentInstructionSlave.Active = false;

                if (await _db.SaveChangesAsync() > 0)
                {
                    resualt = shipmentInstructionSlave.Shipment_ShipmentInstructionFk;
                }
            }
            return resualt.Value;
        }
        public async Task<VMShipmentDeliveryChallanSlave> ShipmentDeliveryChallanGet(int id)
        {
            VMShipmentDeliveryChallanSlave vMShipment = new VMShipmentDeliveryChallanSlave();
            vMShipment = await Task.Run(() => (from t2 in _db.Shipment_DeliveryChallan
                                               join t3 in _db.Common_CountryPort on t2.ShipmentUnloadingPortFk equals t3.ID

                                               join t13 in _db.Common_Supplier on t2.Common_CNFSupplierFk equals t13.ID
                                               join t14 in _db.Common_Supplier on t2.Common_TransporterSupplierFk equals t14.ID
                                               where t2.ID == id && t2.Active
                                               select new VMShipmentDeliveryChallanSlave
                                               {
                                                   ID = t2.ID,
                                                   Date = t2.Date,
                                                   TruckNo = t2.TruckNo,
                                                   Remarks = t2.Remarks,
                                                   ChallanNo = t2.ChallanNo,
                                                   Shipment_DeliveryChallanFk = t2.ID,
                                                   Common_CNFSupplierFk = t2.Common_CNFSupplierFk,
                                                   CNFSupplierName = t13.Name,
                                                   Common_TransporterSupplierFk = t2.Common_TransporterSupplierFk,
                                                   TransporterSupplierName = t14.Name,
                                                   ShipmentUnloadingPortFk = t2.ShipmentUnloadingPortFk,
                                                   ContactPersonName = t2.ContactPersonName,
                                                   ContactPersonMobile = t2.ContactPersonMobile,
                                                   DLNo = t2.DLNo,
                                                   UnloadingPortName = t3.Name,
                                                   DriverName = t2.DriverName,
                                                   DriverMobile = t2.DriverMobile,
                                                   GatePassNo = t2.GatePassNo,
                                                   GatePassDate = t2.GatePassDate,
                                                   LockNo = t2.TruckNo,
                                                   OutTime = t2.OutTime,
                                                   InTime = t2.InTime,
                                                   InvoicedStyleList = new SelectList(StyleForDeliveryChallan(), "Value", "Text"),
                                                   TransportSupplierList = new SelectList(CommonTransportSupplierDropDownList(), "Value", "Text"),
                                                   CandFSupplierList = new SelectList(CommonCandFSupplierDropDownList(), "Value", "Text"),

                                               }).FirstOrDefault());

            vMShipment.DataListSlave = await Task.Run(() => (from t1 in _db.Shipment_DeliveryChallanSlave
                                                             join t2 in _db.Shipment_DeliveryChallan on t1.Shipment_DeliveryChallanFk equals t2.ID

                                                             join t3 in _db.Shipment_ShipmentInvoiceSlave on t1.Shipment_InvoiceSlaveFk equals t3.ID
                                                             join t4 in _db.Shipment_ShipmentInvoice on t3.Shipment_InvoiceFk equals t4.ID

                                                             join t5 in _db.Shipment_ShipmentInstructionSlave on t1.Shipment_InstructionSlaveFk equals t5.ID
                                                             join t6 in _db.Shipment_ShipmentInstruction on t5.Shipment_ShipmentInstructionFk equals t6.ID

                                                             join t7 in _db.Merchandising_StyleShipmentSchedule on t1.Merchandising_StyleShipmentScheduleFk equals t7.ID
                                                             join t8 in _db.Merchandising_Style on t1.Merchandising_StyleFk equals t8.ID
                                                             join t9 in _db.Merchandising_BuyerOrder on t8.Merchandising_BuyerOrderFK equals t9.ID
                                                             join t10 in _db.Common_FinishItem on t8.Common_FinishItemFK equals t10.ID

                                                             join t11 in _db.Common_Country on t7.Common_CountryFK equals t11.ID
                                                             join t12 in _db.Common_CountryPort on t7.Common_CountryPortFK equals t12.ID
                                                             join t13 in _db.Common_Supplier on t2.Common_CNFSupplierFk equals t13.ID
                                                             join t14 in _db.Common_Supplier on t2.Common_TransporterSupplierFk equals t14.ID
                                                             where t1.Shipment_DeliveryChallanFk == id && t1.Active && t2.Active && t3.Active
                                                             && t4.Active && t5.Active && t6.Active && t7.Active

                                                             select new VMShipmentDeliveryChallanSlave
                                                             {
                                                                 InvoiceID = t4.InvoiceCID,
                                                                 BuyerPO = t9.BuyerPO,
                                                                 Destination = t11.Name,
                                                                 PortNo = t7.DestNo +" (" + t12.Name + ")",
                                                                 PackQuantity =decimal.Round(t1.DeliveryChallanQuantity),
                                                                 PcsQuantity =decimal.Round(decimal.Multiply(t1.DeliveryChallanQuantity , t8.PackPieceQty)),
                                                                 CtnQty =decimal.Round(decimal.Divide(t1.DeliveryChallanQuantity , t8.PackInCTN)),
                                                                 ID = t1.ID
                                                             }).OrderByDescending(x => x.ID));



            return vMShipment;
        }

        public async Task<VMShipmentDeliveryChallanSlave> ShipmentDeliveryChallanesPrintGet(int id)
        {
            VMShipmentDeliveryChallanSlave vMShipment = new VMShipmentDeliveryChallanSlave();
            vMShipment = await Task.Run(() => (from t2 in _db.Shipment_DeliveryChallan
                                               join t3 in _db.Common_CountryPort on t2.ShipmentUnloadingPortFk equals t3.ID

                                               join t13 in _db.Common_Supplier on t2.Common_CNFSupplierFk equals t13.ID
                                               join t14 in _db.Common_Supplier on t2.Common_TransporterSupplierFk equals t14.ID
                                               where t2.ID == id && t2.Active
                                               select new VMShipmentDeliveryChallanSlave
                                               {
                                                   ID = t2.ID,
                                                   Date = t2.Date,
                                                   TruckNo = t2.TruckNo,
                                                   Remarks = t2.Remarks,
                                                   ChallanNo = t2.ChallanNo,
                                                   Shipment_DeliveryChallanFk = t2.ID,
                                                   Common_CNFSupplierFk = t2.Common_CNFSupplierFk,
                                                   CNFSupplierName = t13.Name,
                                                   CNFSupplierAddress = t13.Address,
                                                   TransporterSupplierAddress = t14.Address,
                                                   Common_TransporterSupplierFk = t2.Common_TransporterSupplierFk,
                                                   TransporterSupplierName = t14.Name,
                                                   ShipmentUnloadingPortFk = t2.ShipmentUnloadingPortFk,
                                                   ContactPersonName = t2.ContactPersonName,
                                                   ContactPersonMobile = t2.ContactPersonMobile,
                                                   DLNo = t2.DLNo,
                                                   UnloadingPortName = t3.Name,
                                                   DriverName = t2.DriverName,
                                                   DriverMobile = t2.DriverMobile,
                                                   GatePassNo = t2.GatePassNo,
                                                   GatePassDate = t2.GatePassDate,
                                                   LockNo = t2.LockNo,
                                                   OutTime = t2.OutTime,
                                                   InTime = t2.InTime,
                                                   ShipmentInvoiceList = new SelectList(ShipmentInvoiceDropDownList(), "Value", "Text"),
                                               }).FirstOrDefault());

            vMShipment.DataListSlave = await Task.Run(() => (from t1 in _db.Shipment_DeliveryChallanSlave
                                                             join t2 in _db.Shipment_DeliveryChallan on t1.Shipment_DeliveryChallanFk equals t2.ID

                                                             join t3 in _db.Shipment_ShipmentInvoiceSlave on t1.Shipment_InvoiceSlaveFk equals t3.ID
                                                             join t4 in _db.Shipment_ShipmentInvoice on t3.Shipment_InvoiceFk equals t4.ID

                                                             join t5 in _db.Shipment_ShipmentInstructionSlave on t1.Shipment_InstructionSlaveFk equals t5.ID
                                                             join t6 in _db.Shipment_ShipmentInstruction on t5.Shipment_ShipmentInstructionFk equals t6.ID

                                                             join t7 in _db.Merchandising_StyleShipmentSchedule on t1.Merchandising_StyleShipmentScheduleFk equals t7.ID
                                                             join t8 in _db.Merchandising_Style on t1.Merchandising_StyleFk equals t8.ID
                                                             join t9 in _db.Merchandising_BuyerOrder on t8.Merchandising_BuyerOrderFK equals t9.ID
                                                             join t10 in _db.Common_FinishItem on t8.Common_FinishItemFK equals t10.ID

                                                             join t11 in _db.Common_Country on t7.Common_CountryFK equals t11.ID
                                                             join t12 in _db.Common_CountryPort on t7.Common_CountryPortFK equals t12.ID
                                                             join t13 in _db.Common_Supplier on t2.Common_CNFSupplierFk equals t13.ID
                                                             join t14 in _db.Common_Supplier on t2.Common_TransporterSupplierFk equals t14.ID
                                                             where t1.Shipment_DeliveryChallanFk == id && t1.Active && t2.Active && t3.Active

                                                             select new VMShipmentDeliveryChallanSlave
                                                             {
                                                                 InvoiceID = t4.InvoiceCID,
                                                                 BuyerPO = t9.BuyerPO,
                                                                 Destination = t11.Name,
                                                                 PortNo = t7.DestNo + " (" + t12.Name + ")",

                                                                 PackQuantity =decimal.Round(t1.DeliveryChallanQuantity),
                                                                 PcsQuantity =decimal.Round(decimal.Multiply(t1.DeliveryChallanQuantity , t8.PackPieceQty)),
                                                                 CtnQty =decimal.Round(decimal.Divide(t1.DeliveryChallanQuantity , t8.PackInCTN)),
                                                                 ID = t1.ID
                                                             }).OrderByDescending(x => x.ID));



            return vMShipment;
        }
        public IEnumerable<VMShipmentDeliveryChallan> ShipmentDeliveryChallanesGet()
        {
            VMShipmentDeliveryChallan model = new VMShipmentDeliveryChallan();
            model.DataList = (from t1 in _db.Shipment_DeliveryChallan
                              where t1.Active
                              select new VMShipmentDeliveryChallan
                              {
                                  ID = t1.ID,
                                  Date = t1.Date,
                                  TruckNo = t1.TruckNo,
                                  ChallanNo = t1.ChallanNo,
                                  DLNo = t1.DLNo,
                                  DriverName = t1.DriverName,
                                  ContactPersonName = t1.ContactPersonName,
                                  ContactPersonMobile = t1.ContactPersonMobile,
                                  DriverMobile = t1.DriverMobile,

                                  GatePassDate = t1.GatePassDate,
                                  GatePassNo = t1.GatePassNo,
                                  CtnQty = (from st1 in _db.Shipment_DeliveryChallanSlave
                                            join st4 in _db.Merchandising_Style on st1.Merchandising_StyleFk equals st4.ID
                                            where st1.Shipment_DeliveryChallanFk == t1.ID && st1.Active == true
                                            select decimal.Round(decimal.Divide(st1.DeliveryChallanQuantity , st4.PackInCTN))).DefaultIfEmpty(0).Sum(),
                                  PackQuantity = (from st1 in _db.Shipment_DeliveryChallanSlave
                                                  join st4 in _db.Merchandising_Style on st1.Merchandising_StyleFk equals st4.ID
                                                  where st1.Shipment_DeliveryChallanFk == t1.ID && st1.Active == true
                                                  select decimal.Round(st1.DeliveryChallanQuantity)).DefaultIfEmpty(0).Sum(),
                                  PcsQuantity = (from st1 in _db.Shipment_DeliveryChallanSlave
                                                 join st4 in _db.Merchandising_Style on st1.Merchandising_StyleFk equals st4.ID
                                                 where st1.Shipment_DeliveryChallanFk == t1.ID && st1.Active == true
                                                 select decimal.Round(decimal.Multiply(st1.DeliveryChallanQuantity , st4.PackPieceQty))).DefaultIfEmpty(0).Sum()


                              }).OrderByDescending(x => x.ID).AsEnumerable();

            return model.DataList;
        }
        public async Task<int> ShipmentDeliveryChallanAdd(VMShipmentDeliveryChallan vmShipmentDeliveryChallan)
        {
            var result = -1;
            #region Genarate Challan No
            int totalChallan = _db.Shipment_DeliveryChallan.Count();

            if (totalChallan == 0)
            {
                totalChallan = 1;
            }
            else
            {
                totalChallan++;
            }
            int year = DateTime.Now.Year;
            var newString = totalChallan.ToString().PadLeft(5, '0');

            string challanCID = "CHA-" + year + "-" + newString;
            #endregion
            Shipment_DeliveryChallan shipmentDeliveryChallan = new Shipment_DeliveryChallan
            {
                Date = vmShipmentDeliveryChallan.Date,
                ChallanNo = challanCID,
                InTime = vmShipmentDeliveryChallan.InTime,
                OutTime = vmShipmentDeliveryChallan.OutTime,
                LockNo = vmShipmentDeliveryChallan.LockNo,
                ContactPersonMobile = vmShipmentDeliveryChallan.ContactPersonMobile,
                ContactPersonName = vmShipmentDeliveryChallan.ContactPersonName,
                Common_CNFSupplierFk = vmShipmentDeliveryChallan.Common_CNFSupplierFk,
                Common_TransporterSupplierFk = vmShipmentDeliveryChallan.Common_TransporterSupplierFk,
                DLNo = vmShipmentDeliveryChallan.DLNo,
                DriverMobile = vmShipmentDeliveryChallan.DriverMobile,
                DriverName = vmShipmentDeliveryChallan.DriverName,
                GatePassDate = vmShipmentDeliveryChallan.GatePassDate,
                GatePassNo = vmShipmentDeliveryChallan.GatePassNo,
                Remarks = vmShipmentDeliveryChallan.Remarks,
                ShipmentUnloadingPortFk = vmShipmentDeliveryChallan.ShipmentUnloadingPortFk,
                TruckNo = vmShipmentDeliveryChallan.TruckNo,
                UserID = vmShipmentDeliveryChallan.UserID

            };
            _db.Shipment_DeliveryChallan.Add(shipmentDeliveryChallan);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = shipmentDeliveryChallan.ID;
            }
            return result;
        }
        public async Task<int> ShipmentDeliveryChallanEdit(VMShipmentDeliveryChallan vmShipmentDeliveryChallan)
        {
            var result = -1;
            //to select Accountining_Chart_Two data.....
            Shipment_DeliveryChallan shipmentDeliveryChallan = _db.Shipment_DeliveryChallan.Find(vmShipmentDeliveryChallan.ID);
            shipmentDeliveryChallan.Date = vmShipmentDeliveryChallan.Date;
            shipmentDeliveryChallan.TruckNo = vmShipmentDeliveryChallan.TruckNo;
            shipmentDeliveryChallan.Common_CNFSupplierFk = vmShipmentDeliveryChallan.Common_CNFSupplierFk;
            shipmentDeliveryChallan.Common_TransporterSupplierFk = vmShipmentDeliveryChallan.Common_TransporterSupplierFk;
            shipmentDeliveryChallan.DLNo = vmShipmentDeliveryChallan.DLNo;
            shipmentDeliveryChallan.DriverName = vmShipmentDeliveryChallan.DriverName;
            shipmentDeliveryChallan.GatePassDate = vmShipmentDeliveryChallan.GatePassDate;
            shipmentDeliveryChallan.GatePassNo = vmShipmentDeliveryChallan.GatePassNo;
            shipmentDeliveryChallan.ContactPersonMobile = vmShipmentDeliveryChallan.ContactPersonMobile;
            shipmentDeliveryChallan.ContactPersonName = vmShipmentDeliveryChallan.ContactPersonName;
            shipmentDeliveryChallan.DriverMobile = vmShipmentDeliveryChallan.DriverMobile;
            shipmentDeliveryChallan.InTime = vmShipmentDeliveryChallan.InTime;
            shipmentDeliveryChallan.UserID = shipmentDeliveryChallan.UserID;
            shipmentDeliveryChallan.LockNo = vmShipmentDeliveryChallan.LockNo;
            shipmentDeliveryChallan.OutTime = vmShipmentDeliveryChallan.OutTime;
            shipmentDeliveryChallan.Remarks = vmShipmentDeliveryChallan.Remarks;
            shipmentDeliveryChallan.ShipmentUnloadingPortFk = vmShipmentDeliveryChallan.ShipmentUnloadingPortFk;


            if (await _db.SaveChangesAsync() > 0)
            {

                result = shipmentDeliveryChallan.ID;
            }
            return result;
        }
        public async Task<int> ShipmentDeliveryChallanDelete(int id)
        {
            int result = -1;

            if (id != 0)
            {
                Shipment_DeliveryChallan shipmentDeliveryChallan = _db.Shipment_DeliveryChallan.Find(id);
                shipmentDeliveryChallan.Active = false;
                List<Shipment_DeliveryChallanSlave> challanSlaveList = new List<Shipment_DeliveryChallanSlave>();
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = shipmentDeliveryChallan.ID;
                    challanSlaveList = _db.Shipment_DeliveryChallanSlave.Where(x => x.Shipment_DeliveryChallanFk == shipmentDeliveryChallan.ID).ToList();

                }
                if (challanSlaveList.Any())
                {
                    challanSlaveList.ForEach(x => x.Active = false);
                    await _db.SaveChangesAsync();
                }
            }
            return result;
        }

        public IEnumerable<VMShipmentDeliveryChallanSlave> ShipmentDeliveryChallanSlaveGet(VMShipmentDeliveryChallanSlave model)
        {
            model.DataListSlave = (from t1 in _db.Shipment_DeliveryChallanSlave
                                   where t1.Active && t1.Shipment_DeliveryChallanFk == model.Shipment_DeliveryChallanFk
                                   select new VMShipmentDeliveryChallanSlave
                                   {
                                       ID = t1.ID,
                                       Shipment_DeliveryChallanFk = t1.Shipment_DeliveryChallanFk
                                   }).OrderByDescending(x => x.ID).AsEnumerable();

            return model.DataListSlave;
        }
        public async Task<int> ShipmentDeliveryChallanSlaveAdd(VMShipmentDeliveryChallanSlave vmShipmentDCS, VMStyleInvoiced vmStyleInvoiced)
        {
            var result = -1;

            List<Shipment_DeliveryChallanSlave> modelList = new List<Shipment_DeliveryChallanSlave>();
            var styleInvoicedList = vmStyleInvoiced.vmStyleInvoicedList.Where(x => x.DeliveryChallanQuantity > 0 && x.Flag == true).ToList();
            if (styleInvoicedList.Any())
            {
                for (int i = 0; i < styleInvoicedList.Count(); i++)
                {
                    Shipment_DeliveryChallanSlave shipmentInvoiceSlave = new Shipment_DeliveryChallanSlave
                    {
                        Shipment_DeliveryChallanFk = vmShipmentDCS.Shipment_DeliveryChallanFk,
                        Merchandising_StyleFk = styleInvoicedList[i].Merchandising_StyleFk,
                        Merchandising_StyleShipmentScheduleFk = styleInvoicedList[i].Merchandising_StyleShipmentScheduleFk,
                        Shipment_InstructionSlaveFk = styleInvoicedList[i].Shipment_InstructionSlaveFk,
                        Shipment_InvoiceSlaveFk = styleInvoicedList[i].Shipment_InvoiceSlaveFk,
                        DeliveryChallanQuantity = styleInvoicedList[i].DeliveryChallanQuantity,
                        UserID = vmShipmentDCS.UserID


                    };
                    modelList.Add(shipmentInvoiceSlave);
                }
                _db.Shipment_DeliveryChallanSlave.AddRange(modelList);
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = 1;
                }
            }
            return result;
        }
        public async Task<int> ShipmentDeliveryChallanSlaveEdit(VMShipmentDeliveryChallanSlave vmShipmentDeliveryChallanSlave)
        {
            var result = -1;
            //to select Accountining_Chart_Two data.....
            Shipment_DeliveryChallanSlave shipmentDeliveryChallanSlave = _db.Shipment_DeliveryChallanSlave.Find(vmShipmentDeliveryChallanSlave.ID);
            shipmentDeliveryChallanSlave.Shipment_DeliveryChallanFk = vmShipmentDeliveryChallanSlave.Shipment_DeliveryChallanFk;

            if (await _db.SaveChangesAsync() > 0)
            {
                result = shipmentDeliveryChallanSlave.ID;
            }
            return result;
        }
        public async Task<int> ShipmentDeliveryChallanSlaveDelete(int id)
        {
            int result = -1;

            if (id != 0)
            {
                Shipment_DeliveryChallanSlave shipmentDeliveryChallanSlave = _db.Shipment_DeliveryChallanSlave.Find(id);
                shipmentDeliveryChallanSlave.Active = false;
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = shipmentDeliveryChallanSlave.ID;
                }
            }
            return result;
        }

        public async Task<VMShipmentInvoiceSlave> ShipmentInvoiceGet(int id)
        {
            VMShipmentInvoiceSlave vMShipmentInvoiceSlave = new VMShipmentInvoiceSlave();
            vMShipmentInvoiceSlave = await Task.Run(() => (from t1 in _db.Shipment_ShipmentInvoice
                                                           join t2 in _db.Common_CountryPort on t1.Shipment_PortOfLoadingFk equals t2.ID
                                                           join t3 in _db.Shipment_TermsOfShipment on t1.Shipment_TermsOfShipmentFK equals t3.ID


                                                           where t1.Active && t1.ID == id
                                                           select new VMShipmentInvoiceSlave
                                                           {
                                                               ID = t1.ID,
                                                               InvoiceDate = t1.InvoiceDate,
                                                               InvoiceCID = t1.InvoiceCID,
                                                               Shipment_InvoiceFk = t1.ID,
                                                               BLDate = t1.BLDate,
                                                               BLNo = t1.BLNo,

                                                               ErcNo = t1.ErcNo,
                                                               EstimatedReceivedDate = t1.EstimatedReceivedDate,
                                                               ExpDate = t1.ExpDate,
                                                               ExpNo = t1.ExpNo,
                                                               //GWT = t1.GWT,

                                                               InvoiceIdNo = t1.InvoiceIdNo,
                                                               TermsOfShipment = t3.Name,
                                                               AdjustedNote = t1.AdjustedNote,
                                                               AdjustedValue = t1.AdjustedValue,

                                                               Shipment_PortOfLoadingFk = t1.Shipment_PortOfLoadingFk,
                                                               PortOfLoading = t2.Name,
                                                               ShippedBy = t1.ShippedBy,
                                                               ShippingMarks = t1.ShippingMarks,
                                                               UDInfo = (from x in _db.Commercial_ECI
                                                                         join y in _db.Commercial_UD on x.Commercial_UDFk equals y.ID
                                                                         join z in _db.Common_BuyerBank on x.Common_BuyerBankFK equals z.ID
                                                                         join m in _db.Common_CompanyBank on x.Common_CompanyBankFK equals m.ID
                                                                         join n in _db.Common_Buyer on x.Common_BuyerFK equals n.ID
                                                                         //join n in _db.commer on x equals n.


                                                                         where x.Active == true && y.Active == true && z.Active == true && m.Active == true && n.Active == true

                                                                         select new UDInfo
                                                                         {
                                                                             UDNo = y.Ref,
                                                                             VmApplicantBank = new Home.VmApplicantBank { Name = z.Name, Address = z.Address, SwiftCode = z.SwiftCode },
                                                                             VmReceivingBank = new Home.VmReceivingBank { Name = z.Name, Address = z.Address, SwiftCode = z.SwiftCode },
                                                                             VMBuyer = new Home.VMBuyer { Name = n.Name, Address = n.Address, Email = n.Email, Phone = n.Phone }
                                                                         }).FirstOrDefault(),

                                                               Distination = (from x in _db.Shipment_ShipmentInvoiceSlave
                                                                              join y in _db.Merchandising_StyleShipmentSchedule on x.Merchandising_StyleShipmentScheduleFk equals y.ID
                                                                              join z in _db.Common_CountryPort on y.Common_CountryPortFK equals z.ID
                                                                              join o in _db.Common_Country on z.Common_CountryFK equals o.ID
                                                                              where y.Active == true && x.Active == true && x.Shipment_InvoiceFk == t1.ID
                                                                              select new Distination
                                                                              {
                                                                                  DistinationCountry = o.Name,
                                                                                  DistinationPort = y.DestNo +" ("+ z.Name + ")"
                                                                              }).FirstOrDefault(),

                                                               BuyerNotifyPartys = (from x in _db.Shipment_InvoicedBuyerNotifyParty
                                                                                    join y in _db.Common_BuyerNotifyParty on x.Common_BuyerNotifyPartyFk equals y.ID
                                                                                    join z in _db.Common_Country on y.Common_CountryFK equals z.ID

                                                                                    where x.Shipment_InvoiceFk == t1.ID
                                                                                    select new VMBuyerNotifyParty
                                                                                    {
                                                                                        Name = y.Name,
                                                                                        Address = y.Address,
                                                                                        Email = y.Email,
                                                                                        ContactPerson = y.ContactPerson,
                                                                                        Phone = y.Phone,
                                                                                        CountryName = z.Name
                                                                                    }).ToList(),
                                                               VMCompanySetup = (from y in _db.Common_CompanySetup

                                                                                 where y.Active == true
                                                                                 select new VMCompanySetup
                                                                                 {
                                                                                     Name = y.Name,
                                                                                     Address = y.Address,
                                                                                     Email = y.Email,
                                                                                     ContactPerson = y.ContactPerson,
                                                                                     Phone = y.Phone,

                                                                                 }).ToList(),

                                                               ShippedByList = new SelectList(GetShippedByDropdown(), "Value", "Text"),
                                                               TermsOfShipmentList = new SelectList(GetTermsOfShipmentDropDown(), "Value", "Text"),
                                                               //ShipmentInstructionList = new SelectList(_service.ShipmentInstructionDropDownList(), "Value", "Text"),
                                                               CommonBuyerList = new SelectList(CommonBuyerDropDownList(), "Value", "Text"),
                                                               // CommonCountryPortList = new SelectList(_service.CommonCountryPortDropDownList(), "Value", "Text"),

                                                           }).FirstOrDefault());

            //join is error
            vMShipmentInvoiceSlave.DataListSlave = await Task.Run(() => (from t1 in _db.Shipment_ShipmentInvoiceSlave
                                                                         join t2 in _db.Shipment_ShipmentInvoice on t1.Shipment_InvoiceFk equals t2.ID
                                                                         join t3 in _db.Shipment_ShipmentInstructionSlave on t1.Shipment_InstructionSlaveFk equals t3.ID
                                                                         join t4 in _db.Shipment_ShipmentInstruction on t3.Shipment_ShipmentInstructionFk equals t4.ID
                                                                         join t5 in _db.Merchandising_StyleShipmentSchedule on t1.Merchandising_StyleShipmentScheduleFk equals t5.ID
                                                                         join t6 in _db.Merchandising_Style on t1.Merchandising_StyleFk equals t6.ID
                                                                         join t7 in _db.Merchandising_BuyerOrder on t6.Merchandising_BuyerOrderFK equals t7.ID
                                                                         join t8 in _db.Common_FinishItem on t6.Common_FinishItemFK equals t8.ID
                                                                         where t1.Active && t1.Shipment_InvoiceFk == id
                                                                         && t2.Active && t3.Active && t4.Active && t5.Active && t6.Active

                                                                         select new VMShipmentInvoiceSlave
                                                                         {
                                                                             ID = t1.ID,
                                                                             Shipment_InvoiceFk = t2.ID,
                                                                             Shipment_ShipmentInstructionFk = t1.Shipment_InstructionSlaveFk,
                                                                             BuyerPO = t7.BuyerPO,
                                                                             DescriptionOfGoods = t8.Name + ", " + t6.Fabrication,
                                                                             PackQuantity =decimal.Round(t1.InvoicedQuantity),
                                                                             PcsQuantity =decimal.Round(decimal.Multiply(t1.InvoicedQuantity,  t6.PackPieceQty)),
                                                                             Style = t7.BuyerPO + "/" + t6.CID + "/" + t6.StyleName,
                                                                             PackPrice = t6.PackPrice,
                                                                             TotalPrice =decimal.Round(decimal.Multiply(t1.InvoicedQuantity, t6.PackPrice),2),
                                                                             CTNQuantity =decimal.Round(decimal.Divide(t1.InvoicedQuantity, t6.PackInCTN))

                                                                         }).OrderByDescending(x => x.ID).AsEnumerable());


            return vMShipmentInvoiceSlave;
        }


        public async Task<VMShipmentInvoiceSlave> ShipmentInvoicePrintGet(int id)
        {
            VMShipmentInvoiceSlave vMShipmentInvoiceSlave = new VMShipmentInvoiceSlave();
            vMShipmentInvoiceSlave = await Task.Run(() => (from t1 in _db.Shipment_ShipmentInvoice
                                                           join t2 in _db.Common_CountryPort on t1.Shipment_PortOfLoadingFk equals t2.ID
                                                           join t3 in _db.Shipment_TermsOfShipment on t1.Shipment_TermsOfShipmentFK equals t3.ID


                                                           where t1.Active && t1.ID == id
                                                           select new VMShipmentInvoiceSlave
                                                           {
                                                               ID = t1.ID,
                                                               InvoiceDate = t1.InvoiceDate,
                                                               InvoiceCID = t1.InvoiceCID,
                                                               Shipment_InvoiceFk = t1.ID,
                                                               BLDate = t1.BLDate,
                                                               BLNo = t1.BLNo,

                                                               ErcNo = t1.ErcNo,
                                                               EstimatedReceivedDate = t1.EstimatedReceivedDate,
                                                               ExpDate = t1.ExpDate,
                                                               ExpNo = t1.ExpNo,
                                                               //GWT = t1.GWT,

                                                               InvoiceIdNo = t1.InvoiceIdNo,
                                                               TermsOfShipment = t3.Name,
                                                               AdjustedNote = t1.AdjustedNote,
                                                               AdjustedValue = t1.AdjustedValue,

                                                               Shipment_PortOfLoadingFk = t1.Shipment_PortOfLoadingFk,
                                                               PortOfLoading = t2.Name,
                                                               ShippedBy = t1.ShippedBy,
                                                               ShippingMarks = t1.ShippingMarks,
                                                               BuyerNotifyPartys = (from x in _db.Shipment_InvoicedBuyerNotifyParty
                                                                                    join y in _db.Common_BuyerNotifyParty on x.Common_BuyerNotifyPartyFk equals y.ID
                                                                                    join z in _db.Common_Country on y.Common_CountryFK equals z.ID

                                                                                    where x.Shipment_InvoiceFk == t1.ID
                                                                                    select new VMBuyerNotifyParty
                                                                                    {
                                                                                        Name = y.Name,
                                                                                        Address = y.Address,
                                                                                        Email = y.Email,
                                                                                        ContactPerson = y.ContactPerson,
                                                                                        Phone = y.Phone,
                                                                                        CountryName = z.Name
                                                                                    }).ToList(),
                                                               VMCompanySetup = (from y in _db.Common_CompanySetup

                                                                                 where y.Active == true
                                                                                 select new VMCompanySetup
                                                                                 {
                                                                                     Name = y.Name,
                                                                                     Address = y.Address,
                                                                                     Email = y.Email,
                                                                                     ContactPerson = y.ContactPerson,
                                                                                     Phone = y.Phone,

                                                                                 }).ToList(),
                                                               Distination = (from x in _db.Shipment_ShipmentInvoiceSlave
                                                                              join y in _db.Merchandising_StyleShipmentSchedule on x.Merchandising_StyleShipmentScheduleFk equals y.ID
                                                                              join z in _db.Common_CountryPort on y.Common_CountryPortFK equals z.ID
                                                                              join o in _db.Common_Country on z.Common_CountryFK equals o.ID
                                                                              where y.Active == true && x.Active == true && x.Shipment_InvoiceFk == t1.ID
                                                                              select new Distination
                                                                              {
                                                                                  DistinationCountry = o.Name,
                                                                                  DistinationPort =y.DestNo +" ("+ z.Name+")"
                                                                              }).FirstOrDefault(),

                                                               ShippedByList = new SelectList(GetShippedByDropdown(), "Value", "Text"),
                                                               TermsOfShipmentList = new SelectList(GetTermsOfShipmentDropDown(), "Value", "Text"),
                                                               //ShipmentInstructionList = new SelectList(_service.ShipmentInstructionDropDownList(), "Value", "Text"),
                                                               CommonBuyerList = new SelectList(CommonBuyerDropDownList(), "Value", "Text"),
                                                               // CommonCountryPortList = new SelectList(_service.CommonCountryPortDropDownList(), "Value", "Text"),

                                                           }).FirstOrDefault());

            //join is error
            vMShipmentInvoiceSlave.DataListSlave = await Task.Run(() => (from t1 in _db.Shipment_ShipmentInvoiceSlave
                                                                         join t2 in _db.Shipment_ShipmentInvoice on t1.Shipment_InvoiceFk equals t2.ID
                                                                         join t3 in _db.Shipment_ShipmentInstructionSlave on t1.Shipment_InstructionSlaveFk equals t3.ID
                                                                         join t4 in _db.Shipment_ShipmentInstruction on t3.Shipment_ShipmentInstructionFk equals t4.ID
                                                                         join t5 in _db.Merchandising_StyleShipmentSchedule on t1.Merchandising_StyleShipmentScheduleFk equals t5.ID
                                                                         join t6 in _db.Merchandising_Style on t1.Merchandising_StyleFk equals t6.ID
                                                                         join t7 in _db.Merchandising_BuyerOrder on t6.Merchandising_BuyerOrderFK equals t7.ID
                                                                         join t8 in _db.Common_FinishItem on t6.Common_FinishItemFK equals t8.ID
                                                                         where t1.Active && t2.Active && t3.Active && t4.Active && t5.Active && t1.Shipment_InvoiceFk == id

                                                                         select new VMShipmentInvoiceSlave
                                                                         {
                                                                             ID = t1.ID,
                                                                             Shipment_InvoiceFk = t2.ID,
                                                                             Shipment_ShipmentInstructionFk = t1.Shipment_InstructionSlaveFk,
                                                                             BuyerPO = t7.BuyerPO,
                                                                             DescriptionOfGoods = t8.Name + ", " + t6.Fabrication,
                                                                             PackQuantity =decimal.Round(t1.InvoicedQuantity),
                                                                             PcsQuantity =decimal.Round(decimal.Multiply(t1.InvoicedQuantity , t6.PackPieceQty)),
                                                                             Style = t7.BuyerPO + "/" + t6.CID + "/" + t6.StyleName,

                                                                             PackPrice = decimal.Round(t6.PackPrice,2),
                                                                             TotalPrice =decimal.Round(decimal.Multiply(t1.InvoicedQuantity, t6.PackPrice),2),
                                                                             CTNQuantity =decimal.Round(decimal.Divide(t1.InvoicedQuantity, t6.PackInCTN))
                                                                         }).OrderByDescending(x => x.ID).AsEnumerable());


            return vMShipmentInvoiceSlave;
        }

        public IEnumerable<VMShipmentInvoice> ShipmentInvoicesGet()
        {
            VMShipmentInvoice model = new VMShipmentInvoice();
            model.DataList = (from t1 in _db.Shipment_ShipmentInvoice
                              join t2 in _db.Common_CountryPort on t1.Shipment_PortOfLoadingFk equals t2.ID
                              join t3 in _db.Shipment_TermsOfShipment on t1.Shipment_TermsOfShipmentFK equals t3.ID
                              where t1.Active
                              select new VMShipmentInvoice
                              {
                                  ID = t1.ID,
                                  InvoiceDate = t1.InvoiceDate,
                                  InvoiceCID = t1.InvoiceCID,
                                  BLDate = t1.BLDate,
                                  BLNo = t1.BLNo,
                                  //CBM = t1.CBM,
                                  ErcNo = t1.ErcNo,
                                  ExpDate = t1.ExpDate,
                                  ExpNo = t1.ExpNo,
                                  //GWT = t1.GWT,
                                  ShippedBy = t1.ShippedBy,
                                  ShippingMarks = t1.ShippingMarks,
                                  TermsOfShipment = t3.Name,
                                  BuyerNotifyPartys = (from x in _db.Shipment_InvoicedBuyerNotifyParty
                                                       join y in _db.Common_BuyerNotifyParty on x.Common_BuyerNotifyPartyFk equals y.ID
                                                       join z in _db.Common_Country on y.Common_CountryFK equals z.ID

                                                       where x.Shipment_InvoiceFk == t1.ID
                                                       select new VMBuyerNotifyParty
                                                       {
                                                           Name = y.Name,
                                                           Address = y.Address,
                                                           Email = y.Email,
                                                           ContactPerson = y.ContactPerson,
                                                           Phone = y.Phone,
                                                           CountryName = z.Name
                                                       }).ToList(),

                                  //error in join
                                  TotalGenerateClass = (from st1 in _db.Shipment_ShipmentInvoiceSlave
                                                            //join st2 in _db.Commercial_Invoice on t1.Commercial_InvoiceFk equals t2.ID
                                                            //join st3 in _db.Shipment_ShipmentInstruction on st1.Shipment_ShipmentInstructionFk equals st3.ID
                                                            //join st3 in _db.Shipment_ShipmentInstructionSlave on st1.Shipment_InstructionSlaveFk equals st3.Shipment_ShipmentInstructionFk
                                                            //join st0 in _db.Merchandising_StyleShipmentSchedule on st3.Merchandising_StyleShipmentScheduleFk equals st0.ID
                                                        join st4 in _db.Merchandising_Style on st1.Merchandising_StyleFk equals st4.ID
                                                        where st1.Shipment_InvoiceFk == t1.ID && st1.Active
                                                        select new TotalGenerateClass
                                                        {
                                                            TotalOrderQty = (st1.InvoicedQuantity * st4.PackPieceQty),
                                                            //TotalCtnQty = t0.CtnQty,
                                                            TotalPackQty = st1.InvoicedQuantity,
                                                            TotalPrice = (st4.PackPrice * st1.InvoicedQuantity),
                                                        }).ToList()


                              }).OrderByDescending(x => x.ID).AsEnumerable();

            return model.DataList;
        }
        public async Task<int> CommonBuyerNotifyPartyAdd(VMShipmentInvoiceSlave model)
        {
            var result = -1;
            List<Shipment_InvoicedBuyerNotifyParty> invoicedBuyerNotifyPartyList = new List<Shipment_InvoicedBuyerNotifyParty>();

            if (model.BuyerNotifyPartysFk.Any())
            {
                foreach (var item in model.BuyerNotifyPartysFk)
                {
                    Shipment_InvoicedBuyerNotifyParty invoicedBuyerNotifyParty = new Shipment_InvoicedBuyerNotifyParty
                    {
                        Common_BuyerNotifyPartyFk = Convert.ToInt32(item),
                        Shipment_InvoiceFk = model.Shipment_InvoiceFk,
                        UserID = model.UserID
                    };
                    invoicedBuyerNotifyPartyList.Add(invoicedBuyerNotifyParty);
                }
            }


            //if (model.BuyerNotifyPartys.Any())
            //{
            //    foreach (var item in model.BuyerNotifyPartysFk)
            //    {
            //        Shipment_InvoicedBuyerNotifyParty invoicedBuyerNotifyParty = new Shipment_InvoicedBuyerNotifyParty
            //        {
            //            Common_BuyerNotifyPartyFk = Convert.ToInt32(item),
            //            Shipment_InvoiceFk = model.Shipment_InvoiceFk,
            //            UserID = model.UserID
            //        };
            //        invoicedBuyerNotifyPartyList.Add(invoicedBuyerNotifyParty);
            //    }
            //}
            _db.Shipment_InvoicedBuyerNotifyParty.AddRange(invoicedBuyerNotifyPartyList);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = 1;
            }
            return result;
        }

        public async Task<int> ShipmentInvoiceAdd(VMShipmentInvoice model)
        {
            var result = -1;
            #region Genarate Invoice CID
            int totalInvoice = _db.Shipment_ShipmentInvoice.Count();

            if (totalInvoice == 0)
            {
                totalInvoice = 1;
            }
            else
            {
                totalInvoice++;
            }
            int year = DateTime.Now.Year;
            var newString = totalInvoice.ToString().PadLeft(5, '0');

            string invoiceCID = "RFTL-" + year + "-" + newString;
            #endregion
            Shipment_ShipmentInvoice shipmentInvoice = new Shipment_ShipmentInvoice
            {
                InvoiceDate = DateTime.Now,
                InvoiceCID = invoiceCID,

                BLDate = model.BLDate,
                BLNo = model.BLNo,
                AdjustedNote = model.AdjustedNote,
                AdjustedValue = model.AdjustedValue,

                ErcNo = model.ErcNo,
                EstimatedReceivedDate = model.EstimatedReceivedDate,
                ExpDate = model.ExpDate,
                ExpNo = model.ExpNo,

                InvoiceIdNo = model.InvoiceIdNo,

                Shipment_PortOfLoadingFk = model.Shipment_PortOfLoadingFk,
                ShippedBy = model.ShippedBy,
                ShippingMarks = model.ShippingMarks,
                Shipment_TermsOfShipmentFK = model.Shipment_TermsOfShipmentFK,
                UserID = model.UserID,
                

            };
            _db.Shipment_ShipmentInvoice.Add(shipmentInvoice);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = shipmentInvoice.ID;
            }
            return result;
        }

        public async Task<int> ShipmentInvoiceEdit(VMShipmentInvoice vmShipmentInvoice)
        {
            var result = -1;
            //to select Accountining_Chart_Two data.....
            Shipment_ShipmentInvoice shipmentInvoice = _db.Shipment_ShipmentInvoice.Find(vmShipmentInvoice.ID);
            shipmentInvoice.InvoiceDate = vmShipmentInvoice.InvoiceDate;
            shipmentInvoice.InvoiceCID = vmShipmentInvoice.InvoiceCID;
            shipmentInvoice.UserID = vmShipmentInvoice.UserID;
            shipmentInvoice.Shipment_TermsOfShipmentFK = vmShipmentInvoice.Shipment_TermsOfShipmentFK;
            shipmentInvoice.ShippingMarks = vmShipmentInvoice.ShippingMarks;
            shipmentInvoice.ShippedBy = vmShipmentInvoice.ShippedBy;
            shipmentInvoice.Shipment_PortOfLoadingFk = vmShipmentInvoice.Shipment_PortOfLoadingFk;
            shipmentInvoice.InvoiceIdNo = vmShipmentInvoice.InvoiceIdNo;
            shipmentInvoice.InvoiceDate = vmShipmentInvoice.InvoiceDate;
            shipmentInvoice.ExpNo = vmShipmentInvoice.ExpNo;
            shipmentInvoice.ExpDate = vmShipmentInvoice.ExpDate;
            shipmentInvoice.ErcNo = vmShipmentInvoice.ErcNo;
            shipmentInvoice.BLNo = vmShipmentInvoice.BLNo;
            shipmentInvoice.BLDate = vmShipmentInvoice.BLDate;
            shipmentInvoice.AdjustedNote = vmShipmentInvoice.AdjustedNote;
            shipmentInvoice.AdjustedValue = vmShipmentInvoice.AdjustedValue;
            

            if (await _db.SaveChangesAsync() > 0)
            {
                result = vmShipmentInvoice.ID;
            }
            return result;
        }
        public async Task<int> ShipmentInvoiceDelete(int id)
        {
            int result = -1;

            if (id != 0)
            {
                Shipment_ShipmentInvoice shipmentInvoice = _db.Shipment_ShipmentInvoice.Find(id);
                shipmentInvoice.Active = false;

                if (await _db.SaveChangesAsync() > 0)
                {
                    result = shipmentInvoice.ID;
                }

            }
            return result;
        }

        public IEnumerable<VMShipmentInvoiceSlave> ShipmentInvoiceSlaveGet(VMShipmentInvoiceSlave model)
        {
            model.DataListSlave = (from t1 in _db.Shipment_ShipmentInvoiceSlave
                                   where t1.Active && t1.Shipment_InvoiceFk == model.Shipment_InvoiceFk
                                   select new VMShipmentInvoiceSlave
                                   {
                                       ID = t1.ID,
                                       Shipment_ShipmentInstructionFk = t1.Shipment_InstructionSlaveFk,
                                       Shipment_InvoiceFk = t1.Shipment_InvoiceFk
                                   }).OrderByDescending(x => x.ID).AsEnumerable();

            return model.DataListSlave;
        }
        public List<VMStyle> StyleGetByBuyer(int id)
        {

            var v = (from t1 in _db.Merchandising_Style
                     join t2 in _db.Merchandising_BuyerOrder on t1.Merchandising_BuyerOrderFK equals t2.ID
                     join t3 in _db.Common_Buyer on t2.Common_BuyerFK equals t3.ID
                     join t4 in _db.Common_FinishItem on t1.Common_FinishItemFK equals t4.ID

                     where t2.Common_BuyerFK == id && t1.Active
                     select new VMStyle()
                     {
                         ID = t1.ID,
                         CID = t2.BuyerPO + "/" + t1.CID + "/" + t4.Name,

                     }).OrderByDescending(a => a.ID).ToList();


            return v;
        }

        public List<VMStyle> nstructedStyleGetByBuyer(int id)
        {

            var v = (from t1 in _db.Merchandising_Style
                     join t2 in _db.Merchandising_BuyerOrder on t1.Merchandising_BuyerOrderFK equals t2.ID
                     join t3 in _db.Common_Buyer on t2.Common_BuyerFK equals t3.ID
                     join t4 in _db.Common_FinishItem on t1.Common_FinishItemFK equals t4.ID
                     join t5 in _db.Shipment_ShipmentInstructionSlave on t1.ID equals t5.Merchandising_StyleFk

                     where t2.Common_BuyerFK == id && t1.Active
                     select new VMStyle()
                     {
                         ID = t1.ID,
                         CID = t2.BuyerPO + "/" + t1.CID + "/" + t4.Name,

                     }).Distinct().OrderByDescending(a => a.ID).ToList();


            return v;
        }
        public List<VMStyle> BuyerNotifyPartyByIdGet(int id)
        {

            var v = (from t1 in _db.Common_BuyerNotifyParty

                     where t1.Common_BuyerFK == id && t1.Active
                     select new VMStyle()
                     {
                         ID = t1.ID,
                         NotifyPartyName = t1.Name,
                     }).OrderByDescending(a => a.ID).ToList();


            return v;
        }

        public async Task<int> ShipmentInvoiceSlaveAdd(VMShipmentInvoiceSlave vmShipmentInvoiceSlave, VMStyleInstruction vmStyleInstruction)
        {
            var result = -1;
            List<Shipment_ShipmentInvoiceSlave> modelList = new List<Shipment_ShipmentInvoiceSlave>();
            var styleInstructionList = vmStyleInstruction.StyleInstructionList.Where(x => x.InstructionQuantity > 0 && x.Flag == true).ToList();
            if (styleInstructionList.Any())
            {
                for (int i = 0; i < styleInstructionList.Count(); i++)
                {
                    Shipment_ShipmentInvoiceSlave shipmentInvoiceSlave = new Shipment_ShipmentInvoiceSlave
                    {
                        Shipment_InstructionSlaveFk = styleInstructionList[i].ID,
                        InvoicedQuantity = styleInstructionList[i].InstructionQuantity,

                        Merchandising_StyleFk = styleInstructionList[i].Merchandising_StyleFk,
                        Shipment_InvoiceFk = vmShipmentInvoiceSlave.Shipment_InvoiceFk,
                        Merchandising_StyleShipmentScheduleFk = styleInstructionList[i].Merchandising_StyleShipmentScheduleFk,
                        UserID = styleInstructionList[i].UserID,


                    };
                    modelList.Add(shipmentInvoiceSlave);
                    //_db.Shipment_ShipmentInstructionSlave.Add(shipmentInstructionSlave);

                }
                _db.Shipment_ShipmentInvoiceSlave.AddRange(modelList);
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = 1;
                }
            }

            return result;
        }
        public async Task<int> ShipmentInvoiceSlaveEdit(VMShipmentInvoiceSlave vmShipmentInvoiceSlave)
        {
            var result = -1;
            //to select Accountining_Chart_Two data.....
            Shipment_ShipmentInvoiceSlave shipmentInvoiceSlave = _db.Shipment_ShipmentInvoiceSlave.Find(vmShipmentInvoiceSlave.ID);
            shipmentInvoiceSlave.Shipment_InvoiceFk = vmShipmentInvoiceSlave.Shipment_InvoiceFk;
            //shipmentInvoiceSlave.Shipment_ShipmentInstructionFk = vmShipmentInvoiceSlave.Shipment_ShipmentInstructionFk;

            if (await _db.SaveChangesAsync() > 0)
            {
                result = shipmentInvoiceSlave.ID;
            }
            return result;
        }
        public async Task<int> ShipmentInvoiceSlaveDelete(int id)
        {
            int result = -1;

            if (id != 0)
            {
                Shipment_ShipmentInvoiceSlave shipmentInvoiceSlave = _db.Shipment_ShipmentInvoiceSlave.Find(id);
                shipmentInvoiceSlave.Active = false;
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = shipmentInvoiceSlave.Shipment_InvoiceFk;
                }
            }
            return result;
        }

        public async Task<VMShipmentBillOfExchangeSlave> ShipmentBillOfExchangeGet(int id)
        {
            VMShipmentBillOfExchangeSlave vmShipmentBill = new VMShipmentBillOfExchangeSlave();
            vmShipmentBill = await Task.Run(() => (from t1 in _db.Shipment_BillOfExchange
                                                   join t2 in _db.Commercial_ECI on t1.Commercial_ECIFK equals t2.ID
                                                   join t3 in _db.Common_Buyer on t2.Common_BuyerFK equals t3.ID

                                                   where t1.Active && t1.ID == id
                                                   select new VMShipmentBillOfExchangeSlave
                                                   {
                                                       IsFinal = t1.IsFinal,
                                                       BuyerName = t3.Name,
                                                       ECINO = t2.ECINo,
                                                       ID = t1.ID,
                                                       BoEDate = t1.BoEDate,
                                                       BoECID = t1.BoECID,
                                                       Shipment_BillOfExchangeFk = t1.ID,
                                                       Commercial_ECIFK = t1.Commercial_ECIFK,
                                                       CourierDate = t1.CourierDate,
                                                       CourierNO = t1.CourierNO,
                                                       Description = t1.Description,
                                                       FDBCDate = t1.FDBCDate,
                                                       FDBCNO = t1.FDBCNO,
                                                       Remarks = t1.Remarks,
                                                       ReceivedType = t1.ReceivedType,
                                                       ShipmentInvoiceList = new SelectList(ShipmentInvoiceForBillOfExchangeDropDownList(), "Value", "Text")
                                                   }).FirstOrDefault());
            if (vmShipmentBill != null)
            {
                //error in join
                vmShipmentBill.DataListSlave = await Task.Run(() => (from t1 in _db.Shipment_BillOfExchangeSlave
                                                                     join t3 in _db.Shipment_ShipmentInvoice on t1.Shipment_ShipmentInvoiceFk equals t3.ID
                                                                     join t4 in _db.Shipment_ShipmentInvoiceSlave on t3.ID equals t4.Shipment_InvoiceFk
                                                                     //join t5 in _db.Shipment_ShipmentInstruction on t4.Shipment_InstructionSlaveFk equals t5.ID
                                                                     //join t6 in _db.Shipment_ShipmentInstructionSlave on t5.ID equals t6.Shipment_ShipmentInstructionFk
                                                                     //join t7 in _db.Merchandising_StyleShipmentSchedule on t6.Merchandising_StyleShipmentScheduleFk equals t7.ID
                                                                     join t8 in _db.Merchandising_Style on t4.Merchandising_StyleFk equals t8.ID
                                                                     where t1.Active && t1.Shipment_BillOfExchangeFk == id
                                                                     group new { t1, t4, t3, t8 } by new { t3.InvoiceCID } into Group
                                                                     where Group.Count() > 0
                                                                     select new VMShipmentBillOfExchangeSlave
                                                                     {
                                                                         //Merchandising_StyleFk = t1.Merchandising_StyleFk,
                                                                         ID = Group.First().t1.ID,
                                                                         Shipment_BillOfExchangeFk = Group.First().t1.Shipment_BillOfExchangeFk,
                                                                         Shipment_InvoiceFk = Group.First().t1.Shipment_ShipmentInvoiceFk,
                                                                         InvoiceNo = Group.First().t3.InvoiceCID,
                                                                         //StyleName = t4.StyleName + " " + t4.Fabrication,
                                                                         InvoiceDate = Group.First().t3.InvoiceDate,
                                                                         Value = Group.Sum(x => x.t4.InvoicedQuantity * x.t8.PackPrice)

                                                                     }).OrderByDescending(x => x.ID).AsEnumerable());

            }

            return vmShipmentBill;
        }

        public async Task<VMShipmentBillOfExchange> ShipmentBillOfExchangesGetSingle(int id)
        {
            VMShipmentBillOfExchange model = new VMShipmentBillOfExchange();
            model = await Task.Run(() => (from t1 in _db.Shipment_BillOfExchange
                                          join t2 in _db.Commercial_ECI on t1.Commercial_ECIFK equals t2.ID
                                          join t3 in _db.Common_Buyer on t2.Common_BuyerFK equals t3.ID
                                          where t1.Active && t1.ID == id
                                          select new VMShipmentBillOfExchange
                                          {
                                              IsFinal = t1.IsFinal,
                                              BuyerName = t3.Name,
                                              ID = t1.ID,
                                              BoEDate = t1.BoEDate,
                                              BoECID = t1.BoECID,
                                              ECINO = t2.ECINo,
                                              Commercial_ECIFK = t1.Commercial_ECIFK,
                                              CourierDate = t1.CourierDate,
                                              CourierNO = t1.CourierNO,
                                              Description = t1.Description,
                                              FDBCDate = t1.FDBCDate,
                                              FDBCNO = t1.FDBCNO,
                                              Remarks = t1.Remarks,
                                              ReceivedType = t1.ReceivedType,
                                              BillValue = (from x in _db.Shipment_BillOfExchangeSlave
                                                           join y in _db.Shipment_ShipmentInvoiceSlave on x.Shipment_ShipmentInvoiceFk equals y.Shipment_InvoiceFk
                                                           join z in _db.Merchandising_Style on y.Merchandising_StyleFk equals z.ID
                                                           where x.Active == true && x.Shipment_BillOfExchangeFk == t1.ID && y.Active == true
                                                           select y.InvoicedQuantity * z.PackPrice).DefaultIfEmpty(0).Sum()
                                          }).FirstOrDefault());
            return model;
        }

        public async Task<VMShipmentBillOfExchangeSlave> ShipmentBillOfExchangePrintGet(int id)
        {
            VMShipmentBillOfExchangeSlave vmShipmentBill = new VMShipmentBillOfExchangeSlave();
            vmShipmentBill = await Task.Run(() => (from t1 in _db.Shipment_BillOfExchange
                                                   join t2 in _db.Commercial_ECI on t1.Commercial_ECIFK equals t2.ID
                                                   join t3 in _db.Common_Buyer on t2.Common_BuyerFK equals t3.ID
                                                   join t4 in _db.Common_BuyerBank on t2.Common_BuyerBankFK equals t4.ID
                                                   join t5 in _db.Common_BuyerBank on t2.Common_CompanyBankFK equals t5.ID


                                                   where t1.Active && t1.ID == id
                                                   select new VMShipmentBillOfExchangeSlave
                                                   {
                                                       IsFinal = t1.IsFinal,
                                                       BuyerName = t3.Name,
                                                       ECINO = t2.ECINo,
                                                       ID = t1.ID,
                                                       CompanyBank = t5.Name,
                                                       CompanyBankAddress = t5.Address,
                                                       BuyerBank = t4.Name,
                                                       BuyerBankAddress = t4.Address,

                                                       BoEDate = t1.BoEDate,

                                                       BoECID = t1.BoECID,
                                                       ECIDate = t2.ECIDate,
                                                       Shipment_BillOfExchangeFk = t1.ID,
                                                       Commercial_ECIFK = t1.Commercial_ECIFK,
                                                       CourierDate = t1.CourierDate,
                                                       CourierNO = t1.CourierNO,
                                                       Description = t1.Description,
                                                       FDBCDate = t1.FDBCDate,
                                                       FDBCNO = t1.FDBCNO,
                                                       Remarks = t1.Remarks,
                                                       ReceivedType = t1.ReceivedType,
                                                       ShipmentInvoiceList = new SelectList(ShipmentInvoiceDropDownList(), "Value", "Text")
                                                   }).FirstOrDefault());
            //error in join
            vmShipmentBill.DataListSlave = await Task.Run(() => (from t1 in _db.Shipment_BillOfExchangeSlave
                                                                 join t3 in _db.Shipment_ShipmentInvoice on t1.Shipment_ShipmentInvoiceFk equals t3.ID
                                                                 join t4 in _db.Shipment_ShipmentInvoiceSlave on t3.ID equals t4.Shipment_InvoiceFk
                                                                 //join t5 in _db.Shipment_ShipmentInstruction on t4.Shipment_InstructionSlaveFk equals t5.ID
                                                                 //join t6 in _db.Shipment_ShipmentInstructionSlave on t5.ID equals t6.Shipment_ShipmentInstructionFk
                                                                 //join t7 in _db.Merchandising_StyleShipmentSchedule on t6.Merchandising_StyleShipmentScheduleFk equals t7.ID
                                                                 join t8 in _db.Merchandising_Style on t4.Merchandising_StyleFk equals t8.ID
                                                                 where t1.Active && t1.Shipment_BillOfExchangeFk == id
                                                                 group new { t1, t4, t3, t8 } by new { t3.InvoiceCID } into Group

                                                                 select new VMShipmentBillOfExchangeSlave
                                                                 {
                                                                     //Merchandising_StyleFk = t1.Merchandising_StyleFk,
                                                                     ID = Group.First().t1.ID,
                                                                     Shipment_BillOfExchangeFk = Group.First().t1.Shipment_BillOfExchangeFk,
                                                                     Shipment_InvoiceFk = Group.First().t1.Shipment_ShipmentInvoiceFk,
                                                                     InvoiceNo = Group.First().t3.InvoiceCID,
                                                                     //StyleName = t4.StyleName + " " + t4.Fabrication,
                                                                     InvoiceDate = Group.First().t3.InvoiceDate,
                                                                     Value = decimal.Round(Group.Sum(x => x.t4.InvoicedQuantity * x.t8.PackPrice),2) 

                                                                 }).OrderByDescending(x => x.ID).AsEnumerable());

            return vmShipmentBill;
        }

        public IEnumerable<VMShipmentBillOfExchange> ShipmentBillOfExchangesGet()
        {
            VMShipmentBillOfExchange model = new VMShipmentBillOfExchange();
            model.DataList = (from t1 in _db.Shipment_BillOfExchange
                              join t2 in _db.Commercial_ECI on t1.Commercial_ECIFK equals t2.ID
                              join t3 in _db.Common_Buyer on t2.Common_BuyerFK equals t3.ID
                              where t1.Active
                              select new VMShipmentBillOfExchange
                              {
                                  IsFinal = t1.IsFinal,
                                  BuyerName = t3.Name,
                                  ID = t1.ID,
                                  BoEDate = t1.BoEDate,
                                  BoECID = t1.BoECID,
                                  ECINO = t2.ECINo,
                                  Commercial_ECIFK = t1.Commercial_ECIFK,
                                  CourierDate = t1.CourierDate,
                                  CourierNO = t1.CourierNO,
                                  Description = t1.Description,
                                  FDBCDate = t1.FDBCDate,
                                  FDBCNO = t1.FDBCNO,
                                  Remarks = t1.Remarks,
                                  ReceivedType = t1.ReceivedType,

                                  BillValue =decimal.Round((from x in _db.Shipment_BillOfExchangeSlave
                                               join z in _db.Shipment_ShipmentInvoiceSlave on x.Shipment_ShipmentInvoiceFk equals z.Shipment_InvoiceFk
                                               join a in _db.Merchandising_Style on z.Merchandising_StyleFk equals a.ID
                                               where x.Active && x.Shipment_BillOfExchangeFk == t1.ID
                                               select z.InvoicedQuantity * a.PackPrice).DefaultIfEmpty(0).Sum(),2)

                              }).OrderByDescending(x => x.ID).AsEnumerable();

            return model.DataList;
        }
        public async Task<int> ShipmentBillOfExchangeAdd(VMShipmentBillOfExchange vmShipmentBillOfExchange)
        {
            var result = -1;
            #region Genarate Bill of Exchange CID
            int totalBillOfExchange = _db.Shipment_BillOfExchange.Count();

            if (totalBillOfExchange == 0)
            {
                totalBillOfExchange = 1;
            }
            else
            {
                totalBillOfExchange++;
            }
            int year = DateTime.Now.Year;
            var newString = totalBillOfExchange.ToString().PadLeft(5, '0');

            string billOfExchangeCID = "BOE-" + year + "-" + newString;
            #endregion
            Shipment_BillOfExchange shipmentBillOfExchange = new Shipment_BillOfExchange
            {
                BoECID = billOfExchangeCID,
                BoEDate = DateTime.Now,

                Commercial_ECIFK = vmShipmentBillOfExchange.Commercial_ECIFK,
                CourierDate = vmShipmentBillOfExchange.CourierDate,
                CourierNO = vmShipmentBillOfExchange.CourierNO,
                FDBCDate = vmShipmentBillOfExchange.FDBCDate,
                Description = vmShipmentBillOfExchange.Description,
                FDBCNO = vmShipmentBillOfExchange.FDBCNO,
                ReceivedType = vmShipmentBillOfExchange.ReceivedType,
                Remarks = vmShipmentBillOfExchange.Remarks,
                UserID = vmShipmentBillOfExchange.UserID,

            };
            _db.Shipment_BillOfExchange.Add(shipmentBillOfExchange);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = shipmentBillOfExchange.ID;
            }
            return result;
        }
        public async Task<int> ShipmentBillOfExchangeEdit(VMShipmentBillOfExchange vmShipmentBillOfExchange)
        {
            var result = -1;
            //to select Accountining_Chart_Two data.....
            Shipment_BillOfExchange shipmentBillOfExchange = _db.Shipment_BillOfExchange.Find(vmShipmentBillOfExchange.ID);
            shipmentBillOfExchange.BoEDate = vmShipmentBillOfExchange.BoEDate;
            shipmentBillOfExchange.BoECID = vmShipmentBillOfExchange.BoECID;
            shipmentBillOfExchange.Commercial_ECIFK = vmShipmentBillOfExchange.Commercial_ECIFK;
            shipmentBillOfExchange.CourierDate = vmShipmentBillOfExchange.CourierDate;
            shipmentBillOfExchange.CourierNO = vmShipmentBillOfExchange.CourierNO;
            shipmentBillOfExchange.Description = vmShipmentBillOfExchange.Description;
            shipmentBillOfExchange.FDBCDate = vmShipmentBillOfExchange.FDBCDate;
            shipmentBillOfExchange.FDBCNO = vmShipmentBillOfExchange.FDBCNO;
            shipmentBillOfExchange.ReceivedType = vmShipmentBillOfExchange.ReceivedType;
            shipmentBillOfExchange.Remarks = vmShipmentBillOfExchange.Remarks;
            shipmentBillOfExchange.IsFinal = vmShipmentBillOfExchange.IsFinal;

            if (await _db.SaveChangesAsync() > 0)
            {
                result = shipmentBillOfExchange.ID;
            }
            return result;

        }

        public async Task<int> ChangeCompleteShipmentStyle(int styleId)
        {
            int result = -1;

            if (styleId != 0)
            {
                Merchandising_Style merchandisingStyle = _db.Merchandising_Style.Find(styleId);
                merchandisingStyle.IsShipmentComplete = true;

                if (await _db.SaveChangesAsync() > 0)
                {
                    result = merchandisingStyle.ID;
                }
            }
            return result;
        }
        public async Task<int> ShipmentBillOfExchangeDelete(int id)
        {
            int result = -1;

            if (id != 0)
            {
                Shipment_BillOfExchange shipmentBillOfExchange = _db.Shipment_BillOfExchange.Find(id);
                shipmentBillOfExchange.Active = false;

                if (await _db.SaveChangesAsync() > 0)
                {
                    result = shipmentBillOfExchange.ID;
                }
            }
            return result;
        }
        public async Task<int> ShipmentBillOfExchangeFinalize(int id, decimal billValue)
        {
            int result = -1;

            if (id != 0)
            {
                Shipment_BillOfExchange shipmentBillOfExchange = _db.Shipment_BillOfExchange.Find(id);
                shipmentBillOfExchange.IsFinal = true;

                var bId = (from sbe in _db.Shipment_BillOfExchange
                            join ceci in _db.Commercial_ECI on sbe.Commercial_ECIFK equals ceci.ID
                            join cb in _db.Common_Buyer on ceci.Common_BuyerFK equals cb.ID
                            join cc in _db.Common_Currency on ceci.Common_CurrencyFK equals cc.ID
                            where sbe.ID == id
                            select new { buyerId = cb.AccHeadID, billNo = sbe.BoECID, fdbcNo = sbe.FDBCNO, cc.ConversionRateToBDT }).FirstOrDefault();
                string BOEref = "BOE - " + "Bill No : " + bId.billNo + " FDBC No: " + bId.fdbcNo;
                decimal ConvertedAmount = Convert.ToDecimal(billValue * bId.ConversionRateToBDT);

                if (await _db.SaveChangesAsync() > 0)
                {
                    await integrationService.AccountingJournalPush(bId.buyerId, 431, ConvertedAmount, billValue, bId.ConversionRateToBDT, BOEref, (int)CostCenterEnum.General);
                    await integrationService.AccountingJournalPush(853, 76, ConvertedAmount, billValue, bId.ConversionRateToBDT, BOEref, (int)CostCenterEnum.General);
                    result = shipmentBillOfExchange.ID;
                }
            }
            return result;
        }
        public IEnumerable<VMShipmentBillOfExchangeSlave> ShipmentBillOfExchangeSlaveGet(VMShipmentBillOfExchangeSlave model)
        {
            model.DataListSlave = (from t1 in _db.Shipment_BillOfExchangeSlave
                                   where t1.Active && t1.Shipment_BillOfExchangeFk == model.Shipment_BillOfExchangeFk
                                   select new VMShipmentBillOfExchangeSlave
                                   {
                                       ID = t1.ID,
                                       Shipment_BillOfExchangeFk = t1.Shipment_BillOfExchangeFk,
                                       Shipment_InvoiceFk = t1.Shipment_ShipmentInvoiceFk
                                   }).OrderByDescending(x => x.ID).AsEnumerable();

            return model.DataListSlave;
        }
        public async Task<int> ShipmentBillOfExchangeSlaveAdd(VMShipmentBillOfExchangeSlave vmShipmentBillOfExchangeSlave)
        {
            var result = -1;
            //var styleInvoice = vmStyleInvoice.StyleInvoiceList.Where(x => x.Value > 0).ToList();
            var invoiceFk = vmShipmentBillOfExchangeSlave.Shipment_InvoiceListFk;
            List<Shipment_BillOfExchangeSlave> billOfExchangeSlaveList = new List<Shipment_BillOfExchangeSlave>();
            foreach (var invoiceId in invoiceFk)
            {
                Shipment_BillOfExchangeSlave shipmentBillOfExchangeSlave = new Shipment_BillOfExchangeSlave
                {
                    Shipment_BillOfExchangeFk = vmShipmentBillOfExchangeSlave.Shipment_BillOfExchangeFk,
                    Shipment_ShipmentInvoiceFk = Convert.ToInt32(invoiceId),
                    UserID = vmShipmentBillOfExchangeSlave.UserID                 
                };
                billOfExchangeSlaveList.Add(shipmentBillOfExchangeSlave);
            }           
            _db.Shipment_BillOfExchangeSlave.AddRange(billOfExchangeSlaveList);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = 1;
            }


            return result;
        }
        public async Task<int> ShipmentBillOfExchangeSlaveEdit(VMShipmentBillOfExchangeSlave vmShipmentBillOfExchangeSlave)
        {
            var result = -1;
            //to select Accountining_Chart_Two data.....
            Shipment_BillOfExchangeSlave shipmentBillOfExchangeSlave = _db.Shipment_BillOfExchangeSlave.Find(vmShipmentBillOfExchangeSlave.ID);
            shipmentBillOfExchangeSlave.Shipment_BillOfExchangeFk = vmShipmentBillOfExchangeSlave.Shipment_BillOfExchangeFk;
            shipmentBillOfExchangeSlave.Shipment_ShipmentInvoiceFk = vmShipmentBillOfExchangeSlave.Shipment_InvoiceFk;

            if (await _db.SaveChangesAsync() > 0)
            {
                result = shipmentBillOfExchangeSlave.ID;
            }
            return result;
        }
        public async Task<int> ShipmentBillOfExchangeSlaveDelete(int id)
        {
            int result = -1;

            if (id != 0)
            {
                Shipment_BillOfExchangeSlave shipmentBillOfExchangeSlave = _db.Shipment_BillOfExchangeSlave.Find(id);
                shipmentBillOfExchangeSlave.Active = false;
                if (await _db.SaveChangesAsync() > 0)
                {
                    result = shipmentBillOfExchangeSlave.Shipment_BillOfExchangeFk;
                }
            }
            return result;
        }

        public List<VMStyleSchedule> GetStyleShipmentScheduleByDate(DateTime dateTime)
        {
            List<VMStyleSchedule> list = new List<VMStyleSchedule>();
            if (dateTime != null)
            {
                list = (from t1 in _db.Merchandising_StyleShipmentSchedule
                        join t2 in _db.Merchandising_Style on t1.Merchandising_StyleFK equals t2.ID
                        join t3 in _db.Common_Country on t1.Common_CountryFK equals t3.ID
                        join t4 in _db.Common_CountryPort on t1.Common_CountryPortFK equals t4.ID
                        join t5 in _db.Merchandising_BuyerOrder on t2.Merchandising_BuyerOrderFK equals t5.ID
                        where t1.ShipmentDate == dateTime && t1.Active == true && t2.Active == true

                        && t2.PackQty >
                                (from x in _db.Shipment_ShipmentInstructionSlave
                                 where x.Active == true
                                        && x.Merchandising_StyleShipmentScheduleFk == t1.ID
                                        && x.Merchandising_StyleFk == t2.ID
                                 select x.InstructionQuantity).DefaultIfEmpty(0).Sum()

                        select new VMStyleSchedule
                        {
                            ID = t1.ID,
                            Merchandising_StyleFK = t1.Merchandising_StyleFK,
                            Style = t5.BuyerPO + "/" + t2.CID + "/" + t2.StyleName,
                            PortNo = t1.DestNo +" ("+ t4.Name+ ")",
                            Destination = t3.Name,
                            Common_CountryPortFK = t1.Common_CountryPortFK,
                            ScheduledQuantity = t1.Quantity ,
                            Quantity = t1.Quantity - (from x in _db.Shipment_ShipmentInstructionSlave
                                                                          where x.Active == true
                                                                                 && x.Merchandising_StyleShipmentScheduleFk == t1.ID
                                                                                 && x.Merchandising_StyleFk == t2.ID
                                                                          select x.InstructionQuantity).DefaultIfEmpty(0).Sum(),
                            Date = t1.ShipmentDate,
                            ColorSize = t1.Remarks
                        }).Where(x => x.Quantity > 0).ToList();


            }

            return list;
        }
        public List<VMStyleInstruction> GetShipmentInstructionByStyleID(int styleId)
        {
            List<VMStyleInstruction> list = new List<VMStyleInstruction>();
            if (styleId > 0)
            {
                list = (from t1 in _db.Shipment_ShipmentInstructionSlave
                        join t2 in _db.Shipment_ShipmentInstruction on t1.Shipment_ShipmentInstructionFk equals t2.ID
                        join t3 in _db.Merchandising_StyleShipmentSchedule on t1.Merchandising_StyleShipmentScheduleFk equals t3.ID
                        join t4 in _db.Merchandising_Style on t1.Merchandising_StyleFk equals t4.ID
                        join t5 in _db.Merchandising_BuyerOrder on t4.Merchandising_BuyerOrderFK equals t5.ID
                        join t6 in _db.Common_FinishItem on t4.Common_FinishItemFK equals t6.ID
                        join t7 in _db.Common_Country on t3.Common_CountryFK equals t7.ID
                        join t8 in _db.Common_CountryPort on t3.Common_CountryPortFK equals t8.ID
                        where t1.Merchandising_StyleFk == styleId && t1.Active && t2.Active && t3.Active &&
                        t3.Quantity > (from x in _db.Shipment_ShipmentInvoiceSlave
                                                                        join o in _db.Shipment_ShipmentInstructionSlave on x.Shipment_InstructionSlaveFk equals o.ID
                                                                        join y in _db.Merchandising_StyleShipmentSchedule on o.Merchandising_StyleShipmentScheduleFk equals y.ID
                                                                        where x.Active && o.Active == true && y.Active == true && y.Merchandising_StyleFK == t4.ID
                                                                        && x.Shipment_InstructionSlaveFk == t1.ID && x.Merchandising_StyleShipmentScheduleFk == t3.ID
                                                                        select x.InvoicedQuantity).DefaultIfEmpty(0).Sum()

                        select new VMStyleInstruction
                        {
                            ID = t1.ID,
                            SICID = t2.SICID,
                            DestNo = t3.DestNo,
                            SIDate = t2.SIDate,
                            InstructionQuantity = decimal.Round(t3.Quantity - (from x in _db.Shipment_ShipmentInvoiceSlave
                                                                                                  join o in _db.Shipment_ShipmentInstructionSlave on x.Shipment_InstructionSlaveFk equals o.ID
                                                                                                  join y in _db.Merchandising_StyleShipmentSchedule on o.Merchandising_StyleShipmentScheduleFk equals y.ID
                                                                                                  where x.Active && y.Merchandising_StyleFK == t4.ID
                                                                                                  && x.Shipment_InstructionSlaveFk == t1.ID && x.Merchandising_StyleShipmentScheduleFk == t3.ID
                                                                                                  select x.InvoicedQuantity).DefaultIfEmpty(0).Sum()),
                            Merchandising_StyleFk = t4.ID,
                            Style = t5.BuyerPO + "/" + t4.CID + "/" + t6.Name,
                            PortNo = t3.DestNo + " ("+ t8.Name + ")",
                            Destination = t7.Name,
                            ShipmentDate = t3.ShipmentDate,
                            Merchandising_StyleShipmentScheduleFk = t1.Merchandising_StyleShipmentScheduleFk,
                            //- (from x in _db.Shipment_ShipmentInstructionSlave
                            //                                              where x.Active == true
                            //                                                     && x.Merchandising_StyleShipmentScheduleFk == t1.ID
                            //                                                     && x.Merchandising_StyleFk == t2.ID
                            //                                              select x.Quantity).DefaultIfEmpty(0).Sum(),
                            //ShipmentDate = t3.ShipmentDate

                            //Quantity = Group.Sum(x => x.t2.Quantity) - (from x in _db.Shipment_ShipmentInvoiceSlave
                            //                                            join o in _db.Shipment_ShipmentInstructionSlave on x.Shipment_ShipmentInstructionFk equals o.Shipment_ShipmentInstructionFk

                            //                                            join y in _db.Merchandising_StyleShipmentSchedule on o.Merchandising_StyleShipmentScheduleFk equals y.ID
                            //                                            where x.Active == true && y.Merchandising_StyleFK == Group.First().t3.ID
                            //                                            && x.Shipment_ShipmentInstructionFk == Group.First().t1.ID
                            //                                            select x.Quantity).DefaultIfEmpty(0).Sum()

                        }).ToList();
            }

            return list;
        }
        public List<VMStyleInvoiced> GetStyleByShipmentInvoiceForChallan(int StyleId)
        {
            List<VMStyleInvoiced> list = new List<VMStyleInvoiced>();
            if (StyleId > 0)
            {
                list = (from t1 in _db.Shipment_ShipmentInvoiceSlave
                        join t2 in _db.Shipment_ShipmentInvoice on t1.Shipment_InvoiceFk equals t2.ID
                        join t3 in _db.Shipment_ShipmentInstructionSlave on t1.Shipment_InstructionSlaveFk equals t3.ID
                        join t4 in _db.Shipment_ShipmentInstruction on t3.Shipment_ShipmentInstructionFk equals t4.ID
                        join t5 in _db.Merchandising_StyleShipmentSchedule on t1.Merchandising_StyleShipmentScheduleFk equals t5.ID
                        join t6 in _db.Merchandising_Style on t1.Merchandising_StyleFk equals t6.ID
                        join t7 in _db.Merchandising_BuyerOrder on t6.Merchandising_BuyerOrderFK equals t7.ID
                        join t8 in _db.Common_FinishItem on t6.Common_FinishItemFK equals t8.ID
                        join t9 in _db.Common_Country on t5.Common_CountryFK equals t9.ID
                        join t10 in _db.Common_CountryPort on t5.Common_CountryPortFK equals t10.ID
                        where t1.Merchandising_StyleFk == StyleId && t1.Active == true && t2.Active == true && t3.Active == true
                        && t1.InvoicedQuantity > (from x in _db.Shipment_DeliveryChallanSlave
                                                  where x.Active == true && x.Merchandising_StyleFk == t6.ID
                                                  && x.Shipment_InstructionSlaveFk == t3.ID && x.Merchandising_StyleShipmentScheduleFk == t5.ID
                                                  && x.Shipment_InvoiceSlaveFk == t1.ID
                                                  select x.DeliveryChallanQuantity).DefaultIfEmpty(0).Sum()

                        select new VMStyleInvoiced
                        {
                            ShipmentDate = t5.ShipmentDate,
                            Shipment_InvoiceSlaveFk = t1.ID,
                            Shipment_InstructionSlaveFk = t1.Shipment_InstructionSlaveFk,
                            Merchandising_StyleShipmentScheduleFk = t1.Merchandising_StyleShipmentScheduleFk,
                            Merchandising_StyleFk = t1.Merchandising_StyleFk,
                            BuyerPO = t7.BuyerPO,
                            InvoiceID = t2.InvoiceCID,
                            Destination = t9.Name,
                            PortNo = t5.DestNo + " ("+ t10.Name + ")",
                            DeliveryChallanQuantity = t1.InvoicedQuantity - (from x in _db.Shipment_DeliveryChallanSlave
                                                                             where x.Active == true && x.Merchandising_StyleFk == t6.ID
                                                                             && x.Shipment_InstructionSlaveFk == t3.ID && x.Merchandising_StyleShipmentScheduleFk == t5.ID
                                                                             && x.Shipment_InvoiceSlaveFk == t1.ID
                                                                             select x.DeliveryChallanQuantity).DefaultIfEmpty(0).Sum(),

                        }).ToList();
            }

            return list;
        }

        //public List<VMStyleInvoice> GetShipmentInvoiceByStyleID(int styleId)
        //{
        //    List<VMStyleInvoice> list = new List<VMStyleInvoice>();
        //    if (styleId > 0)
        //    {
        //        list = (from t1 in _db.Shipment_ShipmentInvoice
        //                join t2 in _db.Shipment_ShipmentInvoiceSlave on t1.ID equals t2.Shipment_InvoiceFk
        //                join t5 in _db.Shipment_ShipmentInstructionSlave on t2.Shipment_ShipmentInstructionFk equals t5.Shipment_ShipmentInstructionFk

        //                join t4 in _db.Merchandising_StyleShipmentSchedule on t5.Merchandising_StyleShipmentScheduleFk equals t4.ID
        //                join t3 in _db.Merchandising_Style on t4.Merchandising_StyleFK equals t3.ID
        //                where t3.ID == styleId && t1.Active == true && t2.Active == true && t3.Active == true
        //                group new { t1, t2, t3 } by new { t1.ID } into Group

        //                select new VMStyleInvoice
        //                {
        //                    ID = Group.First().t1.ID,
        //                    InvoiceCID = Group.First().t1.InvoiceCID,
        //                    InvoiceDate = Group.First().t1.InvoiceDate,
        //                    Value = Group.Sum(x => x.t2.Quantity * x.t3.PackPrice) - (from x in _db.Shipment_BillOfExchangeSlave
        //                                                                              where x.Active == true && x.Merchandising_StyleFk == Group.First().t3.ID
        //                                                                              && x.Shipment_ShipmentInvoiceFk == Group.First().t2.Shipment_InvoiceFk
        //                                                                              select x.Value).DefaultIfEmpty(0).Sum()
        //                }).ToList();
        //    }

        //    return list;
        //}

        #region Terms of Shipment
        public async Task<VMTermsOfShipment> TermsOfShipmentGet()
        {
            VMTermsOfShipment vmTermsOfShipment = new VMTermsOfShipment();
            vmTermsOfShipment.DataList = await Task.Run(() => TermsOfShipmentDataLoad());
            return vmTermsOfShipment;
        }

        public IEnumerable<VMTermsOfShipment> TermsOfShipmentDataLoad()
        {
            var v = (from t1 in _db.Shipment_TermsOfShipment
                     where t1.Active == true
                     select new VMTermsOfShipment
                     {
                         ID = t1.ID,
                         Name = t1.Name
                     }).OrderByDescending(x => x.ID).AsEnumerable();
            return v;
        }


        public async Task<int> TermsOfShipmentAdd(VMTermsOfShipment vmTermsOfShipment)
        {
            var result = -1;
            Shipment_TermsOfShipment termsOfShipment = new Shipment_TermsOfShipment
            {
                Name = vmTermsOfShipment.Name,
                UserID = vmTermsOfShipment.UserID

            };
            _db.Shipment_TermsOfShipment.Add(termsOfShipment);
            if (await _db.SaveChangesAsync() > 0)
            {
                result = termsOfShipment.ID;
            }
            return result;
        }
        public async Task<int> TermsOfShipmentEdit(VMTermsOfShipment vmTermsOfShipment)
        {
            var result = -1;
            //to select Accountining_Chart_Two data.....
            Shipment_TermsOfShipment termsOfShipment = _db.Shipment_TermsOfShipment.Find(vmTermsOfShipment.ID);
            termsOfShipment.Name = vmTermsOfShipment.Name;
            termsOfShipment.User = vmTermsOfShipment.User;


            if (await _db.SaveChangesAsync() > 0)
            {
                result = termsOfShipment.ID;
            }
            return result;
        }
        public async Task<int> TermsOfShipmentDelete(int id)
        {
            int result = -1;

            if (id != 0)
            {
                Shipment_TermsOfShipment termsOfShipment = _db.Shipment_TermsOfShipment.Find(id);
                termsOfShipment.Active = false;

                if (await _db.SaveChangesAsync() > 0)
                {
                    result = termsOfShipment.ID;
                }
            }
            return result;
        }
        #endregion

        #region Export Realisation
        //public async Task<VMShipmentExportRealisation> ExportRealisationGet()
        //{
        //    VMShipmentExportRealisation vmShipmentExportRealisation = new VMShipmentExportRealisation();
        //    vmShipmentExportRealisation.RealisationDataList = await Task.Run(() => ExportRealisationDataLoad());
        //    return vmShipmentExportRealisation;
        //}

        //public IEnumerable<VMShipmentExportRealisation> ExportRealisationDataLoad()
        //{
        //    var v = (from t1 in _db.Shipment_ExportRealisation
        //             where t1.Active == true
        //             select new VMShipmentExportRealisation
        //             {
        //                 ID = t1.ID,
        //                 AitAmount = t1.AitAmount,
        //                 BillNo = t1.BillNo,
        //                 BillPurchase = t1.BillPurchase,
        //                 BillValue = t1.BillValue,
        //                 BuyingCommission = t1.BuyingCommission,
        //                 CdAmount = t1.CdAmount,
        //                 CourierExpense = t1.CourierExpense,
        //                 DocHandlingCharge = t1.DocHandlingCharge,
        //                 Date = t1.Date,
        //                 DollarValue = t1.DollarValue,
        //                 ERQAmt = t1.ERQAmt,
        //                 FcbParAmt = t1.FcbParAmt,
        //                 FDBC = t1.FDBC,
        //                 FDBCDate = t1.FDBCDate,
        //                 FdrAmount = t1.FdrAmount,
        //                 FxTrading = t1.FxTrading,
        //                 IsFinal = t1.IsFinal,
        //                 Others = t1.Others,
        //                 PcAdjust = t1.PcAdjust,
        //                 RealisedAmount = t1.RealisedAmount,
        //                 RmgAmount = t1.RmgAmount,
        //                 Shipment_BillOfExchangeFK = t1.Shipment_BillOfExchangeFK,
        //                 SodAcAmount = t1.SodAcAmount,
        //                 TimeLoanAmount = t1.TimeLoanAmount,
        //                 ValueDate = t1.ValueDate,
        //                 UserID = t1.UserID,
        //                 IsActive = t1.Active
        //             }).OrderByDescending(x => x.ID).AsEnumerable();
        //    return v;
        //}


        //public async Task<int> ExportRealisationAdd(VMShipmentExportRealisation vmShipmentExportRealisation)
        //{
        //    var result = -1;
        //    Shipment_ExportRealisation exportRealisation = new Shipment_ExportRealisation
        //    {

        //        ID = vmShipmentExportRealisation.ID,
        //        AitAmount = vmShipmentExportRealisation.AitAmount,
        //        BillNo = vmShipmentExportRealisation.BillNo,
        //        BillPurchase = vmShipmentExportRealisation.BillPurchase,
        //        BillValue = vmShipmentExportRealisation.BillValue,
        //        BuyingCommission = vmShipmentExportRealisation.BuyingCommission,
        //        CdAmount = vmShipmentExportRealisation.CdAmount,
        //        CourierExpense = vmShipmentExportRealisation.CourierExpense,
        //        DocHandlingCharge = vmShipmentExportRealisation.DocHandlingCharge,
        //        Date = vmShipmentExportRealisation.Date,
        //        DollarValue = vmShipmentExportRealisation.DollarValue,
        //        ERQAmt = vmShipmentExportRealisation.ERQAmt,
        //        FcbParAmt = vmShipmentExportRealisation.FcbParAmt,
        //        FDBC = vmShipmentExportRealisation.FDBC,
        //        FDBCDate = vmShipmentExportRealisation.FDBCDate,
        //        FdrAmount = vmShipmentExportRealisation.FdrAmount,
        //        FxTrading = vmShipmentExportRealisation.FxTrading,
        //        IsFinal = vmShipmentExportRealisation.IsFinal,
        //        Others = vmShipmentExportRealisation.Others,
        //        PcAdjust = vmShipmentExportRealisation.PcAdjust,
        //        RealisedAmount = vmShipmentExportRealisation.RealisedAmount,
        //        RmgAmount = vmShipmentExportRealisation.RmgAmount,
        //        Shipment_BillOfExchangeFK = vmShipmentExportRealisation.Shipment_BillOfExchangeFK,
        //        SodAcAmount = vmShipmentExportRealisation.SodAcAmount,
        //        TimeLoanAmount = vmShipmentExportRealisation.TimeLoanAmount,
        //        ValueDate = vmShipmentExportRealisation.ValueDate,
        //        UserID = vmShipmentExportRealisation.UserID


        //    };
        //    _db.Shipment_ExportRealisation.Add(exportRealisation);
        //    if (await _db.SaveChangesAsync() > 0)
        //    {
        //        result = exportRealisation.ID;
        //    }
        //    return result;
        //}
        //public async Task<int> ExportRealisationEdit(VMShipmentExportRealisation vmShipmentExportRealisation)
        //{
        //    var result = -1;
        //    //to select Accountining_Chart_Two data.....
        //    Shipment_ExportRealisation exportRealisation = _db.Shipment_ExportRealisation.Find(vmShipmentExportRealisation.ID);

        //    exportRealisation.AitAmount = vmShipmentExportRealisation.AitAmount;
        //    exportRealisation.BillNo = vmShipmentExportRealisation.BillNo;
        //    exportRealisation.BillPurchase = vmShipmentExportRealisation.BillPurchase;
        //    exportRealisation.BillValue = vmShipmentExportRealisation.BillValue;
        //    exportRealisation.BuyingCommission = vmShipmentExportRealisation.BuyingCommission;
        //    exportRealisation.CdAmount = vmShipmentExportRealisation.CdAmount;
        //    exportRealisation.CourierExpense = vmShipmentExportRealisation.CourierExpense;
        //    exportRealisation.DocHandlingCharge = vmShipmentExportRealisation.DocHandlingCharge;
        //    exportRealisation.Date = vmShipmentExportRealisation.Date;
        //    exportRealisation.DollarValue = vmShipmentExportRealisation.DollarValue;
        //    exportRealisation.ERQAmt = vmShipmentExportRealisation.ERQAmt;
        //    exportRealisation.FcbParAmt = vmShipmentExportRealisation.FcbParAmt;
        //    exportRealisation.FDBC = vmShipmentExportRealisation.FDBC;
        //    exportRealisation.FDBCDate = vmShipmentExportRealisation.FDBCDate;
        //    exportRealisation.FdrAmount = vmShipmentExportRealisation.FdrAmount;
        //    exportRealisation.FxTrading = vmShipmentExportRealisation.FxTrading;
        //    exportRealisation.IsFinal = vmShipmentExportRealisation.IsFinal;
        //    exportRealisation.Others = vmShipmentExportRealisation.Others;
        //    exportRealisation.PcAdjust = vmShipmentExportRealisation.PcAdjust;
        //    exportRealisation.RealisedAmount = vmShipmentExportRealisation.RealisedAmount;
        //    exportRealisation.RmgAmount = vmShipmentExportRealisation.RmgAmount;
        //    exportRealisation.Shipment_BillOfExchangeFK = vmShipmentExportRealisation.Shipment_BillOfExchangeFK;
        //    exportRealisation.SodAcAmount = vmShipmentExportRealisation.SodAcAmount;
        //    exportRealisation.TimeLoanAmount = vmShipmentExportRealisation.TimeLoanAmount;
        //    exportRealisation.ValueDate = vmShipmentExportRealisation.ValueDate;
        //    exportRealisation.UserID = vmShipmentExportRealisation.UserID;


        //    if (await _db.SaveChangesAsync() > 0)
        //    {
        //        result = exportRealisation.ID;
        //    }
        //    return result;
        //}
        //public async Task<int> ExportRealisationDelete(int id)
        //{
        //    int result = -1;

        //    if (id != 0)
        //    {
        //        Shipment_ExportRealisation exportRealisation = _db.Shipment_ExportRealisation.Find(id);
        //        exportRealisation.Active = false;

        //        if (await _db.SaveChangesAsync() > 0)
        //        {
        //            result = exportRealisation.ID;
        //        }
        //    }
        //    return result;
        //}
        #endregion

        public List<object> GetShippedByDropdown()
        {
            List<object> shipped = new List<object>();
            shipped.Add(new { Text = "SEA", Value = "SEA" });
            shipped.Add(new { Text = "AIR", Value = "AIR" });
            shipped.Add(new { Text = "Rail", Value = "Rail" });
            shipped.Add(new { Text = "Road", Value = "Road" });
            shipped.Add(new { Text = "Curier", Value = "Curier" });
            return shipped;
        }
        public List<object> GetTermsOfShipmentDropDown()
        {

            var termsList = new List<object>();

            foreach (var shipment in _db.Shipment_TermsOfShipment.Where(x => x.Active == true))
            {
                termsList.Add(new { Text = shipment.Name, Value = shipment.ID });
            }
            return termsList;
        }
        public List<object> StyleDropDownList()
        {
            var styleList = new List<object>();
            var styles = _db.Merchandising_Style.Where(a => a.Active == true).ToList();
            foreach (var style in styles)
            {
                styleList.Add(new { Text = style.StyleName, Value = style.ID });
            }
            return styleList;
        }
        public List<object> StyleForShipmentInstruction()
        {
            var styleList = new List<object>();


            var styles = (from t1 in _db.Merchandising_Style
                          join t3 in _db.Merchandising_BuyerOrder on t1.Merchandising_BuyerOrderFK equals t3.ID

                          join t2 in _db.Merchandising_StyleShipmentSchedule on t1.ID equals t2.Merchandising_StyleFK
                          where t1.Active
                          && (t2.Quantity / t1.PackPieceQty) >
                                 (from x in _db.Shipment_ShipmentInstructionSlave
                                  where x.Active && x.Merchandising_StyleFk == t1.ID && x.Merchandising_StyleShipmentScheduleFk == t2.ID
                                  select x.InstructionQuantity).DefaultIfEmpty(0).Sum()
                          select new
                          {
                              StyleName = t3.BuyerPO + "/" + t1.CID + "/" + t1.StyleName,
                              ID = t1.ID
                          }).Distinct().OrderByDescending(x => x.ID).ToList();
            foreach (var style in styles)
            {
                styleList.Add(new { Text = style.StyleName, Value = style.ID });
            }
            return styleList;
        }

        public List<object> StyleForDeliveryChallan()
        {
            var styleList = new List<object>();


            var styles = (from t1 in _db.Merchandising_Style
                          join t1s in _db.Merchandising_BuyerOrder on t1.Merchandising_BuyerOrderFK equals t1s.ID
                          join t2 in _db.Shipment_ShipmentInvoiceSlave on t1.ID equals t2.Merchandising_StyleFk
                          where t1.Active
                          //where t2.Quantity >
                          //       (from x in _db.Shipment_DeliveryChallanSlave
                          //        where x.Active == true// && x.Merchandising_StyleFk == t1.ID
                          //        && x.Shipment_ShipmentInstructionFk == t2.Shipment_ShipmentInstructionFk
                          //        select x.Quantity).DefaultIfEmpty(0).Sum()
                          select new
                          {
                              StyleName =t1.CID+"/"+ t1s.BuyerPO + "/"+ t1.StyleName,
                              ID = t1.ID 
                          }).Distinct().OrderByDescending(x => x.ID).ToList();
            foreach (var style in styles)
            {
                styleList.Add(new { Text = style.StyleName, Value = style.ID });
            }
            return styleList;
        }
        public List<object> StyleForInvoice()
        {
            var styleList = new List<object>();


            var styles = (from t1 in _db.Merchandising_Style
                          join t1s in _db.Merchandising_BuyerOrder on t1.Merchandising_BuyerOrderFK equals t1s.ID
                          join t2 in _db.Shipment_ShipmentInstructionSlave on t1.ID equals t2.Merchandising_StyleFk
                          where t1.Active
                          //where t2.Quantity >
                          //       (from x in _db.Shipment_ShipmentInvoiceSlave
                          //        join y in _db.Shipment_ShipmentInstructionSlave on x.Shipment_ShipmentInstructionFk equals y.Shipment_ShipmentInstructionFk
                          //        join z in _db.Merchandising_StyleShipmentSchedule on y.Merchandising_StyleShipmentScheduleFk equals z.ID
                          //        where x.Active == true && z.Merchandising_StyleFK == t1.ID && x.Shipment_ShipmentInstructionFk == t2.Shipment_ShipmentInstructionFk
                          //        select x.Quantity).DefaultIfEmpty(0).Sum()
                          select new
                          {
                              StyleName = t1.CID + "/" + t1s.BuyerPO + "/" + t1.StyleName,
                              ID = t1.ID
                          }).Distinct().OrderByDescending(x => x.ID).ToList();
            foreach (var style in styles)
            {
                styleList.Add(new { Text = style.StyleName, Value = style.ID });
            }
            return styleList;
        }
        

        public List<object> ShipmentInstructionDropDownList()
        {
            var siList = new List<object>();
            var si = _db.Shipment_ShipmentInstruction.Where(a => a.Active).ToList();
            foreach (var x in si)
            {
                siList.Add(new { Text = x.SICID, Value = x.ID });
            }
            return siList;
        }
        public List<object> ShipmentInvoiceDropDownList()
        {
            var siList = new List<object>();
            var si = _db.Shipment_ShipmentInvoice.Where(a => a.Active).ToList();
            foreach (var x in si)
            {
                siList.Add(new { Text = x.InvoiceCID, Value = x.ID });
            }
            return siList;
        }

        public List<object> ShipmentInvoiceForBillOfExchangeDropDownList()
        {
            var invoiceFk = _db.Shipment_BillOfExchangeSlave.Where(x => x.Active).Select(x => x.Shipment_ShipmentInvoiceFk).ToList();
            var siList = new List<object>();
            var si = _db.Shipment_ShipmentInvoice.Where(a => a.Active && !invoiceFk.Contains(a.ID)).ToList();
            foreach (var x in si)
            {
                siList.Add(new { Text = x.InvoiceCID, Value = x.ID });
            }
            return siList;
        }
        public List<object> StyleShipmentScheduleDropDownList()
        {
            var shipmentScheduleList = new List<object>();
            var v = (from t1 in _db.Merchandising_StyleShipmentSchedule
                     join t2 in _db.Common_Country on t1.Common_CountryFK equals t2.ID
                     join t3 in _db.Common_CountryPort on t1.Common_CountryPortFK equals t3.ID
                     select new
                     {
                         Text = "Country: " + t2.Name + ", Port: " + t1.DestNo +" ("+ t3.Name + ")",
                         ID = t1.ID
                     }).Distinct().ToList();

            //  var styles = _db.Merchandising_StyleShipmentSchedule.Where(a => a.Active == true).ToList();
            foreach (var sSchedule in v)
            {
                shipmentScheduleList.Add(new { Text = sSchedule.Text, Value = sSchedule.ID });
            }
            return shipmentScheduleList;
        }
        public List<object> CommonCountryDropDownList()
        {
            var commonCountriesList = new List<object>();
            var commonCountries = _db.Common_Country.Where(a => a.Active).ToList();
            foreach (var x in commonCountries)
            {
                commonCountriesList.Add(new { Text = x.Name, Value = x.ID });
            }
            return commonCountriesList;
        }
        public List<object> CommonBuyerDropDownList()
        {
            var commonBuyerList = new List<object>();
            var v = (from t1 in _db.Common_Buyer
                     join t2 in _db.Merchandising_BuyerOrder on t1.ID equals t2.Common_BuyerFK
                     join t3 in _db.Merchandising_Style on t2.ID equals t3.Merchandising_BuyerOrderFK
                     join t4 in _db.Shipment_ShipmentInstructionSlave on t3.ID equals t4.Merchandising_StyleFk
                     select new
                     {
                         Text = t1.Name,
                         Value = t1.ID
                     }).Distinct().ToList();
            //var commonBuyers = _db.Common_Buyer.Where(a => a.Active).ToList();
            foreach (var x in v)
            {
                commonBuyerList.Add(new { Text = x.Text, Value = x.Value });
            }
            return commonBuyerList;
        }
        public List<object> CommonCountryPortDropDownList()
        {
            var commonCountryPortList = new List<object>();
            var commonCountryPort = _db.Common_CountryPort.Where(a => a.Active).ToList();
            foreach (var x in commonCountryPort)
            {
                commonCountryPortList.Add(new { Text = x.Name, Value = x.ID });
            }
            return commonCountryPortList;
        }
        public List<object> PortOfLoadingDropDownList()
        {
            var commonCountryPortList = new List<object>();
            var commonCountryPort = (from t1 in _db.Common_CountryPort
                                     join t2 in _db.Common_Country on t1.Common_CountryFK equals t2.ID
                                     where t1.Active == true && t2.Name.ToLower() == "bangladesh"
                                     select new
                                     {
                                         Name = t1.Name,
                                         ID = t1.ID
                                     }).ToList();// _db.Common_CountryPort.Where(a => a.Active).ToList();
            foreach (var x in commonCountryPort)
            {
                commonCountryPortList.Add(new { Text = x.Name, Value = x.ID });
            }
            return commonCountryPortList;
        }
        public List<object> ExportCommercialInformationDropDownList()
        {
            var exportCommercialInformationList = new List<object>();
            var exportCommercialInformation = _db.Commercial_ECI.Where(a => a.Active).ToList();
            foreach (var x in exportCommercialInformation)
            {
                exportCommercialInformationList.Add(new { Text = x.ECINo, Value = x.ID });
            }
            return exportCommercialInformationList;
        }

        public List<object> CommonSupplierDropDownList()
        {
            var commonSupplierList = new List<object>();
            var commonSupplier = _db.Common_Supplier.Where(a => a.Active).ToList();
            foreach (var x in commonSupplier)
            {
                commonSupplierList.Add(new { Text = x.Name, Value = x.ID });
            }
            return commonSupplierList;
        }

        public List<object> CommonTransportSupplierDropDownList()
        {
            var commonSupplierList = new List<object>();
            var commonSupplier = _db.Common_Supplier.Where(a => a.Active  && a.SupplierType == (int)EnumSupplierType.Transport).ToList();
            foreach (var x in commonSupplier)
            {
                commonSupplierList.Add(new { Text = x.Name, Value = x.ID });
            }
            return commonSupplierList;
        }
        public List<object> CommonCandFSupplierDropDownList()
        {
            var commonSupplierList = new List<object>();
            var commonSupplier = _db.Common_Supplier.Where(a => a.Active && a.SupplierType == (int)EnumSupplierType.CandF).ToList();
            foreach (var x in commonSupplier)
            {
                commonSupplierList.Add(new { Text = x.Name, Value = x.ID });
            }
            return commonSupplierList;
        }

        public List<object> ReceivedTypeDropDownList()
        {
            var exportCommercialInformationList = new List<object>();
            exportCommercialInformationList.Add(new { Text = "Value received and drawn under Contract No", Value = "Value received and drawn under Contract No" });
            exportCommercialInformationList.Add(new { Text = "Value received and drawn under Export L/C No", Value = "Value received and drawn under Export L/C No" });

            return exportCommercialInformationList;
        }

    }
}
