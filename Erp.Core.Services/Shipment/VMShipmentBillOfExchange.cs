﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Text;

namespace Erp.Core.Services.Shipment
{
    public class VMShipmentBillOfExchangeSlave : VMShipmentBillOfExchange
    {
        public int Shipment_InvoiceFk { get; set; }
        public List<string> Shipment_InvoiceListFk { get; set; }



        public int Merchandising_StyleFk { get; set; }
        public string StyleName { get; set; }

        public string InvoiceNo { get; set; }
        public DateTime InvoiceDate { get; set; }
        public decimal Value { get; set; }
        public int Shipment_BillOfExchangeFk { get; set; }
        



        public IEnumerable<VMShipmentBillOfExchangeSlave> DataListSlave { get; set; }
        public string ExportCommercialInformation { get; set; }
        public string CompanyBank { get; set; }
        public string BuyerBank { get; set; }
        public string CompanyBankAddress { get; set; }
        public string BuyerBankAddress { get; set; }
        public string ValueInWord { get; set; }
    }
    public class VMShipmentBillOfExchange : BaseVM
    {
      
        public bool IsFinal { get; set; }

        public DateTime BoEDate { get; set; }
        public string BoECID { get; set; }      
       
        public string CourierNO { get; set; }       
        public DateTime CourierDate { get; set; }
        
        public int Commercial_ECIFK { get; set; }

        
        public string ReceivedType { get; set; }

     
        public string FDBCNO { get; set; }       
        public DateTime FDBCDate { get; set; }
        public DateTime ECIDate { get; set; }


        public string Description { get; set; }
        public string Remarks { get; set; }
        

        public SelectList ShipmentInvoiceList { get; set; } = new SelectList(new List<object>());
        public SelectList ExportCommercialInformationList { get; set; } = new SelectList(new List<object>());
        public SelectList ReceivedTypeList { get; set; } = new SelectList(new List<object>());
        public IEnumerable<VMShipmentBillOfExchange> DataList { get; set; }
        public decimal BillValue { get; set; }
        public string ECINO { get; set; }
        public string BuyerName { get; set; }
        
        //for accounting purpose
        public int? AccHead { get; set; }
        public int? AccID { get; set; }
        public bool? AccFinalize { get; set; } = false;
        public bool? AccApproved { get; set; } = false;
        public decimal AccBillValue { get; set; }
    }

    public class VMStyleInvoice : BaseVM
    {
        public DateTime InvoiceDate { get; set; }
        public string InvoiceCID { get; set; }
        public int Merchandising_StyleFk { get; set; }
        public int Shipment_ShipmentInvoiceFk { get; set; }
        public int Shipment_BillOfExchangeFk { get; set; }
        public decimal Value { get; set; }
        public bool Flag { get; set; }
        public string Style { get; set; }

        public List<VMStyleInvoice> StyleInvoiceList { get; set; }

        // public IEnumerable<VMShipmentColorSize> RatioDataList { get; set; }
    }
}
