﻿using Erp.Core.Services.Home;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Erp.Core.Services.Shipment
{
    public class VMShipmentInvoiceSlave : VMShipmentInvoice
    {
        public int Shipment_ShipmentInstructionFk { get; set; }
        public int MerchandisingStyleFK { get; set; }
        public int OrderID { get; set; }
        public int CommonBuyerFK { get; set; }
        public string SICID { get; set; }
        public DateTime SIDate { get; set; }

        public int Shipment_InvoiceFk { get; set; }
        public decimal PcsQuantity { get; set; }
        public decimal PackQuantity { get; set; }
        

        public IEnumerable<VMShipmentInvoiceSlave> DataListSlave { get; set; }
        public decimal CTNQuantity { get; set; }
        public string BuyerPO { get; set; }
        public string DescriptionOfGoods { get; set; }
        public decimal PackPrice { get; set; }
        public decimal TotalPrice { get; set; }
    }
    public class TotalGenerateClass
    {
        public decimal TotalOrderQty { get; set; }
        public decimal TotalPackQty { get; set; }
        public decimal TotalPrice { get; set; }
        public int TotalCtnQty { get; set; }
        public int GrandTotal { get; set; }
    }
    public class VMShipmentInvoice : BaseVM
    {
        public DateTime InvoiceDate { get; set; }
        public string InvoiceCID { get; set; }
        public string Style { get; set; }
        public SelectList ShipmentInstructionList { get; set; } = new SelectList(new List<object>());
        public SelectList TermsOfShipmentList { get; set; } = new SelectList(new List<object>());

        public SelectList ShippedByList { get; set; } = new SelectList(new List<object>());

        public SelectList CommonBuyerList { get; set; } = new SelectList(new List<object>());

        public SelectList PortOfLoadingList { get; set; } = new SelectList(new List<object>());
        public SelectList BuyerNotifyPartyList { get; set; } = new SelectList(new List<object>());

        public List<TotalGenerateClass> TotalGenerateClass { get; set; }       
        
        public string ErcNo { get; set; }       
        public string InvoiceIdNo { get; set; }       
        public string ShippedBy { get; set; }       
        public List<VMBuyerNotifyParty> BuyerNotifyPartys { get; set; }

        public List<string> BuyerNotifyPartysFk { get; set; }

        public List<VMCompanySetup> VMCompanySetup { get; set; }

        public Distination Distination { get; set; }
        public UDInfo UDInfo { get; set; }

        public int Common_BuyerNotifyPartyFk { get; set; }
        public int Shipment_PortOfLoadingFk { get; set; }
        public int Shipment_TermsOfShipmentFK { get; set; }
        public string PortOfLoading { get; set; }
        public string BLNo { get; set; }       
        public DateTime BLDate { get; set; }
        public string ExpNo { get; set; }
        public DateTime ExpDate { get; set; }
        [DataType(DataType.MultilineText)]
        public string ShippingMarks { get; set; }             
        public string TermsOfShipment { get; set; }
        public DateTime EstimatedReceivedDate { get; set; }        
        public decimal AdjustedValue { get; set; }
        public string AdjustedNote { get; set; }
        public IEnumerable<VMShipmentInvoice> DataList { get; set; }
    }

    public class VMStyleInstruction : BaseVM
    {
        public string DestNo { get; set; }
        public DateTime SIDate { get; set; }
        public string SICID { get; set; }
        public int Merchandising_StyleFk { get; set; }
        public int Merchandising_StyleShipmentScheduleFk { get; set; }

        public decimal InstructionQuantity { get; set; }
       
        public int TotalCTN { get; set; }
        public string Style { get; set; }

        public List<VMStyleInstruction> StyleInstructionList { get; set; }
        public string BuyerName { get; set; }
        public string PortNo { get; set; }
        
        [DisplayName("Destination")]
        public int Common_CountryFK { get; set; }

        [DisplayName("Port")]
        public int Common_CountryPortFK { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime ShipmentDate { get; set; }


       
        public bool IsSelected { get; set; }
        public bool Flag { get; set; }        
        public decimal ScheduledQuantity { get; set; }
        public string Destination { get; set; }

        // public IEnumerable<VMShipmentColorSize> RatioDataList { get; set; }
    }
    public class VMBuyerNotifyParty
    {
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string ContactPerson { get; set; }
        public string Address { get; set; }
        public int Common_CountryFK { get; set; }
        public string CountryName { get; set; }

        public int Common_BuyerFK { get; set; }
        public string BuyerName { get; set; }

    }
    public class UDInfo
    {
        public string UDNo { get; set; }
        public VmReceivingBank VmReceivingBank { get; set; }
        public VmApplicantBank VmApplicantBank { get; set; }
        public VMBuyer VMBuyer { get; set; }

    }
    public class VMCompanySetup
    {
        public string Name { get; set; }
        public string ShortName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string ContactPerson { get; set; }      
        public string Address { get; set; }       
        public byte[] Image { get; set; }
    }
    public class Distination
    {
        public string DistinationCountry { get; set; }
        public string DistinationPort { get; set; }
       
    }
}
