﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Erp.Core.Services.Shipment
{
    
    public class VMTermsOfShipment : BaseVM
    {
        
        public string Name { get; set; }
     
        public IEnumerable<VMTermsOfShipment> DataList { get; set; }
    }
    
}
