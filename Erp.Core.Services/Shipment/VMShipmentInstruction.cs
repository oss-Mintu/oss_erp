﻿using Erp.Core.Services.Merchandising;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Erp.Core.Services.Shipment
{
    public class VMShipmentInstructionSlave : VMShipmentInstruction
    {
        [DisplayFormat(DataFormatString = "{0:yyyy-mm-dd}", ApplyFormatInEditMode = true)]

        public DateTime ShipmentDate { get; set; }
        
        public int InstructedQty { get; set; }
        public decimal ScheduleQuantity { get; set; }
        public string StyleName { get; set; }
        public string Port { get; set; }

        public string Destination { get; set; }



        public int CtnQty { get; set; }
        public int PackQty { get; set; }
        public int Merchandising_StyleFk { get; set; }
        public int Shipment_ShipmentInstructionFk { get; set; }

        public SelectList MerchandisingStyleList { get; set; } = new SelectList(new List<object>());
        public SelectList ShipmentScheduleList { get; set; } = new SelectList(new List<object>());

        public int Merchandising_StyleShipmentScheduleFk { get; set; }

        public IEnumerable<VMShipmentInstructionSlave> DataListSlave { get; set; }
        public string Remarks { get; internal set; }
    }
    public class VMShipmentInstruction : BaseVM
    {
        public DateTime SIDate { get; set; }
        public string SICID { get; set; }

        public List<string> StyleList { get; set; }

        public IEnumerable<VMShipmentInstruction> DataList { get; set; }
        public VMShipmentSchedule VMShipmentSchedule { get; set; }
        public int TotalInstructedQty { get; set; }
    }

    public class VMStyleSchedule : BaseVM
    {

        [DisplayName("Style")]
        public int Merchandising_StyleFK { get; set; }

        [DisplayName("Destination")]
        public int Common_CountryFK { get; set; }

        [DisplayName("Port")]
        public int Common_CountryPortFK { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        [DisplayName("Delivery Date")]
        public DateTime Date { get; set; }

        [DisplayName("Quantity")]
        [Required(ErrorMessage = "Quantity is Required")]
        public int Quantity { get; set; }
        public bool IsSelected { get; set; }

        public bool Flag { get; set; }



        public decimal ScheduledQuantity { get; set; }


        public string Style { get; set; }

        public string Destination { get; set; }

        public string PortNo { get; set; }

        [DisplayName("Color With Size")]
        [DataType(DataType.MultilineText), Required(ErrorMessage = "Color With Size is Required")]
        public string ColorSize { get; set; }

        [DisplayName("Shipment Color")]
        public int Common_ColorFK { get; set; }

        [DisplayName("Color Quantity")]
        public int ColorQty { get; set; }

        public List<string> ColorIds { get; set; }
        public List<string> ColorQtys { get; set; }

        public List<VMStyleSchedule> StyleScheduleList { get; set; }

       // public IEnumerable<VMShipmentColorSize> RatioDataList { get; set; }
    }
  
}
