﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Erp.Core.Services.App
{
    public class VMApp
    {
        public int ID { get; set; }
        public int LineID { get; set; }
        public int SetPackID { get; set; }
        public int StyleID { get; set; }
        public string Name { get; set; }
        public string DeviceID { get; set; }
        public string LineName { get; set; }
        public string SetPackName { get; set; }
    }
}
