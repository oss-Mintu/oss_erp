﻿using Erp.Core.Services.Home;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Erp.Core.Services.App
{
    public class AppService : BaseService
    {
        private readonly VMLogin _vmLogin;
        private readonly HomeService _homeservice;

        public AppService(IErpDbContext db, HttpContext httpContext)
        {
            _db = db;
            _vmLogin = httpContext.Session.GetObjectFromJson<VMLogin>("LoginUser");
            _homeservice = new HomeService(db, httpContext);
        }

        public List<VMLogin> GetUserName(string userName, string password)
        {
            string hashedPassword = _homeservice.GetHashedPassword(password);
            var data = _db.User_User.Where(x => x.UserName == userName && x.Password == hashedPassword && x.Active == true)
                        .Select(t1 => new VMLogin
                        {
                            ID = t1.ID,
                            UserName = t1.UserName,
                            IsActive = t1.Active
                        }).ToList();
            List<VMLogin> a = new List<VMLogin>();
            if (data.Any())
            {
                return data;
            }
            else
            {
                a.Add(new VMLogin() { IsActive = false });
                return a;
            }
        }

        public List<VMApp> GetAllQCStyleSize()
        {
            DateTime today = DateTime.Today;
            var DataList = (from t1 in _db.Prod_ReferencePlan
                            join t2 in _db.Prod_Reference on t1.Prod_ReferenceFK equals t2.ID
                            join t3 in _db.Merchandising_Style on t1.Merchandising_StyleFK equals t3.ID
                            join t4 in _db.Merchandising_StyleMeasurement on t3.ID equals t4.Merchandising_StyleFK
                            join t5 in _db.Common_Size on t4.Common_SizeFK equals t5.ID
                            where t2.ReferenceDate == today && t1.Active == true && t1.SectionId == (int)EnumProcess.Sewing
                            group t1 by new { t1.Merchandising_StyleFK,t4.Common_SizeFK,t5.Name } into all
                            select new VMApp
                            {
                                StyleID = all.Key.Merchandising_StyleFK,
                                ID = all.Key.Common_SizeFK,
                                Name = all.Key.Name
                            }).ToList();
            return DataList;

        }

        public List<VMApp> GetAllQCStyleColor()
        {
            DateTime today = DateTime.Today;
            var DataList = (from t1 in _db.Prod_ReferencePlan
                            join t2 in _db.Prod_Reference on t1.Prod_ReferenceFK equals t2.ID
                            join t3 in _db.Merchandising_Style on t1.Merchandising_StyleFK equals t3.ID
                            join t4 in _db.Merchandising_StyleMeasurement on t3.ID equals t4.Merchandising_StyleFK
                            join t5 in _db.Common_Color on t4.Common_ColorFK equals t5.ID
                            where t2.ReferenceDate == today && t1.Active == true && t1.SectionId==(int)EnumProcess.Sewing
                            group t1 by new { t1.Merchandising_StyleFK, t4.Common_ColorFK, t5.Name } into all
                            select new VMApp
                            {
                                StyleID = all.Key.Merchandising_StyleFK,
                                ID = (int)all.Key.Common_ColorFK,
                                Name = all.Key.Name
                            }).ToList();

            return DataList;
        }

        public List<VMApp> GetAllQCStyle()
        {
            DateTime today = DateTime.Today;
            var DataList = (from t1 in _db.Prod_ReferencePlan
                            join t2 in _db.Prod_Reference on t1.Prod_ReferenceFK equals t2.ID
                            join t3 in _db.Common_LineDevice on t1.ProductionLineFK equals t3.Common_ProductionLineFK
                            join t4 in _db.Merchandising_Style on t1.Merchandising_StyleFK equals t4.ID
                            join t5 in _db.Common_ProductionLine on t3.Common_ProductionLineFK equals t5.ID
                            where t2.ReferenceDate == today && t1.Active == true && t1.SectionId == (int)EnumProcess.Sewing
                            select new VMApp
                            {
                                DeviceID = t3.DeviceID,
                                ID = t1.Merchandising_StyleFK,
                                LineID = t3.Common_ProductionLineFK,
                                Name = t4.StyleName,
                                LineName = t5.Name
                            }).ToList();

            //var DataList = _db.Merchandising_Style.Where(a => a.Active == true).ToList().ConvertAll(
            //      x => new VMApiStyle { ID = x.ID, Name = x.StyleName });

            return DataList;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>List of object take ID=SetpackId Name=SetpackName and StyleID=StyleID</returns>
        public List<VMApp> GetAllQCStyleSetPack()
        {
            DateTime today = DateTime.Today;
            var DataList = (from t1 in _db.Prod_ReferencePlan
                            join t2 in _db.Prod_Reference on t1.Prod_ReferenceFK equals t2.ID
                            join t3 in _db.Merchandising_Style on t1.Merchandising_StyleFK equals t3.ID
                            join t4 in _db.Merchandising_StyleSetPack on t3.ID equals t4.Merchandising_StyleFK
                            where t2.ReferenceDate == today && t1.Active == true
                            group t1 by new { t1.Merchandising_StyleFK, t4.ID, t4.SetPackName } into all
                            select new VMApp
                            {
                                ID = all.Key.ID,
                                StyleID= all.Key.Merchandising_StyleFK,
                                Name = all.Key.SetPackName
                            }).ToList();

            //var DataList = _db.Merchandising_StyleSetPack.Where(a => a.Active == true).ToList().ConvertAll(
            //      x => new VMApiStyleSetPack { ID = x.ID, SetPackName = x.SetPackName });

            return DataList;
        }

        public List<VMApp> GetAllStyleQCType()
        {
            DateTime today = DateTime.Today;
            var DataList = (from t1 in _db.Prod_ReferencePlan
                            join t2 in _db.Prod_Reference on t1.Prod_ReferenceFK equals t2.ID
                            join t3 in _db.Merchandising_Style on t1.Merchandising_StyleFK equals t3.ID
                            join t4 in _db.Prod_QCOrder on t3.ID equals t4.Merchandising_StyleFK
                            join t5 in _db.Prod_QCType on t4.Prod_QCTypeFK equals t5.ID
                            where t2.ReferenceDate == today && t1.Active == true && t4.Active==true && t1.SectionId == (int)EnumProcess.Sewing
                            select new VMApp
                            {
                                StyleID=t3.ID,
                                ID=t5.ID,
                                Name = t5.Name
                            }).ToList();
            return DataList;
        }
    }
}
