﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Erp.Infrastructure.Migrations
{
    public partial class ProductionDB20190901Elephant : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Color",
                table: "Merchandising_YarnCalculation");

            migrationBuilder.DropColumn(
                name: "HRMS_UnitFK",
                table: "Common_SectionLine");

            migrationBuilder.AddColumn<string>(
                name: "Code",
                table: "User_Department",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Common_ColorFK",
                table: "Merchandising_YarnCalculation",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Code",
                table: "HRMS_Unit",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Code",
                table: "HRMS_Section",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Code",
                table: "HRMS_Designation",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Code",
                table: "HRMS_BusinessUnit",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Common_ProductionFloorFK",
                table: "Common_SectionLine",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Code",
                table: "Common_ProductionLine",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TIN",
                table: "Common_CompanySetup",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TradeLicense",
                table: "Common_CompanySetup",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "VatRegNo",
                table: "Common_CompanySetup",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Common_ProductionUnit",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: true),
                    IsColsed = table.Column<bool>(nullable: false),
                    HRMS_UnitFK = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_ProductionUnit", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Common_ProductionFloor",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: true),
                    IsColsed = table.Column<bool>(nullable: false),
                    Common_ProductionUnitFK = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_ProductionFloor", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Common_ProductionFloor_Common_ProductionUnit_Common_ProductionUnitFK",
                        column: x => x.Common_ProductionUnitFK,
                        principalTable: "Common_ProductionUnit",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Common_SectionLine_Common_ProductionFloorFK",
                table: "Common_SectionLine",
                column: "Common_ProductionFloorFK");

            migrationBuilder.CreateIndex(
                name: "IX_Common_ProductionFloor_Common_ProductionUnitFK",
                table: "Common_ProductionFloor",
                column: "Common_ProductionUnitFK");

            migrationBuilder.AddForeignKey(
                name: "FK_Common_SectionLine_Common_ProductionFloor_Common_ProductionFloorFK",
                table: "Common_SectionLine",
                column: "Common_ProductionFloorFK",
                principalTable: "Common_ProductionFloor",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Common_SectionLine_Common_ProductionFloor_Common_ProductionFloorFK",
                table: "Common_SectionLine");

            migrationBuilder.DropTable(
                name: "Common_ProductionFloor");

            migrationBuilder.DropTable(
                name: "Common_ProductionUnit");

            migrationBuilder.DropIndex(
                name: "IX_Common_SectionLine_Common_ProductionFloorFK",
                table: "Common_SectionLine");

            migrationBuilder.DropColumn(
                name: "Code",
                table: "User_Department");

            migrationBuilder.DropColumn(
                name: "Common_ColorFK",
                table: "Merchandising_YarnCalculation");

            migrationBuilder.DropColumn(
                name: "Code",
                table: "HRMS_Unit");

            migrationBuilder.DropColumn(
                name: "Code",
                table: "HRMS_Section");

            migrationBuilder.DropColumn(
                name: "Code",
                table: "HRMS_Designation");

            migrationBuilder.DropColumn(
                name: "Code",
                table: "HRMS_BusinessUnit");

            migrationBuilder.DropColumn(
                name: "Common_ProductionFloorFK",
                table: "Common_SectionLine");

            migrationBuilder.DropColumn(
                name: "Code",
                table: "Common_ProductionLine");

            migrationBuilder.DropColumn(
                name: "TIN",
                table: "Common_CompanySetup");

            migrationBuilder.DropColumn(
                name: "TradeLicense",
                table: "Common_CompanySetup");

            migrationBuilder.DropColumn(
                name: "VatRegNo",
                table: "Common_CompanySetup");

            migrationBuilder.AddColumn<string>(
                name: "Color",
                table: "Merchandising_YarnCalculation",
                maxLength: 40,
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "HRMS_UnitFK",
                table: "Common_SectionLine",
                nullable: false,
                defaultValue: 0);
        }
    }
}
