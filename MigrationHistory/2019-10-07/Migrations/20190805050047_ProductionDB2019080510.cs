﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Erp.Infrastructure.Migrations
{
    public partial class ProductionDB2019080510 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Commercial_Buyer");

            migrationBuilder.DropTable(
                name: "Commercial_BuyerOrder");

            migrationBuilder.DropTable(
                name: "Commercial_PurchaseOrder");

            migrationBuilder.DropTable(
                name: "Commercial_UDBuyerOrder");

            migrationBuilder.DropTable(
                name: "Common_Order");

            migrationBuilder.DropTable(
                name: "Common_Style");

            migrationBuilder.DropTable(
                name: "Common_UD");

            migrationBuilder.DropColumn(
                name: "AddressLine1",
                table: "Common_CompanySetup");

            migrationBuilder.DropColumn(
                name: "AddressLine2",
                table: "Common_CompanySetup");

            migrationBuilder.DropColumn(
                name: "AddressLine3",
                table: "Common_CompanySetup");

            migrationBuilder.DropColumn(
                name: "Common_POFK",
                table: "Commercial_BBLC");

            migrationBuilder.DropColumn(
                name: "Common_RawItemFK",
                table: "Commercial_BBLC");

            migrationBuilder.DropColumn(
                name: "Description",
                table: "Commercial_BBLC");

            migrationBuilder.DropColumn(
                name: "LCDoc",
                table: "Commercial_BBLC");

            migrationBuilder.DropColumn(
                name: "Orgin",
                table: "Commercial_BBLC");

            migrationBuilder.RenameColumn(
                name: "CompanyShortName",
                table: "Common_CompanySetup",
                newName: "ShortName");

            migrationBuilder.RenameColumn(
                name: "CompanyName",
                table: "Common_CompanySetup",
                newName: "Name");

            migrationBuilder.RenameColumn(
                name: "AddressLine4",
                table: "Common_CompanySetup",
                newName: "Address");

            migrationBuilder.RenameColumn(
                name: "TotalValuePO",
                table: "Commercial_ECI",
                newName: "ValueAmendment");

            migrationBuilder.RenameColumn(
                name: "Common_UDFK",
                table: "Commercial_ECI",
                newName: "Common_BuyerNotifyPartyFk");

            migrationBuilder.RenameColumn(
                name: "Common_BuyerPOFK",
                table: "Commercial_ECI",
                newName: "Common_BuyerFK");

            migrationBuilder.RenameColumn(
                name: "Common_ApplicantFK",
                table: "Commercial_ECI",
                newName: "Commercial_UDFk");

            migrationBuilder.RenameColumn(
                name: "AccHeadID",
                table: "Commercial_ECI",
                newName: "BaseHeadId");

            migrationBuilder.RenameColumn(
                name: "TotalValuePO",
                table: "Commercial_BBLC",
                newName: "Amendment");

            migrationBuilder.RenameColumn(
                name: "ReqQty",
                table: "Commercial_BBLC",
                newName: "Status");

            migrationBuilder.RenameColumn(
                name: "Payable",
                table: "Commercial_BBLC",
                newName: "ScanedFile");

            migrationBuilder.RenameColumn(
                name: "Common_UnitFK",
                table: "Commercial_BBLC",
                newName: "Commercial_UDFk");

            migrationBuilder.RenameColumn(
                name: "Common_UDFK",
                table: "Commercial_BBLC",
                newName: "Commercial_LCOreginFK");

            migrationBuilder.AddColumn<int>(
                name: "Commercial_BBLCFk",
                table: "Procurement_PurchaseOrder",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Commercial_ECIFk",
                table: "Merchandising_BuyerOrder",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Address",
                table: "Common_LienBank",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Common_CountryFK",
                table: "Common_LienBank",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ContactNo",
                table: "Common_LienBank",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SwiftCode",
                table: "Common_LienBank",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Address",
                table: "Common_CompanyBank",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Common_CountryFK",
                table: "Common_CompanyBank",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ContactNo",
                table: "Common_CompanyBank",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SwiftCode",
                table: "Common_CompanyBank",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Address",
                table: "Common_BuyerBank",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Common_CountryFK",
                table: "Common_BuyerBank",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ContactNo",
                table: "Common_BuyerBank",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SwiftCode",
                table: "Common_BuyerBank",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsApproved",
                table: "Commercial_BBLC",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateTable(
                name: "Commercial_UD",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    CID = table.Column<string>(nullable: true),
                    Ref = table.Column<string>(nullable: true),
                    Date = table.Column<DateTime>(nullable: false),
                    LimitDeclaration = table.Column<decimal>(nullable: false),
                    Common_BuyerFk = table.Column<int>(nullable: false),
                    Accounting_HeadFk = table.Column<int>(nullable: false),
                    IsClose = table.Column<bool>(nullable: false),
                    ScanedFile = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Commercial_UD", x => x.ID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Commercial_UD");

            migrationBuilder.DropColumn(
                name: "Commercial_BBLCFk",
                table: "Procurement_PurchaseOrder");

            migrationBuilder.DropColumn(
                name: "Commercial_ECIFk",
                table: "Merchandising_BuyerOrder");

            migrationBuilder.DropColumn(
                name: "Address",
                table: "Common_LienBank");

            migrationBuilder.DropColumn(
                name: "Common_CountryFK",
                table: "Common_LienBank");

            migrationBuilder.DropColumn(
                name: "ContactNo",
                table: "Common_LienBank");

            migrationBuilder.DropColumn(
                name: "SwiftCode",
                table: "Common_LienBank");

            migrationBuilder.DropColumn(
                name: "Address",
                table: "Common_CompanyBank");

            migrationBuilder.DropColumn(
                name: "Common_CountryFK",
                table: "Common_CompanyBank");

            migrationBuilder.DropColumn(
                name: "ContactNo",
                table: "Common_CompanyBank");

            migrationBuilder.DropColumn(
                name: "SwiftCode",
                table: "Common_CompanyBank");

            migrationBuilder.DropColumn(
                name: "Address",
                table: "Common_BuyerBank");

            migrationBuilder.DropColumn(
                name: "Common_CountryFK",
                table: "Common_BuyerBank");

            migrationBuilder.DropColumn(
                name: "ContactNo",
                table: "Common_BuyerBank");

            migrationBuilder.DropColumn(
                name: "SwiftCode",
                table: "Common_BuyerBank");

            migrationBuilder.DropColumn(
                name: "IsApproved",
                table: "Commercial_BBLC");

            migrationBuilder.RenameColumn(
                name: "ShortName",
                table: "Common_CompanySetup",
                newName: "CompanyShortName");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "Common_CompanySetup",
                newName: "CompanyName");

            migrationBuilder.RenameColumn(
                name: "Address",
                table: "Common_CompanySetup",
                newName: "AddressLine4");

            migrationBuilder.RenameColumn(
                name: "ValueAmendment",
                table: "Commercial_ECI",
                newName: "TotalValuePO");

            migrationBuilder.RenameColumn(
                name: "Common_BuyerNotifyPartyFk",
                table: "Commercial_ECI",
                newName: "Common_UDFK");

            migrationBuilder.RenameColumn(
                name: "Common_BuyerFK",
                table: "Commercial_ECI",
                newName: "Common_BuyerPOFK");

            migrationBuilder.RenameColumn(
                name: "Commercial_UDFk",
                table: "Commercial_ECI",
                newName: "Common_ApplicantFK");

            migrationBuilder.RenameColumn(
                name: "BaseHeadId",
                table: "Commercial_ECI",
                newName: "AccHeadID");

            migrationBuilder.RenameColumn(
                name: "Status",
                table: "Commercial_BBLC",
                newName: "ReqQty");

            migrationBuilder.RenameColumn(
                name: "ScanedFile",
                table: "Commercial_BBLC",
                newName: "Payable");

            migrationBuilder.RenameColumn(
                name: "Commercial_UDFk",
                table: "Commercial_BBLC",
                newName: "Common_UnitFK");

            migrationBuilder.RenameColumn(
                name: "Commercial_LCOreginFK",
                table: "Commercial_BBLC",
                newName: "Common_UDFK");

            migrationBuilder.RenameColumn(
                name: "Amendment",
                table: "Commercial_BBLC",
                newName: "TotalValuePO");

            migrationBuilder.AddColumn<string>(
                name: "AddressLine1",
                table: "Common_CompanySetup",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AddressLine2",
                table: "Common_CompanySetup",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AddressLine3",
                table: "Common_CompanySetup",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Common_POFK",
                table: "Commercial_BBLC",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Common_RawItemFK",
                table: "Commercial_BBLC",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "Commercial_BBLC",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "LCDoc",
                table: "Commercial_BBLC",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Orgin",
                table: "Commercial_BBLC",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Commercial_Buyer",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Remarks = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Commercial_Buyer", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Commercial_BuyerOrder",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    Agent = table.Column<string>(nullable: true),
                    BuyerPO = table.Column<string>(nullable: true),
                    Commission = table.Column<decimal>(nullable: false),
                    Common_CurrencyFK = table.Column<int>(nullable: false),
                    Common_ECIFK = table.Column<int>(nullable: false),
                    Common_TCTitleFK = table.Column<int>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    InspectionAgent = table.Column<string>(nullable: true),
                    OrderDate = table.Column<DateTime>(nullable: false),
                    PaymentMethod = table.Column<string>(nullable: true),
                    PaymentTerms = table.Column<string>(nullable: true),
                    Reference = table.Column<string>(nullable: true),
                    Remarks = table.Column<string>(nullable: true),
                    SeasonYear = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Commercial_BuyerOrder", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Commercial_PurchaseOrder",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    AlsoATransfer = table.Column<bool>(nullable: false),
                    AuthorizeDate = table.Column<DateTime>(nullable: true),
                    CID = table.Column<string>(nullable: true),
                    Commercial_PurchaseOrderID = table.Column<int>(nullable: true),
                    Common_BTBFK = table.Column<int>(nullable: false),
                    Common_CurrencyFK = table.Column<int>(nullable: false),
                    Common_SupplierFK = table.Column<int>(nullable: false),
                    Common_TCTitleFK = table.Column<int>(nullable: false),
                    Currency = table.Column<string>(nullable: true),
                    CurrencyRate = table.Column<decimal>(nullable: false),
                    Date = table.Column<DateTime>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    IsAuthorize = table.Column<string>(nullable: true),
                    IsComplete = table.Column<bool>(nullable: false),
                    Mkt_BOMCID = table.Column<string>(nullable: true),
                    Mkt_BOMFK = table.Column<int>(nullable: false),
                    POValue = table.Column<decimal>(nullable: false),
                    PaymentCleared = table.Column<bool>(nullable: false),
                    PaymentType = table.Column<string>(nullable: true),
                    Remarks = table.Column<string>(nullable: true),
                    RequisitionFK = table.Column<int>(nullable: true),
                    Status = table.Column<int>(nullable: true),
                    StatusFK = table.Column<int>(nullable: true),
                    Style = table.Column<string>(nullable: true),
                    Supplier = table.Column<string>(nullable: true),
                    TermsCondition = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Commercial_PurchaseOrder", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Commercial_PurchaseOrder_Commercial_PurchaseOrder_Commercial_PurchaseOrderID",
                        column: x => x.Commercial_PurchaseOrderID,
                        principalTable: "Commercial_PurchaseOrder",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Commercial_UDBuyerOrder",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    CID = table.Column<string>(nullable: true),
                    Common_BuyerFk = table.Column<int>(nullable: false),
                    Date = table.Column<DateTime>(nullable: false),
                    LimitDeclaration = table.Column<decimal>(nullable: false),
                    Ref = table.Column<string>(nullable: true),
                    Remarks = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Value = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Commercial_UDBuyerOrder", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Common_Order",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    Common_BuyerFk = table.Column<int>(nullable: false),
                    OrderNo = table.Column<string>(nullable: true),
                    Remarks = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_Order", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Common_Style",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Remarks = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_Style", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Common_UD",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Remarks = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_UD", x => x.ID);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Commercial_PurchaseOrder_Commercial_PurchaseOrderID",
                table: "Commercial_PurchaseOrder",
                column: "Commercial_PurchaseOrderID");
        }
    }
}
