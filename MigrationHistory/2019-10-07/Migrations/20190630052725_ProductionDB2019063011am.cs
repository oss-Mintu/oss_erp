﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Erp.Infrastructure.Migrations
{
    public partial class ProductionDB2019063011am : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Merchandising_StyleShipmentSchedule_Common_CountryPort_Common_CountryPortFK",
                table: "Merchandising_StyleShipmentSchedule");

            migrationBuilder.DropIndex(
                name: "IX_Merchandising_StyleShipmentSchedule_Common_CountryPortFK",
                table: "Merchandising_StyleShipmentSchedule");

            migrationBuilder.DropColumn(
                name: "Approved",
                table: "Accounting_Transaction");

            migrationBuilder.DropColumn(
                name: "Finalized",
                table: "Accounting_Transaction");

            migrationBuilder.AddColumn<int>(
                name: "LineInputQuantity",
                table: "Prod_ReferencePlan",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<decimal>(
                name: "UnitPrice",
                table: "Merchandising_StyleSlave",
                type: "decimal(18,5)",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "TotalPrice",
                table: "Merchandising_StyleSlave",
                type: "decimal(18,5)",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "UnitPrice",
                table: "Merchandising_Style",
                type: "decimal(18,5)",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "PackPrice",
                table: "Merchandising_Style",
                type: "decimal(18,5)",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.AddColumn<string>(
                name: "Kimball",
                table: "Merchandising_Style",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ReferenceNo",
                table: "Merchandising_Style",
                nullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "UnitPrice",
                table: "Merchandising_CBSSlave",
                type: "decimal(18,5)",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "TotalPrice",
                table: "Merchandising_CBSSlave",
                type: "decimal(18,5)",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "BuyerOrderPOValue",
                table: "Merchandising_BuyerOrder",
                type: "decimal(18,5)",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.AddColumn<int>(
                name: "Status",
                table: "Accounting_Transaction",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LineInputQuantity",
                table: "Prod_ReferencePlan");

            migrationBuilder.DropColumn(
                name: "Kimball",
                table: "Merchandising_Style");

            migrationBuilder.DropColumn(
                name: "ReferenceNo",
                table: "Merchandising_Style");

            migrationBuilder.DropColumn(
                name: "Status",
                table: "Accounting_Transaction");

            migrationBuilder.AlterColumn<decimal>(
                name: "UnitPrice",
                table: "Merchandising_StyleSlave",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,5)");

            migrationBuilder.AlterColumn<decimal>(
                name: "TotalPrice",
                table: "Merchandising_StyleSlave",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,5)");

            migrationBuilder.AlterColumn<decimal>(
                name: "UnitPrice",
                table: "Merchandising_Style",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,5)");

            migrationBuilder.AlterColumn<decimal>(
                name: "PackPrice",
                table: "Merchandising_Style",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,5)");

            migrationBuilder.AlterColumn<decimal>(
                name: "UnitPrice",
                table: "Merchandising_CBSSlave",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,5)");

            migrationBuilder.AlterColumn<decimal>(
                name: "TotalPrice",
                table: "Merchandising_CBSSlave",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,5)");

            migrationBuilder.AlterColumn<decimal>(
                name: "BuyerOrderPOValue",
                table: "Merchandising_BuyerOrder",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18,5)");

            migrationBuilder.AddColumn<bool>(
                name: "Approved",
                table: "Accounting_Transaction",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Finalized",
                table: "Accounting_Transaction",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateIndex(
                name: "IX_Merchandising_StyleShipmentSchedule_Common_CountryPortFK",
                table: "Merchandising_StyleShipmentSchedule",
                column: "Common_CountryPortFK");

            migrationBuilder.AddForeignKey(
                name: "FK_Merchandising_StyleShipmentSchedule_Common_CountryPort_Common_CountryPortFK",
                table: "Merchandising_StyleShipmentSchedule",
                column: "Common_CountryPortFK",
                principalTable: "Common_CountryPort",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
