﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Erp.Infrastructure.Migrations
{
    public partial class ProductionDB2019072303pm : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TergetEfficiency",
                table: "Prod_DailyAchivement");

            migrationBuilder.AddColumn<int>(
                name: "CommonStoreFK",
                table: "Store_StockRegister",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "StoreInType",
                table: "Store_StockOut",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "StoreInType",
                table: "Store_StockIn",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "TargetEfficiency",
                table: "Prod_DailyAchivement",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "ExtraRate",
                table: "Plan_PlanConfig",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "ExtraRate",
                table: "Plan_MasterOrderPlan",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "ExtraRate",
                table: "Plan_DraftOrderPlan",
                type: "decimal(18,2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<int>(
                name: "BaseHeadId",
                table: "Mis_IncomeStatement",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ChartTableId",
                table: "Mis_IncomeStatement",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "BaseHeadId",
                table: "Mis_BalanceSheet",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ChartTableId",
                table: "Mis_BalanceSheet",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "SelfReferenceFK",
                table: "Merchandising_CBSSlave",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Common_BuyerFK",
                table: "Merchandising_CBS",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "Reference",
                table: "Merchandising_CBS",
                nullable: true);

            migrationBuilder.AddColumn<byte[]>(
                name: "Image",
                table: "Common_CompanySetup",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "HRMS_JobDescription",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    User_DepartmentFK = table.Column<int>(nullable: true),
                    HRMS_DesignationFK = table.Column<int>(nullable: true),
                    JobDescription = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HRMS_JobDescription", x => x.ID);
                    table.ForeignKey(
                        name: "FK_HRMS_JobDescription_HRMS_Designation_HRMS_DesignationFK",
                        column: x => x.HRMS_DesignationFK,
                        principalTable: "HRMS_Designation",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_HRMS_JobDescription_User_Department_User_DepartmentFK",
                        column: x => x.User_DepartmentFK,
                        principalTable: "User_Department",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Inventorydb",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    StoreTypeFk = table.Column<int>(nullable: false),
                    TransactionType = table.Column<int>(nullable: false),
                    Procurement_PurchaseInvoiceFK = table.Column<int>(nullable: true),
                    Procurement_PurchaseOrderSlaveID = table.Column<int>(nullable: true),
                    Merchandising_StyleID = table.Column<int>(nullable: true),
                    Merchandising_StyleSlaveID = table.Column<int>(nullable: true),
                    Merchandising_BOFID = table.Column<int>(nullable: true),
                    Store_StockInFK = table.Column<int>(nullable: true),
                    Common_RawItemFK = table.Column<int>(nullable: false),
                    Amount = table.Column<decimal>(nullable: false),
                    Price = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Inventorydb", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Mis_Log",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    TableName = table.Column<string>(nullable: true),
                    Status = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Mis_Log", x => x.ID);
                });

            migrationBuilder.CreateIndex(
                name: "IX_HRMS_JobDescription_HRMS_DesignationFK",
                table: "HRMS_JobDescription",
                column: "HRMS_DesignationFK");

            migrationBuilder.CreateIndex(
                name: "IX_HRMS_JobDescription_User_DepartmentFK",
                table: "HRMS_JobDescription",
                column: "User_DepartmentFK");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "HRMS_JobDescription");

            migrationBuilder.DropTable(
                name: "Inventorydb");

            migrationBuilder.DropTable(
                name: "Mis_Log");

            migrationBuilder.DropColumn(
                name: "CommonStoreFK",
                table: "Store_StockRegister");

            migrationBuilder.DropColumn(
                name: "StoreInType",
                table: "Store_StockOut");

            migrationBuilder.DropColumn(
                name: "StoreInType",
                table: "Store_StockIn");

            migrationBuilder.DropColumn(
                name: "TargetEfficiency",
                table: "Prod_DailyAchivement");

            migrationBuilder.DropColumn(
                name: "ExtraRate",
                table: "Plan_PlanConfig");

            migrationBuilder.DropColumn(
                name: "ExtraRate",
                table: "Plan_MasterOrderPlan");

            migrationBuilder.DropColumn(
                name: "ExtraRate",
                table: "Plan_DraftOrderPlan");

            migrationBuilder.DropColumn(
                name: "BaseHeadId",
                table: "Mis_IncomeStatement");

            migrationBuilder.DropColumn(
                name: "ChartTableId",
                table: "Mis_IncomeStatement");

            migrationBuilder.DropColumn(
                name: "BaseHeadId",
                table: "Mis_BalanceSheet");

            migrationBuilder.DropColumn(
                name: "ChartTableId",
                table: "Mis_BalanceSheet");

            migrationBuilder.DropColumn(
                name: "SelfReferenceFK",
                table: "Merchandising_CBSSlave");

            migrationBuilder.DropColumn(
                name: "Common_BuyerFK",
                table: "Merchandising_CBS");

            migrationBuilder.DropColumn(
                name: "Reference",
                table: "Merchandising_CBS");

            migrationBuilder.DropColumn(
                name: "Image",
                table: "Common_CompanySetup");

            migrationBuilder.AddColumn<decimal>(
                name: "TergetEfficiency",
                table: "Prod_DailyAchivement",
                nullable: false,
                defaultValue: 0m);
        }
    }
}
