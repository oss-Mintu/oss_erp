﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Erp.Infrastructure.Migrations
{
    public partial class ProductionDB2019071510am : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Common_SizeFK",
                table: "Store_StockOut",
                newName: "Store_RequisitionSlaveFK");

            migrationBuilder.AddColumn<decimal>(
                name: "Common_Size",
                table: "Store_StockOut",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<int>(
                name: "Common_ColorFK",
                table: "Store_RequisitionSlave",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<decimal>(
                name: "Common_Size",
                table: "Store_RequisitionSlave",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<int>(
                name: "Merchandising_StyleFK",
                table: "Store_RequisitionSlave",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Merchandising_StyleSlaveFK",
                table: "Store_RequisitionSlave",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Procurement_PurchaseOrderSlaveFK",
                table: "Store_RequisitionSlave",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Common_Size",
                table: "Store_StockOut");

            migrationBuilder.DropColumn(
                name: "Common_ColorFK",
                table: "Store_RequisitionSlave");

            migrationBuilder.DropColumn(
                name: "Common_Size",
                table: "Store_RequisitionSlave");

            migrationBuilder.DropColumn(
                name: "Merchandising_StyleFK",
                table: "Store_RequisitionSlave");

            migrationBuilder.DropColumn(
                name: "Merchandising_StyleSlaveFK",
                table: "Store_RequisitionSlave");

            migrationBuilder.DropColumn(
                name: "Procurement_PurchaseOrderSlaveFK",
                table: "Store_RequisitionSlave");

            migrationBuilder.RenameColumn(
                name: "Store_RequisitionSlaveFK",
                table: "Store_StockOut",
                newName: "Common_SizeFK");
        }
    }
}
