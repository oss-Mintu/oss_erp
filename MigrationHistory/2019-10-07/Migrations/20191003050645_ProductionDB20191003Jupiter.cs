﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Erp.Infrastructure.Migrations
{
    public partial class ProductionDB20191003Jupiter : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "StyleName",
                table: "Store_ItemWiseConsumption",
                nullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "Date",
                table: "Mis_ShipmentStatus",
                nullable: true,
                oldClrType: typeof(DateTime));

            migrationBuilder.AddColumn<int>(
                name: "StyleID",
                table: "Common_Notification",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "Commercial_BBLCPaymentInformation",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Commercial_BBLCFk = table.Column<int>(nullable: false),
                    B2bLCPayment = table.Column<decimal>(nullable: false),
                    Date = table.Column<DateTime>(nullable: false),
                    IsApproved = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Commercial_BBLCPaymentInformation", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Store_ItemWiseConsumptionAllStyle",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    StyleName = table.Column<string>(nullable: true),
                    Merchandising_StyleFK = table.Column<int>(nullable: false),
                    Common_RawItemFK = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    UnitName = table.Column<string>(nullable: true),
                    ConsumptionDz = table.Column<double>(nullable: false),
                    OrderConsumQty = table.Column<double>(nullable: false),
                    FinishQtyDz = table.Column<double>(nullable: false),
                    TotalRecMain = table.Column<double>(nullable: false),
                    TotalRecCutting = table.Column<double>(nullable: false),
                    TotalRecSewing = table.Column<double>(nullable: false),
                    TotalRecIroning = table.Column<double>(nullable: false),
                    TotalRecPacking = table.Column<double>(nullable: false),
                    TotalRecFinish = table.Column<double>(nullable: false),
                    TotalOutMain = table.Column<double>(nullable: false),
                    TotalOutCutting = table.Column<double>(nullable: false),
                    TotalOutSewing = table.Column<double>(nullable: false),
                    TotalOutIroning = table.Column<double>(nullable: false),
                    TotalOutPacking = table.Column<double>(nullable: false),
                    TotalOutFinish = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Store_ItemWiseConsumptionAllStyle", x => x.ID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Commercial_BBLCPaymentInformation");

            migrationBuilder.DropTable(
                name: "Store_ItemWiseConsumptionAllStyle");

            migrationBuilder.DropColumn(
                name: "StyleName",
                table: "Store_ItemWiseConsumption");

            migrationBuilder.DropColumn(
                name: "StyleID",
                table: "Common_Notification");

            migrationBuilder.AlterColumn<DateTime>(
                name: "Date",
                table: "Mis_ShipmentStatus",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldNullable: true);
        }
    }
}
