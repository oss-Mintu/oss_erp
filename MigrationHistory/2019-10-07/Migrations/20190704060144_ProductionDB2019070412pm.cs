﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Erp.Infrastructure.Migrations
{
    public partial class ProductionDB2019070412pm : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "OriginType",
                table: "Procurement_PurchaseRequisition",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "PRType",
                table: "Procurement_PurchaseRequisition",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "OriginType",
                table: "Procurement_PurchaseOrder",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "OriginType",
                table: "Procurement_PurchaseRequisition");

            migrationBuilder.DropColumn(
                name: "PRType",
                table: "Procurement_PurchaseRequisition");

            migrationBuilder.DropColumn(
                name: "OriginType",
                table: "Procurement_PurchaseOrder");
        }
    }
}
