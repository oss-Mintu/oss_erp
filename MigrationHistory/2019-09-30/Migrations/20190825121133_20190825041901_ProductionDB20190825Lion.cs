﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Erp.Infrastructure.Migrations
{
    public partial class _20190825041901_ProductionDB20190825Lion : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "StrChallanNo",
                table: "Procurement_PurchaseInvoiceSlave",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "InvoiceMatureDate",
                table: "Procurement_PurchaseInvoice",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "SupplierInvoiceNo",
                table: "Procurement_PurchaseInvoice",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Store_StockOutFK",
                table: "Inventorydb",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "LineRoleId",
                table: "HRMS_Employee",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ReportingDesignationId",
                table: "HRMS_Employee",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "StrChallanNo",
                table: "Procurement_PurchaseInvoiceSlave");

            migrationBuilder.DropColumn(
                name: "InvoiceMatureDate",
                table: "Procurement_PurchaseInvoice");

            migrationBuilder.DropColumn(
                name: "SupplierInvoiceNo",
                table: "Procurement_PurchaseInvoice");

            migrationBuilder.DropColumn(
                name: "Store_StockOutFK",
                table: "Inventorydb");

            migrationBuilder.DropColumn(
                name: "LineRoleId",
                table: "HRMS_Employee");

            migrationBuilder.DropColumn(
                name: "ReportingDesignationId",
                table: "HRMS_Employee");
        }
    }
}
