﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Erp.Infrastructure.Migrations
{
    public partial class ProductionDB2019071005pm : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropForeignKey(
            //    name: "FK_Procurement_PurchaseInvoiceSlave_Procurement_PurchaseOrder_Procurement_PurchaseOrderFK",
            //    table: "Procurement_PurchaseInvoiceSlave");

            //migrationBuilder.DropIndex(
            //    name: "IX_Procurement_PurchaseInvoiceSlave_Procurement_PurchaseOrderFK",
            //    table: "Procurement_PurchaseInvoiceSlave");

            migrationBuilder.RenameColumn(
                name: "Procurement_PurchaseOrderFK",
                table: "Procurement_PurchaseInvoiceSlave",
                newName: "Procurement_PurchaseOrderSlaveID");

            migrationBuilder.AddColumn<int>(
                name: "Merchandising_BOFID",
                table: "Procurement_PurchaseInvoiceSlave",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Merchandising_StyleID",
                table: "Procurement_PurchaseInvoiceSlave",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Merchandising_StyleSlaveID",
                table: "Procurement_PurchaseInvoiceSlave",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Merchandising_BOFID",
                table: "Procurement_PurchaseInvoiceSlave");

            migrationBuilder.DropColumn(
                name: "Merchandising_StyleID",
                table: "Procurement_PurchaseInvoiceSlave");

            migrationBuilder.DropColumn(
                name: "Merchandising_StyleSlaveID",
                table: "Procurement_PurchaseInvoiceSlave");

            migrationBuilder.RenameColumn(
                name: "Procurement_PurchaseOrderSlaveID",
                table: "Procurement_PurchaseInvoiceSlave",
                newName: "Procurement_PurchaseOrderFK");

            //migrationBuilder.CreateIndex(
            //    name: "IX_Procurement_PurchaseInvoiceSlave_Procurement_PurchaseOrderFK",
            //    table: "Procurement_PurchaseInvoiceSlave",
            //    column: "Procurement_PurchaseOrderFK");

            //migrationBuilder.AddForeignKey(
            //    name: "FK_Procurement_PurchaseInvoiceSlave_Procurement_PurchaseOrder_Procurement_PurchaseOrderFK",
            //    table: "Procurement_PurchaseInvoiceSlave",
            //    column: "Procurement_PurchaseOrderFK",
            //    principalTable: "Procurement_PurchaseOrder",
            //    principalColumn: "ID",
            //    onDelete: ReferentialAction.Restrict);
        }
    }
}
