﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Erp.Infrastructure.Migrations
{
    public partial class ProductionDB20190904Giraffe : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Degree",
                table: "HRMS_EmployeeEducation");

            migrationBuilder.AddColumn<bool>(
                name: "IsVarified",
                table: "HRMS_EmployeeReference",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "HRMS_EducationalDegreeFK",
                table: "HRMS_EmployeeEducation",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "Common_RejectType",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    RejectOne = table.Column<string>(nullable: true),
                    RejectTwo = table.Column<string>(nullable: true),
                    RejectThree = table.Column<string>(nullable: true),
                    RejectFour = table.Column<string>(nullable: true),
                    RejectFive = table.Column<string>(nullable: true),
                    RejectSix = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_RejectType", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "HRMS_EducationalDegree",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HRMS_EducationalDegree", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "HRMS_EmployeeNominee",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    FatherName = table.Column<string>(nullable: true),
                    MotherName = table.Column<string>(nullable: true),
                    Relation = table.Column<string>(nullable: true),
                    NID = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    Contact = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    HRMS_EmployeeFK = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HRMS_EmployeeNominee", x => x.ID);
                    table.ForeignKey(
                        name: "FK_HRMS_EmployeeNominee_HRMS_Employee_HRMS_EmployeeFK",
                        column: x => x.HRMS_EmployeeFK,
                        principalTable: "HRMS_Employee",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_HRMS_EmployeeEducation_HRMS_EducationalDegreeFK",
                table: "HRMS_EmployeeEducation",
                column: "HRMS_EducationalDegreeFK");

            migrationBuilder.CreateIndex(
                name: "IX_HRMS_EmployeeNominee_HRMS_EmployeeFK",
                table: "HRMS_EmployeeNominee",
                column: "HRMS_EmployeeFK");

            migrationBuilder.AddForeignKey(
                name: "FK_HRMS_EmployeeEducation_HRMS_EducationalDegree_HRMS_EducationalDegreeFK",
                table: "HRMS_EmployeeEducation",
                column: "HRMS_EducationalDegreeFK",
                principalTable: "HRMS_EducationalDegree",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_HRMS_EmployeeEducation_HRMS_EducationalDegree_HRMS_EducationalDegreeFK",
                table: "HRMS_EmployeeEducation");

            migrationBuilder.DropTable(
                name: "Common_RejectType");

            migrationBuilder.DropTable(
                name: "HRMS_EducationalDegree");

            migrationBuilder.DropTable(
                name: "HRMS_EmployeeNominee");

            migrationBuilder.DropIndex(
                name: "IX_HRMS_EmployeeEducation_HRMS_EducationalDegreeFK",
                table: "HRMS_EmployeeEducation");

            migrationBuilder.DropColumn(
                name: "IsVarified",
                table: "HRMS_EmployeeReference");

            migrationBuilder.DropColumn(
                name: "HRMS_EducationalDegreeFK",
                table: "HRMS_EmployeeEducation");

            migrationBuilder.AddColumn<string>(
                name: "Degree",
                table: "HRMS_EmployeeEducation",
                nullable: true);
        }
    }
}
