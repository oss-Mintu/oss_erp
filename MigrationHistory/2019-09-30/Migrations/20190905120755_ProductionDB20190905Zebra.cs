﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Erp.Infrastructure.Migrations
{
    public partial class ProductionDB20190905Zebra : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Procurement_PurchaseOrder_HRMS_Employee_HRMS_EmployeeFK",
                table: "Procurement_PurchaseOrder");

            migrationBuilder.DropForeignKey(
                name: "FK_Procurement_PurchaseRequisitionSlave_HRMS_Employee_HRMS_EmployeeFK",
                table: "Procurement_PurchaseRequisitionSlave");

            migrationBuilder.DropForeignKey(
                name: "FK_Store_RequisitionSlave_HRMS_Employee_HRMS_EmployeeFK",
                table: "Store_RequisitionSlave");

            migrationBuilder.DropIndex(
                name: "IX_Store_RequisitionSlave_HRMS_EmployeeFK",
                table: "Store_RequisitionSlave");

            migrationBuilder.DropIndex(
                name: "IX_Procurement_PurchaseRequisitionSlave_HRMS_EmployeeFK",
                table: "Procurement_PurchaseRequisitionSlave");

            migrationBuilder.DropIndex(
                name: "IX_Procurement_PurchaseOrder_HRMS_EmployeeFK",
                table: "Procurement_PurchaseOrder");

            migrationBuilder.DropColumn(
                name: "HRMS_EmployeeFK",
                table: "Procurement_PurchaseRequisitionSlave");

            migrationBuilder.DropColumn(
                name: "HRMS_EmployeeFK",
                table: "Procurement_PurchaseOrder");

            migrationBuilder.AddColumn<string>(
                name: "VarifyingComments",
                table: "HRMS_EmployeeReference",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Common_BankFK",
                table: "Common_LienBank",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ContactPerson",
                table: "Common_LienBank",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Common_BankFK",
                table: "Common_CompanyBank",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ContactPerson",
                table: "Common_CompanyBank",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Common_BankFK",
                table: "Common_BuyerBank",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ContactPerson",
                table: "Common_BuyerBank",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Common_Bank",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    SwiftCode = table.Column<string>(nullable: true),
                    Flag = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_Bank", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Common_Notification",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    TStyle = table.Column<int>(nullable: false),
                    NStyle = table.Column<int>(nullable: false),
                    IncompleteBOM = table.Column<int>(nullable: false),
                    CompleteBOM = table.Column<int>(nullable: false),
                    TPR = table.Column<int>(nullable: false),
                    TDraftPR = table.Column<int>(nullable: false),
                    TSubmittedPR = table.Column<int>(nullable: false),
                    PRApproved1 = table.Column<int>(nullable: false),
                    PRApproved2 = table.Column<int>(nullable: false),
                    PRApproved3 = table.Column<int>(nullable: false),
                    PRHold = table.Column<int>(nullable: false),
                    PartialPO = table.Column<int>(nullable: false),
                    FullPO = table.Column<int>(nullable: false),
                    TDratfPO = table.Column<int>(nullable: false),
                    TSubmittedPO = table.Column<int>(nullable: false),
                    POApproved1 = table.Column<int>(nullable: false),
                    POApproved2 = table.Column<int>(nullable: false),
                    POApproved3 = table.Column<int>(nullable: false),
                    POHold = table.Column<int>(nullable: false),
                    PartialReceived = table.Column<int>(nullable: false),
                    TGoodReceived = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_Notification", x => x.ID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Common_Bank");

            migrationBuilder.DropTable(
                name: "Common_Notification");

            migrationBuilder.DropColumn(
                name: "VarifyingComments",
                table: "HRMS_EmployeeReference");

            migrationBuilder.DropColumn(
                name: "Common_BankFK",
                table: "Common_LienBank");

            migrationBuilder.DropColumn(
                name: "ContactPerson",
                table: "Common_LienBank");

            migrationBuilder.DropColumn(
                name: "Common_BankFK",
                table: "Common_CompanyBank");

            migrationBuilder.DropColumn(
                name: "ContactPerson",
                table: "Common_CompanyBank");

            migrationBuilder.DropColumn(
                name: "Common_BankFK",
                table: "Common_BuyerBank");

            migrationBuilder.DropColumn(
                name: "ContactPerson",
                table: "Common_BuyerBank");

            migrationBuilder.AddColumn<int>(
                name: "HRMS_EmployeeFK",
                table: "Procurement_PurchaseRequisitionSlave",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "HRMS_EmployeeFK",
                table: "Procurement_PurchaseOrder",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Store_RequisitionSlave_HRMS_EmployeeFK",
                table: "Store_RequisitionSlave",
                column: "HRMS_EmployeeFK");

            migrationBuilder.CreateIndex(
                name: "IX_Procurement_PurchaseRequisitionSlave_HRMS_EmployeeFK",
                table: "Procurement_PurchaseRequisitionSlave",
                column: "HRMS_EmployeeFK");

            migrationBuilder.CreateIndex(
                name: "IX_Procurement_PurchaseOrder_HRMS_EmployeeFK",
                table: "Procurement_PurchaseOrder",
                column: "HRMS_EmployeeFK");

            migrationBuilder.AddForeignKey(
                name: "FK_Procurement_PurchaseOrder_HRMS_Employee_HRMS_EmployeeFK",
                table: "Procurement_PurchaseOrder",
                column: "HRMS_EmployeeFK",
                principalTable: "HRMS_Employee",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Procurement_PurchaseRequisitionSlave_HRMS_Employee_HRMS_EmployeeFK",
                table: "Procurement_PurchaseRequisitionSlave",
                column: "HRMS_EmployeeFK",
                principalTable: "HRMS_Employee",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Store_RequisitionSlave_HRMS_Employee_HRMS_EmployeeFK",
                table: "Store_RequisitionSlave",
                column: "HRMS_EmployeeFK",
                principalTable: "HRMS_Employee",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
