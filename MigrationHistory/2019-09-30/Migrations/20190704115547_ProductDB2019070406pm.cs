﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Erp.Infrastructure.Migrations
{
    public partial class ProductDB2019070406pm : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_HRMS_Employee_HRMS_BusinessUnit_HRMS_BusinessUnitFK",
                table: "HRMS_Employee");

            migrationBuilder.DropForeignKey(
                name: "FK_HRMS_Employee_HRMS_Unit_HRMS_UnitFK",
                table: "HRMS_Employee");

            migrationBuilder.DropForeignKey(
                name: "FK_HRMS_Employee_User_Department_User_DepartmentFK",
                table: "HRMS_Employee");

            migrationBuilder.DropIndex(
                name: "IX_HRMS_Employee_HRMS_BusinessUnitFK",
                table: "HRMS_Employee");

            migrationBuilder.DropIndex(
                name: "IX_HRMS_Employee_HRMS_UnitFK",
                table: "HRMS_Employee");

            migrationBuilder.DropIndex(
                name: "IX_HRMS_Employee_User_DepartmentFK",
                table: "HRMS_Employee");

            migrationBuilder.DropColumn(
                name: "HRMS_BusinessUnitFK",
                table: "HRMS_Employee");

            migrationBuilder.DropColumn(
                name: "HRMS_UnitFK",
                table: "HRMS_Employee");

            migrationBuilder.DropColumn(
                name: "User_DepartmentFK",
                table: "HRMS_Employee");

            migrationBuilder.AddColumn<string>(
                name: "ContactPerson",
                table: "Common_Supplier",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Common_Agent",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    ContactPerson = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_Agent", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Common_InspectionAgent",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    ContactPerson = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_InspectionAgent", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Merchandising_Packing",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Merchandising_StyleShipmentScheduleFK = table.Column<int>(nullable: false),
                    Merchandising_StyleShipmentRatioFK = table.Column<int>(nullable: false),
                    Merchandising_StyleFK = table.Column<int>(nullable: false),
                    CartonRetio = table.Column<decimal>(nullable: false),
                    GrossWeight = table.Column<decimal>(nullable: false),
                    NetWeight = table.Column<decimal>(nullable: false),
                    CartonMeasurement = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Merchandising_Packing", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Store_SRequisition",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    CID = table.Column<string>(nullable: true),
                    PRType = table.Column<int>(nullable: false),
                    OriginType = table.Column<int>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    Status = table.Column<int>(nullable: false),
                    CreationDate = table.Column<DateTime>(nullable: false),
                    CancellationRemaks = table.Column<string>(nullable: true),
                    ClosedByUserFK = table.Column<int>(nullable: true),
                    StyleId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Store_SRequisition", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Store_SRequisition_User_User_ClosedByUserFK",
                        column: x => x.ClosedByUserFK,
                        principalTable: "User_User",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Store_SRequisitionSlave",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Store_SRequisitionFK = table.Column<int>(nullable: false),
                    Store_RequisitionFK = table.Column<int>(nullable: true),
                    HRMS_EmployeeFK = table.Column<int>(nullable: true),
                    RequiredDate = table.Column<DateTime>(nullable: false),
                    Common_RawItemFK = table.Column<int>(nullable: false),
                    Merchandising_StyleID = table.Column<int>(nullable: true),
                    Merchandising_StyleSlaveFK = table.Column<int>(nullable: true),
                    Merchandising_BOFFK = table.Column<int>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    RequisitionQuantity = table.Column<double>(nullable: false),
                    InspectionRequired = table.Column<bool>(nullable: false),
                    SupplierNames = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Store_SRequisitionSlave", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Store_SRequisitionSlave_Common_RawItem_Common_RawItemFK",
                        column: x => x.Common_RawItemFK,
                        principalTable: "Common_RawItem",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Store_SRequisitionSlave_HRMS_Employee_HRMS_EmployeeFK",
                        column: x => x.HRMS_EmployeeFK,
                        principalTable: "HRMS_Employee",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Store_SRequisitionSlave_Merchandising_BOF_Merchandising_BOFFK",
                        column: x => x.Merchandising_BOFFK,
                        principalTable: "Merchandising_BOF",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Store_SRequisitionSlave_Merchandising_StyleSlave_Merchandising_StyleSlaveFK",
                        column: x => x.Merchandising_StyleSlaveFK,
                        principalTable: "Merchandising_StyleSlave",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Store_SRequisitionSlave_Store_SRequisition_Store_RequisitionFK",
                        column: x => x.Store_RequisitionFK,
                        principalTable: "Store_SRequisition",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Store_SRequisition_ClosedByUserFK",
                table: "Store_SRequisition",
                column: "ClosedByUserFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_SRequisitionSlave_Common_RawItemFK",
                table: "Store_SRequisitionSlave",
                column: "Common_RawItemFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_SRequisitionSlave_HRMS_EmployeeFK",
                table: "Store_SRequisitionSlave",
                column: "HRMS_EmployeeFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_SRequisitionSlave_Merchandising_BOFFK",
                table: "Store_SRequisitionSlave",
                column: "Merchandising_BOFFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_SRequisitionSlave_Merchandising_StyleSlaveFK",
                table: "Store_SRequisitionSlave",
                column: "Merchandising_StyleSlaveFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_SRequisitionSlave_Store_RequisitionFK",
                table: "Store_SRequisitionSlave",
                column: "Store_RequisitionFK");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Common_Agent");

            migrationBuilder.DropTable(
                name: "Common_InspectionAgent");

            migrationBuilder.DropTable(
                name: "Merchandising_Packing");

            migrationBuilder.DropTable(
                name: "Store_SRequisitionSlave");

            migrationBuilder.DropTable(
                name: "Store_SRequisition");

            migrationBuilder.DropColumn(
                name: "ContactPerson",
                table: "Common_Supplier");

            migrationBuilder.AddColumn<int>(
                name: "HRMS_BusinessUnitFK",
                table: "HRMS_Employee",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "HRMS_UnitFK",
                table: "HRMS_Employee",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "User_DepartmentFK",
                table: "HRMS_Employee",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_HRMS_Employee_HRMS_BusinessUnitFK",
                table: "HRMS_Employee",
                column: "HRMS_BusinessUnitFK");

            migrationBuilder.CreateIndex(
                name: "IX_HRMS_Employee_HRMS_UnitFK",
                table: "HRMS_Employee",
                column: "HRMS_UnitFK");

            migrationBuilder.CreateIndex(
                name: "IX_HRMS_Employee_User_DepartmentFK",
                table: "HRMS_Employee",
                column: "User_DepartmentFK");

            migrationBuilder.AddForeignKey(
                name: "FK_HRMS_Employee_HRMS_BusinessUnit_HRMS_BusinessUnitFK",
                table: "HRMS_Employee",
                column: "HRMS_BusinessUnitFK",
                principalTable: "HRMS_BusinessUnit",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_HRMS_Employee_HRMS_Unit_HRMS_UnitFK",
                table: "HRMS_Employee",
                column: "HRMS_UnitFK",
                principalTable: "HRMS_Unit",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_HRMS_Employee_User_Department_User_DepartmentFK",
                table: "HRMS_Employee",
                column: "User_DepartmentFK",
                principalTable: "User_Department",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
