﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Erp.Infrastructure.Migrations
{
    public partial class ProductionDBTuesday2019080602 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ValueAmendment",
                table: "Commercial_ECI");

            migrationBuilder.DropColumn(
                name: "Amendment",
                table: "Commercial_BBLC");

            migrationBuilder.AddColumn<int>(
                name: "IsECIOrBB",
                table: "Common_LcType",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<bool>(
                name: "IsClose",
                table: "Commercial_ECI",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsClose",
                table: "Commercial_BBLC",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateTable(
                name: "Commercial_BBLCValueAmendment",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    ValueAmendment = table.Column<decimal>(nullable: false),
                    Commercial_BBLCFk = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Commercial_BBLCValueAmendment", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Commercial_ECIValueAmendment",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    ValueAmendment = table.Column<decimal>(nullable: false),
                    Commercial_ECIFk = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Commercial_ECIValueAmendment", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Store_QCInformation",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    CID = table.Column<string>(nullable: true),
                    DeviceId = table.Column<string>(nullable: true),
                    Merchandising_StyleIdFK = table.Column<int>(nullable: true),
                    Common_ColorFK = table.Column<int>(nullable: true),
                    Common_SizeFK = table.Column<int>(nullable: true),
                    Common_ProductionLineFK = table.Column<int>(nullable: true),
                    Merchandising_StyleSetPackFK = table.Column<int>(nullable: true),
                    QCQuantity = table.Column<decimal>(nullable: false),
                    RejectQuantityOne = table.Column<int>(nullable: false),
                    RejectQuantityTwo = table.Column<int>(nullable: false),
                    RejectQuantityThree = table.Column<int>(nullable: false),
                    RejectQuantityFour = table.Column<int>(nullable: false),
                    RejectQuantityFive = table.Column<int>(nullable: false),
                    RejectQuantitySix = table.Column<int>(nullable: false),
                    RejectQuantityTotal = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Store_QCInformation", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Store_QCInformation_Common_Color_Common_ColorFK",
                        column: x => x.Common_ColorFK,
                        principalTable: "Common_Color",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Store_QCInformation_Common_ProductionLine_Common_ProductionLineFK",
                        column: x => x.Common_ProductionLineFK,
                        principalTable: "Common_ProductionLine",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Store_QCInformation_Common_Size_Common_SizeFK",
                        column: x => x.Common_SizeFK,
                        principalTable: "Common_Size",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Store_QCInformation_Merchandising_Style_Merchandising_StyleIdFK",
                        column: x => x.Merchandising_StyleIdFK,
                        principalTable: "Merchandising_Style",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Store_QCInformation_Merchandising_StyleSetPack_Merchandising_StyleSetPackFK",
                        column: x => x.Merchandising_StyleSetPackFK,
                        principalTable: "Merchandising_StyleSetPack",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Store_QCInformation_Common_ColorFK",
                table: "Store_QCInformation",
                column: "Common_ColorFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_QCInformation_Common_ProductionLineFK",
                table: "Store_QCInformation",
                column: "Common_ProductionLineFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_QCInformation_Common_SizeFK",
                table: "Store_QCInformation",
                column: "Common_SizeFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_QCInformation_Merchandising_StyleIdFK",
                table: "Store_QCInformation",
                column: "Merchandising_StyleIdFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_QCInformation_Merchandising_StyleSetPackFK",
                table: "Store_QCInformation",
                column: "Merchandising_StyleSetPackFK");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Commercial_BBLCValueAmendment");

            migrationBuilder.DropTable(
                name: "Commercial_ECIValueAmendment");

            migrationBuilder.DropTable(
                name: "Store_QCInformation");

            migrationBuilder.DropColumn(
                name: "IsECIOrBB",
                table: "Common_LcType");

            migrationBuilder.DropColumn(
                name: "IsClose",
                table: "Commercial_ECI");

            migrationBuilder.DropColumn(
                name: "IsClose",
                table: "Commercial_BBLC");

            migrationBuilder.AddColumn<decimal>(
                name: "ValueAmendment",
                table: "Commercial_ECI",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "Amendment",
                table: "Commercial_BBLC",
                nullable: false,
                defaultValue: 0m);
        }
    }
}
