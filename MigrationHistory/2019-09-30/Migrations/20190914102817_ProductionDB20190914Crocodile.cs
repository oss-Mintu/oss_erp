﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Erp.Infrastructure.Migrations
{
    public partial class ProductionDB20190914Crocodile : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Commercial_ECIFK",
                table: "Shipment_BillOfExchange",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Merchandising_StyleSetPackFK",
                table: "Plan_SMVMasterLayout",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "QtyRate",
                table: "Plan_SMVMasterLayout",
                type: "decimal(18,5)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<int>(
                name: "Merchandising_StyleSetPackFK",
                table: "Plan_SMVDraftLayout",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "QtyRate",
                table: "Plan_SMVDraftLayout",
                type: "decimal(18,5)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<int>(
                name: "Merchandising_StyleSlaveFK_Fabric",
                table: "Merchandising_YarnCalculation",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Merchandising_StyleSlaveFK_Fabric",
                table: "Merchandising_BOF",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Commercial_ECIFK",
                table: "Shipment_BillOfExchange");

            migrationBuilder.DropColumn(
                name: "Merchandising_StyleSetPackFK",
                table: "Plan_SMVMasterLayout");

            migrationBuilder.DropColumn(
                name: "QtyRate",
                table: "Plan_SMVMasterLayout");

            migrationBuilder.DropColumn(
                name: "Merchandising_StyleSetPackFK",
                table: "Plan_SMVDraftLayout");

            migrationBuilder.DropColumn(
                name: "QtyRate",
                table: "Plan_SMVDraftLayout");

            migrationBuilder.DropColumn(
                name: "Merchandising_StyleSlaveFK_Fabric",
                table: "Merchandising_YarnCalculation");

            migrationBuilder.DropColumn(
                name: "Merchandising_StyleSlaveFK_Fabric",
                table: "Merchandising_BOF");
        }
    }
}
