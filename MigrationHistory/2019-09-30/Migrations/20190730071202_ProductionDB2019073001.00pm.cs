﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Erp.Infrastructure.Migrations
{
    public partial class ProductionDB201907300100pm : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Store_RequisitionSlave_Common_Color_Common_ColorFK",
                table: "Store_RequisitionSlave");

            migrationBuilder.DropForeignKey(
                name: "FK_Store_StockIn_Common_Color_Common_ColorFK",
                table: "Store_StockIn");

            migrationBuilder.DropForeignKey(
                name: "FK_Store_StockOut_Common_Color_Common_ColorFK",
                table: "Store_StockOut");

            migrationBuilder.DropForeignKey(
                name: "FK_Store_StockOutSlave_Common_Color_Common_ColorFK",
                table: "Store_StockOutSlave");

            migrationBuilder.DropTable(
                name: "Store_ReceivingSlave");

            migrationBuilder.DropTable(
                name: "StoreDispatchSlave");

            migrationBuilder.DropTable(
                name: "Store_Receiving");

            migrationBuilder.DropTable(
                name: "StoreDispatch");

            migrationBuilder.DropTable(
                name: "Store_Lot");

            migrationBuilder.AlterColumn<int>(
                name: "Common_ColorFK",
                table: "Store_StockOutSlave",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "Common_ColorFK",
                table: "Store_StockOut",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "Common_ColorFK",
                table: "Store_StockIn",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "Common_ColorFK",
                table: "Store_RequisitionSlave",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<decimal>(
                name: "InvoicedQuantity",
                table: "Shipment_ShipmentInvoiceSlave",
                nullable: false,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<decimal>(
                name: "DeliveryChallanQuantity",
                table: "Shipment_DeliveryChallanSlave",
                nullable: false,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<int>(
                name: "OriginType",
                table: "Procurement_PurchaseInvoice",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "Shipment_InvoicedBuyerNotifyParty",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Shipment_InvoiceFk = table.Column<int>(nullable: false),
                    Common_BuyerNotifyPartyFk = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Shipment_InvoicedBuyerNotifyParty", x => x.ID);
                });

            migrationBuilder.AddForeignKey(
                name: "FK_Store_RequisitionSlave_Common_Color_Common_ColorFK",
                table: "Store_RequisitionSlave",
                column: "Common_ColorFK",
                principalTable: "Common_Color",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Store_StockIn_Common_Color_Common_ColorFK",
                table: "Store_StockIn",
                column: "Common_ColorFK",
                principalTable: "Common_Color",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Store_StockOut_Common_Color_Common_ColorFK",
                table: "Store_StockOut",
                column: "Common_ColorFK",
                principalTable: "Common_Color",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Store_StockOutSlave_Common_Color_Common_ColorFK",
                table: "Store_StockOutSlave",
                column: "Common_ColorFK",
                principalTable: "Common_Color",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Store_RequisitionSlave_Common_Color_Common_ColorFK",
                table: "Store_RequisitionSlave");

            migrationBuilder.DropForeignKey(
                name: "FK_Store_StockIn_Common_Color_Common_ColorFK",
                table: "Store_StockIn");

            migrationBuilder.DropForeignKey(
                name: "FK_Store_StockOut_Common_Color_Common_ColorFK",
                table: "Store_StockOut");

            migrationBuilder.DropForeignKey(
                name: "FK_Store_StockOutSlave_Common_Color_Common_ColorFK",
                table: "Store_StockOutSlave");

            migrationBuilder.DropTable(
                name: "Shipment_InvoicedBuyerNotifyParty");

            migrationBuilder.DropColumn(
                name: "OriginType",
                table: "Procurement_PurchaseInvoice");

            migrationBuilder.AlterColumn<int>(
                name: "Common_ColorFK",
                table: "Store_StockOutSlave",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "Common_ColorFK",
                table: "Store_StockOut",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "Common_ColorFK",
                table: "Store_StockIn",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "Common_ColorFK",
                table: "Store_RequisitionSlave",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "InvoicedQuantity",
                table: "Shipment_ShipmentInvoiceSlave",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<int>(
                name: "DeliveryChallanQuantity",
                table: "Shipment_DeliveryChallanSlave",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.CreateTable(
                name: "Store_Lot",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    CID = table.Column<string>(nullable: true),
                    Common_RawItemFK = table.Column<int>(nullable: false),
                    Merchandising_BOFFK = table.Column<int>(nullable: true),
                    Merchandising_StyleFK = table.Column<int>(nullable: true),
                    Merchandising_StyleSlaveFK = table.Column<int>(nullable: true),
                    Price = table.Column<decimal>(nullable: false),
                    Quantity = table.Column<decimal>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    User_DepartmentFK = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Store_Lot", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Store_Lot_Common_RawItem_Common_RawItemFK",
                        column: x => x.Common_RawItemFK,
                        principalTable: "Common_RawItem",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Store_Lot_User_Department_User_DepartmentFK",
                        column: x => x.User_DepartmentFK,
                        principalTable: "User_Department",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Store_Receiving",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    CID = table.Column<string>(nullable: true),
                    ReceivingDate = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    User_DepartmentFK = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Store_Receiving", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Store_Receiving_User_Department_User_DepartmentFK",
                        column: x => x.User_DepartmentFK,
                        principalTable: "User_Department",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "StoreDispatch",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    CID = table.Column<string>(nullable: true),
                    DispatchDate = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    User_DepartmentFromFK = table.Column<int>(nullable: true),
                    User_DepartmentToFK = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StoreDispatch", x => x.ID);
                    table.ForeignKey(
                        name: "FK_StoreDispatch_User_Department_User_DepartmentFromFK",
                        column: x => x.User_DepartmentFromFK,
                        principalTable: "User_Department",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_StoreDispatch_User_Department_User_DepartmentToFK",
                        column: x => x.User_DepartmentToFK,
                        principalTable: "User_Department",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Store_ReceivingSlave",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    Common_RawItemFK = table.Column<int>(nullable: false),
                    Merchandising_BOFFK = table.Column<int>(nullable: true),
                    Merchandising_StyleFK = table.Column<int>(nullable: true),
                    Merchandising_StyleSlaveFK = table.Column<int>(nullable: true),
                    Procurement_PurchaseOrderSlaveFK = table.Column<int>(nullable: true),
                    ReceivingPrice = table.Column<decimal>(nullable: false),
                    ReceivingQuantity = table.Column<decimal>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    Store_ReceivingFK = table.Column<int>(nullable: false),
                    Time = table.Column<DateTime>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Store_ReceivingSlave", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Store_ReceivingSlave_Common_RawItem_Common_RawItemFK",
                        column: x => x.Common_RawItemFK,
                        principalTable: "Common_RawItem",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Store_ReceivingSlave_Procurement_PurchaseOrderSlave_Procurement_PurchaseOrderSlaveFK",
                        column: x => x.Procurement_PurchaseOrderSlaveFK,
                        principalTable: "Procurement_PurchaseOrderSlave",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Store_ReceivingSlave_Store_Receiving_Store_ReceivingFK",
                        column: x => x.Store_ReceivingFK,
                        principalTable: "Store_Receiving",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "StoreDispatchSlave",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    Common_RawItemFK = table.Column<int>(nullable: false),
                    DispatchPrice = table.Column<decimal>(nullable: false),
                    DispatchQuantity = table.Column<decimal>(nullable: false),
                    IsAccept = table.Column<bool>(nullable: false),
                    Procurement_PurchaseRequisitionSlaveFK = table.Column<int>(nullable: true),
                    Remarks = table.Column<string>(nullable: true),
                    StoreDispatchFK = table.Column<int>(nullable: false),
                    Store_LotFK = table.Column<int>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StoreDispatchSlave", x => x.ID);
                    table.ForeignKey(
                        name: "FK_StoreDispatchSlave_Common_RawItem_Common_RawItemFK",
                        column: x => x.Common_RawItemFK,
                        principalTable: "Common_RawItem",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_StoreDispatchSlave_Procurement_PurchaseRequisitionSlave_Procurement_PurchaseRequisitionSlaveFK",
                        column: x => x.Procurement_PurchaseRequisitionSlaveFK,
                        principalTable: "Procurement_PurchaseRequisitionSlave",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_StoreDispatchSlave_StoreDispatch_StoreDispatchFK",
                        column: x => x.StoreDispatchFK,
                        principalTable: "StoreDispatch",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_StoreDispatchSlave_Store_Lot_Store_LotFK",
                        column: x => x.Store_LotFK,
                        principalTable: "Store_Lot",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Store_Lot_Common_RawItemFK",
                table: "Store_Lot",
                column: "Common_RawItemFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_Lot_User_DepartmentFK",
                table: "Store_Lot",
                column: "User_DepartmentFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_Receiving_User_DepartmentFK",
                table: "Store_Receiving",
                column: "User_DepartmentFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_ReceivingSlave_Common_RawItemFK",
                table: "Store_ReceivingSlave",
                column: "Common_RawItemFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_ReceivingSlave_Procurement_PurchaseOrderSlaveFK",
                table: "Store_ReceivingSlave",
                column: "Procurement_PurchaseOrderSlaveFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_ReceivingSlave_Store_ReceivingFK",
                table: "Store_ReceivingSlave",
                column: "Store_ReceivingFK");

            migrationBuilder.CreateIndex(
                name: "IX_StoreDispatch_User_DepartmentFromFK",
                table: "StoreDispatch",
                column: "User_DepartmentFromFK");

            migrationBuilder.CreateIndex(
                name: "IX_StoreDispatch_User_DepartmentToFK",
                table: "StoreDispatch",
                column: "User_DepartmentToFK");

            migrationBuilder.CreateIndex(
                name: "IX_StoreDispatchSlave_Common_RawItemFK",
                table: "StoreDispatchSlave",
                column: "Common_RawItemFK");

            migrationBuilder.CreateIndex(
                name: "IX_StoreDispatchSlave_Procurement_PurchaseRequisitionSlaveFK",
                table: "StoreDispatchSlave",
                column: "Procurement_PurchaseRequisitionSlaveFK");

            migrationBuilder.CreateIndex(
                name: "IX_StoreDispatchSlave_StoreDispatchFK",
                table: "StoreDispatchSlave",
                column: "StoreDispatchFK");

            migrationBuilder.CreateIndex(
                name: "IX_StoreDispatchSlave_Store_LotFK",
                table: "StoreDispatchSlave",
                column: "Store_LotFK");

            migrationBuilder.AddForeignKey(
                name: "FK_Store_RequisitionSlave_Common_Color_Common_ColorFK",
                table: "Store_RequisitionSlave",
                column: "Common_ColorFK",
                principalTable: "Common_Color",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Store_StockIn_Common_Color_Common_ColorFK",
                table: "Store_StockIn",
                column: "Common_ColorFK",
                principalTable: "Common_Color",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Store_StockOut_Common_Color_Common_ColorFK",
                table: "Store_StockOut",
                column: "Common_ColorFK",
                principalTable: "Common_Color",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Store_StockOutSlave_Common_Color_Common_ColorFK",
                table: "Store_StockOutSlave",
                column: "Common_ColorFK",
                principalTable: "Common_Color",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
