﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Erp.Infrastructure.Migrations
{
    public partial class ProductionDB201906263pm : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CBM",
                table: "Shipment_ShipmentInvoice");

            migrationBuilder.DropColumn(
                name: "GWT",
                table: "Shipment_ShipmentInvoice");

            migrationBuilder.DropColumn(
                name: "NWT",
                table: "Shipment_ShipmentInvoice");

            migrationBuilder.RenameColumn(
                name: "CTN",
                table: "Shipment_ShipmentInvoiceSlave",
                newName: "UserID");

            migrationBuilder.RenameColumn(
                name: "Commercial_ECIFK",
                table: "Shipment_BillOfExchange",
                newName: "UserID");

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "User_User",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "User_SubMenu",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "User_RoleMenuItem",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "User_Role",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "User_MenuItem",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "User_Menu",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "HRMS_UnitFK",
                table: "User_Department",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "User_Department",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "User_AccessLevel",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Store_StockRegister",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Store_StockOutPersonal",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Store_StockOut",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Store_StockIn",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Store_Section",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Store_RequisitionSlave",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Store_Requisition",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Store_General",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Store_Bin",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Shipment_TermsOfShipment",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Shipment_ShipmentInvoice",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Shipment_ShipmentInstructionSlave",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Shipment_ShipmentInstruction",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Shipment_DeliveryChallanSlave",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Shipment_DeliveryChallan",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Shipment_BillOfExchangeSlave",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Prod_Remarks",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Prod_ReferencePlanFollowupDetails",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Prod_ReferencePlanFollowup",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Prod_ReferencePlan",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Prod_Reference",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Prod_PlanAchievment",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Prod_DailyAchivement",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Procurement_PurchaseRequisitionSlave",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Procurement_PurchaseRequisition",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Procurement_PurchaseOrderSlave",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Procurement_PurchaseOrder",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Plan_SMVMasterLayout",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Plan_SMVDraftLayout",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Plan_PlanConfig",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Plan_MasterOrderPlan",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Plan_MasterLinePlan",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Plan_DraftOrderPlan",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Plan_DraftLinePlan",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Payroll_PayrollMaster",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Payroll_PayrollDetails",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Payroll_GeneratedSections",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Payroll_EODReference",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Payroll_EODRecordMaster",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Payroll_EODRecord",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Mis_ShipmentStatus",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Mis_RawMaterials",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Mis_OrderConfirm",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Mis_IncomeStatement",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Mis_FinancialStatus",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Mis_DailyProduction",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Mis_DailyAttendance",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Mis_CMEarned",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Mis_CashInHand",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Mis_CashAtBank",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Mis_BTBLCStatus",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Mis_BalanceSheet",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Mis_AccountsReceivable",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Mis_AccountsPayable",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Merchandising_YarnType",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<int>(
                name: "QntyPcs",
                table: "Merchandising_YarnCalculation",
                nullable: false,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<int>(
                name: "GSM",
                table: "Merchandising_YarnCalculation",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Merchandising_YarnCalculation",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Merchandising_StyleSlave",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Merchandising_StyleShipmentSchedule",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Merchandising_StyleShipmentRatio",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Merchandising_StyleSetPack",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Merchandising_StyleMeasurement",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Merchandising_Style",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Merchandising_CBSStyle",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Merchandising_CBSSlave",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Merchandising_CBS",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Merchandising_BuyerOrder",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Merchandising_BOF",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "HRMS_BusinessUnitFK",
                table: "HRMS_Unit",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "HRMS_Unit",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "HRMS_ShiftAssign",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "HRMS_Shift",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "HRMS_Section",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "User_DepartmentFK",
                table: "HRMS_Section",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "HRMS_OffDay",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "HRMS_LeaveApplication",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "HRMS_Leave",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "HRMS_Holiday",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "HRMS_EmployeeWeekendAssign",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "HRMS_EmployeeTraining",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "HRMS_EmployeeSkill",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "HRMS_EmployeeServiceBenifit",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "HRMS_EmployeeReference",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "HRMS_EmployeeLeaveAssign",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "HRMS_EmployeeExperience",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "HRMS_EmployeeEducation",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "HRMS_EmployeeEarnLeaveCalculation",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "HRMS_EmployeeDependent",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "HRMS_Employee",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "HRMS_EarnLeave",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "HRMS_Designation",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "HRMS_CompanyHierarchyLabel",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "HRMS_BusinessUnit",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "HRMS_AttendanceRemarks",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "HRMS_AttendanceHistoryLog",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "HRMS_AttendanceHistory",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Common_Unit",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Common_UD",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Common_TCTitle",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Common_Supplier",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Common_Style",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Common_Size",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Common_RawSubCategory",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Common_RawItem",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Common_RawCategory",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Common_ProductionLine",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Common_Order",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Common_Model",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Common_LineSuperVisor",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Common_LineChiefLine",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Common_LineChief",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Common_LienBank",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Common_LcType",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Common_FinishSubCategory",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Common_FinishItem",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Common_FinishCategory",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Common_Currency",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Common_CountrySize",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Common_CountryPort",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Common_Country",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Common_CompanyBank",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Common_Color",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Common_BuyerNotifyParty",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Common_BuyerBank",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Common_Buyer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Common_Brand",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Commercial_UDBuyerOrder",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Commercial_PurchaseOrder",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Commercial_ECI",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Commercial_BuyerOrder",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Commercial_Buyer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Commercial_BBLC",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Accounting_JournalSlave",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Accounting_Journal",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Accounting_Head",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Accounting_CostCenter",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Accounting_Chart2",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UserID",
                table: "Accounting_Chart1",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "Accounting_Transaction",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Date = table.Column<DateTime>(nullable: false),
                    TransactionType = table.Column<string>(maxLength: 20, nullable: true),
                    CID = table.Column<string>(maxLength: 20, nullable: true),
                    To_Acc_NameFK = table.Column<int>(nullable: false),
                    From_Acc_NameFK = table.Column<int>(nullable: false),
                    Accounting_CostCenterFK = table.Column<int>(nullable: false),
                    ChequeNo = table.Column<string>(maxLength: 100, nullable: true),
                    ChequeDate = table.Column<DateTime>(nullable: true),
                    Amount = table.Column<decimal>(nullable: false),
                    Description = table.Column<string>(maxLength: 500, nullable: true),
                    Editable = table.Column<bool>(nullable: true),
                    Finalized = table.Column<bool>(nullable: false),
                    Approved = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Accounting_Transaction", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Accounting_Transaction_Accounting_CostCenter_Accounting_CostCenterFK",
                        column: x => x.Accounting_CostCenterFK,
                        principalTable: "Accounting_CostCenter",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "HRMS_District",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HRMS_District", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "HRMS_PoliceStation",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    HRMS_DistrictFK = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HRMS_PoliceStation", x => x.ID);
                    table.ForeignKey(
                        name: "FK_HRMS_PoliceStation_HRMS_District_HRMS_DistrictFK",
                        column: x => x.HRMS_DistrictFK,
                        principalTable: "HRMS_District",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "HRMS_PostOffice",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    HRMS_PoliceStationFK = table.Column<int>(nullable: false),
                    PostCode = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HRMS_PostOffice", x => x.ID);
                    table.ForeignKey(
                        name: "FK_HRMS_PostOffice_HRMS_PoliceStation_HRMS_PoliceStationFK",
                        column: x => x.HRMS_PoliceStationFK,
                        principalTable: "HRMS_PoliceStation",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_User_Department_HRMS_UnitFK",
                table: "User_Department",
                column: "HRMS_UnitFK");

            migrationBuilder.CreateIndex(
                name: "IX_HRMS_Unit_HRMS_BusinessUnitFK",
                table: "HRMS_Unit",
                column: "HRMS_BusinessUnitFK");

            migrationBuilder.CreateIndex(
                name: "IX_HRMS_Section_User_DepartmentFK",
                table: "HRMS_Section",
                column: "User_DepartmentFK");

            migrationBuilder.CreateIndex(
                name: "IX_Accounting_Transaction_Accounting_CostCenterFK",
                table: "Accounting_Transaction",
                column: "Accounting_CostCenterFK");

            migrationBuilder.CreateIndex(
                name: "IX_HRMS_PoliceStation_HRMS_DistrictFK",
                table: "HRMS_PoliceStation",
                column: "HRMS_DistrictFK");

            migrationBuilder.CreateIndex(
                name: "IX_HRMS_PostOffice_HRMS_PoliceStationFK",
                table: "HRMS_PostOffice",
                column: "HRMS_PoliceStationFK");

            migrationBuilder.AddForeignKey(
                name: "FK_HRMS_Section_User_Department_User_DepartmentFK",
                table: "HRMS_Section",
                column: "User_DepartmentFK",
                principalTable: "User_Department",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_HRMS_Unit_HRMS_BusinessUnit_HRMS_BusinessUnitFK",
                table: "HRMS_Unit",
                column: "HRMS_BusinessUnitFK",
                principalTable: "HRMS_BusinessUnit",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_User_Department_HRMS_Unit_HRMS_UnitFK",
                table: "User_Department",
                column: "HRMS_UnitFK",
                principalTable: "HRMS_Unit",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_HRMS_Section_User_Department_User_DepartmentFK",
                table: "HRMS_Section");

            migrationBuilder.DropForeignKey(
                name: "FK_HRMS_Unit_HRMS_BusinessUnit_HRMS_BusinessUnitFK",
                table: "HRMS_Unit");

            migrationBuilder.DropForeignKey(
                name: "FK_User_Department_HRMS_Unit_HRMS_UnitFK",
                table: "User_Department");

            migrationBuilder.DropTable(
                name: "Accounting_Transaction");

            migrationBuilder.DropTable(
                name: "HRMS_PostOffice");

            migrationBuilder.DropTable(
                name: "HRMS_PoliceStation");

            migrationBuilder.DropTable(
                name: "HRMS_District");

            migrationBuilder.DropIndex(
                name: "IX_User_Department_HRMS_UnitFK",
                table: "User_Department");

            migrationBuilder.DropIndex(
                name: "IX_HRMS_Unit_HRMS_BusinessUnitFK",
                table: "HRMS_Unit");

            migrationBuilder.DropIndex(
                name: "IX_HRMS_Section_User_DepartmentFK",
                table: "HRMS_Section");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "User_User");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "User_SubMenu");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "User_RoleMenuItem");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "User_Role");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "User_MenuItem");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "User_Menu");

            migrationBuilder.DropColumn(
                name: "HRMS_UnitFK",
                table: "User_Department");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "User_Department");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "User_AccessLevel");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Store_StockRegister");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Store_StockOutPersonal");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Store_StockOut");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Store_StockIn");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Store_Section");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Store_RequisitionSlave");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Store_Requisition");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Store_General");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Store_Bin");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Shipment_TermsOfShipment");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Shipment_ShipmentInvoice");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Shipment_ShipmentInstructionSlave");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Shipment_ShipmentInstruction");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Shipment_DeliveryChallanSlave");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Shipment_DeliveryChallan");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Shipment_BillOfExchangeSlave");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Prod_Remarks");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Prod_ReferencePlanFollowupDetails");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Prod_ReferencePlanFollowup");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Prod_ReferencePlan");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Prod_Reference");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Prod_PlanAchievment");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Prod_DailyAchivement");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Procurement_PurchaseRequisitionSlave");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Procurement_PurchaseRequisition");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Procurement_PurchaseOrderSlave");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Procurement_PurchaseOrder");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Plan_SMVMasterLayout");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Plan_SMVDraftLayout");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Plan_PlanConfig");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Plan_MasterOrderPlan");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Plan_MasterLinePlan");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Plan_DraftOrderPlan");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Plan_DraftLinePlan");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Payroll_PayrollMaster");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Payroll_PayrollDetails");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Payroll_GeneratedSections");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Payroll_EODReference");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Payroll_EODRecordMaster");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Payroll_EODRecord");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Mis_ShipmentStatus");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Mis_RawMaterials");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Mis_OrderConfirm");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Mis_IncomeStatement");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Mis_FinancialStatus");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Mis_DailyProduction");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Mis_DailyAttendance");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Mis_CMEarned");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Mis_CashInHand");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Mis_CashAtBank");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Mis_BTBLCStatus");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Mis_BalanceSheet");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Mis_AccountsReceivable");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Mis_AccountsPayable");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Merchandising_YarnType");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Merchandising_YarnCalculation");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Merchandising_StyleSlave");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Merchandising_StyleShipmentSchedule");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Merchandising_StyleShipmentRatio");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Merchandising_StyleSetPack");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Merchandising_StyleMeasurement");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Merchandising_Style");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Merchandising_CBSStyle");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Merchandising_CBSSlave");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Merchandising_CBS");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Merchandising_BuyerOrder");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Merchandising_BOF");

            migrationBuilder.DropColumn(
                name: "HRMS_BusinessUnitFK",
                table: "HRMS_Unit");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "HRMS_Unit");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "HRMS_ShiftAssign");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "HRMS_Shift");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "HRMS_Section");

            migrationBuilder.DropColumn(
                name: "User_DepartmentFK",
                table: "HRMS_Section");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "HRMS_OffDay");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "HRMS_LeaveApplication");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "HRMS_Leave");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "HRMS_Holiday");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "HRMS_EmployeeWeekendAssign");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "HRMS_EmployeeTraining");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "HRMS_EmployeeSkill");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "HRMS_EmployeeServiceBenifit");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "HRMS_EmployeeReference");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "HRMS_EmployeeLeaveAssign");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "HRMS_EmployeeExperience");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "HRMS_EmployeeEducation");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "HRMS_EmployeeEarnLeaveCalculation");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "HRMS_EmployeeDependent");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "HRMS_Employee");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "HRMS_EarnLeave");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "HRMS_Designation");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "HRMS_CompanyHierarchyLabel");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "HRMS_BusinessUnit");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "HRMS_AttendanceRemarks");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "HRMS_AttendanceHistoryLog");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "HRMS_AttendanceHistory");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Common_Unit");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Common_UD");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Common_TCTitle");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Common_Supplier");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Common_Style");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Common_Size");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Common_RawSubCategory");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Common_RawItem");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Common_RawCategory");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Common_ProductionLine");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Common_Order");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Common_Model");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Common_LineSuperVisor");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Common_LineChiefLine");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Common_LineChief");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Common_LienBank");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Common_LcType");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Common_FinishSubCategory");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Common_FinishItem");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Common_FinishCategory");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Common_Currency");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Common_CountrySize");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Common_CountryPort");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Common_Country");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Common_CompanyBank");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Common_Color");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Common_BuyerNotifyParty");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Common_BuyerBank");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Common_Buyer");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Common_Brand");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Commercial_UDBuyerOrder");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Commercial_PurchaseOrder");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Commercial_ECI");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Commercial_BuyerOrder");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Commercial_Buyer");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Commercial_BBLC");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Accounting_JournalSlave");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Accounting_Journal");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Accounting_Head");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Accounting_CostCenter");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Accounting_Chart2");

            migrationBuilder.DropColumn(
                name: "UserID",
                table: "Accounting_Chart1");

            migrationBuilder.RenameColumn(
                name: "UserID",
                table: "Shipment_ShipmentInvoiceSlave",
                newName: "CTN");

            migrationBuilder.RenameColumn(
                name: "UserID",
                table: "Shipment_BillOfExchange",
                newName: "Commercial_ECIFK");

            migrationBuilder.AddColumn<string>(
                name: "CBM",
                table: "Shipment_ShipmentInvoice",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "GWT",
                table: "Shipment_ShipmentInvoice",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "NWT",
                table: "Shipment_ShipmentInvoice",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AlterColumn<decimal>(
                name: "QntyPcs",
                table: "Merchandising_YarnCalculation",
                nullable: false,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "GSM",
                table: "Merchandising_YarnCalculation",
                nullable: true,
                oldClrType: typeof(int));
        }
    }
}
