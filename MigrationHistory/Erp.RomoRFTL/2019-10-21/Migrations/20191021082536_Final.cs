﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Erp.Infrastructure.Migrations
{
    public partial class Final : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Accounting_CostCenter",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Title = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Remark = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Accounting_CostCenter", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Accounting_Type",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    DrIncrease = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Accounting_Type", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Commercial_BBLC",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    CID = table.Column<string>(nullable: true),
                    BBNo = table.Column<string>(nullable: true),
                    LCDate = table.Column<DateTime>(nullable: false),
                    BBValue = table.Column<decimal>(nullable: false),
                    Commercial_UDFk = table.Column<int>(nullable: false),
                    Common_SupplierFK = table.Column<int>(nullable: false),
                    Common_LcTypeFK = table.Column<int>(nullable: false),
                    ScanedFile = table.Column<string>(nullable: true),
                    Common_CurrencyFK = table.Column<int>(nullable: false),
                    Tolerance = table.Column<decimal>(nullable: false),
                    Commercial_LCOreginFK = table.Column<int>(nullable: false),
                    Status = table.Column<string>(nullable: true),
                    IsApproved = table.Column<bool>(nullable: false),
                    IsClose = table.Column<bool>(nullable: false),
                    BaseHeadId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Commercial_BBLC", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Commercial_BBLCPaymentInformation",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Commercial_BBLCFk = table.Column<int>(nullable: false),
                    B2bLCPayment = table.Column<decimal>(nullable: false),
                    Date = table.Column<DateTime>(nullable: false),
                    IsApproved = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Commercial_BBLCPaymentInformation", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Commercial_BBLCValueAmendment",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    ValueAmendment = table.Column<decimal>(nullable: false),
                    Commercial_BBLCFk = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Commercial_BBLCValueAmendment", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Commercial_ECI",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    CID = table.Column<string>(nullable: true),
                    ECINo = table.Column<string>(nullable: true),
                    ECIDate = table.Column<DateTime>(nullable: false),
                    Common_ECITypeFK = table.Column<int>(nullable: false),
                    Common_BuyerFK = table.Column<int>(nullable: false),
                    Common_BuyerBankFK = table.Column<int>(nullable: false),
                    Common_LienBankFK = table.Column<int>(nullable: false),
                    Common_CompanyBankFK = table.Column<int>(nullable: false),
                    Common_CurrencyFK = table.Column<int>(nullable: false),
                    TotalValue = table.Column<decimal>(nullable: false),
                    Tolerance = table.Column<decimal>(nullable: false),
                    ShipmentDate = table.Column<DateTime>(nullable: false),
                    ExpiryDate = table.Column<DateTime>(nullable: false),
                    Destination = table.Column<string>(nullable: true),
                    ScanedFile = table.Column<string>(nullable: true),
                    Commercial_UDFk = table.Column<int>(nullable: false),
                    BaseHeadId = table.Column<int>(nullable: false),
                    Common_BuyerNotifyPartyFk = table.Column<int>(nullable: false),
                    IsClose = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Commercial_ECI", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Commercial_ECIValueAmendment",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    ValueAmendment = table.Column<decimal>(nullable: false),
                    Commercial_ECIFk = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Commercial_ECIValueAmendment", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Commercial_UD",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    CID = table.Column<string>(nullable: true),
                    Ref = table.Column<string>(nullable: true),
                    Date = table.Column<DateTime>(nullable: false),
                    LimitDeclaration = table.Column<decimal>(nullable: false),
                    Common_BuyerFk = table.Column<int>(nullable: false),
                    Accounting_HeadFk = table.Column<int>(nullable: false),
                    IsClose = table.Column<bool>(nullable: false),
                    ScanedFile = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Commercial_UD", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Common_Agent",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    ContactPerson = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_Agent", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Common_Bank",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    SwiftCode = table.Column<string>(nullable: true),
                    Flag = table.Column<bool>(nullable: false),
                    Email = table.Column<string>(nullable: true),
                    Common_CountryFK = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_Bank", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Common_Brand",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_Brand", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Common_Buyer",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    BuyerID = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    ContactPerson = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    Common_CountryFK = table.Column<int>(nullable: false),
                    AccHeadID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_Buyer", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Common_BuyerBank",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    ContactNo = table.Column<string>(nullable: true),
                    Common_CountryFK = table.Column<int>(nullable: true),
                    Common_BankFK = table.Column<int>(nullable: true),
                    ContactPerson = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    SwiftCode = table.Column<string>(nullable: true),
                    Common_BuyerFK = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_BuyerBank", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Common_BuyerNotifyParty",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    ContactPerson = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    Common_CountryFK = table.Column<int>(nullable: false),
                    Common_BuyerFK = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_BuyerNotifyParty", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Common_Color",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_Color", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Common_CompanyBank",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    ContactNo = table.Column<string>(nullable: true),
                    Common_CountryFK = table.Column<int>(nullable: true),
                    SwiftCode = table.Column<string>(nullable: true),
                    ContactPerson = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Common_BankFK = table.Column<int>(nullable: false),
                    Common_CompanySetupFK = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_CompanyBank", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Common_CompanySetup",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    ShortName = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    ContactPerson = table.Column<string>(nullable: true),
                    TIN = table.Column<string>(nullable: true),
                    TradeLicense = table.Column<string>(nullable: true),
                    VatRegNo = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    Image = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_CompanySetup", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Common_Country",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_Country", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Common_Currency",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    ConversionRateToBDT = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_Currency", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Common_FinishCategory",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_FinishCategory", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Common_InspectionAgent",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    ContactPerson = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_InspectionAgent", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Common_LcType",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    IsECIOrBB = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_LcType", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Common_LienBank",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    ContactNo = table.Column<string>(nullable: true),
                    ContactPerson = table.Column<string>(nullable: true),
                    Common_CountryFK = table.Column<int>(nullable: true),
                    SwiftCode = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Common_BankFK = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_LienBank", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Common_LineChief",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_LineChief", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Common_LineDevice",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    DeviceID = table.Column<string>(nullable: true),
                    IsClossed = table.Column<bool>(nullable: false),
                    Common_ProductionLineFK = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_LineDevice", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Common_LineSuperVisor",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_LineSuperVisor", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Common_Model",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Common_BrandFK = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_Model", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Common_Notification",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    StyleID = table.Column<int>(nullable: false),
                    TStyle = table.Column<int>(nullable: false),
                    TCompleteStyle = table.Column<int>(nullable: false),
                    NStyle = table.Column<int>(nullable: false),
                    IncompleteBOM = table.Column<int>(nullable: false),
                    CompleteBOM = table.Column<int>(nullable: false),
                    TPR = table.Column<int>(nullable: false),
                    TCompletePR = table.Column<int>(nullable: false),
                    TPartialPR = table.Column<int>(nullable: false),
                    TCancelPR = table.Column<int>(nullable: false),
                    TCompletePO = table.Column<int>(nullable: false),
                    TPartialPO = table.Column<int>(nullable: false),
                    TCancelPO = table.Column<int>(nullable: false),
                    ShipmentCountDay = table.Column<int>(nullable: false),
                    ShipmentCountMonth = table.Column<int>(nullable: false),
                    TDraftPR = table.Column<int>(nullable: false),
                    TSubmittedPR = table.Column<int>(nullable: false),
                    PRApproved1 = table.Column<int>(nullable: false),
                    PRApproved2 = table.Column<int>(nullable: false),
                    PRApproved3 = table.Column<int>(nullable: false),
                    PRHold = table.Column<int>(nullable: false),
                    PartialPO = table.Column<int>(nullable: false),
                    FullPO = table.Column<int>(nullable: false),
                    TDratfPO = table.Column<int>(nullable: false),
                    TSubmittedPO = table.Column<int>(nullable: false),
                    POApproved1 = table.Column<int>(nullable: false),
                    POApproved2 = table.Column<int>(nullable: false),
                    POApproved3 = table.Column<int>(nullable: false),
                    POHold = table.Column<int>(nullable: false),
                    PartialReceived = table.Column<int>(nullable: false),
                    TGoodReceived = table.Column<int>(nullable: false),
                    TotalPRValue = table.Column<decimal>(nullable: false),
                    TotalPOValue = table.Column<decimal>(nullable: false),
                    TotalGoodReceiveValue = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_Notification", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Common_Notify",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    StyleID = table.Column<int>(nullable: false),
                    IsCloseShipment = table.Column<bool>(nullable: false),
                    NoOfShipment = table.Column<int>(nullable: false),
                    TShipmentDone = table.Column<int>(nullable: false),
                    IsBOMCreate = table.Column<bool>(nullable: false),
                    IsBOMComplete = table.Column<bool>(nullable: false),
                    TDraftPR = table.Column<int>(nullable: false),
                    TSubmittedPR = table.Column<int>(nullable: false),
                    THoldPR = table.Column<int>(nullable: false),
                    TCancelPR = table.Column<int>(nullable: false),
                    TPartialPR = table.Column<int>(nullable: false),
                    TCompletePR = table.Column<int>(nullable: false),
                    PApprovedPR = table.Column<int>(nullable: false),
                    SApprovedPR = table.Column<int>(nullable: false),
                    FApprovedPR = table.Column<int>(nullable: false),
                    TDraftPO = table.Column<int>(nullable: false),
                    TSubmittedPO = table.Column<int>(nullable: false),
                    THoldPO = table.Column<int>(nullable: false),
                    TCancelPO = table.Column<int>(nullable: false),
                    TPartialPO = table.Column<int>(nullable: false),
                    TCompletePO = table.Column<int>(nullable: false),
                    PApprovedPO = table.Column<int>(nullable: false),
                    SApprovedPO = table.Column<int>(nullable: false),
                    FApprovedPO = table.Column<int>(nullable: false),
                    PartialReceived = table.Column<int>(nullable: false),
                    TGoodReceived = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_Notify", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Common_Product",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    barcode = table.Column<string>(nullable: true),
                    description = table.Column<string>(nullable: true),
                    size = table.Column<string>(nullable: true),
                    vatcode = table.Column<string>(nullable: true),
                    quantity = table.Column<int>(nullable: false),
                    cost = table.Column<double>(nullable: false),
                    price = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_Product", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "Common_ProductionUnit",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: true),
                    IsColsed = table.Column<bool>(nullable: false),
                    HRMS_UnitFK = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_ProductionUnit", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Common_RawCategory",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    AutoView = table.Column<int>(nullable: true),
                    CategoryTypeFK = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_RawCategory", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Common_RejectType",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    RejectOne = table.Column<string>(nullable: true),
                    RejectTwo = table.Column<string>(nullable: true),
                    RejectThree = table.Column<string>(nullable: true),
                    RejectFour = table.Column<string>(nullable: true),
                    RejectFive = table.Column<string>(nullable: true),
                    RejectSix = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_RejectType", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Common_Size",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_Size", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Common_TCTitle",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_TCTitle", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Common_Unit",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_Unit", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "HRMS_AttendanceRemarks",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    AttendanceRemark = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HRMS_AttendanceRemarks", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "HRMS_BusinessUnit",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HRMS_BusinessUnit", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "HRMS_CheckInOuts",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<string>(maxLength: 50, nullable: true),
                    CheckTime = table.Column<DateTime>(nullable: false),
                    Checktype = table.Column<string>(maxLength: 50, nullable: true),
                    VerifyCode = table.Column<string>(maxLength: 50, nullable: true),
                    CardNo = table.Column<string>(maxLength: 50, nullable: true),
                    SourceId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HRMS_CheckInOuts", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "HRMS_CompanyHierarchyLabel",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HRMS_CompanyHierarchyLabel", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "HRMS_EducationalDegree",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HRMS_EducationalDegree", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "HRMS_Holiday",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    HoildayName = table.Column<string>(nullable: true),
                    FromDate = table.Column<DateTime>(nullable: false),
                    ToDate = table.Column<DateTime>(nullable: false),
                    TotalDays = table.Column<int>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    IsCompanyWeekendDay = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HRMS_Holiday", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "HRMS_Leave",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    LeaveType = table.Column<int>(nullable: false),
                    EmployeeType = table.Column<int>(nullable: false),
                    LeaveDays = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HRMS_Leave", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "HRMS_OffDay",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Title = table.Column<string>(nullable: true),
                    ApplicableDate = table.Column<DateTime>(nullable: true),
                    ReplacementDate = table.Column<DateTime>(nullable: true),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HRMS_OffDay", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "HRMS_Shift",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    ShiftName = table.Column<string>(nullable: true),
                    StartTime = table.Column<TimeSpan>(nullable: true),
                    EndTime = table.Column<TimeSpan>(nullable: true),
                    BreakTime = table.Column<int>(nullable: false),
                    ShiftRemarks = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HRMS_Shift", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Inventorydb",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    StoreTypeFk = table.Column<int>(nullable: false),
                    TransactionType = table.Column<int>(nullable: false),
                    Procurement_PurchaseInvoiceFK = table.Column<int>(nullable: true),
                    Procurement_PurchaseOrderSlaveID = table.Column<int>(nullable: true),
                    Merchandising_StyleID = table.Column<int>(nullable: true),
                    Merchandising_StyleSlaveID = table.Column<int>(nullable: true),
                    Merchandising_BOFID = table.Column<int>(nullable: true),
                    Store_StockInFK = table.Column<int>(nullable: true),
                    Common_RawItemFK = table.Column<int>(nullable: false),
                    Amount = table.Column<decimal>(nullable: false),
                    Price = table.Column<decimal>(nullable: false),
                    Store_StockOutFK = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Inventorydb", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Merchandising_Packing",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Merchandising_StyleShipmentScheduleFK = table.Column<int>(nullable: false),
                    Merchandising_StyleShipmentRatioFK = table.Column<int>(nullable: false),
                    Merchandising_StyleFK = table.Column<int>(nullable: false),
                    CartonRetio = table.Column<decimal>(nullable: false),
                    GrossWeight = table.Column<decimal>(nullable: false),
                    NetWeight = table.Column<decimal>(nullable: false),
                    CartonMeasurement = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Merchandising_Packing", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Merchandising_YarnCalculation",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    ReferenceNo = table.Column<string>(nullable: true),
                    Merchandising_StyleFk = table.Column<int>(nullable: false),
                    Merchandising_YarnTypeFk = table.Column<int>(nullable: false),
                    Combo = table.Column<string>(maxLength: 40, nullable: true),
                    Fabrication = table.Column<string>(maxLength: 100, nullable: true),
                    Merchandising_StyleSlaveFK_Fabric = table.Column<int>(nullable: false),
                    GSM = table.Column<int>(nullable: false),
                    Raw_ItemFK = table.Column<int>(nullable: false),
                    FinishDIA = table.Column<string>(nullable: true),
                    OrderQuantityPCS = table.Column<decimal>(nullable: false),
                    Consumption = table.Column<decimal>(type: "decimal(18,5)", nullable: false),
                    ProcessLoss = table.Column<decimal>(type: "decimal(18,5)", nullable: false),
                    Lycra = table.Column<decimal>(type: "decimal(18,5)", nullable: false),
                    Common_CurrencyFK = table.Column<int>(nullable: false),
                    Common_UnitFK = table.Column<int>(nullable: false),
                    Price = table.Column<decimal>(type: "decimal(18,5)", nullable: false),
                    IsApprove = table.Column<bool>(nullable: false),
                    Common_ColorFK = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Merchandising_YarnCalculation", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Merchandising_YarnType",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    FirstCreatedBy = table.Column<int>(nullable: false),
                    LastEditeddBy = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 50, nullable: true),
                    Description = table.Column<string>(maxLength: 200, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Merchandising_YarnType", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Mis_AccountsPayable",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Common_SupplierFk = table.Column<int>(nullable: false),
                    Date = table.Column<DateTime>(nullable: false),
                    Value = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Mis_AccountsPayable", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Mis_AccountsReceivable",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Common_BuyerFk = table.Column<int>(nullable: false),
                    Date = table.Column<DateTime>(nullable: false),
                    Value = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Mis_AccountsReceivable", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Mis_BalanceSheet",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    ChartTableId = table.Column<int>(nullable: false),
                    BaseHeadId = table.Column<int>(nullable: false),
                    FromDate = table.Column<DateTime>(nullable: false),
                    ToDate = table.Column<DateTime>(nullable: false),
                    Accounting_Head = table.Column<string>(nullable: true),
                    Value = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Mis_BalanceSheet", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Mis_BTBLCStatus",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    UDNo = table.Column<string>(nullable: true),
                    MasterLCTotalValue = table.Column<decimal>(nullable: false),
                    UdID = table.Column<int>(nullable: false),
                    BTBLimit = table.Column<decimal>(nullable: false),
                    BTBIssued = table.Column<decimal>(nullable: false),
                    BTBLiabilites = table.Column<decimal>(nullable: false),
                    BTBAcceptedValue = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Mis_BTBLCStatus", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Mis_CashAtBank",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Date = table.Column<DateTime>(nullable: false),
                    Accounting_Head = table.Column<string>(nullable: true),
                    Value = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Mis_CashAtBank", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Mis_CashInHand",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Date = table.Column<DateTime>(nullable: false),
                    Accounting_Head = table.Column<string>(nullable: true),
                    Value = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Mis_CashInHand", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Mis_CMEarned",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Date = table.Column<DateTime>(nullable: true),
                    Month = table.Column<string>(nullable: true),
                    Year = table.Column<string>(nullable: true),
                    Merchandising_StyleFK = table.Column<int>(nullable: false),
                    StyleName = table.Column<string>(nullable: true),
                    Quantity = table.Column<int>(nullable: false),
                    Value = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Mis_CMEarned", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Mis_DailyAttendance",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    User_DepartmentFk = table.Column<int>(nullable: false),
                    AttendanceDate = table.Column<DateTime>(nullable: false),
                    WorkingHour = table.Column<int>(nullable: false),
                    TotalEmployee = table.Column<int>(nullable: false),
                    Present = table.Column<int>(nullable: false),
                    Absent = table.Column<int>(nullable: false),
                    OnLeave = table.Column<int>(nullable: false),
                    Off = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Mis_DailyAttendance", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Mis_DailyProduction",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    ProductionDate = table.Column<DateTime>(nullable: false),
                    Common_BuyerFk = table.Column<int>(nullable: false),
                    OrderNo = table.Column<string>(nullable: true),
                    Knitting = table.Column<int>(nullable: false),
                    Dyeing = table.Column<int>(nullable: false),
                    Cutting = table.Column<int>(nullable: false),
                    Sewing = table.Column<int>(nullable: false),
                    Finishing = table.Column<int>(nullable: false),
                    KnittingDone = table.Column<int>(nullable: false),
                    DyeingDone = table.Column<int>(nullable: false),
                    CuttingDone = table.Column<int>(nullable: false),
                    SewingDone = table.Column<int>(nullable: false),
                    FinishingDone = table.Column<int>(nullable: false),
                    OrderQty = table.Column<int>(nullable: false),
                    SMV = table.Column<decimal>(nullable: false),
                    Ironing = table.Column<int>(nullable: false),
                    IroningDone = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Mis_DailyProduction", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Mis_FinancialStatus",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Month = table.Column<string>(nullable: true),
                    Year = table.Column<string>(nullable: true),
                    Inflow = table.Column<decimal>(nullable: false),
                    Outflow = table.Column<decimal>(nullable: false),
                    RevenueProjection = table.Column<decimal>(nullable: false),
                    ExpenditureProjection = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Mis_FinancialStatus", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Mis_IncomeStatement",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    ChartTableId = table.Column<int>(nullable: false),
                    BaseHeadId = table.Column<int>(nullable: false),
                    FromDate = table.Column<DateTime>(nullable: false),
                    ToDate = table.Column<DateTime>(nullable: false),
                    Accounting_Head = table.Column<string>(nullable: true),
                    Value = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Mis_IncomeStatement", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Mis_Log",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    TableName = table.Column<string>(nullable: true),
                    Status = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Mis_Log", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Mis_OrderConfirm",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    OrderNo = table.Column<string>(nullable: true),
                    FinalDeliveryDate = table.Column<DateTime>(nullable: true),
                    Common_BuyerFk = table.Column<int>(nullable: false),
                    OrderValue = table.Column<decimal>(nullable: false),
                    CmValue = table.Column<decimal>(nullable: false),
                    OrderQty = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Mis_OrderConfirm", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Mis_RawMaterials",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Common_RawCategoryFk = table.Column<int>(nullable: false),
                    RawValue = table.Column<int>(nullable: false),
                    WIPValue = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Mis_RawMaterials", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Mis_ShipmentStatus",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Common_BuyerFk = table.Column<int>(nullable: false),
                    OrderNo = table.Column<string>(nullable: true),
                    Date = table.Column<DateTime>(nullable: true),
                    OrderQuantity = table.Column<int>(nullable: false),
                    Exfactory = table.Column<int>(nullable: false),
                    DeliveredQuantity = table.Column<int>(nullable: false),
                    DueQty = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Mis_ShipmentStatus", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Payroll_BonusMaster",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    BonusTitle = table.Column<string>(nullable: true),
                    PaymentDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Payroll_BonusMaster", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Payroll_BonusSettings",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    FromMonth = table.Column<int>(nullable: false),
                    ToMonth = table.Column<int>(nullable: false),
                    BonusRate = table.Column<decimal>(nullable: false),
                    IsBasicOrGross = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Payroll_BonusSettings", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Payroll_EODReference",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    ReferenceNameEn = table.Column<string>(nullable: true),
                    ReferenceNameBn = table.Column<string>(nullable: true),
                    IsMandatory = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Payroll_EODReference", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Payroll_PayrollDetails",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Payroll_PayrollMasterFk = table.Column<int>(nullable: false),
                    EmployeeFk = table.Column<int>(nullable: false),
                    GrossSalary = table.Column<decimal>(nullable: false),
                    BasicSalary = table.Column<decimal>(nullable: false),
                    HouseRent = table.Column<decimal>(nullable: false),
                    OtherAllowance = table.Column<decimal>(nullable: false),
                    TotalPresents = table.Column<int>(nullable: false),
                    HolidayOffdays = table.Column<int>(nullable: false),
                    LeaveDays = table.Column<int>(nullable: false),
                    LateDays = table.Column<int>(nullable: false),
                    AbsentDays = table.Column<int>(nullable: false),
                    TotalDeduction = table.Column<decimal>(nullable: false),
                    DeductedSalary = table.Column<decimal>(nullable: false),
                    AttendanceBonus = table.Column<decimal>(nullable: false),
                    PayableOverTimeHours = table.Column<int>(nullable: false),
                    ExtraOverTimeHours = table.Column<int>(nullable: false),
                    OverTimeRate = table.Column<decimal>(nullable: false),
                    PayableOverTimeAmount = table.Column<decimal>(nullable: false),
                    ExtraOverTimeAmount = table.Column<decimal>(nullable: false),
                    StampCharge = table.Column<decimal>(nullable: false),
                    FinalSalary = table.Column<decimal>(nullable: false),
                    EligibleHoliday = table.Column<int>(nullable: false),
                    HolidayRate = table.Column<decimal>(nullable: false),
                    HolidayBill = table.Column<decimal>(nullable: false),
                    EligibleHalfNight = table.Column<int>(nullable: false),
                    EligibleFullNight = table.Column<int>(nullable: false),
                    NightBill = table.Column<decimal>(nullable: false),
                    ApprovalStatus = table.Column<int>(nullable: false),
                    PaymentStatus = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Payroll_PayrollDetails", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Payroll_SalaryStructure",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    StructerType = table.Column<int>(nullable: false),
                    MedicalAllowance = table.Column<decimal>(nullable: false),
                    FoodlAllowance = table.Column<decimal>(nullable: false),
                    TransportAllowance = table.Column<decimal>(nullable: false),
                    StampCharge = table.Column<decimal>(nullable: false),
                    BasicInPercent = table.Column<decimal>(nullable: false),
                    MonthlyWorkingDays = table.Column<int>(nullable: false),
                    DailyWorkingHour = table.Column<int>(nullable: false),
                    OvertimeRate = table.Column<decimal>(nullable: false),
                    HouseRentInPercent = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Payroll_SalaryStructure", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Plan_OrderAction",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    ActionRange = table.Column<int>(nullable: false),
                    Priority = table.Column<int>(nullable: false),
                    IsStop = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Plan_OrderAction", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Plan_PlanConfig",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    CuttingBFShip = table.Column<int>(nullable: false),
                    FabricINBFCutting = table.Column<int>(nullable: false),
                    AccessoriesINBFCutting = table.Column<int>(nullable: false),
                    PPSampleBFCutting = table.Column<int>(nullable: false),
                    SewingAFCutting = table.Column<int>(nullable: false),
                    ReservedDate = table.Column<int>(nullable: false),
                    ExtraRate = table.Column<decimal>(nullable: false),
                    IsRunning = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Plan_PlanConfig", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Plan_SMVDraftLayout",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Merchandising_StyleSetPackFK = table.Column<int>(nullable: true),
                    Merchandising_StyleFK = table.Column<int>(nullable: false),
                    NoOfOperator = table.Column<int>(nullable: false),
                    NoOfHelper = table.Column<int>(nullable: false),
                    OperatorSMV = table.Column<decimal>(nullable: false),
                    HelperSMV = table.Column<decimal>(nullable: false),
                    TotalWorker = table.Column<int>(nullable: false),
                    TotalSMV = table.Column<decimal>(nullable: false),
                    WorkingHour = table.Column<int>(nullable: false),
                    PerHourTarget = table.Column<int>(nullable: false),
                    Efficiency = table.Column<decimal>(nullable: false),
                    QtyRate = table.Column<decimal>(type: "decimal(18,5)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Plan_SMVDraftLayout", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Prod_DailyAchivement",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Prod_ReferenceFK = table.Column<int>(nullable: false),
                    ProductionLineFK = table.Column<int>(nullable: false),
                    Merchandising_StyleFK = table.Column<int>(nullable: false),
                    Prod_RemarksFK = table.Column<int>(nullable: false),
                    TotalAchievQty = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    TargetEfficiency = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    AchiveEfficiency = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    TargetBalance = table.Column<int>(nullable: false),
                    Prod_ReferencePlanFk = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Prod_DailyAchivement", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Prod_QCOrder",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Prod_QCTypeFK = table.Column<int>(nullable: false),
                    Merchandising_StyleFK = table.Column<int>(nullable: false),
                    Priority = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Prod_QCOrder", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Prod_QCType",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    IsDeprecate = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Prod_QCType", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Prod_Reference",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    ReferenceNo = table.Column<string>(nullable: true),
                    ReferenceDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Prod_Reference", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Prod_Remarks",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    IsClose = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Prod_Remarks", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Shipment_BillOfExchange",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    IsFinal = table.Column<bool>(nullable: false),
                    BoEDate = table.Column<DateTime>(nullable: false),
                    BoECID = table.Column<string>(nullable: false),
                    CourierNO = table.Column<string>(nullable: true),
                    CourierDate = table.Column<DateTime>(nullable: false),
                    Commercial_ECIFK = table.Column<int>(nullable: false),
                    ReceivedType = table.Column<string>(maxLength: 60, nullable: true),
                    FDBCNO = table.Column<string>(nullable: true),
                    FDBCDate = table.Column<DateTime>(nullable: false),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Shipment_BillOfExchange", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Shipment_BillOfExchangeSlave",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Shipment_ShipmentInvoiceFk = table.Column<int>(nullable: false),
                    Shipment_BillOfExchangeFk = table.Column<int>(nullable: false),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Shipment_BillOfExchangeSlave", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Shipment_DeliveryChallan",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Date = table.Column<DateTime>(nullable: false),
                    ChallanNo = table.Column<string>(nullable: true),
                    Common_CNFSupplierFk = table.Column<int>(nullable: false),
                    ContactPersonName = table.Column<string>(nullable: true),
                    Common_TransporterSupplierFk = table.Column<int>(nullable: false),
                    DriverName = table.Column<string>(nullable: true),
                    LockNo = table.Column<string>(nullable: true),
                    DLNo = table.Column<string>(nullable: true),
                    TruckNo = table.Column<string>(nullable: true),
                    DriverMobile = table.Column<string>(nullable: true),
                    ContactPersonMobile = table.Column<string>(nullable: true),
                    InTime = table.Column<string>(nullable: true),
                    OutTime = table.Column<string>(nullable: true),
                    GatePassNo = table.Column<string>(nullable: true),
                    GatePassDate = table.Column<DateTime>(nullable: false),
                    ShipmentUnloadingPortFk = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Shipment_DeliveryChallan", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Shipment_DeliveryChallanSlave",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Shipment_DeliveryChallanFk = table.Column<int>(nullable: false),
                    Shipment_InvoiceSlaveFk = table.Column<int>(nullable: false),
                    Shipment_InstructionSlaveFk = table.Column<int>(nullable: false),
                    Merchandising_StyleShipmentScheduleFk = table.Column<int>(nullable: false),
                    Merchandising_StyleFk = table.Column<int>(nullable: false),
                    DeliveryChallanQuantity = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Shipment_DeliveryChallanSlave", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Shipment_ExportRealisation",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Date = table.Column<DateTime>(nullable: false),
                    BillNo = table.Column<string>(maxLength: 100, nullable: true),
                    FDBC = table.Column<string>(maxLength: 100, nullable: true),
                    BillValue = table.Column<decimal>(nullable: false),
                    RealisedAmount = table.Column<decimal>(nullable: false),
                    DollarValue = table.Column<decimal>(nullable: false),
                    Shipment_BillOfExchangeFK = table.Column<int>(nullable: false),
                    FDBCDate = table.Column<DateTime>(nullable: false),
                    ValueDate = table.Column<DateTime>(nullable: false),
                    IsFinal = table.Column<bool>(nullable: false),
                    FcbParAmt = table.Column<decimal>(nullable: false),
                    Others = table.Column<decimal>(nullable: false),
                    BillPurchase = table.Column<decimal>(nullable: false),
                    ERQAmt = table.Column<decimal>(nullable: false),
                    FxTrading = table.Column<decimal>(nullable: false),
                    PcAdjust = table.Column<decimal>(nullable: false),
                    BuyingCommission = table.Column<decimal>(nullable: false),
                    FdrAmount = table.Column<decimal>(nullable: false),
                    CourierExpense = table.Column<decimal>(nullable: false),
                    RmgAmount = table.Column<decimal>(nullable: false),
                    AitAmount = table.Column<decimal>(nullable: false),
                    SodAcAmount = table.Column<decimal>(nullable: false),
                    CdAmount = table.Column<decimal>(nullable: false),
                    TimeLoanAmount = table.Column<decimal>(nullable: false),
                    DocHandlingCharge = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Shipment_ExportRealisation", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Shipment_InvoicedBuyerNotifyParty",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Shipment_InvoiceFk = table.Column<int>(nullable: false),
                    Common_BuyerNotifyPartyFk = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Shipment_InvoicedBuyerNotifyParty", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Shipment_ShipmentInstruction",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    SIDate = table.Column<DateTime>(nullable: false),
                    SICID = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Shipment_ShipmentInstruction", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Shipment_ShipmentInstructionSlave",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Shipment_ShipmentInstructionFk = table.Column<int>(nullable: true),
                    InstructionQuantity = table.Column<int>(nullable: false),
                    Merchandising_StyleFk = table.Column<int>(nullable: true),
                    Merchandising_StyleShipmentScheduleFk = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Shipment_ShipmentInstructionSlave", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Shipment_ShipmentInvoice",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    InvoiceDate = table.Column<DateTime>(nullable: false),
                    InvoiceCID = table.Column<string>(nullable: true),
                    ErcNo = table.Column<string>(maxLength: 15, nullable: true),
                    InvoiceIdNo = table.Column<string>(maxLength: 50, nullable: true),
                    ShippedBy = table.Column<string>(nullable: true),
                    Shipment_PortOfLoadingFk = table.Column<int>(nullable: false),
                    BLNo = table.Column<string>(maxLength: 50, nullable: true),
                    BLDate = table.Column<DateTime>(nullable: false),
                    ExpNo = table.Column<string>(nullable: true),
                    ExpDate = table.Column<DateTime>(nullable: false),
                    ShippingMarks = table.Column<string>(nullable: true),
                    Shipment_TermsOfShipmentFK = table.Column<int>(nullable: false),
                    EstimatedReceivedDate = table.Column<DateTime>(nullable: false),
                    AdjustedValue = table.Column<decimal>(nullable: false),
                    AdjustedNote = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Shipment_ShipmentInvoice", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Shipment_ShipmentInvoiceSlave",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Shipment_InstructionSlaveFk = table.Column<int>(nullable: false),
                    Shipment_InvoiceFk = table.Column<int>(nullable: false),
                    Merchandising_StyleFk = table.Column<int>(nullable: false),
                    Merchandising_StyleShipmentScheduleFk = table.Column<int>(nullable: false),
                    InvoicedQuantity = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Shipment_ShipmentInvoiceSlave", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Shipment_TermsOfShipment",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Shipment_TermsOfShipment", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Store_AssetDepreciation",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    CID = table.Column<string>(nullable: true),
                    ItemSLNO = table.Column<string>(nullable: true),
                    LotQty = table.Column<double>(nullable: false),
                    UnitPrice = table.Column<double>(nullable: false),
                    MinPrice = table.Column<double>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    HREmployeeDepartmentFK = table.Column<int>(nullable: false),
                    HREmployeeFK = table.Column<int>(nullable: false),
                    HREmployeeReceivedDate = table.Column<DateTime>(nullable: false),
                    CommonStoreFK = table.Column<int>(nullable: false),
                    StockInFK = table.Column<int>(nullable: false),
                    CommonUnitFK = table.Column<int>(nullable: false),
                    CommonBrandFK = table.Column<int>(nullable: false),
                    CommonModelFK = table.Column<int>(nullable: false),
                    CommonDepreciationFK = table.Column<int>(nullable: false),
                    RawItemFK = table.Column<int>(nullable: false),
                    LifeTime = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Store_AssetDepreciation", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Store_ItemWiseConsumption",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    StyleName = table.Column<string>(nullable: true),
                    Merchandising_StyleFK = table.Column<int>(nullable: false),
                    Common_RawItemFK = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    UnitName = table.Column<string>(nullable: true),
                    ConsumptionDz = table.Column<double>(nullable: false),
                    OrderConsumQty = table.Column<double>(nullable: false),
                    FinishQtyDz = table.Column<double>(nullable: false),
                    TotalRecMain = table.Column<double>(nullable: false),
                    TotalRecCutting = table.Column<double>(nullable: false),
                    TotalRecSewing = table.Column<double>(nullable: false),
                    TotalRecIroning = table.Column<double>(nullable: false),
                    TotalRecPacking = table.Column<double>(nullable: false),
                    TotalRecFinish = table.Column<double>(nullable: false),
                    TotalOutMain = table.Column<double>(nullable: false),
                    TotalOutCutting = table.Column<double>(nullable: false),
                    TotalOutSewing = table.Column<double>(nullable: false),
                    TotalOutIroning = table.Column<double>(nullable: false),
                    TotalOutPacking = table.Column<double>(nullable: false),
                    TotalOutFinish = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Store_ItemWiseConsumption", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Store_ItemWiseConsumptionAllStyle",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    StyleName = table.Column<string>(nullable: true),
                    Merchandising_StyleFK = table.Column<int>(nullable: false),
                    Common_RawItemFK = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    UnitName = table.Column<string>(nullable: true),
                    ConsumptionDz = table.Column<double>(nullable: false),
                    OrderConsumQty = table.Column<double>(nullable: false),
                    FinishQtyDz = table.Column<double>(nullable: false),
                    TotalRecMain = table.Column<double>(nullable: false),
                    TotalRecCutting = table.Column<double>(nullable: false),
                    TotalRecSewing = table.Column<double>(nullable: false),
                    TotalRecIroning = table.Column<double>(nullable: false),
                    TotalRecPacking = table.Column<double>(nullable: false),
                    TotalRecFinish = table.Column<double>(nullable: false),
                    TotalOutMain = table.Column<double>(nullable: false),
                    TotalOutCutting = table.Column<double>(nullable: false),
                    TotalOutSewing = table.Column<double>(nullable: false),
                    TotalOutIroning = table.Column<double>(nullable: false),
                    TotalOutPacking = table.Column<double>(nullable: false),
                    TotalOutFinish = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Store_ItemWiseConsumptionAllStyle", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Store_StockOutMaster",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    CID = table.Column<string>(nullable: true),
                    ChallanNo = table.Column<string>(nullable: true),
                    ChallanDate = table.Column<DateTime>(nullable: false),
                    ReceivedDate = table.Column<DateTime>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    StockType = table.Column<string>(nullable: true),
                    StoreOutType = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Store_StockOutMaster", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Store_StockOutPersonal",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    CID = table.Column<string>(nullable: true),
                    ChallanNo = table.Column<string>(nullable: true),
                    ReceivedDate = table.Column<DateTime>(nullable: false),
                    ReceivedQty = table.Column<double>(nullable: false),
                    IssueReturnStatus = table.Column<int>(nullable: false),
                    ItemIDFK = table.Column<int>(nullable: false),
                    CommonUnitFK = table.Column<int>(nullable: false),
                    CommonStoreFK = table.Column<int>(nullable: false),
                    StoreBinFK = table.Column<int>(nullable: false),
                    StoreGeneralFK = table.Column<int>(nullable: false),
                    CommonDepartmentFK = table.Column<int>(nullable: false),
                    EmployeeFK = table.Column<int>(nullable: false),
                    CommonBrandFK = table.Column<int>(nullable: false),
                    CommonModelFK = table.Column<int>(nullable: false),
                    RawItemFK = table.Column<int>(nullable: false),
                    CommonAssetFK = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Store_StockOutPersonal", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Store_StockRegister",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    CID = table.Column<string>(nullable: true),
                    ItemSLNO = table.Column<string>(nullable: true),
                    LotQty = table.Column<double>(nullable: false),
                    UnitPrice = table.Column<double>(nullable: false),
                    MinPrice = table.Column<double>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    HREmployeeDepartmentFK = table.Column<int>(nullable: false),
                    HREmployeeFK = table.Column<int>(nullable: false),
                    HREmployeeReceivedDate = table.Column<DateTime>(nullable: false),
                    CommonStoreFK = table.Column<int>(nullable: false),
                    StockInFK = table.Column<int>(nullable: false),
                    CommonUnitFK = table.Column<int>(nullable: false),
                    CommonBrandFK = table.Column<int>(nullable: false),
                    CommonModelFK = table.Column<int>(nullable: false),
                    CommonDepreciationFK = table.Column<int>(nullable: false),
                    RawItemFK = table.Column<int>(nullable: false),
                    LifeTime = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Store_StockRegister", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "User_AccessLevel",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User_AccessLevel", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "User_Department",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User_Department", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "User_Menu",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Priority = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User_Menu", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "User_Role",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User_Role", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Accounting_Journal",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Date = table.Column<DateTime>(nullable: false),
                    CID = table.Column<string>(nullable: true),
                    Title = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Remark = table.Column<string>(nullable: true),
                    Finalized = table.Column<bool>(nullable: false),
                    Approved = table.Column<bool>(nullable: false),
                    JournalType = table.Column<int>(nullable: false),
                    Accounting_CostCenterFK = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Accounting_Journal", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Accounting_Journal_Accounting_CostCenter_Accounting_CostCenterFK",
                        column: x => x.Accounting_CostCenterFK,
                        principalTable: "Accounting_CostCenter",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Accounting_Transaction",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Date = table.Column<DateTime>(nullable: false),
                    TransactionType = table.Column<string>(maxLength: 20, nullable: true),
                    CID = table.Column<string>(maxLength: 20, nullable: true),
                    To_Acc_NameFK = table.Column<int>(nullable: false),
                    From_Acc_NameFK = table.Column<int>(nullable: false),
                    Accounting_CostCenterFK = table.Column<int>(nullable: false),
                    ChequeNo = table.Column<string>(maxLength: 100, nullable: true),
                    ChequeDate = table.Column<DateTime>(nullable: true),
                    Amount = table.Column<decimal>(nullable: false),
                    Description = table.Column<string>(maxLength: 500, nullable: true),
                    Status = table.Column<int>(nullable: false),
                    Editable = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Accounting_Transaction", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Accounting_Transaction_Accounting_CostCenter_Accounting_CostCenterFK",
                        column: x => x.Accounting_CostCenterFK,
                        principalTable: "Accounting_CostCenter",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Accounting_Chart1",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 150, nullable: true),
                    Code = table.Column<string>(maxLength: 20, nullable: true),
                    Accounting_TypeFK = table.Column<int>(nullable: false),
                    Entryable = table.Column<bool>(nullable: false),
                    Addable = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Accounting_Chart1", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Accounting_Chart1_Accounting_Type_Accounting_TypeFK",
                        column: x => x.Accounting_TypeFK,
                        principalTable: "Accounting_Type",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Merchandising_BuyerOrder",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Common_BuyerFK = table.Column<int>(nullable: false),
                    Common_AgentFK = table.Column<int>(nullable: false),
                    Common_InspectionAgentFK = table.Column<int>(nullable: false),
                    CID = table.Column<string>(nullable: true),
                    BuyerPO = table.Column<string>(nullable: true),
                    OrderDate = table.Column<DateTime>(nullable: false),
                    Season = table.Column<string>(nullable: true),
                    Agent = table.Column<string>(nullable: true),
                    InspectionAgent = table.Column<string>(nullable: true),
                    Commission = table.Column<decimal>(nullable: false),
                    BuyerOrderPOValue = table.Column<decimal>(type: "decimal(18,5)", nullable: false),
                    IsComplete = table.Column<bool>(nullable: false),
                    Commercial_ECIFk = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Merchandising_BuyerOrder", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Merchandising_BuyerOrder_Common_Buyer_Common_BuyerFK",
                        column: x => x.Common_BuyerFK,
                        principalTable: "Common_Buyer",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Common_CountryPort",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Common_CountryFK = table.Column<int>(nullable: true),
                    Code = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_CountryPort", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Common_CountryPort_Common_Country_Common_CountryFK",
                        column: x => x.Common_CountryFK,
                        principalTable: "Common_Country",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Common_Supplier",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: true),
                    SupplierType = table.Column<int>(nullable: false),
                    IsForeignSupplier = table.Column<bool>(nullable: false),
                    ContactPerson = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    Common_CountryFK = table.Column<int>(nullable: false),
                    AccHeadID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_Supplier", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Common_Supplier_Common_Country_Common_CountryFK",
                        column: x => x.Common_CountryFK,
                        principalTable: "Common_Country",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "HRMS_District",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Common_CountryFK = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HRMS_District", x => x.ID);
                    table.ForeignKey(
                        name: "FK_HRMS_District_Common_Country_Common_CountryFK",
                        column: x => x.Common_CountryFK,
                        principalTable: "Common_Country",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Common_CurrencyChangesLog",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Common_CurrencyFK = table.Column<int>(nullable: false),
                    PreviousConversionRateToBDT = table.Column<decimal>(nullable: false),
                    ChangesConversionRateToBDT = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_CurrencyChangesLog", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Common_CurrencyChangesLog_Common_Currency_Common_CurrencyFK",
                        column: x => x.Common_CurrencyFK,
                        principalTable: "Common_Currency",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Common_FinishSubCategory",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Common_FinishCategoryFK = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_FinishSubCategory", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Common_FinishSubCategory_Common_FinishCategory_Common_FinishCategoryFK",
                        column: x => x.Common_FinishCategoryFK,
                        principalTable: "Common_FinishCategory",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Common_ProductionFloor",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: true),
                    IsColsed = table.Column<bool>(nullable: false),
                    Common_ProductionUnitFK = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_ProductionFloor", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Common_ProductionFloor_Common_ProductionUnit_Common_ProductionUnitFK",
                        column: x => x.Common_ProductionUnitFK,
                        principalTable: "Common_ProductionUnit",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Common_RawSubCategory",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Common_RawCategoryFK = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_RawSubCategory", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Common_RawSubCategory_Common_RawCategory_Common_RawCategoryFK",
                        column: x => x.Common_RawCategoryFK,
                        principalTable: "Common_RawCategory",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Common_CountrySize",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Common_SizeFK = table.Column<int>(nullable: true),
                    Common_CountryFK = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_CountrySize", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Common_CountrySize_Common_Country_Common_CountryFK",
                        column: x => x.Common_CountryFK,
                        principalTable: "Common_Country",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Common_CountrySize_Common_Size_Common_SizeFK",
                        column: x => x.Common_SizeFK,
                        principalTable: "Common_Size",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "HRMS_Unit",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: true),
                    HRMS_BusinessUnitFK = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HRMS_Unit", x => x.ID);
                    table.ForeignKey(
                        name: "FK_HRMS_Unit_HRMS_BusinessUnit_HRMS_BusinessUnitFK",
                        column: x => x.HRMS_BusinessUnitFK,
                        principalTable: "HRMS_BusinessUnit",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Payroll_PayrollMaster",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    PayrollTitle = table.Column<string>(nullable: true),
                    FromDate = table.Column<DateTime>(nullable: false),
                    ToDate = table.Column<DateTime>(nullable: false),
                    PaymentDate = table.Column<DateTime>(nullable: false),
                    HRMS_BusinessUnitFK = table.Column<int>(nullable: false),
                    Status = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Payroll_PayrollMaster", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Payroll_PayrollMaster_HRMS_BusinessUnit_HRMS_BusinessUnitFK",
                        column: x => x.HRMS_BusinessUnitFK,
                        principalTable: "HRMS_BusinessUnit",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Payroll_EODRecordMaster",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    EmployeeFk = table.Column<int>(nullable: false),
                    GrossSalary = table.Column<decimal>(nullable: false),
                    SalaryAllowance = table.Column<int>(nullable: false),
                    BillEligibily = table.Column<int>(nullable: false),
                    NextIncreamentDate = table.Column<DateTime>(nullable: false),
                    AffectedDate = table.Column<DateTime>(nullable: false),
                    IsIncreamentedSalary = table.Column<bool>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: true),
                    Status = table.Column<int>(nullable: false),
                    PreviousSalaryRecord = table.Column<int>(nullable: false),
                    SalaryStructureFk = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Payroll_EODRecordMaster", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Payroll_EODRecordMaster_Payroll_SalaryStructure_SalaryStructureFk",
                        column: x => x.SalaryStructureFk,
                        principalTable: "Payroll_SalaryStructure",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Plan_OrderActionStep",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Plan_OrderActionFK = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Priority = table.Column<int>(nullable: false),
                    DefaultRange = table.Column<int>(nullable: false),
                    SetColor = table.Column<string>(nullable: true),
                    ExtendedRange = table.Column<int>(nullable: false),
                    ExtendColor = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Plan_OrderActionStep", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Plan_OrderActionStep_Plan_OrderAction_Plan_OrderActionFK",
                        column: x => x.Plan_OrderActionFK,
                        principalTable: "Plan_OrderAction",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "HRMS_Designation",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    HRMS_CompanyHierarchyLabelFK = table.Column<int>(nullable: true),
                    User_DepartmentFK = table.Column<int>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    AttendanceBonus = table.Column<decimal>(nullable: false),
                    Code = table.Column<string>(nullable: true),
                    IsAttendanceBonusGiven = table.Column<bool>(nullable: false),
                    NightBill = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HRMS_Designation", x => x.ID);
                    table.ForeignKey(
                        name: "FK_HRMS_Designation_HRMS_CompanyHierarchyLabel_HRMS_CompanyHierarchyLabelFK",
                        column: x => x.HRMS_CompanyHierarchyLabelFK,
                        principalTable: "HRMS_CompanyHierarchyLabel",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_HRMS_Designation_User_Department_User_DepartmentFK",
                        column: x => x.User_DepartmentFK,
                        principalTable: "User_Department",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "User_SubMenu",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    User_MenuFk = table.Column<int>(nullable: false),
                    Priority = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User_SubMenu", x => x.ID);
                    table.ForeignKey(
                        name: "FK_User_SubMenu_User_Menu_User_MenuFk",
                        column: x => x.User_MenuFk,
                        principalTable: "User_Menu",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "User_User",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    UserName = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    Photo = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Mobile = table.Column<string>(nullable: true),
                    User_AccessLevelFK = table.Column<int>(nullable: false),
                    User_RoleFK = table.Column<int>(nullable: false),
                    Locked = table.Column<bool>(nullable: false),
                    HRMS_EmployeeFK = table.Column<int>(nullable: false),
                    User_DepartmentFK = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User_User", x => x.ID);
                    table.ForeignKey(
                        name: "FK_User_User_User_AccessLevel_User_AccessLevelFK",
                        column: x => x.User_AccessLevelFK,
                        principalTable: "User_AccessLevel",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_User_User_User_Department_User_DepartmentFK",
                        column: x => x.User_DepartmentFK,
                        principalTable: "User_Department",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_User_User_User_Role_User_RoleFK",
                        column: x => x.User_RoleFK,
                        principalTable: "User_Role",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Accounting_Chart2",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 150, nullable: true),
                    Code = table.Column<string>(maxLength: 20, nullable: true),
                    Accounting_Chart1FK = table.Column<int>(nullable: false),
                    Entryable = table.Column<bool>(nullable: false),
                    Addable = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Accounting_Chart2", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Accounting_Chart2_Accounting_Chart1_Accounting_Chart1FK",
                        column: x => x.Accounting_Chart1FK,
                        principalTable: "Accounting_Chart1",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "HRMS_PoliceStation",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    HRMS_DistrictFK = table.Column<int>(nullable: true),
                    Common_CountryFK = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HRMS_PoliceStation", x => x.ID);
                    table.ForeignKey(
                        name: "FK_HRMS_PoliceStation_Common_Country_Common_CountryFK",
                        column: x => x.Common_CountryFK,
                        principalTable: "Common_Country",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_HRMS_PoliceStation_HRMS_District_HRMS_DistrictFK",
                        column: x => x.HRMS_DistrictFK,
                        principalTable: "HRMS_District",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Common_FinishItem",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Common_FinishSubCategoryFK = table.Column<int>(nullable: false),
                    Common_UnitFK = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_FinishItem", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Common_FinishItem_Common_FinishSubCategory_Common_FinishSubCategoryFK",
                        column: x => x.Common_FinishSubCategoryFK,
                        principalTable: "Common_FinishSubCategory",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Common_FinishItem_Common_Unit_Common_UnitFK",
                        column: x => x.Common_UnitFK,
                        principalTable: "Common_Unit",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Common_SectionLine",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: true),
                    BuildingFk = table.Column<int>(nullable: false),
                    FloorFk = table.Column<int>(nullable: false),
                    Common_ProductionFloorFK = table.Column<int>(nullable: true),
                    IsClosed = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_SectionLine", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Common_SectionLine_Common_ProductionFloor_Common_ProductionFloorFK",
                        column: x => x.Common_ProductionFloorFK,
                        principalTable: "Common_ProductionFloor",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Common_RawItem",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Common_RawSubCategoryFK = table.Column<int>(nullable: false),
                    Common_UnitFK = table.Column<int>(nullable: false),
                    Quantity = table.Column<double>(nullable: false),
                    GSM = table.Column<string>(nullable: true),
                    AccountHead = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_RawItem", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Common_RawItem_Common_RawSubCategory_Common_RawSubCategoryFK",
                        column: x => x.Common_RawSubCategoryFK,
                        principalTable: "Common_RawSubCategory",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Common_RawItem_Common_Unit_Common_UnitFK",
                        column: x => x.Common_UnitFK,
                        principalTable: "Common_Unit",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "HRMS_Section",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: true),
                    HRMS_UnitFK = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HRMS_Section", x => x.ID);
                    table.ForeignKey(
                        name: "FK_HRMS_Section_HRMS_Unit_HRMS_UnitFK",
                        column: x => x.HRMS_UnitFK,
                        principalTable: "HRMS_Unit",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Store_General",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    StoreName = table.Column<string>(nullable: true),
                    Location = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Capacity = table.Column<int>(nullable: false),
                    Common_StoreTypeFK = table.Column<int>(nullable: true),
                    HRMS_BusinessUnitFK = table.Column<int>(nullable: false),
                    HRMS_UnitFK = table.Column<int>(nullable: false),
                    User_DepartmentFK = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Store_General", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Store_General_HRMS_BusinessUnit_HRMS_BusinessUnitFK",
                        column: x => x.HRMS_BusinessUnitFK,
                        principalTable: "HRMS_BusinessUnit",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Store_General_HRMS_Unit_HRMS_UnitFK",
                        column: x => x.HRMS_UnitFK,
                        principalTable: "HRMS_Unit",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Store_General_User_Department_User_DepartmentFK",
                        column: x => x.User_DepartmentFK,
                        principalTable: "User_Department",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Payroll_EODRecord",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Payroll_EODRecordMasterFk = table.Column<int>(nullable: false),
                    EODReferenceFk = table.Column<int>(nullable: false),
                    Amount = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Payroll_EODRecord", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Payroll_EODRecord_Payroll_EODRecordMaster_Payroll_EODRecordMasterFk",
                        column: x => x.Payroll_EODRecordMasterFk,
                        principalTable: "Payroll_EODRecordMaster",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "HRMS_JobDescription",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    User_DepartmentFK = table.Column<int>(nullable: true),
                    HRMS_DesignationFK = table.Column<int>(nullable: true),
                    JobDescription = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HRMS_JobDescription", x => x.ID);
                    table.ForeignKey(
                        name: "FK_HRMS_JobDescription_HRMS_Designation_HRMS_DesignationFK",
                        column: x => x.HRMS_DesignationFK,
                        principalTable: "HRMS_Designation",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_HRMS_JobDescription_User_Department_User_DepartmentFK",
                        column: x => x.User_DepartmentFK,
                        principalTable: "User_Department",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "User_MenuItem",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Priority = table.Column<int>(nullable: false),
                    User_SubMenuFk = table.Column<int>(nullable: false),
                    Method = table.Column<string>(nullable: true),
                    IsAlone = table.Column<bool>(nullable: false),
                    IsMultiLayerOption = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User_MenuItem", x => x.ID);
                    table.ForeignKey(
                        name: "FK_User_MenuItem_User_SubMenu_User_SubMenuFk",
                        column: x => x.User_SubMenuFk,
                        principalTable: "User_SubMenu",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Procurement_PurchaseInvoice",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    CID = table.Column<string>(nullable: true),
                    OriginType = table.Column<int>(nullable: false),
                    Common_SupplierFK = table.Column<int>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Status = table.Column<int>(nullable: false),
                    CreationDate = table.Column<DateTime>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    InvoiceDate = table.Column<DateTime>(nullable: false),
                    ApprovedDate = table.Column<DateTime>(nullable: false),
                    PaymentMethod = table.Column<int>(nullable: false),
                    CancellationRemaks = table.Column<string>(nullable: true),
                    ClosedByUserFK = table.Column<int>(nullable: true),
                    TotalInvoiceValue = table.Column<double>(nullable: false),
                    InvoiceMatureDate = table.Column<DateTime>(nullable: false),
                    SupplierInvoiceNo = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Procurement_PurchaseInvoice", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Procurement_PurchaseInvoice_User_User_ClosedByUserFK",
                        column: x => x.ClosedByUserFK,
                        principalTable: "User_User",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Procurement_PurchaseInvoice_Common_Supplier_Common_SupplierFK",
                        column: x => x.Common_SupplierFK,
                        principalTable: "Common_Supplier",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Procurement_PurchaseOrder",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    CID = table.Column<string>(nullable: true),
                    OriginType = table.Column<int>(nullable: false),
                    Common_SupplierFK = table.Column<int>(nullable: false),
                    Common_CurrencyFK = table.Column<int>(nullable: false),
                    TermsAndCondition = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Status = table.Column<int>(nullable: false),
                    CreationDate = table.Column<DateTime>(nullable: false),
                    DeliveryAddress = table.Column<string>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: true),
                    OrderDate = table.Column<DateTime>(nullable: false),
                    DeliveryDate = table.Column<DateTime>(nullable: true),
                    ApprovedDate = table.Column<DateTime>(nullable: true),
                    RevisionNo = table.Column<int>(nullable: false),
                    POPaymentMethod = table.Column<int>(nullable: false),
                    POType = table.Column<int>(nullable: false),
                    CancellationRemaks = table.Column<string>(nullable: true),
                    ClosedByUserFK = table.Column<int>(nullable: true),
                    TotalPOValue = table.Column<double>(nullable: false),
                    Commercial_BBLCFk = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Procurement_PurchaseOrder", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Procurement_PurchaseOrder_User_User_ClosedByUserFK",
                        column: x => x.ClosedByUserFK,
                        principalTable: "User_User",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Procurement_PurchaseOrder_Common_Currency_Common_CurrencyFK",
                        column: x => x.Common_CurrencyFK,
                        principalTable: "Common_Currency",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Procurement_PurchaseOrder_Common_Supplier_Common_SupplierFK",
                        column: x => x.Common_SupplierFK,
                        principalTable: "Common_Supplier",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Procurement_PurchaseRequisition",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    CID = table.Column<string>(nullable: true),
                    PRType = table.Column<int>(nullable: false),
                    OriginType = table.Column<int>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    Status = table.Column<int>(nullable: false),
                    CreationDate = table.Column<DateTime>(nullable: false),
                    CancellationRemaks = table.Column<string>(nullable: true),
                    ClosedByUserFK = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Procurement_PurchaseRequisition", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Procurement_PurchaseRequisition_User_User_ClosedByUserFK",
                        column: x => x.ClosedByUserFK,
                        principalTable: "User_User",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Accounting_Head",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 150, nullable: true),
                    Code = table.Column<string>(maxLength: 20, nullable: true),
                    Accounting_Chart2FK = table.Column<int>(nullable: false),
                    Balance = table.Column<decimal>(nullable: false),
                    Editable = table.Column<bool>(nullable: false),
                    Journalable = table.Column<bool>(nullable: false),
                    BaseHeadId = table.Column<int>(nullable: true),
                    HeadType = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Accounting_Head", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Accounting_Head_Accounting_Chart2_Accounting_Chart2FK",
                        column: x => x.Accounting_Chart2FK,
                        principalTable: "Accounting_Chart2",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "HRMS_PostOffice",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    HRMS_DistrictFK = table.Column<int>(nullable: true),
                    HRMS_PoliceStationFK = table.Column<int>(nullable: true),
                    Common_CountryFK = table.Column<int>(nullable: true),
                    PostCode = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HRMS_PostOffice", x => x.ID);
                    table.ForeignKey(
                        name: "FK_HRMS_PostOffice_Common_Country_Common_CountryFK",
                        column: x => x.Common_CountryFK,
                        principalTable: "Common_Country",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_HRMS_PostOffice_HRMS_District_HRMS_DistrictFK",
                        column: x => x.HRMS_DistrictFK,
                        principalTable: "HRMS_District",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_HRMS_PostOffice_HRMS_PoliceStation_HRMS_PoliceStationFK",
                        column: x => x.HRMS_PoliceStationFK,
                        principalTable: "HRMS_PoliceStation",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Merchandising_CBSStyle",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Common_FinishItemFK = table.Column<int>(nullable: false),
                    CID = table.Column<string>(nullable: true),
                    FirstMove = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: false),
                    Class = table.Column<string>(nullable: true),
                    Fabrication = table.Column<string>(nullable: true),
                    StyleName = table.Column<string>(nullable: true),
                    SetQuantity = table.Column<int>(nullable: false),
                    PackPieceQty = table.Column<int>(nullable: false),
                    PackQty = table.Column<int>(nullable: false),
                    PieceQty = table.Column<int>(nullable: false),
                    UnitPrice = table.Column<decimal>(nullable: false),
                    PackPrice = table.Column<decimal>(nullable: false),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Merchandising_CBSStyle", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Merchandising_CBSStyle_Common_FinishItem_Common_FinishItemFK",
                        column: x => x.Common_FinishItemFK,
                        principalTable: "Common_FinishItem",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Common_ProductionLine",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    ProductionStepFK = table.Column<int>(nullable: true),
                    Code = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Type = table.Column<string>(nullable: true),
                    Priority = table.Column<int>(nullable: false),
                    Common_SectionLineFK = table.Column<int>(nullable: false),
                    Common_SectionLineID = table.Column<int>(nullable: true),
                    IsClosed = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_ProductionLine", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Common_ProductionLine_Common_SectionLine_Common_SectionLineID",
                        column: x => x.Common_SectionLineID,
                        principalTable: "Common_SectionLine",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Merchandising_Style",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Merchandising_BuyerOrderFK = table.Column<int>(nullable: false),
                    Common_FinishItemFK = table.Column<int>(nullable: false),
                    Common_CurrencyFK = table.Column<int>(nullable: false),
                    CID = table.Column<string>(nullable: true),
                    FirstMove = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: false),
                    Class = table.Column<string>(nullable: true),
                    Fabrication = table.Column<string>(nullable: true),
                    StyleName = table.Column<string>(nullable: true),
                    ReferenceNo = table.Column<string>(nullable: true),
                    Kimball = table.Column<string>(nullable: true),
                    SetQuantity = table.Column<int>(nullable: false),
                    PackPieceQty = table.Column<int>(nullable: false),
                    PackQty = table.Column<int>(nullable: false),
                    PieceQty = table.Column<int>(nullable: false),
                    PackInCTN = table.Column<int>(nullable: false),
                    UnitPrice = table.Column<decimal>(type: "decimal(18,5)", nullable: false),
                    PackPrice = table.Column<decimal>(type: "decimal(18,5)", nullable: false),
                    MakingCost = table.Column<decimal>(type: "decimal(18,5)", nullable: false),
                    OverheadRate = table.Column<decimal>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    POChangeHistory = table.Column<string>(nullable: true),
                    IsComplete = table.Column<bool>(nullable: false),
                    IsShipmentComplete = table.Column<bool>(nullable: false),
                    Common_RawItemID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Merchandising_Style", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Merchandising_Style_Common_FinishItem_Common_FinishItemFK",
                        column: x => x.Common_FinishItemFK,
                        principalTable: "Common_FinishItem",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Merchandising_Style_Common_RawItem_Common_RawItemID",
                        column: x => x.Common_RawItemID,
                        principalTable: "Common_RawItem",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Merchandising_Style_Merchandising_BuyerOrder_Merchandising_BuyerOrderFK",
                        column: x => x.Merchandising_BuyerOrderFK,
                        principalTable: "Merchandising_BuyerOrder",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Store_Bin",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    CID = table.Column<string>(nullable: true),
                    Section = table.Column<string>(nullable: true),
                    Row = table.Column<int>(nullable: false),
                    Shelf = table.Column<int>(nullable: false),
                    Rack = table.Column<int>(nullable: false),
                    Capacity = table.Column<int>(nullable: false),
                    TotalRow = table.Column<int>(nullable: false),
                    TotalShelf = table.Column<int>(nullable: false),
                    Dimension = table.Column<int>(nullable: false),
                    ItemTotalQty = table.Column<double>(nullable: false),
                    Store_GeneralFK = table.Column<int>(nullable: false),
                    Common_RawItemFK = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Store_Bin", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Store_Bin_Store_General_Store_GeneralFK",
                        column: x => x.Store_GeneralFK,
                        principalTable: "Store_General",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Store_Requisition",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    CID = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Status = table.Column<int>(nullable: false),
                    CreationDate = table.Column<DateTime>(nullable: false),
                    CancellationRemaks = table.Column<string>(nullable: true),
                    RequisitionType = table.Column<int>(nullable: false),
                    ClosedByUserFK = table.Column<int>(nullable: true),
                    User_UserFK = table.Column<int>(nullable: true),
                    Store_GeneralOutFK = table.Column<int>(nullable: true),
                    Store_GeneralInFK = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Store_Requisition", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Store_Requisition_Store_General_Store_GeneralInFK",
                        column: x => x.Store_GeneralInFK,
                        principalTable: "Store_General",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Store_Requisition_Store_General_Store_GeneralOutFK",
                        column: x => x.Store_GeneralOutFK,
                        principalTable: "Store_General",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Store_Requisition_User_User_User_UserFK",
                        column: x => x.User_UserFK,
                        principalTable: "User_User",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Store_Section",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    SectionName = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Capacity = table.Column<int>(nullable: false),
                    Store_GeneralFK = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Store_Section", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Store_Section_Store_General_Store_GeneralFK",
                        column: x => x.Store_GeneralFK,
                        principalTable: "Store_General",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Store_StockOut",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    CID = table.Column<string>(nullable: true),
                    ChallanNo = table.Column<string>(nullable: true),
                    ChallanDate = table.Column<DateTime>(nullable: false),
                    ReceivedDate = table.Column<DateTime>(nullable: false),
                    WarrentyDate = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    StockType = table.Column<string>(nullable: true),
                    StoreInType = table.Column<string>(nullable: true),
                    IsLocked = table.Column<bool>(nullable: false),
                    UnitPrice = table.Column<double>(nullable: false),
                    TotalPrice = table.Column<double>(nullable: false),
                    RemainingQty = table.Column<double>(nullable: false),
                    DamagedQty = table.Column<double>(nullable: false),
                    ReceivedQty = table.Column<double>(nullable: false),
                    FixAsset = table.Column<bool>(nullable: false),
                    BinDefine = table.Column<bool>(nullable: false),
                    CommonUnitFK = table.Column<int>(nullable: false),
                    ItemIDFK = table.Column<int>(nullable: false),
                    IPOFK = table.Column<int>(nullable: false),
                    CommonSupplierFK = table.Column<int>(nullable: false),
                    CommonStoreFK = table.Column<int>(nullable: false),
                    StoreBinFK = table.Column<int>(nullable: false),
                    CommonCategoryFK = table.Column<int>(nullable: false),
                    CommonSubCategoryFK = table.Column<int>(nullable: false),
                    Common_Size = table.Column<decimal>(nullable: false),
                    Merchandising_StyleFK = table.Column<int>(nullable: false),
                    Merchandising_StyleSlaveFK = table.Column<int>(nullable: false),
                    Procurement_PurchaseOrderSlaveFK = table.Column<int>(nullable: false),
                    Store_RequisitionSlaveFK = table.Column<int>(nullable: false),
                    Common_RawItemFK = table.Column<int>(nullable: false),
                    Store_StockOutMasterFK = table.Column<int>(nullable: false),
                    Common_SupplierFK = table.Column<int>(nullable: false),
                    Store_GeneralFK = table.Column<int>(nullable: false),
                    Common_ColorFK = table.Column<int>(nullable: true),
                    Store_GeneralOutFK = table.Column<int>(nullable: true),
                    Store_GeneralInFK = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Store_StockOut", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Store_StockOut_Common_Color_Common_ColorFK",
                        column: x => x.Common_ColorFK,
                        principalTable: "Common_Color",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Store_StockOut_Common_RawItem_Common_RawItemFK",
                        column: x => x.Common_RawItemFK,
                        principalTable: "Common_RawItem",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Store_StockOut_Common_Supplier_Common_SupplierFK",
                        column: x => x.Common_SupplierFK,
                        principalTable: "Common_Supplier",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Store_StockOut_Store_General_Store_GeneralFK",
                        column: x => x.Store_GeneralFK,
                        principalTable: "Store_General",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Store_StockOut_Store_StockOutMaster_Store_StockOutMasterFK",
                        column: x => x.Store_StockOutMasterFK,
                        principalTable: "Store_StockOutMaster",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Store_StockOutSlave",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    PurchaseQuantity = table.Column<double>(nullable: false),
                    RemainingQty = table.Column<double>(nullable: false),
                    DamagedQty = table.Column<double>(nullable: false),
                    ReceivedQty = table.Column<double>(nullable: false),
                    UnitPrice = table.Column<double>(nullable: false),
                    TotalPrice = table.Column<double>(nullable: false),
                    IsLocked = table.Column<bool>(nullable: false),
                    FixAsset = table.Column<bool>(nullable: false),
                    BinDefine = table.Column<bool>(nullable: false),
                    Common_RawItemFK = table.Column<int>(nullable: false),
                    Store_StockOutMasterFK = table.Column<int>(nullable: false),
                    Common_SupplierFK = table.Column<int>(nullable: false),
                    Store_GeneralFK = table.Column<int>(nullable: false),
                    Common_ColorFK = table.Column<int>(nullable: true),
                    Common_Size = table.Column<decimal>(nullable: false),
                    Merchandising_StyleFK = table.Column<int>(nullable: false),
                    Merchandising_StyleSlaveFK = table.Column<int>(nullable: false),
                    PurchaseOrderSlaveFK = table.Column<int>(nullable: false),
                    Store_BinFK = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Store_StockOutSlave", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Store_StockOutSlave_Common_Color_Common_ColorFK",
                        column: x => x.Common_ColorFK,
                        principalTable: "Common_Color",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Store_StockOutSlave_Common_RawItem_Common_RawItemFK",
                        column: x => x.Common_RawItemFK,
                        principalTable: "Common_RawItem",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Store_StockOutSlave_Common_Supplier_Common_SupplierFK",
                        column: x => x.Common_SupplierFK,
                        principalTable: "Common_Supplier",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Store_StockOutSlave_Store_General_Store_GeneralFK",
                        column: x => x.Store_GeneralFK,
                        principalTable: "Store_General",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Store_StockOutSlave_Store_StockOutMaster_Store_StockOutMasterFK",
                        column: x => x.Store_StockOutMasterFK,
                        principalTable: "Store_StockOutMaster",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "User_RoleMenuItem",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    User_RoleFK = table.Column<int>(nullable: false),
                    User_MenuItemFk = table.Column<int>(nullable: false),
                    IsAllowed = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User_RoleMenuItem", x => x.ID);
                    table.ForeignKey(
                        name: "FK_User_RoleMenuItem_User_MenuItem_User_MenuItemFk",
                        column: x => x.User_MenuItemFk,
                        principalTable: "User_MenuItem",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_User_RoleMenuItem_User_Role_User_RoleFK",
                        column: x => x.User_RoleFK,
                        principalTable: "User_Role",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Store_StockInMaster",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    CID = table.Column<string>(nullable: true),
                    ChallanNo = table.Column<string>(nullable: true),
                    ChallanDate = table.Column<DateTime>(nullable: false),
                    ReceivedDate = table.Column<DateTime>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    StockType = table.Column<string>(nullable: true),
                    StoreInType = table.Column<string>(nullable: true),
                    PurchaseOrderFK = table.Column<int>(nullable: true),
                    Common_SupplierFK = table.Column<int>(nullable: true),
                    Store_GeneralFK = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Store_StockInMaster", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Store_StockInMaster_Common_Supplier_Common_SupplierFK",
                        column: x => x.Common_SupplierFK,
                        principalTable: "Common_Supplier",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Store_StockInMaster_Procurement_PurchaseOrder_PurchaseOrderFK",
                        column: x => x.PurchaseOrderFK,
                        principalTable: "Procurement_PurchaseOrder",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Store_StockInMaster_Store_General_Store_GeneralFK",
                        column: x => x.Store_GeneralFK,
                        principalTable: "Store_General",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Accounting_JournalSlave",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Title = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Remark = table.Column<string>(nullable: true),
                    Debit = table.Column<decimal>(nullable: false),
                    Credit = table.Column<decimal>(nullable: false),
                    ForeignCurrencyAmount = table.Column<decimal>(nullable: false),
                    ConversionRateToBDT = table.Column<decimal>(nullable: false),
                    Accounting_JournalFK = table.Column<int>(nullable: false),
                    Accounting_HeadFK = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Accounting_JournalSlave", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Accounting_JournalSlave_Accounting_Head_Accounting_HeadFK",
                        column: x => x.Accounting_HeadFK,
                        principalTable: "Accounting_Head",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Accounting_JournalSlave_Accounting_Journal_Accounting_JournalFK",
                        column: x => x.Accounting_JournalFK,
                        principalTable: "Accounting_Journal",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Merchandising_CBS",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    CID = table.Column<string>(nullable: true),
                    Merchandising_CBSStyleFK = table.Column<int>(nullable: false),
                    Common_BuyerFK = table.Column<int>(nullable: false),
                    OrderQty = table.Column<int>(nullable: false),
                    OverheadRate = table.Column<decimal>(nullable: false),
                    ProfitMargin = table.Column<decimal>(nullable: false),
                    BuyingCommission = table.Column<decimal>(nullable: false),
                    DozonQty = table.Column<decimal>(nullable: false),
                    IsLocked = table.Column<bool>(nullable: false),
                    Reference = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Merchandising_CBS", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Merchandising_CBS_Merchandising_CBSStyle_Merchandising_CBSStyleFK",
                        column: x => x.Merchandising_CBSStyleFK,
                        principalTable: "Merchandising_CBSStyle",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Common_LineChiefLine",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Common_LineChiefFk = table.Column<int>(nullable: true),
                    Common_LineSuperVisorFk = table.Column<int>(nullable: true),
                    Common_ProductionLineFk = table.Column<int>(nullable: false),
                    IsLineChief = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Common_LineChiefLine", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Common_LineChiefLine_Common_LineChief_Common_LineChiefFk",
                        column: x => x.Common_LineChiefFk,
                        principalTable: "Common_LineChief",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Common_LineChiefLine_Common_LineSuperVisor_Common_LineSuperVisorFk",
                        column: x => x.Common_LineSuperVisorFk,
                        principalTable: "Common_LineSuperVisor",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Common_LineChiefLine_Common_ProductionLine_Common_ProductionLineFk",
                        column: x => x.Common_ProductionLineFk,
                        principalTable: "Common_ProductionLine",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "HRMS_Employee",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    CardNo = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    NameBN = table.Column<string>(nullable: true),
                    FatherName = table.Column<string>(nullable: true),
                    MotherName = table.Column<string>(nullable: true),
                    DOB = table.Column<DateTime>(nullable: false),
                    Gender = table.Column<string>(nullable: true),
                    HRMS_DesignationFK = table.Column<int>(nullable: true),
                    User_DepartmentFK = table.Column<int>(nullable: true),
                    HRMS_SectionFK = table.Column<int>(nullable: true),
                    Common_CountryFK = table.Column<int>(nullable: true),
                    HRMS_PostOfficeFK = table.Column<int>(nullable: true),
                    Common_ProductionLineFK = table.Column<int>(nullable: true),
                    BloodGroup = table.Column<int>(nullable: false),
                    JoiningDate = table.Column<DateTime>(nullable: false),
                    Religion = table.Column<string>(nullable: true),
                    AltPersonName = table.Column<string>(nullable: true),
                    MobileNo = table.Column<string>(nullable: true),
                    AltMobileNo = table.Column<string>(nullable: true),
                    AltPersonContactNo = table.Column<string>(nullable: true),
                    AltPersonAddress = table.Column<string>(nullable: true),
                    EmergencyContactNo = table.Column<string>(nullable: true),
                    OfficeContact = table.Column<string>(nullable: true),
                    LandPhone = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    EmployementType = table.Column<int>(nullable: false),
                    NIDNo = table.Column<string>(nullable: true),
                    PassportNo = table.Column<string>(nullable: true),
                    PresentAddress = table.Column<string>(nullable: true),
                    PermanentAddress = table.Column<string>(nullable: true),
                    MaritalStatus = table.Column<string>(nullable: true),
                    PresentStatus = table.Column<int>(nullable: false),
                    StaffType = table.Column<int>(nullable: false),
                    OvertimeEligibility = table.Column<string>(nullable: true),
                    Grade = table.Column<string>(nullable: true),
                    QuitDate = table.Column<DateTime>(nullable: true),
                    EmployeeIdentity = table.Column<string>(nullable: true),
                    IsLeaveAssigned = table.Column<bool>(nullable: false),
                    IsWeekendAssigned = table.Column<bool>(nullable: false),
                    LineRoleId = table.Column<int>(nullable: true),
                    ReportingDesignationId = table.Column<int>(nullable: true),
                    ImagePath = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HRMS_Employee", x => x.ID);
                    table.ForeignKey(
                        name: "FK_HRMS_Employee_Common_Country_Common_CountryFK",
                        column: x => x.Common_CountryFK,
                        principalTable: "Common_Country",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_HRMS_Employee_Common_ProductionLine_Common_ProductionLineFK",
                        column: x => x.Common_ProductionLineFK,
                        principalTable: "Common_ProductionLine",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_HRMS_Employee_HRMS_Designation_HRMS_DesignationFK",
                        column: x => x.HRMS_DesignationFK,
                        principalTable: "HRMS_Designation",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_HRMS_Employee_HRMS_PostOffice_HRMS_PostOfficeFK",
                        column: x => x.HRMS_PostOfficeFK,
                        principalTable: "HRMS_PostOffice",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_HRMS_Employee_HRMS_Section_HRMS_SectionFK",
                        column: x => x.HRMS_SectionFK,
                        principalTable: "HRMS_Section",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_HRMS_Employee_User_Department_User_DepartmentFK",
                        column: x => x.User_DepartmentFK,
                        principalTable: "User_Department",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Merchandising_BOF",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Merchandising_StyleFK = table.Column<int>(nullable: false),
                    Merchandising_YarnCalculationFK = table.Column<int>(nullable: false),
                    Merchandising_StyleSlaveFK_Fabric = table.Column<int>(nullable: false),
                    Common_RawItemFK = table.Column<int>(nullable: true),
                    Common_CurrencyFK = table.Column<int>(nullable: true),
                    Common_UnitFK = table.Column<int>(nullable: true),
                    Description = table.Column<string>(maxLength: 200, nullable: true),
                    Consumption = table.Column<decimal>(type: "decimal(18,5)", nullable: false),
                    RequiredQuantity = table.Column<decimal>(type: "decimal(18,5)", nullable: true),
                    Price = table.Column<decimal>(type: "decimal(18,5)", nullable: false),
                    IsFabric = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Merchandising_BOF", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Merchandising_BOF_Common_RawItem_Common_RawItemFK",
                        column: x => x.Common_RawItemFK,
                        principalTable: "Common_RawItem",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Merchandising_BOF_Merchandising_Style_Merchandising_StyleFK",
                        column: x => x.Merchandising_StyleFK,
                        principalTable: "Merchandising_Style",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Merchandising_StyleMeasurement",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Merchandising_StyleFK = table.Column<int>(nullable: false),
                    Common_ColorFK = table.Column<int>(nullable: false),
                    Common_SizeFK = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Merchandising_StyleMeasurement", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Merchandising_StyleMeasurement_Common_Color_Common_ColorFK",
                        column: x => x.Common_ColorFK,
                        principalTable: "Common_Color",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Merchandising_StyleMeasurement_Common_Size_Common_SizeFK",
                        column: x => x.Common_SizeFK,
                        principalTable: "Common_Size",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Merchandising_StyleMeasurement_Merchandising_Style_Merchandising_StyleFK",
                        column: x => x.Merchandising_StyleFK,
                        principalTable: "Merchandising_Style",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Merchandising_StyleSetPack",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Merchandising_StyleFK = table.Column<int>(nullable: false),
                    SetPackName = table.Column<string>(nullable: true),
                    Quantity = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Merchandising_StyleSetPack", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Merchandising_StyleSetPack_Merchandising_Style_Merchandising_StyleFK",
                        column: x => x.Merchandising_StyleFK,
                        principalTable: "Merchandising_Style",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Merchandising_StyleShipmentSchedule",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Merchandising_StyleFK = table.Column<int>(nullable: false),
                    Common_CountryFK = table.Column<int>(nullable: false),
                    Common_CountryPortFK = table.Column<int>(nullable: false),
                    DestNo = table.Column<string>(nullable: true),
                    ShipmentDate = table.Column<DateTime>(nullable: false),
                    Quantity = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Merchandising_StyleShipmentSchedule", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Merchandising_StyleShipmentSchedule_Common_Country_Common_CountryFK",
                        column: x => x.Common_CountryFK,
                        principalTable: "Common_Country",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Merchandising_StyleShipmentSchedule_Merchandising_Style_Merchandising_StyleFK",
                        column: x => x.Merchandising_StyleFK,
                        principalTable: "Merchandising_Style",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Plan_OrderProcess",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Plan_OrderActionStepFK = table.Column<int>(nullable: false),
                    Merchandising_StyleFK = table.Column<int>(nullable: false),
                    PlanStartDate = table.Column<DateTime>(nullable: false),
                    PlanFinishDate = table.Column<DateTime>(nullable: false),
                    ActualStartDate = table.Column<DateTime>(nullable: false),
                    ActualFinishDate = table.Column<DateTime>(nullable: false),
                    ExtendDate = table.Column<DateTime>(nullable: false),
                    AssignedTo = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Plan_OrderProcess", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Plan_OrderProcess_Merchandising_Style_Merchandising_StyleFK",
                        column: x => x.Merchandising_StyleFK,
                        principalTable: "Merchandising_Style",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Plan_OrderProcess_Plan_OrderActionStep_Plan_OrderActionStepFK",
                        column: x => x.Plan_OrderActionStepFK,
                        principalTable: "Plan_OrderActionStep",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Plan_SMVMasterLayout",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    PLan_SMVDraftLayoutFK = table.Column<int>(nullable: true),
                    Merchandising_StyleSetPackFK = table.Column<int>(nullable: true),
                    Merchandising_StyleFK = table.Column<int>(nullable: false),
                    NoOfOperator = table.Column<int>(nullable: false),
                    NoOfHelper = table.Column<int>(nullable: false),
                    OperatorSMV = table.Column<decimal>(nullable: false),
                    HelperSMV = table.Column<decimal>(nullable: false),
                    TotalWorker = table.Column<int>(nullable: false),
                    TotalSMV = table.Column<decimal>(nullable: false),
                    WorkingHour = table.Column<int>(nullable: false),
                    PerHourTarget = table.Column<int>(nullable: false),
                    Efficiency = table.Column<decimal>(nullable: false),
                    QtyRate = table.Column<decimal>(type: "decimal(18,5)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Plan_SMVMasterLayout", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Plan_SMVMasterLayout_Merchandising_Style_Merchandising_StyleFK",
                        column: x => x.Merchandising_StyleFK,
                        principalTable: "Merchandising_Style",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Plan_SMVMasterLayout_Plan_SMVDraftLayout_PLan_SMVDraftLayoutFK",
                        column: x => x.PLan_SMVDraftLayoutFK,
                        principalTable: "Plan_SMVDraftLayout",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Store_RequisitionSlave",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    RequisitionQuantity = table.Column<double>(nullable: false),
                    RemainingQty = table.Column<double>(nullable: false),
                    InspectionRequired = table.Column<bool>(nullable: false),
                    Common_ColorFK = table.Column<int>(nullable: true),
                    Store_RequisitionFK = table.Column<int>(nullable: false),
                    Common_RawItemFK = table.Column<int>(nullable: false),
                    Common_Size = table.Column<decimal>(nullable: false),
                    Merchandising_StyleFK = table.Column<int>(nullable: false),
                    Merchandising_StyleSlaveFK = table.Column<int>(nullable: false),
                    HRMS_EmployeeFK = table.Column<int>(nullable: false),
                    RequiredDate = table.Column<DateTime>(nullable: false),
                    Merchandising_StyleSetPackFK = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Store_RequisitionSlave", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Store_RequisitionSlave_Common_Color_Common_ColorFK",
                        column: x => x.Common_ColorFK,
                        principalTable: "Common_Color",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Store_RequisitionSlave_Common_RawItem_Common_RawItemFK",
                        column: x => x.Common_RawItemFK,
                        principalTable: "Common_RawItem",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Store_RequisitionSlave_Store_Requisition_Store_RequisitionFK",
                        column: x => x.Store_RequisitionFK,
                        principalTable: "Store_Requisition",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Store_StockIn",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    CID = table.Column<string>(nullable: true),
                    ChallanNo = table.Column<string>(nullable: true),
                    ChallanDate = table.Column<DateTime>(nullable: false),
                    ReceivedDate = table.Column<DateTime>(nullable: false),
                    WarrentyDate = table.Column<DateTime>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    StockType = table.Column<string>(nullable: true),
                    StoreInType = table.Column<string>(nullable: true),
                    PurchaseQuantity = table.Column<double>(nullable: false),
                    RemainingQty = table.Column<double>(nullable: false),
                    DamagedQty = table.Column<double>(nullable: false),
                    ReceivedQty = table.Column<double>(nullable: false),
                    CommonUnitFK = table.Column<int>(nullable: false),
                    ItemIDFK = table.Column<int>(nullable: false),
                    IPOFK = table.Column<int>(nullable: false),
                    CommonSupplierFK = table.Column<int>(nullable: false),
                    CommonStoreFK = table.Column<int>(nullable: false),
                    StoreBinFK = table.Column<int>(nullable: false),
                    CommonCategoryFK = table.Column<int>(nullable: false),
                    CommonSubCategoryFK = table.Column<int>(nullable: false),
                    Common_Size = table.Column<decimal>(nullable: false),
                    Merchandising_StyleFK = table.Column<int>(nullable: false),
                    Merchandising_StyleSlaveFK = table.Column<int>(nullable: false),
                    Procurement_PurchaseOrderSlaveFK = table.Column<int>(nullable: false),
                    IsLocked = table.Column<bool>(nullable: false),
                    UnitPrice = table.Column<double>(nullable: false),
                    TotalPrice = table.Column<double>(nullable: false),
                    FixAsset = table.Column<bool>(nullable: false),
                    BinDefine = table.Column<bool>(nullable: false),
                    Common_RawItemFK = table.Column<int>(nullable: false),
                    Store_StockInMasterFK = table.Column<int>(nullable: true),
                    Common_SupplierFK = table.Column<int>(nullable: false),
                    Store_GeneralFK = table.Column<int>(nullable: false),
                    Common_ColorFK = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Store_StockIn", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Store_StockIn_Common_Color_Common_ColorFK",
                        column: x => x.Common_ColorFK,
                        principalTable: "Common_Color",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Store_StockIn_Common_RawItem_Common_RawItemFK",
                        column: x => x.Common_RawItemFK,
                        principalTable: "Common_RawItem",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Store_StockIn_Common_Supplier_Common_SupplierFK",
                        column: x => x.Common_SupplierFK,
                        principalTable: "Common_Supplier",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Store_StockIn_Store_General_Store_GeneralFK",
                        column: x => x.Store_GeneralFK,
                        principalTable: "Store_General",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Store_StockIn_Store_StockInMaster_Store_StockInMasterFK",
                        column: x => x.Store_StockInMasterFK,
                        principalTable: "Store_StockInMaster",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Store_StockInSlave",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    PurchaseQuantity = table.Column<double>(nullable: false),
                    RemainingQty = table.Column<double>(nullable: false),
                    DamagedQty = table.Column<double>(nullable: false),
                    ReceivedQty = table.Column<double>(nullable: false),
                    UnitPrice = table.Column<double>(nullable: false),
                    TotalPrice = table.Column<double>(nullable: false),
                    IsLocked = table.Column<bool>(nullable: false),
                    FixAsset = table.Column<bool>(nullable: false),
                    BinDefine = table.Column<bool>(nullable: false),
                    Common_RawItemFK = table.Column<int>(nullable: false),
                    Store_StockInMasterFK = table.Column<int>(nullable: false),
                    Common_Size = table.Column<decimal>(nullable: false),
                    Merchandising_StyleFK = table.Column<int>(nullable: false),
                    Merchandising_StyleSlaveFK = table.Column<int>(nullable: false),
                    PurchaseOrderSlaveFK = table.Column<int>(nullable: false),
                    Store_BinFK = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Store_StockInSlave", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Store_StockInSlave_Common_RawItem_Common_RawItemFK",
                        column: x => x.Common_RawItemFK,
                        principalTable: "Common_RawItem",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Store_StockInSlave_Store_StockInMaster_Store_StockInMasterFK",
                        column: x => x.Store_StockInMasterFK,
                        principalTable: "Store_StockInMaster",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Merchandising_CBSSlave",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Merchandising_CBSFK = table.Column<int>(nullable: false),
                    Common_RawItemFK = table.Column<int>(nullable: true),
                    Common_CurrencyFK = table.Column<int>(nullable: true),
                    ConRequiredQty = table.Column<decimal>(nullable: false),
                    Consumption = table.Column<decimal>(type: "decimal(18,5)", nullable: false),
                    RequiredQty = table.Column<decimal>(nullable: false),
                    UnitPrice = table.Column<decimal>(type: "decimal(18,5)", nullable: false),
                    TotalPrice = table.Column<decimal>(type: "decimal(18,5)", nullable: false),
                    GSM = table.Column<decimal>(nullable: false),
                    Common_ColorFK = table.Column<int>(nullable: true),
                    SelfReferenceFK = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Merchandising_CBSSlave", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Merchandising_CBSSlave_Common_Currency_Common_CurrencyFK",
                        column: x => x.Common_CurrencyFK,
                        principalTable: "Common_Currency",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Merchandising_CBSSlave_Common_RawItem_Common_RawItemFK",
                        column: x => x.Common_RawItemFK,
                        principalTable: "Common_RawItem",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Merchandising_CBSSlave_Merchandising_CBS_Merchandising_CBSFK",
                        column: x => x.Merchandising_CBSFK,
                        principalTable: "Merchandising_CBS",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "HRMS_AttendanceHistory",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    HRMS_EmployeeFK = table.Column<int>(nullable: true),
                    Date = table.Column<DateTime>(nullable: false),
                    InTime = table.Column<DateTime>(nullable: false),
                    OutTime = table.Column<DateTime>(nullable: false),
                    TotalTime = table.Column<int>(nullable: false),
                    Late = table.Column<int>(nullable: false),
                    OverTime = table.Column<int>(nullable: false),
                    AttendanceStatus = table.Column<int>(nullable: false),
                    CardNo = table.Column<string>(maxLength: 50, nullable: true),
                    HRMS_ShiftFK = table.Column<int>(nullable: false),
                    LeavePaidType = table.Column<int>(nullable: true),
                    EntryDate = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: false),
                    EntryByUserFK = table.Column<int>(nullable: true),
                    UpdateByUserFK = table.Column<int>(nullable: true),
                    PayableOverTime = table.Column<int>(nullable: true),
                    IsEdited = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HRMS_AttendanceHistory", x => x.ID);
                    table.ForeignKey(
                        name: "FK_HRMS_AttendanceHistory_HRMS_Employee_HRMS_EmployeeFK",
                        column: x => x.HRMS_EmployeeFK,
                        principalTable: "HRMS_Employee",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_HRMS_AttendanceHistory_HRMS_Shift_HRMS_ShiftFK",
                        column: x => x.HRMS_ShiftFK,
                        principalTable: "HRMS_Shift",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_HRMS_AttendanceHistory_User_User_UpdateByUserFK",
                        column: x => x.UpdateByUserFK,
                        principalTable: "User_User",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "HRMS_EarnLeave",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    HRMS_BusinessUnitFK = table.Column<int>(nullable: false),
                    JoiningDate = table.Column<DateTime>(nullable: true),
                    ToDate = table.Column<DateTime>(nullable: true),
                    HRMS_EmployeeFK = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HRMS_EarnLeave", x => x.ID);
                    table.ForeignKey(
                        name: "FK_HRMS_EarnLeave_HRMS_BusinessUnit_HRMS_BusinessUnitFK",
                        column: x => x.HRMS_BusinessUnitFK,
                        principalTable: "HRMS_BusinessUnit",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_HRMS_EarnLeave_HRMS_Employee_HRMS_EmployeeFK",
                        column: x => x.HRMS_EmployeeFK,
                        principalTable: "HRMS_Employee",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "HRMS_EmployeeDependent",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    HRMS_EmployeeFK = table.Column<int>(nullable: false),
                    IsSpouse = table.Column<bool>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    ContactNo = table.Column<string>(nullable: true),
                    BloodGroup = table.Column<int>(nullable: false),
                    DOB = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HRMS_EmployeeDependent", x => x.ID);
                    table.ForeignKey(
                        name: "FK_HRMS_EmployeeDependent_HRMS_Employee_HRMS_EmployeeFK",
                        column: x => x.HRMS_EmployeeFK,
                        principalTable: "HRMS_Employee",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "HRMS_EmployeeEarnLeaveCalculation",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    HRMS_EmployeeFK = table.Column<int>(nullable: false),
                    PaymentDate = table.Column<DateTime>(nullable: false),
                    TotalMonth = table.Column<int>(nullable: false),
                    WorkingDays = table.Column<int>(nullable: false),
                    Holidays = table.Column<int>(nullable: false),
                    Offdays = table.Column<int>(nullable: false),
                    TotalLeave = table.Column<int>(nullable: false),
                    Absent = table.Column<int>(nullable: false),
                    TotalDeductionDays = table.Column<int>(nullable: false),
                    PayableWorkingDays = table.Column<int>(nullable: false),
                    TotalEarnLeave = table.Column<int>(nullable: false),
                    EnjoyableEarnLeaveDays = table.Column<int>(nullable: false),
                    PayableEarnLeave = table.Column<int>(nullable: false),
                    Stamp = table.Column<int>(nullable: false),
                    PayableEarnLeaveTk = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HRMS_EmployeeEarnLeaveCalculation", x => x.ID);
                    table.ForeignKey(
                        name: "FK_HRMS_EmployeeEarnLeaveCalculation_HRMS_Employee_HRMS_EmployeeFK",
                        column: x => x.HRMS_EmployeeFK,
                        principalTable: "HRMS_Employee",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "HRMS_EmployeeEducation",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    HRMS_EducationalDegreeFK = table.Column<int>(nullable: false),
                    Institute = table.Column<string>(nullable: true),
                    GroupOrSubject = table.Column<string>(nullable: true),
                    PassingYear = table.Column<string>(nullable: true),
                    GradingType = table.Column<string>(nullable: true),
                    Result = table.Column<string>(nullable: true),
                    HRMS_EmployeeFK = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HRMS_EmployeeEducation", x => x.ID);
                    table.ForeignKey(
                        name: "FK_HRMS_EmployeeEducation_HRMS_EducationalDegree_HRMS_EducationalDegreeFK",
                        column: x => x.HRMS_EducationalDegreeFK,
                        principalTable: "HRMS_EducationalDegree",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_HRMS_EmployeeEducation_HRMS_Employee_HRMS_EmployeeFK",
                        column: x => x.HRMS_EmployeeFK,
                        principalTable: "HRMS_Employee",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "HRMS_EmployeeExperience",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Designation = table.Column<string>(nullable: true),
                    Department = table.Column<string>(nullable: true),
                    Responsibility = table.Column<string>(nullable: true),
                    JobStartDate = table.Column<DateTime>(nullable: false),
                    JobEndDate = table.Column<DateTime>(nullable: false),
                    Duration = table.Column<string>(nullable: true),
                    Location = table.Column<string>(nullable: true),
                    Comments = table.Column<string>(nullable: true),
                    HRMS_EmployeeFK = table.Column<int>(nullable: false),
                    CompanyName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HRMS_EmployeeExperience", x => x.ID);
                    table.ForeignKey(
                        name: "FK_HRMS_EmployeeExperience_HRMS_Employee_HRMS_EmployeeFK",
                        column: x => x.HRMS_EmployeeFK,
                        principalTable: "HRMS_Employee",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "HRMS_EmployeeImage",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    HRMS_EmployeeFK = table.Column<int>(nullable: false),
                    Image = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HRMS_EmployeeImage", x => x.ID);
                    table.ForeignKey(
                        name: "FK_HRMS_EmployeeImage_HRMS_Employee_HRMS_EmployeeFK",
                        column: x => x.HRMS_EmployeeFK,
                        principalTable: "HRMS_Employee",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "HRMS_EmployeeLeaveAssign",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    HRMS_EmployeeFK = table.Column<int>(nullable: false),
                    HRMS_LeaveFK = table.Column<int>(nullable: false),
                    Year = table.Column<int>(nullable: false),
                    AnnualLeaveDays = table.Column<int>(nullable: true),
                    TakenAnnualLeaveDays = table.Column<int>(nullable: true),
                    CasualLeaveDays = table.Column<int>(nullable: true),
                    TakenCasualLeaveDays = table.Column<int>(nullable: true),
                    SickLeaveDays = table.Column<int>(nullable: true),
                    TakenSickLeaveDays = table.Column<int>(nullable: true),
                    MaternityLeaveDays = table.Column<int>(nullable: true),
                    TakenMaternityLeaveDays = table.Column<int>(nullable: true),
                    EarnLeaveDays = table.Column<int>(nullable: true),
                    TakenEarnLeaveDays = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HRMS_EmployeeLeaveAssign", x => x.ID);
                    table.ForeignKey(
                        name: "FK_HRMS_EmployeeLeaveAssign_HRMS_Employee_HRMS_EmployeeFK",
                        column: x => x.HRMS_EmployeeFK,
                        principalTable: "HRMS_Employee",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_HRMS_EmployeeLeaveAssign_HRMS_Leave_HRMS_LeaveFK",
                        column: x => x.HRMS_LeaveFK,
                        principalTable: "HRMS_Leave",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "HRMS_EmployeeNominee",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    FatherName = table.Column<string>(nullable: true),
                    MotherName = table.Column<string>(nullable: true),
                    Relation = table.Column<string>(nullable: true),
                    NID = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    Contact = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    HRMS_EmployeeFK = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HRMS_EmployeeNominee", x => x.ID);
                    table.ForeignKey(
                        name: "FK_HRMS_EmployeeNominee_HRMS_Employee_HRMS_EmployeeFK",
                        column: x => x.HRMS_EmployeeFK,
                        principalTable: "HRMS_Employee",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "HRMS_EmployeeReference",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    EmailId = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    Relation = table.Column<string>(nullable: true),
                    HRMS_EmployeeFK = table.Column<int>(nullable: false),
                    RefereeName = table.Column<string>(nullable: true),
                    ContactNo = table.Column<string>(nullable: true),
                    IsVarified = table.Column<bool>(nullable: false),
                    VarifyingComments = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HRMS_EmployeeReference", x => x.ID);
                    table.ForeignKey(
                        name: "FK_HRMS_EmployeeReference_HRMS_Employee_HRMS_EmployeeFK",
                        column: x => x.HRMS_EmployeeFK,
                        principalTable: "HRMS_Employee",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "HRMS_EmployeeServiceBenifit",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    HRMS_EmployeeFK = table.Column<int>(nullable: false),
                    ResignationDate = table.Column<DateTime>(nullable: false),
                    Year = table.Column<int>(nullable: false),
                    Month = table.Column<int>(nullable: false),
                    Day = table.Column<int>(nullable: false),
                    PayableServiceBenifitDays = table.Column<int>(nullable: false),
                    PresentSalary = table.Column<decimal>(nullable: false),
                    PresentBasic = table.Column<decimal>(nullable: false),
                    PresentBasicPerDay = table.Column<decimal>(nullable: false),
                    TotalAmount = table.Column<decimal>(nullable: false),
                    Stamp = table.Column<int>(nullable: false),
                    PayableAmount = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HRMS_EmployeeServiceBenifit", x => x.ID);
                    table.ForeignKey(
                        name: "FK_HRMS_EmployeeServiceBenifit_HRMS_Employee_HRMS_EmployeeFK",
                        column: x => x.HRMS_EmployeeFK,
                        principalTable: "HRMS_Employee",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "HRMS_EmployeeSkill",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    HRMS_EmployeeFK = table.Column<int>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    Type = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HRMS_EmployeeSkill", x => x.ID);
                    table.ForeignKey(
                        name: "FK_HRMS_EmployeeSkill_HRMS_Employee_HRMS_EmployeeFK",
                        column: x => x.HRMS_EmployeeFK,
                        principalTable: "HRMS_Employee",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "HRMS_EmployeeTraining",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Institute = table.Column<string>(nullable: true),
                    CountryID = table.Column<int>(nullable: false),
                    CountryName = table.Column<string>(nullable: true),
                    CourseStartDate = table.Column<DateTime>(nullable: false),
                    CourseEndDate = table.Column<DateTime>(nullable: false),
                    CourseDuration = table.Column<string>(nullable: true),
                    HRMS_EmployeeFK = table.Column<int>(nullable: false),
                    CourseName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HRMS_EmployeeTraining", x => x.ID);
                    table.ForeignKey(
                        name: "FK_HRMS_EmployeeTraining_HRMS_Employee_HRMS_EmployeeFK",
                        column: x => x.HRMS_EmployeeFK,
                        principalTable: "HRMS_Employee",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "HRMS_EmployeeWeekendAssign",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    HRMS_EmployeeFK = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HRMS_EmployeeWeekendAssign", x => x.ID);
                    table.ForeignKey(
                        name: "FK_HRMS_EmployeeWeekendAssign_HRMS_Employee_HRMS_EmployeeFK",
                        column: x => x.HRMS_EmployeeFK,
                        principalTable: "HRMS_Employee",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "HRMS_LeaveApplication",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    HRMS_EmployeeFK = table.Column<int>(nullable: true),
                    ReplacementEmployeeId = table.Column<int>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    HRMS_LeaveFK = table.Column<int>(nullable: true),
                    From = table.Column<DateTime>(nullable: true),
                    To = table.Column<DateTime>(nullable: true),
                    TotalDays = table.Column<int>(nullable: false),
                    Purpose = table.Column<string>(nullable: true),
                    StayDuringLeave = table.Column<string>(nullable: true),
                    LeaveApplicationRemarks = table.Column<string>(nullable: true),
                    LeavePaidType = table.Column<int>(nullable: false),
                    LeaveStatus = table.Column<int>(nullable: false),
                    LeaveDeptStatus = table.Column<int>(nullable: false),
                    HRMS_EmployeeApprovedByFK = table.Column<int>(nullable: true),
                    HRMS_EmployeeManagerFK = table.Column<int>(nullable: true),
                    HRMS_EmployeeSectionInchargeFK = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HRMS_LeaveApplication", x => x.ID);
                    table.ForeignKey(
                        name: "FK_HRMS_LeaveApplication_HRMS_Employee_HRMS_EmployeeApprovedByFK",
                        column: x => x.HRMS_EmployeeApprovedByFK,
                        principalTable: "HRMS_Employee",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_HRMS_LeaveApplication_HRMS_Employee_HRMS_EmployeeFK",
                        column: x => x.HRMS_EmployeeFK,
                        principalTable: "HRMS_Employee",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_HRMS_LeaveApplication_HRMS_Employee_HRMS_EmployeeManagerFK",
                        column: x => x.HRMS_EmployeeManagerFK,
                        principalTable: "HRMS_Employee",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_HRMS_LeaveApplication_HRMS_Employee_HRMS_EmployeeSectionInchargeFK",
                        column: x => x.HRMS_EmployeeSectionInchargeFK,
                        principalTable: "HRMS_Employee",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_HRMS_LeaveApplication_HRMS_Leave_HRMS_LeaveFK",
                        column: x => x.HRMS_LeaveFK,
                        principalTable: "HRMS_Leave",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "HRMS_ShiftAssign",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    HRMS_ShiftFK = table.Column<int>(nullable: false),
                    HRMS_EmployeeFK = table.Column<int>(nullable: false),
                    AssignedDate = table.Column<DateTime>(nullable: false),
                    UID = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HRMS_ShiftAssign", x => x.ID);
                    table.ForeignKey(
                        name: "FK_HRMS_ShiftAssign_HRMS_Employee_HRMS_EmployeeFK",
                        column: x => x.HRMS_EmployeeFK,
                        principalTable: "HRMS_Employee",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_HRMS_ShiftAssign_HRMS_Shift_HRMS_ShiftFK",
                        column: x => x.HRMS_ShiftFK,
                        principalTable: "HRMS_Shift",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Payroll_BonusDetails",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Payroll_BonusMasterFk = table.Column<int>(nullable: false),
                    HRMS_EmployeeFK = table.Column<int>(nullable: false),
                    GrossSalary = table.Column<decimal>(nullable: false),
                    BasicSalary = table.Column<decimal>(nullable: false),
                    StampCharge = table.Column<decimal>(nullable: false),
                    JoiningDate = table.Column<DateTime>(nullable: false),
                    Tenure = table.Column<string>(nullable: true),
                    BonusRate = table.Column<decimal>(nullable: false),
                    BonusAmount = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Payroll_BonusDetails", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Payroll_BonusDetails_HRMS_Employee_HRMS_EmployeeFK",
                        column: x => x.HRMS_EmployeeFK,
                        principalTable: "HRMS_Employee",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Payroll_BonusDetails_Payroll_BonusMaster_Payroll_BonusMasterFk",
                        column: x => x.Payroll_BonusMasterFk,
                        principalTable: "Payroll_BonusMaster",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Payroll_EmployeePromotionalHistory",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    HRMS_EmployeeFK = table.Column<int>(nullable: false),
                    HRMS_SectionFK = table.Column<int>(nullable: true),
                    HRMS_DesignationFK = table.Column<int>(nullable: true),
                    Common_ProductionLineFK = table.Column<int>(nullable: true),
                    Grade = table.Column<string>(nullable: true),
                    PromotionType = table.Column<int>(nullable: false),
                    PromotionDate = table.Column<DateTime>(nullable: false),
                    PreviousPromotionDate = table.Column<DateTime>(nullable: false),
                    Payroll_EODRecordMasterFk = table.Column<int>(nullable: true),
                    PreviousPayroll_EODRecordMasterFk = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Payroll_EmployeePromotionalHistory", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Payroll_EmployeePromotionalHistory_Common_ProductionLine_Common_ProductionLineFK",
                        column: x => x.Common_ProductionLineFK,
                        principalTable: "Common_ProductionLine",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Payroll_EmployeePromotionalHistory_HRMS_Designation_HRMS_DesignationFK",
                        column: x => x.HRMS_DesignationFK,
                        principalTable: "HRMS_Designation",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Payroll_EmployeePromotionalHistory_HRMS_Employee_HRMS_EmployeeFK",
                        column: x => x.HRMS_EmployeeFK,
                        principalTable: "HRMS_Employee",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Payroll_EmployeePromotionalHistory_HRMS_Section_HRMS_SectionFK",
                        column: x => x.HRMS_SectionFK,
                        principalTable: "HRMS_Section",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Payroll_EmployeePromotionalHistory_Payroll_EODRecordMaster_Payroll_EODRecordMasterFk",
                        column: x => x.Payroll_EODRecordMasterFk,
                        principalTable: "Payroll_EODRecordMaster",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Payroll_EmployeePromotionalHistory_Payroll_EODRecordMaster_PreviousPayroll_EODRecordMasterFk",
                        column: x => x.PreviousPayroll_EODRecordMasterFk,
                        principalTable: "Payroll_EODRecordMaster",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Store_QCInformation",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    CID = table.Column<string>(nullable: true),
                    DeviceId = table.Column<string>(nullable: true),
                    Merchandising_StyleIdFK = table.Column<int>(nullable: true),
                    Common_ColorFK = table.Column<int>(nullable: true),
                    Common_SizeFK = table.Column<int>(nullable: true),
                    Common_ProductionLineFK = table.Column<int>(nullable: true),
                    Merchandising_StyleSetPackFK = table.Column<int>(nullable: true),
                    QCQuantity = table.Column<decimal>(nullable: false),
                    RejectQuantityOne = table.Column<int>(nullable: false),
                    RejectQuantityTwo = table.Column<int>(nullable: false),
                    RejectQuantityThree = table.Column<int>(nullable: false),
                    RejectQuantityFour = table.Column<int>(nullable: false),
                    RejectQuantityFive = table.Column<int>(nullable: false),
                    RejectQuantitySix = table.Column<int>(nullable: false),
                    RejectQuantityTotal = table.Column<int>(nullable: false),
                    EntryDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Store_QCInformation", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Store_QCInformation_Common_Color_Common_ColorFK",
                        column: x => x.Common_ColorFK,
                        principalTable: "Common_Color",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Store_QCInformation_Common_ProductionLine_Common_ProductionLineFK",
                        column: x => x.Common_ProductionLineFK,
                        principalTable: "Common_ProductionLine",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Store_QCInformation_Common_Size_Common_SizeFK",
                        column: x => x.Common_SizeFK,
                        principalTable: "Common_Size",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Store_QCInformation_Merchandising_Style_Merchandising_StyleIdFK",
                        column: x => x.Merchandising_StyleIdFK,
                        principalTable: "Merchandising_Style",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Store_QCInformation_Merchandising_StyleSetPack_Merchandising_StyleSetPackFK",
                        column: x => x.Merchandising_StyleSetPackFK,
                        principalTable: "Merchandising_StyleSetPack",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Merchandising_StyleShipmentRatio",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Merchandising_StyleShipmentScheduleFK = table.Column<int>(nullable: false),
                    Merchandising_StyleMeasurementFK = table.Column<int>(nullable: true),
                    Merchandising_StyleSetPackFK = table.Column<int>(nullable: true),
                    Quantity = table.Column<int>(nullable: false),
                    Ratio = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Merchandising_StyleShipmentRatio", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Merchandising_StyleShipmentRatio_Merchandising_StyleMeasurement_Merchandising_StyleMeasurementFK",
                        column: x => x.Merchandising_StyleMeasurementFK,
                        principalTable: "Merchandising_StyleMeasurement",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Merchandising_StyleShipmentRatio_Merchandising_StyleShipmentSchedule_Merchandising_StyleShipmentScheduleFK",
                        column: x => x.Merchandising_StyleShipmentScheduleFK,
                        principalTable: "Merchandising_StyleShipmentSchedule",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Plan_DraftOrderPlan",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Merchandising_StyleFK = table.Column<int>(nullable: false),
                    Merchandising_StyleShipmentScheduleFK = table.Column<int>(nullable: true),
                    Merchandising_StyleSetPackFK = table.Column<int>(nullable: true),
                    PlanQty = table.Column<decimal>(nullable: false),
                    ShipmentDate = table.Column<DateTime>(nullable: false),
                    SampleApprovedDate = table.Column<DateTime>(nullable: false),
                    FabricInDate = table.Column<DateTime>(nullable: false),
                    AccessoriesInDate = table.Column<DateTime>(nullable: false),
                    CuttingDate = table.Column<DateTime>(nullable: false),
                    SewingDate = table.Column<DateTime>(nullable: false),
                    ExtraRate = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    IsClosed = table.Column<bool>(nullable: false),
                    IsPaused = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Plan_DraftOrderPlan", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Plan_DraftOrderPlan_Merchandising_Style_Merchandising_StyleFK",
                        column: x => x.Merchandising_StyleFK,
                        principalTable: "Merchandising_Style",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Plan_DraftOrderPlan_Merchandising_StyleShipmentSchedule_Merchandising_StyleShipmentScheduleFK",
                        column: x => x.Merchandising_StyleShipmentScheduleFK,
                        principalTable: "Merchandising_StyleShipmentSchedule",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Prod_ReferencePlan",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Prod_ReferenceFK = table.Column<int>(nullable: false),
                    CID = table.Column<string>(nullable: true),
                    ProductionDate = table.Column<DateTime>(nullable: false),
                    Merchandising_StyleFK = table.Column<int>(nullable: false),
                    Merchandising_StyleSetPackFK = table.Column<int>(nullable: true),
                    Common_ColorFK = table.Column<int>(nullable: true),
                    ProductionLineFK = table.Column<int>(nullable: true),
                    Common_LineChiefFK = table.Column<int>(nullable: true),
                    Common_SuperVisorFK = table.Column<int>(nullable: true),
                    Plan_SMVMasterLayoutFK = table.Column<int>(nullable: true),
                    LineInputQuantity = table.Column<int>(nullable: false),
                    PlanQuantity = table.Column<int>(nullable: false),
                    ExtraCutting = table.Column<int>(nullable: false),
                    FabricLayer = table.Column<int>(nullable: false),
                    LayerWeight = table.Column<decimal>(nullable: false),
                    MarkerPiece = table.Column<int>(nullable: false),
                    MarkerLength = table.Column<decimal>(nullable: false),
                    MarkerWidth = table.Column<decimal>(nullable: false),
                    GSMORSMV = table.Column<decimal>(nullable: false),
                    MachineRunning = table.Column<int>(nullable: false),
                    WorkingHour = table.Column<int>(nullable: false),
                    PresentOperator = table.Column<int>(nullable: false),
                    PresentHelper = table.Column<int>(nullable: false),
                    SectionId = table.Column<int>(nullable: false),
                    InputMan = table.Column<int>(nullable: false),
                    SizeMan = table.Column<int>(nullable: false),
                    LineMan = table.Column<int>(nullable: false),
                    CuttingNo = table.Column<string>(nullable: true),
                    DayQty = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Prod_ReferencePlan", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Prod_ReferencePlan_Common_Color_Common_ColorFK",
                        column: x => x.Common_ColorFK,
                        principalTable: "Common_Color",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Prod_ReferencePlan_Common_LineChief_Common_LineChiefFK",
                        column: x => x.Common_LineChiefFK,
                        principalTable: "Common_LineChief",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Prod_ReferencePlan_Common_LineSuperVisor_Common_SuperVisorFK",
                        column: x => x.Common_SuperVisorFK,
                        principalTable: "Common_LineSuperVisor",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Prod_ReferencePlan_Merchandising_Style_Merchandising_StyleFK",
                        column: x => x.Merchandising_StyleFK,
                        principalTable: "Merchandising_Style",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Prod_ReferencePlan_Plan_SMVMasterLayout_Plan_SMVMasterLayoutFK",
                        column: x => x.Plan_SMVMasterLayoutFK,
                        principalTable: "Plan_SMVMasterLayout",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Prod_ReferencePlan_Prod_Reference_Prod_ReferenceFK",
                        column: x => x.Prod_ReferenceFK,
                        principalTable: "Prod_Reference",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Prod_ReferencePlan_Common_ProductionLine_ProductionLineFK",
                        column: x => x.ProductionLineFK,
                        principalTable: "Common_ProductionLine",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Procurement_PurchaseInvoiceSlave",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    InvoiceQuantity = table.Column<double>(nullable: false),
                    InvoicePrice = table.Column<double>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    Procurement_PurchaseInvoiceFK = table.Column<int>(nullable: false),
                    Procurement_PurchaseOrderSlaveID = table.Column<int>(nullable: true),
                    Store_StockInFK = table.Column<int>(nullable: true),
                    Common_RawItemFK = table.Column<int>(nullable: false),
                    Merchandising_StyleID = table.Column<int>(nullable: true),
                    Merchandising_StyleSlaveID = table.Column<int>(nullable: true),
                    Merchandising_BOFID = table.Column<int>(nullable: true),
                    StrChallanNo = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Procurement_PurchaseInvoiceSlave", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Procurement_PurchaseInvoiceSlave_Common_RawItem_Common_RawItemFK",
                        column: x => x.Common_RawItemFK,
                        principalTable: "Common_RawItem",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Procurement_PurchaseInvoiceSlave_Procurement_PurchaseInvoice_Procurement_PurchaseInvoiceFK",
                        column: x => x.Procurement_PurchaseInvoiceFK,
                        principalTable: "Procurement_PurchaseInvoice",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Procurement_PurchaseInvoiceSlave_Store_StockIn_Store_StockInFK",
                        column: x => x.Store_StockInFK,
                        principalTable: "Store_StockIn",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Merchandising_StyleSlave",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Merchandising_StyleFK = table.Column<int>(nullable: false),
                    Merchandising_CBSSlaveFK = table.Column<int>(nullable: true),
                    Common_RawItemFK = table.Column<int>(nullable: true),
                    Common_CurrencyFK = table.Column<int>(nullable: true),
                    ConRequiredQty = table.Column<decimal>(nullable: false),
                    GSM = table.Column<decimal>(nullable: false),
                    Common_ColorFK = table.Column<int>(nullable: true),
                    Consumption = table.Column<decimal>(type: "decimal(18,5)", nullable: false),
                    Tolerance = table.Column<decimal>(nullable: false),
                    RequiredQty = table.Column<decimal>(nullable: false),
                    UnitPrice = table.Column<decimal>(type: "decimal(18,5)", nullable: false),
                    TotalPrice = table.Column<decimal>(type: "decimal(18,5)", nullable: false),
                    IsFabric = table.Column<bool>(nullable: false),
                    IsLocked = table.Column<bool>(nullable: false),
                    SelfReferenceFK = table.Column<int>(nullable: true),
                    Common_SizeFk = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Merchandising_StyleSlave", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Merchandising_StyleSlave_Common_Currency_Common_CurrencyFK",
                        column: x => x.Common_CurrencyFK,
                        principalTable: "Common_Currency",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Merchandising_StyleSlave_Common_RawItem_Common_RawItemFK",
                        column: x => x.Common_RawItemFK,
                        principalTable: "Common_RawItem",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Merchandising_StyleSlave_Merchandising_CBSSlave_Merchandising_CBSSlaveFK",
                        column: x => x.Merchandising_CBSSlaveFK,
                        principalTable: "Merchandising_CBSSlave",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Merchandising_StyleSlave_Merchandising_Style_Merchandising_StyleFK",
                        column: x => x.Merchandising_StyleFK,
                        principalTable: "Merchandising_Style",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "HRMS_AttendanceHistoryLog",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    HRMS_AttendanceHistoryFK = table.Column<int>(nullable: true),
                    HRMS_EmployeeFK = table.Column<int>(nullable: true),
                    Date = table.Column<DateTime>(nullable: false),
                    InTime = table.Column<DateTime>(nullable: false),
                    OutTime = table.Column<DateTime>(nullable: false),
                    TotalTime = table.Column<int>(nullable: false),
                    Late = table.Column<int>(nullable: false),
                    OverTime = table.Column<int>(nullable: false),
                    AttendanceStatus = table.Column<int>(nullable: false),
                    CardNo = table.Column<string>(maxLength: 50, nullable: true),
                    HRMS_ShiftFK = table.Column<int>(nullable: false),
                    LeavePaidType = table.Column<int>(nullable: true),
                    EntryDate = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: false),
                    EntryByUserFK = table.Column<int>(nullable: true),
                    UpdateByUserFK = table.Column<int>(nullable: true),
                    PayableOverTime = table.Column<int>(nullable: true),
                    IsEdited = table.Column<bool>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HRMS_AttendanceHistoryLog", x => x.ID);
                    table.ForeignKey(
                        name: "FK_HRMS_AttendanceHistoryLog_HRMS_AttendanceHistory_HRMS_AttendanceHistoryFK",
                        column: x => x.HRMS_AttendanceHistoryFK,
                        principalTable: "HRMS_AttendanceHistory",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "HRMS_EmployeeWeekend",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    HRMS_EmployeeWeekendAssignFK = table.Column<int>(nullable: true),
                    WeekendDay = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_HRMS_EmployeeWeekend", x => x.ID);
                    table.ForeignKey(
                        name: "FK_HRMS_EmployeeWeekend_HRMS_EmployeeWeekendAssign_HRMS_EmployeeWeekendAssignFK",
                        column: x => x.HRMS_EmployeeWeekendAssignFK,
                        principalTable: "HRMS_EmployeeWeekendAssign",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Plan_DraftLinePlan",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Plan_DraftOrderPlanFK = table.Column<int>(nullable: false),
                    Plan_SMVDraftLayoutFK = table.Column<int>(nullable: true),
                    Common_ProductionLineFK = table.Column<int>(nullable: false),
                    StartDate = table.Column<DateTime>(nullable: false),
                    FinishDate = table.Column<DateTime>(nullable: false),
                    HourTargetPlan = table.Column<decimal>(nullable: false),
                    NoOfOperator = table.Column<int>(nullable: false),
                    WorkingHour = table.Column<decimal>(nullable: false),
                    TargetDays = table.Column<decimal>(nullable: false),
                    DayOutputQty = table.Column<decimal>(nullable: false),
                    TargetOutputQty = table.Column<decimal>(nullable: false),
                    LineHour = table.Column<decimal>(nullable: false),
                    RemainQty = table.Column<decimal>(nullable: false),
                    IsClosed = table.Column<bool>(nullable: false),
                    IsPaused = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Plan_DraftLinePlan", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Plan_DraftLinePlan_Common_ProductionLine_Common_ProductionLineFK",
                        column: x => x.Common_ProductionLineFK,
                        principalTable: "Common_ProductionLine",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Plan_DraftLinePlan_Plan_DraftOrderPlan_Plan_DraftOrderPlanFK",
                        column: x => x.Plan_DraftOrderPlanFK,
                        principalTable: "Plan_DraftOrderPlan",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Plan_DraftLinePlan_Plan_SMVDraftLayout_Plan_SMVDraftLayoutFK",
                        column: x => x.Plan_SMVDraftLayoutFK,
                        principalTable: "Plan_SMVDraftLayout",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Plan_MasterOrderPlan",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Plan_DraftOrderPlanFK = table.Column<int>(nullable: true),
                    Merchandising_StyleFK = table.Column<int>(nullable: false),
                    Merchandising_StyleSetPackFK = table.Column<int>(nullable: true),
                    PlanQty = table.Column<decimal>(nullable: false),
                    ShipmentDate = table.Column<DateTime>(nullable: false),
                    SampleApprovedDate = table.Column<DateTime>(nullable: false),
                    FabricInDate = table.Column<DateTime>(nullable: false),
                    AccessoriesInDate = table.Column<DateTime>(nullable: false),
                    CuttingDate = table.Column<DateTime>(nullable: false),
                    SewingDate = table.Column<DateTime>(nullable: false),
                    ExtraRate = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    IsClosed = table.Column<bool>(nullable: false),
                    IsPaused = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Plan_MasterOrderPlan", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Plan_MasterOrderPlan_Merchandising_Style_Merchandising_StyleFK",
                        column: x => x.Merchandising_StyleFK,
                        principalTable: "Merchandising_Style",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Plan_MasterOrderPlan_Plan_DraftOrderPlan_Plan_DraftOrderPlanFK",
                        column: x => x.Plan_DraftOrderPlanFK,
                        principalTable: "Plan_DraftOrderPlan",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Prod_ReferencePlanFollowup",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Prod_ReferencePlanFK = table.Column<int>(nullable: true),
                    Merchandising_StyleShipmentScheduleFK = table.Column<int>(nullable: true),
                    Merchandising_StyleShipmentRatioFK = table.Column<int>(nullable: true),
                    Quantity = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Prod_ReferencePlanFollowup", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Prod_ReferencePlanFollowup_Prod_ReferencePlan_Prod_ReferencePlanFK",
                        column: x => x.Prod_ReferencePlanFK,
                        principalTable: "Prod_ReferencePlan",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Procurement_PurchaseRequisitionSlave",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Procurement_PurchaseRequisitionFK = table.Column<int>(nullable: false),
                    RequiredDate = table.Column<DateTime>(nullable: false),
                    Common_RawItemFK = table.Column<int>(nullable: false),
                    Merchandising_StyleID = table.Column<int>(nullable: true),
                    Merchandising_StyleSlaveFK = table.Column<int>(nullable: true),
                    Merchandising_BOFFK = table.Column<int>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    RequisitionQuantity = table.Column<double>(nullable: false),
                    InspectionRequired = table.Column<bool>(nullable: false),
                    SupplierNames = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Procurement_PurchaseRequisitionSlave", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Procurement_PurchaseRequisitionSlave_Common_RawItem_Common_RawItemFK",
                        column: x => x.Common_RawItemFK,
                        principalTable: "Common_RawItem",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Procurement_PurchaseRequisitionSlave_Merchandising_BOF_Merchandising_BOFFK",
                        column: x => x.Merchandising_BOFFK,
                        principalTable: "Merchandising_BOF",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Procurement_PurchaseRequisitionSlave_Merchandising_StyleSlave_Merchandising_StyleSlaveFK",
                        column: x => x.Merchandising_StyleSlaveFK,
                        principalTable: "Merchandising_StyleSlave",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Procurement_PurchaseRequisitionSlave_Procurement_PurchaseRequisition_Procurement_PurchaseRequisitionFK",
                        column: x => x.Procurement_PurchaseRequisitionFK,
                        principalTable: "Procurement_PurchaseRequisition",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Plan_MasterLinePlan",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    PLan_MasterOrderPlanFK = table.Column<int>(nullable: false),
                    Plan_SMVMasterLayoutFK = table.Column<int>(nullable: true),
                    Common_ProductionLineFK = table.Column<int>(nullable: false),
                    StartDate = table.Column<DateTime>(nullable: false),
                    FinishDate = table.Column<DateTime>(nullable: false),
                    HourTargetPlan = table.Column<decimal>(nullable: false),
                    NoOfOperator = table.Column<int>(nullable: false),
                    WorkingHour = table.Column<decimal>(nullable: false),
                    TargetDays = table.Column<decimal>(nullable: false),
                    DayOutputQty = table.Column<decimal>(nullable: false),
                    TargetOutputQty = table.Column<decimal>(nullable: false),
                    LineHour = table.Column<decimal>(nullable: false),
                    RemainQty = table.Column<decimal>(nullable: false),
                    IsClosed = table.Column<bool>(nullable: false),
                    IsPaused = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Plan_MasterLinePlan", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Plan_MasterLinePlan_Common_ProductionLine_Common_ProductionLineFK",
                        column: x => x.Common_ProductionLineFK,
                        principalTable: "Common_ProductionLine",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Plan_MasterLinePlan_Plan_MasterOrderPlan_PLan_MasterOrderPlanFK",
                        column: x => x.PLan_MasterOrderPlanFK,
                        principalTable: "Plan_MasterOrderPlan",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Plan_MasterLinePlan_Plan_SMVMasterLayout_Plan_SMVMasterLayoutFK",
                        column: x => x.Plan_SMVMasterLayoutFK,
                        principalTable: "Plan_SMVMasterLayout",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Prod_PlanAchievment",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    AchiveDate = table.Column<DateTime>(nullable: false),
                    Merchandising_StyleFK = table.Column<int>(nullable: false),
                    ProductionLineFK = table.Column<int>(nullable: true),
                    MasterOrderPlanFk = table.Column<int>(nullable: true),
                    HourCapacity = table.Column<decimal>(nullable: false),
                    WorkingHour = table.Column<decimal>(nullable: false),
                    PlanAchievQty = table.Column<decimal>(nullable: false),
                    IsPlanedLine = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Prod_PlanAchievment", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Prod_PlanAchievment_Plan_MasterOrderPlan_MasterOrderPlanFk",
                        column: x => x.MasterOrderPlanFk,
                        principalTable: "Plan_MasterOrderPlan",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Prod_PlanAchievment_Merchandising_Style_Merchandising_StyleFK",
                        column: x => x.Merchandising_StyleFK,
                        principalTable: "Merchandising_Style",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Prod_PlanAchievment_Common_ProductionLine_ProductionLineFK",
                        column: x => x.ProductionLineFK,
                        principalTable: "Common_ProductionLine",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Prod_ReferencePlanFollowupDetails",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    Prod_ReferencePlanFollowupFK = table.Column<int>(nullable: false),
                    Quantity = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Prod_ReferencePlanFollowupDetails", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Prod_ReferencePlanFollowupDetails_Prod_ReferencePlanFollowup_Prod_ReferencePlanFollowupFK",
                        column: x => x.Prod_ReferencePlanFollowupFK,
                        principalTable: "Prod_ReferencePlanFollowup",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Procurement_PurchaseOrderSlave",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(nullable: false),
                    User = table.Column<string>(nullable: true),
                    Time = table.Column<DateTime>(nullable: false),
                    Remarks = table.Column<string>(nullable: true),
                    UserID = table.Column<int>(nullable: false),
                    PurchaseQuantity = table.Column<double>(nullable: false),
                    PurchasingPrice = table.Column<double>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    InspectionRequired = table.Column<bool>(nullable: false),
                    Procurement_PurchaseOrderFK = table.Column<int>(nullable: false),
                    Procurement_PurchaseRequisitionSlaveFK = table.Column<int>(nullable: true),
                    Common_RawItemFK = table.Column<int>(nullable: false),
                    Merchandising_StyleID = table.Column<int>(nullable: true),
                    Merchandising_StyleSlaveFK = table.Column<int>(nullable: true),
                    Merchandising_BOFFK = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Procurement_PurchaseOrderSlave", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Procurement_PurchaseOrderSlave_Common_RawItem_Common_RawItemFK",
                        column: x => x.Common_RawItemFK,
                        principalTable: "Common_RawItem",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Procurement_PurchaseOrderSlave_Merchandising_BOF_Merchandising_BOFFK",
                        column: x => x.Merchandising_BOFFK,
                        principalTable: "Merchandising_BOF",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Procurement_PurchaseOrderSlave_Merchandising_StyleSlave_Merchandising_StyleSlaveFK",
                        column: x => x.Merchandising_StyleSlaveFK,
                        principalTable: "Merchandising_StyleSlave",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Procurement_PurchaseOrderSlave_Procurement_PurchaseOrder_Procurement_PurchaseOrderFK",
                        column: x => x.Procurement_PurchaseOrderFK,
                        principalTable: "Procurement_PurchaseOrder",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Procurement_PurchaseOrderSlave_Procurement_PurchaseRequisitionSlave_Procurement_PurchaseRequisitionSlaveFK",
                        column: x => x.Procurement_PurchaseRequisitionSlaveFK,
                        principalTable: "Procurement_PurchaseRequisitionSlave",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Accounting_Chart1_Accounting_TypeFK",
                table: "Accounting_Chart1",
                column: "Accounting_TypeFK");

            migrationBuilder.CreateIndex(
                name: "IX_Accounting_Chart2_Accounting_Chart1FK",
                table: "Accounting_Chart2",
                column: "Accounting_Chart1FK");

            migrationBuilder.CreateIndex(
                name: "IX_Accounting_Head_Accounting_Chart2FK",
                table: "Accounting_Head",
                column: "Accounting_Chart2FK");

            migrationBuilder.CreateIndex(
                name: "IX_Accounting_Journal_Accounting_CostCenterFK",
                table: "Accounting_Journal",
                column: "Accounting_CostCenterFK");

            migrationBuilder.CreateIndex(
                name: "IX_Accounting_JournalSlave_Accounting_HeadFK",
                table: "Accounting_JournalSlave",
                column: "Accounting_HeadFK");

            migrationBuilder.CreateIndex(
                name: "IX_Accounting_JournalSlave_Accounting_JournalFK",
                table: "Accounting_JournalSlave",
                column: "Accounting_JournalFK");

            migrationBuilder.CreateIndex(
                name: "IX_Accounting_Transaction_Accounting_CostCenterFK",
                table: "Accounting_Transaction",
                column: "Accounting_CostCenterFK");

            migrationBuilder.CreateIndex(
                name: "IX_Common_CountryPort_Common_CountryFK",
                table: "Common_CountryPort",
                column: "Common_CountryFK");

            migrationBuilder.CreateIndex(
                name: "IX_Common_CountrySize_Common_CountryFK",
                table: "Common_CountrySize",
                column: "Common_CountryFK");

            migrationBuilder.CreateIndex(
                name: "IX_Common_CountrySize_Common_SizeFK",
                table: "Common_CountrySize",
                column: "Common_SizeFK");

            migrationBuilder.CreateIndex(
                name: "IX_Common_CurrencyChangesLog_Common_CurrencyFK",
                table: "Common_CurrencyChangesLog",
                column: "Common_CurrencyFK");

            migrationBuilder.CreateIndex(
                name: "IX_Common_FinishItem_Common_FinishSubCategoryFK",
                table: "Common_FinishItem",
                column: "Common_FinishSubCategoryFK");

            migrationBuilder.CreateIndex(
                name: "IX_Common_FinishItem_Common_UnitFK",
                table: "Common_FinishItem",
                column: "Common_UnitFK");

            migrationBuilder.CreateIndex(
                name: "IX_Common_FinishSubCategory_Common_FinishCategoryFK",
                table: "Common_FinishSubCategory",
                column: "Common_FinishCategoryFK");

            migrationBuilder.CreateIndex(
                name: "IX_Common_LineChiefLine_Common_LineChiefFk",
                table: "Common_LineChiefLine",
                column: "Common_LineChiefFk");

            migrationBuilder.CreateIndex(
                name: "IX_Common_LineChiefLine_Common_LineSuperVisorFk",
                table: "Common_LineChiefLine",
                column: "Common_LineSuperVisorFk");

            migrationBuilder.CreateIndex(
                name: "IX_Common_LineChiefLine_Common_ProductionLineFk",
                table: "Common_LineChiefLine",
                column: "Common_ProductionLineFk");

            migrationBuilder.CreateIndex(
                name: "IX_Common_ProductionFloor_Common_ProductionUnitFK",
                table: "Common_ProductionFloor",
                column: "Common_ProductionUnitFK");

            migrationBuilder.CreateIndex(
                name: "IX_Common_ProductionLine_Common_SectionLineID",
                table: "Common_ProductionLine",
                column: "Common_SectionLineID");

            migrationBuilder.CreateIndex(
                name: "IX_Common_RawItem_Common_RawSubCategoryFK",
                table: "Common_RawItem",
                column: "Common_RawSubCategoryFK");

            migrationBuilder.CreateIndex(
                name: "IX_Common_RawItem_Common_UnitFK",
                table: "Common_RawItem",
                column: "Common_UnitFK");

            migrationBuilder.CreateIndex(
                name: "IX_Common_RawSubCategory_Common_RawCategoryFK",
                table: "Common_RawSubCategory",
                column: "Common_RawCategoryFK");

            migrationBuilder.CreateIndex(
                name: "IX_Common_SectionLine_Common_ProductionFloorFK",
                table: "Common_SectionLine",
                column: "Common_ProductionFloorFK");

            migrationBuilder.CreateIndex(
                name: "IX_Common_Supplier_Common_CountryFK",
                table: "Common_Supplier",
                column: "Common_CountryFK");

            migrationBuilder.CreateIndex(
                name: "IX_HRMS_AttendanceHistory_HRMS_EmployeeFK",
                table: "HRMS_AttendanceHistory",
                column: "HRMS_EmployeeFK");

            migrationBuilder.CreateIndex(
                name: "IX_HRMS_AttendanceHistory_HRMS_ShiftFK",
                table: "HRMS_AttendanceHistory",
                column: "HRMS_ShiftFK");

            migrationBuilder.CreateIndex(
                name: "IX_HRMS_AttendanceHistory_UpdateByUserFK",
                table: "HRMS_AttendanceHistory",
                column: "UpdateByUserFK");

            migrationBuilder.CreateIndex(
                name: "IX_HRMS_AttendanceHistoryLog_HRMS_AttendanceHistoryFK",
                table: "HRMS_AttendanceHistoryLog",
                column: "HRMS_AttendanceHistoryFK");

            migrationBuilder.CreateIndex(
                name: "IX_HRMS_Designation_HRMS_CompanyHierarchyLabelFK",
                table: "HRMS_Designation",
                column: "HRMS_CompanyHierarchyLabelFK");

            migrationBuilder.CreateIndex(
                name: "IX_HRMS_Designation_User_DepartmentFK",
                table: "HRMS_Designation",
                column: "User_DepartmentFK");

            migrationBuilder.CreateIndex(
                name: "IX_HRMS_District_Common_CountryFK",
                table: "HRMS_District",
                column: "Common_CountryFK");

            migrationBuilder.CreateIndex(
                name: "IX_HRMS_EarnLeave_HRMS_BusinessUnitFK",
                table: "HRMS_EarnLeave",
                column: "HRMS_BusinessUnitFK");

            migrationBuilder.CreateIndex(
                name: "IX_HRMS_EarnLeave_HRMS_EmployeeFK",
                table: "HRMS_EarnLeave",
                column: "HRMS_EmployeeFK");

            migrationBuilder.CreateIndex(
                name: "IX_HRMS_Employee_Common_CountryFK",
                table: "HRMS_Employee",
                column: "Common_CountryFK");

            migrationBuilder.CreateIndex(
                name: "IX_HRMS_Employee_Common_ProductionLineFK",
                table: "HRMS_Employee",
                column: "Common_ProductionLineFK");

            migrationBuilder.CreateIndex(
                name: "IX_HRMS_Employee_HRMS_DesignationFK",
                table: "HRMS_Employee",
                column: "HRMS_DesignationFK");

            migrationBuilder.CreateIndex(
                name: "IX_HRMS_Employee_HRMS_PostOfficeFK",
                table: "HRMS_Employee",
                column: "HRMS_PostOfficeFK");

            migrationBuilder.CreateIndex(
                name: "IX_HRMS_Employee_HRMS_SectionFK",
                table: "HRMS_Employee",
                column: "HRMS_SectionFK");

            migrationBuilder.CreateIndex(
                name: "IX_HRMS_Employee_User_DepartmentFK",
                table: "HRMS_Employee",
                column: "User_DepartmentFK");

            migrationBuilder.CreateIndex(
                name: "IX_HRMS_EmployeeDependent_HRMS_EmployeeFK",
                table: "HRMS_EmployeeDependent",
                column: "HRMS_EmployeeFK");

            migrationBuilder.CreateIndex(
                name: "IX_HRMS_EmployeeEarnLeaveCalculation_HRMS_EmployeeFK",
                table: "HRMS_EmployeeEarnLeaveCalculation",
                column: "HRMS_EmployeeFK");

            migrationBuilder.CreateIndex(
                name: "IX_HRMS_EmployeeEducation_HRMS_EducationalDegreeFK",
                table: "HRMS_EmployeeEducation",
                column: "HRMS_EducationalDegreeFK");

            migrationBuilder.CreateIndex(
                name: "IX_HRMS_EmployeeEducation_HRMS_EmployeeFK",
                table: "HRMS_EmployeeEducation",
                column: "HRMS_EmployeeFK");

            migrationBuilder.CreateIndex(
                name: "IX_HRMS_EmployeeExperience_HRMS_EmployeeFK",
                table: "HRMS_EmployeeExperience",
                column: "HRMS_EmployeeFK");

            migrationBuilder.CreateIndex(
                name: "IX_HRMS_EmployeeImage_HRMS_EmployeeFK",
                table: "HRMS_EmployeeImage",
                column: "HRMS_EmployeeFK");

            migrationBuilder.CreateIndex(
                name: "IX_HRMS_EmployeeLeaveAssign_HRMS_EmployeeFK",
                table: "HRMS_EmployeeLeaveAssign",
                column: "HRMS_EmployeeFK");

            migrationBuilder.CreateIndex(
                name: "IX_HRMS_EmployeeLeaveAssign_HRMS_LeaveFK",
                table: "HRMS_EmployeeLeaveAssign",
                column: "HRMS_LeaveFK");

            migrationBuilder.CreateIndex(
                name: "IX_HRMS_EmployeeNominee_HRMS_EmployeeFK",
                table: "HRMS_EmployeeNominee",
                column: "HRMS_EmployeeFK");

            migrationBuilder.CreateIndex(
                name: "IX_HRMS_EmployeeReference_HRMS_EmployeeFK",
                table: "HRMS_EmployeeReference",
                column: "HRMS_EmployeeFK");

            migrationBuilder.CreateIndex(
                name: "IX_HRMS_EmployeeServiceBenifit_HRMS_EmployeeFK",
                table: "HRMS_EmployeeServiceBenifit",
                column: "HRMS_EmployeeFK");

            migrationBuilder.CreateIndex(
                name: "IX_HRMS_EmployeeSkill_HRMS_EmployeeFK",
                table: "HRMS_EmployeeSkill",
                column: "HRMS_EmployeeFK");

            migrationBuilder.CreateIndex(
                name: "IX_HRMS_EmployeeTraining_HRMS_EmployeeFK",
                table: "HRMS_EmployeeTraining",
                column: "HRMS_EmployeeFK");

            migrationBuilder.CreateIndex(
                name: "IX_HRMS_EmployeeWeekend_HRMS_EmployeeWeekendAssignFK",
                table: "HRMS_EmployeeWeekend",
                column: "HRMS_EmployeeWeekendAssignFK");

            migrationBuilder.CreateIndex(
                name: "IX_HRMS_EmployeeWeekendAssign_HRMS_EmployeeFK",
                table: "HRMS_EmployeeWeekendAssign",
                column: "HRMS_EmployeeFK");

            migrationBuilder.CreateIndex(
                name: "IX_HRMS_JobDescription_HRMS_DesignationFK",
                table: "HRMS_JobDescription",
                column: "HRMS_DesignationFK");

            migrationBuilder.CreateIndex(
                name: "IX_HRMS_JobDescription_User_DepartmentFK",
                table: "HRMS_JobDescription",
                column: "User_DepartmentFK");

            migrationBuilder.CreateIndex(
                name: "IX_HRMS_LeaveApplication_HRMS_EmployeeApprovedByFK",
                table: "HRMS_LeaveApplication",
                column: "HRMS_EmployeeApprovedByFK");

            migrationBuilder.CreateIndex(
                name: "IX_HRMS_LeaveApplication_HRMS_EmployeeFK",
                table: "HRMS_LeaveApplication",
                column: "HRMS_EmployeeFK");

            migrationBuilder.CreateIndex(
                name: "IX_HRMS_LeaveApplication_HRMS_EmployeeManagerFK",
                table: "HRMS_LeaveApplication",
                column: "HRMS_EmployeeManagerFK");

            migrationBuilder.CreateIndex(
                name: "IX_HRMS_LeaveApplication_HRMS_EmployeeSectionInchargeFK",
                table: "HRMS_LeaveApplication",
                column: "HRMS_EmployeeSectionInchargeFK");

            migrationBuilder.CreateIndex(
                name: "IX_HRMS_LeaveApplication_HRMS_LeaveFK",
                table: "HRMS_LeaveApplication",
                column: "HRMS_LeaveFK");

            migrationBuilder.CreateIndex(
                name: "IX_HRMS_PoliceStation_Common_CountryFK",
                table: "HRMS_PoliceStation",
                column: "Common_CountryFK");

            migrationBuilder.CreateIndex(
                name: "IX_HRMS_PoliceStation_HRMS_DistrictFK",
                table: "HRMS_PoliceStation",
                column: "HRMS_DistrictFK");

            migrationBuilder.CreateIndex(
                name: "IX_HRMS_PostOffice_Common_CountryFK",
                table: "HRMS_PostOffice",
                column: "Common_CountryFK");

            migrationBuilder.CreateIndex(
                name: "IX_HRMS_PostOffice_HRMS_DistrictFK",
                table: "HRMS_PostOffice",
                column: "HRMS_DistrictFK");

            migrationBuilder.CreateIndex(
                name: "IX_HRMS_PostOffice_HRMS_PoliceStationFK",
                table: "HRMS_PostOffice",
                column: "HRMS_PoliceStationFK");

            migrationBuilder.CreateIndex(
                name: "IX_HRMS_Section_HRMS_UnitFK",
                table: "HRMS_Section",
                column: "HRMS_UnitFK");

            migrationBuilder.CreateIndex(
                name: "IX_HRMS_ShiftAssign_HRMS_EmployeeFK",
                table: "HRMS_ShiftAssign",
                column: "HRMS_EmployeeFK");

            migrationBuilder.CreateIndex(
                name: "IX_HRMS_ShiftAssign_HRMS_ShiftFK",
                table: "HRMS_ShiftAssign",
                column: "HRMS_ShiftFK");

            migrationBuilder.CreateIndex(
                name: "IX_HRMS_Unit_HRMS_BusinessUnitFK",
                table: "HRMS_Unit",
                column: "HRMS_BusinessUnitFK");

            migrationBuilder.CreateIndex(
                name: "IX_Merchandising_BOF_Common_RawItemFK",
                table: "Merchandising_BOF",
                column: "Common_RawItemFK");

            migrationBuilder.CreateIndex(
                name: "IX_Merchandising_BOF_Merchandising_StyleFK",
                table: "Merchandising_BOF",
                column: "Merchandising_StyleFK");

            migrationBuilder.CreateIndex(
                name: "IX_Merchandising_BuyerOrder_Common_BuyerFK",
                table: "Merchandising_BuyerOrder",
                column: "Common_BuyerFK");

            migrationBuilder.CreateIndex(
                name: "IX_Merchandising_CBS_Merchandising_CBSStyleFK",
                table: "Merchandising_CBS",
                column: "Merchandising_CBSStyleFK");

            migrationBuilder.CreateIndex(
                name: "IX_Merchandising_CBSSlave_Common_CurrencyFK",
                table: "Merchandising_CBSSlave",
                column: "Common_CurrencyFK");

            migrationBuilder.CreateIndex(
                name: "IX_Merchandising_CBSSlave_Common_RawItemFK",
                table: "Merchandising_CBSSlave",
                column: "Common_RawItemFK");

            migrationBuilder.CreateIndex(
                name: "IX_Merchandising_CBSSlave_Merchandising_CBSFK",
                table: "Merchandising_CBSSlave",
                column: "Merchandising_CBSFK");

            migrationBuilder.CreateIndex(
                name: "IX_Merchandising_CBSStyle_Common_FinishItemFK",
                table: "Merchandising_CBSStyle",
                column: "Common_FinishItemFK");

            migrationBuilder.CreateIndex(
                name: "IX_Merchandising_Style_Common_FinishItemFK",
                table: "Merchandising_Style",
                column: "Common_FinishItemFK");

            migrationBuilder.CreateIndex(
                name: "IX_Merchandising_Style_Common_RawItemID",
                table: "Merchandising_Style",
                column: "Common_RawItemID");

            migrationBuilder.CreateIndex(
                name: "IX_Merchandising_Style_Merchandising_BuyerOrderFK",
                table: "Merchandising_Style",
                column: "Merchandising_BuyerOrderFK");

            migrationBuilder.CreateIndex(
                name: "IX_Merchandising_StyleMeasurement_Common_ColorFK",
                table: "Merchandising_StyleMeasurement",
                column: "Common_ColorFK");

            migrationBuilder.CreateIndex(
                name: "IX_Merchandising_StyleMeasurement_Common_SizeFK",
                table: "Merchandising_StyleMeasurement",
                column: "Common_SizeFK");

            migrationBuilder.CreateIndex(
                name: "IX_Merchandising_StyleMeasurement_Merchandising_StyleFK",
                table: "Merchandising_StyleMeasurement",
                column: "Merchandising_StyleFK");

            migrationBuilder.CreateIndex(
                name: "IX_Merchandising_StyleSetPack_Merchandising_StyleFK",
                table: "Merchandising_StyleSetPack",
                column: "Merchandising_StyleFK");

            migrationBuilder.CreateIndex(
                name: "IX_Merchandising_StyleShipmentRatio_Merchandising_StyleMeasurementFK",
                table: "Merchandising_StyleShipmentRatio",
                column: "Merchandising_StyleMeasurementFK");

            migrationBuilder.CreateIndex(
                name: "IX_Merchandising_StyleShipmentRatio_Merchandising_StyleShipmentScheduleFK",
                table: "Merchandising_StyleShipmentRatio",
                column: "Merchandising_StyleShipmentScheduleFK");

            migrationBuilder.CreateIndex(
                name: "IX_Merchandising_StyleShipmentSchedule_Common_CountryFK",
                table: "Merchandising_StyleShipmentSchedule",
                column: "Common_CountryFK");

            migrationBuilder.CreateIndex(
                name: "IX_Merchandising_StyleShipmentSchedule_Merchandising_StyleFK",
                table: "Merchandising_StyleShipmentSchedule",
                column: "Merchandising_StyleFK");

            migrationBuilder.CreateIndex(
                name: "IX_Merchandising_StyleSlave_Common_CurrencyFK",
                table: "Merchandising_StyleSlave",
                column: "Common_CurrencyFK");

            migrationBuilder.CreateIndex(
                name: "IX_Merchandising_StyleSlave_Common_RawItemFK",
                table: "Merchandising_StyleSlave",
                column: "Common_RawItemFK");

            migrationBuilder.CreateIndex(
                name: "IX_Merchandising_StyleSlave_Merchandising_CBSSlaveFK",
                table: "Merchandising_StyleSlave",
                column: "Merchandising_CBSSlaveFK");

            migrationBuilder.CreateIndex(
                name: "IX_Merchandising_StyleSlave_Merchandising_StyleFK",
                table: "Merchandising_StyleSlave",
                column: "Merchandising_StyleFK");

            migrationBuilder.CreateIndex(
                name: "IX_Payroll_BonusDetails_HRMS_EmployeeFK",
                table: "Payroll_BonusDetails",
                column: "HRMS_EmployeeFK");

            migrationBuilder.CreateIndex(
                name: "IX_Payroll_BonusDetails_Payroll_BonusMasterFk",
                table: "Payroll_BonusDetails",
                column: "Payroll_BonusMasterFk");

            migrationBuilder.CreateIndex(
                name: "IX_Payroll_EmployeePromotionalHistory_Common_ProductionLineFK",
                table: "Payroll_EmployeePromotionalHistory",
                column: "Common_ProductionLineFK");

            migrationBuilder.CreateIndex(
                name: "IX_Payroll_EmployeePromotionalHistory_HRMS_DesignationFK",
                table: "Payroll_EmployeePromotionalHistory",
                column: "HRMS_DesignationFK");

            migrationBuilder.CreateIndex(
                name: "IX_Payroll_EmployeePromotionalHistory_HRMS_EmployeeFK",
                table: "Payroll_EmployeePromotionalHistory",
                column: "HRMS_EmployeeFK");

            migrationBuilder.CreateIndex(
                name: "IX_Payroll_EmployeePromotionalHistory_HRMS_SectionFK",
                table: "Payroll_EmployeePromotionalHistory",
                column: "HRMS_SectionFK");

            migrationBuilder.CreateIndex(
                name: "IX_Payroll_EmployeePromotionalHistory_Payroll_EODRecordMasterFk",
                table: "Payroll_EmployeePromotionalHistory",
                column: "Payroll_EODRecordMasterFk");

            migrationBuilder.CreateIndex(
                name: "IX_Payroll_EmployeePromotionalHistory_PreviousPayroll_EODRecordMasterFk",
                table: "Payroll_EmployeePromotionalHistory",
                column: "PreviousPayroll_EODRecordMasterFk");

            migrationBuilder.CreateIndex(
                name: "IX_Payroll_EODRecord_Payroll_EODRecordMasterFk",
                table: "Payroll_EODRecord",
                column: "Payroll_EODRecordMasterFk");

            migrationBuilder.CreateIndex(
                name: "IX_Payroll_EODRecordMaster_SalaryStructureFk",
                table: "Payroll_EODRecordMaster",
                column: "SalaryStructureFk");

            migrationBuilder.CreateIndex(
                name: "IX_Payroll_PayrollMaster_HRMS_BusinessUnitFK",
                table: "Payroll_PayrollMaster",
                column: "HRMS_BusinessUnitFK");

            migrationBuilder.CreateIndex(
                name: "IX_Plan_DraftLinePlan_Common_ProductionLineFK",
                table: "Plan_DraftLinePlan",
                column: "Common_ProductionLineFK");

            migrationBuilder.CreateIndex(
                name: "IX_Plan_DraftLinePlan_Plan_DraftOrderPlanFK",
                table: "Plan_DraftLinePlan",
                column: "Plan_DraftOrderPlanFK");

            migrationBuilder.CreateIndex(
                name: "IX_Plan_DraftLinePlan_Plan_SMVDraftLayoutFK",
                table: "Plan_DraftLinePlan",
                column: "Plan_SMVDraftLayoutFK");

            migrationBuilder.CreateIndex(
                name: "IX_Plan_DraftOrderPlan_Merchandising_StyleFK",
                table: "Plan_DraftOrderPlan",
                column: "Merchandising_StyleFK");

            migrationBuilder.CreateIndex(
                name: "IX_Plan_DraftOrderPlan_Merchandising_StyleShipmentScheduleFK",
                table: "Plan_DraftOrderPlan",
                column: "Merchandising_StyleShipmentScheduleFK");

            migrationBuilder.CreateIndex(
                name: "IX_Plan_MasterLinePlan_Common_ProductionLineFK",
                table: "Plan_MasterLinePlan",
                column: "Common_ProductionLineFK");

            migrationBuilder.CreateIndex(
                name: "IX_Plan_MasterLinePlan_PLan_MasterOrderPlanFK",
                table: "Plan_MasterLinePlan",
                column: "PLan_MasterOrderPlanFK");

            migrationBuilder.CreateIndex(
                name: "IX_Plan_MasterLinePlan_Plan_SMVMasterLayoutFK",
                table: "Plan_MasterLinePlan",
                column: "Plan_SMVMasterLayoutFK");

            migrationBuilder.CreateIndex(
                name: "IX_Plan_MasterOrderPlan_Merchandising_StyleFK",
                table: "Plan_MasterOrderPlan",
                column: "Merchandising_StyleFK");

            migrationBuilder.CreateIndex(
                name: "IX_Plan_MasterOrderPlan_Plan_DraftOrderPlanFK",
                table: "Plan_MasterOrderPlan",
                column: "Plan_DraftOrderPlanFK");

            migrationBuilder.CreateIndex(
                name: "IX_Plan_OrderActionStep_Plan_OrderActionFK",
                table: "Plan_OrderActionStep",
                column: "Plan_OrderActionFK");

            migrationBuilder.CreateIndex(
                name: "IX_Plan_OrderProcess_Merchandising_StyleFK",
                table: "Plan_OrderProcess",
                column: "Merchandising_StyleFK");

            migrationBuilder.CreateIndex(
                name: "IX_Plan_OrderProcess_Plan_OrderActionStepFK",
                table: "Plan_OrderProcess",
                column: "Plan_OrderActionStepFK");

            migrationBuilder.CreateIndex(
                name: "IX_Plan_SMVMasterLayout_Merchandising_StyleFK",
                table: "Plan_SMVMasterLayout",
                column: "Merchandising_StyleFK");

            migrationBuilder.CreateIndex(
                name: "IX_Plan_SMVMasterLayout_PLan_SMVDraftLayoutFK",
                table: "Plan_SMVMasterLayout",
                column: "PLan_SMVDraftLayoutFK");

            migrationBuilder.CreateIndex(
                name: "IX_Procurement_PurchaseInvoice_ClosedByUserFK",
                table: "Procurement_PurchaseInvoice",
                column: "ClosedByUserFK");

            migrationBuilder.CreateIndex(
                name: "IX_Procurement_PurchaseInvoice_Common_SupplierFK",
                table: "Procurement_PurchaseInvoice",
                column: "Common_SupplierFK");

            migrationBuilder.CreateIndex(
                name: "IX_Procurement_PurchaseInvoiceSlave_Common_RawItemFK",
                table: "Procurement_PurchaseInvoiceSlave",
                column: "Common_RawItemFK");

            migrationBuilder.CreateIndex(
                name: "IX_Procurement_PurchaseInvoiceSlave_Procurement_PurchaseInvoiceFK",
                table: "Procurement_PurchaseInvoiceSlave",
                column: "Procurement_PurchaseInvoiceFK");

            migrationBuilder.CreateIndex(
                name: "IX_Procurement_PurchaseInvoiceSlave_Store_StockInFK",
                table: "Procurement_PurchaseInvoiceSlave",
                column: "Store_StockInFK");

            migrationBuilder.CreateIndex(
                name: "IX_Procurement_PurchaseOrder_ClosedByUserFK",
                table: "Procurement_PurchaseOrder",
                column: "ClosedByUserFK");

            migrationBuilder.CreateIndex(
                name: "IX_Procurement_PurchaseOrder_Common_CurrencyFK",
                table: "Procurement_PurchaseOrder",
                column: "Common_CurrencyFK");

            migrationBuilder.CreateIndex(
                name: "IX_Procurement_PurchaseOrder_Common_SupplierFK",
                table: "Procurement_PurchaseOrder",
                column: "Common_SupplierFK");

            migrationBuilder.CreateIndex(
                name: "IX_Procurement_PurchaseOrderSlave_Common_RawItemFK",
                table: "Procurement_PurchaseOrderSlave",
                column: "Common_RawItemFK");

            migrationBuilder.CreateIndex(
                name: "IX_Procurement_PurchaseOrderSlave_Merchandising_BOFFK",
                table: "Procurement_PurchaseOrderSlave",
                column: "Merchandising_BOFFK");

            migrationBuilder.CreateIndex(
                name: "IX_Procurement_PurchaseOrderSlave_Merchandising_StyleSlaveFK",
                table: "Procurement_PurchaseOrderSlave",
                column: "Merchandising_StyleSlaveFK");

            migrationBuilder.CreateIndex(
                name: "IX_Procurement_PurchaseOrderSlave_Procurement_PurchaseOrderFK",
                table: "Procurement_PurchaseOrderSlave",
                column: "Procurement_PurchaseOrderFK");

            migrationBuilder.CreateIndex(
                name: "IX_Procurement_PurchaseOrderSlave_Procurement_PurchaseRequisitionSlaveFK",
                table: "Procurement_PurchaseOrderSlave",
                column: "Procurement_PurchaseRequisitionSlaveFK");

            migrationBuilder.CreateIndex(
                name: "IX_Procurement_PurchaseRequisition_ClosedByUserFK",
                table: "Procurement_PurchaseRequisition",
                column: "ClosedByUserFK");

            migrationBuilder.CreateIndex(
                name: "IX_Procurement_PurchaseRequisitionSlave_Common_RawItemFK",
                table: "Procurement_PurchaseRequisitionSlave",
                column: "Common_RawItemFK");

            migrationBuilder.CreateIndex(
                name: "IX_Procurement_PurchaseRequisitionSlave_Merchandising_BOFFK",
                table: "Procurement_PurchaseRequisitionSlave",
                column: "Merchandising_BOFFK");

            migrationBuilder.CreateIndex(
                name: "IX_Procurement_PurchaseRequisitionSlave_Merchandising_StyleSlaveFK",
                table: "Procurement_PurchaseRequisitionSlave",
                column: "Merchandising_StyleSlaveFK");

            migrationBuilder.CreateIndex(
                name: "IX_Procurement_PurchaseRequisitionSlave_Procurement_PurchaseRequisitionFK",
                table: "Procurement_PurchaseRequisitionSlave",
                column: "Procurement_PurchaseRequisitionFK");

            migrationBuilder.CreateIndex(
                name: "IX_Prod_PlanAchievment_MasterOrderPlanFk",
                table: "Prod_PlanAchievment",
                column: "MasterOrderPlanFk");

            migrationBuilder.CreateIndex(
                name: "IX_Prod_PlanAchievment_Merchandising_StyleFK",
                table: "Prod_PlanAchievment",
                column: "Merchandising_StyleFK");

            migrationBuilder.CreateIndex(
                name: "IX_Prod_PlanAchievment_ProductionLineFK",
                table: "Prod_PlanAchievment",
                column: "ProductionLineFK");

            migrationBuilder.CreateIndex(
                name: "IX_Prod_ReferencePlan_Common_ColorFK",
                table: "Prod_ReferencePlan",
                column: "Common_ColorFK");

            migrationBuilder.CreateIndex(
                name: "IX_Prod_ReferencePlan_Common_LineChiefFK",
                table: "Prod_ReferencePlan",
                column: "Common_LineChiefFK");

            migrationBuilder.CreateIndex(
                name: "IX_Prod_ReferencePlan_Common_SuperVisorFK",
                table: "Prod_ReferencePlan",
                column: "Common_SuperVisorFK");

            migrationBuilder.CreateIndex(
                name: "IX_Prod_ReferencePlan_Merchandising_StyleFK",
                table: "Prod_ReferencePlan",
                column: "Merchandising_StyleFK");

            migrationBuilder.CreateIndex(
                name: "IX_Prod_ReferencePlan_Plan_SMVMasterLayoutFK",
                table: "Prod_ReferencePlan",
                column: "Plan_SMVMasterLayoutFK");

            migrationBuilder.CreateIndex(
                name: "IX_Prod_ReferencePlan_Prod_ReferenceFK",
                table: "Prod_ReferencePlan",
                column: "Prod_ReferenceFK");

            migrationBuilder.CreateIndex(
                name: "IX_Prod_ReferencePlan_ProductionLineFK",
                table: "Prod_ReferencePlan",
                column: "ProductionLineFK");

            migrationBuilder.CreateIndex(
                name: "IX_Prod_ReferencePlanFollowup_Prod_ReferencePlanFK",
                table: "Prod_ReferencePlanFollowup",
                column: "Prod_ReferencePlanFK");

            migrationBuilder.CreateIndex(
                name: "IX_Prod_ReferencePlanFollowupDetails_Prod_ReferencePlanFollowupFK",
                table: "Prod_ReferencePlanFollowupDetails",
                column: "Prod_ReferencePlanFollowupFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_Bin_Store_GeneralFK",
                table: "Store_Bin",
                column: "Store_GeneralFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_General_HRMS_BusinessUnitFK",
                table: "Store_General",
                column: "HRMS_BusinessUnitFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_General_HRMS_UnitFK",
                table: "Store_General",
                column: "HRMS_UnitFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_General_User_DepartmentFK",
                table: "Store_General",
                column: "User_DepartmentFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_QCInformation_Common_ColorFK",
                table: "Store_QCInformation",
                column: "Common_ColorFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_QCInformation_Common_ProductionLineFK",
                table: "Store_QCInformation",
                column: "Common_ProductionLineFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_QCInformation_Common_SizeFK",
                table: "Store_QCInformation",
                column: "Common_SizeFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_QCInformation_Merchandising_StyleIdFK",
                table: "Store_QCInformation",
                column: "Merchandising_StyleIdFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_QCInformation_Merchandising_StyleSetPackFK",
                table: "Store_QCInformation",
                column: "Merchandising_StyleSetPackFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_Requisition_Store_GeneralInFK",
                table: "Store_Requisition",
                column: "Store_GeneralInFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_Requisition_Store_GeneralOutFK",
                table: "Store_Requisition",
                column: "Store_GeneralOutFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_Requisition_User_UserFK",
                table: "Store_Requisition",
                column: "User_UserFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_RequisitionSlave_Common_ColorFK",
                table: "Store_RequisitionSlave",
                column: "Common_ColorFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_RequisitionSlave_Common_RawItemFK",
                table: "Store_RequisitionSlave",
                column: "Common_RawItemFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_RequisitionSlave_Store_RequisitionFK",
                table: "Store_RequisitionSlave",
                column: "Store_RequisitionFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_Section_Store_GeneralFK",
                table: "Store_Section",
                column: "Store_GeneralFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_StockIn_Common_ColorFK",
                table: "Store_StockIn",
                column: "Common_ColorFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_StockIn_Common_RawItemFK",
                table: "Store_StockIn",
                column: "Common_RawItemFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_StockIn_Common_SupplierFK",
                table: "Store_StockIn",
                column: "Common_SupplierFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_StockIn_Store_GeneralFK",
                table: "Store_StockIn",
                column: "Store_GeneralFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_StockIn_Store_StockInMasterFK",
                table: "Store_StockIn",
                column: "Store_StockInMasterFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_StockInMaster_Common_SupplierFK",
                table: "Store_StockInMaster",
                column: "Common_SupplierFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_StockInMaster_PurchaseOrderFK",
                table: "Store_StockInMaster",
                column: "PurchaseOrderFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_StockInMaster_Store_GeneralFK",
                table: "Store_StockInMaster",
                column: "Store_GeneralFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_StockInSlave_Common_RawItemFK",
                table: "Store_StockInSlave",
                column: "Common_RawItemFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_StockInSlave_Store_StockInMasterFK",
                table: "Store_StockInSlave",
                column: "Store_StockInMasterFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_StockOut_Common_ColorFK",
                table: "Store_StockOut",
                column: "Common_ColorFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_StockOut_Common_RawItemFK",
                table: "Store_StockOut",
                column: "Common_RawItemFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_StockOut_Common_SupplierFK",
                table: "Store_StockOut",
                column: "Common_SupplierFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_StockOut_Store_GeneralFK",
                table: "Store_StockOut",
                column: "Store_GeneralFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_StockOut_Store_StockOutMasterFK",
                table: "Store_StockOut",
                column: "Store_StockOutMasterFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_StockOutSlave_Common_ColorFK",
                table: "Store_StockOutSlave",
                column: "Common_ColorFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_StockOutSlave_Common_RawItemFK",
                table: "Store_StockOutSlave",
                column: "Common_RawItemFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_StockOutSlave_Common_SupplierFK",
                table: "Store_StockOutSlave",
                column: "Common_SupplierFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_StockOutSlave_Store_GeneralFK",
                table: "Store_StockOutSlave",
                column: "Store_GeneralFK");

            migrationBuilder.CreateIndex(
                name: "IX_Store_StockOutSlave_Store_StockOutMasterFK",
                table: "Store_StockOutSlave",
                column: "Store_StockOutMasterFK");

            migrationBuilder.CreateIndex(
                name: "IX_User_MenuItem_User_SubMenuFk",
                table: "User_MenuItem",
                column: "User_SubMenuFk");

            migrationBuilder.CreateIndex(
                name: "IX_User_RoleMenuItem_User_MenuItemFk",
                table: "User_RoleMenuItem",
                column: "User_MenuItemFk");

            migrationBuilder.CreateIndex(
                name: "IX_User_RoleMenuItem_User_RoleFK",
                table: "User_RoleMenuItem",
                column: "User_RoleFK");

            migrationBuilder.CreateIndex(
                name: "IX_User_SubMenu_User_MenuFk",
                table: "User_SubMenu",
                column: "User_MenuFk");

            migrationBuilder.CreateIndex(
                name: "IX_User_User_User_AccessLevelFK",
                table: "User_User",
                column: "User_AccessLevelFK");

            migrationBuilder.CreateIndex(
                name: "IX_User_User_User_DepartmentFK",
                table: "User_User",
                column: "User_DepartmentFK");

            migrationBuilder.CreateIndex(
                name: "IX_User_User_User_RoleFK",
                table: "User_User",
                column: "User_RoleFK");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Accounting_JournalSlave");

            migrationBuilder.DropTable(
                name: "Accounting_Transaction");

            migrationBuilder.DropTable(
                name: "Commercial_BBLC");

            migrationBuilder.DropTable(
                name: "Commercial_BBLCPaymentInformation");

            migrationBuilder.DropTable(
                name: "Commercial_BBLCValueAmendment");

            migrationBuilder.DropTable(
                name: "Commercial_ECI");

            migrationBuilder.DropTable(
                name: "Commercial_ECIValueAmendment");

            migrationBuilder.DropTable(
                name: "Commercial_UD");

            migrationBuilder.DropTable(
                name: "Common_Agent");

            migrationBuilder.DropTable(
                name: "Common_Bank");

            migrationBuilder.DropTable(
                name: "Common_Brand");

            migrationBuilder.DropTable(
                name: "Common_BuyerBank");

            migrationBuilder.DropTable(
                name: "Common_BuyerNotifyParty");

            migrationBuilder.DropTable(
                name: "Common_CompanyBank");

            migrationBuilder.DropTable(
                name: "Common_CompanySetup");

            migrationBuilder.DropTable(
                name: "Common_CountryPort");

            migrationBuilder.DropTable(
                name: "Common_CountrySize");

            migrationBuilder.DropTable(
                name: "Common_CurrencyChangesLog");

            migrationBuilder.DropTable(
                name: "Common_InspectionAgent");

            migrationBuilder.DropTable(
                name: "Common_LcType");

            migrationBuilder.DropTable(
                name: "Common_LienBank");

            migrationBuilder.DropTable(
                name: "Common_LineChiefLine");

            migrationBuilder.DropTable(
                name: "Common_LineDevice");

            migrationBuilder.DropTable(
                name: "Common_Model");

            migrationBuilder.DropTable(
                name: "Common_Notification");

            migrationBuilder.DropTable(
                name: "Common_Notify");

            migrationBuilder.DropTable(
                name: "Common_Product");

            migrationBuilder.DropTable(
                name: "Common_RejectType");

            migrationBuilder.DropTable(
                name: "Common_TCTitle");

            migrationBuilder.DropTable(
                name: "HRMS_AttendanceHistoryLog");

            migrationBuilder.DropTable(
                name: "HRMS_AttendanceRemarks");

            migrationBuilder.DropTable(
                name: "HRMS_CheckInOuts");

            migrationBuilder.DropTable(
                name: "HRMS_EarnLeave");

            migrationBuilder.DropTable(
                name: "HRMS_EmployeeDependent");

            migrationBuilder.DropTable(
                name: "HRMS_EmployeeEarnLeaveCalculation");

            migrationBuilder.DropTable(
                name: "HRMS_EmployeeEducation");

            migrationBuilder.DropTable(
                name: "HRMS_EmployeeExperience");

            migrationBuilder.DropTable(
                name: "HRMS_EmployeeImage");

            migrationBuilder.DropTable(
                name: "HRMS_EmployeeLeaveAssign");

            migrationBuilder.DropTable(
                name: "HRMS_EmployeeNominee");

            migrationBuilder.DropTable(
                name: "HRMS_EmployeeReference");

            migrationBuilder.DropTable(
                name: "HRMS_EmployeeServiceBenifit");

            migrationBuilder.DropTable(
                name: "HRMS_EmployeeSkill");

            migrationBuilder.DropTable(
                name: "HRMS_EmployeeTraining");

            migrationBuilder.DropTable(
                name: "HRMS_EmployeeWeekend");

            migrationBuilder.DropTable(
                name: "HRMS_Holiday");

            migrationBuilder.DropTable(
                name: "HRMS_JobDescription");

            migrationBuilder.DropTable(
                name: "HRMS_LeaveApplication");

            migrationBuilder.DropTable(
                name: "HRMS_OffDay");

            migrationBuilder.DropTable(
                name: "HRMS_ShiftAssign");

            migrationBuilder.DropTable(
                name: "Inventorydb");

            migrationBuilder.DropTable(
                name: "Merchandising_Packing");

            migrationBuilder.DropTable(
                name: "Merchandising_StyleShipmentRatio");

            migrationBuilder.DropTable(
                name: "Merchandising_YarnCalculation");

            migrationBuilder.DropTable(
                name: "Merchandising_YarnType");

            migrationBuilder.DropTable(
                name: "Mis_AccountsPayable");

            migrationBuilder.DropTable(
                name: "Mis_AccountsReceivable");

            migrationBuilder.DropTable(
                name: "Mis_BalanceSheet");

            migrationBuilder.DropTable(
                name: "Mis_BTBLCStatus");

            migrationBuilder.DropTable(
                name: "Mis_CashAtBank");

            migrationBuilder.DropTable(
                name: "Mis_CashInHand");

            migrationBuilder.DropTable(
                name: "Mis_CMEarned");

            migrationBuilder.DropTable(
                name: "Mis_DailyAttendance");

            migrationBuilder.DropTable(
                name: "Mis_DailyProduction");

            migrationBuilder.DropTable(
                name: "Mis_FinancialStatus");

            migrationBuilder.DropTable(
                name: "Mis_IncomeStatement");

            migrationBuilder.DropTable(
                name: "Mis_Log");

            migrationBuilder.DropTable(
                name: "Mis_OrderConfirm");

            migrationBuilder.DropTable(
                name: "Mis_RawMaterials");

            migrationBuilder.DropTable(
                name: "Mis_ShipmentStatus");

            migrationBuilder.DropTable(
                name: "Payroll_BonusDetails");

            migrationBuilder.DropTable(
                name: "Payroll_BonusSettings");

            migrationBuilder.DropTable(
                name: "Payroll_EmployeePromotionalHistory");

            migrationBuilder.DropTable(
                name: "Payroll_EODRecord");

            migrationBuilder.DropTable(
                name: "Payroll_EODReference");

            migrationBuilder.DropTable(
                name: "Payroll_PayrollDetails");

            migrationBuilder.DropTable(
                name: "Payroll_PayrollMaster");

            migrationBuilder.DropTable(
                name: "Plan_DraftLinePlan");

            migrationBuilder.DropTable(
                name: "Plan_MasterLinePlan");

            migrationBuilder.DropTable(
                name: "Plan_OrderProcess");

            migrationBuilder.DropTable(
                name: "Plan_PlanConfig");

            migrationBuilder.DropTable(
                name: "Procurement_PurchaseInvoiceSlave");

            migrationBuilder.DropTable(
                name: "Procurement_PurchaseOrderSlave");

            migrationBuilder.DropTable(
                name: "Prod_DailyAchivement");

            migrationBuilder.DropTable(
                name: "Prod_PlanAchievment");

            migrationBuilder.DropTable(
                name: "Prod_QCOrder");

            migrationBuilder.DropTable(
                name: "Prod_QCType");

            migrationBuilder.DropTable(
                name: "Prod_ReferencePlanFollowupDetails");

            migrationBuilder.DropTable(
                name: "Prod_Remarks");

            migrationBuilder.DropTable(
                name: "Shipment_BillOfExchange");

            migrationBuilder.DropTable(
                name: "Shipment_BillOfExchangeSlave");

            migrationBuilder.DropTable(
                name: "Shipment_DeliveryChallan");

            migrationBuilder.DropTable(
                name: "Shipment_DeliveryChallanSlave");

            migrationBuilder.DropTable(
                name: "Shipment_ExportRealisation");

            migrationBuilder.DropTable(
                name: "Shipment_InvoicedBuyerNotifyParty");

            migrationBuilder.DropTable(
                name: "Shipment_ShipmentInstruction");

            migrationBuilder.DropTable(
                name: "Shipment_ShipmentInstructionSlave");

            migrationBuilder.DropTable(
                name: "Shipment_ShipmentInvoice");

            migrationBuilder.DropTable(
                name: "Shipment_ShipmentInvoiceSlave");

            migrationBuilder.DropTable(
                name: "Shipment_TermsOfShipment");

            migrationBuilder.DropTable(
                name: "Store_AssetDepreciation");

            migrationBuilder.DropTable(
                name: "Store_Bin");

            migrationBuilder.DropTable(
                name: "Store_ItemWiseConsumption");

            migrationBuilder.DropTable(
                name: "Store_ItemWiseConsumptionAllStyle");

            migrationBuilder.DropTable(
                name: "Store_QCInformation");

            migrationBuilder.DropTable(
                name: "Store_RequisitionSlave");

            migrationBuilder.DropTable(
                name: "Store_Section");

            migrationBuilder.DropTable(
                name: "Store_StockInSlave");

            migrationBuilder.DropTable(
                name: "Store_StockOut");

            migrationBuilder.DropTable(
                name: "Store_StockOutPersonal");

            migrationBuilder.DropTable(
                name: "Store_StockOutSlave");

            migrationBuilder.DropTable(
                name: "Store_StockRegister");

            migrationBuilder.DropTable(
                name: "User_RoleMenuItem");

            migrationBuilder.DropTable(
                name: "Accounting_Head");

            migrationBuilder.DropTable(
                name: "Accounting_Journal");

            migrationBuilder.DropTable(
                name: "HRMS_AttendanceHistory");

            migrationBuilder.DropTable(
                name: "HRMS_EducationalDegree");

            migrationBuilder.DropTable(
                name: "HRMS_EmployeeWeekendAssign");

            migrationBuilder.DropTable(
                name: "HRMS_Leave");

            migrationBuilder.DropTable(
                name: "Merchandising_StyleMeasurement");

            migrationBuilder.DropTable(
                name: "Payroll_BonusMaster");

            migrationBuilder.DropTable(
                name: "Payroll_EODRecordMaster");

            migrationBuilder.DropTable(
                name: "Plan_OrderActionStep");

            migrationBuilder.DropTable(
                name: "Procurement_PurchaseInvoice");

            migrationBuilder.DropTable(
                name: "Store_StockIn");

            migrationBuilder.DropTable(
                name: "Procurement_PurchaseRequisitionSlave");

            migrationBuilder.DropTable(
                name: "Plan_MasterOrderPlan");

            migrationBuilder.DropTable(
                name: "Prod_ReferencePlanFollowup");

            migrationBuilder.DropTable(
                name: "Merchandising_StyleSetPack");

            migrationBuilder.DropTable(
                name: "Store_Requisition");

            migrationBuilder.DropTable(
                name: "Store_StockOutMaster");

            migrationBuilder.DropTable(
                name: "User_MenuItem");

            migrationBuilder.DropTable(
                name: "Accounting_Chart2");

            migrationBuilder.DropTable(
                name: "Accounting_CostCenter");

            migrationBuilder.DropTable(
                name: "HRMS_Shift");

            migrationBuilder.DropTable(
                name: "HRMS_Employee");

            migrationBuilder.DropTable(
                name: "Common_Size");

            migrationBuilder.DropTable(
                name: "Payroll_SalaryStructure");

            migrationBuilder.DropTable(
                name: "Plan_OrderAction");

            migrationBuilder.DropTable(
                name: "Store_StockInMaster");

            migrationBuilder.DropTable(
                name: "Merchandising_BOF");

            migrationBuilder.DropTable(
                name: "Merchandising_StyleSlave");

            migrationBuilder.DropTable(
                name: "Procurement_PurchaseRequisition");

            migrationBuilder.DropTable(
                name: "Plan_DraftOrderPlan");

            migrationBuilder.DropTable(
                name: "Prod_ReferencePlan");

            migrationBuilder.DropTable(
                name: "User_SubMenu");

            migrationBuilder.DropTable(
                name: "Accounting_Chart1");

            migrationBuilder.DropTable(
                name: "HRMS_Designation");

            migrationBuilder.DropTable(
                name: "HRMS_PostOffice");

            migrationBuilder.DropTable(
                name: "HRMS_Section");

            migrationBuilder.DropTable(
                name: "Procurement_PurchaseOrder");

            migrationBuilder.DropTable(
                name: "Store_General");

            migrationBuilder.DropTable(
                name: "Merchandising_CBSSlave");

            migrationBuilder.DropTable(
                name: "Merchandising_StyleShipmentSchedule");

            migrationBuilder.DropTable(
                name: "Common_Color");

            migrationBuilder.DropTable(
                name: "Common_LineChief");

            migrationBuilder.DropTable(
                name: "Common_LineSuperVisor");

            migrationBuilder.DropTable(
                name: "Plan_SMVMasterLayout");

            migrationBuilder.DropTable(
                name: "Prod_Reference");

            migrationBuilder.DropTable(
                name: "Common_ProductionLine");

            migrationBuilder.DropTable(
                name: "User_Menu");

            migrationBuilder.DropTable(
                name: "Accounting_Type");

            migrationBuilder.DropTable(
                name: "HRMS_CompanyHierarchyLabel");

            migrationBuilder.DropTable(
                name: "HRMS_PoliceStation");

            migrationBuilder.DropTable(
                name: "User_User");

            migrationBuilder.DropTable(
                name: "Common_Supplier");

            migrationBuilder.DropTable(
                name: "HRMS_Unit");

            migrationBuilder.DropTable(
                name: "Common_Currency");

            migrationBuilder.DropTable(
                name: "Merchandising_CBS");

            migrationBuilder.DropTable(
                name: "Merchandising_Style");

            migrationBuilder.DropTable(
                name: "Plan_SMVDraftLayout");

            migrationBuilder.DropTable(
                name: "Common_SectionLine");

            migrationBuilder.DropTable(
                name: "HRMS_District");

            migrationBuilder.DropTable(
                name: "User_AccessLevel");

            migrationBuilder.DropTable(
                name: "User_Department");

            migrationBuilder.DropTable(
                name: "User_Role");

            migrationBuilder.DropTable(
                name: "HRMS_BusinessUnit");

            migrationBuilder.DropTable(
                name: "Merchandising_CBSStyle");

            migrationBuilder.DropTable(
                name: "Common_RawItem");

            migrationBuilder.DropTable(
                name: "Merchandising_BuyerOrder");

            migrationBuilder.DropTable(
                name: "Common_ProductionFloor");

            migrationBuilder.DropTable(
                name: "Common_Country");

            migrationBuilder.DropTable(
                name: "Common_FinishItem");

            migrationBuilder.DropTable(
                name: "Common_RawSubCategory");

            migrationBuilder.DropTable(
                name: "Common_Buyer");

            migrationBuilder.DropTable(
                name: "Common_ProductionUnit");

            migrationBuilder.DropTable(
                name: "Common_FinishSubCategory");

            migrationBuilder.DropTable(
                name: "Common_Unit");

            migrationBuilder.DropTable(
                name: "Common_RawCategory");

            migrationBuilder.DropTable(
                name: "Common_FinishCategory");
        }
    }
}
